// @(#)USINE/include:$Id:
// Author(s): Maurin et al. (1999-2019)

// C/C++ include
#include <unistd.h>
// ROOT include
#include <TLegend.h>
#include <TRint.h>
#include <TROOT.h>
// USINE include
#include "../include/TUInteractions.h"
#include "../include/TUNumMethods.h"
#include "../include/TUModelBase.h"
#include "../include/TUModel0DLeakyBox.h"
#include "../include/TUModel1DKisoVc.h"
#include "../include/TUModel2DKisoVc.h"
#include "../include/TURunPropagation.h"
#include "../include/TURunOutputs.h"
#include "../include/TUSolMod0DFF.h"
#include "../include/TUSrcMultiCRs.h"
#include "../include/TUSrcPointLike.h"
#include "../include/TUSrcSteadyState.h"


Bool_t DrawFromFitAndCovMat(vector<vector<Double_t>> &drawn_values, TUFreeParList &loaded_pars, string const &fit_file, string const &covmat_file = "NONE", Int_t n_samples = 0, TURunPropagation *run = NULL);
void   LoadUserOptions(string init_file = "$USINE/inputs/init.TEST.par", string output_dir = "$USINE/output", Bool_t is_verbose = true, Bool_t is_saveinfiles = true, Bool_t is_showplots = true, Bool_t is_test = false, FILE *f_test = stdout);
void   Option_E(Int_t argc, char *argv[], Bool_t is_test = false, FILE *f_test = stdout);
void   Option_EA(string const &menu, Bool_t is_test = false, FILE *f_test = stdout);
void   Option_EB(string const &menu, Bool_t is_test = false, FILE *f_test = stdout);
void   Option_EC(string const &menu, Bool_t is_test = false, FILE *f_test = stdout);
void   Option_ED(string const &menu, Bool_t is_verbose = true, Bool_t is_test = false, FILE *f_test = stdout);
void   Option_EE(string const &menu, Bool_t is_test = false, FILE *f_test = stdout);
void   Option_I(string const &exename, string const &opt, string const &opt_fname = "", Bool_t is_test = false, FILE *f_test = stdout);
void   Option_ID(string const &f_crdata, Int_t opt_num, Bool_t is_test = false, FILE *f_test = stdout);
void   Option_IE(Int_t opt_num, Bool_t is_test = false, FILE *f_test = stdout);
void   Option_IX(Int_t opt_num, Bool_t is_test = false, FILE *f_test = stdout);
void   Option_L(Int_t argc, char *argv[]);
void   Option_M(Int_t argc, char *argv[]);
void   Option_T(Int_t argc, char *argv[]);
void   Option_U(Int_t argc, char *argv[]);
FILE   *OutputFile(string const &output_dir, string const &file_name);
void   PrintLogoAndVersion(FILE *f);
void   TestResult(string const &output_dir, string const &test_name);


TApplication *my_app = NULL;
TUInitParList *init_pars = NULL;
TURunPropagation *run = NULL;
string indent = "";

//______________________________________________________________________________
Bool_t DrawFromFitAndCovMat(vector<vector<Double_t>> &drawn_values, TUFreeParList &loaded_pars,
                            string const &fit_file, string const &covmat_file, Int_t n_samples,
                            TURunPropagation *run)
{
   //--- Returns vector of drawn parameters sampled from fit and covariance matrix.
   //    We ensure that drawn parameters are within the range of run configuration.
   //    Returns true if parameters were drawn (i.e. covmat_file != "NONE")
   // INPUTS:
   //  fit_file             File name for best-fit parameters
   //  covmat_file          File name for covariance-matrix of errors (in NONE, do not fill)
   //  n_samples            Number of samples to draw (0 if not drawn)
   //  run                  Used to check boundary of parameters (if covmat_file used, i.e. drawn parameters)
   // OUTPUTS:
   //  drawn_values         [Nsamples][Npars] Values of parameters drawn
   //  loaded_pars          Best-fit values loaded from reading 'fit_file'


   // If covmat_file=='NONE', only use best-fit transport parameters, otherwise, load
   // cov. matrix of errors on best-fit parameters to propagate transport uncertainties.
   vector<Int_t> dummy;
   printf("    - Load parameter best-fit values (USINE-formatted file %s)\n", fit_file.c_str());
   loaded_pars.LoadFitValues(fit_file, dummy, /*is_skipped_fixed*/true, /*is_verbose*/false, stdout);
   //loaded_pars.PrintFitValsAndInit(stdout);
   drawn_values.clear();
   string cov_uc = covmat_file;
   TUMisc::UpperCase(cov_uc);
   if (cov_uc == "NONE")
      return false;


   // If covariance matrix to draw from
   printf("    - Load cov. matrix from fit (USINE-formatted file %s)\n", covmat_file.c_str());
   loaded_pars.LoadFitCovMatrix(covmat_file, dummy, /*is_verbose*/false, stdout);
   //loaded_pars.PrintFitCovMatrix(stdout);
   printf("        => check two files have same parameters (FIXED skipped in fit file)\n");
   loaded_pars.CheckMatchingParNamesAndCovParNames(stdout);

   // Extract best-fit vector and cov.matrix in suitable format to later draw from
   TVectorD bestfit = loaded_pars.ExtractBestFitVals();
   TMatrixDSym covmat = loaded_pars.GetFitCovMatrix();

   Int_t i_sample = 0;
   for (Int_t i_try = 0; i_sample < n_samples; ++i_try) {
      // Draw new parameters
      TVectorD pars_gen;
      TUMath::DrawFromMultiGauss(bestfit, covmat, pars_gen);
      // Update loaded pars from new generated parameters. Note that drawn
      // parameters might be in 'log', and need to be converted appropriately.
      vector<Double_t> drawn(loaded_pars.GetNPars(), 0.);
      for (Int_t i = 0; i < loaded_pars.GetNPars(); ++i) {
         loaded_pars.GetParEntry(i)->SetVal(pars_gen(i), loaded_pars.GetParEntry(i)->GetFitSampling());
         drawn[i] = pars_gen(i);
      }
      // Update current run parameters from content of 'loaded_pars'
      if (!run->GetFitPars()->UpdateParVals(loaded_pars, /*gENUM_FITPARTYPE*/kANY, /*is_enforce_allfound*/ true, /*is_check_range*/true, stdout))
         continue;
      else
         drawn_values.push_back(drawn);

      ++i_sample;
   }
   return true;
}

//______________________________________________________________________________
void LoadUserOptions(string init_file, string output_dir, Bool_t is_verbose,
                     Bool_t is_saveinfiles, Bool_t is_showplots, Bool_t is_test,
                     FILE *f_test)
{
   //--- Loads user options and print/prepare for run (common to any 'usine' command).
   // INPUTS:
   //  init_file       USINE initialisation parfile
   //  output_dir      Directory to store USINE outputs
   //  is_verbose      Additional information on or off
   //  is_saveinfiles  Whether to save or not outputs in files
   //  is_showplots    If true, how plots otherwise, batch mode
   //  is_test         Whether called as test function or not
   //  f_test          File in which to print test

   // Initialise parameters and propagation
   string f_name = output_dir + "/usine.last_run.log";
   FILE *f_log = NULL;
   FILE *f_prompt = stdout;
   if (is_test) {
      f_prompt = f_test;
      f_log = f_test;
   } else
      f_log = fopen(f_name.c_str(), "w");
   fprintf(f_prompt, "%s### Output dir (for run): %s\n", indent.c_str(), output_dir.c_str());
   fprintf(f_prompt, "%s### Log file: %s\n", indent.c_str(), f_name.c_str());
   gROOT->SetBatch(!is_showplots);

   // Load initialisation file, and save configuration in output
   if (init_pars)
      delete init_pars;
   fprintf(f_prompt, "%s### USINE parameters (for run):\n", indent.c_str());
   fprintf(f_prompt, "%s    - Load %s\n", indent.c_str(), init_file.c_str());
   init_pars = new TUInitParList();
   init_pars->SetClass(init_file, is_verbose, f_log);
   if (!is_test) {
      string file_init = output_dir + "/usine.last_run.init.par";
      FILE *fp = fopen(TUMisc::GetPath(file_init).c_str(), "w");
      init_pars->Print(fp, "ALL", "ALL");
      fclose(fp);
      fprintf(f_prompt, "%s    - Save last config in %s\n", indent.c_str(), file_init.c_str());
   }
   // Load initialisation file, and save configuration in output
   if (run)
      delete run;
   fprintf(f_prompt, "%s### USINE run:\n", indent.c_str());
   fprintf(f_prompt, "%s    - Initialise class\n", indent.c_str());
   run = new TURunPropagation();
   run->SetClass(init_pars, is_verbose, output_dir, f_log);
   run->SetIsSaveInFiles(is_saveinfiles);
   f_log = NULL;
}

//______________________________________________________________________________
void Option_E(Int_t argc, char *argv[], Bool_t is_test, FILE *f_test)
{
   //--- USINE -e option for extra plots (text user interface).
   //  argc              Number of user parameters
   //  argv              Value of user parameters
   //  is_test         Whether called as test function or not
   //  f_test          File in which to print test

   if ((argc < 7 && argc != 2) || (argc == 2 && (string)argv[1] != "-eALL")) {
      printf("USAGE:   %s -e  init_file  output_dir  is_verbose  is_saveinfiles  is_showplots\n", argv[0]);
      printf("EXAMPLE: %s -e   inputs/init.TEST.par  $USINE/output   1   1   1\n", argv[0]);
      printf("\n"
             "   [init_file]       USINE initialisation file\n"
             "   [output_dir]      Directory for outputs (plots, files, etc.)\n"
             "   [is_verbose]      To have more informations on the run (printed in logfile)\n"
             "   [is_saveinfiles]  Save outputs of calculation and macros for plots (1) or not (0)\n"
             "   [is_showplots]    Show plots (1) or batch mode (0)\n");
      printf("\n");
      return;
   }
   indent = TUMessages::Indent(true);
   TUMessages::Indent(false);

   string opt = argv[1];
   TUMisc::UpperCase(opt);

   // Run all options
   if (opt == "-EALL") {
      // Set default values for -e arguments
      string f_init = "inputs/init.TEST.par";
      LoadUserOptions(f_init, "$USINE/usine.tests", false, false, false, is_test, f_test);

      // Loop on all e options
      const Int_t n_opt = 26;
      string options[n_opt] = {"-EA", "-EB", "-EC",
                               "-ED0", "-ED1", "-ED2a", "-ED2b", "-ED3", "-ED3+", "-ED4", "-ED4+",
                               "-ED5", "-ED6", "-ED7", "-ED8", "-ED8-", "-ED9",
                               "-EE1", "-EE2", "-EE3", "-EE4", "-EE5", "-EE6", "-EE7", "-EE8", "-EE9"
                              };
      for (Int_t i = 0; i < n_opt; ++i) {
         string message = "Test option " + options[i];
         TUMessages::Separator(f_test, message);
         // Extract options
         string menu = options[i].substr(2);
         if (f_test != stdout)
            cout << "            ./bin/usine " << options[i] << endl;
         //cout << "                       " << options[i] << endl;
         if (menu[0] == 'A')
            Option_EA(menu, is_test, f_test);
         else if (menu[0] == 'B')
            Option_EB(menu, is_test, f_test);
         else if (menu[0] == 'C')
            Option_EC(menu, is_test, f_test);
         else if (menu[0] == 'D')
            Option_ED(menu, true, is_test, f_test);
         else if (menu[0] == 'E')
            Option_EE(menu, is_test, f_test);
      }
      return;
   }

   // LoadUserOptions(init_file, output_dir, is_verbose, is_saveinfiles, is_showplots)
   Bool_t is_verbose = atoi(argv[4]);
   LoadUserOptions(argv[2], TUMisc::CreateDir(argv[3], "usine", "Option_E"), is_verbose, atoi(argv[5]), atoi(argv[6]));
   printf("%s    - Calculate\n", indent.c_str());

   // Loop on text-user interface
   do {
      TUMessages::Separator(stdout, "Text-User Interface", 2);

      printf("    A) PROPAGATION MODEL RESULTS (PRINTS & PLOTS)\n");
      printf("       A1   Local IS/TOA fluxes, isot/elem + prim/sec fractions\n");
      printf("       A2   Local GCR vs SS abundances\n");
      printf("       A3   [TODO] Spatial distribution (1D or 2D depending on models selected)\n");
      printf("\n");
      printf("    B) MODIFY MODEL PARAMETERS AND RERUN (B to modify all)\n");
      printf("       B1   Propagation switches\n");
      printf("       B2   Transport parameters\n");
      printf("       B3   CR source parameters\n");
      printf("\n");
      printf("    C) INFO ON MODEL/PARAMETERS (C to print all)\n");
      printf("       C1   Models (propagation and modulation)\n");
      printf("       C2   Geometry\n");
      printf("       C3   Transport parameters\n");
      printf("       C4   Propagation switches\n");
      printf("       C5   CR sources\n");
      printf("       C6   ISM\n");
      printf("       C7   CR list and parents (and E grids)\n");
      printf("       C8   CR and normalisation data\n");
      printf("       C9   X-section files and targets\n");
      printf("\n");
      printf("    D) EXTRA PLOTS\n");
      printf("       ... Nuclear production related ...\n");
      printf("           D0   Relative contributions (primary, secondary, radioactive) in isotopes and elements\n");
      printf("           D1   Ranking (propag.-weighted) of multi-step channels: ISM-weighted + effective XS (including ghosts)\n");
      printf("           D2a  Ranking (propag.-weighted) of individual effective XS (ISM elements separately)\n");
      printf("           D2b  Ranking (propag.-weighted) of ghost-separated XS (ISM elements separately)\n");
      printf("               [D1+, D2a+, and D2b+ to use hard-coded propag.params]\n");
      printf("               (see TURunPropagation::Plots_XSProdRanking)\n");
      if (run->GetPropagModel(0)->IsDecayBETA() || run->GetPropagModel(0)->IsDecayEC()) {
         printf("       ... Decay related ...\n");
         if (run->GetPropagModel(0)->IsDecayBETA())
            printf("           D3   BETA-decay species [D3+ to check decayed=appeared]\n");
         if (run->GetPropagModel(0)->IsDecayEC())
            printf("           D4   [TODO] EC-decay species [D4+ to check decayed=appeared]\n");
      }
      if (run->GetPropagModel(0)->IsAtLeast1DiffProd()) {
         printf("       ... Differential production ...\n");
         printf("           D5   Source terms per reaction (before propagation)\n");
         printf("           D6   Contributions per reaction (with propagation)\n");
         printf("           D7   Contributions per energy range (with propagation)\n");
      }
      if (run->GetPropagModel(0)->IsAtLeastOneAntinuc()) {
         if (run->GetPropagModel(0)->IsTertiaries())
            printf("           D8   Tertiary contributions for antinuclei [D8- to set sigINAtot to 0.]\n");
         if (run->GetPropagModel(0)->IsPrimExotic())
            printf("           D9   [TODO] Primary antinuclei: comparison to analytical approximations\n");
      }
      printf("\n");
      printf("    E) COMPARISON PLOTS VARYING SOME INGREDIENTS (\'+\' to normalise to data for each config., e.g. E3+)\n");
      printf("           E1   Boundary conditions\n");
      printf("       ... Physics parameters ...\n");
      printf("           E2   Switch on/off propagation effects (losses, decay, etc.)\n");
      printf("       ... X-sections ...\n");
      printf("           E3   [NUC] Inelastic (files $USINE/inputs/XS_NUCLEI/sigInel*)\n");
      printf("           E4   [NUC] Production (files $USINE/inputs/XS_NUCLEI/sigProd*)\n");
      if (run->GetPropagModel(0)->IsAtLeastOneAntinuc()) {
         printf("           E5   [ANTINUC] Inelastic (files $USINE/inputs/XS_ANTINUC/sigInel*)\n");
         printf("           E6   [ANTINUC] Production (files $USINE/inputs/XS_ANTINUC/dSdE*)\n");
         printf("           E7   [ANTINUC] Tertiary: NONAN (files $USINE/inputs/XS_ANTINUC/sigInelNONANN*)\n");
         printf("           E8   [ANTINUC] Tertiary: redistribution (files $USINE/inputs/XS_ANTINUC/*tertiary*)\n");
      }
      if (run->GetPropagModel(0)->IsAtLeastOneLepton())
         printf("           E9   [LEPTON] Production X-sections (files $USINE/inputs/XS_ANTINUC/dSdE*)\n");
      printf("\n");

      cout << indent << "    [Q to quit]" << endl;
      string menu = "";
      cout << endl;
      cout << indent << " >> selection [e.g., A1]: ";
      cin >> menu;
      TUMisc::UpperCase(menu);

      if (menu[0] == 'A')      Option_EA(menu);
      else if (menu[0] == 'B') Option_EB(menu);
      else if (menu[0] == 'C') Option_EC(menu);
      else if (menu[0] == 'D') Option_ED(menu, is_verbose);
      else if (menu[0] == 'E') Option_EE(menu);

      if (menu == "Q")
         break;
   } while (1);

   // Quit the session
   TUMessages::Separator(stdout, "End of session... Bye!!!", 2);

   // Close log file if necessary
   run->CloseOutputLog();
   return;
}

//______________________________________________________________________________
void Option_EA(string const &menu, Bool_t is_test, FILE *f_test)
{
   //--- A selections: local fluxes and spatial distributions.
   //      -> A1: local IS and TOA fluxes (and primary./sec./elemental fractions)
   //             [A1+ to have separately isotopic contributions]
   //      -> A2: GCR vs SS abundances
   //      -> A3: Spatial distribution [TODO]
   //  menu              Option to pass (from A1 to E9...)
   //  is_test           Whether called as test function or not
   //  f_test          File in which to print test

   if (menu == "A" || menu == "A1") {
      if (is_test) {
         TUCoordTXYZ *coord_txyz = NULL;
         TURunOutputs results;
         run->FillSpectra(results, "B/C,O:kR:0.,0.5,1.2", run->GetDiplayedFluxEPower(), coord_txyz, is_test, f_test);
         run->PlotSpectra(my_app, results, false, is_test, f_test);
      } else
         run->LoopDisplaySpectra(my_app, false);
   }
   if (menu == "A" || menu == "A1+") {
      if (is_test) {
         TUCoordTXYZ *coord_txyz = NULL;
         TURunOutputs results;
         run->FillSpectra(results, "B/C,O:kR:0.,0.5,1.2", run->GetDiplayedFluxEPower(), coord_txyz, is_test, f_test);
         run->PlotSpectra(my_app, results, true, is_test, f_test);
      } else
         run->LoopDisplaySpectra(my_app, true);
   }

   if (menu == "A" || menu == "A2") {
      if (is_test)
         run->Plots_GCRvsSSAbund(my_app, 1., is_test, f_test);
      else
         run->Plots_GCRvsSSAbund(my_app, -1.);
   }

   if (menu == "A" || menu == "A3") {
      fprintf(f_test, "   [TO DO] Not implemented yet...\n");
      return;// [TODO] Loop_spatial_distribution();
   }
}

//______________________________________________________________________________
void Option_EB(string const &menu, Bool_t is_test, FILE *f_test)
{
   //--- B selections: modify model parameters.
   //      -> B1 = propagation switches
   //      -> B2 = transport parameters
   //      -> B3 = source parameters
   //  menu              Option to pass (from A1 to E9...)
   //  is_test           Whether called as test function or not
   //  f_test          File in which to print test

   // Nothing really that can be done as integration test here...
   if (is_test) {
      fprintf(f_test, "   No integration tests meaningful for B options\n");
      return;
   }

   if (menu == "B" || menu == "B1") {
      //  is_test           Whether called as test function or not
      TUMessages::Separator(f_test, "Modify propagation switches");
      run->TUI_ModifyPropagSwitches();
      for (Int_t n = 0; n < run->GetNPropagModels(); ++n) {
         if (run->GetPropagModel(n)->TUPropagSwitches::GetStatus()) {
            run->GetPropagModel(n)->SetPropagated(false);
            run->GetPropagModel(n)->TUPropagSwitches::SetStatus(false);
         }
      }
   }
   if (menu == "B" || menu == "B2") {
      TUMessages::Separator(f_test, "Modify transport parameters");
      for (Int_t n = 0; n < run->GetNPropagModels(); ++n) {
         if (run->GetNPropagModels() > 1)
            fprintf(f_test, "%s  Model=%s\n", indent.c_str(), run->GetPropagModel(n)->GetModelName().c_str());
         run->GetPropagModel(n)->TUI_ModifyTransportParameters();
      }
      // Update model setup/precalculations
      for (Int_t n = 0; n < run->GetNPropagModels(); ++n) {
         // Check whether to propagate next run (in case of a change)
         if (run->GetPropagModel(n)->TUFreeParList::GetParListStatus())
            run->GetPropagModel(n)->SetPropagated(false);
         // Update and reset transport parameters
         if (run->GetPropagModel(n)->GetFreeParsStatusTransport())
            run->GetPropagModel(n)->GetTransport()->UpdateFromFreeParsAndResetStatus();
      }
   }
   if (menu == "B" || menu == "B3") {
      TUMessages::Separator(f_test, "Modify CR source parameters");
      for (Int_t n = 0; n < run->GetNPropagModels(); ++n) {
         if (run->GetNPropagModels() > 1)
            fprintf(f_test, "%s  Model=%s\n", indent.c_str(), run->GetPropagModel(n)->GetModelName().c_str());
         run->GetPropagModel(n)->TUI_ModifySrcParameters();
      }
      // Update model setup/precalculations
      for (Int_t n = 0; n < run->GetNPropagModels(); ++n) {
         // Check whether to propagate next run (in case of a change)
         if (run->GetPropagModel(n)->TUFreeParList::GetParListStatus())
            run->GetPropagModel(n)->SetPropagated(false);
         // Loop on changes astro sources
         for (Int_t s = 0; s < run->GetPropagModel(n)->GetNSrcAstro(); ++s) {
            if (run->GetPropagModel(n)->GetFreeParsStatusSrcAstro(s))
               run->GetPropagModel(n)->GetSrcAstro(s)->UpdateFromFreeParsAndResetStatus();
         }
         // Loop on changes DM sources
         for (Int_t s = 0; s < run->GetPropagModel(n)->GetNSrcDM(); ++s) {
            if (run->GetPropagModel(n)->GetFreeParsStatusSrcDM(s))
               run->GetPropagModel(n)->GetSrcDM(s)->UpdateFromFreeParsAndResetStatus();
         }
      }
   }
}

//______________________________________________________________________________
void Option_EC(string const &menu, Bool_t is_test, FILE *f_test)
{
   //--- C selections: info on model parameters.
   //      -> C1 = list of models
   //      -> C2 = model geometry
   //      -> C3 = transport parameters
   //      -> C4 = propagation switches
   //      -> C5 = cosmic-ray sources
   //      -> C6 = ISM
   //      -> C7 = CR lists and parents
   //      -> C8 = XS files and targets
   //      -> C9 = Data used for renormalisation
   //  menu              Option to pass (from A1 to E9...)
   //  is_test           Whether called as test function or not
   //  f_test          File in which to print test

   Int_t sleep_sec = 2;
   if (menu == "C" || menu == "C1") {
      run->PrintListOfModels(f_test);
      if (!is_test) sleep(sleep_sec);
   }

   if (menu == "C" || menu == "C2") {
      TUMessages::Separator(f_test, "Geometry");
      for (Int_t n = 0; n < run->GetNPropagModels(); ++n) {
         if (run->GetNPropagModels() > 1)
            fprintf(f_test, "%s  Model=%s\n", indent.c_str(), run->GetPropagModel(n)->GetModelName().c_str());
         run->GetPropagModel(n)->GetAxesTXYZ()->PrintSummary(f_test);
         TUFreeParList *free_pars = run->GetPropagModel(n)->GetAxesTXYZ()->GetFreePars();
         if (free_pars)
            free_pars->PrintPars(f_test);
         free_pars = NULL;
      }
      if (!is_test) sleep(sleep_sec);
   }

   if (menu == "C" || menu == "C3") {
      TUMessages::Separator(f_test, "Transport parameters");
      for (Int_t n = 0; n < run->GetNPropagModels(); ++n) {
         if (run->GetNPropagModels() > 1)
            fprintf(f_test, "%s  Model=%s\n", indent.c_str(), run->GetPropagModel(n)->GetModelName().c_str());
         run->GetPropagModel(n)->PrintSummaryTransport(f_test);
         TUFreeParList *free_pars = run->GetPropagModel(n)->GetTransport()->GetFreePars();
         if (free_pars)
            free_pars->PrintPars(f_test);
         free_pars = NULL;
      }
      if (!is_test) sleep(sleep_sec);
   }

   if (menu == "C" || menu == "C4") {
      TUMessages::Separator(f_test, "Propagation switches");
      run->GetPropagModel(0)->PrintPropagSwitches(f_test);
      if (!is_test) sleep(sleep_sec);
   }

   if (menu == "C" || menu == "C5") {
      TUMessages::Separator(f_test, "CR sources");
      for (Int_t n = 0; n < run->GetNPropagModels(); ++n) {
         if (run->GetNPropagModels() > 1)
            fprintf(f_test, "%s  Model=%s\n", indent.c_str(), run->GetPropagModel(n)->GetModelName().c_str());
         run->GetPropagModel(n)->PrintSources(f_test, false);
      }
      if (!is_test) sleep(sleep_sec);
   }

   if (menu == "C" || menu == "C6") {
      TUMessages::Separator(f_test, "ISM");
      for (Int_t n = 0; n < run->GetNPropagModels(); ++n) {
         if (run->GetNPropagModels() > 1)
            fprintf(f_test, "%s  Model=%s\n", indent.c_str(), run->GetPropagModel(n)->GetModelName().c_str());
         run->GetPropagModel(n)->GetISM()->PrintSummary(f_test);
         TUFreeParList *free_pars = run->GetPropagModel(n)->GetISM()->GetFreePars();
         if (free_pars)
            free_pars->PrintPars(f_test);
         free_pars = NULL;
      }
      if (!is_test) sleep(sleep_sec);
   }

   if (menu == "C" || menu == "C7") {
      TUMessages::Separator(f_test, "CR list and parents (and E grids)");
      run->GetPropagModel(0)->PrintCRSummary(f_test);
      run->GetPropagModel(0)->TUAxesCrE::PrintESummary(f_test);
      indent = TUMessages::Indent(true);
      indent = TUMessages::Indent(true);
      fprintf(f_test, "=> atomic properties from: %s\n", run->GetPropagModel(0)->TUAxesCrE::GetFileName().c_str());
      TUMessages::Indent(false);
      TUMessages::Indent(false);
      if (!is_test) sleep(sleep_sec);
   }

   if (menu == "C" || menu == "C8") {
      TUMessages::Separator(f_test, "X-section files and targets");
      run->GetPropagModel(0)->PrintTargetList(f_test);
      run->GetPropagModel(0)->PrintTertiaryList(f_test);
      run->GetPropagModel(0)->PrintXSecFiles(f_test);
      if (!is_test) sleep(sleep_sec);
   }

   if (menu == "C" || menu == "C9") {
      TUMessages::Separator(f_test, "CRs and normalisation data");
      run->GetPropagModel(0)->TUDataSet::PrintFileNames(f_test);
      run->GetPropagModel(0)->PrintNormData(f_test);
      if (!is_test) sleep(sleep_sec);
   }
   fprintf(f_test, "\n");
}

//______________________________________________________________________________
void Option_ED(string const &menu, Bool_t is_verbose, Bool_t is_test, FILE *f_test)
{
   //--- D selections: extra plots.
   //      -> D0 = histograms of primary/secondary/radioactive fractions (elements and isotopes)
   //      -> D1 = ranking of channels for secondary production of an element (and its isotopes)
   //      -> D2 = ranking of XS (with or w/o ghosts) for secondary production of an element (and its isotopes)
   //      -> D3 = fractions of (dis-)appeared beta-decay parent/daughter and impact on associated elements
   //      -> D4 = [TODO] fractions of (dis-)appeared EX-decay parent/daughter and impact on associated elements
   //      -> D5-D8 = antinuclei (source terms, , redistribution of energies, etc.)
   //  menu              Option to pass (from A1 to E9...)
   //  is_verbose        Chatter on or off
   //  is_test           Whether called as test function or not
   //  f_test          File in which to print test


   // Which dependence to display (only enabled for D5 to D8 options for now)
   gENUM_ETYPE e_type = kEKN;
   if (!is_test && (menu == "D5" || menu == "D6" || menu == "D7" || menu == "D8" || menu == "D8-")) {
      // E-type for this list of combos
      cout << "### Select e-type to display (kEKN, kR, kETOT, or kEK):" << endl;
      cout << "    >>  ";
      string tmp = "";
      cin >> tmp;
      TUEnum::Name2Enum(tmp, e_type);
   }

   if (menu.find("D0") != string::npos)
      run->Plots_XSProdRanking(my_app, menu, e_type, is_test, f_test);
   else if (menu.find("D1") != string::npos || menu.find("D2") != string::npos) {
      // Ensure that the code runs with or without ghosts
      // (need to delete and update ghost before run)
      Int_t i_ghost = init_pars->IndexPar("Base", "ListOfCRs", "IsLoadGhosts");
      Bool_t is_same = true;
      if (menu.substr(0, 2) == "D1" || menu.substr(0, 3) == "D2A") {
         if (atoi(init_pars->GetParEntry(i_ghost).GetVal().c_str())) {
            is_same = false;
            init_pars->SetVal(i_ghost, "0"/*false*/);
         }
      } else if (menu.substr(0, 3) == "D2B") {
         if (!atoi(init_pars->GetParEntry(i_ghost).GetVal().c_str())) {
            is_same = false;
            init_pars->SetVal(i_ghost, "1"/*true*/);
         }
         // Add a ghost-separately XS (for the code not to crash)
         Int_t i_xs = init_pars->IndexPar("Base", "XSections", "fProd");
         string xs = "$USINE/inputs/XS_NUCLEI/GHOST_SEPARATELY/sigProdGALPROP17_OPT12.dat";
         init_pars->SetVal(i_xs, xs);
      }
      if (!is_same) {
         string output_dir = run->GetOutputDir();
         run->Initialise(/*is_delete*/true);
         run->SetClass(init_pars, false, output_dir, f_test);
         //run->SetIsSaveInFiles(is_save);
      }
      run->Plots_XSProdRanking(my_app, menu, e_type, is_test, f_test);
   } else if (menu == "D3") run->Plots_DecayOnOff(my_app, /*is_beta_or_ec*/true, /*is_test_exactmatch*/false, menu, e_type, is_test, f_test);
   else if (menu == "D3+")  run->Plots_DecayOnOff(my_app, /*is_beta_or_ec*/true, /*is_test_exactmatch*/true, menu, e_type, is_test, f_test);
   else if (menu == "D4")   return; //[TODO] run->Plots_DecayOnOff(my_app, /*is_beta_or_ec*/false, /*is_test_exactmatch*/false, menu, is_test, f_test);
   else if (menu == "D4+")  return; //[TODO] run->Plots_DecayOnOff(my_app, /*is_beta_or_ec*/false, /*is_test_exactmatch*/true, menu, is_test, f_test);
   else if (menu == "D5")   run->Plots_AntinucExtra(my_app, /*switch_plot*/0, /*is_noina*/false, menu, e_type, is_test, f_test);
   else if (menu == "D6")   run->Plots_AntinucExtra(my_app, /*switch_plot*/1, /*is_noina*/false, menu, e_type, is_test, f_test);
   else if (menu == "D7")   run->Plots_AntinucExtra(my_app, /*switch_plot*/2, /*is_noina*/false, menu, e_type, is_test, f_test);
   else if (menu == "D8")   run->Plots_AntinucExtra(my_app, /*switch_plot*/3, /*is_noina*/false, menu, e_type, is_test, f_test);
   else if (menu == "D8-")  run->Plots_AntinucExtra(my_app, /*switch_plot*/3, /*is_noina*/true, menu, e_type, is_test, f_test);
   else if (menu == "D9")   return;// [TODO]
   if (!is_test)
      cout << endl;
}

//______________________________________________________________________________
void Option_EE(string const &menu, Bool_t is_test, FILE *f_test)
{
   //--- E selections: comparison plots for local fluxes
   //      -> E1 = compare calculations from different boundary conditions
   //      -> E2 = switch on/off some propagation effects (E losses, convection, inelastic XS, etc.)
   //      -> E3-E4 (nuclei) = compare calculations from different XS datasets
   //      -> E5-E8 (antinuclei) = compare calculations from different XS datasets
   //      -> E9 (leptons) = compare calculations from different XS datasets
   //  menu              Option to pass (from A1 to E9...)
   //  is_test           Whether called as test function or not
   //  f_test          File in which to print test

   // Check at least one model (warning if more than one model): only calculate for model 0
   if (run->GetNPropagModels() == 0) {
      TUMessages::Warning(f_test, "usine", "(option E)", "No propagation model loaded");
      return;
   } else if (run->GetNPropagModels() > 1) {
      string message = "Display result for first propagation model only (" + run->GetPropagModel(0)->GetModelName() + ")";
      TUMessages::Warning(f_test, "usine", "(option E)", message);
      return;
   }

   // [BOUNDARY CONDITIONS]
   if (menu == "E1")        run->Plots_ImpactBoundaryConditions(my_app, false, menu, is_test, f_test);
   else if (menu == "E1+")  run->Plots_ImpactBoundaryConditions(my_app, true, menu, is_test, f_test);
   // [SWITCHES]
   else if (menu == "E2")   run->Plots_ImpactPropagSwitches(my_app, false, menu, is_test, f_test);
   else if (menu == "E2+")  run->Plots_ImpactPropagSwitches(my_app, true, menu, is_test, f_test);
   // [XS NUC]
   else if (menu == "E3")   run->Plots_ImpactXSOnCombos(my_app, 0, false, menu, is_test, f_test);
   else if (menu == "E3+")  run->Plots_ImpactXSOnCombos(my_app, 0, true, menu, is_test, f_test);
   else if (menu == "E4")   run->Plots_ImpactXSOnCombos(my_app, 1, false, menu, is_test, f_test);
   else if (menu == "E4+")  run->Plots_ImpactXSOnCombos(my_app, 1, true, menu, is_test, f_test);
   // [XS ANTINUC]
   else if (menu == "E5")   run->Plots_ImpactXSOnCombos(my_app, 2, false, menu, is_test, f_test);
   else if (menu == "E5+")  run->Plots_ImpactXSOnCombos(my_app, 2, true, menu, is_test, f_test);
   else if (menu == "E6")   run->Plots_ImpactXSOnCombos(my_app, 3, false, menu, is_test, f_test);
   else if (menu == "E6+")  run->Plots_ImpactXSOnCombos(my_app, 3, true, menu, is_test, f_test);
   else if (menu == "E7")   run->Plots_ImpactXSOnCombos(my_app, 4, false, menu, is_test, f_test);
   else if (menu == "E7+")  run->Plots_ImpactXSOnCombos(my_app, 4, true, menu, is_test, f_test);
   else if (menu == "E8")   run->Plots_ImpactXSOnCombos(my_app, 5, false, menu, is_test, f_test);
   else if (menu == "E8+")  run->Plots_ImpactXSOnCombos(my_app, 5, true, menu, is_test, f_test);
   // [XS LEPTON]
   else if (menu == "E9")   run->Plots_ImpactXSOnCombos(my_app, 5, false, menu, is_test, f_test);
   else if (menu == "E9+")  run->Plots_ImpactXSOnCombos(my_app, 5, true, menu, is_test, f_test);
   fprintf(f_test, "\n");
}

//______________________________________________________________________________
void Option_I(string const &exename, string const &opt, string const &opt_fname, Bool_t is_test, FILE *f_test)
{
   //--- USINE -i option for display of various USINE inputs.
   //  exename           Name of executable
   //  opt               Flag/option full name (e.g., -id1, -iALL)
   //  opt_fname         If option requires a file name
   //  is_test           Whether run in test mode (use default values)
   //  f_test            File in which to output result of test

   string opt_base = opt.substr(0, 3);
   Int_t opt_num = -1;
   if (opt.size() == 4) {
      const char *c = opt.substr(3, 4).c_str();
      opt_num = atoi(c);
   }

   if ((opt != "-iALL" && opt_base != "-id" && opt_base != "-ie" && opt_base != "-ix") || (opt != "-iALL" && opt_num == -1)) {
      printf("  USAGE: %s flags  input_file\n", exename.c_str());
      printf("   [input_file]    [Optional] USINE input file (for -i and -x options) \n"
             "   [flags]\n"
             "             -id  crdata_crdb_file  (e.g., one of the inputs/crdata_* files) \n"
             "                    -id1   Extract all data from experiment name\n"
             "                    -id2   Extract all data from experiment/combo name\n"
             "                    -id3   Plot CR data from user-selection\n"
             "                    -id4   Plot demodulated CR data from user-selection\n"
             "             -ie\n"
             "                    -ie1   Plot energy losses for nuclei\n"
             "                    -ie2   Plot EC cross sections\n"
             "             -ix   init_file (e.g., $USINE/inputs/init.TEST.par)\n"
             "                    -ix1   Plot inelastic XS (P+T->X) from files in \'init_file\'\n"
             "                    -ix2   Plot also production XS (P+T->F) from files in \'init_file\'\n"
             "\n");
      return;
   }

   // Run all options
   if (opt == "-iALL") {
      const Int_t n_opt = 8;
      string options[n_opt] = {"-id1", "-id2", "-id3", "-id4", "-ie1", "-ie2", "-ix1", "-ix2"};
      for (Int_t i = 0; i < n_opt; ++i) {
         if (f_test != stdout)
            cout << "            ./bin/usine " << options[i] << endl;
         //cout << "                       " << options[i] << endl;
         string message = "Test option " + options[i];
         TUMessages::Separator(f_test, message);
         Option_I(exename, options[i], opt_fname, is_test, f_test);
      }
   }

   // Define default parameter file
   string f_init = "inputs/init.TEST.par";
   if (opt_base == "-ix" && opt_fname != "")
      f_init = opt_fname;
   // Fill parameter file
   if (!init_pars) {
      init_pars = new TUInitParList();
      init_pars->SetClass(TUMisc::GetPath(f_init), false, f_test);
   }

   // Call different options
   if (opt_base == "-id") {
      string f_crdata = init_pars->GetParEntry(init_pars->IndexPar("Base", "CRData", "fCRData", false)).GetVal();
      if (opt_fname != "")
         f_crdata = opt_fname;
      Option_ID(f_crdata, opt_num, is_test, f_test);
   } else if (opt_base == "-ie")
      Option_IE(opt_num, is_test, f_test);
   else if (opt_base == "-ix")
      Option_IX(opt_num, is_test, f_test);

   return;
}

//______________________________________________________________________________
void Option_ID(string const &f_crdata, Int_t opt_num, Bool_t is_test, FILE *f_test)
{
   //--- USINE -id option for display of CR data.
   //  f_crdata          File of CR data to use
   //  opt_num           Option number
   //  is_test           Whether run in test mode (use default values)
   //  f_test            File in which to output result of test

   TUDataSet *crdata = new TUDataSet();
   crdata->LoadData(f_crdata, true, true, f_test);
   TText *usine_txt = TUMisc::OrphanUSINEAdOnPlots();

   switch (opt_num) {
      case 1: {
            TUMessages::Separator(f_test, "List data from experiment names");
            string exp_names = "Voyager,AMS,ACE";
            fprintf(f_test, "\n");
            if (!is_test) {
               do {
                  cout << ">>>>> Comma-separated experiment names (case insensitive) [Q=quit]: " << endl;;
                  cin >> exp_names;
                  if (exp_names == "Q") break;
                  crdata->PrintFromExpList(f_test, exp_names);
               } while (1);
            } else
               crdata->PrintFromExpList(f_test, exp_names);
         }
         break;
      case 2: {
            TUMessages::Separator(f_test, "List data from combo qties/exp_names/etype");
            string exp_data_etype = "B/C:HEAO:kEKN;H:PAMELA:kR";
            if (!is_test) {
               do {
                  cout << ">>>>> data/exp/etype (e.g., B/C:HEAO:kEKN;H:PAMELA:kR) [Q=quit]: " << endl;;
                  cin >> exp_data_etype;
                  if (exp_data_etype == "Q") break;
                  TUDataSet *subset = crdata->OrphanFormSubSet(exp_data_etype, f_test);
                  subset->Print(f_test, false);
                  delete subset;
               } while (1);
            } else {
               TUDataSet *subset = crdata->OrphanFormSubSet(exp_data_etype, f_test);
               subset->Print(f_test, false);
               delete subset;
            }
         }
         break;
      case 3:
      case 4: {
            // Data plots from exps and qties/exps
            TUMessages::Separator(f_test, "Plot TOA from single qty/exp_names/energy_range");
            string qty_exp_etype = "B/C:ALL:kEKN";
            Double_t e_power = 2.8;
            Double_t e_min = 1.e-10, e_max = 1.e40;
            Int_t z_demodul = -1;
            Int_t a_demodul = -1;

            if (!is_test) {
               cout << ">>>>> Enter a list of quantity/experiments/etype (e.g. B/C:HEAO,VOYAGER,CRIS,IMP:kEKN or B/C:ALL:kEKN) [Q=quit]: ";
               cin >> qty_exp_etype;
               if (qty_exp_etype == "Q") return;
               cout << ">>>>> Enter energy range in GeV/n sought (e.g., \"0.1,100.\", or \"ALL\" for all energies): ";
               string tmp;
               cin >> tmp;
               if (tmp == "ALL") {
                  e_min = 1.e-10;
                  e_max =  1.e40;
               } else {
                  vector<Double_t> l_xval;
                  TUMisc::String2List(tmp, ",", l_xval);
                  e_min = l_xval[0];
                  e_max = l_xval[1];
               }
            } else {
               z_demodul = 5;
               a_demodul = 10;
            }

            TUDataSet *subset = crdata->OrphanFormSubSet(qty_exp_etype, f_test, e_min, e_max);

            if (subset->GetNData() <= 0) {
               fprintf(f_test, "No data found!!!\n");
               return;
            }

            vector<string> tmp2;
            TUMisc::String2List(qty_exp_etype, ":", tmp2);
            string qty = tmp2[0];
            TUMisc::UpperCase(qty);
            Bool_t is_ratio = TUMisc::IsRatio(qty);
            gENUM_ETYPE e_type;
            TUEnum::Name2Enum(tmp2[2], e_type);

            gENUM_ERRTYPE err_type = kERRTOT;
            if (!is_test) {
               string tmp;
               cout << ">>>>> Enter gENUM_ERRTYPE to show (kERRSTAT, kERRSYST, or kERRTOT): ";
               cin >> tmp;
               TUEnum::Name2Enum(tmp, err_type);
            }

            if (!is_ratio && !is_test) {
               cout << ">>>>> Enter exponent \"a\" (flux => flux*Etype^a, default is a=0.): ";
               cin >> e_power;
            }
            subset->Print(f_test);

            Bool_t is_demodulate;
            string leg_header = "";
            if (opt_num == 3) {
               is_demodulate = false;
               leg_header = "TOA fluxes";
            } else {
               is_demodulate = true;
               leg_header = "FF-demodulated (IS) fluxes";
            }

            TCanvas *c0 = NULL;
            TLegend *leg = new TLegend(0.3, 0.2, 0.8, 0.9);
            leg->SetHeader(leg_header.c_str());
            TMultiGraph *mg = subset->OrphanGetMultiGraph(qty, "ALL", err_type, e_type, e_power, is_demodulate, leg, f_test, z_demodul, a_demodul);
            delete subset;
            subset = NULL;

            if (is_test)
               TUMisc::Print(f_test, mg, 2);
            else {
               c0 = new TCanvas("usine_inputs", "usine_inputs", 700, 500);
               //c0->SetGridx();
               //c0->SetGridy();
               c0->SetLogx(1);
               if (is_ratio && qty.find("BAR") == string::npos && qty.find("ELEC") == string::npos)
                  c0->SetLogy(0);
               else
                  c0->SetLogy(1);
               mg->Draw("AP");
               leg->SetFillColor(0);
               leg->SetFillStyle(0);
               leg->SetBorderSize(0);
               leg->SetTextColor(kBlack);
               leg->SetTextFont(132);
               leg->SetTextSize(0.03);
               leg->Draw("P");
               usine_txt->Draw();

               c0->Modified();
               c0->Update();
               my_app->Run(kTRUE);

            }
            // Free memory
            if (mg) delete mg;
            mg = NULL;
            if (c0) delete c0;
            c0 = NULL;
            break;
         }
      default:
         break;
   }

   // Free memory
   delete crdata;
   crdata = NULL;
   delete usine_txt;
}

//______________________________________________________________________________
void Option_IE(Int_t opt_num, Bool_t is_test, FILE *f_test)
{
   //--- USINE -ie option for display of energy losses and EC.
   //  opt_num           Option number
   //  is_test           Whether run in test mode (use default values)
   //  f_test            File in which to output result of test

   // If this point reached, ready to execute...
   switch (opt_num) {
      case 1: {
            // USINE: load parameter file
            string gr_sub_name[3] = {"UsineRun", "Models", "Propagation"};
            string name = init_pars->GetParEntry(init_pars->IndexPar(gr_sub_name)).GetVal();
            gENUM_PROPAG_MODEL prop_mod = kBASE;
            TUEnum::Name2Enum(name, prop_mod);
            TUModelBase *model = NULL;
            // Read model: contains ISM and Solar position
            if (prop_mod == kMODEL0DLEAKYBOX)
               model = new TUModel0DLeakyBox();
            else if (prop_mod == kMODEL1DKISOVC)
               model = new TUModel1DKisoVc();
            else if (prop_mod == kMODEL2DKISOVC)
               model = new TUModel2DKisoVc();
            else {
               string message = "Model " + name + " does not exist!";
               TUMessages::Error(f_test, "usine", "-ie1", message);
            }
            model->SetClass(init_pars, name, false, f_test);
            fprintf(f_test, "    => %s loaded\n", name.c_str());

            // Select position for calculation
            TUCoordTXYZ coords;
            if (!is_test)
               model->TUI_SetTXYZ(coords);

            // Loop no CRs for which to plot losses
            fprintf(f_test, "\n");
            TUMessages::Separator(f_test, "Ready for E loss calculation");
            fprintf(f_test, "   CRs available in list are:\n");
            model->TUCRList::PrintList(f_test, false, true);
            fprintf(f_test, "\n");
            string cr_list = "10B,12C,16O";
            if (!is_test) {
               // Read std input to Quit (Q) or display other nuclei.
               cout << "    Enter comma-separated list of CRs (Q to quit): ";
               cin >> cr_list;
            }
            vector<string> crs;
            TUMisc::String2List(cr_list, ",", crs);
            vector<TUCREntry> cr_entries;
            for (Int_t i = 0; i < (Int_t)crs.size(); ++i) {
               Int_t j_cr = model->Index(crs[i]);
               if (j_cr >= 0)
                  cr_entries.push_back(model->GetCREntry(j_cr));
            }
            TUInteractions::PlotELosses(cr_entries, model->GetISM()->GetMediumEntry(&coords), model, model->GetEFamily(kNUC), my_app, is_test, f_test);
            break;
         }
      case 2: {
            TUInteractions::PlotEC(my_app, is_test, f_test);
            break;
         }
   }
}

//______________________________________________________________________________
void Option_IX(Int_t opt_num, Bool_t is_test, FILE *f_test)
{
   //--- USINE -ix option for display of XS.
   //  opt_num           Option number
   //  is_test           Whether run in test mode (use default values)
   //  f_test            File in which to output result of test

   // If this point reached, ready to execute...

   // Read cross sections
   TUXSections xsec;
   xsec.SetClass(init_pars, false, f_test);
   fprintf(f_test, "\n");
   fprintf(f_test, "\n");

   // Print status
   TUMessages::Separator(f_test, "Available X-sections, CRs (and parents), and energies");
   xsec.PrintXSecFiles(f_test);
   xsec.PrintTargetList(f_test);
   xsec.PrintTertiaryList(f_test);
   xsec.GetAxesCrE()->PrintParents(f_test);
   xsec.PrintESummary(f_test);
   fprintf(f_test, "\n");

   // Loop to display cross sections...
   string list_proj = "12C,16O", list_frag = "10B,11B";
   switch (opt_num) {
      case 1:
         do {
            if (!is_test) {
               cout << "  Enter comma-separated list of CRs (Q to quit): ";
               cin >> list_frag;
               cout << endl;
               if (list_frag == "Q")
                  break;
            }
            // Plot
            xsec.Plots("", "ALL", list_frag, my_app, is_test, f_test);
            if (is_test)
               break;
         } while (true);
      case 2:
         do {
            if (!is_test) {
               cout << "  Enter comma-separated list of projectiles (Q to quit): ";
               cin >> list_proj;
               if (list_proj == "Q")
                  break;
               cout << "  Enter comma-separated list of fragments (Q to quit): ";
               cin >> list_frag;
               if (list_frag == "Q")
                  break;
            }
            // Plot
            xsec.Plots(list_proj, "ALL", list_frag, my_app, is_test, f_test);
            if (is_test)
               break;
         } while (true);
   }
   return;
}

//______________________________________________________________________________
void Option_L(Int_t argc, char *argv[])
{
   //--- USINE -l option for local flux calculations.
   //  argc              Number of user parameters
   //  argv              Value of user parameters

   if (argc < 9) {
      printf("USAGE:   %s -l  init_file  output_dir  qties_etypes_phiff  e_power  is_verbose  is_saveinfiles  is_showplots\n"
             "EXAMPLE: %s -l  inputs/init.TEST.par  $USINE/output  \"B/C,B,C,O:KEKN:0.,0.5,1.;B/C:kR:0.79\"  2.8  1  1  1\n"
             "\n"
             "   [init_file]       USINE initialisation file\n"
             "   [output_dir]      Directory for outputs (plots, files, etc.)\n"
             "   [qties_etypes]    Quantities(\"B/C,<LNA>,ALLSPECTRUM\"):E-type(kR, kEKN, kEK, kETOT):phi_FF(0.,0.5) to show\n"
             "   [e_power]         Fluxes multiplied by E-type^(e_power); supersedes init. file value\n"
             "   [is_verbose]      More information on run (printed in logfile)\n"
             "   [is_saveinfiles]  Save outputs and macros for plots (if 1) or do not not (if 0)\n"
             "   [is_showplots]    Show plots (if 1) or batch mode (if 0)\n",
             argv[0], argv[0]);
      printf("\n");
      return;
   }
   indent = TUMessages::Indent(true);

   // LoadUserOptions(init_file, output_dir, is_verbose, is_saveinfiles, is_showplots)
   LoadUserOptions(argv[2], TUMisc::CreateDir(argv[3], "usine", "Option_L"), atoi(argv[6]), atoi(argv[7]), atoi(argv[8]));
   // Calculate and run local spectra
   printf("%s    - Calculate\n", indent.c_str());
   TUCoordTXYZ *coord_txyz = NULL;
   TURunOutputs results;
   run->FillSpectra(results, argv[4], atof(argv[5]));
   run->PlotSpectra(my_app, results, false/*is_sep_isot*/, false/*is_print*/, stdout/*fprint*/);

   TUMessages::Indent(false);
   return;
}

//______________________________________________________________________________
void Option_M(Int_t argc, char *argv[])
{
   //--- USINE -m option related to minimisation and free parameters.
   //  argc              Number of user parameters
   //  argv              Value of user parameters


   string opt = argv[1];
   indent = TUMessages::Indent(true);
   TUMessages::Indent(false);

   if (opt == "-m1") {
      // List of fit-able parameters
      if (argc < 3) {
         printf("USAGE:   %s -m1  init_file\n"
                "EXAMPLE: %s -m1  inputs/init.TEST.par\n"
                "\n"
                "   [init_file]       USINE initialisation file\n",
                argv[0], argv[0]);
         printf("\n");
         return;
      }
      Bool_t is_verbose = false;
      // LoadUserOptions(init_file, output_dir, is_verbose, is_saveinfiles, is_showplots)
      LoadUserOptions(argv[2], ".", is_verbose, 0, 0);
      run->UpdateFitParsAndFitData(init_pars, is_verbose, true);

      // Get model free-able parameters
      TUFreeParList *par_list = run->GetPropagModel(0);
      par_list->ResetStatusParsAndParList();
      // Add CR free-able parameters
      par_list->AddPars(run->GetPropagModel(0)->GetAxesCrE()->TUCRList::GetFreePars(), true);
      // Add XS free-able parameters
      par_list->AddPars(run->GetPropagModel(0)->TUXSections::GetFreePars(), true);
      // Add solmod free-able parameters (for corresponding data)
      for (Int_t i = 0; i < run->GetSolModModel(0)->ParsModExps_GetN(); ++i) {
         TUFreeParList *tmp = run->GetSolModModel(0)->ParsModExps_Get(i);
         for (Int_t j = 0; j < tmp->GetNPars(); ++j)
            par_list->AddPar(tmp->GetParEntry(j), true);
         tmp = NULL;
      }
      // Add data error as nuisance
      for (Int_t i = 0; i < run->GetFitData()->ParsData_GetN(); ++i) {
         TUFreeParList *tmp = run->GetFitData()->ParsData_Get(i);
         for (Int_t j = 0; j < tmp->GetNPars(); ++j)
            par_list->AddPar(tmp->GetParEntry(j), true);
         tmp = NULL;
      }

      par_list->ResetStatusParsAndParList();

      printf("\n");
      printf("          ================================================================\n");
      printf("                              LIST OF FITABLE PARAMETERS                  \n");
      printf("           (add as \'UsineRun @ FitFreePars @ ...' in %s\n", argv[2]);
      //if (run->GetSolModModel(0)->GetModel() == kSOLMOD0DFF && run->GetSolModModel(0)->ParsModExps_GetN())
      //   printf("              [N.B.: phi values (per sub-exp) below are read in CR data file]\n");
      printf("          ================================================================\n");
      par_list->PrintPars();
      printf("          ================================================================\n");
      par_list = NULL;
   } else if (opt == "-m2") {
      // Minimisation on TOA fluxes
      if (argc < 7) {
         printf("USAGE:   %s -m2  init_file  output_dir  is_verbose  is_saveinfiles is_showplots\n"
                "EXAMPLE: %s -m2  inputs/init.TEST.par  $USINE/output   1    1    0\n"
                "\n"
                "   [init_file]       USINE initialisation file\n"
                "   [output_dir]      Directory for outputs (plots, files, etc.)\n"
                "   [is_verbose]      To have more informations on the run (printed in logfile)\n"
                "   [is_saveinfiles]  Save outputs of calculation and macros for plots (1) or not (0)\n"
                "   [is_showplots]    Show plots (1) or batch mode (0)\n",
                argv[0], argv[0]);
         printf("\n");
         return;
      }

      // LoadUserOptions(init_file, output_dir, is_verbose, is_saveinfiles, is_showplots)
      Bool_t is_verbose = atoi(argv[4]);
      Bool_t is_saveinfiles = atoi(argv[5]);
      string output_dir = argv[3];
      LoadUserOptions(argv[2], TUMisc::CreateDir(output_dir, "usine", "-m2"), is_verbose, is_saveinfiles, atoi(argv[6]));

      printf("%s    - Calculate\n", indent.c_str());
      run->UpdateFitParsAndFitData(init_pars, is_verbose, true);
      run->MinimiseTOAFluxes(init_pars, is_verbose, true);

      // Plot best-chi2 with fit_data used for minimisation
      TUDataSet *fit_data = run->GetFitData();
      run->GetPropagModel(0)->TUDataSet::Copy(*fit_data);
      // Do not uncomment following line, otherwise delete Sol.Mod nuisance parameters!
      //run->GetSolModModel(0)->ParsModExps_Delete();

      // If XS nuisance parameters, plot XS (new and reference)
      TMultiGraph *mg_xs = NULL;
      TLegend *leg_xs = NULL;
      TCanvas *c_xs = NULL;
      TText *usine_txt = TUMisc::OrphanUSINEAdOnPlots();

      if (run->GetPropagModel(0)->TUXSections::GetNFreePars() > 0) {
         TUXSections *xs = run->GetPropagModel(0);
         // Set Plot style
         TUMisc::RootSetStylePlots();
         vector<TH2D *> hs_xs;

         // Loop on reactions (in free parameters)
         Bool_t is_xs = false;
         for (Int_t r = 0; r < xs->ParsXS_GetNReac(); ++ r) {
            // If global parameter, skip (only display reactions)
            if (xs->ParsXS_GetReac(r).is_all || xs->ParsXS_GetReac(r).is_all_lcinel
                  || xs->ParsXS_GetReac(r).is_all_lcprod || xs->ParsXS_GetReac(r).is_all_frag ||
                  xs->ParsXS_GetReac(r).is_all_proj || xs->ParsXS_GetReac(r).is_all_targ) {
               printf("     ... Modified XS for %s not displayed (too many reactions)\n", xs->ParsXS_GetReac(r).name.c_str());
               continue;
            } else if (!is_xs) {
               mg_xs = new TMultiGraph();
               leg_xs = new TLegend(0.13, 0.4, 0.5, 0.93, NULL, "brNDC");
               is_xs = true;
            }

            // N.B.: subtlety in ExtractXSGraphs function (for inelastic, uses j_frag)
            Int_t j_frag = xs->ParsXS_GetReac(r).i_proj;
            Int_t t_targ = xs->ParsXS_GetReac(r).i_targ;
            Int_t j_projlop = -1;
            if (xs->ParsXS_GetReac(r).i_frag >= 0) {
               j_frag = xs->ParsXS_GetReac(r).i_frag;
               j_projlop = xs->ParsXS_GetReacIndexProjLop(r);
            }
            // Reference XS
            xs->ParsXS_GetRefXS()->ExtractXSGraphs(j_projlop, t_targ, j_frag, mg_xs, leg_xs, hs_xs, 1);
            // New XS (from modifications of XS parameters)
            xs->ExtractXSGraphs(j_projlop, t_targ, j_frag, mg_xs, leg_xs, hs_xs, 2);
         }

         xs = NULL;
         // We do not display differential XS
         for (Int_t i = 0; i < (Int_t)hs_xs.size(); ++i) {
            if (hs_xs[i]) delete hs_xs[i];
            hs_xs[i] = NULL;
         }
         hs_xs.clear();

         // Display total inelastic and straight-ahead prod)
         if (mg_xs && mg_xs->GetListOfGraphs() && mg_xs->GetListOfGraphs()->GetSize() != 0) {
            c_xs = new TCanvas("xsec_ref_vs_nuisance", "xsec_ref_vs_nuisance", 1020, 0, 500, 350);
            c_xs->SetLogx(1);
            c_xs->SetLogy(0);
            mg_xs->Draw("AL");
            mg_xs->GetXaxis()->CenterTitle();
            mg_xs->GetYaxis()->CenterTitle();
            usine_txt->Draw();
            leg_xs->Draw();
            c_xs->Modified();
            c_xs->Update();
            if (is_saveinfiles)
               TUMisc::RootSave(c_xs, output_dir, c_xs->GetName(), "CP");
         }
      }
      TURunOutputs results;
      run->FillSpectra(results, fit_data->GetQtiesEtypesphiFF(), run->GetDiplayedFluxEPower());
      run->PlotSpectra(my_app, results, false/*is_sep_isot*/, false/*is_print*/, stdout/*fprint*/);

      // Free memory X-sec prod
      if (c_xs) delete c_xs;
      c_xs = NULL;
      if (mg_xs)     delete mg_xs;
      mg_xs = NULL;
      delete usine_txt;
      usine_txt = NULL;
   }
   return;
}

//______________________________________________________________________________
void Option_T(Int_t argc, char *argv[])
{
   //--- USINE -t option for test functions.
   //  argc              Number of user parameters
   //  argv              Value of user parameters

   string flag = argv[1];

   if (flag == "-t" || argc <= 1 || argc > 3 || (flag.size() > 4 && flag != "-tALL" && flag != "-tUSINE" && flag != "-tGENREF")) {
      printf("USAGE: %s flags  init_file\n"
             "   [init_file]      [Optional] USINE initialisation file to use (in not provided, use inputs/init.TEST.par)\n"
             "   [flags]\n"
             "             -t1    [TEST classes related to Atomic properties]\n"
             "                    -t1a   TUAtomElements\n"
             "                    -t1b   TUAtomProperties   (inherits from TUAtomElements)\n"
             "             -t2    [TEST classes related to any kind of \"Axis\" and handlers]\n"
             "                    -t2a   TUAxis                    \n"
             "                    -t2b   TUCREntry                 \n"
             "                    -t2c   TUCRList           (inherits from TUAtomProperties)\n"
             "                    -t2d   TUAxesCrE                 \n"
             "                    -t2e   TUAxesTXYZ                \n"
             "                    -t2f   TUCoordE                  \n"
             "                    -t2g   TUCoordTXYZ              \n"
             "                    -t2h   TUSplineFitFc            \n"
             "                    -t2i   TUValsTXYZEFormula (inherits from TUValsTXYZEVirtual)\n"
             "                    -t2j   TUValsTXYZEGrid    (inherits from TUValsTXYZEVirtual)\n"
             "                    -t2k   TUValsTXYZESpline  (inherits from TUValsTXYZEVirtual)\n"
             "                    -t2l   TUValsTXYZECr      (inherits from TUFreeParList, extends TUValsTXYZEVirtual)\n"
             "                          TUValsTXYZEVirtual [abstract class]\n"
             "                    -t2m   TUCRFluxes (IS and modulated fluxes)\n"
             "             -t3    [TEST classes related to initialisation and free parameters]\n"
             "                    -t3a   TUFreeParEntry            \n"
             "                    -t3b   TUFreeParList             \n"
             "                    -t3c   TUInitParEntry            \n"
             "                    -t3d   TUInitParList             \n"
             "             -t4    [TEST classes related to input ingredients]\n"
             "                    -t4a   TUDataEntry               \n"
             "                    -t4b   TUDataSet                 \n"
             "                    -t4c   TUDatime                  \n"
             "                    -t4d   TUNormEntry               \n"
             "                    -t4e   TUNormList                \n"
             "                    -t4f   TUXSections               \n"
             "             -t5    [TEST classes related to encapsulated functions, TUMessage excepted]\n"
             "                    -t5a   TUMath                    \n"
             "                    -t5b   TUMisc                    \n"
             "                    -t5c   TUNumMethods              \n"
             "                    -t5d   TUPhysics                 \n"
             "                    -t5e   TUInteractions        \n"
             "             -t6    [TEST classes related to solar modulation]\n"
             "                    -t6a   TUSolMod0DFF (inherits from TUSolModVirtual)\n"
             "                    -t6b   TUSolMod1DSpher (inherits from TUSolModVirtual)  \n"
             "                     t     TUSolModVirtual [abstract class]\n"
             "             -t7    [TEST classes related to astro and DM sources]\n"
             "                    -t7a   TUSrcTemplates            \n"
             "                    -t7b   TUSrcPointLike            \n"
             "                    -t7c   TUsrcSteadyState          \n"
             "                    -t7d   TUsrcMultiCRs             \n"
             "                    -t7e   TUBesselJ0                \n"
             "                    -t7f   TUBesselQiSrc             \n"
             "                          TUSrcVirtual [abstract class]\n"
             "             -t8    [TEST classes related to propagation]\n"
             "                    -t8a   TUMediumEntry             \n"
             "                    -t8b   TUMediumTXYZ       (inherits from TUMediumTXYZ)\n"
             "                    -t8c   TUPropagSwitches          \n"
             "                    -t8d   TUTransport               \n"
             "                    -t8e   TUModelBase               \n"
             "                    -t8f   TUModel0DLeakyBox         \n"
             "                    -t8g   TUModel1DKisoVc           \n"
             "                    -t8h   TUModel2DKisoVc           \n"
             "             -t9    [Integration tests]\n"
             "                    -t9a   Propagation (TUModel0DLeakyBox, TUModel1DKisoVc, and TUModel2DKisoVc)\n"
             "                    -t9b   Runs -e (Extra) options\n"
             "                    -t9c   Runs -i (Input) options\n"
             "                    -t9d   Minimisation with TUModel1DKisoVc\n"
             "             -tALL  [TEST all USINE classes, propagation, minimisation, and -e/-i options]\n"
             "\n"
             "             -tUSINE    [RUN -tALL against reference test files (in usine.tests/)]\n"
             "             -tGENREF   [RUN -tALL and save test files (in usine.tests/)]\n",
             argv[0]);
      return;
   }

   //--- Parameters from command line
   string f_init = "inputs/init.TEST.par";
   if (argc == 3) f_init = argv[2];
   string output_dir = "";
   string extension = ".ref";
   if (flag == "-tUSINE") {
      output_dir = "usine.tests";
      extension = ".fail";
   } else if (flag == "-tGENREF") {
      output_dir = "usine.tests";
      TUMisc::CreateDir(output_dir, "usine_test", "main");
   }

   //---USINE: load parameter file
   init_pars = new TUInitParList();
   init_pars->SetClass(TUMisc::GetPath(f_init), false, stdout);

   if (flag == "-tGENREF")
      printf("   ... Run all tests and save outputs in usine.tests/\n");
   if (flag == "-tUSINE") {
      printf("   Run all test and check outputs against usine.tests/*.ref:\n");
      printf("   - classes and namespaces (\'unit\' tests)\n");
   }

   //----------------
   // Related to atomic properties
   //----------------
   if (flag == "-t1" || flag == "-t1a" || flag == "-tALL" || flag == "-tUSINE" || flag == "-tGENREF") {
      TUAtomElements atomelem;
      string test = "class_TUAtomElements";
      FILE *fp = OutputFile(output_dir, (test + extension));
      atomelem.TEST(fp);
      if (fp != stdout) fclose(fp);
      if (flag == "-tUSINE") TestResult(output_dir, test);
   }
   if (flag == "-t1" || flag == "-t1b" || flag == "-tALL" || flag == "-tUSINE" || flag == "-tGENREF") {
      TUAtomProperties atomprop;
      string test = "class_TUAtomProperties";
      FILE *fp = OutputFile(output_dir, (test + extension));
      atomprop.TEST(init_pars, fp);
      if (fp != stdout) fclose(fp);
      if (flag == "-tUSINE") TestResult(output_dir, test);
   }


   //----------------
   // Related to axes (CRs, energy, space...) and containers (TTXYZ, TXYZE, and TXYZECr)
   //----------------
   if (flag == "-t2" || flag == "-t2a" || flag == "-tALL" || flag == "-tUSINE" || flag == "-tGENREF") {
      TUAxis axis;
      string test = "class_TUAxis";
      FILE *fp = OutputFile(output_dir, (test + extension));
      axis.TEST(fp);
      if (fp != stdout) fclose(fp);
      if (flag == "-tUSINE") TestResult(output_dir, test);
   }
   if (flag == "-t2" || flag == "-t2b" || flag == "-tALL" || flag == "-tUSINE" || flag == "-tGENREF") {
      TUCREntry cr_entry;
      string test = "class_TUCREntry";
      FILE *fp = OutputFile(output_dir, (test + extension));
      cr_entry.TEST(fp);
      if (fp != stdout) fclose(fp);
      if (flag == "-tUSINE") TestResult(output_dir, test);
   }
   if (flag == "-t2" || flag == "-t2c" || flag == "-tALL" || flag == "-tUSINE" || flag == "-tGENREF") {
      TUCRList cr_list;
      string test = "class_TUCRList";
      FILE *fp = OutputFile(output_dir, (test + extension));
      cr_list.TEST(init_pars, fp);
      if (fp != stdout) fclose(fp);
      if (flag == "-tUSINE") TestResult(output_dir, test);
   }
   if (flag == "-t2" || flag == "-t2d" || flag == "-tALL" || flag == "-tUSINE" || flag == "-tGENREF") {
      TUAxesCrE axes_cre;
      string test = "class_TUAxesCrE";
      FILE *fp = OutputFile(output_dir, (test + extension));
      axes_cre.TEST(init_pars, fp);
      if (fp != stdout) fclose(fp);
      if (flag == "-tUSINE") TestResult(output_dir, test);
   }
   if (flag == "-t2" || flag == "-t2e" || flag == "-tALL" || flag == "-tUSINE" || flag == "-tGENREF") {
      TUAxesTXYZ axes_txyz;
      string test = "class_TUAxesTXYZ";
      FILE *fp = OutputFile(output_dir, (test + extension));
      axes_txyz.TEST(init_pars, fp);
      if (fp != stdout) fclose(fp);
      if (flag == "-tUSINE") TestResult(output_dir, test);
   }
   if (flag == "-t2" || flag == "-t2f" || flag == "-tALL" || flag == "-tUSINE" || flag == "-tGENREF") {
      TUCoordE coord_e;
      string test = "class_TUCoordE";
      FILE *fp = OutputFile(output_dir, (test + extension));
      coord_e.TEST(fp);
      if (fp != stdout) fclose(fp);
      if (flag == "-tUSINE") TestResult(output_dir, test);
   }
   if (flag == "-t2" || flag == "-t2g" || flag == "-tALL" || flag == "-tUSINE" || flag == "-tGENREF") {
      TUCoordTXYZ coord_txyz;
      string test = "class_TUCoordTXYZ";
      FILE *fp = OutputFile(output_dir, (test + extension));
      coord_txyz.TEST(fp);
      if (fp != stdout) fclose(fp);
      if (flag == "-tUSINE") TestResult(output_dir, test);
   }
   if (flag == "-t2" || flag == "-t2h" || flag == "-tALL" || flag == "-tUSINE" || flag == "-tGENREF") {
      //TUSplineFitFcn fcn_spline;
      //FILE *fp = OutputFile(output_dir, ("class_TUSplineFitFcn"+extension));
      //fcn_spline.TEST(fp);
      //if (fp !=stdout) fclose(fp);
   }
   if (flag == "-t2" || flag == "-t2i" || flag == "-tALL" || flag == "-tUSINE" || flag == "-tGENREF") {
      TUValsTXYZEFormula formula;
      string test = "class_TUValsTXYZEFormula";
      FILE *fp = OutputFile(output_dir, (test + extension));
      formula.TEST(fp);
      if (fp != stdout) fclose(fp);
      if (flag == "-tUSINE") TestResult(output_dir, test);
   }
   if (flag == "-t2" || flag == "-t2j" || flag == "-tALL" || flag == "-tUSINE" || flag == "-tGENREF") {
      TUValsTXYZEGrid grid;
      string test = "class_TUValsTXYZEGrid";
      FILE *fp = OutputFile(output_dir, (test + extension));
      grid.TEST(init_pars, fp);
      if (fp != stdout) fclose(fp);
      if (flag == "-tUSINE") TestResult(output_dir, test);
   }
   if (flag == "-t2" || flag == "-t2k" || flag == "-tALL" || flag == "-tUSINE" || flag == "-tGENREF") {
      //TUValsTXYZESpline spline;
      //string test = "class_TUValsTXYZESpline";
      //FILE *fp = OutputFile(output_dir, (test + extension));
      //spline.TEST(fp);
      //if (fp != stdout) fclose(fp);
      //if (flag == "-tUSINE") TestResult(output_dir, test);
   }
   if (flag == "-t2" || flag == "-t2l" || flag == "-tALL" || flag == "-tUSINE" || flag == "-tGENREF") {
      TUValsTXYZECr fluxes;
      string test = "class_TUValsTXYZECr";
      FILE *fp = OutputFile(output_dir, (test + extension));
      fluxes.TEST(init_pars, fp);
      if (fp != stdout) fclose(fp);
      if (flag == "-tUSINE") TestResult(output_dir, test);
   }
   if (flag == "-t2" || flag == "-t2m" || flag == "-tALL" || flag == "-tUSINE" || flag == "-tGENREF") {
      //TUCRFluxes fluxes;
      //string test = "class_TUCRFluxes";
      //FILE *fp = OutputFile(output_dir, (test + extension));
      //fluxes.TEST(f_init, fp);
      //if (fp != stdout) fclose(fp);
      //if (flag == "-tUSINE") TestResult(output_dir, test);
   }


   //----------------
   // Related to parameters
   //----------------
   if (flag == "-t3" || flag == "-t3a" || flag == "-tALL" || flag == "-tUSINE" || flag == "-tGENREF") {
      TUFreeParEntry freepar_entry;
      string test = "class_TUFreeParEntry";
      FILE *fp = OutputFile(output_dir, (test + extension));
      freepar_entry.TEST(fp);
      if (fp != stdout) fclose(fp);
      if (flag == "-tUSINE") TestResult(output_dir, test);
   }
   if (flag == "-t3" || flag == "-t3b" || flag == "-tALL" || flag == "-tUSINE" || flag == "-tGENREF") {
      TUFreeParList freepar_list;
      string test = "class_TUFreeParList";
      FILE *fp = OutputFile(output_dir, (test + extension));
      freepar_list.TEST(init_pars, fp);
      if (fp != stdout) fclose(fp);
      if (flag == "-tUSINE") TestResult(output_dir, test);
   }
   if (flag == "-t3" || flag == "-t3c" || flag == "-tALL" || flag == "-tUSINE" || flag == "-tGENREF") {
      TUInitParEntry initpar_entry;
      string test = "class_TUInitParEntry";
      FILE *fp = OutputFile(output_dir, (test + extension));
      initpar_entry.TEST(fp);
      if (fp != stdout) fclose(fp);
      if (flag == "-tUSINE") TestResult(output_dir, test);
   }
   if (flag == "-t3" || flag == "-t3d" || flag == "-tALL" || flag == "-tUSINE" || flag == "-tGENREF") {
      TUInitParList initpar_list;
      string test = "class_TUInitParList";
      FILE *fp = OutputFile(output_dir, (test + extension));
      initpar_list.TEST(f_init, fp);
      if (fp != stdout) fclose(fp);
      if (flag == "-tUSINE") TestResult(output_dir, test);
   }


   //----------------
   // Related to input ingredients
   //----------------
   if (flag == "-t4" || flag == "-t4a" || flag == "-tALL" || flag == "-tUSINE" || flag == "-tGENREF") {
      TUDataEntry data_entry;
      string test = "class_TUDataEntry";
      FILE *fp = OutputFile(output_dir, (test + extension));
      data_entry.TEST(fp);
      if (fp != stdout) fclose(fp);
      if (flag == "-tUSINE") TestResult(output_dir, test);
   }
   if (flag == "-t4" || flag == "-t4b" || flag == "-tALL" || flag == "-tUSINE" || flag == "-tGENREF") {
      TUDataSet data_set;
      string test = "class_TUDataSet";
      FILE *fp = OutputFile(output_dir, (test + extension));
      data_set.TEST(init_pars, fp);
      if (fp != stdout) fclose(fp);
      if (flag == "-tUSINE") TestResult(output_dir, test);
   }
   if (flag == "-t4" || flag == "-t4c" || flag == "-tALL" || flag == "-tUSINE" || flag == "-tGENREF") {
      TUDatime datime;
      string test = "class_TUDatime";
      FILE *fp = OutputFile(output_dir, (test + extension));
      datime.TEST(fp);
      if (fp != stdout) fclose(fp);
      if (flag == "-tUSINE") TestResult(output_dir, test);
   }
   if (flag == "-t4" || flag == "-t4d" || flag == "-tALL" || flag == "-tUSINE" || flag == "-tGENREF") {
      TUNormEntry norm_entry;
      string test = "class_TUNormEntry";
      FILE *fp = OutputFile(output_dir, (test + extension));
      norm_entry.TEST(fp);
      if (fp != stdout) fclose(fp);
      if (flag == "-tUSINE") TestResult(output_dir, test);
   }
   if (flag == "-t4" || flag == "-t4e" || flag == "-tALL" || flag == "-tUSINE" || flag == "-tGENREF") {
      TUNormList norm_list;
      string test = "class_TUNormList";
      FILE *fp = OutputFile(output_dir, (test + extension));
      norm_list.TEST(init_pars, fp);
      if (fp != stdout) fclose(fp);
      if (flag == "-tUSINE") TestResult(output_dir, test);
   }
   if (flag == "-t4" || flag == "-t4f" || flag == "-tALL" || flag == "-tUSINE" || flag == "-tGENREF") {
      TUXSections xsec;
      string test = "class_TUXSections";
      FILE *fp = OutputFile(output_dir, (test + extension));
      xsec.TEST(init_pars, fp);
      if (fp != stdout) fclose(fp);
      if (flag == "-tUSINE") TestResult(output_dir, test);
   }



   //----------------
   // Related to encapsulated function (math/physics/queries)
   //----------------
   if (flag == "-t5" || flag == "-t5a" || flag == "-tALL" || flag == "-tUSINE" || flag == "-tGENREF") {
      string test = "namespace_TUMath";
      FILE *fp = OutputFile(output_dir, (test + extension));
      TUMath::TEST(fp);
      if (fp != stdout) fclose(fp);
      if (flag == "-tUSINE") TestResult(output_dir, test);
   }
   if (flag == "-t5" || flag == "-t5b" || flag == "-tALL" || flag == "-tUSINE" || flag == "-tGENREF") {
      string test = "namespace_TUMisc";
      FILE *fp = OutputFile(output_dir, (test + extension));
      TUMisc::TEST(fp);
      if (fp != stdout) fclose(fp);
      if (flag == "-tUSINE") TestResult(output_dir, test);
   }
   if (flag == "-t5" || flag == "-t5c" || flag == "-tALL" || flag == "-tUSINE" || flag == "-tGENREF") {
      string test = "namespace_TUNumMethods";
      FILE *fp = OutputFile(output_dir, (test + extension));
      TUNumMethods::TEST(fp);
      if (fp != stdout) fclose(fp);
      if (flag == "-tUSINE") TestResult(output_dir, test);
   }
   if (flag == "-t5" || flag == "-t5d" || flag == "-tALL" || flag == "-tUSINE" || flag == "-tGENREF") {
      string test = "namespace_TUPhysics";
      FILE *fp = OutputFile(output_dir, (test + extension));
      TUPhysics::TEST(fp);
      if (fp != stdout) fclose(fp);
      if (flag == "-tUSINE") TestResult(output_dir, test);
   }
   if (flag == "-t5" || flag == "-t5e" || flag == "-tALL" || flag == "-tUSINE" || flag == "-tGENREF") {
      string test = "namespace_TUInteractions";
      FILE *fp = OutputFile(output_dir, (test + extension));
      TUInteractions::TEST(fp);
      if (fp != stdout) fclose(fp);
      if (flag == "-tUSINE") TestResult(output_dir, test);
   }



   //----------------
   // Related to Solar modulation
   //----------------
   if (flag == "-t6" || flag == "-t6a" || flag == "-tALL" || flag == "-tUSINE" || flag == "-tGENREF") {
      TUSolMod0DFF ff;
      string test = "class_TUSolMod0DFF";
      FILE *fp = OutputFile(output_dir, (test + extension));
      ff.TEST(init_pars, fp);
      if (fp != stdout) fclose(fp);
      if (flag == "-tUSINE") TestResult(output_dir, test);
   }
   if (flag == "-t6" || flag == "-t6b" || flag == "-tALL" || flag == "-tUSINE" || flag == "-tGENREF") {
      //TUSolMod1DSpher spher_sym;
      //string test = "class_TUSolMod1DSpher";
      //FILE *fp = OutputFile(output_dir, (test + extension));
      //spher_sym.TEST(init_pars, fp);
      //if (fp != stdout) fclose(fp);
      //if (flag == "-tUSINE") TestResult(output_dir, test);
   }



   //----------------
   // Related to Sources (spatial and energy dist.)
   //----------------
   if (flag == "-t7" || flag == "-t7a" || flag == "-tALL" || flag == "-tUSINE" || flag == "-tGENREF") {
      TUSrcTemplates src_templ;
      string test = "class_TUSrcTemplates";
      FILE *fp = OutputFile(output_dir, (test + extension));
      src_templ.TEST(init_pars, fp);
      if (fp != stdout) fclose(fp);
      if (flag == "-tUSINE") TestResult(output_dir, test);
   }
   if (flag == "-t7" || flag == "-t7b" || flag == "-tALL" || flag == "-tUSINE" || flag == "-tGENREF") {
      TUSrcPointLike src_pl;
      string test = "class_TUSrcPointLike";
      FILE *fp = OutputFile(output_dir, (test + extension));
      src_pl.TEST(init_pars, fp);
      if (fp != stdout) fclose(fp);
      if (flag == "-tUSINE") TestResult(output_dir, test);
   }
   if (flag == "-t7" || flag == "-t7c" || flag == "-tALL" || flag == "-tUSINE" || flag == "-tGENREF") {
      TUSrcSteadyState src_ss;
      string test = "class_TUSrcSteadyState";
      FILE *fp = OutputFile(output_dir, (test + extension));
      src_ss.TEST(init_pars, fp);
      if (fp != stdout) fclose(fp);
      if (flag == "-tUSINE") TestResult(output_dir, test);
   }
   if (flag == "-t7" || flag == "-t7d" || flag == "-tALL" || flag == "-tUSINE" || flag == "-tGENREF") {
      TUSrcMultiCRs *src_crlist = new TUSrcMultiCRs();
      string test = "class_TUSrcMultiCRs";
      FILE *fp = OutputFile(output_dir, (test + extension));
      src_crlist->TEST(init_pars, fp);
      if (fp != stdout) fclose(fp);
      if (flag == "-tUSINE") TestResult(output_dir, test);
      delete src_crlist;
   }
   if (flag == "-t7" || flag == "-t7e" || flag == "-tALL" || flag == "-tUSINE" || flag == "-tGENREF") {
      TUBesselJ0 bessel;
      string test = "class_TUBesselJ0";
      FILE *fp = OutputFile(output_dir, (test + extension));
      bessel.TEST(fp);
      if (fp != stdout) fclose(fp);
      if (flag == "-tUSINE") TestResult(output_dir, test);
   }
   if (flag == "-t7" || flag == "-t7f" || flag == "-tALL" || flag == "-tUSINE" || flag == "-tGENREF") {
      TUBesselQiSrc qi_src;
      string test = "class_TUBesselQiSrc";
      FILE *fp = OutputFile(output_dir, (test + extension));
      qi_src.TEST(init_pars, fp);
      if (fp != stdout) fclose(fp);
      if (flag == "-tUSINE") TestResult(output_dir, test);
   }



   //----------------
   // Related to propagation
   //----------------
   if (flag == "-t8" || flag == "-t8a" || flag == "-tALL" || flag == "-tUSINE" || flag == "-tGENREF") {
      TUMediumEntry medium_entry;
      string test = "class_TUMediumEntry";
      FILE *fp = OutputFile(output_dir, (test + extension));
      medium_entry.TEST(fp);
      if (fp != stdout) fclose(fp);
      if (flag == "-tUSINE") TestResult(output_dir, test);
   }
   if (flag == "-t8" || flag == "-t8b" || flag == "-tALL" || flag == "-tUSINE" || flag == "-tGENREF") {
      TUMediumTXYZ medium;
      string test = "class_TUMediumTXYZ";
      FILE *fp = OutputFile(output_dir, (test + extension));
      medium.TEST(init_pars, fp);
      if (fp != stdout) fclose(fp);
      if (flag == "-tUSINE") TestResult(output_dir, test);
   }
   if (flag == "-t8" || flag == "-t8c" || flag == "-tALL" || flag == "-tUSINE" || flag == "-tGENREF") {
      TUPropagSwitches mode;
      string test = "class_TUPropagSwitches";
      FILE *fp = OutputFile(output_dir, (test + extension));
      mode.TEST(init_pars, fp);
      if (fp != stdout) fclose(fp);
      if (flag == "-tUSINE") TestResult(output_dir, test);
   }
   if (flag == "-t8" || flag == "-t8d" || flag == "-tALL" || flag == "-tUSINE" || flag == "-tGENREF") {
      TUTransport transport;
      string test = "class_TUTransport";
      FILE *fp = OutputFile(output_dir, (test + extension));
      transport.TEST(init_pars, fp);
      if (fp != stdout) fclose(fp);
      if (flag == "-tUSINE") TestResult(output_dir, test);
   }
   if (flag == "-t8" || flag == "-t8e" || flag == "-tALL" || flag == "-tUSINE" || flag == "-tGENREF") {
      TUModelBase model_base;
      string test = "class_TUModelBase";
      FILE *fp = OutputFile(output_dir, (test + extension));
      model_base.TEST(init_pars, fp);
      if (fp != stdout) fclose(fp);
      if (flag == "-tUSINE") TestResult(output_dir, test);
   }
   if (flag == "-t8" || flag == "-t8f" || flag == "-tALL" || flag == "-tUSINE" || flag == "-tGENREF") {
      TUModel0DLeakyBox leaky_box;
      string test = "class_TUModel0DLeakyBox";
      FILE *fp = OutputFile(output_dir, (test + extension));
      leaky_box.TEST(init_pars, fp);
      if (fp != stdout) fclose(fp);
      if (flag == "-tUSINE") TestResult(output_dir, test);
   }
   if (flag == "-t8" || flag == "-t8g" || flag == "-tALL" || flag == "-tUSINE" || flag == "-tGENREF") {
      TUModel1DKisoVc plan;
      string test = "class_TUModel1DKisoVc";
      FILE *fp = OutputFile(output_dir, (test + extension));
      plan.TEST(init_pars, fp);
      if (fp != stdout) fclose(fp);
      if (flag == "-tUSINE") TestResult(output_dir, test);
   }
   if (flag == "-t8" || flag == "-t8h" || flag == "-tALL" || flag == "-tUSINE" || flag == "-tGENREF") {
      TUModel2DKisoVc cyl;
      string test = "class_TUModel2DKisoVc";
      FILE *fp = OutputFile(output_dir, (test + extension));
      cyl.TEST(init_pars, fp);
      if (fp != stdout) fclose(fp);
      if (flag == "-tUSINE") TestResult(output_dir, test);
   }


   //----------------
   // Integration tests
   //----------------
   if (flag == "-tUSINE")
      printf("   - integration tests\n");
   if (flag == "-t9" || flag == "-t9a" || flag == "-tALL" || flag == "-tUSINE" || flag == "-tGENREF") {
      // Propagation/modulation calculations (test)
      //printf("                [test propagation... (in progress)] \n");
      string test = "propag_run";
      FILE *fp = OutputFile(output_dir, (test + extension));

      string message = "Propagation/modulation for all models (2H-BAR,1H-BAR,1H,2H:kEKN;10Be,B/C,O:kR)";
      TUMessages::Test(fp, "usine_test", message);

      Int_t i_propmod = init_pars->IndexPar("UsineRun", "Models", "Propagation", true);

      for (Int_t i_prop = 0; i_prop < gN_PROPAG_MODEL; ++i_prop) {
         gENUM_PROPAG_MODEL model = TUEnum::gENUM_PROPAG_MODEL_LIST[i_prop];
         fprintf(fp, "# Reset initialisation parameters to %s\n", f_init.c_str());
         TUInitParList param_test;
         param_test.Copy(*init_pars);
         fprintf(fp, "# Set propagation model to %s\n", TUEnum::Enum2Name(model).c_str());
         param_test.SetVal(i_propmod, TUEnum::Enum2Name(model));

         gROOT->SetBatch(true);
         run = new TURunPropagation();
         run->SetClass(&param_test, false /*is_verbose*/, output_dir, fp);
         run->SetIsSaveInFiles(false);
         // Check local spectra
         TUCoordTXYZ *coord_txyz = NULL;
         TURunOutputs results;
         run->FillSpectra(results, "2H-BAR,1H-BAR,1H,2H:kEKN:0.,0.5;10Be,B/C,O:kR:0.,0.5", run->GetDiplayedFluxEPower(), coord_txyz, true, fp);
         run->PlotSpectra(my_app, results, false/*is_sep_isot*/, true/*is_print*/, fp/*fprint*/);
         delete run;
         run = NULL;
      }
      if (fp != stdout) fclose(fp);
      if (flag == "-tUSINE") TestResult(output_dir, test);
   }
   if (flag == "-t9" || flag == "-t9b" || flag == "-tALL" || flag == "-tUSINE" || flag == "-tGENREF") {
      // TEST 'usine -e' options
      //printf("                [usine -eALL... (in progress)] \n");
      string test = "usine_e_options";
      FILE *fp = OutputFile(output_dir, (test + extension));
      sprintf(argv[1], "-eALL");
      Option_E(argc, argv, /*is_test*/true, fp);
      if (fp != stdout) fclose(fp);
      if (flag == "-tUSINE") TestResult(output_dir, test);
   }
   if (flag == "-t9" || flag == "-t9c" || flag == "-tALL" || flag == "-tUSINE" || flag == "-tGENREF") {
      // TEST 'usine -i' options
      //printf("                [usine -iALL... (in progress)] \n");
      string test = "usine_i_options";
      FILE *fp = OutputFile(output_dir, (test + extension));
      // Option_I(exename, opt, opt_fname, Boolt is_test, FILE *f_test)
      gROOT->SetBatch(kTRUE);
      Option_I(string(argv[0]), "-iALL", "", /*is_test*/true, fp);
      if (fp != stdout) fclose(fp);
      if (flag == "-tUSINE") TestResult(output_dir, test);
   }
   if (flag == "-t9" || flag == "-t9d" || flag == "-tALL" || flag == "-tUSINE" || flag == "-tGENREF") {
      // Related to minimisation (test)
      //printf("                [test Minimization... (in progress)] \n");
      string test = "propag_minimisation";
      FILE *fp = OutputFile(output_dir, (test + extension));

      //--------------------
      // FITABLE PARAMETERS
      //--------------------
      string message = "Minimisation: fitable parameters";
      TUMessages::Test(fp, "usine_test", message);

      run = new TURunPropagation();
      run->SetClass(init_pars, false /*is_verbose*/, output_dir, fp /*f_log*/);
      run->SetIsSaveInFiles(false);
      // Read all free parameters, and add modulation and data.err ones
      run->UpdateFitParsAndFitData(init_pars, false /*is_verbose*/, false);
      TUFreeParList *par_list = run->GetPropagModel(0);
      // Add solar modulation ones
      for (Int_t i = 0; i < run->GetSolModModel(0)->ParsModExps_GetN(); ++i) {
         TUFreeParList *tmp = run->GetSolModModel(0)->ParsModExps_Get(i);
         for (Int_t j = 0; j < tmp->GetNPars(); ++j)
            par_list->AddPar(tmp->GetParEntry(j), true);
         tmp = NULL;
      }
      // Add data error as nuisance
      for (Int_t i = 0; i < run->GetFitData()->ParsData_GetN(); ++i) {
         TUFreeParList *tmp = run->GetFitData()->ParsData_Get(i);
         for (Int_t j = 0; j < tmp->GetNPars(); ++j)
            par_list->AddPar(tmp->GetParEntry(j), true);
         tmp = NULL;
      }
      par_list->ResetStatusParsAndParList();
      par_list->PrintPars(fp);
      par_list = NULL;
      fprintf(fp, "\n");

      //----------------------
      // Calculate Chi2
      //----------------------
//      vector<Double_t> par_vals;
//      for (Int_t i = 0; i < par_list->GetNPars(); ++i)
//         par_vals.push_back(par_list->GetParEntry(i)->GetFitInit());
//      fprintf(fp, "Output of run->Chi2_TOAFluxes(*pars): %le\n\n", run->Chi2_TOAFluxes(&par_vals[0]));

      //----------------------
      // FIT (1000 CALLS ONLY)
      //----------------------
      message = "Minimisation: Minuit fit";
      TUMessages::Test(fp, "usine_test", message);

      TUInitParList param_test;
      param_test.Copy(*init_pars);
      Int_t n_calls = 300;
      fprintf(fp, "# Set number of calls to %d\n", n_calls);
      Int_t i_ncalls = param_test.IndexPar("UsineFit", "Config", "NMaxCall", true);
      param_test.SetVal(i_ncalls, Form("%d", n_calls));

      // Run minimisation
      run->SetClass(&param_test, false /*is_verbose*/, output_dir, fp /*f_log*/);
      run->SetIsSaveInFiles(false);
      param_test.GetParEntry(i_ncalls).Print(fp);
      // Read all free parameters, and add modulation and data.err ones
      run->UpdateFitParsAndFitData(&param_test, false /*is_verbose*/, false);
      run->MinimiseTOAFluxes(&param_test, true, false);
      // Print best-chi2 with data used for minimisation
      gROOT->SetBatch(true);
      TUDataSet *data = run->GetFitData();
      run->GetPropagModel(0)->TUDataSet::Copy(*data);
      TUCoordTXYZ *coord_txyz = NULL;
      TURunOutputs results;
      run->FillSpectra(results, data->GetQtiesEtypesphiFF(), run->GetDiplayedFluxEPower(), coord_txyz, true, fp);
      run->PlotSpectra(my_app, results, false/*is_sep_isot*/, true/*is_print*/, fp/*fprint*/);

      // Run second minimisation (to test LC of XS)
      fprintf(fp, "# Second minimisation using linear combination of XS\n");
      //  0) Remove most nuisance parameters
      Int_t i_crs = param_test.IndexPar("UsineFit", "FreePars", "CRs", true);
      param_test.Delete(i_crs);
      Int_t i_dataerr = param_test.IndexPar("UsineFit", "FreePars", "DataErr", true);
      param_test.Delete(i_dataerr);
      Int_t i_modul = param_test.IndexPar("UsineFit", "FreePars", "Modulation", true);
      param_test.Delete(i_modul);
      Int_t i_src = param_test.IndexPar("UsineFit", "FreePars", "SrcSteadyState", true);
      param_test.Delete(i_src);
      Int_t i_scan = param_test.IndexPar("UsineFit", "Outputs", "Scans", true);
      param_test.Delete(i_scan);
      //  1) use linear combination of XS as nuisance
      Int_t i_xs = param_test.IndexPar("UsineFit", "FreePars", "XSection", true);
      param_test.ClearVals(i_xs);
      param_test.SetVal(i_xs, "LCInelBar94_12C+H:NUISANCE,LIN,[0.,1.5],0.25,0.1", 0);
      param_test.SetVal(i_xs, "LCInelTri99_12C+H:NUISANCE,LIN,[0.,1.5],0.25,0.1", 1);
      param_test.SetVal(i_xs, "LCInelWeb03_12C+H:NUISANCE,LIN,[0.,1.5],0.25,0.1", 2);
      param_test.SetVal(i_xs, "LCInelWel97_12C+H:NUISANCE,LIN,[0.,1.5],0.25,0.1", 3);
      param_test.SetVal(i_xs, "LCProdGal17_16O+H->11B:NUISANCE,LIN,[0.,1.5],0.25,0.1", 4);
      param_test.SetVal(i_xs, "LCProdSou01_16O+H->11B:NUISANCE,LIN,[0.,1.5],0.25,0.1", 5);
      param_test.SetVal(i_xs, "LCProdWeb03_16O+H->11B:NUISANCE,LIN,[0.,1.5],0.25,0.1", 6);
      param_test.SetVal(i_xs, "LCProdWKS98_16O+H->11B:NUISANCE,LIN,[0.,1.5],0.25,0.1", 7);
      //  2) minimise
      run->SetClass(&param_test, false /*is_verbose*/, output_dir, fp /*f_log*/);
      run->SetIsSaveInFiles(false);
      // Read all free parameters, and add modulation and data.err ones
      run->UpdateFitParsAndFitData(&param_test, false /*is_verbose*/, false);
      run->MinimiseTOAFluxes(&param_test, true, false);

      // Run third minimisation (to test LC of XS with key 'ALL' for inel and prod)
      fprintf(fp, "# Third minimisation using linear combination of XS (using key 'ALL')\n");
      param_test.ClearVals(i_xs);
      param_test.SetVal(i_xs, "LCInelBar94_ALL:NUISANCE,LIN,[0.,1.5],0.25,0.1", 0);
      param_test.SetVal(i_xs, "LCInelTri99_ALL:NUISANCE,LIN,[0.,1.5],0.25,0.1", 1);
      param_test.SetVal(i_xs, "LCInelWeb03_ALL:NUISANCE,LIN,[0.,1.5],0.25,0.1", 2);
      param_test.SetVal(i_xs, "LCInelWel97_ALL:NUISANCE,LIN,[0.,1.5],0.25,0.1", 3);
      param_test.SetVal(i_xs, "LCProdGal17_ALL:NUISANCE,LIN,[0.,1.5],0.25,0.1", 4);
      param_test.SetVal(i_xs, "LCProdSou01_ALL:NUISANCE,LIN,[0.,1.5],0.25,0.1", 5);
      param_test.SetVal(i_xs, "LCProdWeb03_ALL:NUISANCE,LIN,[0.,1.5],0.25,0.1", 6);
      param_test.SetVal(i_xs, "LCProdWKS98_ALL:NUISANCE,LIN,[0.,1.5],0.25,0.1", 7);
      //  2) minimise
      run->SetClass(&param_test, false /*is_verbose*/, output_dir, fp /*f_log*/);
      run->SetIsSaveInFiles(false);
      // Read all free parameters, and add modulation and data.err ones
      run->UpdateFitParsAndFitData(&param_test, false /*is_verbose*/, false);
      run->MinimiseTOAFluxes(&param_test, true, false);


      delete run;
      run = NULL;
      if (fp != stdout) fclose(fp);
      if (flag == "-tUSINE") TestResult(output_dir, test);
   }
   return;
}

//______________________________________________________________________________
void Option_U(Int_t argc, char *argv[])
{
   //--- USINE -u option related to propagation of uncertainties.
   //  argc              Number of user parameters
   //  argv              Value of user parameters

   string opt = argv[1];
   indent = TUMessages::Indent(true);
   TUMessages::Indent(false);


   if (opt == "-u1") {
      // Display drawn values of fit/covariance matrix values
      if (argc < 9) {
         printf("USAGE:   %s -u1  init_file  output_dir  bestfit_file  cov_file  commasep_indices  n_samples  is_bounds\n"
                "EXAMPLE: %s -u1  inputs/init.TEST.par  $USINE/output  $USINE/inputs/fit.TEST.result.out  $USINE/inputs/fit.TEST.covmatrix.out  0,1,4,5  1000  1\n"
                "\n"
                "   [init_file]        USINE initialisation file\n"
                "   [output_dir]       Directory for outputs (plots, files, etc.)\n"
                "   [bestfit_file]     Best-fit file (USINE format)\n"
                "   [cov_file]         Covariance file (USINE format)\n"
                "   [commasep_indices] Comma-separated indices of parameters to draw (L to print list, ALL to draw all)"
                "   [n_samples]        Number of samples to draw from covariance matrix (as many calculations)"
                "   [is_bounds]        If true, discard all drawn parameters out of boundaries (as specified in .par file)\n",
                argv[0], argv[0]);
         printf("\n");
         return;
      }

      // LoadUserOptions(init_file, output_dir, is_verbose, is_saveinfiles, is_showplots)
      Bool_t is_verbose = true;
      Bool_t is_saveinfiles = false;
      Bool_t is_showplots = true;
      string output_dir = argv[3];
      LoadUserOptions(argv[2], TUMisc::CreateDir(output_dir, "usine", "-u1"),  is_verbose, is_saveinfiles, is_showplots);

      // Load fit and cov.mat files
      string fit_file = argv[4];
      string covmat_file = argv[5];
      string commasep_indices = argv[6];
      Int_t n_samples = atoi(argv[7]);
      Bool_t is_bounds = atoi(argv[8]);
      TUMisc::UpperCase(commasep_indices);
      vector<Int_t> indices2draw;
      if (commasep_indices.substr(0, 3) != "ALL")
         TUMisc::String2List(commasep_indices, ",", indices2draw);

      TUFreeParList loaded_pars;
      // If only print parameter names
      if (commasep_indices[0] == 'L') {
         indices2draw.clear();
         loaded_pars.LoadFitValues(fit_file, indices2draw, /*is_skipped_fixed*/true, is_verbose, run->GetOutputLog());
         printf("%s        => List of valid parameters are:\n", indent.c_str());
         loaded_pars.PrintFitVals(stdout);
         return;
      }
      printf("%s    - Load fit parameter values (USINE-formatted file %s)\n", indent.c_str(), fit_file.c_str());
      loaded_pars.LoadFitValues(fit_file, indices2draw, /*is_skipped_fixed*/true, is_verbose, run->GetOutputLog());
      printf("%s    - Load cov. matrix from fit (USINE-formatted file %s)\n", indent.c_str(), covmat_file.c_str());
      loaded_pars.LoadFitCovMatrix(covmat_file, indices2draw, is_verbose, run->GetOutputLog());
      //loaded_pars.PrintFitCovMatrix(stdout);
      printf("%s        => check two files have same parameters (FIXED skipped in fit file)\n", indent.c_str());
      loaded_pars.CheckMatchingParNamesAndCovParNames(run->GetOutputLog());


      vector<Double_t> vals_min(loaded_pars.GetNPars()), vals_max(loaded_pars.GetNPars());
      if (is_bounds) {
         for (Int_t i = 0; i < loaded_pars.GetNPars(); ++i) {
            vals_min[i] = loaded_pars.GetParEntry(i)->GetFitInitMin();
            vals_max[i] = loaded_pars.GetParEntry(i)->GetFitInitMax();
         }
      }

      // Extract best-fit vector and cov.matrix in suitable format to later draw from
      TVectorD bestfit = loaded_pars.ExtractBestFitVals();
      TMatrixDSym covmat = loaded_pars.GetFitCovMatrix();

      // Draw for n_samples
      printf("%s    - Plot 1D and 2D distributions drawn from %d samples)\n", indent.c_str(), n_samples);
      if (is_bounds)
         TUMath::DrawFromMultiGaussAndPlot(my_app, loaded_pars.GetFitCovParNames(), bestfit, covmat, n_samples, &vals_min[0], &vals_max[0]);
      else
         TUMath::DrawFromMultiGaussAndPlot(my_app, loaded_pars.GetFitCovParNames(), bestfit, covmat, n_samples);
   }


   if (opt == "-u2") {
      // Minimisation on TOA fluxes
      if (argc < 16) {
         printf("USAGE:   %s -u2  init_file  output_dir  bestfit_file  cov_file  n_samples  is_median  is_1sigma  is_2sigma  is_3sigma  combos_etypes_phiff  e_power  is_verbose  is_saveinfiles is_showplots\n"
                "EXAMPLE: %s -u2  inputs/init.TEST.par  $USINE/output  $USINE/inputs/fit.TEST.result.out $USINE/inputs/fit.TEST.covmatrix.out 100  1 1 0 0  \"B/C,C:kR:0.,0.73;O:kEKN:0.5\"  0.  1  1  1\n"
                "\n"
                "   [init_file]           USINE initialisation file\n"
                "   [output_dir]          Directory for outputs (plots, files, etc.)\n"
                "   [bestfit_file]        Best-fit file (USINE format)\n"
                "   [cov_file]            Covariance file (USINE format)\n"
                "   [n_samples]           Number of samples to draw from covariance matrix (as many calculations)\n"
                "   [is_median]           Whether (1) or not (0) to draw median from sample\n"
                "   [is_1sigma]           Whether (1) or not (0) to draw 1-sigma contours from sample\n"
                "   [is_2sigma]           Whether (1) or not (0) to draw 2-sigma contours from sample\n"
                "   [is_3sigma]           Whether (1) or not (0) to draw 3-sigma contours from sample\n"
                "   [combos_etypes_phiff] List of CR fluxes/ratios and associated E types/phi_FF (e.g., B/C,O:kEKN:0.73,0.5;C,N,O:kR:0.38)\n"
                "   [e_power]             Fluxes are multiplied by E^e_power, where E is e_type\n"
                "   [is_verbose]          To have more informations on the run (printed in logfile)\n"
                "   [is_saveinfiles]      Save outputs of calculation and macros for plots (1) or not (0)\n"
                "   [is_showplots]        Show plots (1) or batch mode (0)\n",
                argv[0], argv[0]);
         printf("\n");
         return;
      }

      string fit_file = argv[4];
      string covmat_file = argv[5];
      Int_t n_samples = atoi(argv[6]);
      Bool_t is_median = atoi(argv[7]);
      Bool_t is_1sigma = atoi(argv[8]);
      Bool_t is_2sigma = atoi(argv[9]);
      Bool_t is_3sigma = atoi(argv[10]);
      string combos_etypes_phiff = argv[11];
      Double_t e_power = atof(argv[12]);
      Bool_t is_verbose = atoi(argv[13]);
      Bool_t is_saveinfiles = atoi(argv[14]);
      Bool_t is_showplots = atoi(argv[15]);
      string output_dir = argv[3];
      LoadUserOptions(argv[2], TUMisc::CreateDir(output_dir, "usine", "-u2"), is_verbose, is_saveinfiles, is_showplots);
      run->SetIsPrintHessianMatrix(false);
      run->SetIsPrintCovMatrix(false);
      run->SetIsPrintBestFit(false);
      run->SetIsCLMode(true);
      if (run->GetNPropagModels() > 1)
         TUMessages::Error(stdout, "usine", "(option -u2)", "Option not enabled if more than 1 propagation model selected");


      TUMessages::Separator(stdout, "INITIALISATION RUN", 1);


      // N.B.: is this run, all parameters are set to FIXED (use Minimiser in 'simple' calculation mode)
      printf("%s    - Current parameters (set to kFIXED) used in run:\n", indent.c_str());
      run->UpdateFitParsAndFitData(init_pars, is_verbose, true);
      for (Int_t i_par = 0; i_par < run->GetFitPars()->GetNPars(); ++i_par)
         run->GetFitPars()->GetParEntry(i_par)->SetFitType(kFIXED);
      run->GetFitPars()->ResetStatusParsAndParList();
      run->GetFitPars()->PrintPars(stdout);
      run->SetMinuitPrintLevel(0);
      run->SetNSamples(n_samples);

      // Retrieve sample (n_samples)
      vector<vector<Double_t>> drawn_vals;
      TUFreeParList loaded_pars;
      Bool_t is_propag_cls = DrawFromFitAndCovMat(drawn_vals, loaded_pars, fit_file, covmat_file, n_samples, run);

      // Loop on number of configurations to calculate
      Int_t n_digits = TUMisc::Number2Digits(n_samples);
      printf("%s    - Loop on %d configurations (ensuring parameter boundaries) to evaluate confidence levels on %s\n", indent.c_str(), n_samples, combos_etypes_phiff.c_str());
      vector<TURunOutputs> sample_results(n_samples);

      for (Int_t i_sample = 0; i_sample < n_samples; ++i_sample) {
         string id_config = Form("%0*d", n_digits, i_sample + 1);
         string message = Form("calculate config. #%s/%d", id_config.c_str(), n_samples);
         TUMessages::Separator(stdout, message, 2);

         // Update loaded pars from new generated parameters. Note that drawn
         // parameters might be in 'log', and need to be converted appropriately.
         for (Int_t i = 0; i < loaded_pars.GetNPars(); ++i)
            loaded_pars.GetParEntry(i)->SetVal(drawn_vals[i_sample][i], loaded_pars.GetParEntry(i)->GetFitSampling());
         loaded_pars.PrintPars();
         run->GetFitPars()->UpdateParVals(loaded_pars, /*gENUM_FITPARTYPE*/kANY, /*is_enforce_allfound*/ true, /*is_check_range*/false, stdout);

         // Calculate and retrieve local spectra
         run->SetIndexSample(i_sample);
         Double_t chi2 = run->Chi2_TOAFluxes(&run->GetFitPars()->ExtractParsValsForChi2()[0]);
         printf("    => chi2/ndof = %le/%d = %le\n", chi2, run->GetNdof(), chi2 / run->GetNdof());
         run->FillSpectra(sample_results[i_sample], combos_etypes_phiff, e_power);
      }

      TUMessages::Separator(stdout, "RESULTS", 2);
      // Extract CLs
      TURunOutputs results_cls;
      results_cls.FillAsCLs(sample_results, is_median, is_1sigma, is_2sigma, is_3sigma);
      run->PlotSpectra(my_app, results_cls, false/*is_sep_isot*/);
   }
}

//______________________________________________________________________________
FILE *OutputFile(string const &output_dir, string const &file_name)
{
   //--- Open file (write mode) depending on output_dir value.
   //  output_dir      Directory to store files (if "", output file is stdout)
   //  file_name       Name of output file

   if (output_dir == "")
      return stdout;
   else {
      string f_name = output_dir + "/" + file_name;
      FILE *fp = fopen(f_name.c_str(), "w");
      if (f_name.find(".ref") != string::npos)
         printf("         -> save in %s\n", f_name.c_str());
      return fp;
   }
}

//______________________________________________________________________________
void PrintLogoAndVersion(FILE *f)
{
   //--- Prints logo and version in file.
   //  f_test          File in which to print
   string git_tag = TUMisc::SystemExec("git --git-dir=$USINE/.git describe --abbrev=7 --always --tags");
   git_tag = TUMisc::RemoveBlancksFromStartStop(TUMisc::RemoveChars(git_tag, "\n"));
   string usine_version_print = gUSINE_VERSION.substr(6) + ", git_ID=" + git_tag;

   fprintf(f, "\n");
   fprintf(f, "             ██╗   ██╗███████╗██╗███╗   ██╗███████╗ \n");
   fprintf(f, "             ██║   ██║██╔════╝██║████╗  ██║██╔════╝ \n");
   fprintf(f, "             ██║   ██║███████╗██║██╔██╗ ██║█████╗   \n");
   fprintf(f, "             ██║   ██║╚════██║██║██║╚██╗██║██╔══╝   \n");
   fprintf(f, "             ╚██████╔╝███████║██║██║ ╚████║███████╗ \n");
   fprintf(f, "              ╚═════╝ ╚══════╝╚═╝╚═╝  ╚═══╝╚══════╝ \n");
   fprintf(f, "                    (%s)\n", usine_version_print.c_str());
   fprintf(f, "\n");
}

//______________________________________________________________________________
void TestResult(string const &output_dir, string const &test_name)
{
   //--- Test output for test/file 'test_name'
   //  output_dir      Directory to store USINE outputs
   //  test_naùe       Name of test (file name) performed

   string base = "$USINE/" + output_dir + "/" + test_name;
   string command = "diff -q " + base + ".fail " + base + ".ref";
   printf("      ... %-30s", test_name.c_str());
   Int_t sys = system(command.c_str());
   if (sys == 0) {
      // If test passes, remove generated file (.fail)
      printf("    [passed]\n");
      system(("rm " + base + ".fail").c_str());
   } else
      // If test fails, chatter
      printf("   [ FAILED ]\n");

   return;
}

//______________________________________________________________________________
//______________________________________________________________________________
Int_t main(Int_t argc, char *argv[])
{
   //--- USINE run!
   PrintLogoAndVersion(stdout);

   // Main run options
   if (argc <= 1 || (argc >= 2 && (argv[1][0] != '-' || string(argv[1]) == "-m" || string(argv[1]) == "-u"))) {
      printf("  USAGE:\n");
      printf("     %s -l         (L)ocal flux calculation, show only selected quantities\n", argv[0]);
      printf("     %s -m         (M)inimisation-related\n", argv[0]);
      printf("                     -m1       [List of fit-able parameters]\n"
             "                     -m2       [Chi2 minimisation on TOA fluxes (best chi2)]\n");
      printf("     %s -u         (U)ncertainty propagation (generates multiple fluxes and ratios)\n", argv[0]);
      printf("                     -u1       [Display sample of parameters drawn best-fit/covariance matrix]\n"
             "                     -u2       [CLs from USINE-formatted best-fit and covariance matrix files]\n");
      printf("\n");
      printf("\n");
      printf("     %s -e         (E)xtras and comparisons (interactive run)\n", argv[0]);
      printf("                     -e        [List/detail of all options]\n"
             "                     -ea       [Local fluxes and spatial dependences]\n"
             "                     -eb       [Text-interface to modify model params (re-run any -e option)]\n"
             "                     -ec       [Infos on current models and their parameters]\n"
             "                     -ed       [Extra calculations/checks: XS-related, decay-related, diff. prod.]\n"
             "                     -ee       [Comparison plots varying boundary conditions, params, XS files...\n");
      printf("     %s -i         (I)nput files/properties\n", argv[0]);
      printf("                     -i        [List all options]\n"
             "                     -id       [List options related to CR data input files]\n"
             "                     -ie       [List options related to interaction]\n"
             "                     -ix       [List options related to cross sections input files]\n");
      printf("     %s -t         (T)est functions for USINE (\'-t\' to see all test options)\n", argv[0]);
      printf("                     -t        [List all test functions available]\n"
             "                     -tALL     [TEST all USINE classes, propagation, and minimisation]\n"
             "                     -tUSINE   [RUN -tALL against reference test files (in usine.tests/)]\n");
      printf("\n");
      return 1;
   }

   // ROOT: OPEN root application and set USINE plot style
   my_app = new TApplication("App", NULL, NULL);
   TUMisc::RootSetStylePlots();


   string opt = "";
   if (argc >= 2) opt = string(argv[1]).substr(0, 2);

   // Text-user-interface
   if (opt == "-e")
      Option_E(argc, argv);
   // Input files plotted
   else if (opt == "-i") {
      string opt_fname = "";
      if (argc >= 3)
         opt_fname = argv[2];
      Option_I(string(argv[0]), string(argv[1]), opt_fname);
   }
   // Local flux propagation
   else if (opt == "-l")
      Option_L(argc, argv);
   // Minimisation-related
   else if (opt == "-m")
      Option_M(argc, argv);
   // Text-user-interface
   else if (opt == "-t")
      Option_T(argc, argv);
   // Uncertainty-propagated calculation
   else if (opt == "-u")
      Option_U(argc, argv);

   // Free memory
   if (my_app)
      delete my_app;
   if (init_pars)
      delete init_pars;
   if (run)
      delete run;

   return 1;
}

