// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

// C/C++ include
#include <cmath>
// ROOT include
// USINE include
#include "../include/TUMath.h"
#include "../include/TUMessages.h"
#include "../include/TUPhysics.h"

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TUPhysics                                                            //
//                                                                      //
// Encapsulate frequently used (astro-)physics constants and functions. //
//                                                                      //
// The namespace TUPhysics encapsulates:                                //
//    - (astro-)physics constant (Rsol, hbar, c, etc.);
//    - conversion factors for units (cm<->pc, mb<->cm2, etc.);
//    - conversion formulae (E <-> R <-> p <-> Ekn...);
//                                                                      //
//////////////////////////////////////////////////////////////////////////

NamespaceImp(TUPhysics)

//______________________________________________________________________________
Double_t TUPhysics::AltitudeToPressure_mbar(Double_t const &h_m)
{
   //--- Returns pressure [mbar] from altitude [m] (valid below 11000 m)
   //    from the barometric formula (see wikipedia page).
   //  h_m               Altitude above sea level [m]

   const Double_t p0 = 101325.;     // Static pressure            [Pa]
   const Double_t l0 = 0.0065;      // Temperature lapse rate     [K/m]
   const Double_t t0 = 288.15;      // Standard temperature         [K]
   const Double_t g = 9.80665;      // Gravitational acceleration [m/s2]
   const Double_t m = 0.0289644;    // Molar mass of Earth's air  [kg/mol]
   const Double_t r_star = 8.31447; // Universal gas constant     [J/(mol.K)]
   const Double_t h0 = 0.;          // Height Above Sea Level     [geopotential meters]

   // N.B.: 1Pa = 0.01 mbar
   return 0.01 * p0 * pow(t0 / (t0 + l0 * (h_m - h0)), g * m / (r_star * l0));
}

//______________________________________________________________________________
Double_t TUPhysics::Beta_pE(Double_t const &p_gev, Double_t const &e_gev)
{
   //--- Return beta of the particle (= v/c = p/E).
   //  p                 Momentum of the particle [GeV]
   //  E                 Total energy of the particle [GeV]

   return p_gev / e_gev;
}

//______________________________________________________________________________
Double_t TUPhysics::Beta_mEk(Double_t const &m_gev, Double_t const &ek_gev)
{
   //--- Returns beta of the particle (= v/c = p/E).
   //  m_gev             Mass of the particle [GeV]
   //  Ek                Kinetic energy (Ek=E+m) [GeV]

   return sqrt(ek_gev * (ek_gev + 2. * m_gev)) / (ek_gev + m_gev);
}

//______________________________________________________________________________
Double_t TUPhysics::Beta_mAEkn(Double_t const &m_gev, Int_t a_cr, Double_t const &ekn_gevn)
{
   //--- Returns beta of the particle (= v/c = p/E).
   //  m_gev             Mass of the particle [GeV]
   //  A                 Atomic number of the species (A=1 for leptons so that 'Ekn'(=Ek) exists)
   //  Ekn               Kinetic energy per nucleon (Ekn=Ek/n) of the particle [GeV/n]

   // A=1 for leptons so that 'Ekn'(=Ek) exists
   if (a_cr == 0) a_cr = 1;
   Double_t ek_gev = ekn_gevn * (Double_t)a_cr;
   return sqrt(ek_gev * (ek_gev + 2. * m_gev)) / (ek_gev + m_gev);
}

//______________________________________________________________________________
Double_t TUPhysics::Convert_to_Myr(string const &unit)
{
   //--- Returns conversion factor to go from unit to Myr.
   //  unit              Unit to convert: s, h, d, yr, kyr or Myr

   string unit_upper = unit;
   TUMisc::UpperCase(unit_upper);

   if (unit_upper == "S") return (1.e-6 / Convert_yr_to_s());
   else if (unit_upper == "H") return 3600.*1.e-6 / (Convert_yr_to_s());
   else if (unit_upper == "D") return (8.64e4 * 1.e-6 / Convert_yr_to_s());
   else if (unit_upper == "YR") return 1.e-6;
   else if (unit_upper == "KYR") return 1.e-3;
   else if (unit_upper == "MYR") return 1.;
   else {
      string message = "bad unit " + unit;
      TUMessages::Error(stdout, "TUPhysics", "Convert_to_Myr", message);
      return 0.;
   }
}

//______________________________________________________________________________
Double_t TUPhysics::ConvertE(Double_t const &e_in, gENUM_ETYPE etype_in, gENUM_ETYPE etype_out,
                             Int_t a_cr, Int_t z_cr, Double_t const &m_gev)
{
   //--- Converts and returns energy 'e_in' of type 'etype_in' into 'etype_out'
   //    for a given CR. See allowed types in TUEnum.h (kEKN, kEK, kR, kETOT).
   //  e_in              Energy value 'etype_in' (GeV/n, GeV or GV depending on etype_in)
   //  etype_in          Type of 'e_in' (kEKN, kEK, kR, kETOT)
   //  etype_out         Type for the output energy
   //  A                 Atomic number of the species (A=1 for leptons so that 'Ekn'(=Ek) exists)
   //  Z                 Charge of the species
   //  m_gev             Mass of the particle [GeV]

   if (etype_in == etype_out)
      return e_in;

   if (etype_in == kEKN) {
      if (etype_out == kEK) return Ekn_to_Ek(e_in, a_cr);
      else if (etype_out == kR) return Ekn_to_R(e_in, a_cr, m_gev, z_cr);
      else if (etype_out == kETOT) return Ekn_to_E(e_in, a_cr, m_gev);
   } else if (etype_in == kEK) {
      if (etype_out == kEKN) return Ek_to_Ekn(e_in, a_cr);
      else if (etype_out == kR) return Ek_to_R(e_in, m_gev, z_cr);
      else if (etype_out == kETOT) return Ek_to_E(e_in, m_gev);
   } else if (etype_in == kR) {
      if (etype_out == kEKN) return R_to_Ekn(e_in, a_cr, m_gev, z_cr);
      else if (etype_out == kEK) return R_to_Ek(e_in, m_gev, z_cr);
      else if (etype_out == kETOT) return R_to_E(e_in, m_gev, z_cr);
   } else if (etype_in == kETOT) {
      if (etype_out == kEKN) return E_to_Ekn(e_in, a_cr, m_gev);
      else if (etype_out == kEK) return E_to_Ek(e_in, m_gev);
      else if (etype_out == kR) return E_to_R(e_in, m_gev, z_cr);
   }

   return 0.;
}

//______________________________________________________________________________
Double_t TUPhysics::dNdEin_to_dNdEout(Double_t const &e_in, gENUM_ETYPE etype_in, gENUM_ETYPE etype_out,
                                      Int_t a_cr, Int_t z_cr, Double_t const &m_gev)
{
   //--- Converts and returns factor A for a given CR such that
   //       dN/dEtype_out(e_in) = A * dN/dEtype_in(e_in)
   //    See allowed types in TUEnum.h (kEKN, kEK, kR, kETOT).
   //  e_in              Energy value 'etype_in' (GeV/n, GeV or GV depending on etype_in)
   //  etype_in          Type of 'e_in' (kEKN, kEK, kR, kETOT)
   //  etype_out         Type for the output energy
   //  A                 Atomic number of the species (A=1 for leptons so that 'Ekn'(=Ek) exists)
   //  Z                 Charge of the species
   //  m_gev             Mass of the particle [GeV]

   if (etype_in == etype_out)
      return 1.;

   // A=1 for leptons so that 'Ekn'(=Ek) exists
   if (a_cr == 0) a_cr = 1;

   if (etype_in == kEKN) {
      if (etype_out == kEK || etype_out == kETOT)
         return (1. / Double_t(a_cr));
      else if (etype_out == kR)
         return (1. / Double_t(a_cr) * Double_t(abs(z_cr)) * Beta_mAEkn(m_gev, a_cr, e_in));
   } else if (etype_in == kEK) {
      if (etype_out == kEKN)
         return (Double_t(a_cr));
      else if (etype_out == kR)
         return (Double_t(abs(z_cr)) * Beta_mEk(m_gev, e_in));
      else if (etype_out == kETOT)
         return 1.;
   } else if (etype_in == kR) {
      Double_t betaz = Double_t(abs(z_cr)) * Beta_mAEkn(m_gev, a_cr, R_to_Ekn(e_in, a_cr, m_gev, z_cr));
      if (etype_out == kEKN)
         return (Double_t(a_cr) / betaz);
      else if (etype_out == kEK || etype_out == kETOT)
         return (1. / betaz);
   } else if (etype_in == kETOT) {
      if (etype_out == kEKN)
         return (Double_t(a_cr));
      else if (etype_out == kEK)
         return 1.;
      else if (etype_out == kR)
         return (Double_t(abs(z_cr)) * Beta_mEk(m_gev,   E_to_Ek(e_in, m_gev)));
   }

   return 0.;
}

//______________________________________________________________________________
Double_t TUPhysics::dEkndR_Ekn(Double_t const &ekn_gevn, Int_t a_cr, Int_t z_cr, Double_t const &m_gev)
{
   //--- Converts differential kinetic energy per nucleon to differential
   //    rigidity: dEkn(@Ekn)/dR = (Z*beta(Ekn))/A.
   //  Ekn               Kinetic energy per nucleon (Ekn=Ek/n) of the particle [GeV/n]
   //  A                 Atomic number of the species (A=1 for leptons so that 'Ekn'(=Ek) exists)
   //  Z                 Charge of the species
   //  m_gev             Mass of the particle [GeV]

   // A=1 for leptons so that 'Ekn'(=Ek) exists
   if (a_cr == 0) a_cr = 1;
   return  Double_t(abs(z_cr)) * Beta_mAEkn(m_gev, a_cr, ekn_gevn) / (Double_t)a_cr;
}

//______________________________________________________________________________
void TUPhysics::dNdEkn_to_dNdR(TUAxis *ekn_axis, Double_t *dndekn,
                               TUAxis *r_axis, vector<Double_t> &dndr,
                               Int_t a_cr, Int_t z_cr, Double_t const &m_gev)
{
   //--- Converts and interpolates dN(r_axis)/dR to dN(ekn_axis)/dEkn.
   // INPUTS:
   //  ekn_axis          Kinetic energy per nucleon axis [GeV/n]
   //  dndekn            Differential quantity in Ekn
   //  r_axis            Rigidity axis on which to interpolate dndekn
   //  A                 Atomic number of the species (A=1 for leptons so that 'Ekn'(=Ek) exists)
   //  Z                 Charge of the species
   //  m_gev             Mass of the particle [GeV]
   // OUTPUTS:
   //  dndr              Interpolated differential quantity in rigidity

   // A=1 for leptons so that 'Ekn'(=Ek) exists
   if (a_cr == 0) a_cr = 1;

   // First step:
   //  - calculate R(Ekn) from Ekn (on ekn_axis)
   //  - calculate dN(R(Ekn))/dR = dN(Ekn)/dEkn * dEkn(@Ekn)/dR
   //                            = (dN(Ekn)/dEkn) / (dR(@R(Ekn))/dEkn)
   vector<Double_t> r_oneknaxis;
   vector<Double_t> dndr_eknaxis;
   for (Int_t i = 0; i < ekn_axis->GetN(); ++i) {
      Double_t ekn_gevn = ekn_axis->GetVal(i);
      Double_t r_gv = Ekn_to_R(ekn_gevn, a_cr, m_gev, z_cr);

      // Ekn values for R axis
      r_oneknaxis.push_back(r_gv);
      dndr_eknaxis.push_back(dndekn[i] / dRdEkn_R(r_gv, a_cr, z_cr, m_gev));
   }

   // Check range for 'new' energies within range 'old' energies
   if (r_axis->GetVal(0) < 0.99 * r_oneknaxis[0])
      TUMessages::Error(stdout, "TUPhysics", "dNdEkn_to_dNdR",
                        "Min energy to interpolate out of range (of input energies)");
   else if (r_axis->GetVal(r_axis->GetN() - 1) > 1.01 * r_oneknaxis[ekn_axis->GetN() - 1])
      TUMessages::Error(stdout, "TUPhysics", "dNdEkn_to_dNdR",
                        "Max energy to interpolate out of range (of input energies)");


   // Allocate right size for dndekn
   dndr.clear();
   dndr.assign(r_axis->GetN(), 0);

   // Interpolate values for dn_dekn_raxis (on r_axis) on ekn_axis grid
   TUMath::Interpolate(&r_oneknaxis[0], ekn_axis->GetN(), &dndr_eknaxis[0],
                       r_axis->GetVals(), r_axis->GetN(), &dndr[0], kLOGLOG);
}

//______________________________________________________________________________
void TUPhysics::dNdR_to_dNdEkn(TUAxis *r_axis, Double_t *dndr,
                               TUAxis *ekn_axis, vector<Double_t> &dndekn,
                               Int_t a_cr, Int_t z_cr, Double_t const &m_gev)
{
   //--- Converts and interpolates dN(r_axis)/dR to dN(ekn_axis)/dEkn.
   // INPUTS:
   //  r_axis            Rigidity axis [GV]
   //  dndr              Differential quantity in rigidity
   //  ekn_axis          Ekn axis on which to interpolate dndr
   //  A                 Atomic number of the species (A=1 for leptons so that 'Ekn'(=Ek) exists)
   //  Z                 Charge of the species
   //  m_gev             Mass of the particle [GeV]
   // OUTPUTS:
   //  dndekn            Interpolated differential quantity in kinetic energy per nucleon

   // A=1 for leptons so that 'Ekn'(=Ek) exists
   if (a_cr == 0) a_cr = 1;

   // First step:
   //  - calculate Ekn(R) from R (on r_axis)
   //  - calculate dN(Ekn(R))/dEkn = dN(R)/dR * dR(@R)/Ekn
   //                              = (dN(R)/dR) / (dEkn(@Ekn(R))/dR)
   vector<Double_t> ekn_onraxis;
   vector<Double_t> dndekn_raxis;
   for (Int_t i = 0; i < r_axis->GetN(); ++i) {
      Double_t r_gv = r_axis->GetVal(i);
      Double_t ekn_gevn = R_to_Ekn(r_gv, a_cr, m_gev, z_cr);

      // Ekn values for R axis
      ekn_onraxis.push_back(ekn_gevn);
      dndekn_raxis.push_back(dndr[i] / dEkndR_Ekn(ekn_gevn, a_cr, z_cr, m_gev));
   }


   // Check range for 'new' energies within range 'old' energies
   if (ekn_axis->GetVal(0) < 0.99 * ekn_onraxis[0])
      TUMessages::Error(stdout, "TUPhysics", "dNdR_to_dNdEkn",
                        "Min energy to interpolate out of range (of input energies)");
   else if (ekn_axis->GetVal(ekn_axis->GetN() - 1) > 1.01 * ekn_onraxis[r_axis->GetN() - 1])
      TUMessages::Error(stdout, "TUPhysics", "dNdR_to_dNdEkn",
                        "Max energy to interpolate out of range (of input energies)");


   // Allocate right size for dndekn
   dndekn.clear();
   dndekn.assign(ekn_axis->GetN(), 0);

   // Interpolate values for dn_dekn_raxis (on r_axis) on ekn_axis grid
   TUMath::Interpolate(&ekn_onraxis[0], r_axis->GetN(), &dndekn_raxis[0],
                       ekn_axis->GetVals(), ekn_axis->GetN(), &dndekn[0], kLOGLOG);
}


//______________________________________________________________________________
Double_t TUPhysics::dRdEkn_R(Double_t const &r_gv, Int_t a_cr, Int_t z_cr, Double_t const &m_gev)
{
   //--- Converts differential rigidity to differential kinetic energy per
   //    nucleon: dR(@R)/dEkn = A/(Z*beta(R)).
   //  R                 Rigidity (R=pc/Ze) of the particle [GV]
   //  A                 Atomic number of the species (A=1 for leptons so that 'Ekn'(=Ek) exists)
   //  Z                 Charge of the species
   //  m_gev             Mass of the particle [GeV]

   // A=1 for leptons so that 'Ekn'(=Ek) exists
   if (a_cr == 0) a_cr = 1;
   Double_t ekn_gevn = R_to_Ekn(r_gv, a_cr, m_gev, z_cr);
   return 1. / dEkndR_Ekn(ekn_gevn, a_cr, z_cr, m_gev);
}

//______________________________________________________________________________
Double_t TUPhysics::Ekn_to_E(Double_t const &ekn_gevn, Int_t a_cr, Double_t const &m_gev)
{
   //--- Returns E from Ekn.
   //  E                 Total energy of the particle [GeV]
   //  Ekn               Kinetic energy per nucleon (Ekn=Ek/n) of the particle [GeV/n]
   //  A                 Atomic number of the species (A=1 for leptons so that 'Ekn'(=Ek) exists)
   //  m_gev             Mass of the particle [GeV]

   // A=1 for leptons so that 'Ekn'(=Ek) exists
   if (a_cr == 0) a_cr = 1;
   return ekn_gevn * (Double_t)a_cr + m_gev;
}

//______________________________________________________________________________
Double_t TUPhysics::Ekn_to_Ek(Double_t const &ekn_gevn, Int_t a_cr)
{
   //--- Returns Ek from Ekn.
   //  Ek                Kinetic energy (Ek=E-m) of the particle [GeV]
   //  Ekn               Kinetic energy per nucleon (Ekn=Ek/n) of the particle [GeV/n]
   //  A                 Atomic number of the species (A=1 for leptons so that 'Ekn'(=Ek) exists)

   // A=1 for leptons so that 'Ekn'(=Ek) exists
   if (a_cr == 0) a_cr = 1;
   return ekn_gevn * (Double_t)a_cr;
}

//______________________________________________________________________________
Double_t TUPhysics::Ekn_to_p(Double_t const &ekn_gevn, Int_t a_cr, Double_t const &m_gev)
{
   //--- Returns p from Ekn.
   //  p                 Momentum of the particle [GeV]
   //  Ekn               Kinetic energy per nucleon (Ekn=Ek/n) of the particle [GeV/n]
   //  A                 Atomic number of the species (A=1 for leptons so that 'Ekn'(=Ek) exists)
   //  m_gev             Mass of the particle [GeV]

   // A=1 for leptons so that 'Ekn'(=Ek) exists
   if (a_cr == 0) a_cr = 1;
   Double_t ek_gev = ekn_gevn * (Double_t)a_cr;
   return sqrt(ek_gev * (ek_gev +  2. * m_gev));
}

//______________________________________________________________________________
Double_t TUPhysics::Ekn_to_R(Double_t const &ekn_gevn, Int_t a_cr, Double_t const &m_gev, Int_t z_cr)
{
   //--- Returns R from Ekn.
   //  R                 Rigidity (R=pc/Ze) of the particle [GV]
   //  Ekn               Kinetic energy per nucleon (Ekn=Ek/n) of the particle [GeV/n]
   //  A                 Atomic number of the species (A=1 for leptons so that 'Ekn'(=Ek) exists)
   //  Z                 Charge of the species

   // A=1 for leptons so that 'Ekn'(=Ek) exists
   if (a_cr == 0) a_cr = 1;
   Double_t ek_gev = ekn_gevn * (Double_t)a_cr;
   return sqrt(ek_gev * (ek_gev +  2. * m_gev)) / (Double_t)(abs(z_cr));
}

//______________________________________________________________________________
Double_t TUPhysics::Ek_to_E(Double_t const &ek_gev, Double_t const &m_gev)
{
   //--- Returns E from Ek.
   //  E                 Total energy of the particle [GeV]
   //  Ek                Kinetic energy (Ek=E-m) of the particle [GeV]
   //  m_gev             Mass of the particle [GeV]

   return ek_gev + m_gev;
}

//______________________________________________________________________________
Double_t TUPhysics::Ek_to_Ekn(Double_t const &ek_gev, Int_t a_cr)
{
   //--- Returns Ekn from Ek.
   //  Ekn               Kinetic energy per nucleon (Ekn=Ek/n) of the particle [GeV/n]
   //  Ek                Kinetic energy (Ek=E-m) of the particle [GeV]
   //  A                 Atomic number of the species (A=1 for leptons so that 'Ekn'(=Ek) exists)

   // A=1 for leptons so that 'Ekn'(=Ek) exists
   if (a_cr == 0) a_cr = 1;
   return ek_gev / (Double_t)a_cr;
}

//______________________________________________________________________________
Double_t TUPhysics::Ek_to_p(Double_t const &ek_gev, Double_t const &m_gev)
{
   //--- Returns p from Ek.
   //  p                 Momentum of the particle [GeV]
   //  Ek                Kinetic energy (Ek=E-m) of the particle [GeV]
   //  m_gev             Mass of the particle [GeV]

   return sqrt(ek_gev * (ek_gev +  2. * m_gev));
}

//______________________________________________________________________________
Double_t TUPhysics::Ek_to_R(Double_t const &ek_gev, Double_t const &m_gev, Int_t z_cr)
{
   //--- Returns R from Ek.
   //  R                 Rigidity (R=pc/Ze) of the particle [GV]
   //  Ek                Kinetic energy (Ek=E-m) of the particle [GeV]
   //  m_gev             Mass of the particle [GeV]
   //  Z                 Charge of the species

   return sqrt(ek_gev * (ek_gev +  2. * m_gev)) / (Double_t)(abs(z_cr));
}

//______________________________________________________________________________
Double_t TUPhysics::E_to_Ek(Double_t const &e_gev, Double_t const &m_gev)
{
   //--- Returns Ek from E.
   //  E                 Total energy of the particle [GeV]
   //  Ek                Kinetic energy (Ek=E-m) of the particle [GeV]
   //  m_gev             Mass of the particle [GeV]

   return e_gev - m_gev;
}

//______________________________________________________________________________
Double_t TUPhysics::E_to_Ekn(Double_t const &e_gev, Int_t a_cr, Double_t const &m_gev)
{
   //--- Returns Ekn from E.
   //  E                 Total energy of the particle [GeV]
   //  A                 Atomic number of the species (A=1 for leptons so that 'Ekn'(=Ek) exists)
   //  Ekn               Kinetic energy per nucleon (Ekn=Ek/n) of the particle [GeV/n]
   //  m_gev             Mass of the particle [GeV]

   // A=1 for leptons so that 'Ekn'(=Ek) exists
   if (a_cr == 0) a_cr = 1;
   return (e_gev -  m_gev) / (Double_t)a_cr;
}

//______________________________________________________________________________
Double_t TUPhysics::E_to_p(Double_t const &e_gev, Double_t const &m_gev)
{
   //--- Returns p from E.
   //  E                 Total energy of the particle [GeV]
   //  p                 Momentum of the particle [GeV]
   //  m_gev             Mass of the particle [GeV]

   return sqrt(e_gev * e_gev - m_gev * m_gev);
}
//______________________________________________________________________________
Double_t TUPhysics::E_to_R(Double_t const &e_gev, Double_t const &m_gev,
                           Int_t z_cr)
{
   //--- Returns R from E.
   //  E                 Total energy of the particle [GeV]
   //  m_gev             Mass of the particle [GeV]
   //  Z                 Charge of the species

   return sqrt(e_gev * e_gev - m_gev * m_gev) / (Double_t)(abs(z_cr));
}

//______________________________________________________________________________
Double_t TUPhysics::Gamma_Em(Double_t const &e_gev, Double_t const &m_gev)
{
   //--- Returns gamma lorentz of the particle (= 1/(1-beta^2) = E/m).
   //  E                 Total energy of the particle [GeV]
   //  m_gev             Mass of the particle [GeV]

   return e_gev / m_gev;
}

//______________________________________________________________________________
Double_t TUPhysics::Gamma_mAZR(Double_t const &m_gev, Int_t a_cr, Int_t z_cr, Double_t const &r_gv)
{
   //--- Returns gamma lorentz of the particle (= 1/(1-beta^2) = E/m).
   //  m_gev             Mass of the particle [GeV]
   //  A                 Atomic number of the species (A=1 for leptons so that 'Ekn'(=Ek) exists)
   //  Z                 Charge of the species
   //  R                 Rigidity (R=pc/Ze) of the particle [GV]

   // A=1 for leptons so that 'Ekn'(=Ek) exists
   if (a_cr == 0) a_cr = 1;
   Double_t e = R_to_E(r_gv, m_gev, z_cr);
   return e / m_gev;
}

//______________________________________________________________________________
Double_t TUPhysics::Gamma_mAEkn(Double_t const &m_gev, Int_t a_cr, Double_t const &ekn_gevn)
{
   //--- Returns gamma lorentz of the particle (= 1/(1-beta^2) = E/m).
   //  m_gev             Mass of the particle [GeV]
   //  A                 Atomic number of the species (A=1 for leptons so that 'Ekn'(=Ek) exists)
   //  Ekn               Kinetic energy per nucleon (Ekn=Ek/n) of the particle [GeV/n]
   //  E                 Total energy of the particle [GeV]

   // A=1 for leptons so that 'Ekn'(=Ek) exists
   if (a_cr == 0) a_cr = 1;
   return (ekn_gevn * (Double_t)a_cr + m_gev) / m_gev;
}

//______________________________________________________________________________
Double_t TUPhysics::p_to_Ek(Double_t const &p_gev, Double_t const &m_gev)
{
   //--- Returns Ek from p: Ek=E-m is the kinetic energy of the particle [GeV].
   //  p                 Momentum of the particle [GeV]
   //  m_gev             Mass of the particle [GeV]

   return sqrt(p_gev * p_gev + m_gev * m_gev) - m_gev;
}

//______________________________________________________________________________
Double_t TUPhysics::p_to_Ekn(Double_t const &p_gev, Int_t a_cr, Double_t const &m_gev)
{
   //--- Returns Ekn from p.
   //  Ekn               Kinetic energy per nucleon (Ekn=Ek/n) of the particle [GeV/n]
   //  p                 Momentum of the particle [GeV]
   //  A                 Atomic number of the species (A=1 for leptons so that 'Ekn'(=Ek) exists)
   //  m_gev             Mass of the particle [GeV]

   // A=1 for leptons so that 'Ekn'(=Ek) exists
   if (a_cr == 0) a_cr = 1;
   return (sqrt(p_gev * p_gev + m_gev * m_gev) - m_gev) / (Double_t)a_cr;
}

//______________________________________________________________________________
Double_t TUPhysics::p_to_E(Double_t const &p_gev, Double_t const &m_gev)
{
   //--- Returns E from p.
   //  E                 Total energy of the particle [GeV]
   //  p                 Momentum of the particle [GeV]
   //  m_gev             Mass of the particle [GeV]

   return sqrt(p_gev * p_gev + m_gev * m_gev);
}

//______________________________________________________________________________
Double_t TUPhysics::p_to_R(Double_t const &p_gev, Int_t z_cr)
{
   //--- Returns R from p.
   //  R                 Rigidity (R=pc/Ze) of the particle [GV]
   //  p                 Momentum of the particle [GeV]
   //  Z                 Charge of the species

   return p_gev / (Double_t)(abs(z_cr));
}

//______________________________________________________________________________
Double_t TUPhysics::R_to_Ek(Double_t const &r_gv, Double_t const &m_gev, Int_t z_cr)
{
   //--- Returns Ek from R: Ek=E-m is the kinetic energy of the particle [GeV].
   //  R                 Rigidity (R=pc/Ze) of the particle [GV]
   //  m_gev             Mass of the particle [GeV]
   //  Z                 Charge of the species

   Double_t p_gev = r_gv * (Double_t)(abs(z_cr));
   return sqrt(p_gev * p_gev + m_gev * m_gev) - m_gev;
}

//______________________________________________________________________________
Double_t TUPhysics::R_to_Ekn(Double_t const &r_gv, Int_t a_cr, Double_t const &m_gev, Int_t z_cr)
{
   //--- Returns Ekn from R.
   //  Ekn               Kinetic energy per nucleon (Ekn=Ek/n) of the particle [GeV/n]
   //  R                 Rigidity (R=pc/Ze) of the particle [GV]
   //  A                 Atomic number of the species (A=1 for leptons so that 'Ekn'(=Ek) exists)
   //  m_gev             Mass of the particle [GeV]
   //  Z                 Charge of the species

   // A=1 for leptons so that 'Ekn'(=Ek) exists
   if (a_cr == 0) a_cr = 1;
   Double_t p_gev = r_gv * (Double_t)(abs(z_cr));
   return (sqrt(p_gev * p_gev + m_gev * m_gev) - m_gev) / (Double_t)a_cr;
}

//______________________________________________________________________________
Double_t TUPhysics::R_to_E(Double_t const &r_gv, Double_t const &m_gev, Int_t z_cr)
{
   //--- Returns E from R.
   //  E                 Total energy of the particle [GeV]
   //  R                 Rigidity (R=pc/Ze) of the particle [GV]
   //  m_gev             Mass of the particle [GeV]
   //  Z                 Charge of the species

   Double_t p_gev = r_gv * (Double_t)(abs(z_cr));
   return sqrt(p_gev * p_gev + m_gev * m_gev);
}

//______________________________________________________________________________
Double_t TUPhysics::R_to_p(Double_t const &r_gv, Int_t z_cr)
{
   //--- Returns p from R.
   //  p                 Momentum of the particle [GeV]
   //  R                 Rigidity (R=pc/Ze) of the particle [GV]
   //  Z                 Charge of the species

   return r_gv * (Double_t)(abs(z_cr));
}

//______________________________________________________________________________
void TUPhysics::TEST(FILE *f)
{
   //--- Test function for this class (called in usine -t).
   //  f                 Output file for test

   string message = "The namespace TUPhysics contains\n"
                    "physics constants/conversion factors and more...";
   TUMessages::Test(f, "TUPhysics", message);

   Double_t e1 = 0.5;
   Int_t z = 2;
   Int_t a = 4;
   Double_t m = 4.00260325;
   fprintf(f, " * Circular calls E->R->p->beta->gamma->Ekn->E to test conversion functions (for CR=4He)\n");
   fprintf(f, "   - Ekn->E->Ek->p->R->Ekn: %f => ", e1);
   e1 = Ekn_to_E(e1, a, m);
   e1 = E_to_Ek(e1, m);
   e1 = Ek_to_p(e1, m);
   e1 = p_to_R(e1, z);
   e1 = R_to_Ekn(e1, a, m, z);
   fprintf(f, " %f\n", e1);

   fprintf(f, "   - Ekn->Ek->R->p->E->Ekn: %f => ", e1);
   e1 = Ekn_to_Ek(e1, a);
   e1 = Ek_to_R(e1, m, z);
   e1 = R_to_p(e1, z);
   e1 = p_to_E(e1, m);
   e1 = E_to_Ekn(e1, a, m);
   fprintf(f, " %f\n", e1);

   fprintf(f, "   - R->E->p->Ek->Ekn->R:   %f => ", e1);
   e1 = R_to_E(e1, m, z);
   e1 = E_to_p(e1, m);
   e1 = p_to_Ek(e1, m);
   e1 = Ek_to_Ekn(e1, a);
   e1 = Ekn_to_R(e1, a, m, z);
   fprintf(f, " %f\n", e1);

   fprintf(f, "   - R->Ek->Ekn->p->Ekn->R: %f => ", e1);
   e1 = R_to_Ek(e1, m, z);
   e1 = Ek_to_Ekn(e1, a);
   e1 = Ekn_to_p(e1, a, m);
   e1 = p_to_Ekn(e1, a, m);
   e1 = Ekn_to_R(e1, a, m, z);
   fprintf(f, " %f\n", e1);

   e1 = 4 + 0.5;
   fprintf(f, "   - E->p->Ekn->E:          %f => ", e1);
   e1 = E_to_p(e1, m);
   e1 = p_to_Ekn(e1, a, m);
   e1 = Ekn_to_E(e1, a, m);
   fprintf(f, " %f\n", e1);
   fprintf(f, "\n");

}

//______________________________________________________________________________
Double_t TUPhysics::Vcms_mAEkn(Double_t const &m_gev, Int_t a_cr, Double_t const &ekn_gevn)
{
   //--- Returns v (=beta/*c) [cm/s] from ekn.
   //  Ekn               Kinetic energy per nucleon (Ekn=Ek/n) of the particle [GeV/n]
   //  A                 Atomic number of the species (A=1 for leptons so that 'Ekn'(=Ek) exists)
   //  m_gev             Mass of the particle [GeV]

   // A=1 for leptons so that 'Ekn'(=Ek) exists
   if (a_cr == 0) a_cr = 1;
   Double_t beta = Beta_mAEkn(m_gev, a_cr, ekn_gevn);
   return C_cmpers() * beta;
}

//______________________________________________________________________________
Double_t TUPhysics::VkpcMyr_mAEkn(Double_t const &m_gev, Int_t a_cr, Double_t const &ekn_gevn)
{
   //--- Returns v (=beta/*c) [kpc/Myr] from ekn.
   //  Ekn               Kinetic energy per nucleon (Ekn=Ek/n) of the particle [GeV/n]
   //  A                 Atomic number of the species (A=1 for leptons so that 'Ekn'(=Ek) exists)
   //  m_gev             Mass of the particle [GeV]

   // A=1 for leptons so that 'Ekn'(=Ek) exists
   if (a_cr == 0) a_cr = 1;
   Double_t beta = Beta_mAEkn(m_gev, a_cr, ekn_gevn);
   return C_cmpers() * Convert_cmpers_to_kpcperMyr() * beta;
}

//______________________________________________________________________________
Double_t TUPhysics::Vms_mAEkn(Double_t const &m_gev, Int_t a_cr, Double_t const &ekn_gevn)
{
   //--- Returns v (=beta/*c) [cm/s] from ekn.
   //  Ekn               Kinetic energy per nucleon (Ekn=Ek/n) of the particle [GeV/n]
   //  A                 Atomic number of the species (A=1 for leptons so that 'Ekn'(=Ek) exists)
   //  m_gev             Mass of the particle [GeV]

   // A=1 for leptons so that 'Ekn'(=Ek) exists
   if (a_cr == 0) a_cr = 1;
   Double_t beta = Beta_mAEkn(m_gev, a_cr, ekn_gevn);
   return C_mpers() * beta;
}
