// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

// C/C++ include
// ROOT include
// USINE include
#include "../include/TUSolModVirtual.h"

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TUSolModVirtual                                                      //
//                                                                      //
// Pure virtual (abstract) class for Solar modulation models.           //
//                                                                      //
// The abstract class TUSolModVirtual provides a virtual class for all  //
// Solar modulation models (defined to be the transport equation of     //
// charged cosmic rays in the Solar cavity), which must derive from it. //
// This abstract class allows to use - for instance in the cosmic-ray   //
// propagation module - a 'generic' object created from any existing    //
// Solar modulation class (e.g., Force-Field in TUSolMod0DFF.h,         //
// spherically symmetric model in TUSolMod1DSpher. It allows to         //
// modulate the IS (interstellar) flux into the TOA (top-of-atmosphere) //
// flux at 1 AU.                                                        //
//                                                                      //
// The virtual methods (we remind that models deriving from this        //
// class must have them at least) of the class are:                     //
//   - GetModel() returns the model in use (gENUM_SOLMOD_MODEL);
//   - GetFreePars() returns the model free parameters;
//   - IStoTOA() modulates fluxes;
//   - SetClass() initialise the class.
//                                                                      //
// N.B.: for IStoTOA() method, a cosmic ray and an Ekn energy grid      //
// must be specified (we force the TOA flux to be calculated on the     //
// same energy grid as the IS flux).                                    //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

ClassImp(TUSolModVirtual)

//______________________________________________________________________________
TUSolModVirtual::TUSolModVirtual()
{
   // ****** Default constructor ******

}

//______________________________________________________________________________
TUSolModVirtual::~TUSolModVirtual()
{
   // ****** Default destructor ******
}

//______________________________________________________________________________
void TUSolModVirtual::ParsModExps_Create(TUDataSet *data)
{
   //--- Creates as many sets of Sol.mod. parameters as the number of exp in data.
   //    We recall that an exp uniquely determines a data taking period, i.e. no overlap
   //    between different exp by construction in the CRDB (http::/lpsc.in2p3.fr/crdb).
   //  data          Data containing in particular the exp

   // Reset
   ParsModExps_Delete();

   // If no data, nothing to do
   if (data->GetNData() == 0)
      return;

   // Clone and push in fParsExps using as new parameter names
   // (for each 'par' in the sol mod class) 'par_expname'
   for (Int_t i_exp = 0; i_exp < data->GetNExps(); ++i_exp) {
      TUFreeParList *new_pars = GetFreePars()->Clone();

      // Update name of parameter
      for (Int_t j_par = 0; j_par < new_pars->GetNPars(); ++j_par) {
         TUFreeParEntry *par = new_pars->GetParEntry(j_par);
         string par_name = par->GetName() + "_" + data->GetExp(i_exp);
         TUMisc::RemoveSpecialChars(par_name);

         // If model is FF, initialise parameter value from data value
         if (GetModel() == kSOLMOD0DFF) {
            // Loop on all possible e-types for this experiment
            for (Int_t e = 0; e < gN_ETYPE; ++e) {
               gENUM_ETYPE e_type = TUEnum::gENUM_ETYPE_LIST[e];
               Int_t i_data = data->IndexData(i_exp, e_type);
               if (i_data >= 0) {
                  par->SetPar(par_name, "GV", data->GetEntry(i_data)->GetExpphi());
                  break;
               }
            }
         } else {
            string message = "Nuisance parameters for model other than kSOLMOD0DFF not implemented yet";
            TUMessages::Error(stdout, "TUSolModVirtual", "ParsModExps_Create", message);
         }
         par = NULL;
      }
      fParsExps.push_back(new_pars);
   }

   // Copy in the last entry the 'base' parameters
   TUFreeParList *pars = this->TUFreeParList::Clone();
   fParsExps.push_back(pars);
}

//______________________________________________________________________________
void TUSolModVirtual::ParsModExps_Delete()
{
   //--- Empties fParsExps and restore parameter name to their value.

   // Delete all parameters
   if (ParsModExps_GetN() > 0) {
      // Last entry used to reset initial parameters
      this->TUFreeParList::Copy(*fParsExps[ParsModExps_GetN()]);

      // Delete each parameter in vector
      for (Int_t i = 0; i < (Int_t)fParsExps.size(); ++i) {
         delete fParsExps[i];
         fParsExps[i] = NULL;
      }
      fParsExps.clear();
   }
}

//______________________________________________________________________________
Int_t TUSolModVirtual::ParsModExps_Index(string exp_name) const
{
   //--- Returns index (which exp) whose any par matches par_name (-1 if not found).
   //  par_name      Name of parameter sought

   if (ParsModExps_GetN() == 0)
      return -1;

   // In par name, exp name is trimmed (see in ParsModExps_Create above)
   TUMisc::RemoveSpecialChars(exp_name);

   for (Int_t i = 0; i < ParsModExps_GetN(); ++i) {
      // We just test the 0-th par
      if (fParsExps[i]->GetParEntry(0)->GetName().find(exp_name) != string::npos)
         return i;
   }
   return -1;
}

//______________________________________________________________________________
void TUSolModVirtual::ParsModExps_Indices(string const &par_name, Int_t &i_exp, Int_t &i_par) const
{
   //--- Looks for indices (which exp and which modulation parameter for this index) matching par_name.
   // INPUT:
   //  par_name      Name of parameter sought
   // OUTPUTS:
   //  i_exp         Index of exp matching par_name (-1 if not found)
   //  i_par         Index of sol.mod. par matching name (-1 if not found)

   i_exp = -1;
   i_par = -1;
   if (ParsModExps_GetN() == 0)
      return;

   for (Int_t i = 0; i < ParsModExps_GetN(); ++i) {
      for (Int_t j = 0; j < fParsExps[i]->GetNPars(); ++j) {
         if (fParsExps[i]->GetParEntry(j)->GetName() == par_name) {
            i_exp = i;
            i_par = j;
            return;
         }
      }
   }
   return;
}