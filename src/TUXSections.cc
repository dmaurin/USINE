// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

// C/C++ include
#include <fstream>
// ROOT include
// DAVID ROOT
#include <TGraph.h>
#include <TLegend.h>
#include <TLegendEntry.h>
#include <TList.h>
#include <TMultiGraph.h>
#include <TStyle.h>
// USINE include
#include "../include/TUInteractions.h"
#include "../include/TUMath.h"
#include "../include/TUNumMethods.h"
#include "../include/TUXSections.h"

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TUXSections                                                          //
//                                                                      //
// Energy-dependent nuclear cross-sections (total, diff., inel., etc.). //
//                                                                      //
// The class TUXSections provides vectors and matrices to store         //
// nuclear cross-sections from USINE-formatted X-sec files). All the    //
// reactions correspond to the case where an energetic CR interact      //
// with a target nucleus (e.g., H and He in the ISM) at rest. The       //
// basic requirement for such a class is that is has:                   //
//    - a list of CRs,
//    - a list of energies,
// which is exactly what is provided by the class TUAxesCrE, and
//    - a list of targets;
//    - vectors for each type of X-sec (detailed below);
//    - checks to ensure that no nuclear X-sec is missing
//      after the initialisation stage.
//                                                                      //
// * Note that the number of CRs (NCRs) and parents, the number of      //
//   targets (NTarg), and the number of energies (NE) are  all          //
//   dynamically allocated: it complicates a bit the code, but this     //
//   extra layer (flexibility to add another target, other CRs, etc.)   //
//   remains hidden to the user.                                        //
//                                                                      //
// * This is a read only class, i.e. it does not compute X-sec of       //
//   any kind (such a piece of code exists elsewhere, but it is not     //
//   distributed). You can use any file or cross-section data-set, as   //
//   long as the format of the file is USINE compliant (see, e.g.,      //
//   the files in $USINE_XSECDATA). In particular, the class will do    //
//   the job even if the targets (usually H and He) are changed, as     //
//   long as the corresponding X-sec are provided in the loaded file.   //
//                                                                      //
// * How does the class work? It reads as many Xsec-like files as       //
//   specified by the user (or say defined in the initialisation        //
//   parameter file), and then:                                         //
//    - if the same reaction appears twice, it replaces the value
//      with the one found in the last file read;
//    - after reading all the files, the method IsAllReacFilled()
//      is called to check that no reaction is missing.
//                                                                      //
//                                                                      //
// We now detail below the various X-sec handled by this class. We      //
// start with a description of the various possible processes, and      //
// then we briefly comment on how this is implemented in the class.     //
//                                                                      //
// * Acronyms:                                                          //
//    - IA: Inelastic Annihilating;
//    - INA: Inelastic Non-annihilating (sometimes denotes
//           NAR for non-annihilating rescattering);
//    - SAA: Straight-ahead approximation.
//                                                                      //
// * What is the difference between a total and a differential X-sec?   //
// A differential cross-section describes the probability of the        //
// interaction depending on the incoming and outgoing energy, whereas   //
// a total Xsec integrates out the outgoing energies (so that they only //
// depend on the incoming energy).                                      //
//                                                                      //
// * What is a INA reaction? For instance, it involves an anti-proton   //
//   having an inelastic reaction (i.e if the two-body reaction is A+B, //
//   the outgoing particles are not A+B), but which survives nonetheless//
//   in the collision, having lost some fraction of its energy:         //
//       A + B  -> A* + B + C                                           //
//   where A* is the surviving A particle (the energy of A* is less     //
//   than that of A). This is a differential cross-section, so that     //
//   it both requires an incoming and an outgoing energy.               //
//                                                                      //
// * What is a production X-sec? A production X-sec corresponds to      //
//   a reaction A+B->C+X with                                           //
//    - A is the incoming particle with energy Ekn;
//    - B is the target at rest in the laboratory;
//    - C is the outgoing product of the reaction with energy Ekn.
//   The inclusive cross-section takes into account all reactions       //
//   leading to C (X can be anything).                                  //
//                                                                      //
//                                                                      //
// I. Total IA and INA X-sec on NTarg [mb]                              //
//                                                                      //
// Total cross-sections describe the nuclear reaction of a CR on        //
// a target, for the following processes (in USINE):                    //
//    - Inel.Ann (IA): CR + Targ -> X (not CR);
//    - Inel.Non-Ann. (INA): CR(Ekn) + Targ -> CR (@ any Ekn'<Ekn).
//                                                                      //
// Hence, any total X-sec requires a matrix of [NTarg*NCRs*NE] values.  //
// Note however, that not all CRs have an INA X-sec. In that case,      //
// only the sub-sample of NTert reactions (instead of NCRs) are stored. //
//                                                                      //
// II. Production X-sec on NTarg from all the CR parents                //
//                                                                      //
// Production X-sec are generally differential X-sec. However, for      //
// nuclei, the straight-ahead approximation (SAA) is a good first-order //
// description of what happens. Hence, in the class, all X-sec can      //
// be filled by either one or the other description:                    //
//    - differential [mb/GeV/n]: Proj(Ekn) + Targ -> Frag(Ekn'<Ekn) + X;
//    - SAA [mb]: Proj(Ekn) + Targ -> Frag(Ekn'=Ekn) + X.
//                                                                      //
// In other words, depending on the reaction, the class reads, e.g.,    //
// 'sigma' (e.g. SAA for the 12C->10B reaction) and 'dsigma' (e.g.      //
// for the 1H+H->1H-bar reaction), and this is dealt with automatically //
// in the routine that calculate secondaries. For the SAA case, only    //
// one energy grid is required, and this is simply the CR energy grid.  //
// For differential production, the projectile and fragment energy      //
// grids must be provided. In that case, the projectile grid usually    //
// needs to cover a larger energy range than the fragment grid. Indeed, //
// a 10 GeV 1H-bar are created by >100 GeV CR protons. This is why, in  //
// fCrE, different energy grids for the different families (kNUC,       //
// kANTINUC, and kLEPTON...) are allowed. The constraint is for all     //
// the grids to have the same number of bins.                           //
//                                                                      //
// III. INA differential cross sections [mb/GeV/n]                      //
//                                                                      //
// For the INA mechanism, the incoming and outgoing particle is one     //
// and the same, so that in this case the incoming and outgoing CR      //
// energies share the same energy grid.                                 //
//                                                                      //
// BEGIN_HTML
// <b>To display XS data: <tt>$USINE/bin/usine -ix</tt>)
// END_HTML
//////////////////////////////////////////////////////////////////////////

ClassImp(TUXSections)

//______________________________________________________________________________
TUXSections::TUXSections()
{
   // ****** Default constructor ******
   //

   Initialise(false);
   ParsXS_Initialise(false);
}

//______________________________________________________________________________
TUXSections::TUXSections(TUXSections const &xsec)
{
   // ****** Copy constructor ******
   //  xsec              Object to copy from

   Initialise(false);
   ParsXS_Initialise(false);
   Copy(xsec);
}

//______________________________________________________________________________
TUXSections::TUXSections(TUInitParList *init_pars, Bool_t is_verbose, FILE *f_log, TUAxesCrE *use_axescre)
{
   // ****** Normal constructor (from list of parameters) ******
   //  init_pars         TUInitParList class of initialisation parameters
   //  is_verbose        Verbose or not when class is set
   //  use_axescre       If Non-NULL, does not create fCrE but points to an existing one 'use_axescre'
   //  f_log             Log for USINE

   Initialise(false);
   ParsXS_Initialise(false);
   SetClass(init_pars, is_verbose, f_log, use_axescre);
}

//______________________________________________________________________________
TUXSections::~TUXSections()
{
   // ****** Default destructor ******
   //

   Initialise(true);
   ParsXS_Initialise(true);
}

//______________________________________________________________________________
void TUXSections::AllocateAndInitialiseXSec()
{
   //--- Allocates memory and initialises all X-sec related elements to -1.

   // Check
   if (IsTargetOrCROrEknEmpty(stdout))
      TUMessages::Error(stdout, "TUXSections", "AllocateAndInitialiseXSec",
                        "List of targets, or CRs, or Ekn grid is empty, fill it first!");

   // Delete Optimisation variable, because calling this function
   // means X-sec have changed
   DeleteProdRate_ISM0D();

   if (!fCrE)
      return;

   // Allocates and initialises...
   // fTotIA and fIsTotIAFilled
   if (!fTotIA)
      fTotIA = new Double_t[GetNTargets() * GetNCRs() * GetNE()];
   if (!fIsTotIAFilled)
      fIsTotIAFilled = new Bool_t[GetNCRs()];
   for (Int_t k =  GetNTargets() * GetNCRs() * GetNE() - 1; k >= 0; --k) {
      fTotIA[k] = -1.;
      if (k < GetNCRs())
         fIsTotIAFilled[k] = false;
   }

   // fTotINA and fIsTotINAFilled
   if (!fTotINA)
      fTotINA = new Double_t[GetNTargets() * GetNTertiaries() * GetNE()];
   if (!fIsTotINAFilled)
      fIsTotINAFilled = new Bool_t[GetNTertiaries()];
   for (Int_t k = GetNTargets() * GetNTertiaries() * GetNE() - 1; k >= 0; --k) {
      fTotINA[k] = -1.;
      if (k < GetNTertiaries())
         fIsTotINAFilled[k] = false;
   }

   // fDSigDEkINA and fIsDSigDEkINAFilled
   if (!fDSigDEkINA)
      fDSigDEkINA = new Double_t[GetNTargets() * GetNTertiaries() * GetNE() * GetNE()];
   if (!fIsDSigDEkINAFilled)
      fIsDSigDEkINAFilled = new Bool_t[GetNTertiaries()];
   for (Int_t k = GetNTargets() * GetNTertiaries() * GetNE() * GetNE() - 1; k >= 0; --k) {
      fDSigDEkINA[k] = -1.;
      if (k < GetNTertiaries())
         fIsDSigDEkINAFilled[k] = false;
   }


   // fProdDSigDEk, fProdSig, and fProdStatus
   if (!fProdDSigDEk && !fProdSig && !fProdStatus) {
      fProdDSigDEk = new Double_t **[GetNTargets() * GetNCRs()];
      fProdSig = new Double_t **[GetNTargets() * GetNCRs()];
      for (Int_t i = 0; i < GetNTargets() * GetNCRs(); ++i) {
         fProdDSigDEk[i] = NULL;
         fProdSig[i] = NULL;
      }
      fProdStatus = new Int_t*[GetNCRs()];
      // For each cr, allocate according to the number of parents
      for (Int_t j_cr = 0; j_cr < GetNCRs(); ++j_cr) {
         fIsTotIAFilled[j_cr] = false;
         fProdStatus[j_cr] = NULL;
         // Check whether there is a parent for j_cr
         // (otherwise, it was not allocated)
         if (fCrE->GetNParents(j_cr) <= 0)
            continue;
         // Initialise fProdStatus to -1 (X-sections not filled yet)
         fProdStatus[j_cr] = new Int_t[fCrE->GetNParents(j_cr)];
         for (Int_t j_parent = fCrE->GetNParents(j_cr) - 1; j_parent >= 0; --j_parent)
            fProdStatus[j_cr][j_parent] = -1;
         // Allocate (but for energy) and set to NULL
         for (Int_t t_targ = 0; t_targ < GetNTargets(); ++t_targ) {
            fProdDSigDEk[t_targ * GetNCRs() + j_cr] = new Double_t *[fCrE->GetNParents(j_cr)];
            fProdSig[t_targ * GetNCRs() + j_cr] = new Double_t *[fCrE->GetNParents(j_cr)];
            for (Int_t k = 0; k < fCrE->GetNParents(j_cr); ++k) {
               fProdDSigDEk[t_targ * GetNCRs() + j_cr][k] = NULL;
               fProdSig[t_targ * GetNCRs() + j_cr][k] = NULL;
            }
         }
      }
   }

   // And for ghosts
   if (fCrE && fCrE->IsGhostsLoaded() && !fGhostsProdDSigDEk && !fGhostsProdSig && !fGhostsProdStatus) {
      fGhostsProdDSigDEk = new Double_t **[GetNTargets() * GetNCRs()];
      fGhostsProdSig = new Double_t **[GetNTargets() * GetNCRs()];
      for (Int_t i = 0; i < GetNTargets() * GetNCRs(); ++i) {
         fGhostsProdDSigDEk[i] = NULL;
         fGhostsProdSig[i] = NULL;
      }
      fGhostsProdStatus = new Int_t*[GetNCRs()];
      // For each cr, allocate according to the number of parents
      for (Int_t j_cr = 0; j_cr < GetNCRs(); ++j_cr) {
         fGhostsProdStatus[j_cr] = NULL;
         // Check whether there is a parent for j_cr
         // (otherwise, it was not allocated)
         if (fCrE->GetNParents(j_cr) <= 0)
            continue;
         fGhostsProdStatus[j_cr] = new Int_t[fCrE->GetNParents(j_cr)*fCrE->GetCREntry(j_cr).GetNGhosts()];
         for (Int_t jj = fCrE->GetNParents(j_cr) * fCrE->GetCREntry(j_cr).GetNGhosts() - 1; jj >= 0; --jj)
            fGhostsProdStatus[j_cr][jj] = -1;

         // Allocate (but for energy) and set to NULL
         for (Int_t t_targ = 0; t_targ < GetNTargets(); ++t_targ) {
            fGhostsProdDSigDEk[t_targ * GetNCRs() + j_cr] = new Double_t *[fCrE->GetNParents(j_cr)*fCrE->GetCREntry(j_cr).GetNGhosts()];
            fGhostsProdSig[t_targ * GetNCRs() + j_cr] = new Double_t *[fCrE->GetNParents(j_cr)*fCrE->GetCREntry(j_cr).GetNGhosts()];
            for (Int_t k = 0; k < fCrE->GetNParents(j_cr)*fCrE->GetCREntry(j_cr).GetNGhosts(); ++k) {
               fGhostsProdDSigDEk[t_targ * GetNCRs() + j_cr][k] = NULL;
               fGhostsProdSig[t_targ * GetNCRs() + j_cr][k] = NULL;
            }
         }
      }
   }
}

//______________________________________________________________________________
void TUXSections::AllocateProdDSigAllTargets(Int_t j_parent, Int_t j_cr, Int_t g_ghost)
{
   //--- Allocates 3rd dimension (energies) and initialises to 0. X-Sec values
   //    for fProdDSigDEk[nTargets*j_cr][j_parent][GetNE()*GetNE()] or for
   //    fGhostsProdDSigDEk if g_ghost>-1.
   //  j_parent          Parent index for CR j_cr
   //  j_cr              CR index
   //  g_ghost           Ghost index (-1 if not a ghost)

   // Delete Optimisation variable, because calling
   // this function means X-sec have changed
   DeleteProdRate_ISM0D();

   // Allocate dSigProd for Proj->CR
   if (g_ghost == -1) {
      for (Int_t t_targ = 0; t_targ < GetNTargets(); ++ t_targ) {
         if (!fProdDSigDEk[t_targ * GetNCRs() + j_cr][j_parent])
            fProdDSigDEk[t_targ * GetNCRs() + j_cr][j_parent] = new Double_t[GetNE() * GetNE()];
         for (Int_t k = GetNE() * GetNE() - 1; k >= 0; --k)
            fProdDSigDEk[t_targ * GetNCRs() + j_cr][j_parent][k] = 0.;
      }
   } else if (fCrE && fCrE->IsGhostsLoaded()) {
      for (Int_t t_targ = 0; t_targ < GetNTargets(); ++ t_targ) {
         if (!fGhostsProdDSigDEk[t_targ * GetNCRs() + j_cr][j_parent * fCrE->GetCREntry(j_cr).GetNGhosts() + g_ghost])
            fGhostsProdDSigDEk[t_targ * GetNCRs() + j_cr][j_parent * fCrE->GetCREntry(j_cr).GetNGhosts() + g_ghost] = new Double_t[GetNE()*GetNE()];
         for (Int_t k = GetNE() * GetNE() - 1; k >= 0; --k)
            fGhostsProdDSigDEk[t_targ * GetNCRs() + j_cr][j_parent * fCrE->GetCREntry(j_cr).GetNGhosts() + g_ghost][k] = 0.;
      }
   }
}

//______________________________________________________________________________
void TUXSections::AllocateProdSigAllTargets(Int_t j_parent, Int_t j_cr, Int_t g_ghost)
{
   //--- Allocates 3rd dimension (energies) and initialises to 0. X-Sec values
   //    for fProdSig[nTargets*j_cr][j_parent][GetNE()]  or for
   //    fGhostsProdDSigDEk if g_ghost>-1.
   //  j_parent          Parent index for CR j_cr
   //  j_cr              CR index
   //  g_ghost           Ghost index (-1 if not a ghost)

   // Delete Optimisation variable, because calling
   // this function means X-sec have changed
   DeleteProdRate_ISM0D();

   // Allocate SigProd for Proj->CR
   if (g_ghost == -1) {
      for (Int_t t_targ = 0; t_targ < GetNTargets(); ++t_targ) {
         if (!fProdSig[t_targ * GetNCRs() + j_cr][j_parent])
            fProdSig[t_targ * GetNCRs() + j_cr][j_parent] = new Double_t[GetNE()];
         for (Int_t k = GetNE() - 1; k >= 0; --k)
            fProdSig[t_targ * GetNCRs() + j_cr][j_parent][k] = 0.;
      }
   } else if (fCrE->IsGhostsLoaded()) {
      for (Int_t t_targ = 0; t_targ < GetNTargets(); ++t_targ) {
         if (!fGhostsProdSig[t_targ * GetNCRs() + j_cr][j_parent * fCrE->GetCREntry(j_cr).GetNGhosts() + g_ghost])
            fGhostsProdSig[t_targ * GetNCRs() + j_cr][j_parent * fCrE->GetCREntry(j_cr).GetNGhosts() + g_ghost] = new Double_t[GetNE()];
         for (Int_t k = GetNE() - 1; k >= 0; --k)
            fGhostsProdSig[t_targ * GetNCRs() + j_cr][j_parent * fCrE->GetCREntry(j_cr).GetNGhosts() + g_ghost][k] = 0.;
      }
   }
}

//______________________________________________________________________________
void TUXSections::Copy(TUXSections const &xsec)
{
   //--- Copies xsec in current class.
   //  xsec              Object to copy from

   Initialise(true);

   Int_t n_crs = xsec.GetNCRs();
   Int_t n_targ = xsec.GetNTargets();
   Int_t n_tert = xsec.GetNTertiaries();
   Int_t n_e = xsec.GetNE();

   // Copy CR and E axes
   fIsDeleteCrE = xsec.IsDeleteCrE();
   if (fIsDeleteCrE) {
      TUAxesCrE *cre = xsec.GetAxesCrE();
      if (cre)
         fCrE = cre->Clone();
      cre = NULL;
   } else
      fCrE = xsec.GetAxesCrE();
   // Copy free pars
   fIsDeleteFreePars = xsec.IsDeleteFreePars();
   if (fIsDeleteFreePars) {
      TUFreeParList *pars = xsec.GetFreePars();
      if (pars)
         fFreePars = pars->Clone();
      pars = NULL;
   } else
      fFreePars = xsec.GetFreePars();

   // Copy file names
   for (Int_t i = 0; i < xsec.GetNFilesXDiffINA(); ++i)
      fFilesXDiffINA.push_back(xsec.GetFileXDiffINA(i));
   for (Int_t i = 0; i < xsec.GetNFilesXProd(); ++i)
      fFilesXProd.push_back(xsec.GetFileXProd(i));
   for (Int_t i = 0; i < xsec.GetNFilesXTotIA(); ++i)
      fFilesXTotIA.push_back(xsec.GetFileXTotIA(i));
   for (Int_t i = 0; i < xsec.GetNFilesXTotINA(); ++i)
      fFilesXTotINA.push_back(xsec.GetFileXTotINA(i));

   // Copy targets and tertiaries
   fTargets.clear();
   for (Int_t i = 0; i < n_targ; ++i)
      fTargets.push_back(xsec.GetNameTarget(i));
   fTertiaries.clear();
   for (Int_t i = 0; i < n_tert; ++i)
      fTertiaries.push_back(xsec.GetNameTertiary(i));


   // WARNING
   // -> do not copy members related to nuisance parameters!
   // -> do not copy members related to linear combination XS!
   //// Deal with ParsXS_... parameters
   //ParsXS_Initialise(true);
   //fParsXS_NProd = xsec.ParsXS_GetNProd();
   //fParsXS_NTotIA = xsec.ParsXS_GetNTotIA();
   //Int_t n_reac = xsec.ParsXS_GetNReac();
   //if (n_reac > 0) {
   //   fParsXS_RefProdDSigDEk = new Double_t[n_reac * GetNE()*GetNE()];
   //   fParsXS_RefProdSig = new Double_t[n_reac * GetNE()];
   //   fParsXS_RefTotIA = new Double_t[n_reac * GetNE()];
   //   for (Int_t r_reac = 0; r_reac < n_reac; ++r_reac) {
   //      fParsXS_ReacIndices.push_back(xsec.ParsXS_GetReacIndices(r_reac));
   //      for (Int_t k_in = 0; k_in < GetNE(); ++k_in) {
   //         ParsXS_SetRefTotIA(r_reac, k_in, xsec.ParsXS_GetRefTotIA(r_reac, k_in));
   //         ParsXS_SetRefProdSig(r_reac, k_in, xsec.ParsXS_GetRefProdSig(r_reac, k_in));
   //         for (Int_t k_out = 0; k_out < GetNE(); ++k_out)
   //            ParsXS_SetRefProdDSigDEk(r_reac, k_in, k_out, ParsXS_GetRefProdDSigDEk(r_reac, k_in, k_out));
   //      }
   //   }
   //}

   // All X-sections are allocated
   // Allocate and fill/initialise
   AllocateAndInitialiseXSec();
   fIsAtLeast1DiffProd = xsec.IsAtLeast1DiffProd();

   // Copy checks for X-sections and mapping
   for (Int_t i = 0; i < n_tert; ++i)
      fIsDSigDEkINAFilled[i] = xsec.IsDSigDEkINAFilled(i);
   for (Int_t i = 0; i < n_tert; ++i)
      fIsTotINAFilled[i] = xsec.IsTotINAFilled(i);
   for (Int_t i = 0; i < n_crs; ++i)
      fIsTotIAFilled[i] = xsec.IsTotIAFilled(i);

   if (n_tert > 0) {
      if (!fMapINAIndex2CRIndex)
         fMapINAIndex2CRIndex = new Int_t[n_tert];
      for (Int_t i = 0; i < n_tert; ++i)
         fMapINAIndex2CRIndex[i] = xsec.MapINAIndex2CRIndex(i);
   }
   if (n_crs > 0) {
      if (!fMapCRIndex2INAIndex)
         fMapCRIndex2INAIndex = new Int_t[n_crs];
      for (Int_t i = 0; i < n_crs; ++i)
         fMapCRIndex2INAIndex[i] = xsec.MapCRIndex2INAIndex(i);
   }

   // Copy X-sections
   // fTotIA[NCRs*NTargets*NE]
   for (Int_t j_cr = 0; j_cr < n_crs; ++j_cr) {
      fIsTotIAFilled[j_cr] = xsec.IsTotIAFilled(j_cr);
      for (Int_t t_targ = 0; t_targ < n_targ; ++t_targ)
         for (Int_t k = 0; k < n_e; ++k)
            fTotIA[t_targ * GetNCRs()*GetNE() + j_cr * GetNE() + k]
               = xsec.GetTotIA(j_cr, t_targ, k);
   }
   // fTotINA[NTertiaries*NTargets*NE]
   for (Int_t j_tert = 0; j_tert < n_tert; ++j_tert) {
      fIsTotINAFilled[j_tert] = xsec.IsTotINAFilled(j_tert);
      for (Int_t t_targ = 0; t_targ < n_targ; ++t_targ)
         for (Int_t k = 0; k < n_e; ++k)
            fTotINA[t_targ * GetNTertiaries()*GetNE() + j_tert * GetNE() + k]
               = xsec.GetTotINA(j_tert, t_targ, k);
   }
   // fDSigDEknINA[NTertiaries*NTargets*NE_in*NE_out]
   for (Int_t j_tert = 0; j_tert < n_tert; ++j_tert) {
      fIsDSigDEkINAFilled[j_tert] = xsec.IsDSigDEkINAFilled(j_tert);
      for (Int_t t_targ = 0; t_targ < n_targ; ++t_targ)
         for (Int_t k_in = 0; k_in < n_e; ++k_in)
            for (Int_t k_out = 0; k_out < n_e; ++k_out)
               fDSigDEkINA[t_targ * GetNTertiaries()*GetNE()*GetNE() + j_tert * GetNE()*GetNE() + k_out * GetNE() + k_in] =
                  xsec.GetDSigDEkINA(j_tert, t_targ, k_in, k_out);
   }
   // Prod (fProdSig or fProdDSigDEk depending on fProdSig)
   for (Int_t j_cr = 0; j_cr < n_crs; ++j_cr) {
      // Loop over CR and all its ghosts if enabled
      Int_t g_max = -1;
      if (fCrE->IsGhostsLoaded())
         g_max = fCrE->GetCREntry(j_cr).GetNGhosts() - 1;
      for (Int_t j_parent = fCrE->GetNParents(j_cr) - 1; j_parent >= 0; --j_parent) {
         for (Int_t g_ghost = g_max; g_ghost >= -1; --g_ghost) {
            SetProdStatus(j_parent, j_cr, xsec.GetProdStatus(j_parent, j_cr, g_ghost), g_ghost);

            if (GetProdStatus(j_parent, j_cr, g_ghost) == 1 || GetProdStatus(j_parent, j_cr, g_ghost) == 11) {
               AllocateProdDSigAllTargets(j_parent, j_cr, g_ghost);
               for (Int_t t_targ = 0; t_targ < GetNTargets(); ++ t_targ) {
                  for (Int_t k_in = 0; k_in < n_e; ++k_in) {
                     for (Int_t k_out = 0; k_out < n_e; ++k_out)
                        SetProdDSigDEk(j_parent, t_targ, j_cr, k_in, k_out,
                                       xsec.GetProdDSigDEk(j_parent, t_targ, j_cr, k_in, k_out, g_ghost), g_ghost);
                  }
               }
            } else if (GetProdStatus(j_parent, j_cr, g_ghost) == 0 || GetProdStatus(j_parent, j_cr, g_ghost) == 10) {
               AllocateProdSigAllTargets(j_parent, j_cr, g_ghost);
               for (Int_t t_targ = 0; t_targ < GetNTargets(); ++ t_targ) {
                  for (Int_t k = 0; k < n_e; ++k)
                     SetProdSig(j_parent, t_targ, j_cr, k,
                                xsec.GetProdSig(j_parent, t_targ, j_cr, k, g_ghost), g_ghost);
               }
            }
         }
      }
   }

   // Fill ISM0D quantities
   TUMediumEntry *ism = xsec.GetMedium_ISM0D();
   if (ism)
      FillInterRate_ISM0D(ism, true);
   ism = NULL;
}

//______________________________________________________________________________
void TUXSections::DeleteProdRate_ISM0D()
{
   //--- Free memory for production rate variable (OD ISM).

   // Free fnvDSigDEkINA_ISM0D[NTert*NE*NE]
   if (fnvDSigDEkINA_ISM0D)
      delete[] fnvDSigDEkINA_ISM0D;
   fnvDSigDEkINA_ISM0D = NULL;

   // Free fnvSigTotIA_ISM0D[NCRs*NE]
   if (fnvSigTotIA_ISM0D)
      delete[] fnvSigTotIA_ISM0D;
   fnvSigTotIA_ISM0D = NULL;

   // Free fnvSigTotINA_ISM0D[NTert*NE]
   if (fnvSigTotINA_ISM0D)
      delete[] fnvSigTotINA_ISM0D;
   fnvSigTotINA_ISM0D = NULL;

   // Free fnvDSigDEkProd_ISM0D[NCRs][NParents(CR)][NE*NE]
   if (fnvDSigDEkProd_ISM0D) {
      for (Int_t j_cr = 0; j_cr < GetNCRs(); ++j_cr) {
         for (Int_t j_parent = 0; j_parent < fCrE->GetNParents(j_cr); ++j_parent) {
            if (fnvDSigDEkProd_ISM0D[j_cr][j_parent]) delete[] fnvDSigDEkProd_ISM0D[j_cr][j_parent];
            fnvDSigDEkProd_ISM0D[j_cr][j_parent] = NULL;
         }
         if (fnvDSigDEkProd_ISM0D[j_cr]) delete[] fnvDSigDEkProd_ISM0D[j_cr];
         fnvDSigDEkProd_ISM0D[j_cr] = NULL;
      }
      if (fnvDSigDEkProd_ISM0D) delete[] fnvDSigDEkProd_ISM0D;
      fnvDSigDEkProd_ISM0D = NULL;
   }

   // Free fnvSigProd_ISM0D[NCRs][NParents(CR)][NE]
   if (fnvSigProd_ISM0D) {
      for (Int_t j_cr = 0; j_cr < GetNCRs(); ++j_cr) {
         for (Int_t j_parent = 0; j_parent < fCrE->GetNParents(j_cr); ++j_parent) {
            if (fnvSigProd_ISM0D[j_cr][j_parent]) delete[] fnvSigProd_ISM0D[j_cr][j_parent];
            fnvSigProd_ISM0D[j_cr][j_parent] = NULL;
         }
         if (fnvSigProd_ISM0D[j_cr]) delete[] fnvSigProd_ISM0D[j_cr];
         fnvSigProd_ISM0D[j_cr] = NULL;
      }
      if (fnvSigProd_ISM0D) delete[] fnvSigProd_ISM0D;
      fnvSigProd_ISM0D = NULL;
   }

   // Free fMedium_ISM0D
   if (fMedium_ISM0D)
      delete fMedium_ISM0D;
   fMedium_ISM0D = NULL;
}

//______________________________________________________________________________
void TUXSections::DeleteProdDSigAllTargets(Int_t j_parent, Int_t j_cr, Int_t g_ghost)
{
   //--- Free memory 3rd dimension (energies) for fProdDSigDEk[nTargets*j_cr][j_parent].
   //  j_parent          Parent index for CR j_cr
   //  j_cr              CR index
   //  g_ghost           Ghost index (if no ghost, -1)

   for (Int_t t_targ = 0; t_targ < GetNTargets(); ++t_targ) {
      if (g_ghost == -1) {
         if (fProdDSigDEk[t_targ * GetNCRs() + j_cr][j_parent])
            delete[] fProdDSigDEk[t_targ * GetNCRs() + j_cr][j_parent];
         fProdDSigDEk[t_targ * GetNCRs() + j_cr][j_parent] = NULL;
      } else if (fCrE && fCrE->IsGhostsLoaded()) {
         if (fGhostsProdDSigDEk[t_targ * GetNCRs() + j_cr][j_parent * fCrE->GetCREntry(j_cr).GetNGhosts() + g_ghost])
            delete[] fGhostsProdDSigDEk[t_targ * GetNCRs() + j_cr][j_parent * fCrE->GetCREntry(j_cr).GetNGhosts() + g_ghost];
         fGhostsProdDSigDEk[t_targ * GetNCRs() + j_cr][j_parent * fCrE->GetCREntry(j_cr).GetNGhosts() + g_ghost] = NULL;
      }
   }
}

//______________________________________________________________________________
void TUXSections::DeleteProdSigAllTargets(Int_t j_parent, Int_t j_cr, Int_t g_ghost)
{
   //--- Free memory 3rd dimension (energies) for fProdSig[nTargets*j_cr][j_parent].
   //  j_parent          Parent index for CR j_cr
   //  j_cr              CR index
   //  g_ghost           Ghost index (if no ghost, -1)

   for (Int_t t_targ = 0; t_targ < GetNTargets(); ++t_targ) {
      if (g_ghost == -1) {
         if (fProdSig[t_targ * GetNCRs() + j_cr][j_parent])
            delete[] fProdSig[t_targ * GetNCRs() + j_cr][j_parent];
         fProdSig[t_targ * GetNCRs() + j_cr][j_parent] = NULL;
      } else if (fCrE && fCrE->IsGhostsLoaded()) {
         if (fGhostsProdSig[t_targ * GetNCRs() + j_cr][j_parent * fCrE->GetCREntry(j_cr).GetNGhosts() + g_ghost])
            delete[] fGhostsProdSig[t_targ * GetNCRs() + j_cr][j_parent * fCrE->GetCREntry(j_cr).GetNGhosts() + g_ghost];
         fGhostsProdSig[t_targ * GetNCRs() + j_cr][j_parent * fCrE->GetCREntry(j_cr).GetNGhosts() + g_ghost] = NULL;
      }
   }
}

//______________________________________________________________________________
void TUXSections::DeleteXSec()
{
   //--- Frees memory for all X-sections class members (fCrE is dealt with elsewhere).


   // Clear files
   fFilesXDiffINA.clear();
   fFilesXProd.clear();
   fFilesXTotIA.clear();
   fFilesXTotINA.clear();

   // Free memory for Optim0D members
   DeleteProdRate_ISM0D();

   // Free memory for 1D arrays
   //  1. Arrays of boolean
   if (fIsDSigDEkINAFilled) delete[] fIsDSigDEkINAFilled;
   fIsDSigDEkINAFilled = NULL;
   if (fIsTotIAFilled) delete[] fIsTotIAFilled;
   fIsTotIAFilled = NULL;
   if (fIsTotINAFilled) delete[] fIsTotINAFilled;
   fIsTotINAFilled = NULL;
   //  2. Array of double
   if (fTotIA) delete[] fTotIA;
   fTotIA = NULL;
   if (fTotINA) delete[] fTotINA;
   fTotINA = NULL;
   if (fDSigDEkINA) delete[] fDSigDEkINA;
   fDSigDEkINA = NULL;
   //  3. Arrays of integer
   if (fMapCRIndex2INAIndex) delete[] fMapCRIndex2INAIndex;
   fMapCRIndex2INAIndex = NULL;
   if (fMapINAIndex2CRIndex) delete[] fMapINAIndex2CRIndex;
   fMapINAIndex2CRIndex = NULL;

   // Free memory for 2D and 3D arrays (fProd... and fGhostsProd...)
   //   fProdDSigDEk[NTarg*NCRs][NParents(CR)][NE*NE],
   //   fProdSig[NTarg*NCRs][NParents(CR)][NE],
   //   fProdSig[NTarg*NCRs][NParents(CR)][NE],
   // and
   //   fGhostsProdDSigDEk[NTarg*NCRs][NParents(CR)*NGhosts(CR)][NE*NE],
   //   fGhostsProdSig[NTarg*NCRs][NParents(CR)*NGhosts(CR)][NE],
   //   fGhostsProdSig[NTarg*NCRs][NParents(CR)*NGhosts(CR)][NE],
   if (fProdDSigDEk || fProdSig) {
      // Loop over CR and all its ghosts if enabled
      for (Int_t j_cr = 0; j_cr < GetNCRs(); ++j_cr) {
         Int_t g_max = -1;
         if (fCrE->IsGhostsLoaded())
            g_max = fCrE->GetCREntry(j_cr).GetNGhosts() - 1;
         // Loop over parents
         for (Int_t j_parent = 0; j_parent < fCrE->GetNParents(j_cr); ++j_parent) {
            // Loop over ghosts
            for (Int_t g_ghost = g_max; g_ghost >= -1; --g_ghost) {
               DeleteProdSigAllTargets(j_parent, j_cr, g_ghost);
               DeleteProdDSigAllTargets(j_parent, j_cr, g_ghost);
            }
         }

         // Loop over targets
         for (Int_t t_targ = 0; t_targ < GetNTargets(); ++t_targ) {
            Int_t i = t_targ * GetNCRs() + j_cr;
            if (fProdDSigDEk[i]) {
               delete[] fProdDSigDEk[i];
               fProdDSigDEk[i] = NULL;
            }
            if (fProdSig[i]) {
               delete[] fProdSig[i];
               fProdSig[i] = NULL;
            }
            // And for ghosts
            if (fCrE && fCrE->IsGhostsLoaded()) {
               if (fGhostsProdDSigDEk[i]) {
                  delete[] fGhostsProdDSigDEk[i];
                  fGhostsProdDSigDEk[i] = NULL;
               }
               if (fGhostsProdSig[i]) {
                  delete[] fGhostsProdSig[i];
                  fGhostsProdSig[i] = NULL;
               }
            }
         }
         // Free fProdStatus[NCRs][NParents(CR)].
         if (fProdStatus[j_cr]) {
            delete[] fProdStatus[j_cr];
            fProdStatus[j_cr] = NULL;
         }
         // Free fGhostsProdStatus[NCRs][NParents(CR)].
         if (fCrE && fCrE->IsGhostsLoaded() && fGhostsProdStatus[j_cr]) {
            delete[] fGhostsProdStatus[j_cr];
            fGhostsProdStatus[j_cr] = NULL;
         }
      }
      if (fProdDSigDEk) delete[] fProdDSigDEk;
      fProdDSigDEk = NULL;
      if (fProdSig) delete[] fProdSig;
      fProdSig = NULL;
      if (fProdStatus) delete[] fProdStatus;
      fProdStatus = NULL;

      // And for ghosts
      if (fCrE && fCrE->IsGhostsLoaded()) {
         if (fGhostsProdDSigDEk) delete[] fGhostsProdDSigDEk;
         fGhostsProdDSigDEk = NULL;
         if (fGhostsProdSig) delete[] fGhostsProdSig;
         fGhostsProdSig = NULL;
         if (fGhostsProdStatus) delete[] fGhostsProdStatus;
         fGhostsProdStatus = NULL;
      }
   }

   // Clear free XS pars
   if (fParsXS_RefXS) delete fParsXS_RefXS;
   fParsXS_RefXS = NULL;

   // Clear targets and tertiary list (do not move!)
   fTargets.clear();
   fTertiaries.clear();
}

//______________________________________________________________________________
void TUXSections::ExtractDiffProdSpecies(vector<Int_t> &indices_crs,
      vector<vector<Int_t> > &indices_parents,
      gENUM_CRFAMILY switch_family) const
{
   //--- Returns list of indices (parents for crs) of species produced by differential cross-section,
   //    only if they belong to 'switch_family' (kNUC, kANTINUC, kLEPTON)
   //
   // INPUT:
   //  gENUM_CRFAMILY     Selection to search in (see list in TUEnum.h)
   // OUTPUT:
   //  indices_crs        Indices of CR species found
   //  indices_parents    For each CR in 'indice_crs', list of parents found

   indices_crs.clear();
   indices_parents.clear();
   for (Int_t j_cr = 0; j_cr < GetNCRs(); ++j_cr) {
      vector<Int_t> tmp;
      for (Int_t j_parent = 0; j_parent < fCrE->GetNParents(j_cr); ++j_parent) {
         if (IsProdSigmaOrdSigma(j_parent, j_cr))
            continue;
         if (switch_family == fCrE->GetCREntry(j_cr).GetFamily())
            tmp.push_back(j_parent);
      }
      if (tmp.size() != 0) {
         indices_crs.push_back(j_cr);
         indices_parents.push_back(tmp);
      }
   }
}

//______________________________________________________________________________
void TUXSections::ExtractIndicesForXSToRead(string const &parent, string const &frag,
      Bool_t &is_skip_xs, Bool_t &is_ghost_xs,
      vector<Int_t> &cr_indices, vector<Int_t> &parent_indices,
      vector<Int_t> &ghost_indices,
      vector<string> &reactions) const
{
   //--- For reaction 'parent -> frag', check whether it involves ghosts and if
   //    this reaction is in CR list.
   // INPUTS:
   //  parent            Name of the CR parent to search for
   //  frag              Name of the frag to search for
   // OUTPUTS:
   //  is_skip_xs        If reaction not in CR reactions, false
   //  is_ghost_xs       True if 'frag' is a ghost, false otherwise
   //  cr_indices        Vector of index of 'frag' or, if frag is a ghost, CR indices having this ghost
   //  parent_indices    Vector of indices of 'parent' in parent list of list of cr_indices
   //  ghost_indices     Vector of ghost_indices for list of cr_indices
   //  reactions         Vector of reactions (associated to cr_indices)

   is_ghost_xs = false;
   is_skip_xs = true;
   cr_indices.clear();
   parent_indices.clear();
   ghost_indices.clear();
   reactions.clear();
   // If parent not in list, skip!
   if (fCrE->Index(parent, false, stdout, false) < 0)
      return;

   Int_t j_cr = fCrE->Index(frag, false, stdout, false);
   if (j_cr < 0) {
      if (fCrE->IsGhostsLoaded()) {
         // Search ghost indices
         vector<Int_t> tmp_cr_indices, tmp_ghost_indices;
         fCrE->IndicesForGhost(frag, tmp_cr_indices, tmp_ghost_indices);
         // If ghosts, update list of associated parent indices (for reaction)
         if (tmp_cr_indices.size() != 0) {
            is_ghost_xs = true;
            is_skip_xs = false;
            Bool_t is_one_parent = false;
            for (Int_t ii = 0; ii < (Int_t)tmp_cr_indices.size(); ++ii) {
               Int_t j_parent_lop = fCrE->IndexInParentList(tmp_cr_indices[ii], parent);
               if (j_parent_lop >= 0) {
                  is_one_parent = true;
                  cr_indices.push_back(tmp_cr_indices[ii]);
                  ghost_indices.push_back(tmp_ghost_indices[ii]);
                  parent_indices.push_back(j_parent_lop);
                  reactions.push_back(FormReactionNameProd(parent_indices[ii], tmp_cr_indices[ii], tmp_ghost_indices[ii]));
               }
            }
            // If no valid parent, skip
            if (!is_one_parent) {
               is_skip_xs = true;
               return;
            }
         }
      }
   } else if (j_cr != GetNCRs() - 1 && fCrE->GetNParents(j_cr) != 0) {
      // No ghost: still use cr_indices and parent_indices (for generality below)
      // N.B.: if not a parent for this CR, skip
      Int_t j_parent_lop = fCrE->IndexInParentList(j_cr, parent);
      if (j_parent_lop < 0)
         return;
      is_skip_xs = false;
      cr_indices.push_back(j_cr);
      parent_indices.push_back(j_parent_lop);
      ghost_indices.push_back(-1); // no ghosts, used elsewhere
      reactions.push_back(FormReactionNameProd(j_parent_lop, j_cr, -1));
   }
}

//______________________________________________________________________________
void TUXSections::ExtractXSGraphs(Int_t j_parent, Int_t t_targ, Int_t j_frag, TMultiGraph *mg,
                                  TLegend *leg_mg, vector<TH2D *> &v_hist, Int_t calledfrom,
                                  Int_t g_ghost, FILE *f_out) const
{
   //--- Returns for CR j_parent all available plots for total or differential
   //    cross sections (IA or prod, and INA or prod) and their legend (one graph
   //    per ISM target element, one 2D histo per differential interaction).
   //  j_parent           Projectile index (in list of parents for j_frag) or -1 (if total X-sec)
   //  t_targ             Target index (use -1 to display all targets)
   //  j_frag             Fragment index
   //  mg                 multigraph for CR+targets (prod, or IA and INA (if applies))
   //  leg_mg             Legend for mg
   //  v_hist             List of TH2D (one per target)
   //  called_from        Integer to encode that function was called w/wo nuisance XS parameters
   //                        0: std call for XS displays
   //                        1: XS display with XS nuisance parameters: reference XS
   //                        2: XS display with XS nuisance parameters: XS from minimisation on CR data
   //  g_ghost            Ghost index (if enabled)
   //  f_out              File in which to print

   if (!mg)
      TUMessages::Error(stdout, "TUXSections", "ExtractXSGraphs", "Multigraph must be created before calling function");
   if (!leg_mg)
      TUMessages::Error(stdout, "TUXSections", "ExtractXSGraphs", "Legend must be created before calling function");

   string indent = TUMessages::Indent(true);

   // Set legent properties
   leg_mg->SetTextColor(kBlack);
   leg_mg->SetTextFont(132);
   leg_mg->SetTextSize(0.035);
   leg_mg->SetBorderSize(0);
   leg_mg->SetFillColor(0);
   leg_mg->SetFillStyle(0);
   if (t_targ < 0)
      leg_mg->SetHeader((string("ISM=" + ExtractTargets())).c_str());

   // Get name (and root name) of frag and proj (if exists)
   string frag = fCrE->GetCREntry(j_frag).GetName();
   string frag_root = fCrE->GetNameROOT(j_frag);

   string proj = "", proj_root = "";
   Int_t j_parent_in_loc = -1;
   if (j_parent >= 0) {
      j_parent_in_loc = fCrE->IndexInCRList_Parent(j_frag, j_parent);
      proj = fCrE->GetCREntry(j_parent_in_loc).GetName();
      proj_root = fCrE->GetNameROOT(j_parent_in_loc);
   }

   // Loop on targets
   Int_t n_targs = GetNTargets();
   Int_t t_start = 0;
   if (t_targ >= 0) {
      n_targs = 1;
      t_start = t_targ;
   }
   for (Int_t i_targ = t_targ; i_targ < t_targ + n_targs; ++i_targ) {
      string target = GetNameTarget(i_targ);
      // Create graph:
      //   j_parent<0 => total inelastic annihilating (IA) X-sec
      //   j_parent>=0 and straight-ahead prod. exist => Straight-ahead X-sec
      TGraph *gr = NULL;
      string name = "", title = "";
      string prename = "xs";
      string pretitle = "";
      string x_title = "";
      string y_title = "";
      Int_t l_width = 2;
      if (calledfrom == 1) {
         prename = "xs_prefit";
         pretitle = "Pre-fit: ";
         l_width = 1;
      } else if (calledfrom == 2) {
         prename = "xs_postfit";
         pretitle = "Post-fit: ";
         l_width = 4;
      }
      if (j_parent < 0) {
         name = prename + "_sig_inelann_" + frag + "_" + target;
         title = pretitle + "#sigma_{inel.} (" + frag_root + "+" + target + ")";
         if (calledfrom == 0)
            fprintf(f_out, "%s    - %s\n", indent.c_str(), title.c_str());
         gr = OrphanGetTGraphSigInel(j_frag, i_targ, /*is_ia_or_ina*/ true);
      } else if (IsProdSigmaOrdSigma(j_parent, j_frag)) {
         name = prename + "_sig_prod_" + proj + "_" + target + "_" + frag;
         title = pretitle + "#sigma_{prod} (" + proj_root + "+" + target + "#rightarrow" + frag_root + ")";
         if (calledfrom == 0)
            fprintf(f_out, "%s    - %s\n", indent.c_str(), title.c_str());
         if (g_ghost == -1)
            gr = OrphanGetTGraphSigProd(j_parent, i_targ, j_frag);
         else {
            string ghost = fCrE->GetCREntry(j_frag).GetGhost(g_ghost).GetName();
            name = prename + "_sig_prod_" + proj + "_" + target + "_" + frag + "__" + ghost;
            title = pretitle + "#sigma_{prod} (" + proj_root + "+" + target + "#rightarrow (" + ghost + "#rightarrow)" + frag_root;
            gr = OrphanGetTGraphSigProd(j_parent, i_targ, j_frag, g_ghost);
         }
      }


      // Add graph in multigraph (only if graph is filled at previous step)
      string option = "L";
      //string option = "LP";
      if (gr) {
         x_title = gr->GetXaxis()->GetTitle();
         y_title = gr->GetYaxis()->GetTitle();

         gr->SetName(name.c_str());
         gr->SetTitle(title.c_str());
         gr->GetXaxis()->SetTitle(x_title.c_str());
         gr->GetYaxis()->SetTitle(y_title.c_str());
         //gr->SetMarkerColor(TUMisc::RootColor(j_frag));
         gr->SetLineColor(TUMisc::RootColor(j_frag));
         gr->SetLineStyle((j_parent + j_frag + i_targ) % 9 + 1);
         gr->SetLineWidth(l_width);
         if (j_parent < 0)
            gr->SetMarkerStyle(TUMisc::RootMarker(j_frag));
         else
            gr->SetMarkerStyle(TUMisc::RootMarker(j_parent_in_loc));
         gr->SetMarkerSize(0.8);
         gr->SetDrawOption(option.c_str());
         gr->SetFillStyle(0);
         mg->Add(gr, option.c_str());
         if (leg_mg) {
            TLegendEntry *entry = leg_mg->AddEntry(gr, title.c_str(), option.c_str());
            entry->SetTextColor(TUMisc::RootColor(j_frag));
            entry = NULL;
         }
      }

      // If tertiary and j_frag=-1 (not production), add to
      // elastic non-annihilating (INA) X-sec (total and differential)
      if (IsTertiary(j_frag) && j_parent < 0) {
         Int_t j_tertiary = MapCRIndex2INAIndex(j_frag);

         // Add graph for total inelastic non-annihilating (INA) X-sec
         name = prename + "_sig_inelnonann_" + frag + "_" + target;
         TUMisc::RemoveSpecialChars(name);
         title = pretitle + "#sigma_{inel.non-ann.} (" + frag_root + "+" + target + ")";
         if (calledfrom == 0)
            fprintf(f_out, "%s    - %s\n", indent.c_str(), title.c_str());
         TGraph *gr_tert = OrphanGetTGraphSigInel(j_tertiary, i_targ, /*is_ia_or_ina*/ false);
         gr_tert->SetName(name.c_str());
         gr_tert->GetXaxis()->SetTitle(x_title.c_str());
         gr_tert->GetYaxis()->SetTitle(y_title.c_str());
         gr_tert->SetTitle(title.c_str());
         gr_tert->SetMarkerColor(TUMisc::RootColor(j_frag));
         gr_tert->SetLineColor(TUMisc::RootColor(j_frag));
         gr_tert->SetLineStyle((j_parent + j_frag + i_targ) % 9 + 1);
         gr_tert->SetLineWidth(l_width);
         gr_tert->SetMarkerStyle(TUMisc::RootMarker(j_frag));
         gr_tert->SetMarkerSize(0.4);
         gr_tert->SetDrawOption(option.c_str());
         gr_tert->SetFillStyle(0);
         mg->Add(gr_tert, option.c_str());
         if (leg_mg) {
            TLegendEntry *entry = leg_mg->AddEntry(gr_tert, title.c_str(), option.c_str());
            entry->SetTextColor(TUMisc::RootColor(j_frag));
            entry = NULL;
         }

         // If tertiary, we also want to calculate total inelastic
         TGraph *gr_tot = new TGraph(*gr); // Clone
         string name_tot = prename + "_sig_ineltot_" + frag + "_" + target;
         TUMisc::RemoveSpecialChars(name_tot);
         string title_tot = "#sigma_{inel.tot.} (" + frag_root
                            + "+" + target + ") = #sigma_{inel.ann.} + #sigma_{inel.non-ann.}";
         gr_tot->SetName(name_tot.c_str());
         gr_tot->SetTitle(title_tot.c_str());
         gr_tot->GetXaxis()->SetTitle(x_title.c_str());
         gr_tot->GetYaxis()->SetTitle(y_title.c_str());

         for (Int_t k = 0; k < gr->GetN(); ++k) {
            Double_t x_gr, y_gr, x_gr_tot, y_gr_tot;
            gr->GetPoint(k, x_gr, y_gr);
            gr_tot->GetPoint(k, x_gr_tot, y_gr_tot);
            gr_tot->SetPoint(k, x_gr + x_gr_tot, y_gr + y_gr_tot);
         }
         gr_tot->SetMarkerColor(TUMisc::RootColor(j_frag));
         gr_tot->SetLineColor(TUMisc::RootColor(j_frag));
         gr_tot->SetLineStyle((j_parent + j_frag + i_targ) % 9 + 1);
         gr_tot->SetLineWidth(l_width);
         gr_tot->SetMarkerStyle(TUMisc::RootMarker(j_frag));
         gr_tot->SetMarkerSize(0.8);
         gr_tot->SetDrawOption(option.c_str());
         gr_tot->SetFillStyle(0);
         mg->Add(gr_tot, option.c_str());
         if (leg_mg) {
            TLegendEntry *entry = leg_mg->AddEntry(gr_tot, title_tot.c_str(), option.c_str());
            entry->SetTextColor(TUMisc::RootColor(j_frag));
            entry = NULL;
         }
      }

      // If tertiary or differential production (add to
      // elastic non-annihilating (INA)) or production X-sec
      if ((IsTertiary(j_frag) && j_parent < 0) || (j_parent >= 0
            && !IsProdSigmaOrdSigma(j_parent, j_frag))) {
         TH2D *h2 = NULL;
         if (j_parent < 0) {
            // Add TH2 for differential inelastic non-annihilating (INA) X-sec
            Int_t j_tertiary = MapCRIndex2INAIndex(j_frag);
            name = prename + "_dsig_inelnonann_" + frag + "_" + target;
            TUMisc::RemoveSpecialChars(name);
            title = pretitle + "#frac{d#sigma_{inel.non-ann.}}{dEk} ("
                    + frag_root + "+" + target + " -> " + frag_root + "X)";
            if (calledfrom == 0)
               fprintf(f_out, "%s    - %s\n", indent.c_str(), title.c_str());
            h2 = OrphanGetTH2DSigINA(j_tertiary, i_targ);
         } else {
            // Add TH2 for differential production
            name = prename + "_dsig_prod_" + proj + "_" + target + "_" + frag;
            TUMisc::RemoveSpecialChars(name);
            title = pretitle + "#frac{d#sigma_{prod.}}{dEk} ("
                    + proj_root + "+" + target + "#rightarrow" + frag_root + ")";
            if (calledfrom == 0)
               fprintf(f_out, "%s    - %s\n", indent.c_str(), title.c_str());
            if (g_ghost == -1)
               h2 = OrphanGetTH2DSigProd(j_parent, i_targ, j_frag);
            else {
               h2 = OrphanGetTH2DSigProd(j_parent, i_targ, j_frag, g_ghost);
               string ghost = fCrE->GetCREntry(j_frag).GetGhost(g_ghost).GetName();
               name = prename + "_dsig_prod_" + proj + "_" + target + "_" + frag + "__" + ghost;
               title = pretitle + "#frac{d#sigma_{prod.}}{dEk} (" + proj_root + "+" + target + "#rightarrow (" + ghost + "#rightarrow)" + frag_root;
            }

         }
         x_title = h2->GetXaxis()->GetTitle();
         y_title = h2->GetYaxis()->GetTitle();

         h2->SetName(name.c_str());
         h2->GetXaxis()->SetTitle(x_title.c_str());
         h2->GetYaxis()->SetTitle(y_title.c_str());
         h2->SetTitle(title.c_str());
         h2->SetStats(kFALSE);
         h2->SetMarkerColor(TUMisc::RootColor(j_frag));
         h2->SetLineColor(TUMisc::RootColor(j_frag));
         h2->SetLineStyle((j_parent + j_frag + i_targ) % 9 + 1);
         h2->SetLineWidth(2);
         if (j_parent < 0)
            h2->SetMarkerStyle(TUMisc::RootMarker(j_frag));
         else
            h2->SetMarkerStyle(TUMisc::RootMarker(j_parent_in_loc));
         h2->SetMarkerSize(0.7);
         h2->SetFillStyle(0);
         v_hist.push_back(h2);
         h2 = NULL;
      }
   }

   // multigraph title
   string x_title = "Ekn [GeV/n]";
   string y_title;
   if (j_parent < 0)
      y_title = "#sigma_{inel} [mb]";
   else
      y_title = "#sigma_{prod. (straight-ahead approx.)} [mb]";
   string mg_titles = ";" + x_title + ";" + y_title;
   mg->SetTitle(mg_titles.c_str());

   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUXSections::ExtractXSGraphsList(string list_proj, string list_targ, string list_frag,
                                      TMultiGraph *mg, TLegend *leg_mg, vector<TH2D *> &v_hist,
                                      FILE *f_out) const
{
   //--- Returns for a list of CRs all available plots for total or differential
   //    cross sections (IA or prod, and INA or prod) and their legend (one graph
   //    per ISM target element, one 2D histo per differential interaction).
   //  list_proj          Comma-separated list of projectile names (empty if total X-section)
   //  list_targ          Comma-separated list of target names (set to "ALL" to display all XS)
   //  list_frag          Comma-separated list of fragment names
   //  mg                 multigraph for CR+targets (prod, or IA and INA (if applies))
   //  leg_mg             Legend for mg
   //  v_hist             List of TH2D (one per target)
   //  f_out              File in which to print


   if (!mg)
      TUMessages::Error(stdout, "TUXSections", "ExtractXSGraphsList", "Multigraph must be created before calling function");
   if (!leg_mg)
      TUMessages::Error(stdout, "TUXSections", "ExtractXSGraphsList", "Legend must be created before calling function");

   string indent = TUMessages::Indent(true);

   // Extract targets
   TUMisc::UpperCase(list_targ);
   if (list_targ == "ALL")
      list_targ = ExtractTargets();
   vector<Int_t> t_targ = IndicesTargetsInFile(list_targ);

   // Extract fragment and projectiles (if applies) indices
   // N.B.: if production, need to ensure that CRs in projectile lists
   // are parents for CRs in fragment list
   vector<string> names_frag;
   vector<Int_t> i_frag;
   TUMisc::String2List((const string &)list_frag, ",", names_frag);
   for (Int_t i = 0; i < (Int_t)names_frag.size(); ++i) {
      Int_t j_cr = fCrE->Index(names_frag[i], true, stdout, false);
      if (j_cr >= 0)
         i_frag.push_back(j_cr);
   }
   Int_t n_frag = i_frag.size();

   // If inelastic cross sections
   if (list_proj == "" && n_frag > 0) {
      for (Int_t i = 0; i < n_frag; ++i) {
         fprintf(f_out, "%s  --> Add plots for inelastic reaction %s + ISM\n",
                 indent.c_str(), fCrE->GetCREntry(i_frag[i]).GetName().c_str());
         for (Int_t t = 0; t < (Int_t)t_targ.size(); ++t) {
            ExtractXSGraphs(-1, t_targ[t], i_frag[i], mg, leg_mg, v_hist, 0, -1, f_out);
         }
      }
   }

   // If production cross sections
   if (list_proj != "" && n_frag > 0) {
      vector<string> names_proj;
      TUMisc::String2List((const string &)list_proj, ",", names_proj);

      // Loop on fragments
      for (Int_t i = 0; i < n_frag; ++i) {
         Int_t j_frag = i_frag[i];

         // Loop on CR projectiles (and discard those not parents)
         for (Int_t j = 0; j < (Int_t)names_proj.size(); ++j) {
            Int_t j_parent = fCrE->IndexInParentList(j_frag, names_proj[j]);
            if (j_parent < 0)
               continue;
            fprintf(f_out, "%s  --> Add plots for reaction %s + ISM -> %s\n", indent.c_str(),
                    fCrE->GetCREntry(fCrE->IndexInCRList_Parent(j_frag, j_parent)).GetName().c_str(),
                    fCrE->GetCREntry(j_frag).GetName().c_str());
            for (Int_t t = 0; t < (Int_t)t_targ.size(); ++t)
               ExtractXSGraphs(j_parent, t_targ[t], j_frag, mg, leg_mg, v_hist, 0, -1, f_out);
            // And add all ghosts if enabled
            if (fCrE->IsGhostsLoaded()) {
               for (Int_t g = 0; g < fCrE->GetCREntry(j_frag).GetNGhosts(); ++g) {
                  fprintf(f_out, "%s          ... and for ghosts %s + ISM -> (%s->)%s",
                          indent.c_str(), fCrE->GetCREntry(fCrE->IndexInCRList_Parent(j_frag, j_parent)).GetName().c_str(),
                          fCrE->GetCREntry(j_frag).GetGhost(g).GetName().c_str(),
                          fCrE->GetCREntry(j_frag).GetName().c_str());
                  for (Int_t t = 0; t < (Int_t)t_targ.size(); ++t)
                     ExtractXSGraphs(j_parent, t_targ[t], j_frag, mg, leg_mg, v_hist, 0, g, f_out);
               }
            }
         }
      }
   }

   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUXSections::FillInterRateDSigINA_ISM0D(Int_t j_tert, TUMediumEntry *medium)
{
   //--- Fill fnvDSigDEkINA_ISM0D (differential INA XS [/(GeV Myr)]) for j_tert.
   //  j_tert            Index of tertiary
   //  j_cr              Index of fragment for reaction
   //  medium            ISM properties for a given space-time

   // Fill
   for (Int_t k_out = 0; k_out < GetNE(); ++k_out) {
      for (Int_t k_in = 0; k_in < GetNE(); ++k_in) {
         Double_t val = ISMWeightedTertINA_nvDSigDEk(j_tert, k_in, k_out, medium);
         Set_nvDSigDEkINA_ISM0D(j_tert, k_in, k_out, val);
      }
   }
}

//______________________________________________________________________________
void TUXSections::FillInterRateProd_ISM0D(Int_t j_parent, Int_t j_cr, TUMediumEntry *medium)
{
   //--- Fill fnvSigProd_ISM0D (straight-ahead approx. [/Myr]) or
   //    fnvDSigDEkProd_ISM0D (differential production [/(GeV Myr)])
   //    for j_parent->j_cr and 'medium'.
   //  j_parent          Index of parent for reaction
   //  j_cr              Index of fragment for reaction
   //  medium            ISM properties for a given space-time

   // Total or differential XS
   if (IsProdSigmaOrdSigma(j_parent, j_cr)) {
      // Allocate if necessary
      if (!fnvSigProd_ISM0D[j_cr][j_parent])
         fnvSigProd_ISM0D[j_cr][j_parent] = new Double_t[GetNE()];
      // Fill
      for (Int_t k = 0; k < GetNE(); ++k) {
         Double_t val =  ISMWeightedProd_nvSig(j_parent, j_cr, k, medium);
         Set_nvSigProd_ISM0D(j_parent, j_cr, k, val);
      }
   } else {
      // Allocate if necessary
      if (!fnvDSigDEkProd_ISM0D[j_cr][j_parent])
         fnvDSigDEkProd_ISM0D[j_cr][j_parent] = new Double_t[GetNE() * GetNE()];
      // Fill
      for (Int_t k_out = 0; k_out < GetNE(); ++k_out) {
         for (Int_t k_in = 0; k_in < GetNE(); ++k_in) {
            Double_t val = ISMWeightedProd_nvDSigDEk(j_parent, j_cr, k_in, k_out, medium);
            Set_nvDSigDEkProd_ISM0D(j_parent, j_cr, k_in, k_out, val);
         }
      }
   }
}

//______________________________________________________________________________
void TUXSections::FillInterRateTotIA_ISM0D(Int_t j_cr, TUMediumEntry *medium)
{
   //--- Fill fnvSigTotIA_ISM0D for inelastic annihilating [/Myr] for j_cr and 'medium'.
   //  j_cr              Index of CR to fill
   //  medium            ISM properties for a given space-time

   // For fnvSigTotIA_ISM0D[NCRs*NE]
   for (Int_t k_ekn = GetNE() - 1; k_ekn >= 0; --k_ekn) {
      Double_t val = ISMWeightedTotIA_nvSig(j_cr, k_ekn, medium);
      Set_nvSigTotIA_ISM0D(j_cr, k_ekn, val);
   }
}

//______________________________________________________________________________
void TUXSections::FillInterRateTotINA_ISM0D(Int_t j_tert, TUMediumEntry *medium)
{
   //--- Fill fnvSigTotINA_ISM0D for inelastic non-annihilating [/Myr] for j_tert and 'medium'.
   //  j_tert            Index of tertiary to fill
   //  medium            ISM properties for a given space-time

   // For fnvSigTotINA_ISM0D[NCRs*NE]
   for (Int_t k_ekn = GetNE() - 1; k_ekn >= 0; --k_ekn) {
      Double_t val = ISMWeightedTotINA_nvSig(j_tert, k_ekn, medium);
      Set_nvSigTotINA_ISM0D(j_tert, k_ekn, val);
   }
}

//______________________________________________________________________________
void TUXSections::FillInterRate_ISM0D(TUMediumEntry *medium, Bool_t is_force_refill)
{
   //--- Allocates and fills ISM element-weighted interaction rate (for an ISM entry):
   //       - fnvDSigDEkINA_ISM0D for inelastic non-annihilating [/Myr]
   //       - fnvSigTotIA_ISM0D for inelastic annihilating [/Myr]
   //       - fnvDSigDEkProd_ISM0D for differential production  [/(GeV Myr)]
   //       - fnvSigProd_ISM0D for straight-ahead approx. prod. [/Myr]
   //    for all energies Ekn and CR parent/fragment associations as given in fCrE
   //    (it is useful to store this values once for all in some case).
   //
   //  medium            ISM properties for a given space-time
   //  is_force_refill   Force re-calculation, even if fnvDSigXXX and fnvSigXXX already exist
   //BEGIN_LATEX(fontsize=14, separator='=', align='rl')
   //       < #frac{d#Gamma^{prod}}{dEk_{out}} (proj, Ekn_{in}, frag, Ekn_{out}) >_{ISM} = #sum_{i #in ISM} #left( n_{i} #times v(Ekn_{in}) #times #frac{d#sigma^{proj+i#rightarrow frag}}{dEk_{out}}(Ekn_{in},Ekn_{out})#right)
   //       < #Gamma^{prod}(proj, frag, Ekn) >_{ISM} = #sum_{i #in ISM} #left( n_{i} #times v(Ekn) #times #sigma^{proj+i#rightarrow frag}(Ekn)#right)
   //END_LATEX

   // Free memory (if pre-exists and force refill)
   if (is_force_refill)
      DeleteProdRate_ISM0D();
   else if (fnvDSigDEkINA_ISM0D)
      return;

   // Allocate for fnvDSigDEkINA_ISM0D[NTert*NE(out)*NE(in)]
   fnvDSigDEkINA_ISM0D = new Double_t[GetNTertiaries()*GetNE()*GetNE()];
   for (Int_t j_tert = GetNTertiaries() - 1; j_tert >= 0; --j_tert) {
      for (Int_t k_out = GetNE() - 1; k_out >= 0; --k_out) {
         for (Int_t k_in = GetNE() - 1; k_in >= 0; --k_in) {
            Double_t val = ISMWeightedTertINA_nvDSigDEk(j_tert, k_in, k_out, medium);
            Set_nvDSigDEkINA_ISM0D(j_tert, k_in, k_out, val);
         }
      }
   }

   // Allocate for fnvSigTotIA_ISM0D[NCRs*NE]
   fnvSigTotIA_ISM0D = new Double_t[GetNCRs()*GetNE()];
   for (Int_t j_cr = GetNCRs() - 1; j_cr >= 0; --j_cr)
      FillInterRateTotIA_ISM0D(j_cr, medium);

   // Allocate fnvSigTotINA_ISM0D[NTert*NE]
   fnvSigTotINA_ISM0D = new Double_t[GetNTertiaries()*GetNE()];
   for (Int_t j_tert = GetNTertiaries() - 1; j_tert >= 0; --j_tert) {
      for (Int_t k_ekn = GetNE() - 1; k_ekn >= 0; --k_ekn) {
         Double_t val = ISMWeightedTotINA_nvSig(j_tert, k_ekn, medium);
         Set_nvSigTotINA_ISM0D(j_tert, k_ekn, val);
      }
   }

   // Allocate and fill for prod X-sec
   fnvSigProd_ISM0D = new Double_t **[GetNCRs()];
   fnvDSigDEkProd_ISM0D = new Double_t **[GetNCRs()];
   // For each cr, allocate according to the number of parents
   for (Int_t j_cr = 0; j_cr < GetNCRs(); ++j_cr) {
      fnvSigProd_ISM0D[j_cr] = NULL;
      fnvDSigDEkProd_ISM0D[j_cr] = NULL;

      // Check whether there is a parent for j_cr (otherwise, it was not allocated)
      if (fCrE->GetNParents(j_cr) <= 0)
         continue;

      fnvSigProd_ISM0D[j_cr] = new Double_t *[fCrE->GetNParents(j_cr)];
      fnvDSigDEkProd_ISM0D[j_cr] = new Double_t *[fCrE->GetNParents(j_cr)];
      for (Int_t j_par = 0; j_par < fCrE->GetNParents(j_cr); ++j_par) {
         fnvSigProd_ISM0D[j_cr][j_par] = NULL;
         fnvDSigDEkProd_ISM0D[j_cr][j_par] = NULL;
         FillInterRateProd_ISM0D(j_par, j_cr, medium);
      }
   }

   if (fMedium_ISM0D)
      delete fMedium_ISM0D;
   fMedium_ISM0D = medium->Clone();
}

//______________________________________________________________________________
string TUXSections::FormReactionNameProd(Int_t j_parent, Int_t j_cr, Int_t g_ghost, Int_t t_targ) const
{
   //--- Forms name of reaction (used many times).
   //  j_parent           Parent index
   //  j_cr               CR index
   //  g_ghost            Ghost index (if ghost enabled)
   //  t_targ             Target index (default=-1 => all targets)

   string target = ExtractTargets();
   if (t_targ != -1)
      target = GetNameTarget(t_targ);

   if (g_ghost == -1)
      return (fCrE->GetCREntry(fCrE->IndexInCRList_Parent(j_cr, j_parent)).GetName()
              + " + " + target + " -> " + fCrE->GetCREntry(j_cr).GetName());
   else
      return (fCrE->GetCREntry(fCrE->IndexInCRList_Parent(j_cr, j_parent)).GetName()
              + " + " + target + " -> (" + fCrE->GetCREntry(j_cr).GetGhost(g_ghost).GetName() + "->@"
              + Form("%.1f", fCrE->GetCREntry(j_cr).GetGhostBr(g_ghost)) + "%)" + fCrE->GetCREntry(j_cr).GetName());
}

//______________________________________________________________________________
vector<Int_t> TUXSections::IndicesTargetsInFile(string const &commasep_targets) const
{
   //--- Returns vector of indices[n_targets] such as commasep_targets[indices[i]] = fTargets[i].
   // commasep_targets    Comma-separated list of target names to match to fTargets

   vector<Int_t> indices;
   vector<string> targets;
   TUMisc::String2List(commasep_targets, ",", targets);

   for (Int_t i = 0; i < GetNTargets(); ++i) {
      for (Int_t k = 0; k < (Int_t)targets.size(); ++k) {
         TUMisc::UpperCase(targets[k]);
         if (fTargets[i] == targets[k]) {
            indices.push_back(k);
            break;
         }
      }
   }

   if (GetNTargets() != (Int_t)indices.size()) {
      string targ_class = ExtractTargets();
      string message = "Targets " + targ_class
                       + " not found in the X-sec file targets ("
                       + commasep_targets + ")";
      TUMessages::Error(stdout, "TUXSections", "IndicesTargetsInFile", message);
   }
   return indices;
}

//______________________________________________________________________________
void TUXSections::Initialise(Bool_t is_delete)
{
   //--- Initialises class members (after deletion if is_delete=true).
   //  is_delete          Whether to delete class members or not

   // If delete
   if (is_delete) {
      DeleteXSec();
      if (fIsDeleteCrE && fCrE)
         delete fCrE;
      if (fMedium_ISM0D)
         delete fMedium_ISM0D;
      if (fIsDeleteFreePars && fFreePars)
         delete fFreePars;
   }

   fCrE = NULL;
   fDSigDEkINA = NULL;
   fFilesXDiffINA.clear();
   fFilesXProd.clear();
   fFilesXTotIA.clear();
   fFilesXTotINA.clear();
   fFreePars = NULL;
   fGhostsProdDSigDEk = NULL;
   fGhostsProdSig = NULL;
   fGhostsProdStatus = NULL;
   fIsAtLeast1DiffProd = false;
   fIsDeleteCrE = true;
   fIsDeleteFreePars = true;
   fIsDSigDEkINAFilled = NULL;
   fIsTotIAFilled = NULL;
   fIsTotINAFilled = NULL;
   fMapCRIndex2INAIndex = NULL;
   fMapINAIndex2CRIndex = NULL;
   fMedium_ISM0D = NULL;
   fnvDSigDEkINA_ISM0D = NULL;
   fnvDSigDEkProd_ISM0D = NULL;
   fnvSigProd_ISM0D = NULL;
   fnvSigTotIA_ISM0D = NULL;
   fnvSigTotINA_ISM0D = NULL;
   fParsXS_ReacIndices.clear();
   fParsXS_RefXS = NULL;
   fProdDSigDEk = NULL;
   fProdSig = NULL;
   fProdStatus = NULL;
   fTargets.clear();
   fTertiaries.clear();
   fTotIA = NULL;
   fTotINA = NULL;

   // Clear any XS used for linear combination
   fXSLinComb.clear();
   fXSLinComb_InelReacs.clear();
   fXSLinComb_ProdReacs.clear();
   fXSLinComb_NInelXS = 0.;
   fXSLinComb_NProdXS = 0.;
}

//______________________________________________________________________________
Bool_t TUXSections::IsAllDSigDEkINAFilled() const
{
   //--- Returns true if all Non-Ann (tertiary) X-Secs are filled, false otherwise.

   for (Int_t j = GetNTertiaries() - 1; j >= 0; --j) {
      if (!IsDSigDEkINAFilled(j)) {
         string message = "Reaction " + fTertiaries[j] + " + " + ExtractTargets() +  " is not filled";
         TUMessages::Error(stdout, "TUXSections", "IsDSigDEkINAFilled", message);
         return false;
      }
   }
   return true;
}

//______________________________________________________________________________
Bool_t TUXSections::IsAllProdFilled()
{
   //--- Returns true if all production X-Secs are filled, false otherwise.

   // Loop over all CRs
   for (Int_t j_cr = GetNCRs() - 1; j_cr >= 0; --j_cr) {
      // Loop over CR and all its ghosts if enabled
      Int_t g_max = -1;
      if (fCrE->IsGhostsLoaded())
         g_max = fCrE->GetCREntry(j_cr).GetNGhosts() - 1;
      // Loop over parents
      for (Int_t j_parent = fCrE->GetNParents(j_cr) - 1; j_parent >= 0; --j_parent) {
         // Loop over ghosts
         for (Int_t g_ghost = g_max; g_ghost >= -1; --g_ghost) {
            // Only check for ghost with A_ghost <= A_parent
            if ((g_ghost >= 0
                  && fCrE->GetCREntry(j_cr).GetGhost(g_ghost).GetA() <= fCrE->GetCREntry(fCrE->IndexInCRList_Parent(j_cr, j_parent)).GetA()
                  && !IsProdFilled(j_parent, j_cr, g_ghost))
                  || (g_ghost == -1 && !IsProdFilled(j_parent, j_cr, g_ghost))) {
               string message = "Reaction " + FormReactionNameProd(j_parent, j_cr, g_ghost) + " is not filled";
               TUMessages::Error(stdout, "TUXSections", "IsProdFilled", message);
               return false;
            }
            // Check if there is at least one differential cross-section for production
            if (!IsProdSigmaOrdSigma(j_parent, j_cr, g_ghost))
               fIsAtLeast1DiffProd = true;
         }
      }
   }
   return true;
}

//______________________________________________________________________________
Bool_t TUXSections::IsAllTotIAFilled() const
{
   //--- Returns true if all Inelastic Annihil. X-Secs are filled, false otherwise.

   for (Int_t j = GetNCRs() - 1; j >= 0; --j) {
      if ((fCrE->GetCREntry(j).GetFamily() == kNUC || fCrE->GetCREntry(j).GetFamily() == kANTINUC) && !IsTotIAFilled(j)) {
         string message = "Reaction " + fCrE->GetCREntry(j).GetName() + " + " + ExtractTargets() +  " is not filled";
         TUMessages::Error(stdout, "TUXSections", "IsTotIAFilled", message);
         return false;
      }
   }
   return true;
}

//______________________________________________________________________________
Bool_t TUXSections::IsAllTotINAFilled() const
{
   //--- Returns true if all Inelastic Non-Annihil. X-Secs are filled, false otherwise.
   //    N.B.: only for (anti)-nuclei (LEPTONS X-sec on H, He... irrelevant).

   for (Int_t j = GetNTertiaries() - 1; j >= 0; --j) {
      if (!IsTotINAFilled(j)) {
         string message = "Reaction " + fTertiaries[j] + " + " + ExtractTargets() +  " is not filled";
         TUMessages::Error(stdout, "TUXSections", "IsTotINAFilled", message);
         return false;
      }
   }
   return true;
}

//______________________________________________________________________________
Bool_t TUXSections::IsFileAlreadyLoaded(string const &f_xsec, Bool_t is_verbose, FILE *f_log) const
{
   //--- Returns true if file f_xsec already loaded, false otherwise.
   //  f_xsec            X-Sec file name to check
   //  is_verbose        Verbose on or off
   //  f_log             Log for USINE

   string message = Form("File %s has already been loaded... nothing to do...", f_xsec.c_str());

   for (Int_t i = 0; i < GetNFilesXDiffINA(); ++i) {
      if (fFilesXDiffINA[i] == f_xsec) {
         if (is_verbose)
            TUMessages::Warning(f_log, "TUXSections", "IsFileAlreadyLoaded", message.c_str());
         return true;
      }
   }

   for (Int_t i = 0; i < GetNFilesXProd(); ++i) {
      if (fFilesXProd[i] == f_xsec) {
         if (is_verbose)
            TUMessages::Warning(f_log, "TUXSections", "IsFileAlreadyLoaded", message.c_str());
         return true;
      }
   }
   for (Int_t i = 0; i < GetNFilesXTotIA(); ++i) {
      if (fFilesXTotIA[i] == f_xsec) {
         if (is_verbose)
            TUMessages::Warning(f_log, "TUXSections", "IsFileAlreadyLoaded", message.c_str());
         return true;
      }
   }
   for (Int_t i = 0; i < GetNFilesXTotINA(); ++i) {
      if (fFilesXTotINA[i] == f_xsec) {
         if (is_verbose)
            TUMessages::Warning(f_log, "TUXSections", "IsFileAlreadyLoaded", message.c_str());
         return true;
      }
   }
   return false;
}

//______________________________________________________________________________
Double_t TUXSections::ISMWeightedProd_nvDSigDEk(Int_t j_parent, Int_t j_cr, Int_t k_in, Int_t k_out,
      TUMediumEntry *medium) const
{
   //--- Returns ISM-weighted (ISM entry) differential production rate [/(GeV Myr)].
   //  j_parent            Index of projectile (in list of parents)
   //  j_cr              CR index (in list of CRs)
   //  k_in              Incoming projectile energy (kinetic energy per nucleon)
   //  k_out             Kinetic energy per nucleon of the CR fragment (outgoing)
   //  medium            ISM density for a given space-time point
   //BEGIN_LATEX(fontsize=14, separator='=', align='rl')
   //       < #frac{d#Gamma^{prod}}{dEk_out} (proj, Ekn_{in}, frag, Ekn_{out}) >_{ISM0D} = #sum_{i #in ISM} #left( n_{i} #times v(k_in) #times #frac{d#sigma(j_parent+i #rightarrow j_cr)}{dEk_out} (k_in, k_out) #right)
   //END_LATEX

   // Index parent in list of CR
   Int_t j_in_loc = fCrE->IndexInCRList_Parent(j_cr, j_parent);

   return ISMWeightedProdDSig(j_parent, j_cr, k_in, k_out, medium)
          * fCrE->GetVelocity_cmperMyr(j_in_loc, k_in)
          * TUPhysics::Convert_mb_to_cm2();
}

//______________________________________________________________________________
Double_t TUXSections::ISMWeightedProd_nvSig(Int_t j_parent, Int_t j_cr, Int_t k_out, TUMediumEntry *medium) const
{
   //--- Returns ISM-weighted (ISM entry) production rate in straight-ahead approximation [/Myr].
   //  j_parent            Index of projectile (in list of parents)
   //  j_cr              CR index (in list of CRs)
   //  k_out             Kinetic energy per nucleon of the CR fragment (outgoing)
   //  medium            ISM density for a given space-time point
   //BEGIN_LATEX(fontsize=14, separator='=', align='rl')
   //       < #Gamma^{prod}(proj, frag, Ekn) >_{ISM0D} = #sum_{i #in ISM} #left( n_{i} #times v(k_out) #times #sigma^{j_parent+i #rightarrow j_cr} (k_out) #right)
   //END_LATEX


   // Index parent in list of CR
   Int_t j_in_loc = fCrE->IndexInCRList_Parent(j_cr, j_parent);

   return ISMWeightedProdSig(j_parent, j_cr, k_out, medium)
          * fCrE->GetVelocity_cmperMyr(j_in_loc, k_out)
          * TUPhysics::Convert_mb_to_cm2();
}

//______________________________________________________________________________
Double_t TUXSections::ISMWeightedProdDSig(Int_t j_parent, Int_t j_cr, Int_t k_in, Int_t k_out,
      TUMediumEntry *medium) const
{
   //--- Returns diff. prod. cross section weighted by ISM density.
   //  j_parent            Index of projectile (in list of parents)
   //  j_cr              CR index (in list of CRs)
   //  k_in              Incoming projectile energy (kinetic energy per nucleon)
   //  k_out             Kinetic energy per nucleon of the CR fragment (outgoing)
   //  medium            ISM density for a given space-time point

   Double_t res = 0.;

   for (Int_t t = 0; t < GetNTargets(); ++t) {
      res += GetProdDSigDEk(j_parent, t, j_cr, k_in, k_out) * medium->GetDensity(t);
      // And for ghosts (those with A_ghost<=A_parent)
      if (fCrE->IsGhostsLoaded()) {
         for (Int_t g = fCrE->GetCREntry(j_cr).GetNGhosts() - 1; g >= 0; --g) {
            if (fCrE->GetCREntry(j_cr).GetGhost(g).GetA() > fCrE->GetCREntry(fCrE->IndexInCRList_Parent(j_cr, j_parent)).GetA())
               continue;
            res += GetProdDSigDEk(j_parent, t, j_cr, k_in, k_out, g)
                   * fCrE->GetCREntry(j_cr).GetGhostBr(g) / 100. * medium->GetDensity(t);
         }
      }
   }
   return res;
}

//______________________________________________________________________________
Double_t TUXSections::ISMWeightedProdSig(Int_t j_parent, Int_t j_cr, Int_t k_out, TUMediumEntry *medium) const
{
   //--- Returns prod. (straight-ahead) cross section weighted by ISM density.
   //  j_parent            Index of projectile (in list of parents)
   //  j_cr              CR index (in list of CRs)
   //  k_out             Kinetic energy per nucleon of the CR fragment (outgoing)
   //  medium            ISM density for a given space-time point

   Double_t res = 0.;

   for (Int_t t = 0; t < GetNTargets(); ++t) {
      //if (k_out==GetNE()-1) {
      //   cout << "sigma(" << FormReactionNameProd(j_parent, j_cr, -1, t) << ")="
      //        << GetProdSig(j_parent, t, j_cr, k_out) * medium->GetDensity(t) << endl;
      //}
      res += GetProdSig(j_parent, t, j_cr, k_out) * medium->GetDensity(t);

      // And for ghosts
      if (fCrE->IsGhostsLoaded()) {
         for (Int_t g = fCrE->GetCREntry(j_cr).GetNGhosts() - 1; g >= 0; --g) {
            if (fCrE->GetCREntry(j_cr).GetGhost(g).GetA() > fCrE->GetCREntry(fCrE->IndexInCRList_Parent(j_cr, j_parent)).GetA())
               continue;
            //if (k_out==GetNE()-1) {
            //   cout << "sigma(" << FormReactionNameProd(j_parent, j_cr, g, t) << ")="
            //        << GetProdSig(j_parent, t, j_cr, k_out, g)
            //           * fCrE->GetGhostBr(j_cr, g) /100. * medium->GetDensity(t) << endl;
            //}
            res += GetProdSig(j_parent, t, j_cr, k_out, g)
                   * fCrE->GetCREntry(j_cr).GetGhostBr(g) / 100. * medium->GetDensity(t);
         }
      }
   }

   return res;
}

//______________________________________________________________________________
Double_t TUXSections::ISMWeightedTertINA_nvDSigDEk(Int_t j_tertiary, Int_t k_in, Int_t k_out,
      TUMediumEntry *medium) const
{
   //--- Returns tertiaries (INA) rate: Sum_ISM {n.v(ekn_in).dSigINA/dEkn (ekn_in,ekn_out)} [/(GeV Myr)]
   //  j_tertiary        CR tertiary index (in list of tertiaries)
   //  k_in              Incoming Ekn index (kinetic energy per nucleon)
   //  k_out             Outgoing Ekn index (kinetic energy per nucleon)
   //  medium            ISM density for a given space-time point

   return ISMWeightedTertINADSig(j_tertiary, k_in, k_out, medium)
          * fCrE->GetVelocity_cmperMyr(MapINAIndex2CRIndex(j_tertiary), k_in)
          * TUPhysics::Convert_mb_to_cm2();
}

//______________________________________________________________________________
Double_t TUXSections::ISMWeightedTertINADSig(Int_t j_tertiary, Int_t k_in, Int_t k_out,
      TUMediumEntry *medium) const
{
   //--- Returns INA diff. cross section (tertiaries) weighted by ISM density.
   //  j_tertiary        CR tertiary index (in list of tertiaries)
   //  k_in              Incoming Ekn energy for CR
   //  k_out             Outgoing Ekn energy for CR
   //  medium            ISM density for a given space-time point

   Double_t res = 0.;

   for (Int_t t = 0; t < GetNTargets(); ++t)
      res += GetDSigDEkINA(j_tertiary, t, k_in, k_out) * medium->GetDensity(t);

   return res;
}

//______________________________________________________________________________
Double_t TUXSections::ISMWeightedTotIA_nvSig(Int_t j_cr, Int_t k_ekn, TUMediumEntry *medium) const
{
   //--- Returns tot. inel. annihil. (IA) rate: Sum_ISM {n.v(ekn).SigIA(ekn)} [/Myr].
   //  j_cr              Index in list of CRs
   //  k_ekn             Kinetic energy per nucleon CR
   //  medium            ISM density for a given space-time point

   return ISMWeightedTotIASig(j_cr, k_ekn, medium)
          * fCrE->GetVelocity_cmperMyr(j_cr, k_ekn)
          * TUPhysics::Convert_mb_to_cm2();
}

//______________________________________________________________________________
Double_t TUXSections::ISMWeightedTotIASig(Int_t j_cr, Int_t k_ekn, TUMediumEntry *medium) const
{
   //--- Returns tot. inel annihil. (IA) cross section weighted by ISM density.
   //  j_cr              Index in list of CRs
   //  k_ekn             Kinetic energy per nucleon CR
   //  medium            ISM density for a given space-time point

   Double_t res = 0.;
   for (Int_t t = 0; t < GetNTargets(); ++t)
      res += GetTotIA(j_cr, t, k_ekn) * medium->GetDensity(t);
   return res;
}

//______________________________________________________________________________
Double_t TUXSections::ISMWeightedTotINA_nvSig(Int_t j_tert, Int_t k_ekn, TUMediumEntry *medium) const
{
   //--- Returns tot. inel. non-annihil. (INA) rate: Sum_ISM {n.v(ekn).SigINA(ekn)} [/Myr].
   //  j_tert            Index in list of tertiaries
   //  k_ekn             Kinetic energy per nucleon CR
   //  medium            ISM density for a given space-time point
   //  is_ia_or_ina      Whether this is IA or INA cross section

   return ISMWeightedTotINASig(j_tert, k_ekn, medium)
          * fCrE->GetVelocity_cmperMyr(MapINAIndex2CRIndex(j_tert), k_ekn)
          * TUPhysics::Convert_mb_to_cm2();
}

//______________________________________________________________________________
Double_t TUXSections::ISMWeightedTotINASig(Int_t j_tert, Int_t k_ekn, TUMediumEntry *medium) const
{
   //--- Returns tot. inel non-annihil. (INA) cross section weighted by ISM density.
   //  j_tert            Index in list of tertiaries
   //  k_ekn             Kinetic energy per nucleon CR
   //  medium            ISM density for a given space-time point

   Double_t res = 0.;
   for (Int_t t = 0; t < GetNTargets(); ++t)
      res += GetTotINA(j_tert, t, k_ekn) * medium->GetDensity(t);
   return res;
}

//______________________________________________________________________________
Bool_t TUXSections::IsTargetOrCROrEknEmpty(FILE *f_log) const
{
   //--- Returns true if any of fCrE (CRs or Ekn) or fTargets empty, false otherwise.
   //  f_log             Log for USINE


   if (GetNE() == 0) {
      TUMessages::Warning(f_log, "TUXSections", "IsTargetOrCROrEknEmpty",
                          "Energy grid is empty, fill it first!");
      return true;
   }

   if (GetNCRs() == 0) {
      TUMessages::Warning(f_log, "TUXSections", "IsTargetOrCROrEknEmpty",
                          "List of CRs is empty, fill it first!");
      return true;
   }

   if (GetNTargets() == 0) {
      TUMessages::Warning(f_log, "TUXSections", "IsTargetOrCROrEknEmpty",
                          "List of targets is empty, fill it first!");
      return true;
   }

   return false;
}

//______________________________________________________________________________
void TUXSections::LoadOrUpdateDSigDEkINA(string const &f_xsec, Bool_t is_verbose, FILE *f_log)
{
   //--- Updates differential Inel Non-Annihilating X-Sec from values read in f_xsec.
   //  f_xsec             File from which X-Sec are read
   //  is_verbose        Verbose or not when class is set
   //  f_log             Log for USINE

   string indent = TUMessages::Indent(true);
   TUMessages::Indent(false);

   // Check
   if (IsTargetOrCROrEknEmpty(f_log))
      TUMessages::Error(f_log, "TUXSections", "LoadOrUpdateDSigDEkINA",
                        "List of targets, or CRs, or Ekn grid is empty, fill it first!");

   // Check if file exists
   string file = TUMisc::GetPath(f_xsec);
   ifstream f_read(file.c_str());
   TUMisc::IsFileExist(f_read, file);
   // Find if $USINE, if yes, we replace it in file
   TUMisc::SubstituteEnvInPath(file, "$USINE");

   // If file not previously read, add to list, otherwise returns
   if (is_verbose)
      fprintf(f_log, "%s    - Read %s\n", indent.c_str(), file.c_str());
   if (!IsFileAlreadyLoaded(file, false, f_log))
      fFilesXDiffINA.push_back(file);
   else {
      string message = "X-sec file " + file + " already loaded, nothing to do!";
      TUMessages::Warning(f_log, "TUXSections", "LoadOrUpdateDSigDEkINA", message);
      return;
   }

   // Read X-sec infos
   string f_type, f_unit, f_targets;
   TUAxis axisekn_in, axisekn_out;
   Int_t f_repeatedloop;
   ReadFileHeader(f_read, file, f_type, f_unit, f_targets, axisekn_in, axisekn_out, f_repeatedloop, is_verbose, f_log);
   Int_t f_nin = axisekn_in.GetN();
   Int_t f_nout = axisekn_out.GetN();
   Double_t f_eknmax_in = axisekn_in.GetMax();
   Double_t f_eknmax_out = axisekn_out.GetMax();

   vector<Int_t> t_targs = IndicesTargetsInFile(f_targets);
   Int_t f_ntarg = t_targs.size();


   //------------------------------
   // Loop over all species in file...
   //------------------------------
   string trash;
   do {
      // Each diff X-sec starts with "CRname -> CRname"
      string cr;
      if (!(f_read >> cr)) break;
      string line = TUMisc::RemoveBlancksFromStartStop(cr);
      if (line[0] == '#' || line.empty()) continue;

      f_read >> trash >> trash;
      TUMisc::UpperCase(cr);

      // Does the CRname exist and tertiary exists?
      Int_t j_cr = fCrE->Index(cr, false, stdout, false);
      if (j_cr < 0 || !IsTertiary(j_cr)) {
         // if not, skip cross sections...
         for (Int_t k = f_ntarg * f_nin * f_nout - 1; k >= 0; --k)
            f_read >> trash;
         continue;
      } else {

         Int_t j_tertiary = MapCRIndex2INAIndex(j_cr);

         // If high energies demanded are larger than found in Xfile, abort()
         if (GetEkn(j_cr)->GetMax() > f_eknmax_out) {
            string message = Form("Max energy requested for frag. %s (=%.3le) > available in file (=%.3le): change value in init file!",
                                  fCrE->GetCREntry(j_cr).GetName().c_str(), GetEkn(j_cr)->GetMax(), f_eknmax_out);
            TUMessages::Error(f_log, "TUXSections", "LoadOrUpdateDSigDEkINA", message);
         }
         if (GetEkn(j_cr)->GetMax() > f_eknmax_in) {
            string message = Form("Max energy requested for proj. %s (=%.3le) > available in file (=%.3le): change value in init file!",
                                  fCrE->GetCREntry(j_cr).GetName().c_str(), GetEkn(j_cr)->GetMax(), f_eknmax_in);
            TUMessages::Error(f_log, "TUXSections", "LoadOrUpdateDSigDEkINA", message);
         }

         // Allocate memory for energy grid from file
         vector<Double_t > f_dsig(f_ntarg * f_nout * f_nin, 0.);

         // Read xsec
         if (f_repeatedloop == 2) {
            for (Int_t k_in = 0; k_in < f_nin; ++k_in) {
               for (Int_t k_out = 0; k_out < f_nout; ++k_out) {
                  for (Int_t t = 0; t < f_ntarg; ++t) {
                     Double_t xsec = 0.;
                     f_read >> xsec;
                     if (xsec < 0.) {
                        string message = "Negative value " + (string)Form("%le", xsec) + " found in "
                                         + file + ": set to 0.";
                        TUMessages::Warning(f_log, "TUXSections", "LoadOrUpdateDSigDEkINA", message);
                        xsec = 0.;
                     }
                     f_dsig[t * f_nin * f_nout + k_out * f_nin + k_in] = xsec;
                     if (f_dsig[t * f_nin * f_nout + k_out * f_nin + k_in]) {
                        // All the cross sections must be in mb/GeV !!!
                        if (f_unit == "CM2/GEV")
                           f_dsig[t * f_nin * f_nout + k_out * f_nin + k_in]
                           /= TUPhysics::Convert_mb_to_cm2();
                        else if (f_unit != "MB/GEV") {
                           string message = "In " + file + " unit of the file read is neither mb/GeV nor cm2/GeV";
                           TUMessages::Error(f_log, "TUXSections", "LoadOrUpdateDSigDEkINA", message);
                        }
                     }
                  }
               }
            }
         } else if (f_repeatedloop == 1) {
            for (Int_t k_out = 0; k_out < f_nout; ++k_out) {
               for (Int_t k_in = 0; k_in < f_nin; ++k_in) {
                  for (Int_t t = 0; t < f_ntarg; ++t) {
                     Double_t xsec = 0.;
                     f_read >> xsec;
                     if (xsec < 0.) {
                        string message = "Negative value " + (string)Form("%le", xsec) + " found in "
                                         + file + ": set to 0.";
                        TUMessages::Warning(f_log, "TUXSections", "LoadOrUpdateDSigDEkINA", message);
                        xsec = 0.;
                     }
                     f_dsig[t * f_nin * f_nout + k_out * f_nin + k_in] = xsec;
                     // All the cross sections must be in mb/GeV !!!
                     if (f_unit == "CM2/GEV")
                        f_dsig[t * f_nin * f_nout + k_out * f_nin + k_in] /= TUPhysics::Convert_mb_to_cm2();
                     else if (f_unit != "MB/GEV") {
                        string message = "In " + file + " unit of the file read is neither mb/GeV nor cm2/GeV";
                        TUMessages::Error(f_log, "TUXSections", "LoadOrUpdateDSigDEkINA", message);
                     }
                  }
               }
            }
         }

         //--- FIRST STEP: INTERPOLATION TO THE RIGHT incoming-ENERGIES (and right targets)
         // N.B:   f_dsig = [f_ntarg * f_nout * f_nin]
         //        interp1_dsigh = [GetNTargets() * f_nout * GetNE()]
         vector<Double_t> interp1_dsig(GetNTargets()*f_nout * GetNE(), 0.);
         for (Int_t t = 0; t < GetNTargets(); ++t) {
            for (Int_t k_out = 0; k_out < f_nout; ++k_out) {
               TUMath::Interpolate(axisekn_in.GetVals(), f_nin,
                                   &(f_dsig[t_targs[t]*f_nout * f_nin + k_out * f_nin + 0]), GetEkn(j_cr)->GetVals(), GetNE(),
                                   &(interp1_dsig[t * f_nout * GetNE() + k_out * GetNE() + 0]), kLOGLIN_INFNULL_SUPCST, false, f_log);
            }
         }

         //--- SECOND AND LAST STEP: INTERPOLATION TO THE RIGHT outgoing-ENERGIES
         // N.B:  interp1_dsigh = [GetNTargets() * f_nout * GetNE()]
         //       fDSigDEkINA = [GetNTargets() * GetNE() * GetNE()]
         for (Int_t t = 0; t < GetNTargets(); ++t) {
            for (Int_t k_out = 0; k_out < GetNE(); ++k_out) {
               // Seek position to interpolate energy
               Int_t pos = TUMath::BinarySearch(f_nout, axisekn_out.GetVals(), GetEkn(j_cr)->GetVal(k_out));
               Double_t tmp = 0.;
               for (Int_t k_in = 0; k_in < GetNE(); ++k_in) {

                  if (pos == -1)
                     tmp  = interp1_dsig[t * f_nout * GetNE() + 0 * GetNE() + k_in];
                  else if (pos == f_nout - 1)
                     tmp  = interp1_dsig[t * f_nout * GetNE() + (f_nout - 1) * GetNE() + k_in];
                  else
                     tmp =
                        TUMath::Interpolate(GetEkn(j_cr)->GetVal(k_out), axisekn_out.GetVal(pos), axisekn_out.GetVal(pos + 1),
                                            interp1_dsig[t * f_nout * GetNE() + pos * GetNE() + k_in],
                                            interp1_dsig[t * f_nout * GetNE() + (pos + 1) * GetNE() + k_in],
                                            kLOGLIN);

                  SetDSigDEkINA(j_tertiary, t,  k_in, k_out, tmp);
               }
            }
         }

         if (IsDSigDEkINAFilled(j_tertiary) && is_verbose) {
            string reaction = fCrE->GetCREntry(j_cr).GetName() + " + " + ExtractTargets();;
            string message = "The previously filled "  + reaction + " is overwritten from the new XSec file";
            TUMessages::Warning(f_log, "TUXSections", "LoadOrUpdateDSigDEkINA", message);
         }
         fIsDSigDEkINAFilled[j_tertiary] = true;
      }
   } while (1);
}

//______________________________________________________________________________
void TUXSections::LoadOrUpdateProd(string const &f_xsec, Bool_t is_verbose, FILE *f_log)
{
   //--- Updates production X-Sec from values read in f_xsec: fill the class from
   //    a formatted production X-section file (see in Data.Xsec/ for some samples).
   //    The production X-sec for a given reaction is either in the straight-ahead
   //    approx., or is a differential one. Depending on the keyword in the file read,
   //    call the required method FillSig or FillDSig.
   //  f_xsec            File from which X-Sec are read
   //  is_verbose        Verbose or not when class is set
   //  f_log             Log for USINE

   string indent = TUMessages::Indent(true);
   TUMessages::Indent(false);

   // Check
   if (IsTargetOrCROrEknEmpty(f_log))
      TUMessages::Error(f_log, "TUXSections", "LoadOrUpdateProd",
                        "List of targets, or CRs, or Ekn grid is empty, fill it first!");

   // Check if file exists
   string file = TUMisc::GetPath(f_xsec);
   ifstream f_read(file.c_str());
   TUMisc::IsFileExist(f_read, file);
   // Find if $USINE, if yes, we replace it in file
   TUMisc::SubstituteEnvInPath(file, "$USINE");

   // If file not previously read, add to list, otherwise returns
   if (is_verbose)
      fprintf(f_log, "%s    - Read %s\n", indent.c_str(), file.c_str());
   if (!IsFileAlreadyLoaded(file, false, f_log))
      fFilesXProd.push_back(file);
   else {
      if (is_verbose) {
         string message = "X-sec file " + file + " already loaded, nothing to do!";
         TUMessages::Warning(f_log, "TUXSections", "LoadOrUpdateProd", message);
      }
      return;
   }


   // Read X-sec infos
   string f_type, f_unit, f_targets;
   TUAxis axisekn_in, axisekn_out;
   Int_t f_repeatedloop = 0;
   ReadFileHeader(f_read, file, f_type, f_unit, f_targets, axisekn_in, axisekn_out, f_repeatedloop, is_verbose, f_log);

   if (f_type == "SIGMA")
      LoadOrUpdateProdSig(f_read, file, f_unit, f_targets, axisekn_in, is_verbose, f_log);
   else if (f_type == "DSIGMA")
      LoadOrUpdateProdDSig(f_read, file, f_unit, f_targets, axisekn_in, axisekn_out, f_repeatedloop, is_verbose, f_log);
   else {
      string message = "Unknown format (neither \"SIGMA\" nor \"DSIGMA\") for " + file;
      TUMessages::Error(f_log, "TUXSections", "LoadOrUpdateProd", message);
   }
}

//______________________________________________________________________________
void TUXSections::LoadOrUpdateProdDSig(ifstream &f_read, string const &f_name, string const &f_unit, string const &f_targets,
                                       TUAxis &axisekn_proj, TUAxis &axisekn_frag, Int_t f_repeatedloop,
                                       Bool_t is_verbose, FILE *f_log)
{
   //--- Updates Differential production X-Sec from ifstream container f_read:
   //    fill fDSig and fDSigHe for all reactions (i.e. one reaction per pair
   //    'parent-cr' from CR list).
   //    N.B.: the energy grids is fEkn*fEkn.
   //
   //  f_read            Ifstream container of X-sections
   //  f_name            File name associated to f_read
   //  f_unit            Unit of reaction: mb/GeV or cm2/GeV
   //  f_targets         Comma-separated list of targets
   //  axisekn_proj      Ekn grid of projectile energies in file
   //  axisekn_frag      Ekn grid of fragments energies in file
   //  f_repeatedloop    Order of the loop for incoming and outgoing E (unused if total X-section)
   //  is_verbose        Chatter on or off
   //  f_log             Log for USINE

   Int_t f_nekn_proj = axisekn_proj.GetN();
   Int_t f_nekn_frag = axisekn_frag.GetN();
   Double_t f_eknmax_proj = axisekn_proj.GetMax();
   Double_t f_eknmax_frag = axisekn_frag.GetMax();

   vector<Int_t> t_targs = IndicesTargetsInFile(f_targets);
   Int_t f_ntarg = t_targs.size();
   //---------------------------
   // Loop over all reaction combination in file...
   //---------------------------
   string trash;
   do {
      // Each Xsections start with "name_parent -> fragment"
      // NB: Parent is just another way to denote the projectile...
      string parent;
      string frag;
      if (!(f_read >> parent)) break;
      string line = TUMisc::RemoveBlancksFromStartStop(parent);
      if (line[0] == '#' || line.empty()) continue;
      f_read >> trash >>  frag;
      //----------------------
      // Seek for this combination of parent/fragment in CRs fragments' parent list
      // (this is probably not optimal, but allows to call several "files" to fill
      // the sought quantity).
      //----------------------
      // Does CR fragment and its parent exist in list (or as a ghost)?
      vector<Int_t> cr_indices, parent_indices, ghost_indices;
      vector<string> reactions;
      Bool_t is_ghost_xs, is_skip_xs;
      ExtractIndicesForXSToRead(parent, frag, is_skip_xs, is_ghost_xs,
                                cr_indices, parent_indices, ghost_indices, reactions);
      // If not in list and not a ghost, skip
      if (is_skip_xs) {
         for (Int_t k = f_ntarg * f_nekn_frag * f_nekn_proj - 1; k >= 0; --k)
            f_read >> trash;
         continue;
      }
      // Does the parent exist in the List of Parents (LOP) for this fragment?
      Int_t j_parent_in_loc = fCrE->Index(parent, false, f_log);
      // If high energies demanded are larger than found in Xfile, abort()
      if (GetEkn(j_parent_in_loc)->GetMax() > f_eknmax_proj) {
         string message = Form("Max energy requested for proj. %s (=%.3le) > available in %s (=%.3le): change value in init file!",
                               fCrE->GetCREntry(j_parent_in_loc).GetName().c_str(),
                               GetEkn(j_parent_in_loc)->GetMax(), f_name.c_str(), f_eknmax_proj);
         TUMessages::Error(f_log, "TUXSections", "LoadOrUpdateProdDSig", message);
      }

      // Otherwise, read XS for reaction (parent and fragment exist)
      // ALLOCATE cross sections read from file
      vector<Double_t> f_dsig(f_ntarg * f_nekn_frag * f_nekn_proj, 0.);
      Bool_t is_all_null = true;
      // Parent and fragment exist: fill "f_dsigh/he"
      if (f_repeatedloop == 2) {
         for (Int_t k_in = 0; k_in < f_nekn_proj; ++k_in) {
            for (Int_t k_out = 0; k_out < f_nekn_frag; ++k_out) {
               for (Int_t t = 0; t < f_ntarg; ++t) {
                  Double_t xsec = 0.;
                  f_read >> xsec;
                  if (xsec < 0.) {
                     string message = "Negative value " + (string)Form("%le", xsec) + " found in "
                                      + f_name + ": set to 0.";
                     TUMessages::Warning(f_log, "TUXSections", "LoadOrUpdateProdDSig", message);
                     xsec = 0.;
                  }
                  f_dsig[t * f_nekn_frag * f_nekn_proj + k_out * f_nekn_proj + k_in] = xsec;
                  // All the cross sections must be in mb/GeV !!!
                  if (f_unit == "CM2/GEV")
                     f_dsig[t * f_nekn_frag * f_nekn_proj + k_out * f_nekn_proj + k_in]
                     /= TUPhysics::Convert_mb_to_cm2();
                  else if (f_unit != "MB/GEV") {
                     string message = "In " + f_name + ", unit of the file read is neither mb nor cm2";
                     TUMessages::Error(f_log, "TUXSections", "LoadOrUpdateProdDSig", message);
                  }
                  if (fabs(f_dsig[t * f_nekn_frag * f_nekn_proj + k_out * f_nekn_proj + k_in]) > 1.e-60)
                     is_all_null = false;
               }
            }
         }
      } else if (f_repeatedloop == 1) {
         for (Int_t k_out = 0; k_out < f_nekn_frag; ++k_out) {
            for (Int_t k_in = 0; k_in < f_nekn_proj; ++k_in) {
               for (Int_t t = 0; t < f_ntarg; ++t) {
                  Double_t xsec = 0.;
                  f_read >> xsec;
                  if (xsec < 0.) {
                     string message = "Negative value " + (string)Form("%le", xsec) + " found in "
                                      + f_name + ": set to 0.";
                     TUMessages::Warning(f_log, "TUXSections", "LoadOrUpdateProdDSig", message);
                     xsec = 0.;
                  }
                  f_dsig[t * f_nekn_frag * f_nekn_proj + k_out * f_nekn_proj + k_in] = xsec;
                  // All the cross sections must be in mb/GeV !!!
                  if (f_unit == "CM2/GEV")
                     f_dsig[t * f_nekn_frag * f_nekn_proj + k_out * f_nekn_proj + k_in]
                     /= TUPhysics::Convert_mb_to_cm2();
                  else if (f_unit != "MB/GEV") {
                     string message = "In " + f_name + ", unit of the file read is neither mb nor cm2";
                     TUMessages::Error(f_log, "TUXSections", "LoadOrUpdateProdDSig", message);
                  }
                  if (fabs(f_dsig[t * f_nekn_frag * f_nekn_proj + k_out * f_nekn_proj + k_in]) > 1.e-60)
                     is_all_null = false;
               }
            }
         }
      }

      //--- FIRST STEP: INTERPOLATION TO THE RIGHT projectile-ENERGIES
      // N.B:   f_dsig   = [f_ntarg * f_nekn_frag * f_nekn_proj];
      //    -> tmp_dsig = [GetNTargets() * f_nekn_frag * GetNE()];
      vector<Double_t> tmp_dsig(GetNTargets()*f_nekn_frag * GetNE(), 0.);
      // for each value of k_out, the Xsec column if interpolated
      for (Int_t t = 0; t < GetNTargets(); ++t) {
         for (Int_t k_out = 0; k_out < f_nekn_frag; ++k_out) {
            TUMath::Interpolate(axisekn_proj.GetVals(), f_nekn_proj,
                                &(f_dsig[t_targs[t]*f_nekn_frag * f_nekn_proj + k_out * f_nekn_proj + 0]),
                                GetEkn(j_parent_in_loc)->GetVals(), GetNE(),
                                &(tmp_dsig[t * f_nekn_frag * GetNE() + k_out * GetNE() + 0]), kLOGLIN_INFNULL_SUPCST, false, f_log);
         }
      }

      // This x-sec is a differential cross-section (status is 1 or 11 if only zeros)
      // => fIsSigOrDSig[j_cr][j_parent] = 1 (or 11 if only zeros)
      for (Int_t ii = 0; ii < (Int_t)cr_indices.size(); ++ii) {
         Int_t j_cr = cr_indices[ii];
         Int_t j_parent = parent_indices[ii];
         Int_t g_ghost = ghost_indices[ii];
         string reaction = reactions[ii];

         // If high energies demanded are larger than found in Xfile, abort()
         if (GetEkn(j_cr)->GetMax() > f_eknmax_frag) {
            string message = Form("Max energy requested for frag. %s (=%.3le) > available in %s (=%.3le): change value in init file!",
                                  fCrE->GetCREntry(j_cr).GetName().c_str(), GetEkn(j_cr)->GetMax(), f_name.c_str(), f_eknmax_frag);
            TUMessages::Error(f_log, "TUXSections", "LoadOrUpdateProdDSig", message);
         }

         //--- SECOND AND LAST STEP: INTERPOLATION TO THE RIGHT fragment-ENERGIES
         // tmp_dsig = [GetNTargets() * f_nekn_frag * GetNE()];
         // tmp_dsig_final= [GetNTargets() * GetNE()*GetNE()];
         vector<Double_t> tmp_dsig_final(GetNTargets()* GetNE() * GetNE(), 0.);
         for (Int_t t = 0; t < GetNTargets(); ++t) {
            for (Int_t k_out = 0; k_out < GetNE(); ++k_out) {
               // Seek position to interpolate energy
               Int_t pos = TUMath::BinarySearch(f_nekn_frag, axisekn_frag.GetVals(), GetEkn(j_cr)->GetVal(k_out));
               Double_t tmp = 0.;
               for (Int_t k_in = 0; k_in < GetNE(); ++k_in) {
                  if (pos == -1)
                     tmp  = tmp_dsig[t * f_nekn_frag * GetNE() + 0 * GetNE() + k_in];
                  else if (pos == f_nekn_frag - 1)
                     tmp  = tmp_dsig[t * f_nekn_frag * GetNE() + (f_nekn_frag - 1) * GetNE() + k_in];
                  else
                     tmp =
                        TUMath::Interpolate(GetEkn(j_cr)->GetVal(k_out), axisekn_frag.GetVal(pos),
                                            axisekn_frag.GetVal(pos + 1),
                                            tmp_dsig[t * f_nekn_frag * GetNE() + pos * GetNE() + k_in],
                                            tmp_dsig[t * f_nekn_frag * GetNE() + (pos + 1) * GetNE() + k_in],
                                            kLOGLIN);
                  tmp_dsig_final[t * GetNE()*GetNE() + k_out * GetNE() + k_in] = tmp;
                  //if (t==0 && j_cr==1 && j_parent==1)
                  //   cout << k_in << " " << k_out << " " << GetProdDSigDEk(j_parent, t, j_cr, k_in, k_out) << endl;
               }
            }
         }

         // Depending on XS current status, different operations required:
         //   - status = -1 <=> was not filled
         //       => Allocate fProdDSigDEk[NTarg*j_cr][j_parent] (and fill it)
         //   - status = 0 (or 10) <=> was previously filled with straight-ahead
         //       => Delete fProdSig[NTarg*j_cr][j_parent], then allocate fProdDSigDEk (and fill it)
         //   - status = 1 (or 11) <=> was previously filled with diff. X-sec
         //       => Only fill it
         // N.B.: for each case, status has now to be set to 1 (diff. X-sec)
         //       or 11 (if only zero values for all x-sections)
         string message = "";
         DeleteProdSigAllTargets(j_parent, j_cr, g_ghost);
         switch (GetProdStatus(j_parent, j_cr, g_ghost)) {
            case -1:
               AllocateProdDSigAllTargets(j_parent, j_cr, g_ghost);
               break;
            case 0:
            case 10:
               if (is_verbose) {
                  message = reaction + " => allocate and load differential XS (delete previously read straight-ahead XS)";
                  TUMessages::Warning(f_log, "TUXSections", "LoadOrUpdateProdDSig", message);
               }
               AllocateProdDSigAllTargets(j_parent, j_cr, g_ghost);
               break;
            case 1:
            case 11:
               if (is_verbose) {
                  message = reaction + " => update differential XS (replace previously read XS)";
                  TUMessages::Warning(f_log, "TUXSections", "LoadOrUpdateProdDSig", message);
               }
               break;
         }

         // This x-sec is a differential cross-section
         // => status set to 1 (or 11, if only zero)
         if (is_all_null)
            SetProdStatus(j_parent, j_cr, 11, g_ghost);
         else
            SetProdStatus(j_parent, j_cr, 1, g_ghost);

         // Fill ProdDSig
         for (Int_t t = 0; t < GetNTargets(); ++t) {
            for (Int_t k_out = 0; k_out < GetNE(); ++k_out) {
               for (Int_t k_in = 0; k_in < GetNE(); ++k_in)
                  SetProdDSigDEk(j_parent, t, j_cr, k_in, k_out, tmp_dsig_final[t * GetNE()*GetNE() + k_out * GetNE() + k_in], g_ghost);
            }
         }
      }
   } while (1);
}

//______________________________________________________________________________
void TUXSections::LoadOrUpdateProdSig(ifstream &f_read, string const &f_name, string const &f_unit, string const &f_targets,
                                      TUAxis &axisekn, Bool_t is_verbose, FILE *f_log)
{
   //--- Updates Straight-ahead production X-Sec from ifstream container f_read:
   //    fill fSig and fSigHe for all reactions (one reac per pair 'parent/cr' in CRs).
   //    N.B.: energy grid is Ekn (incoming and outgoing energies are the same).
   //  f_read            Ifstream container of X-sections
   //  f_name            File name associated to f_read
   //  f_unit            Unit of reaction: mb or cm2
   //  f_targets         Comma-separated list of targets
   //  axisekn_in        Ekn grid of projectile energies in file
   //  is_verbose        Chatter or on off
   //  f_log             Log for USINE

   Int_t f_nekn = axisekn.GetN();
   vector<Int_t> t_targs = IndicesTargetsInFile(f_targets);
   Int_t f_ntarg = t_targs.size();

   //---------------------------
   // Loop over all parents/fragment combination in file...
   //---------------------------
   string trash;
   do {
      // Each Xsections start with "name_parent -> fragment"
      string parent = "", frag = "";
      if (!(f_read >> parent)) break;
      string line = TUMisc::RemoveBlancksFromStartStop(parent);
      if (line[0] == '#' || line.empty()) continue;
      f_read >> trash >> frag;
      string reaction = parent + " + " + ExtractTargets() + " -> " + frag;

      //----------------------
      // Seek for this combination of parent/fragment in CRs fragments' parent list
      // (this is probably not optimal, but allows to call several "files" to fill
      // the sought quantity).
      //----------------------
      // Does CR fragment and its parent exist in list (or as a ghost)?
      vector<Int_t> cr_indices, parent_indices, ghost_indices;
      vector<string> reactions;
      Bool_t is_ghost_xs, is_skip_xs;
      ExtractIndicesForXSToRead(parent, frag, is_skip_xs, is_ghost_xs,
                                cr_indices, parent_indices, ghost_indices, reactions);

      // If not in list and not a ghost, skip
      if (is_skip_xs) {
         for (Int_t k = f_ntarg * f_nekn - 1; k >= 0; --k)
            f_read >> trash;
         continue;
      } else { // Otherwise, read XS for reaction (parent and fragment exist)
         // Fill temporary "tmpsigma" and "f_vekn"...
         Bool_t is_all_null = true;
         vector<Double_t> tmp_sig(f_ntarg * f_nekn, 0.);
         for (Int_t k = 0; k < f_nekn; ++k) {
            for (Int_t t = 0; t < f_ntarg; ++t) {
               Double_t xsec = 0.;
               f_read >> xsec;

               if (xsec < 0.) {
                  string message = "Negative value " + (string)Form("%le", xsec) + " found in "
                                   + f_name + ": set to 0.";
                  TUMessages::Warning(f_log, "TUXSections", "LoadOrUpdateProdSig", message);
                  xsec = 0.;
               }
               tmp_sig[t * f_nekn + k] = xsec;
               // All the cross sections must be in mb !!!
               if (f_unit == "CM2")
                  tmp_sig[t * f_nekn + k] /= TUPhysics::Convert_mb_to_cm2();
               else if (f_unit != "MB")
                  TUMessages::Error(f_log, "TUXSections", "LoadOrUpdateProdSig",
                                    "Unit of the file read is neither mb nor cm2");
               if (fabs(tmp_sig[t * f_nekn + k] > 1.e-60))
                  is_all_null = false;
            }
         }

         // Use read XS extrapolated to desired energies (tmp_sig_final)
         // to fill all relevant XS (at least one, multiple for some ghosts)
         for (Int_t ii = 0; ii < (Int_t)cr_indices.size(); ++ii) {
            Int_t j_cr = cr_indices[ii];
            Int_t j_parent = parent_indices[ii];
            Int_t g_ghost = ghost_indices[ii];

            // 1DInterpolate...
            vector<Double_t> tmp_sig_final(GetNTargets() * GetNE(), 0.);
            for (Int_t t = 0; t < GetNTargets(); ++t) {
               TUMath::Interpolate(axisekn.GetVals(),
                                   f_nekn, &(tmp_sig[t_targs[t]*f_nekn + 0]),
                                   GetEkn(j_cr)->GetVals(), GetNE(), &tmp_sig_final[t * GetNE() + 0], kLOGLIN_INFNULL_SUPCST, false, f_log);
            }

            // Depending on XS status, different operations required:
            //   - status = -1 <=> was not filled
            //       => Allocate fProdSig[NTarg*j_cr][j_parent] (and fill it)
            //   - status = 0 (or 10) <=> was previously filled with straight-ahead
            //       => Only fill it
            //   - status = 1 (or 11) <=> was previously filled with diff. X-sec
            //       => Delete fProdDSigDEk[NTarg*j_cr][j_parent], then allocate fProdSig (and fill it)
            // N.B.: for each case, status has now to be set to 0 (straight-ahead)
            //       or 10 (if only zero values for all x-sections)
            string message = "";
            DeleteProdDSigAllTargets(j_parent, j_cr, g_ghost);
            switch (GetProdStatus(j_parent, j_cr, g_ghost)) {
               case -1:
                  AllocateProdSigAllTargets(j_parent, j_cr, g_ghost);
                  break;
               case 0:
               case 10:
                  if (is_verbose) {
                     message = reactions[ii] + (string)" => update straight-ahead XS (replace previously read straight-ahead XS)";
                     TUMessages::Warning(f_log, "TUXSections", "LoadOrUpdateProdSig", message);
                  }
                  break;
               case 1:
               case 11:
                  if (is_verbose) {
                     message = reaction[ii] + (string)" => allocate and load straigh-ahead XS (delete previously read differential XS)";
                     TUMessages::Warning(f_log, "TUXSections", "LoadOrUpdateProdSig", message);
                  }
                  AllocateProdSigAllTargets(j_parent, j_cr, g_ghost);
                  break;
            }
            // This x-sec is a straight-ahead cross-section
            // => status set to 0 (or 10, if only zero)
            if (is_all_null)
               SetProdStatus(j_parent, j_cr, 10, g_ghost);
            else
               SetProdStatus(j_parent, j_cr, 0, g_ghost);

            // Fill ProdSig
            for (Int_t t = 0; t < GetNTargets(); ++t) {
               for (Int_t k_ekn = 0; k_ekn < GetNE(); ++k_ekn)
                  SetProdSig(j_parent, t, j_cr, k_ekn, tmp_sig_final[t * GetNE() + k_ekn], g_ghost);
            }
         }
      }
   } while (1);
}

//______________________________________________________________________________
void TUXSections::LoadOrUpdateTotInel(string const &f_xsec, Bool_t is_ia_or_ina, Bool_t is_verbose, FILE *f_log)
{
   //--- Updates Tot. Inel. X-Sec from file f_xsec.
   //    N.B.: if a second file is read, (i) either fill the empty X-section found,
   //    or (ii) override the filled value with the new one found.
   //  f_xsec            File from which X-Sec are read
   //  is_ia_or_ina      Switch for Inel.ann. or Inel.non-ann. X-Sec
   //  is_verbose        Verbose or not when class is set
   //  f_log             Log for USINE

   string indent = TUMessages::Indent(true);
   TUMessages::Indent(false);

   // Check
   if (IsTargetOrCROrEknEmpty(f_log))
      TUMessages::Error(f_log, "TUXSections", "LoadOrUpdateTotInel",
                        "List of targets, or CRs, or Ekn grid is empty, fill it first!");

   // Check if file exists
   string file = TUMisc::GetPath(f_xsec);
   ifstream f_read(file.c_str());
   TUMisc::IsFileExist(f_read, file);
   // Find if $USINE, if yes, we replace it in file
   TUMisc::SubstituteEnvInPath(file, "$USINE");

   // If file not previously read, add to list, otherwise returns
   if (is_verbose)
      fprintf(f_log, "%s    - Read %s\n", indent.c_str(), file.c_str());
   if (!IsFileAlreadyLoaded(file, false, f_log)) {
      if (is_ia_or_ina)
         fFilesXTotIA.push_back(file);
      else
         fFilesXTotINA.push_back(file);
   } else {
      string message = "X-sec file " + file + " already loaded, nothing to do!";
      TUMessages::Warning(f_log, "TUXSections", "LoadOrUpdateTotInel", message);
      return;
   }


   // Read X-sec infos
   string f_type, f_unit, f_targets;
   TUAxis axisekn, axisdummy;
   Int_t dummy;
   ReadFileHeader(f_read, file, f_type, f_unit, f_targets, axisekn, axisdummy, dummy, is_verbose, f_log);
   Int_t f_nekn = axisekn.GetN();

   vector<Int_t> t_targs = IndicesTargetsInFile(f_targets);
   Int_t f_ntarg = t_targs.size();

   //------------------------------
   // Loop over all species in file...
   //------------------------------
   string trash;
   do {
      // Each Total X-sec starts with "CR-name"
      string cr;
      if (!(f_read >> cr)) break;
      string line = TUMisc::RemoveBlancksFromStartStop(cr);
      if (line[0] == '#' || line.empty()) continue;

      // Does the CRname exist and if non-ann X-sec, does tertiary exists?
      Int_t j_cr = fCrE->Index(cr, false, stdout, false);
      if (j_cr < 0 || (!is_ia_or_ina && !IsTertiary(j_cr))) {
         // if not, skip cross sections...
         for (Int_t k = f_ntarg * f_nekn - 1; k >= 0; --k)
            f_read >> trash;
         continue;
      } else {
         // fill temporary arrays for one nucleus (all energies)
         vector<Double_t> sig(f_ntarg * f_nekn, 0.);
         for (Int_t k = 0; k < f_nekn; ++k) {
            for (Int_t t = 0; t < f_ntarg; ++t) {
               Double_t xsec = 0.;
               f_read >> xsec;
               if (xsec < 0.) {
                  string message = "Negative value " + (string)Form("%le", xsec) + " found in "
                                   + file + ": set to 0.";
                  TUMessages::Warning(f_log, "TUXSections", "LoadOrUpdateTotInel", message);
                  xsec = 0.;
               }
               sig[t * f_nekn + k] = xsec;

               // All the cross sections must be in mb !!!
               if (f_unit == "CM2")
                  sig[t * f_nekn + k] /= TUPhysics::Convert_mb_to_cm2();
               else if (f_unit != "MB")
                  TUMessages::Error(f_log, "TUXSecTot", "LoadOrUpdateTotInel",
                                    "Unit of the file read is neither mb nor cm2");
            }
         }

         // Interpolate...
         if (is_ia_or_ina) {
            // Check whether needs to overwrite some previously loaded X-sec
            if (IsTotIAFilled(j_cr) && is_verbose) {
               string reaction = fCrE->GetCREntry(j_cr).GetName() + " + " + ExtractTargets();
               string message = "The previously filled "  + reaction + " is overwritten from the new XSec file";
               TUMessages::Warning(f_log, "TUXSections", "LoadOrUpdateTotInel", message);
            }

            for (Int_t t = 0; t < GetNTargets(); ++t)
               TUMath::Interpolate(axisekn.GetVals(),
                                   f_nekn, &sig[t_targs[t]*f_nekn + 0], GetEkn(j_cr)->GetVals(),
                                   GetNE(), &fTotIA[t * GetNCRs()*GetNE() + j_cr * GetNE() + 0], kLOGLIN_INFNULL_SUPCST, false, f_log);
            fIsTotIAFilled[j_cr] = true;
         } else {
            Int_t j_tertiary = MapCRIndex2INAIndex(j_cr);

            // Check whether needs to overwrite some previously loaded X-sec
            if (IsTotINAFilled(j_tertiary) && is_verbose) {
               string reaction = fCrE->GetCREntry(j_cr).GetName() + " + " + ExtractTargets();
               string message = "The previously filled "  + reaction
                                + " is overwritten from the new XSec file";
               TUMessages::Warning(f_log, "TUXSections", "LoadOrUpdateTotInel", message);
            }

            for (Int_t t = 0; t < GetNTargets(); ++t)
               TUMath::Interpolate(axisekn.GetVals(), f_nekn,
                                   &sig[t_targs[t]*f_nekn + 0], GetEkn(j_cr)->GetVals(), GetNE(),
                                   &fTotINA[t * GetNTertiaries()*GetNE() + j_tertiary * GetNE() + 0], kLOGLIN_INFNULL_SUPCST, false, f_log);

            fIsTotINAFilled[j_tertiary] = true;
         }
      }
   } while (1);
}

//______________________________________________________________________________
TGraph *TUXSections::OrphanGetTGraphSigProd(Int_t j_parent, Int_t t_targ, Int_t j_frag, Int_t g_ghost) const
{
   //--- Returns ROOT TGraph for straight-ahead production X-Sec j_parent+t_targ->j_frag.
   //  j_parent             Projectile (parent) index for fragment (CR) j_frag
   //  t_targ             Target index
   //  j_frag             Fragment (CR) index
   //  g_ghost            Ghost index (if ghost enabled)

   string proj = fCrE->GetCREntry(fCrE->IndexInCRList_Parent(j_frag, j_parent)).GetName();
   string targ = GetNameTarget(t_targ);
   string frag = fCrE->GetCREntry(j_frag).GetName();

   // Create and fill histo
   string gr_title = proj + "+" + targ + " -> " + frag;
   string gr_name = "xs_" + proj + "_" + targ + "_" + frag;

   TGraph *gr = new TGraph(GetNE());
   for (Int_t k = 0; k < GetNE(); ++k)
      gr->SetPoint(k, GetEkn(j_frag)->GetVal(k), GetProdSig(j_parent, t_targ, j_frag, k, g_ghost));
   if (g_ghost != -1) {
      // Update name
      string ghost = fCrE->GetCREntry(j_frag).GetGhost(g_ghost).GetName();
      gr_title = proj + "+" + targ + " -> (" + ghost + "->)" + frag;
      gr_name = "xs_" + proj + "_" + targ + "_" + frag + "__" + ghost;
   }
   TUMisc::RemoveSpecialChars(gr_name);
   gr->SetName(gr_name.c_str());
   gr->SetTitle(gr_title.c_str());
   gr->GetXaxis()->SetTitle(GetENativeNameUnit(j_frag).c_str());
   gr->GetYaxis()->SetTitle("#sigma_{straight-ahead} [mb]");
   gr->GetXaxis()->CenterTitle();
   gr->GetYaxis()->CenterTitle();

   return gr;
}

//______________________________________________________________________________
TGraph *TUXSections::OrphanGetTGraphSigInel(Int_t j_cr_or_tertiary, Int_t t_targ,
      Bool_t is_ia_or_ina) const
{
   //--- Returns ROOT TGraph for Tot. Inel. (Ann or Non-Ann) X-Sec j_cr+t_targ->X.
   //  j_cr_or_tertiary   Index in list of CRs (is_ia_or_ina=true) or in list of tertiaries (is_ia_or_ina=false)
   //  t_targ             Target index
   //  is_ia_or_ina       Switch for Inel.ann. or Inel.non-ann. X-Sec

   Int_t j_cr = j_cr_or_tertiary;
   if (!is_ia_or_ina)
      j_cr = MapINAIndex2CRIndex(j_cr_or_tertiary);

   string proj = fCrE->GetCREntry(j_cr).GetName();
   string targ = GetNameTarget(t_targ);

   // Create and fill histo
   string xsec_type = "Tot.Inel.NonAnn";
   if (is_ia_or_ina) xsec_type = "Tot.Inel.Ann";
   string gr_title = proj + "+" + targ + " (" + xsec_type + ")";
   string gr_name = "xs_" + proj + "_" + targ + "_" + xsec_type;
   TUMisc::RemoveSpecialChars(gr_name);

   TGraph *gr = NULL;
   if (is_ia_or_ina)
      gr = new TGraph(GetNE(), GetEkn(j_cr)->GetVals(), GetTotIA(j_cr, t_targ));
   else
      gr = new TGraph(GetNE(), GetEkn(j_cr)->GetVals(), GetTotINA(j_cr_or_tertiary, t_targ));
   gr->SetName(gr_name.c_str());
   gr->SetTitle(gr_title.c_str());
   gr->GetXaxis()->SetTitle(GetENativeNameUnit(j_cr).c_str());
   string y_title = "#sigma_{Non-Ann} [mb]";
   if (is_ia_or_ina) y_title = "#sigma_{Ann} [mb]";
   gr->GetYaxis()->SetTitle(y_title.c_str());
   gr->GetXaxis()->CenterTitle();
   gr->GetYaxis()->CenterTitle();

   return gr;
}

//______________________________________________________________________________
TH2D *TUXSections::OrphanGetTH2DSigINA(Int_t j_tertiary, Int_t t_targ) const
{
   //--- Returns ROOT TH2 for dSig/dEkn Non-Ann X-Sec j_cr+t_targ->j_cr.
   //  j_tertiary         CR tertiary index (in list of tertiaries)
   //  t_targ             Target index

   Int_t j_cr = MapINAIndex2CRIndex(j_tertiary);
   string cr = fCrE->GetCREntry(j_cr).GetName();
   string targ = GetNameTarget(t_targ);

   // Create and fill histo
   string h2_title = cr + "+" + targ + " -> " + cr;
   string h2_name = "xs_dsigdekn_" + cr + "_" + targ + "_" + cr;
   TUMisc::RemoveSpecialChars(h2_name);

   TH2D *h2d = new TH2D(h2_name.c_str(), h2_title.c_str(),
                        GetNE(), log10(GetEkn(j_cr)->GetMin()), log10(GetEkn(j_cr)->GetMax()),
                        GetNE(), log10(GetEkn(j_cr)->GetMin()), log10(GetEkn(j_cr)->GetMax()));
   for (Int_t k_in = 0; k_in < GetNE(); ++k_in)
      for (Int_t k_out = 0; k_out < GetNE(); ++k_out)
         h2d->SetBinContent(k_in, k_out, GetDSigDEkINA(j_tertiary, t_targ, k_in, k_out));

   // Define in and out axis title
   string in = GetENativeNameUnit(j_cr);
   in.insert(in.find_first_of(" "), "_{in} / ");
   in.insert(0, "log_{10} (");
   in.append(" )");

   string out = GetENativeNameUnit(j_cr);
   out.insert(out.find_first_of(" "), "_{out} / ");
   out.insert(0, "log_{10} (");
   out.append(" )");

   gStyle->SetOptStat("ne");
   h2d->GetXaxis()->SetTitle(in.c_str());
   h2d->GetYaxis()->SetTitle(out.c_str());
   h2d->GetZaxis()->SetTitle("d#sigma/dEtot [mb/GeV]");
   h2d->GetXaxis()->CenterTitle();
   h2d->GetYaxis()->CenterTitle();
   h2d->GetZaxis()->CenterTitle();
   return h2d;
}

//______________________________________________________________________________
TH2D *TUXSections::OrphanGetTH2DSigProd(Int_t j_parent, Int_t t_targ, Int_t j_frag, Int_t g_ghost) const
{
   //--- Returns ROOT TH2 for differential production X-Sec j_parent+t_targ->j_frag.
   //  j_parent             Projectile (parent) index for fragment (CR) j_frag
   //  t_targ             Target index
   //  j_frag             Fragment (CR) index
   //  g_ghost            Ghost index (if ghost enabled)

   Int_t j_parent_in_loc = fCrE->IndexInCRList_Parent(j_frag, j_parent);
   string proj = fCrE->GetCREntry(j_parent_in_loc).GetName();
   string targ = GetNameTarget(t_targ);
   string frag = fCrE->GetCREntry(j_frag).GetName();

   // Create and fill histo
   string h2_title = proj + "+" + targ + " -> " + frag;
   string h2_name = "xs_dsigdekn_" + proj + "_" + targ + "_" + frag;
   TUMisc::RemoveSpecialChars(h2_name);
   TH2D *h2d = new TH2D(h2_name.c_str(), h2_title.c_str(),
                        GetNE(), log10(GetEkn(j_parent_in_loc)->GetMin()), log10(GetEkn(j_parent_in_loc)->GetMax()),
                        GetNE(), log10(GetEkn(j_frag)->GetMin()), log10(GetEkn(j_frag)->GetMax()));
   for (Int_t k_in = 0; k_in < GetNE(); ++k_in)
      for (Int_t k_out = 0; k_out < GetNE(); ++k_out)
         h2d->SetBinContent(k_in, k_out, GetProdDSigDEk(j_parent, t_targ, j_frag, k_in, k_out, g_ghost));
   if (g_ghost != -1) {
      // Update name
      string ghost = fCrE->GetCREntry(j_frag).GetGhost(g_ghost).GetName();
      h2_title = proj + "+" + targ + " -> (" + ghost + "->)" + frag;
      h2_name = "xs_dsigdekn_" + proj + "_" + targ + "_" + frag + "__" + ghost;
      TUMisc::RemoveSpecialChars(h2_name);
      h2d->SetTitle(h2_title.c_str());
      h2d->SetName(h2_name.c_str());
   }


   // Define in and out axis title
   string in = GetENativeNameUnit(j_parent_in_loc);
   in.insert(in.find_first_of(" "), "_{proj} / ");
   in.insert(0, "log_{10} (");
   in.append(" )");

   string out = GetENativeNameUnit(j_frag);
   out.insert(out.find_first_of(" "), "_{frag} / ");
   out.insert(0, "log_{10} (");
   out.append(" )");

   gStyle->SetOptStat("ne");
   h2d->GetXaxis()->SetTitle(in.c_str());
   h2d->GetYaxis()->SetTitle(out.c_str());
   h2d->GetZaxis()->SetTitle("d#sigma/dEtot_{proj} [mb/GeV]");
   h2d->GetXaxis()->CenterTitle();
   h2d->GetYaxis()->CenterTitle();
   h2d->GetZaxis()->CenterTitle();

   return h2d;
}

//______________________________________________________________________________
void TUXSections::ParsXS_AddPar(TUFreeParList *pars, Int_t i_par, Bool_t is_use_or_copy, FILE *f_log)
{
   //--- Checks whether pars contain valid XS parameters, and add in fParsXS_ReacIndices and fFreePars.
   //  pars              Free parameters
   //  i_par             Index of parameter to check (in 'pars')
   //  is_use_or_copy    Whether to use or copy XS-related pars in fFreePars
   //  f_log             Log for USINE

   string par_name = pars->GetParEntry(i_par)->GetName();
   // Check if parameter already in list
   if (fFreePars->IndexPar(par_name) >= 0) {
      string message = Form("parameter %s already present in list, nothing to do", par_name.c_str());
      TUMessages::Warning(f_log, "TUXSections", "ParsXS_AddPar", message);
      return;
   }

   // Parameter for XS should be formatted as "X_Y", with
   //    X = key -> see list in parsxs_keys (TUXSectios.h)
   //      - "Standard" keys
   //         - 'Norm' (overall normalisation),
   //         - 'EknScale' (global energy scale)
   //         - 'EknThresh' (threshold energy) and 'SlopeLE' (to multiply by Ekn/Ekn_thresh)^slope below Ekn_thresh)
   //      - linear combination (inel and/or production)
   //         - inel: "LCInelBar94", "LCInelTri99", "LCInelWeb03", "LCInelWel97",
   //         - prod: "LCProdGal17", "LCProdSou01", "LCProdWeb03", "LCProdWKS98"

   //    Y = reaction
   //      - inel. ann (IA):
   //          -> 'CR+T' (to apply on CR total inelastic XS with T a target element (e.g., H, He...))
   //          -> 'ANY+T' means all CRs on target T
   //          -> 'CR+ANY' means CR interacting on all targets
   //      - inel. non-ann (INA):
   //          -> 'CR+T->CR' (to apply to non-annihilating rescattering XS)
   //          -> 'CR+ANY->CR' means rescattering on all targets
   //      - prod:
   //          -> 'CR1+T->CR2' (to apply on CR1->CR2 production XS with T a target element (e.g., H, He...))
   //          -> 'ANY+T->CR2' means all parents on target T leading to CR2
   //          -> 'CR1+ANY->CR2' means CR1 giving CR2 for all targets
   //          -> 'CR1+T->ANY' means CR1 on target T giving any product
   //          -> 'CR1+X->ANY' means CR1 on any target giving any product
   //          -> 'ANY+T->ANY' means any production reaction on target T
   //          -> 'ANY+ANY->CR2' means all reactions on any target giving CR2
   //      - 'ALL'
   //           -> applies to all XS: inelastic, production, rescattering (if not linear combination)
   //           -> applies to linear combination for inelastic (for key LCInel...) and/or production (for key LCProd...)

   vector<string> par_reac;
   TUMisc::String2List(par_name, "_", par_reac);
   if (par_reac.size() != 2)
      return;

   // Initialise reac and update some quantities
   XSReacPars_t reac;
   ParsXS_InitReac(reac);
   reac.ipar_lcinel.assign(4, -1); // Allow only 4 inelastic XS to combine
   reac.ipar_lcprod.assign(4, -1); // Allow only 4 production XS to combine
   reac.name = par_reac[1];

   const Int_t n_keys = parsxs_keys.size();

   for (Int_t i = 0; i < n_keys; ++i)
      TUMisc::UpperCase(parsxs_keys[i]);

   TUMisc::UpperCase(par_reac[0]);
   // 1) Check parameter key and store index in fFreePars
   Bool_t is_key = false;
   Bool_t is_lcinel = false;
   Bool_t is_lcprod = false;
   for (Int_t i = 0; i < n_keys; ++i) {
      if (par_reac[0] == parsxs_keys[i]) {
         is_key = true;
         switch (i) {
            case 0:
               reac.ipar_xsnorm = fFreePars->GetNPars();
               break;
            case 1:
               reac.ipar_eaxisscale = fFreePars->GetNPars();
               break;
            case 2:
               reac.ipar_enhancepowhe = fFreePars->GetNPars();
               break;
            case 3:
               reac.ipar_ethresh = fFreePars->GetNPars();
               break;
            case 4:
               reac.ipar_slopele = fFreePars->GetNPars();
               break;
            case 5:
            case 6:
            case 7:
            case 8:
               reac.ipar_lcinel[i - 5] = fFreePars->GetNPars();
               is_lcinel = true;
               break;
            case 9:
            case 10:
            case 11:
            case 12:
               reac.ipar_lcprod[i - 9] = fFreePars->GetNPars();
               is_lcprod = true;
               break;
         }
         break;
      }
   }
   // If not valid key, means that not a XS parameter and skip!
   if (!is_key) {
      //string message = Form("%s is not an XS parameters (skipped): format is \'key_reac\' with key in %s",
      //                      par_name.c_str(), TUMisc::List2String(keys, ',').c_str());
      //TUMessages::Warning(f_log, "TUXSections", "ParsXS_AddPar", message);
      return;
   }


   // 1) Check format
   // separator '+' or keyword 'ALL'
   if (par_reac[1].find("+") == string::npos && par_reac[1] != "ALL") {
      string message = Form("%s is not a valid reaction (target missing), format CR+T or PROJ+T->CR or ALL expected, cannot add XS parameter %s", par_reac[1].c_str(), par_name.c_str());
      TUMessages::Error(f_log, "TUXSections", "ParsXS_AddPar", message);
      return;
   }

   // 2) Extract 'ALL' (global parameter) or reaction
   if (par_reac[1] == "ALL") {
      if (is_lcinel)
         reac.is_all_lcinel = true;
      else if (is_lcprod)
         reac.is_all_lcprod = true;
      else
         reac.is_all = true;
   } else if (par_reac[1] == "ANY+ANY" || par_reac[1] == "ANY+ANY->ANY") {
      string message = Form("%s is not an allowed combination", par_reac[1].c_str());
      TUMessages::Error(f_log, "TUXSections", "ParsXS_AddPar", message);
      return;
   } else {
      vector<string> split_reac;
      // N.B. we do not use "+->" but "+>" because split issue with antinuclei (e.g. 1H-BAR)
      TUMisc::String2List(par_reac[1], "+>", split_reac);

      // Find target
      // If nar or prod, remove remaining '-' from initial '->'
      if (split_reac[1][split_reac[1].size() - 1] == '-')
         split_reac[1].erase(split_reac[1].size() - 1);
      if (split_reac[1] == "ANY")
         reac.is_all_targ = true;
      else {

         reac.i_targ = TUMisc::IndexInList(fTargets, split_reac[1], true);
         if (reac.i_targ < 0) {
            string message = Form("%s is not a target (select from %s), cannot add XS reaction %s",
                                  split_reac[1].c_str(), TUMisc::List2String(fTargets, ',').c_str(), par_name.c_str());
            TUMessages::Error(f_log, "TUXSections", "ParsXS_AddPar", message);
            return;
         }
      }

      // Find inel/prod/non-ann XS and their CR indices
      if (split_reac[0] == "ANY")
         reac.is_all_proj = true;
      else {
         reac.i_proj = fCrE->Index(split_reac[0], false);
         if (reac.i_proj < 0) {
            string message = Form("%s is not in CR list, cannot add XS reaction %s", split_reac[0].c_str(), par_name.c_str());
            TUMessages::Error(f_log, "TUXSections", "ParsXS_AddPar", message);
            return;
         }
      }

      if (split_reac.size() == 2) // inel.
         reac.is_xs_ia = true;
      else if (split_reac.size() == 3) {

         // Set for
         //   1) CR+T->CR (rescattering)
         //   2) PROJ+T->CR (production)
         if (split_reac[0] != "ANY" && split_reac[0] == split_reac[2]) {
            // If Non-annihilating reaction
            reac.is_xs_ina = true;
            Int_t j_cr = fCrE->Index(split_reac[0], false);
            if (!IsTertiary(j_cr)) {
               string message = Form("%s is not in list of tertiary species, cannot add XS reaction %s", split_reac[0].c_str(), par_name.c_str());
               TUMessages::Error(f_log, "TUXSections", "ParsXS_AddPar", message);
               return;
            } else {
               reac.i_proj = j_cr;
               reac.i_frag = j_cr;
               reac.i_tert = MapCRIndex2INAIndex(j_cr);
            }
         } else {
            // Production XS
            reac.is_xs_prod = true;

            // Look at fragment
            if (split_reac[2] == "ANY")
               reac.is_all_frag = true;
            else {
               reac.i_frag = fCrE->Index(split_reac[2], false);
               // Search for fragment in list
               if (reac.i_frag < 0) {
                  string message = Form("%s is not in CR list, cannot add XS reaction %s", split_reac[2].c_str(), par_name.c_str());
                  TUMessages::Error(f_log, "TUXSections", "ParsXS_AddPar", message);
                  return;
               } else if (!reac.is_all_proj) {
                  // Check PROJ as a parent for CR
                  Int_t j_projlop = fCrE->IndexInParentList(reac.i_frag, split_reac[0]);
                  if (j_projlop < 0) {
                     string message = Form("%s is not a parent for %s, cannot add XS reaction %s", split_reac[0].c_str(), split_reac[2].c_str(), par_name.c_str());
                     TUMessages::Error(f_log, "TUXSections", "ParsXS_AddPar", message);
                     return;
                  }
               }
            }
         }
      }

      if ((reac.is_xs_prod || reac.is_xs_ina) && is_lcinel) {
         string message = Form("%s is malformed: cannot use LCInel keyword for production reaction", par_name.c_str());
         TUMessages::Error(f_log, "TUXSections", "ParsXS_AddPar", message);
         return;
      } else if ((reac.is_xs_ia || reac.is_xs_ina) && is_lcprod) {
         string message = Form("%s is malformed: cannot use LCProd keyword for inelastic reaction", par_name.c_str());
         TUMessages::Error(f_log, "TUXSections", "ParsXS_AddPar", message);
         return;
      }
   }

   // 3) Check if reaction already in list of reactions and update list
   Bool_t is_exist = false;
   for (Int_t i = 0; i < (Int_t)fParsXS_ReacIndices.size(); ++i) {
      // N.B.: overlapping XS are forbidden if applies to similar parameters!
      // Note also that duplicate parameters were excluded above (we cannot have twice 'ALL').

      //cout << par_name << "   i_{proj,targ,cr}=" << reac.i_proj << " " << reac.i_targ << " " << reac.i_frag
      //     << "  is_all_{proj,targ,frag,-,lcinel,lcprod} = " << reac.is_all_proj << " " << reac.is_all_targ << " " << reac.is_all_frag
      //     << " " << reac.is_all << " " << reac.is_all_lcinel << " " << reac.is_all_lcprod <<endl;
      // Abort if redundant reactions for similar parameters
      ParsXS_IsOverlappingReacs(par_reac[0], reac, fParsXS_ReacIndices[i], f_log);
      // For concision, use shorter name (pointer required, we modify some values!)
      XSReacPars_t *reac_i = &fParsXS_ReacIndices[i];
      // If reaction exists, only update structure elements not null
      if (reac.i_frag == reac_i->i_frag
            && reac.i_proj == reac_i->i_proj
            && reac.i_targ == reac_i->i_targ
            && reac.i_tert == reac_i->i_tert
            && reac.is_all == reac_i->is_all
            && reac.is_all_lcinel == reac_i->is_all_lcinel
            && reac.is_all_lcprod == reac_i->is_all_lcprod
            && reac.is_all_frag == reac_i->is_all_frag
            && reac.is_all_proj == reac_i->is_all_proj
            && reac.is_all_targ == reac_i->is_all_targ
            && reac.is_xs_ia == reac_i->is_xs_ia
            && reac.is_xs_ina == reac_i->is_xs_ina
            && reac.is_xs_prod == reac_i->is_xs_prod
         ) {
         if (reac.ipar_xsnorm >= 0)
            reac_i->ipar_xsnorm = reac.ipar_xsnorm;
         if (reac.ipar_eaxisscale >= 0)
            reac_i->ipar_eaxisscale = reac.ipar_eaxisscale;
         if (reac.ipar_enhancepowhe >= 0)
            reac_i->ipar_enhancepowhe = reac.ipar_enhancepowhe;
         if (reac.ipar_ethresh >= 0)
            reac_i->ipar_ethresh = reac.ipar_ethresh;
         if (reac.ipar_slopele >= 0)
            reac_i->ipar_slopele = reac.ipar_slopele;
         for (Int_t nn = 0; nn < (Int_t)reac_i->ipar_lcinel.size(); ++nn) {
            if (reac.ipar_lcinel[nn] >= 0)
               reac_i->ipar_lcinel[nn] = reac.ipar_lcinel[nn];
         }
         for (Int_t nn = 0; nn < (Int_t)reac_i->ipar_lcprod.size(); ++nn) {
            if (reac.ipar_lcprod[nn] >= 0)
               reac_i->ipar_lcprod[nn] = reac.ipar_lcprod[nn];
         }
         // If exists, break (don't forget to )
         is_exist = true;
         reac_i = NULL;
         break;
      }
      reac_i = NULL;
   }
   // If does not exist, add in reactions + update # of total and prod reactions
   if (!is_exist) {
      //ParsXS_Print(stdout, reac);
      fParsXS_ReacIndices.push_back(reac);
      // If linear combination, need to store reaction name for later
      if (is_lcinel)
         fXSLinComb_InelReacs.push_back(par_reac[1]);
      if (is_lcprod)
         fXSLinComb_ProdReacs.push_back(par_reac[1]);
   }
   // 4) If reach this point, must add as a new free parameter
   fFreePars->AddPar(pars->GetParEntry(i_par), is_use_or_copy);
   return;
}

//______________________________________________________________________________
void TUXSections::ParsXS_ExtractExample(XSReacPars_t &reac) const
{
   //--- Returns reaction with updated indices corresponding to a single example reaction.
   // INPUT:
   //  reac             Reaction from which to get example
   // OUTPUT:
   //  reac             Reaction with updated indices/name


   if (reac.is_all || reac.is_all_lcinel || reac.is_all_lcprod) { // Set as inelastic XS
      reac.i_proj = 0;
      reac.i_targ = 0;
      reac.is_xs_ia = 0;
   }

   reac.is_all = false;
   reac.is_all_lcinel = false;
   reac.is_all_lcprod = false;
   reac.is_all_frag = false;
   reac.is_all_proj = false;
   reac.is_all_targ = false;

   if (reac.i_proj < 0)
      reac.i_proj = fCrE->GetNCRs() - 1;
   string proj = fCrE->TUCRList::GetCREntry(reac.i_proj).GetName();
   if (reac.i_targ < 0)
      reac.i_targ = 0;
   string targ = GetNameTarget(reac.i_targ);
   if (reac.is_xs_ia)
      reac.name = proj + "+" + targ;
   else if (reac.is_xs_ina)
      reac.name = proj + "+" + targ + "->" + proj;
   else if (reac.is_xs_prod) {
      if (reac.i_frag < 0)
         reac.i_frag = 0;
      // And must check that reaction exists!
      if (fCrE->IndexInParentList(reac.i_frag, proj) < 0) {
         reac.i_proj = fCrE->IndexInCRList_Parent(reac.i_frag, 0);
         proj = fCrE->TUCRList::GetCREntry(reac.i_proj).GetName();
      }
      reac.name = proj + "+" + targ + "->" + fCrE->TUCRList::GetCREntry(reac.i_frag).GetName();
   }
}

//______________________________________________________________________________
string TUXSections::ParsXS_ExtractKeywords(XSReacPars_t const &reac) const
{
   //--- Extracts enabled keywords (nuisance parameters) in reaction.

   vector<string> keys;

   if (reac.ipar_xsnorm >= 0)
      keys.push_back(parsxs_keys[0]);
   if (reac.ipar_eaxisscale >= 0)
      keys.push_back(parsxs_keys[1]);
   if (reac.ipar_enhancepowhe >= 0)
      keys.push_back(parsxs_keys[2]);
   if (reac.ipar_ethresh >= 0)
      keys.push_back(parsxs_keys[3]);
   if (reac.ipar_slopele >= 0)
      keys.push_back(parsxs_keys[4]);

   // For linear combination of IA XS
   for (Int_t i = 0; i < (Int_t)reac.ipar_lcinel.size(); ++i) {
      if (reac.ipar_lcinel[i] >= 0)
         keys.push_back(parsxs_keys[i + 5]);
   }

   // For linear combination of prod XS
   for (Int_t i = 0; i < (Int_t)reac.ipar_lcprod.size(); ++i) {
      if (reac.ipar_lcprod[i] >= 0)
         keys.push_back(parsxs_keys[i + 9]);
   }

   return TUMisc::List2String(keys, ',');
}

//______________________________________________________________________________
void TUXSections::ParsXS_Initialise(Bool_t is_delete)
{
   //--- Initialises members fParsXS_... related to usage of XS free parameters.
   //  is_delete        Whether to delete class members or not

   // If delete
   if (is_delete && fParsXS_RefXS)
      delete fParsXS_RefXS;

   fParsXS_ReacIndices.clear();
   fParsXS_RefXS = NULL;
}

//______________________________________________________________________________
void TUXSections::ParsXS_InitReac(XSReacPars_t &reac) const
{
   //--- Initialise reaction.
   //  reac              Reaction to initialise

   reac.i_frag = -1;
   reac.i_proj = -1;
   reac.i_targ = -1;
   reac.i_tert = -1;
   reac.ipar_eaxisscale = -1;
   reac.ipar_enhancepowhe = -1;
   reac.ipar_xsnorm = -1;
   reac.ipar_ethresh = -1;
   reac.ipar_slopele = -1;
   reac.ipar_lcinel.clear();
   reac.ipar_lcprod.clear();
   reac.is_all = false;
   reac.is_all_lcinel = false;
   reac.is_all_lcprod = false;
   reac.is_all_frag = false;
   reac.is_all_proj = false;
   reac.is_all_targ = false;
   reac.name = "";
   reac.is_xs_ia = false;
   reac.is_xs_ina = false;
   reac.is_xs_prod = false;
}

//______________________________________________________________________________
Bool_t TUXSections::ParsXS_IsMatchingKey(XSReacPars_t const &reac1, XSReacPars_t const &reac2) const
{
   //--- Returns true if reac1 and reac2 have at least one matching nuisance parameters.
   //  reac1            Reaction to compare
   //  reac2            Reaction to compare

   if (reac1.ipar_xsnorm >= 0 && reac2.ipar_xsnorm >= 0)
      return true;
   else if (reac1.ipar_eaxisscale >= 0 && reac2.ipar_eaxisscale >= 0)
      return true;
   else if (reac1.ipar_enhancepowhe >= 0 && reac2.ipar_enhancepowhe >= 0)
      return true;
   else if (reac1.ipar_ethresh >= 0 && reac2.ipar_ethresh >= 0)
      return true;
   else if (reac1.ipar_slopele >= 0 && reac2.ipar_slopele >= 0)
      return true;
   else if (reac1.ipar_lcinel.size() > 0 && reac1.ipar_lcinel.size() == reac2.ipar_lcinel.size()) {
      for (Int_t i = 0; i < (Int_t)reac1.ipar_lcinel.size(); ++i)
         if (reac1.ipar_lcinel[i] >= 0 && reac2.ipar_lcinel[i] >= 0)
            return true;
   } else if (reac1.ipar_lcprod.size() > 0 && reac1.ipar_lcprod.size() == reac2.ipar_lcprod.size()) {
      for (Int_t i = 0; i < (Int_t)reac1.ipar_lcprod.size(); ++i)
         if (reac1.ipar_lcprod[i] >= 0 && reac2.ipar_lcprod[i] >= 0)
            return true;
   } else
      return false;
   return false;
}

//______________________________________________________________________________
Bool_t TUXSections::ParsXS_IsOverlappingReacs(string const &key, XSReacPars_t const &reac1, XSReacPars_t const &reac2,
      FILE *f_log, Bool_t is_abort) const
{
   //--- If reac1 and reac2 have overlapping reactions for same nuisance parameters, aborts.
   //  key              Name of nuisance parameter for this reaction
   //  reac1            First reaction in comparison
   //  reac2            Second reaction in comparison
   //  f_log            Log for USINE
   //  is_abort         If true, aborts, otherwise, only only warning message

   if (!is_abort)
      fprintf(f_log, "   - check no overlap between %s_%s and %s_%s",
              ParsXS_ExtractKeywords(reac1).c_str(), reac1.name.c_str(),
              ParsXS_ExtractKeywords(reac2).c_str(), reac2.name.c_str());


   // If reac1 or reac2 is 'all', aborts if same nuisance parameter
   if (reac1.is_all || reac2.is_all) {
      // Abort only if similar nuisance parameters
      if (ParsXS_IsMatchingKey(reac1, reac2)) {
         string message = Form("%s has overlapping reactions with %s for keyword %s",
                               reac1.name.c_str(), reac2.name.c_str(), key.c_str());
         if (is_abort)
            TUMessages::Error(f_log, "TUXSections", "ParsXS_AddPar", message);
         else
            fprintf(f_log, "     => Overlap!\n");
         return true;
      }
      if (!is_abort)
         fprintf(f_log, "     => No overlap\n");
      return false;
   } else if (ParsXS_IsSame(reac1, reac2)) {
      // Check that reac1 is not equal to reac2
      if (is_abort) {
         string message = Form("%s (and keywords) equals %s!", reac1.name.c_str(), reac2.name.c_str());
         TUMessages::Error(f_log, "TUXSections", "ParsXS_AddPar", message);
      } else
         fprintf(f_log, "     => Same reactions/keywords!\n");
      return true;
   } else if (reac1.is_xs_ia && reac2.is_xs_ia) {
      // (i) Inelastic annihilating reactions (IA)
      //    if (reac1.(ANY+T or CR+ANY) and reac2.(CR+T)) or (reac1.(CR+ANY) and reac2.(ANY+T or CR+ANY))
      //     or (reac1.(ANY+T) and reac2.(CR+ANY)) or (reac1.(CR+ANY) and reac2.(ANY+T)) )
      //     => aborts if overlapping reactions for same nuisance parameters!
      //    else adds
      if (
         (((reac1.is_all_proj && reac2.i_proj >= 0 && !reac2.is_all_targ && reac2.i_targ == reac1.i_targ)
           || (reac1.is_all_targ && reac2.i_targ >= 0 && !reac2.is_all_proj && reac2.i_proj == reac1.i_proj))
          || ((reac2.is_all_proj && reac1.i_proj >= 0 && !reac1.is_all_targ && reac2.i_targ == reac1.i_targ)
              || (reac2.is_all_targ && reac1.i_targ >= 0 && !reac1.is_all_proj && reac2.i_proj == reac1.i_proj))
          || ((reac1.is_all_proj && reac2.i_proj >= 0 && reac2.is_all_targ)
              || (reac2.is_all_proj && reac1.i_proj >= 0 && reac1.is_all_targ))
         )
         || (reac1.i_proj >= 0 && reac1.i_proj == reac2.i_proj && reac1.i_targ >= 0 && reac1.i_targ == reac2.i_targ)
      ) {
         // Abort only if similar nuisance parameters
         if (ParsXS_IsMatchingKey(reac1, reac2)) {
            string message = Form("%s has overlapping reactions with %s for keyword %s",
                                  reac1.name.c_str(), reac2.name.c_str(), key.c_str());
            if (is_abort)
               TUMessages::Error(f_log, "TUXSections", "ParsXS_AddPar", message);
            else
               fprintf(f_log, "     => Overlap!\n");
            return true;
         }
      }
   } else if (reac1.is_xs_ina && reac2.is_xs_ina) {
      // (ii) Inelastic non-annihilating XS (INA)
      //  if (reac1.(CR+T->CR or CR+ANY->CR) and reac2.(CR+T->CR))
      //     or 'reac1 <-> reac2'
      //  else abort()
      if (
         ((((reac1.i_targ >= 0 && reac2.i_targ == reac1.i_targ) || reac1.is_all_targ)
           && reac1.i_proj == reac2.i_proj)
          || (((reac2.i_targ >= 0 && reac1.i_targ == reac2.i_targ) || reac2.is_all_targ)
              && reac2.i_proj == reac1.i_proj)
         )
         ||
         (reac1.i_proj >= 0 && reac1.i_proj == reac2.i_proj
          && reac1.i_targ >= 0 && reac1.i_targ == reac2.i_targ
         )
      ) {
         // Abort only if similar nuisance parameters
         if (ParsXS_IsMatchingKey(reac1, reac2)) {
            string message = Form("%s has overlapping reactions with %s for keyword %s",
                                  reac1.name.c_str(), reac2.name.c_str(), key.c_str());
            if (is_abort)
               TUMessages::Error(f_log, "TUXSections", "ParsXS_AddPar", message);
            else
               fprintf(f_log, "     => Overlap!\n");
            return true;
         }
      }
   } else if (reac1.is_xs_prod && reac2.is_xs_prod) {
      // (iii) Production XS
      //  If ( (reac1.(ANY+T->ANY or CR1+ANY->ANY or ANY+ANY->CR2) and reac2.(CR1+T->CR2))
      //    or all the above with 'reac1 <-> reac2'
      //  => aborts, cannot have overlapping reactions!
      //  else adds
      if (
         (reac1.i_proj >= 0 && reac1.i_proj == reac2.i_proj
          && reac1.i_targ >= 0 && reac1.i_targ == reac2.i_targ
          && reac1.i_frag >= 0 && reac1.i_frag == reac2.i_frag
         )
         ||
         ((reac1.is_all_proj && reac1.is_all_frag && (reac2.i_proj >= 0 || reac2.i_frag >= 0) && reac2.i_targ == reac1.i_targ)
          || (reac1.is_all_targ && reac1.is_all_frag && (reac2.i_targ >= 0 || reac2.i_frag >= 0) && reac2.i_proj == reac1.i_proj)
          || (reac1.is_all_proj && reac1.is_all_targ && (reac2.i_proj >= 0 || reac2.i_targ >= 0) && reac2.i_frag == reac1.i_frag)
         )
         ||
         ((reac2.is_all_proj && reac2.is_all_frag && (reac1.i_proj >= 0 || reac1.i_frag >= 0) && reac1.i_targ == reac2.i_targ)
          || (reac2.is_all_targ && reac2.is_all_frag && (reac1.i_targ >= 0 || reac1.i_frag >= 0) && reac1.i_proj == reac2.i_proj)
          || (reac2.is_all_proj && reac2.is_all_targ && (reac1.i_proj >= 0 || reac1.i_targ >= 0) && reac1.i_frag == reac2.i_frag)
         )
         ||
         // or reac1.(ANY+T->CR2) and reac2.(CR1+T->CR2 or CR1+ANY->CR2 or CR1+T->ANY)
         // or reac1.(CR1+ANY->CR2) and reac2.(CR1+T->CR2 or ANY+T->CR2 or CR1+T->ANY)
         // or reac1.(CR1+T->ANY) and reac2.(CR1+T->CR2 or ANY+T->CR2 or CR1+ANY->CR2)
         // or (and all the above with 'reac1 <-> reac2')
         (reac1.is_all_proj &&
          ((reac2.i_targ == reac1.i_targ && reac2.i_frag == reac1.i_frag
            && reac2.i_proj >= 0 && reac2.i_targ >= 0 && reac2.i_frag >= 0)
           || (reac2.i_proj >= 0 && reac2.is_all_targ && reac2.i_frag == reac1.i_frag)
           || (reac2.i_proj >= 0 && reac2.i_targ == reac1.i_targ && reac2.is_all_frag)
          )
         )
         ||
         (reac1.is_all_targ &&
          ((reac2.i_proj == reac1.i_proj && reac2.i_frag == reac1.i_frag
            && reac2.i_proj >= 0 && reac2.i_targ >= 0 && reac2.i_frag >= 0)
           || (reac2.is_all_proj && reac2.i_targ >= 0 && reac2.i_frag == reac1.i_frag)
           || (reac2.i_proj == reac1.i_proj && reac2.i_targ >= 0 && reac2.is_all_frag)
          )
         )
         ||
         (reac1.is_all_frag &&
          ((reac2.i_proj == reac1.i_proj && reac2.i_targ == reac1.i_targ
            && reac2.i_proj >= 0 && reac2.i_targ >= 0 && reac2.i_frag >= 0)
           || (reac2.is_all_proj && reac2.i_frag >= 0 && reac2.i_targ == reac1.i_targ)
           || (reac2.i_proj == reac1.i_proj && reac2.is_all_targ && reac2.i_frag >= 0)
          )
         )
         ||
         (reac2.is_all_proj &&
          ((reac1.i_targ == reac2.i_targ && reac1.i_frag == reac2.i_frag
            && reac1.i_proj >= 0 && reac1.i_targ >= 0 && reac1.i_frag >= 0)
           || (reac1.i_proj >= 0 && reac1.is_all_targ && reac1.i_frag == reac2.i_frag)
           || (reac1.i_proj >= 0 && reac1.i_targ == reac2.i_targ && reac1.is_all_frag)
          )
         )
         ||
         (reac2.is_all_targ &&
          ((reac1.i_proj == reac2.i_proj && reac1.i_frag == reac2.i_frag
            && reac1.i_proj >= 0 && reac1.i_targ >= 0 && reac1.i_frag >= 0)
           || (reac1.is_all_proj && reac1.i_targ >= 0 && reac1.i_frag == reac2.i_frag)
           || (reac1.i_proj == reac2.i_proj && reac1.i_targ >= 0 && reac1.is_all_frag)
          )
         )
         ||
         (reac2.is_all_frag &&
          ((reac1.i_proj == reac2.i_proj && reac1.i_targ == reac2.i_targ
            && reac1.i_proj >= 0 && reac1.i_targ >= 0 && reac1.i_frag >= 0)
           || (reac1.is_all_proj && reac1.i_frag >= 0 && reac1.i_targ == reac2.i_targ)
           || (reac1.i_proj == reac2.i_proj && reac1.is_all_targ && reac1.i_frag >= 0)
          )
         )
      ) {
         // Abort only if similar nuisance parameters
         if (ParsXS_IsMatchingKey(reac1, reac2)) {
            string message = Form("%s has overlapping reactions with %s for keyword %s",
                                  reac1.name.c_str(), reac2.name.c_str(), key.c_str());
            if (is_abort)
               TUMessages::Error(f_log, "TUXSections", "ParsXS_AddPar", message);
            else
               fprintf(f_log, "     => Overlap!\n");
            return true;
         }
      }
   }
   if (!is_abort)
      fprintf(f_log, "     => No overlap\n");
   return false;
}

//______________________________________________________________________________
Bool_t TUXSections::ParsXS_IsSame(XSReacPars_t const &reac1, XSReacPars_t const &reac2) const
{
   //--- Check whether reac1 == reac2 or not.
   //  reac1            First reaction in comparison
   //  reac2            Second reaction in comparison

   if ((reac1.i_frag == reac2.i_frag)
         && (reac1.i_proj == reac2.i_proj)
         && (reac1.i_targ == reac2.i_targ)
         && (reac1.i_tert == reac2.i_tert)
         && (reac1.ipar_xsnorm == reac2.ipar_xsnorm)
         && (reac1.ipar_eaxisscale == reac2.ipar_eaxisscale)
         && (reac1.ipar_enhancepowhe == reac2.ipar_enhancepowhe)
         && (reac1.ipar_ethresh == reac2.ipar_ethresh)
         && (reac1.ipar_slopele == reac2.ipar_slopele)
         && (reac1.ipar_lcinel == reac2.ipar_lcinel)
         && (reac1.ipar_lcprod == reac2.ipar_lcprod)
         && (reac1.is_all == reac2.is_all)
         && (reac1.is_all_lcinel == reac2.is_all_lcinel)
         && (reac1.is_all_lcprod == reac2.is_all_lcprod)
         && (reac1.is_all_frag == reac2.is_all_frag)
         && (reac1.is_all_proj == reac2.is_all_proj)
         && (reac1.is_all_targ == reac2.is_all_targ)
         && (reac1.is_xs_ia == reac2.is_xs_ia)
         && (reac1.is_xs_ina == reac2.is_xs_ina)
         && (reac1.is_xs_prod == reac2.is_xs_prod)
         && (reac1.name == reac2.name)
      )
      return true;
   else
      return false;
}

//______________________________________________________________________________
void TUXSections::ParsXS_Print(FILE *f, XSReacPars_t const &reac)
{
   //--- Prints reaction indices.
   //  f                 File in which to print
   //  reac              Structure containing indices of reaction

   string indent = TUMessages::Indent(true);
   fprintf(f, "%s Indices for reaction: \n", indent.c_str());
   fprintf(f, "%s    -> i_proj = %d\n", indent.c_str(), reac.i_proj);
   fprintf(f, "%s    -> i_targ = %d\n", indent.c_str(), reac.i_targ);
   fprintf(f, "%s    -> i_frag = %d\n", indent.c_str(), reac.i_frag);
   fprintf(f, "%s    -> i_tert = %d\n", indent.c_str(), reac.i_tert);
   fprintf(f, "%s    -> ipar_xsnorm = %d\n", indent.c_str(), reac.ipar_xsnorm);
   fprintf(f, "%s    -> ipar_eaxisscale = %d\n", indent.c_str(), reac.ipar_eaxisscale);
   fprintf(f, "%s    -> ipar_enhancepowhe = %d\n", indent.c_str(), reac.ipar_enhancepowhe);
   fprintf(f, "%s    -> ipar_ethresh = %d\n", indent.c_str(), reac.ipar_ethresh);
   fprintf(f, "%s    -> ipar_slopele = %d\n", indent.c_str(), reac.ipar_slopele);
   for (Int_t nn = 0; nn < (Int_t)reac.ipar_lcinel.size(); ++nn)
      fprintf(f, "%s    -> ipar_lcinel[%d] = %d\n", indent.c_str(), nn, reac.ipar_lcinel[nn]);
   for (Int_t nn = 0; nn < (Int_t)reac.ipar_lcprod.size(); ++nn)
      fprintf(f, "%s    -> ipar_lcprod[%d] = %d\n", indent.c_str(), nn, reac.ipar_lcprod[nn]);
   fprintf(f, "%s    -> is_all = %d\n", indent.c_str(), reac.is_all);
   fprintf(f, "%s    -> is_all_lcinel = %d\n", indent.c_str(), reac.is_all_lcinel);
   fprintf(f, "%s    -> is_all_lcprod = %d\n", indent.c_str(), reac.is_all_lcprod);
   fprintf(f, "%s    -> is_all_proj = %d\n", indent.c_str(), reac.is_all_proj);
   fprintf(f, "%s    -> is_all_targ = %d\n", indent.c_str(), reac.is_all_targ);
   fprintf(f, "%s    -> is_all_frag = %d\n", indent.c_str(), reac.is_all_frag);
   fprintf(f, "%s    -> is_xs_ia = %d\n", indent.c_str(), reac.is_xs_ia);
   fprintf(f, "%s    -> is_xs_ina = %d\n", indent.c_str(), reac.is_xs_ina);
   fprintf(f, "%s    -> is_xs_prod = %d\n", indent.c_str(), reac.is_xs_prod);
   fprintf(f, "%s    -> name = %s\n", indent.c_str(), reac.name.c_str());
   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUXSections::ParsXS_PrintXSRefvsCurrent(FILE *f, Int_t r_reac) const
{
   //--- Prints reaction parameters (r_reac) and XS values (reference and current ones).
   //  f                 File in which to print
   //  r_reac            Index of reaction in fParsXS_ReacIndices

   if (r_reac < 0 || r_reac >= (Int_t)fParsXS_ReacIndices.size()) {
      string message = Form("Index of r_reac=%d is out of range, skip it", r_reac);
      TUMessages::Warning(f, "TUXSections", "ParsXS_Print", message);
      return;
   }

   // Extract printable example (reac_p) from reac
   const XSReacPars_t reac = fParsXS_ReacIndices[r_reac];
   XSReacPars_t reac_p = reac;
   ParsXS_ExtractExample(reac_p);


   // Print free par value for all free parameters in reactions
   if (reac.ipar_xsnorm >= 0)
      fFreePars->GetParEntry(reac.ipar_xsnorm)->Print(f);
   if (reac.ipar_eaxisscale >= 0)
      fFreePars->GetParEntry(reac.ipar_eaxisscale)->Print(f);
   if (reac.ipar_enhancepowhe >= 0)
      fFreePars->GetParEntry(reac.ipar_enhancepowhe)->Print(f);
   if (reac.ipar_ethresh >= 0)
      fFreePars->GetParEntry(reac.ipar_ethresh)->Print(f);
   if (reac.ipar_slopele >= 0)
      fFreePars->GetParEntry(reac.ipar_slopele)->Print(f);
   for (Int_t nn = 0; nn < (Int_t)reac.ipar_lcinel.size(); ++nn) {
      if (reac.ipar_lcinel[nn] >= 0)
         fFreePars->GetParEntry(reac.ipar_lcinel[nn])->Print(f);
   }
   for (Int_t nn = 0; nn < (Int_t)reac.ipar_lcprod.size(); ++nn) {
      if (reac.ipar_lcprod[nn] >= 0)
         fFreePars->GetParEntry(reac.ipar_lcprod[nn])->Print(f);
   }




   if (reac.is_all || reac.is_all_lcinel || reac.is_all_lcprod || reac.is_all_proj || reac.is_all_targ || reac.is_all_frag)
      fprintf(f, "  %s (illustrated for %s below)\n", reac.name.c_str(), reac_p.name.c_str());
   else
      fprintf(f, "  %s\n", reac.name.c_str());

   if (reac.is_all || reac.is_xs_ia || reac.is_all_lcinel) { // Print tot.inel. XS (if 'is_all', we print only one as well)
      fprintf(f, "  %12s      Ref.          Current \n", fCrE->GetE(reac_p.i_proj)->GetNameAndUnit().c_str());
      for (Int_t k_ekn = 0; k_ekn < GetNE(); ++k_ekn)
         fprintf(f, "   %le   %le  %le\n", fCrE->GetEkn(reac_p.i_proj, k_ekn),
                 fParsXS_RefXS->GetTotIA(reac_p.i_proj, reac_p.i_targ, k_ekn),
                 GetTotIA(reac_p.i_proj, reac_p.i_targ, k_ekn));
   } else if (reac.is_xs_ina) { // Print non-annihilating rescattering XS
      // Print total XS
      fprintf(f, "  %12s      Ref.          Current \n", fCrE->GetE(reac_p.i_proj)->GetNameAndUnit().c_str());
      for (Int_t k_ekn = 0; k_ekn < GetNE(); ++k_ekn)
         fprintf(f, "   %le   %le  %le\n", fCrE->GetEkn(reac_p.i_proj, k_ekn),
                 fParsXS_RefXS->GetTotINA(reac_p.i_tert, reac_p.i_targ, k_ekn),
                 GetTotINA(reac_p.i_tert, reac_p.i_targ, k_ekn));
      // Print differential XS
      fprintf(f, "   In%7s     Out%7s       Ref.          Current \n",
              fCrE->GetE(reac_p.i_frag)->GetUnit(true).c_str(), fCrE->GetE(reac_p.i_frag)->GetUnit(true).c_str());
      for (Int_t k_in = 0; k_in < GetNE(); ++k_in) {
         for (Int_t k_out = 0; k_out < GetNE(); ++k_out)
            fprintf(f, "   %le->%le   %le  %le\n", fCrE->GetEkn(reac_p.i_frag, k_in), fCrE->GetEkn(reac_p.i_frag, k_out),
                    fParsXS_RefXS->GetDSigDEkINA(reac_p.i_tert, reac_p.i_targ, k_in, k_out),
                    GetDSigDEkINA(reac_p.i_tert, reac_p.i_targ, k_in, k_out));
      }
   } else if (reac.is_xs_prod || reac.is_all_lcprod) { // Print prod XS
      Int_t j_parent = fCrE->IndexInParentList(reac_p.i_frag, fCrE->TUCRList::GetCREntry(reac_p.i_proj).GetName());

      if (IsProdSigmaOrdSigma(j_parent, reac_p.i_frag)) {
         fprintf(f, "  %12s      Ref.          Current \n", fCrE->GetE(reac_p.i_frag)->GetNameAndUnit().c_str());
         for (Int_t k_ekn = 0; k_ekn < GetNE(); ++k_ekn)
            fprintf(f, "   %le   %le  %le\n", fCrE->GetEkn(reac_p.i_frag, k_ekn),
                    fParsXS_RefXS->GetProdSig(j_parent, reac_p.i_targ, reac_p.i_frag, k_ekn),
                    GetProdSig(j_parent, reac_p.i_targ, reac_p.i_frag, k_ekn));
      } else {
         fprintf(f, "   In%7s     Out%7s       Ref.          Current \n",
                 fCrE->GetE(j_parent)->GetUnit(true).c_str(), fCrE->GetE(reac_p.i_frag)->GetUnit(true).c_str());
         for (Int_t k_in = 0; k_in < GetNE(); ++k_in) {
            for (Int_t k_out = 0; k_out < GetNE(); ++k_out)
               fprintf(f, "   %le->%le   %le  %le\n", fCrE->GetEkn(reac_p.i_proj, k_in), fCrE->GetEkn(reac_p.i_frag, k_out),
                       fParsXS_RefXS->GetProdDSigDEk(j_parent, reac_p.i_targ, reac_p.i_frag, k_in, k_out),
                       GetProdDSigDEk(j_parent, reac_p.i_targ, reac_p.i_frag, k_in, k_out));
         }
      }
   }
}

//______________________________________________________________________________
void TUXSections::ParsXS_SetPars(TUFreeParList *pars, Bool_t is_use_or_copy, Bool_t is_verbose, FILE *f_log)
{
   //--- Fills fParXS_... parameters from 'pars', and use/copy XS-related (of pars) in fFreePars.
   //  pars              List of free parameters in which XS-related parameters are sought
   //  is_use_or_copy    Whether to use or copy XS-related pars in fFreePars
   //  is_verbose        Verbose or not when class is set
   //  f_log             Log for USINE

   string indent = TUMessages::Indent(true);
   if (is_verbose)
      fprintf(f_log, "%s[TUXSections::ParsXS_SetPars]\n", indent.c_str());

   // Initialise quantities used if XS free pars
   ParsXS_Initialise(true);

   // Reset fFreePars
   if (fIsDeleteFreePars && fFreePars)
      delete fFreePars;
   fFreePars = NULL;
   fIsDeleteFreePars = is_use_or_copy;

   // If no free pars, nothing to do
   if (!pars || pars->GetNPars() == 0) {
      string message = "pars is empty, nothing to do!";
      TUMessages::Warning(f_log, "TUXSections", "ParsXS_SetPars", message);
      fprintf(f_log, "%s[TUXSections::ParsXS_SetPars] <DONE>\n\n", indent.c_str());
      TUMessages::Indent(false);
      return;
   }

   // Loop on pars, and in fParsXS_ReacIndices and fFreePars if valid reaction/parameter
   fXSLinComb_InelReacs.clear();
   fXSLinComb_ProdReacs.clear();
   fFreePars = new TUFreeParList();
   for (Int_t p = 0; p < pars->GetNPars(); ++p)
      ParsXS_AddPar(pars, p, is_use_or_copy, f_log);

   // If no parameter found, nothing to do
   if (fFreePars->GetNPars() == 0) {
      string message = "No valid XS parameter found in pars, nothing to do!";
      TUMessages::Warning(f_log, "TUXSections", "ParsXS_SetPars", message);
      fprintf(f_log, "%s[TUXSections::ParsXS_SetPars] <DONE>\n\n", indent.c_str());
      TUMessages::Indent(false);
      return;
   }

   // Allocate fParsXS_RefXS as a copy of current class.
   if (ParsXS_GetNReac() != 0)
      fParsXS_RefXS = Clone();

   indent = TUMessages::Indent(false);
   if (is_verbose)
      fprintf(f_log, "%s[TUXSections::ParsXS_SetPars] <DONE>\n\n", indent.c_str());
}

//______________________________________________________________________________
void TUXSections::ParsXS_UpdateFromFreeParsAndResetStatus(TUMediumEntry *medium, Bool_t is_verbose, Bool_t is_update_xs, FILE *f_log)
{
   //--- Updates all relevant XS from updated fFreePars values and reset fFreePars status.
   //    If fnv...ISM0D variables used (and 'medium' non null), update their values
   //    for the updated XS.
   //  medium             Local ISM t use for calculation
   //  is_verbose         If true, print list of XS modified given the XS params
   //  is_update          If true, update XS values
   //  f_log              Log for USINE

   if (ParsXS_GetNReac() == 0)
      return;

   // If status of parameters has changed
   if (fFreePars && fFreePars->GetParListStatus()) {
      string indent = "";
      if (is_verbose) {
         indent = TUMessages::Indent(true);
         TUMessages::Indent(false);
         fprintf(f_log, "%s- XS reactions modified (associated to a nuisance parameters)\n", indent.c_str());
      }

      // Loop on reac
      for (Int_t r_reac = 0; r_reac < ParsXS_GetNReac(); ++r_reac) {
         XSReacPars_t *reac = &fParsXS_ReacIndices[r_reac];
         string projs4check = "";
         string targs4check = "";
         string frags4check = "";

         // Add all necessary reactions
         vector<Int_t> list_proj, list_targ;
         // Add all projectiles or selected one
         if (reac->is_all || reac->is_all_lcinel || reac->is_all_lcprod || reac->is_all_proj) {
            for (Int_t i = 0; i < fCrE->GetNCRs(); ++i) {
               list_proj.push_back(i);
               if (i == 0)
                  projs4check = fCrE->GetCREntry(i).GetName() + "...";
               else if (i == fCrE->GetNCRs() - 1)
                  projs4check += fCrE->GetCREntry(i).GetName();
            }
         } else {
            list_proj.push_back(reac->i_proj);
            projs4check = fCrE->GetCREntry(reac->i_proj).GetName();
         }

         // Add all targets or selected one
         if (reac->is_all || reac->is_all_lcinel || reac->is_all_lcprod || reac->is_all_targ) {
            targs4check = TUMisc::List2String(fTargets, ',');
            for (Int_t i = 0; i < GetNTargets(); ++i)
               list_targ.push_back(i);
         } else {
            list_targ.push_back(reac->i_targ);
            targs4check = GetNameTarget(reac->i_targ);
         }

         // Get free parameters for this reaction
         Double_t norm = 1., ekn_thresh = 5., eaxisscale = 1., slope_le = 0., enhancepow_he = 1.;
         Bool_t is_shift = false, is_enhancehe = false;
         if (reac->ipar_xsnorm >= 0)
            norm = fFreePars->GetParEntry(reac->ipar_xsnorm)->GetVal(false);
         if (reac->ipar_enhancepowhe >= 0) {
            is_enhancehe = true;
            enhancepow_he = fFreePars->GetParEntry(reac->ipar_enhancepowhe)->GetVal(false);
         }
         if (reac->ipar_ethresh >= 0)
            ekn_thresh = fFreePars->GetParEntry(reac->ipar_ethresh)->GetVal(false);
         if (reac->ipar_slopele >= 0)
            slope_le = fFreePars->GetParEntry(reac->ipar_slopele)->GetVal(false);
         if (reac->ipar_eaxisscale >= 0) {
            is_shift = true;
            eaxisscale = fFreePars->GetParEntry(reac->ipar_eaxisscale)->GetVal(false);
         }

         // For linear combination XS, to be able to update, we need to store
         //  - whether we use this option or not
         //  - value of coefficients
         Bool_t is_lcinel = false;
         vector<Double_t> coeffs_inel;
         for (Int_t nn = 0; nn < (Int_t)reac->ipar_lcinel.size(); ++nn) {
            if (reac->ipar_lcinel[nn] >= 0) {
               coeffs_inel.push_back(fFreePars->GetParEntry(reac->ipar_lcinel[nn])->GetVal(false));
               is_lcinel = true;
            } else
               coeffs_inel.push_back(0.);
         }
         Bool_t is_lcprod = false;
         vector<Double_t> coeffs_prod;
         for (Int_t nn = 0; nn < (Int_t)reac->ipar_lcprod.size(); ++nn) {
            if (reac->ipar_lcprod[nn] >= 0) {
               coeffs_prod.push_back(fFreePars->GetParEntry(reac->ipar_lcprod[nn])->GetVal(false));
               is_lcprod = true;
            } else
               coeffs_prod.push_back(0.);
         }


         // If at least one XS is asked to be calculated from linear combination
         // and XS not previously loaded, load all enabled options!
         if (is_lcinel && fXSLinComb_NInelXS == 0) {
            vector<string> files = {
               "sigInelBarashenkov94.dat", "sigInelTripathi99+Coste12.dat",
               "sigInelWebber03.dat", "sigInelWellish97.dat"
            };
            for (Int_t f = 0; f < (Int_t)files.size(); ++f)
               files[f] = "$USINE/inputs/XS_NUCLEI/" + files[f];
            XSLinComb_Load(files, true/*is_inel_or_prod*/, true/*is_verbose*/, stdout /*f_log*/);
         }
         if (is_lcprod && fXSLinComb_NProdXS == 0) {
            vector<string> files = {
               "sigProdGALPROP17_OPT12.dat", "sigProdSoutoul01.dat",
               "sigProdWebber03+Coste12.dat", "sigProdWKS98.dat"
            };
            for (Int_t f = 0; f < (Int_t)files.size(); ++f)
               files[f] = "$USINE/inputs/XS_NUCLEI/" + files[f];
            XSLinComb_Load(files, false/*is_inel_or_prod*/, true/*is_verbose*/, stdout /*f_log*/);
         }

         // Bias inelastic XS and/or total INA XS
         if (reac->is_xs_ia || reac->is_xs_ina || reac->is_all || reac->is_all_lcinel) { // For tot.inel. XS
            // Verbose info for check purpose (should only be called once before minimisation)
            if (is_verbose)
               fprintf(f_log, "%s   - Inelastic (%d proj., %d targ.): {%s}_%s+%s\n",
                       indent.c_str(), (Int_t)list_proj.size(), (Int_t)list_targ.size(),
                       ParsXS_ExtractKeywords(*reac).c_str(), projs4check.c_str(), targs4check.c_str());
            if (!is_update_xs)
               continue;

            // Loop on all CRs for reaction (one or all)
            for (Int_t i_pj = 0; i_pj < (Int_t)list_proj.size(); ++i_pj) {
               Int_t j_proj = list_proj[i_pj];
               Int_t j_tert = MapCRIndex2INAIndex(j_proj); // -1 if not a tertiary

               // Loop on all targets for reaction (one or all)
               for (Int_t i_tg = 0; i_tg < (Int_t)list_targ.size(); ++i_tg) {
                  Int_t t_targ = list_targ[i_tg];
                  // Hard check (in loop) that all desired reactions are updated!
                  //if (j_tert<0)
                  //   cout << "     ... modify XS for " << fCrE->GetCREntry(j_proj).GetName() << "+" << GetNameTarget(t_targ) << endl;
                  //else if (!is_lcinel)
                  //   cout << "     ... modify XS for " << GetNameTertiary(j_tert) << "+" << GetNameTarget(t_targ) << "->" << GetNameTertiary(j_tert) << endl;

                  // If linear combination of XS, only possible change!
                  if (is_lcinel) {
                     XSLinComb_UpdateXSInel(coeffs_inel, j_proj, t_targ);
                     //ParsXS_PrintXSRefvsCurrent(stdout, r_reac);

                     // Update rate on ISM
                     if (medium && fnvSigTotIA_ISM0D)
                        FillInterRateTotIA_ISM0D(j_proj, medium);
                  } else {
                     // N.B.: the XS std transformation is ordered as follows
                     //   - Ekn scaling
                     //   - Normalisation
                     //   - (Ekn/Ekn_thresh)^a bias (below Ekn_thresh)
                     //   - (Enhancement at high-energy)^power

                     TUAxis eaxis_scaled(*fCrE->GetE(j_proj));
                     if (is_shift)
                        eaxis_scaled.Scale(eaxisscale);

                     // Applies to inelastic (IA) XS
                     if (reac->is_xs_ia || reac->is_all) {
                        Double_t *xs_interp = NULL;
                        // Shift
                        if (is_shift) {
                           xs_interp = new Double_t[GetNE()];
                           TUMath::Interpolate(eaxis_scaled.GetVals(), GetNE(), fParsXS_RefXS->GetTotIA(j_proj, t_targ),
                                               fCrE->GetE(j_proj)->GetVals(), GetNE(), xs_interp,
                                               kLOGLOG, false, stdout);
                        } else
                           xs_interp = fParsXS_RefXS->GetTotIA(j_proj, t_targ);

                        // Fill new values
                        for (Int_t k_ekn = 0; k_ekn < GetNE(); ++k_ekn) {
                           Double_t ekn = fCrE->GetE(j_proj)->GetVal(k_ekn);
                           Double_t bias = 1.;
                           if (ekn < ekn_thresh)
                              bias *= pow(ekn / ekn_thresh, slope_le);
                           Double_t val_new = bias * norm * xs_interp[k_ekn];
                           if (is_enhancehe)
                              val_new *= pow(XS_EnhancementHE(j_proj, k_ekn), enhancepow_he);
                           SetTotIA(j_proj, t_targ, k_ekn, val_new);
                        }
                        // Free memory
                        if (is_shift)
                           delete[] xs_interp;
                        xs_interp = NULL;

                        // Update rate on ISM
                        if (medium && fnvSigTotIA_ISM0D)
                           FillInterRateTotIA_ISM0D(j_proj, medium);
                     }
                     // Applies to NAR XS
                     if ((reac->is_xs_ina || reac->is_all) && j_tert >= 0) {
                        Double_t *xs_interp = NULL;
                        if (is_shift) {
                           // Shift
                           xs_interp = new Double_t[GetNE()];
                           TUMath::Interpolate(eaxis_scaled.GetVals(), GetNE(), fParsXS_RefXS->GetTotINA(j_tert, t_targ),
                                               fCrE->GetE(j_proj)->GetVals(), GetNE(), xs_interp,
                                               kLOGLOG, false, stdout);
                        } else
                           xs_interp = fParsXS_RefXS->GetTotINA(j_tert, t_targ);

                        // Fill new values
                        for (Int_t k_ekn = 0; k_ekn < GetNE(); ++k_ekn) {
                           Double_t ekn = fCrE->GetE(j_proj)->GetVal(k_ekn);
                           Double_t bias = 1.;
                           if (ekn < ekn_thresh)
                              bias *= pow(ekn / ekn_thresh, slope_le);
                           Double_t val_new = bias * norm * xs_interp[k_ekn];
                           if (is_enhancehe)
                              val_new *= pow(XS_EnhancementHE(j_proj, k_ekn), enhancepow_he);
                           SetTotINA(j_tert, t_targ, k_ekn, val_new);
                        }
                        // Free memory
                        if (is_shift)
                           delete[] xs_interp;
                        xs_interp = NULL;

                        // Update rate on ISM
                        if (medium && fnvSigTotINA_ISM0D)
                           FillInterRateTotINA_ISM0D(j_tert, medium);
                     }
                  }
               }
            }
         }
         // Bias production XS
         if (reac->is_xs_prod || reac->is_all || reac->is_all_lcprod) {
            // Add all fragments or selected one
            vector<Int_t> list_frag;
            if (reac->is_all || reac->is_all_lcprod || reac->is_all_frag) {
               for (Int_t i = 0; i < fCrE->GetNCRs() - 1; ++i) {
                  list_frag.push_back(i);
                  if (i == 0)
                     frags4check = fCrE->GetCREntry(i).GetName() + "...";
                  else if (i == fCrE->GetNCRs() - 2)
                     frags4check += fCrE->GetCREntry(i).GetName();
               }
            } else if (reac->i_frag >= 0) {
               list_frag.push_back(reac->i_frag);
               frags4check = fCrE->GetCREntry(reac->i_frag).GetName();

               // Update list of projectiles (directly taken from list of parents)
               if (reac->is_all_proj) {
                  vector<Int_t> list_tmp;
                  for (Int_t i_p = 0; i_p < fCrE->GetNParents(reac->i_frag); ++i_p) {
                     list_tmp.push_back(fCrE->IndexInCRList_Parent(reac->i_frag, i_p));
                     if (i_p == 0)
                        projs4check = fCrE->GetCREntry(list_tmp[i_p]).GetName() + "...";
                     else if (i_p == fCrE->GetNParents(reac->i_frag) - 1)
                        projs4check += fCrE->GetCREntry(list_tmp[i_p]).GetName();
                  }
                  list_proj.clear();
                  list_proj = list_tmp;
               }
            }
            //cout << "FRAGMENTS: " << TUMisc::List2String(list_frag,',') << endl;

            // Verbose info for check purpose (should only be called once before minimisation)
            if (is_verbose)
               fprintf(f_log, "%s   - Production (%d proj., %d targ., %d frag.): {%s}_%s+%s->%s\n",
                       indent.c_str(), (Int_t)list_proj.size(), (Int_t)list_targ.size(), (Int_t)list_frag.size(),
                       ParsXS_ExtractKeywords(*reac).c_str(), projs4check.c_str(), targs4check.c_str(), frags4check.c_str());
            if (!is_update_xs)
               continue;

            // Loop on all fragments
            for (Int_t i_fg = 0; i_fg < (Int_t)list_frag.size(); ++i_fg) {
               Int_t j_frag = list_frag[i_fg];
               //cout << "Loop j_frag=" << j_frag << "=" << fCrE->GetCREntry(j_frag).GetName() << endl;

               // Update list of projectiles for each fragment (if necessary)
               vector<Int_t> list_parent;
               if (list_proj.size() == 1) {
                  Int_t j_proj_lop = fCrE->IndexInParentList(j_frag, fCrE->GetCREntry(reac->i_proj).GetName());
                  // Must ensure that requested parent is indeed a parent
                  // (may happen if keyword 'ANY' is used for fragments)
                  if (j_proj_lop >= 0)
                     list_parent.push_back(j_proj_lop);
               } else {
                  // Means ANY projectile is wanted: update list
                  for (Int_t i_p = 0; i_p < fCrE->GetNParents(j_frag); ++i_p)
                     list_parent.push_back(i_p);
               }
               //cout << "PARENTS: " << TUMisc::List2String(list_parent,',') << endl;

               // Loop on all targets for reaction
               for (Int_t i_tg = 0; i_tg < (Int_t)list_targ.size(); ++i_tg) {
                  Int_t t_targ = list_targ[i_tg];
                  //cout << "Loop j_targ=" << t_targ << "=" << GetNameTarget(t_targ) << endl;

                  // Loop on all projectiles
                  for (Int_t i_par = 0; i_par < (Int_t)list_parent.size(); ++i_par) {
                     Int_t j_parent = list_parent[i_par];
                     Int_t j_proj = list_proj[i_par];
                     // Hard check (in loop) that all desired reactions are updated!
                     //cout << "     ... modify XS for " << fCrE->GetCREntry(fCrE->IndexInCRList_Parent(j_frag, j_parent)).GetName()
                     //     << "+" << GetNameTarget(t_targ) << "->" << fCrE->GetCREntry(j_frag).GetName() << endl;

                     // If linear combination of XS, only possible change!
                     if (is_lcprod)
                        XSLinComb_UpdateXSProd(coeffs_prod, j_parent, t_targ, j_frag);
                     else {
                        // N.B.: the XS std transformation is ordered as follows
                        //   - Ekn scaling
                        //   - Normalisation
                        //   - (Ekn/Ekn_thresh)^a bias (below Ekn_thresh)
                        //   - (Enhancement at high-energy)^power

                        TUAxis eaxis_scaled(*fCrE->GetE(j_frag));
                        if (is_shift)
                           eaxis_scaled.Scale(eaxisscale);

                        if (IsProdSigmaOrdSigma(j_parent, j_frag)) { // Straight-ahead production
                           Double_t *xs_interp = NULL;

                           // Shift
                           if (is_shift) {
                              xs_interp = new Double_t[GetNE()];
                              TUMath::Interpolate(eaxis_scaled.GetVals(), GetNE(), fParsXS_RefXS->GetProdSig(j_parent, t_targ, j_frag),
                                                  fCrE->GetE(j_frag)->GetVals(), GetNE(), xs_interp,
                                                  kLOGLOG, false, stdout);
                           } else
                              xs_interp = fParsXS_RefXS->GetProdSig(j_parent, t_targ, j_frag);

                           // Fill new values
                           for (Int_t k_ekn = 0; k_ekn < GetNE(); ++k_ekn) {
                              Double_t ekn = fCrE->GetE(j_frag)->GetVal(k_ekn);
                              Double_t bias = 1.;
                              if (ekn < ekn_thresh)
                                 bias *= pow(ekn / ekn_thresh, slope_le);
                              Double_t val_new = bias * norm * xs_interp[k_ekn];
                              if (is_enhancehe)
                                 val_new *= pow(XS_EnhancementHE(j_proj, k_ekn), enhancepow_he);
                              SetProdSig(j_parent, t_targ, j_frag, k_ekn, val_new);
                           }

                           // Free memory
                           if (is_shift)
                              delete[] xs_interp;
                           xs_interp = NULL;

                        } else { // Differential production
                           // Loop on incoming energies
                           for (Int_t k_in = 0; k_in < GetNE(); ++k_in) {
                              Double_t *xs_interp =  new Double_t[GetNE()];
                              // Shift
                              if (is_shift) {
                                 Double_t *xs_ref = new Double_t[GetNE()];
                                 for (Int_t k_out = 0; k_out < GetNE(); ++k_out)
                                    xs_ref[k_out] = fParsXS_RefXS->GetProdDSigDEk(j_parent, t_targ, j_frag, k_in, k_out);
                                 TUMath::Interpolate(eaxis_scaled.GetVals(), GetNE(), xs_ref,
                                                     fCrE->GetE(j_frag)->GetVals(), GetNE(), xs_interp,
                                                     kLOGLOG, false, stdout);
                                 delete[] xs_ref;
                                 xs_ref = NULL;
                              } else {
                                 for (Int_t k_out = 0; k_out < GetNE(); ++k_out)
                                    xs_interp[k_out] = fParsXS_RefXS->GetProdDSigDEk(j_parent, t_targ, j_frag, k_in, k_out);
                              }
                              // Fill new values
                              for (Int_t k_out = 0; k_out < GetNE(); ++k_out) {
                                 Double_t ekn = fCrE->GetE(j_frag)->GetVal(k_out);
                                 Double_t bias = 1.;
                                 if (ekn < ekn_thresh)
                                    bias *= pow(ekn / ekn_thresh, slope_le);
                                 Double_t val_new = bias * norm * xs_interp[k_out];
                                 if (is_enhancehe)
                                    val_new *= pow(XS_EnhancementHE(j_frag, k_in), enhancepow_he);
                                 SetProdDSigDEk(j_parent, t_targ, j_frag, k_in, k_out, val_new);
                              }
                              delete[] xs_interp;
                              xs_interp = NULL;
                           }
                        }
                     }

                     // Update rate on ISM
                     if (medium && (fnvDSigDEkProd_ISM0D || fnvSigProd_ISM0D))
                        FillInterRateProd_ISM0D(j_parent, j_frag, medium);
                  }
               }
            }
         }

         // Bias INA differential XS
         if (reac->is_xs_ina || reac->is_all) {
            // Loop on all projectiles and applies to tertiaries only
            vector<Int_t> list_tmp;
            vector<string> tert;
            for (Int_t i_pj = 0; i_pj < (Int_t)list_proj.size(); ++i_pj) {
               Int_t j_proj = list_proj[i_pj];
               Int_t j_tert = MapCRIndex2INAIndex(j_proj); // -1 if not a tertiary
               if (j_tert >= 0) {
                  list_tmp.push_back(j_proj);
                  tert.push_back(GetNameTertiary(j_tert));
               }
            }
            list_proj.clear();
            list_proj = list_tmp;

            // Verbose info for check purpose (should only be called once before minimisation)
            if (is_verbose) {
               projs4check = TUMisc::List2String(tert, ',');
               fprintf(f_log, "%s   - Tertiaries (%d tert., %d targ.): {%s}_%s+%s->%s\n",
                       indent.c_str(), (Int_t)list_proj.size(), (Int_t)list_targ.size(),
                       ParsXS_ExtractKeywords(*reac).c_str(), projs4check.c_str(), targs4check.c_str(), projs4check.c_str());
            }
            if (!is_update_xs)
               continue;

            // Loop on all projectiles and applies to tertiaries only
            for (Int_t i_pj = 0; i_pj < (Int_t)list_proj.size(); ++i_pj) {
               Int_t j_proj = list_proj[i_pj];
               Int_t j_tert = MapCRIndex2INAIndex(j_proj); // -1 if not a tertiary
               if (j_tert < 0)
                  continue;

               // Loop on all targets for reaction
               for (Int_t i_tg = 0; i_tg < (Int_t)list_targ.size(); ++i_tg) {
                  Int_t t_targ = list_targ[i_tg];
                  //cout << "Loop j_targ=" << t_targ << "=" << GetNameTarget(t_targ) << endl;
                  // N.B.: the XS std transformation is ordered as follows
                  //   - Ekn scaling
                  //   - Normalisation
                  //   - (Ekn/Ekn_thresh)^a bias (below Ekn_thresh)
                  //   - (Enhancement at high-energy)^power
                  TUAxis eaxis_scaled(*fCrE->GetE(j_proj));
                  if (is_shift)
                     eaxis_scaled.Scale(eaxisscale);

                  // Loop on incoming energies
                  for (Int_t k_in = 0; k_in < GetNE(); ++k_in) {
                     Double_t *xs_interp =  new Double_t[GetNE()];
                     // Shift
                     if (is_shift) {
                        Double_t *xs_ref = new Double_t[GetNE()];
                        for (Int_t k_out = 0; k_out < GetNE(); ++k_out)
                           xs_ref[k_out] = fParsXS_RefXS->GetDSigDEkINA(j_tert, t_targ, k_in, k_out);
                        TUMath::Interpolate(eaxis_scaled.GetVals(), GetNE(), xs_ref,
                                            fCrE->GetE(j_proj)->GetVals(), GetNE(), xs_interp,
                                            kLOGLOG, false, stdout);
                        delete[] xs_ref;
                        xs_ref = NULL;
                     } else {
                        for (Int_t k_out = 0; k_out < GetNE(); ++k_out)
                           xs_interp[k_out] = fParsXS_RefXS->GetDSigDEkINA(j_tert, t_targ, k_in, k_out);
                     }

                     // Fill new values
                     for (Int_t k_out = 0; k_out < GetNE(); ++k_out) {
                        Double_t ekn = fCrE->GetE(j_proj)->GetVal(k_out);
                        Double_t bias = 1.;
                        if (ekn < ekn_thresh)
                           bias *= pow(ekn / ekn_thresh, slope_le);
                        Double_t val_new = bias * norm * xs_interp[k_out];
                        if (is_enhancehe)
                           val_new *= pow(XS_EnhancementHE(j_proj, k_in), enhancepow_he);
                        SetDSigDEkINA(j_tert, t_targ, k_in, k_out, val_new);
                     }
                     delete[] xs_interp;
                     xs_interp = NULL;
                  }
               }

               // Update rate on ISM
               if (medium && fnvDSigDEkINA_ISM0D)
                  FillInterRateDSigINA_ISM0D(j_tert, medium);
            }
         }
         // Reset
         reac = NULL;
      }
      if (is_update_xs)
         fFreePars->ResetStatusParsAndParList();
   }
}

//______________________________________________________________________________
void TUXSections::Plots(string const &list_proj, string const &list_targ, string const &list_frag,
                        TApplication *my_app, Bool_t is_test, FILE *f_test) const
{
   //--- Plots or save plots (root and jpeg) for user-selected entries.
   //  list_proj          Comma-separated list of projectile names (empty for total X-sec)
   //  list_targ          Comma-separated list of targets (use "ALL" to display for all targets)
   //  list_frag          Comma-separated list of fragment names
   //  my_app             ROOT application where to plot selection
   //  is_print           Whether called as test (print in file graph content)
   //  f                  File in which to print

   Bool_t is_plot = false;
   string reaction = list_proj + " + " + list_targ + " -> ";
   if (list_frag == "")
      reaction += "ANY";
   else
      reaction += list_frag;

   // Set Plot style
   TUMisc::RootSetStylePlots();
   gStyle->SetOptStat("ne");
   TText *usine_txt = TUMisc::OrphanUSINEAdOnPlots();

   // Get Production plots (if required)
   TMultiGraph *mg_prod = NULL;
   vector<TH2D *> hs_prod;
   TLegend *leg_mg_prod = NULL;
   TCanvas *c_sigprod = NULL;
   TCanvas **c_dsigprod = NULL;
   if (list_proj != "") {
      mg_prod = new TMultiGraph();
      string mg_name = "xs_prod_" + reaction;
      TUMisc::RemoveSpecialChars(mg_name);
      mg_prod->SetName(mg_name.c_str());
      leg_mg_prod = new TLegend(0.13, 0.5, 0.3, 0.93, NULL, "brNDC");

      ExtractXSGraphsList(list_proj, list_targ, list_frag, mg_prod, leg_mg_prod, hs_prod, f_test);

      if (mg_prod->GetListOfGraphs() && mg_prod->GetListOfGraphs()->GetSize() != 0) {
         is_plot = true;
         string list_trimmed = list_proj;
         TUMisc::RemoveSpecialChars(list_trimmed);
         string name = "xsec_sig_prod_" + list_trimmed;
         c_sigprod = new TCanvas(name.c_str(), name.c_str(), 610, 0, 600, 400);
         c_sigprod->SetLogx(1);
         c_sigprod->SetLogy(1);
         //mg_prod->Draw("ALP");
         mg_prod->Draw("AL");
         mg_prod->GetXaxis()->CenterTitle();
         mg_prod->GetYaxis()->CenterTitle();
         if (is_test)
            TUMisc::Print(f_test, mg_prod, 0);
         usine_txt->Draw();
         leg_mg_prod->Draw();
         c_sigprod->Modified();
         c_sigprod->Update();
      }
      if (hs_prod.size() > 0) {
         is_plot = true;
         c_dsigprod = new TCanvas*[(Int_t)hs_prod.size()];
         gStyle->SetPalette();
         for (Int_t i = 0; i < (Int_t)hs_prod.size(); ++i) {
            string tmp = Form("xsec_dsig_prod_%d", i);
            c_dsigprod[i] = new TCanvas(tmp.c_str(), tmp.c_str(), 610, 450 + i * 40, 500, 350);
            c_dsigprod[i]->SetRightMargin(0.1);
            c_dsigprod[i]->SetLogx(0);
            c_dsigprod[i]->SetLogy(0);
            c_dsigprod[i]->SetLogz(0);
            hs_prod[i]->Draw("surf3z");
            hs_prod[i]->GetXaxis()->CenterTitle();
            hs_prod[i]->GetYaxis()->CenterTitle();
            usine_txt->Draw();
            c_dsigprod[i]->Modified();
            c_dsigprod[i]->Update();
         }
      }
   }


   // Get inelastic plots
   TMultiGraph *mg_inel = new TMultiGraph();
   vector<TH2D *> hs_inel;
   TLegend *leg_mg_inel = new TLegend(0.13, 0.5, 0.3, 0.93, NULL, "brNDC");

   if (list_proj != "")
      ExtractXSGraphsList("", list_targ, list_proj, mg_inel, leg_mg_inel, hs_inel, f_test);
   else
      ExtractXSGraphsList("", list_targ, list_frag, mg_inel, leg_mg_inel, hs_inel, f_test);

   TCanvas *c_siginel = NULL;
   TCanvas **c_dsiginel = NULL;
   if (mg_inel->GetListOfGraphs() && mg_inel->GetListOfGraphs()->GetSize() != 0) {
      is_plot = true;
      string list_trimmed = list_proj;
      TUMisc::RemoveSpecialChars(list_trimmed);
      string name = "xsec_sig_inel_" + list_trimmed;
      c_siginel = new TCanvas(name.c_str(), name.c_str(), 0, 0, 600, 400);
      c_siginel->SetLogx(1);
      c_siginel->SetLogy(1);
      //mg_inel->Draw("ALP");
      string mg_name = "xs_inel_" + reaction;
      TUMisc::RemoveSpecialChars(mg_name);
      mg_inel->SetName(mg_name.c_str());
      mg_inel->Draw("AL");
      mg_inel->GetXaxis()->CenterTitle();
      mg_inel->GetYaxis()->CenterTitle();
      usine_txt->Draw();
      if (is_test)
         TUMisc::Print(f_test, mg_inel, 0);
      leg_mg_inel->Draw();
      c_siginel->Modified();
      c_siginel->Update();
   }
   if (hs_inel.size() > 0) {
      is_plot = true;
      c_dsiginel = new TCanvas*[(Int_t)hs_inel.size()];
      gStyle->SetPalette();
      for (Int_t i = 0; i < (Int_t)hs_inel.size(); ++i) {
         string tmp = Form("xsec_dsig_inel_%d", i);
         c_dsiginel[i] = new TCanvas(tmp.c_str(), tmp.c_str(), 0, 450 + i * 40, 500, 350);
         c_dsiginel[i]->SetRightMargin(0.1);
         c_dsiginel[i]->SetLogx(0);
         c_dsiginel[i]->SetLogy(0);
         c_dsiginel[i]->SetLogz(0);
         hs_inel[i]->Draw("surf3z");
         hs_inel[i]->GetXaxis()->CenterTitle();
         hs_inel[i]->GetYaxis()->CenterTitle();
         usine_txt->Draw();
         c_dsiginel[i]->Modified();
         c_dsiginel[i]->Update();
      }
   }


   if (is_plot) {
      if (!is_test)
         my_app->Run(kTRUE);
   } else {
      string message = "No plots found for " + reaction;
      TUMessages::Warning(stdout, "TUXSections", "PlotsAndSave", message);
   }


   // Free memory X-sec prod
   if (c_sigprod) delete c_sigprod;
   c_sigprod = NULL;
   if (c_dsigprod) delete[] c_dsigprod;
   c_dsigprod = NULL;
   if (mg_prod)     delete mg_prod;
   mg_prod = NULL;
   for (Int_t i = 0; i < (Int_t)hs_prod.size(); ++i) {
      if (hs_prod[i]) delete hs_prod[i];
      hs_prod[i] = NULL;
   }
   hs_prod.clear();
   if (leg_mg_prod) delete leg_mg_prod;
   leg_mg_prod = NULL;


   // Free memory X-sec inel
   delete usine_txt;
   usine_txt = NULL;
   if (c_siginel) delete c_siginel;
   c_siginel = NULL;
   if (c_dsiginel) delete[] c_dsiginel;
   c_dsiginel = NULL;

   if (mg_inel)     delete mg_inel;
   mg_inel = NULL;
   for (Int_t i = 0; i < (Int_t)hs_inel.size(); ++i) {
      if (hs_inel[i]) delete hs_inel[i];
      hs_inel[i] = NULL;
   }
   hs_inel.clear();
   if (leg_mg_inel) delete leg_mg_inel;
   leg_mg_inel = NULL;
}

//______________________________________________________________________________
void TUXSections::PrintListOfXSecProdEqualZero(FILE *f) const
{
   //--- Prints in file f list of reactions with only null values.
   //  f                  File in which to print

   string indent = TUMessages::Indent(true);

   fprintf(f, "%s  -- Null production cross-sections:\n", indent.c_str());
   // Loop on CRs
   for (Int_t j_cr = fCrE->GetNCRs() - 1; j_cr >= 0; --j_cr) {
      // Loop on their parents
      for (Int_t j_parent = fCrE->GetNParents(j_cr) - 1; j_parent >= 0; --j_parent) {
         Int_t j = fCrE->IndexInCRList_Parent(j_cr, j_parent);
         if (IsProdNull(j_parent, j_cr))
            fprintf(f, "%s       %-5s -> %-5s is null for all targets\n", indent.c_str(),
                    fCrE->GetCREntry(j).GetName().c_str(), fCrE->GetCREntry(j_cr).GetName().c_str());
      }
   }
   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUXSections::PrintTargetList(FILE *f) const
{
   //--- Prints in file f target list.
   //  f                  File in which to print

   string indent = TUMessages::Indent(true);
   fprintf(f, "%s  -- Targets loaded: %s\n", indent.c_str(), ExtractTargets().c_str());
   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUXSections::PrintTertiaryList(FILE *f) const
{
   //--- Prints in file f tertiary list.
   //  f                  File in which to print

   string indent = TUMessages::Indent(true);
   fprintf(f, "%s  -- Tertiaries loaded: %s\n", indent.c_str(), ExtractTertiaries().c_str());
   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUXSections::PrintXSecFiles(FILE *f) const
{
   //--- Prints in file f all X-Sec file names read.
   //  f                  File in which to print

   string indent = TUMessages::Indent(true);

   fprintf(f, "%s  -- Cross section files used:\n", indent.c_str());

   for (Int_t i = 0; i < GetNFilesXTotIA(); ++i)
      fprintf(f, "%s     - %s\n", indent.c_str(), fFilesXTotIA[i].c_str());

   for (Int_t i = 0; i < GetNFilesXProd(); ++i)
      fprintf(f, "%s     - %s\n", indent.c_str(), fFilesXProd[i].c_str());

   for (Int_t i = 0; i < GetNFilesXTotINA(); ++i)
      fprintf(f, "%s     - %s\n", indent.c_str(), fFilesXTotINA[i].c_str());

   for (Int_t i = 0; i < GetNFilesXDiffINA(); ++i)
      fprintf(f, "%s     - %s\n", indent.c_str(), fFilesXDiffINA[i].c_str());

   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUXSections::ReadFileHeader(ifstream &f_read, string const &f_name, string &f_type, string &f_unit,
                                 string &f_targets, TUAxis &axisekn_in, TUAxis &axisekn_out,
                                 Int_t &f_repeatedloop, Bool_t is_verbose, FILE *f_log)
{
   //--- Read file 'header' (infos before providing X-sections').
   // INPUT:
   //  f_read             File from which X-Sec are read
   //  f_name             File name associated to f_read
   //  is_verbose         Verbose or not when class is set
   //  f_log              Log for USINE
   // OUTPUTS:
   //  f_type             Type of reaction: total (sigma) or differential (dsigma)
   //  f_unit             Unit of reaction: mb or cm2 if f_type=sigma, mb/GeV or cm2/GeV if f_type=dsigma
   //  f_targets          Comma-separated list of targets
   //  axisekn_in         Ekn grid of projectile energies in file
   //  axisekn_out        Ekn grid of fragments energies in file (unused if total X-section)
   //  f_repeatedloop     Order of the loop for incoming and outgoing energies (unused if total X-section)

   string indent = TUMessages::Indent(true);
   TUMessages::Indent(false);

   Double_t f_eknmin_in, f_eknmin_out, f_eknmax_in, f_eknmax_out;
   Int_t f_nin, f_nout;

   Int_t i_line = 0;
   do {
      string tmp_line;

      // Read current line
      getline(f_read, tmp_line);

      // if it is a comment (start with #) or a blank line, skip it
      string line = TUMisc::RemoveBlancksFromStartStop(tmp_line);
      if (line[0] == '#' || line.empty()) continue;
      else ++i_line;

      // 1st non-commented line should contain
      // #  Type  Unit     Targets
      if (i_line == 1) {
         vector<string> first;
         TUMisc::String2List(tmp_line, " \t", first);
         f_type = first[0];
         TUMisc::UpperCase(f_type);
         f_unit = first[1];
         TUMisc::UpperCase(f_unit);
         f_targets = first[2];

         if (f_type == "DSIGMA") {
            // Check correct unit for a total cross section
            if (f_unit != "CM2/GEV" && f_unit != "MB/GEV") {
               string message = "f_unit=" + f_unit + " in " + f_name + " is not defined for type=DSIGMA...";
               TUMessages::Error(f_log, "TUXSections", "ReadFileHeader", message);
            }
            // If verbose
            if (is_verbose) {
               if (f_unit == "MB/GEV")
                  fprintf(f_log, "%s        - Unit: %s\n", indent.c_str(), f_unit.c_str());
               else if (f_unit == "CM2/GEV")
                  fprintf(f_log, "%s        - Unit: %s (converted in mb/GeV for propagation)\n", indent.c_str(), f_unit.c_str());
            }
         } else if (f_type == "SIGMA") {
            // Check correct unit for a total cross section
            if (f_unit != "CM2" && f_unit != "MB") {
               string message = "f_unit=" + f_unit + " in " + f_name + " is not defined for type=SIGMA...";
               TUMessages::Error(f_log, "TUXSections", "ReadFileHeader", message);
            }
            // If verbose
            if (is_verbose) {
               if (f_unit == "MB")
                  fprintf(f_log, "%s        - Unit: %s\n", indent.c_str(), f_unit.c_str());
               else if (f_unit == "CM2")
                  fprintf(f_log, "%s        - Unit: %s (converted in mb/GeV for propagation)\n", indent.c_str(), f_unit.c_str());
            }
         } else {
            string message = "f_type=" + f_type + " in " + f_name + " is not a valid type: "
                             + "must be SIGMA (total X-sec) or DSIGMA (differential X-sec)...";
            TUMessages::Error(f_log, "TUXSections", "ReadFileHeader", message);
         }
         if (is_verbose)
            fprintf(f_log, "%s        - Targets available: %s\n", indent.c_str(), f_targets.c_str());
      } else if (i_line == 2) {
         // 2nd non-commented line should contain
         // # EknMIN[GeV/n]   EknMAX[GeV/n]   nEkn  (PROJECTILE GRID)
         vector<string> second;
         TUMisc::String2List(tmp_line, " \t", second);
         f_eknmin_in = atof(second[0].c_str());
         f_eknmax_in = atof(second[1].c_str());
         f_nin = atoi(second[2].c_str());
         axisekn_in.SetClass(f_eknmin_in, f_eknmax_in, f_nin, "Ekn_in", "GeV/n", kLOG);
         if (is_verbose)
            fprintf(f_log, "%s        - EknMIN_in=%le EknMAX_in=%le f_nin=%d\n",
                    indent.c_str(), f_eknmin_in, f_eknmax_in, f_nin);
         if (f_type == "SIGMA")
            return;
      } else if (i_line == 3) {
         // 3rd non-commented line should contain
         // # EknMIN[GeV/n]   EknMAX[GeV/n]   nEkn  (FRAGMENT GRID)
         vector<string> third;
         TUMisc::String2List(tmp_line, " \t", third);
         f_eknmin_out = atof(third[0].c_str());
         f_eknmax_out = atof(third[1].c_str());
         f_nout = atoi(third[2].c_str());
         axisekn_out.SetClass(f_eknmin_out, f_eknmax_out, f_nout, "Ekn_out", "GeV/n", kLOG);
         if (is_verbose)
            fprintf(f_log, "%s        - EknMIN_out=%le EknMAX_out=%le f_nout=%d\n",
                    indent.c_str(), f_eknmin_out, f_eknmax_out, f_nout);
      } else if (i_line == 4) {
         // 3rd non-commented line should contain
         // # Repeated loop
         f_repeatedloop = atoi(tmp_line.c_str());
         return;
      }
   } while (1);
   return;
}

//______________________________________________________________________________
void TUXSections::ResetProdZeroFor1StepReactionNotCRToX(Int_t j_cr)
{
   //--- Sets to 0 all production cross-sections not j_cr->X.
   //  j_cr              Index of CR used as progenitor

   for (Int_t j_test = 0; j_test < fCrE->GetNCRs(); ++j_test) {
      // Loop over CR and all its ghosts if enabled
      Int_t g_max = -1;
      if (fCrE->IsGhostsLoaded())
         g_max = fCrE->GetCREntry(j_test).GetNGhosts() - 1;

      // Loop on all parents
      for (Int_t j_p = 0; j_p < fCrE->GetNParents(j_test); ++j_p) {
         // Set all to zero, but for reactions for which the parent is jP
         if (fCrE->IndexInCRList_Parent(j_test, j_p) == j_cr)
            continue;
         // Set the rest to 0 (depends whether differential or straight-ahead prod)
         if (IsProdSigmaOrdSigma(j_p, j_test)) {
            for (Int_t g_ghost = g_max; g_ghost >= -1; --g_ghost) {
               for (Int_t tt = 0; tt < GetNTargets(); ++tt) {
                  for (Int_t kk = 0; kk < GetNE(); ++kk)
                     SetProdSig(j_p, tt, j_test, kk, 0., g_ghost);
               }
            }
         } else {
            for (Int_t g_ghost = g_max; g_ghost >= -1; --g_ghost) {
               for (Int_t tt = 0; tt < GetNTargets(); ++tt) {
                  for (Int_t kk_in = 0; kk_in < GetNE(); ++kk_in) {
                     for (Int_t kk_out = 0; kk_out < GetNE(); ++kk_out)
                        SetProdDSigDEk(j_p, tt, j_test, kk_in, kk_out, 0., g_ghost);
                  }
               }
            }
         }
      }
   }
}

//______________________________________________________________________________
void TUXSections::ResetProdZeroFor2StepReactionNotCRToInterToX(Int_t j_cr, Int_t j_inter)
{
   //--- Sets to 0 all production cross-sections not 1->2->X.
   //  j_cr               Index of CR used as progenitor
   //  j_inter            Index of CR used as intermediary

   for (Int_t j_test = 0; j_test < fCrE->GetNCRs(); ++j_test) {
      // Loop over CR and all its ghosts if enabled
      Int_t g_max = -1;
      if (fCrE->IsGhostsLoaded())
         g_max = fCrE->GetCREntry(j_test).GetNGhosts() - 1;

      // Loop on all parents
      for (Int_t j_p = 0; j_p < fCrE->GetNParents(j_test); ++j_p) {
         Int_t jp_in_loc = fCrE->IndexInCRList_Parent(j_test, j_p);
         // Set all to 0., but for reactions jP->j_inter and j_inter->X
         if ((jp_in_loc == j_cr && j_test == j_inter) || jp_in_loc == j_inter)
            continue;
         // Set the rest to 0 (depends whether differential or straight-ahead prod)
         if (IsProdSigmaOrdSigma(j_p, j_test)) {
            for (Int_t g_ghost = g_max; g_ghost >= -1; --g_ghost) {
               for (Int_t tt = 0; tt < GetNTargets(); ++tt) {
                  for (Int_t kk = 0; kk < GetNE(); ++kk)
                     SetProdSig(j_p, tt, j_test, kk, 0., g_ghost);
               }
            }
         } else {
            for (Int_t g_ghost = g_max; g_ghost >= -1; --g_ghost) {
               for (Int_t tt = 0; tt < GetNTargets(); ++tt) {
                  for (Int_t kk_in = 0; kk_in < GetNE(); ++kk_in) {
                     for (Int_t kk_out = 0; kk_out < GetNE(); ++kk_out)
                        SetProdDSigDEk(j_p, tt, j_test, kk_in, kk_out, 0., g_ghost);
                  }
               }
            }
         }
      }
   }
}
//______________________________________________________________________________
void TUXSections::SecondaryProduction(Double_t *dndekn_proj, Int_t l_proj, Int_t j_cr,
                                      TUMediumEntry *medium, Double_t *output,
                                      Double_t const &eps, Bool_t is_ism0D, FILE *f_log)
{
   //--- Calculates secondary source term for a given CR projectile over all ISM
   //    targets (ISM_0D) [dN/dEkn_proj(Ekn)]/Myr and add result to 'output[NE].
   //    It works indifferently for
   //      - straight-ahead (SA) production (Ekn conserved, with sigma in [mb]);
   //      - differential production (dsigma in [mb/GeV]).
   //    N.B.: for differential production, an extra factor appears, because the
   //    diff. X-sec available is dSig/dEkn not dSig/DEkn! The integration uses a
   //    Simpson adaptive log step (TNumMethods::IntegrationSimpsonAdapt), which
   //    doubles the step until the user-desired convergence criterion (eps) is met.
   //    Actually, given the finite number of E bins, convergence is not always reached,
   //    and a warning message indicates over which range it fails: the user can always
   //    increase the number of energy steps in the parameter file to improve the convergence.
   //
   // INPUTS
   //  dndekn_proj        Differential density dN/dEkn (on Ekn proj. grid) for projectile [#part/(m^3.GeV/n)]
   //  l_proj             Index of parent (in parent list)
   //  j_cr               CR index (in CR list)
   //  medium             ISM(TXYZ)
   //  output             [NEkn] Current content of secondary production
   //  eps                Relative precision sought for integration (differential prod)
   //  is_ism0D           If true (constant ISM), enable used of pre-calculated values (for speed)
   //  f_log              Log for USINE
   // OUTPUT:
   //  output             [NEkn] Add secondaries for this projectile for all energies
   //
   //BEGIN_LATEX(fontsize=14, separator='=', align='rl')
   //     #frac{dQ_{sec}^{SA~prod.}}{dEkn_{out}}(proj, frag, Ekn_{out})=#frac{dN^{proj}}{dEkn_{out}}(Ekn_{out})  #times  <  #Gamma^{prod}(proj#rightarrow frag, Ekn_{out})  >_{ISM0D}
   //     #frac{dQ_{sec}^{diff.~prod.}}{dEkn_{out}}(proj, frag, Ekn_{out})=#int_{0}^{#infty} #frac{dN^{proj}}{dEkn_{in}}(Ekn_{in}) #times A_{frag} #times <  #frac{d#Gamma^{prod}}{dEk_{out}} (proj#rightarrow frag, Ekn_{in}#rightarrow Ekn_{out})  >_{ISM0D} dEkn_{in}
   //     N.B.: extra factor A_{frag} for diff.X.Sec from #frac{d#sigma}{dEkn_{out}}= A_{frag} #times #frac{d#sigma}{dEk_{out}}
   //END_LATEX


   // If ISM0D (constant density), calculate once for
   // all, production rates not if previously filled
   if (is_ism0D && !fnvDSigDEkProd_ISM0D)
      FillInterRate_ISM0D(medium, false);

   // STRAIGHT-AHEAD APPROXIMATION
   if (TUXSections::IsProdSigmaOrdSigma(l_proj, j_cr)) {
      if (is_ism0D) {
         for (Int_t k = GetNE() - 1; k >= 0; --k) {
            output[k] += dndekn_proj[k] * Get_nvSigProd_ISM0D(l_proj, j_cr, k);
         }
         return;
      } else {
         for (Int_t k = GetNE() - 1; k >= 0; --k)
            output[k] += dndekn_proj[k] * ISMWeightedProd_nvSig(l_proj, j_cr, k, medium);
         return;
      }
   } else {
      // DIFFERENTIAL CROSS-SECTION

      // Index parent in list of CR
      Int_t j_in_loc = fCrE->IndexInCRList_Parent(j_cr, l_proj);
      TUAxis *axis = fCrE->GetE(j_in_loc);

      vector<Bool_t> is_converged;
      Double_t threshold = 1.e-50;
      Bool_t is_force_power_of_two = false;
      if (is_ism0D) {
         for (Int_t k_out = GetNE() - 1; k_out >= 0; --k_out) {
            // [nvdsig] = [/(GeV Myr)]
            Double_t *nvdsig = Get_nvDSigDEkProd_ISM0D(l_proj, j_cr, k_out);

            // Adapt k_min and k_max to non-null values
            Int_t k_min = 0;
            Int_t k_max = GetNE() - 1;
            TUMisc::OptimiseRange(nvdsig, dndekn_proj, threshold, GetNE(), k_min, k_max, is_force_power_of_two);

            Double_t res = 0.;
            // If set to false, use standard Simpson integration, otherwise use adaptive Simpson
            // (beware that the latter becomes inaccurate for a energy grid with few points
            //  as the integrand contains many zeros).
            if (!is_force_power_of_two) {
               if (k_min > 0 && (k_max - k_min) % 2 != 0)
                  k_min -= 1;
               TUNumMethods::IntegrationSimpsonLog(axis, dndekn_proj, nvdsig, k_min, k_max, res);
               is_converged.push_back(true);
            } else {
               // Perform integration between k_min and k_max:  we check whether convergence
               // (precision eps of integration) is reached or not, but only if flux is non-null!
               if (!TUNumMethods::IntegrationSimpsonAdapt(axis, dndekn_proj, nvdsig, k_min, k_max, res, eps, "TUXSections::SecondaryProduction", false, -1)
                     && fabs(res) > threshold) {
                  //cout << "NOT OK : " << fCrE->GetCREntry(j_in_loc).GetName() << " -> "
                  //     << fCrE->GetCREntry(j_cr).GetName() << " @ " << fCrE->GetE(j_cr)->GetVal(k_out) << endl;
                  is_converged.push_back(false);
                  //for (Int_t k_in = GetNE() - 1; k_in >= 0; --k_in) {
                  //   if (k_in==k_min)
                  //      cout << "  [k_min] ";
                  //   else if (k_in==k_max)
                  //      cout << "  [k_max] ";
                  //   else
                  //      cout << "          ";
                  //   cout << "  k_in=" << k_in << "  Ekn_in=" << axis->GetVal(k_in) << "  dndekn_proj=" << dndekn_proj[k_in]
                  //        <<  "  nvdsig=" << nvdsig[k_in] << endl;
                  //}
               } else {
                  //cout << "    OK : " << fCrE->GetCREntry(j_in_loc).GetName() << " -> "
                  //     << fCrE->GetCREntry(j_cr).GetName() << " @ " << fCrE->GetE(j_cr)->GetVal(k_out) << endl;
                  is_converged.push_back(true);
               }
            }
            nvdsig = NULL;

            if (res > 0.)
               output[k_out] += (Double_t)fCrE->GetCREntry(j_cr).GetA() * res;
         }
      } else {
         for (Int_t k_out = GetNE() - 1; k_out >= 0; --k_out) {
            // [nvdsig] = [/(GeV Myr)]
            vector<Double_t> nvdsig(GetNE(), 0.);
            for (Int_t k_in = 0; k_in < GetNE(); ++k_in)
               nvdsig[k_in] = ISMWeightedProd_nvDSigDEk(l_proj, j_cr, k_in, k_out, medium);

            // Adapt k_min and k_max to non-null values
            Int_t k_min = 0;
            Int_t k_max = GetNE() - 1;
            TUMisc::OptimiseRange(&nvdsig[0], dndekn_proj, threshold, GetNE(), k_min, k_max, is_force_power_of_two);

            Double_t res = 0.;

            // Perform integration between k_min and k_max:  we check whether convergence
            // (precision eps of integration) is reached or not, but only if flux is non-null!
            if (!is_force_power_of_two) {
               if (k_min > 0 && (k_max - k_min) % 2 != 0)
                  k_min -= 1;
               TUNumMethods::IntegrationSimpsonLog(axis, dndekn_proj, &nvdsig[0], k_min, k_max, res);
               is_converged.push_back(true);
            } else {
               if (!TUNumMethods::IntegrationSimpsonAdapt(axis, dndekn_proj, &nvdsig[0], k_min, k_max, res, eps, "TUXSections::SecondaryProduction", false, - 1)
                     && fabs(res) > threshold)
                  is_converged.push_back(false);
               else
                  is_converged.push_back(true);
            }
            if (res > 0.)
               output[k_out] += (Double_t)fCrE->GetCREntry(j_cr).GetA() * res;
         }
      }

      // Warning message if convergence not reached
      Int_t k_converged_min = 0;
      Int_t k_converged_max = GetNE() - 1;
      for (Int_t k = 0; k < GetNE() - 1; ++k) {
         if (is_converged[k] && is_converged[k + 1]) {
            k_converged_min = k;
            break;
         }
      }
      for (Int_t k = GetNE() - 1; k > 0; --k) {
         if (is_converged[k] && is_converged[k - 1]) {
            k_converged_max = k;
            break;
         }
      }

      if (k_converged_min == 0 && k_converged_max < GetNE() - 1) {
         string message = Form("Precision eps=%f (for numerical integration) not reached for %s -> %s @ Ekn >=%.3le [GeV/n]",
                               eps, fCrE->GetCREntry(j_in_loc).GetName().c_str(), fCrE->GetCREntry(j_cr).GetName().c_str(),
                               fCrE->GetEkn(j_cr, k_converged_max + 1, kEKN));
         TUMessages::Warning(f_log, "TUXSections", "SecondaryProduction", string(message));
      } else if (k_converged_min > 0 && k_converged_max == GetNE() - 1) {
         string message = Form("Precision eps=%f (for numerical integration) not reached for %s -> %s @ Ekn <=%.3le [GeV/n]",
                               eps, fCrE->GetCREntry(j_in_loc).GetName().c_str(), fCrE->GetCREntry(j_cr).GetName().c_str(),
                               fCrE->GetEkn(j_cr, k_converged_min + 1, kEKN));
         TUMessages::Warning(f_log, "TUXSections", "SecondaryProduction", string(message));
      } else if (k_converged_min > k_converged_max) {
         string message = Form("Precision eps=%f (for numerical integration) not reached for %s -> %s @ all Ekn",
                               eps, fCrE->GetCREntry(j_in_loc).GetName().c_str(),
                               fCrE->GetCREntry(j_cr).GetName().c_str());
         TUMessages::Warning(f_log, "TUXSections", "SecondaryProduction", string(message));
      } else if (k_converged_min > 0 && k_converged_max < GetNE() - 1) {
         string message = Form("Precision eps=%f (for numerical integration) not reached for %s -> %s @ Ekn outside the range [%.3le,%.3le] GeV/n",
                               eps, fCrE->GetCREntry(j_in_loc).GetName().c_str(), fCrE->GetCREntry(j_cr).GetName().c_str(),
                               fCrE->GetEkn(j_cr, k_converged_min, kEKN),
                               fCrE->GetEkn(j_cr, k_converged_max, kEKN));
         TUMessages::Warning(f_log, "TUXSections", "SecondaryProduction", string(message));
      }
      axis = NULL;
   }
}

//______________________________________________________________________________
void TUXSections::SecondaryProduction(Double_t *dndekn_proj, Int_t l_proj, Int_t j_cr,
                                      TUMediumTXYZ *medium_txyz, TUCoordTXYZ *coord_txyz,
                                      Double_t *output, Double_t const &eps, FILE *f_log)
{
   //--- Calculates secondary source term for a given CR projectile over all ISM targets
   //    (at coord_txyz) [dN/dEkn_proj(Ekn)]/Myr and add result to 'output[NE]'. It works
   //    indifferently for
   //      - straight-ahead (SA) production (Ekn conserved, with sigma in [mb]);
   //      - differential production (dsigma in [mb/GeV]).
   //    N.B.: for differential production, an extra factor appears, because the
   //    diff. X-sec available is dSig/dEkn not dSig/DEkn! The integration uses a
   //    simpson adaptive log step (TNumMethods::IntegrationSimpsonAdapt), which
   //    doubles the step until the user-desired convergence criterion (eps) is met.
   //    Actually, given the finite number of E bins, convergence is not always reached,
   //    and a warning message indicates over which range it fails: the user can always
   //    increase the number of energy steps in the parameter file to improve the convergence.
   //
   // INPUTS:
   //  dndekn_proj        Differential density dN/dEkn (on Ekn proj. grid) for projectile [#part/(m^3.GeV/n)]
   //  l_proj             Index of parent (in parent list)
   //  j_cr               CR index (in CR list)
   //  medium_txyz        ISM(TXYZ)
   //  coord_txyz         Space-time coordinates T, X, Y, and Z
   //  eps                Relative precision sought for integration (differential prod)
   //  f_log              Log for USINE
   // OUTPUT:
   //  output             [NEkn] Returns for all energies the calculation below
   //
   //BEGIN_LATEX(fontsize=14, separator='=', align='rl')
   //     #frac{dQ_{sec}^{SA prod.}}{dEkn_{out}}(proj, frag, Ekn_{out})=#frac{dN^{proj}}{dEkn_{out}}(Ekn_{out})  #times  <  #Gamma^{prod}(proj#rightarrow frag, Ekn_{out})  >_{ISM(coord_txyz)}
   //     #frac{dQ_{sec}^{diff. prod.}}{dEkn_{out}}(proj, frag, Ekn_{out})=#int_{0}^{#infty} #frac{dN^{proj}}{dEkn_{in}}(Ekn_{in}) #times A_{frag} #times <  #frac{d#Gamma^{prod}}{dEk_{out}} (proj#rightarrow frag, Ekn_{in} #rightarrow Ekn_{out})  >_{ISM(coord_txyz)} dEkn_{in}
   //     N.B.: extra factor A_{frag} from #frac{d#sigma}{dEkn_{out}}= A_{frag} #times #frac{d#sigma}{dEk_{out}}
   //END_LATEX

   // Fill Medium entry
   medium_txyz->FillMediumEntry(coord_txyz);
   SecondaryProduction(dndekn_proj, l_proj, j_cr, medium_txyz->GetMediumEntry(), output, eps, false, f_log);
}

//______________________________________________________________________________
void TUXSections::SetClass(TUInitParList *init_pars, Bool_t is_verbose, FILE *f_log, TUAxesCrE *use_axescre)
{
   //--- Sets class parameters.
   //  init_pars          TUInitParList class of initialisation parameters
   //  use_axescre        If non-NULL, does not create fCrE but points to an existing one 'use_axescre'
   //  is_verbose        Verbose or not when class is set
   //  f_log             Log for USINE

   string indent = TUMessages::Indent(true);
   if (is_verbose)
      fprintf(f_log, "%s[TUXSections::SetClass]\n", indent.c_str());

   // Check and free memory of Xsec if necessary
   Initialise(true);

   if (use_axescre) {
      fCrE = use_axescre;
      fIsDeleteCrE = false;
   } else {
      fCrE = new TUAxesCrE();
      fCrE->SetClass(init_pars, is_verbose, f_log);
      fIsDeleteCrE = true;
   }

   const Int_t gg = 3;
   string gr_sub_name[gg] = {"Base", "XSections", ""};

   if (is_verbose)
      fprintf(f_log, "%s### Read Destruction and Production cross-sections\n", indent.c_str());

   //--- Set targets (N.B.: subgroup for Target is "MediumCompo")
   fTargets.clear();
   gr_sub_name[1] = "MediumCompo";
   gr_sub_name[2] = "Targets";
   TUMisc::String2List(init_pars->GetParEntry(init_pars->IndexPar(gr_sub_name)).GetVal(), ",", fTargets);
   for (Int_t i = 0; i < GetNTargets(); ++i)
      TUMisc::UpperCase(fTargets[i]);
   if (is_verbose)
      PrintTargetList(f_log);

   //--- Set tertiaries
   gr_sub_name[1] = "XSections";
   gr_sub_name[2] = "Tertiaries";
   UpdateTertiaryList(init_pars->GetParEntry(init_pars->IndexPar(gr_sub_name)).GetVal(), f_log);
   if (is_verbose)
      PrintTertiaryList(f_log);

   // Allocate and fill/initialise
   AllocateAndInitialiseXSec();

   //--- Set fTotIA, fProd, etc. (and check X-sec filled for all CRs)
   const Int_t n_x = 4;
   string subgr_name[n_x] = {"fTotInelAnn", "fProd", "fTotInelNonAnn", "fdSigdENAR"};
   string text_base[n_x] = {"DESTRUCTION", "PRODUCTION", "TOTAL INEL. NON-ANN [TERTIARIES]", "DIFF. NON-ANN RESCAT [TERTIARIES]"};
   string text_long[n_x] = {"Inel.tot. Ann. cross-sections are filled for all (anti)-nuclei and targets",
                            "all production cross sections reactions for all parents/CRs are filled",
                            "Tot. INA X-sec are filled for all declared tertiaries",
                            "Diff. INA X-sec are filled for all declared tertiaries"
                           };


   for (Int_t jj = 0; jj < n_x; ++jj) {
      // Skip tertiaries if none
      if ((jj == 2 || jj == 3) &&  GetNTertiaries() < 1)
         continue;
      else
         text_long[jj] = text_long[jj] + string(" (i.e. ") + ExtractTertiaries() + string(")");

      gr_sub_name[2] = subgr_name[jj];
      if (is_verbose) {
         fprintf(f_log, "%s  * %s [%s#%s#%s]\n", indent.c_str(), text_base[jj].c_str(),
                 gr_sub_name[0].c_str(), gr_sub_name[1].c_str(), gr_sub_name[2].c_str());
         fprintf(f_log, "%s    -----------\n", indent.c_str());
      }

      Int_t i_multival = init_pars->IndexPar(gr_sub_name);
      Int_t n_multival = init_pars->GetParEntry(i_multival).GetNVals();
      for (Int_t i = 0; i < n_multival; ++i) {
         if (jj == 0)
            LoadOrUpdateTotInel(TUMisc::GetPath(init_pars->GetParEntry(i_multival).GetVal(i)), true, is_verbose, f_log);
         else if (jj == 1)
            LoadOrUpdateProd(TUMisc::GetPath(init_pars->GetParEntry(i_multival).GetVal(i)), is_verbose, f_log);
         else if (jj == 2)
            LoadOrUpdateTotInel(TUMisc::GetPath(init_pars->GetParEntry(i_multival).GetVal(i)), false, is_verbose, f_log);
         else if (jj == 3)
            LoadOrUpdateDSigDEkINA(TUMisc::GetPath(init_pars->GetParEntry(i_multival).GetVal(i)), is_verbose, f_log);
      }
      if (n_multival > 0) {
         if (is_verbose)
            fprintf(f_log, "%s      ... and check that %s!\n", indent.c_str(), text_long[jj].c_str());
         if ((jj == 0 && IsAllTotIAFilled()) || (jj == 1 && IsAllProdFilled())
               || (jj == 2 && IsAllTotINAFilled()) || (jj == 3 && IsAllDSigDEkINAFilled()))  {
            if (is_verbose)
               fprintf(f_log, "%s      => CHECK OK (all expected reactions filled)\n", indent.c_str());
         } else
            abort();
      }
   }

   if (is_verbose) {
      TUMessages::Separator(f_log, "Summary X-sections", 2);
      PrintTargetList(f_log);
      PrintTertiaryList(f_log);
      PrintXSecFiles(f_log);
      fprintf(f_log, "%s[TUXSections::SetClass] <DONE>\n\n", indent.c_str());
   }

   TUMessages::Indent(false);
}

//______________________________________________________________________________
Double_t TUXSections::TertiaryProduction(Double_t *dndekn, Int_t j_cr, Int_t k_ekn,
      TUMediumEntry *medium, Double_t const &eps,
      Bool_t is_ism0D, Bool_t &is_converged)
{
   //--- Returns tertiary source term (inel. non-annihil. rescattering)
   //       [dN/dEkn(Ekn)]/Myr = #part/(m^3.GeV/n)/Myr.
   //    Tertiaries (at ekn) come from the redistribution of a fraction of the
   //    population at higher energy minus the fraction that is moved to lower
   //    energy (INA interaction).
   //    N.B.: An extra factor appears in the integration, because the differential
   //    cross-section available is dSig/dEkn not dSig/DEkn! The integration uses a
   //    simpson adaptive log step (TNumMethods::IntegrationSimpsonAdapt), which
   //    doubles the step until the user-desired convergence criterion (eps) is met.
   //    Actually, given the finite number of E bins, convergence is not always
   //    reached, especially at high energy (though the user can always increase
   //    the number of energy steps in the parameter file to improve the convergence).
   //
   // INPUTS:
   //  dndekn             Differential density dN/dEkn (on Ekn grid) for CR [#part/(m^3.GeV/n)]
   //  j_tertiary         CR tertiary index (in list of tertiaries)
   //  k_ekn              Ekn index for CR
   //  medium             ISM density for a given space-time point
   //  eps                Relative precision sought for integration (differential prod)
   //  is_ism0D           If ISM of model is 0D (constant density), can optimise calculation
   // OUTPUT:
   //  is_converged       Whether or not convergence of integral over tertiary production is reached
   //
   //BEGIN_LATEX(fontsize=14, separator='=-', align='rl')
   //    #frac{dQ_{ter}^{CR}}{dEkn_{out}}(Ekn_{out}) = #int_{Ekn_{out}}^{#infty} #frac{dN^{CR}}{dEkn_{in}}(Ekn_{in}) #times A_{CR} #times < #frac{d#Gamma^{INA}}{dEk_{out}} (CR, Ekn_{in}#rightarrow Ekn_{out})>_{ISM0D} dEkn_{in}
   //                                                - #frac{dN^{CR}}{dEkn_{out}}(Ekn_{out}) < #Gamma^{INA} (CR, Ekn_{out}) >_{ISM0D}
   //     N.B.: extra factor A_{CR} from #frac{d#sigma}{dEkn_{out}}= A_{CR} #times #frac{d#sigma}{dEk_{out}}
   //END_LATEX

   // If j_cr is not a tertiary, nothing to calculate (return 0)
   if (!IsTertiary(j_cr))
      return 0.;

   // If ISM0D (constant density), calculate once for
   // all, production rates not if previously filled
   if (is_ism0D && !fnvDSigDEkProd_ISM0D)
      FillInterRate_ISM0D(medium, false);

   Int_t j_tertiary = MapCRIndex2INAIndex(j_cr);
   TUAxis *axis = fCrE->GetE(j_cr);
   Double_t res = 0.;

   // Integration has to start @ current energy (non-annihilating rescattering
   // can only proceed from energy higher than current energy). Note that we also
   // wish to evaluate whether or not convergence of the integration in calculation
   // is reached, so that an adaptive log-step integration must be used. However,
   // it requires a power of 2 number of points, and if we enforce this condition,
   // it is at the cost of the maximum energy used in the integration (because we
   // integrate values in arrays given on a finite number of points). In order to
   // get this estimate (convergence), the trick is to use a fixed number of bin
   // 'non-adaptive' integration most of the time, and use the adaptive
   // integration only when the number of points matches a power of 2.
   Double_t threshold = 1.e-30;

   // If OD ISM model (constant density)
   if (is_ism0D) {
      Double_t *nvdsig = Get_nvDSigDEkINA_ISM0D(j_tertiary, k_ekn);
      // Adapt optimal range: if range does not have a 'power of two' bins,
      // use non-adaptive integration
      Int_t k_min = k_ekn;
      Int_t k_max = GetNE() - 1;
      Bool_t is_power_of_two = false;
      res = 0.;
      TUMisc::OptimiseRange(dndekn, nvdsig, threshold, GetNE(), k_min, k_max, is_power_of_two);
      if (is_power_of_two && k_min == k_ekn) {
         is_converged = TUNumMethods::IntegrationSimpsonAdapt(axis, dndekn, nvdsig, k_min, k_max, res, eps, "TUXSections::TertiaryProduction", false, -1);
         //cout << fCrE->GetCREntry(j_cr).GetName() << " @ Ekn_out=" << axis->GetVal(k_ekn) << endl;
         //for (Int_t k=k_min; k<=k_max; ++k)
         //   cout << "      k=" << k << "  Ekn_in[k]=" << axis->GetVal(k) << "  dndekn[k]=" <<  dndekn[k]
         //         << "  nvdsig[k]=" << nvdsig[k] << "   dndekn*nvdsig[k]=" << dndekn[k]*nvdsig[k]
         //         << "  [is_converged="<< is_converged << "]" << endl;
      } else {
         //if (j_cr == 0 && (k_ekn == 30 || k_ekn == 40)) {
         //   cout << "  CR=" << fCrE->GetCREntry(j_cr).GetName() << " @  Ek_out=" << axis->GetVal(k_ekn) << endl;
         //   for (Int_t kk=k_ekn; kk<GetNE(); ++kk)
         //      cout << "       kk=" << kk << "  Ekn_in[kk]=" << axis->GetVal(kk)
         //           << "  dndekn[kk]=" <<  dndekn[kk]  << "  nvdsig[kk]=" << nvdsig[kk] << endl;
         //}

         // Fixed-step integration: we cannot evaluate convergence (so we set it to true)
         // Energy to start integration is k_ekn.
         TUNumMethods::IntegrationSimpsonLog(axis, dndekn, nvdsig, k_min, k_max, res);
         is_converged = true;
      }
      nvdsig = NULL;
      res *= (Double_t)fCrE->GetCREntry(j_cr).GetA();
      res += -dndekn[k_ekn] * Get_nvSigTotINA_ISM0D(j_tertiary, k_ekn);
   } else {
      // General case, recalculate!
      vector<Double_t> nvdsig(GetNE(), 0.);
      for (Int_t k_in = 0; k_in < GetNE(); ++k_in) {
         if (k_in <= k_ekn)
            nvdsig[k_in] = 0.;
         else
            nvdsig[k_in] = ISMWeightedTertINA_nvDSigDEk(j_tertiary, k_in, k_ekn, medium);
      }

      // Adapt optimal range: if range does not have a 'power of two' bins,
      // use non-adaptive integration
      Int_t k_min = k_ekn;
      Int_t k_max = GetNE() - 1;
      Bool_t is_power_of_two = false;
      res = 0.;
      TUMisc::OptimiseRange(dndekn, &nvdsig[0], threshold, GetNE(), k_min, k_max, is_power_of_two);
      if (is_power_of_two)
         is_converged = TUNumMethods::IntegrationSimpsonAdapt(axis, dndekn, &nvdsig[0], k_min, k_max, res, eps, "TUXSections::TertiaryProduction", false, -1);
      else {
         // Fixed-step integration: we cannot evaluate convergence and set it to true
         TUNumMethods::IntegrationSimpsonLog(axis, dndekn, &nvdsig[0], k_min, k_max, res);
         is_converged = true;
      }
      res *= (Double_t)fCrE->GetCREntry(j_cr).GetA();
      res += -dndekn[k_ekn] * ISMWeightedTotINA_nvSig(j_tertiary, k_ekn, medium);
   }
   axis = NULL;

   return res;
}

//______________________________________________________________________________
Double_t TUXSections::TertiaryProduction(Double_t *dndekn, Int_t j_cr, Int_t k_ekn,
      TUMediumTXYZ *medium_txyz, TUCoordTXYZ *coord_txyz,
      Double_t const &eps, Bool_t &is_converged)
{
   //--- Returns tertiary source term (inel. non-annihil. rescattering)
   //       [dN/dEkn(Ekn)]/Myr = #part/(m^3.GeV/n)/Myr.
   //    Tertiaries (at ekn) come from the redistribution of a fraction of the
   //    population at higher energy minus the fraction that is moved to lower
   //    energy (INA interaction).
   //    N.B.: An extra factor appears in the integration, because the differential
   //    cross-section available is dSig/dEkn not dSig/DEkn! The integration uses a
   //    Simpson adaptive log step (TNumMethods::IntegrationSimpsonAdapt), which
   //    doubles the step until the user-desired convergence criterion (eps) is met.
   //    Actually, given the finite number of E bins, convergence is not always
   //    reached, especially at high energy (though the user can always increase
   //    the number of energy steps in the parameter file to improve the convergence).
   //
   // INPUTS:
   //  dndekn             Differential density dN/dEkn (on Ekn grid) for CR [#part/(m^3.GeV/n)]
   //  j_tertiary         CR tertiary index (in list of tertiaries)
   //  k_ekn              Ekn index for CR
   //  medium             ISM density for a given space-time point
   //  coord_txyz        Space-time coordinates T, X, Y, and Z
   //  eps                Relative precision sought for integration (differential prod)
   // OUTPUT:
   //  is_converged       Whether or not convergence of integral over tertiary production is reached
   //
   //BEGIN_LATEX(fontsize=14, separator='=-', align='rl')
   //    #frac{dQ_{ter}^{CR}}{dEkn_{out}}(Ekn_{out}) = #int_{Ekn_{out}}^{#infty} #frac{dN^{CR}}{dEkn_{in}}(Ekn_{in}) #times A_{CR} #times < #frac{d#Gamma^{INA}}{dEk_{out}} (CR, Ekn_{in}#rightarrow Ekn_{out})>_{ISM(coord_txyz)} dEkn_{in}
   //                                                - #frac{dN^{CR}}{dEkn_{out}}(Ekn_{out}) < #Gamma^{INA} (CR, Ekn_{out}) >_{ISM(coord_txyz)}
   //     N.B.: extra factor A_{CR} from #frac{d#sigma}{dEkn_{out}}= A_{CR} #times #frac{d#sigma}{dEk_{out}}
   //END_LATEX

   // Fill Medium entry
   medium_txyz->FillMediumEntry(coord_txyz);
   return TertiaryProduction(dndekn, j_cr, k_ekn, medium_txyz->GetMediumEntry(), eps, false, is_converged);
}

//______________________________________________________________________________
void TUXSections::TEST(TUInitParList *init_pars, FILE *f)
{
   //--- Test function for this class (called in usine -t).
   //  init_pars          TUInitParList class of initialisation parameters
   //  f                  Output file for test

   string message = "Load all cross section files (tot. inelastic, production...).";
   TUMessages::Test(f, "TUXSections", message);


   // Set class and test methods
   fprintf(f, " * Load X-sec and check prints\n");
   fprintf(f, "   > SetClass(init_pars=\"%s\", is_verbose=true, f_log=f);\n", init_pars->GetFileNames().c_str());
   SetClass(init_pars, true, f);

   fprintf(f, "\n");
   fprintf(f, " * Check Print() methods\n");
   fprintf(f, "   > PrintESummary(f)\n");
   PrintESummary(f);
   fprintf(f, "   > PrintTargetList(f)\n");
   PrintTargetList(f);
   fprintf(f, "   > PrintTertiaryList(f)\n");
   PrintTertiaryList(f);
   fprintf(f, "   > PrintXSecFiles(f)\n");
   PrintXSecFiles(f);
   fprintf(f, "   > PrintListOfXSecProdEqualZero(f)\n");
   PrintListOfXSecProdEqualZero(f);
   fprintf(f, "\n");

   Int_t j_e = fCrE->Index("ELECTRON", true, f, false);
   Int_t j_1hbar = fCrE->Index("1H-BAR", true, f, false);
   Int_t j_7li = fCrE->Index("7Li", true, f, false);
   Int_t j_10be = fCrE->Index("10Be", true, f, false);
   Int_t j_12c = fCrE->Index("12C", true, f, false);
   //if (j_e >= 0 && j_1hbar >= 0 && j_7li >= 0 && j_10be >= 0 && j_12c >= 0) {
   //   fprintf(f, " * Check other methods, using indices\n"
   //          "      j_e=%d (%s)\n      j_1hbar=%d (%s)\n      j_7li=%d (%s)\n"
   //          "      j_10be=%d (%s)\n      j_12c=%d (%s)\n",
   //          j_e, fCrE->GetName(j_e).c_str(),  j_1hbar, fCrE->GetName(j_1hbar).c_str(),
   //          j_7li, fCrE->GetName(j_7li).c_str(), j_10be, fCrE->GetName(j_10be).c_str(),
   //          j_12c, fCrE->GetName(j_12c).c_str());
   if (j_1hbar >= 0 && j_7li >= 0 && j_10be >= 0 && j_12c >= 0) {
      fprintf(f, " * Check other methods, using indices\n"
              "      j_1hbar=%d (%s)\n      j_7li=%d (%s)\n"
              "      j_10be=%d (%s)\n      j_12c=%d (%s)\n",
              j_1hbar, fCrE->GetCREntry(j_1hbar).GetName().c_str(),
              j_7li, fCrE->GetCREntry(j_7li).GetName().c_str(), j_10be, fCrE->GetCREntry(j_10be).GetName().c_str(),
              j_12c, fCrE->GetCREntry(j_12c).GetName().c_str());
      //fprintf(f, "   > GetENativeNameUnit(j_e)         =>  %s\n", GetENativeNameUnit(j_e).c_str());
      fprintf(f, "   > GetENativeNameUnit(j_1hbar)     =>  %s\n", GetENativeNameUnit(j_1hbar).c_str());
      fprintf(f, "   > GetENativeNameUnit(j_7li)       =>  %s\n", GetENativeNameUnit(j_7li).c_str());
      fprintf(f, "   > GetENativeNameUnit(j_10be)      =>  %s\n", GetENativeNameUnit(j_10be).c_str());
      fprintf(f, "   > GetENativeNameUnit(j_12c)       =>  %s\n", GetENativeNameUnit(j_12c).c_str());
      //fprintf(f, "   > IsTertiary(j_e)                 =>  %d\n", IsTertiary(j_e));
      fprintf(f, "   > IsTertiary(j_1hbar)             =>  %d\n", IsTertiary(j_1hbar));
      fprintf(f, "   > IsTertiary(j_7li)               =>  %d\n", IsTertiary(j_7li));
      fprintf(f, "   > IsTertiary(j_10be)              =>  %d\n", IsTertiary(j_10be));
      fprintf(f, "   > IsProdSigmaOrdSigma(0,j_1hbar)  =>  %d\n", IsProdSigmaOrdSigma(0, j_1hbar));
      fprintf(f, "   > IsProdSigmaOrdSigma(0,j_7li)    =>  %d\n", IsProdSigmaOrdSigma(0, j_7li));
      fprintf(f, "   > IsProdSigmaOrdSigma(0,j_10be)   =>  %d\n", IsProdSigmaOrdSigma(0, j_10be));
      fprintf(f, "   > XS_EnhancementHE(j_10be, 0)   =>  %le\n", XS_EnhancementHE(j_10be, 0));
      fprintf(f, "   > XS_EnhancementHE(j_10be,NE/2) =>  %le\n", XS_EnhancementHE(j_10be, fCrE->GetNE() / 2));
      fprintf(f, "   > XS_EnhancementHE(j_10be, NE)  =>  %le\n", XS_EnhancementHE(j_10be, fCrE->GetNE() - 1));
      fprintf(f, "\n");
   }



   fprintf(f, " * Set ISM entry and check filling/retrieving quantities stored for 0D ISM (ISM entry)\n");
   fprintf(f, "   > TUMediumEntry ism; ... and fill...\n");
   TUMediumEntry ism;
   ism.SetElements("H,He");
   ism.SetDensity(1, 2.);
   ism.SetPlasmaT(1.e5);
   ism.SetDensityH(0.1, 0.5, 5.);
   fprintf(f, "   > ism.Print(f)\n");
   ism.Print(f);
   fprintf(f, "   > FillInterRate_ISM0D(&ism, is_force_refill=true);\n");
   FillInterRate_ISM0D(&ism, true);

   if (j_1hbar >= 0) {
      fprintf(f, "   > Get_nvSigTotIA_ISM0D(i_1hbar, k_ekn=0)  =>  %le\n",
              Get_nvSigTotIA_ISM0D(j_1hbar, 0));
      fprintf(f, "   > Get_nvSigTotIA_ISM0D(i_1hbar, k_ekn=10) =>  %le\n",
              Get_nvSigTotIA_ISM0D(j_1hbar, 10));
      fprintf(f, "   > Get_nvSigTotIA_ISM0D(i_1hbar, k_ekn=20) =>  %le\n",
              Get_nvSigTotIA_ISM0D(j_1hbar, 20));
      fprintf(f, "\n");
   }

   if (j_10be >= 0 && j_12c >= 0) {
      fprintf(f, "   > Int_t j_par_12c = fCrE->IndexInParentList(j_10be, \"12C\")\n");
      Int_t j_par_12c = fCrE->IndexInParentList(j_10be, "12C");
      fprintf(f, "   > j_par_12c=%d => fCrE->GetCREntry(fCrE->IndexInCRList_Parent(j_10be, j_par_12c)).GetName()=%s\n",
              j_par_12c, (fCrE->GetCREntry(fCrE->IndexInCRList_Parent(j_10be, j_par_12c)).GetName()).c_str());
      fprintf(f, "   > Get_nvSigProd_ISM0D(j_parent=j_par_12c, j_cr=j_10be, k_ekn=0)  =>  %le\n",
              Get_nvSigProd_ISM0D(j_par_12c, j_10be, 0));
      fprintf(f, "   > Get_nvSigProd_ISM0D(j_parent=j_par_12c, j_cr=j_10be, k_ekn=10) =>  %le\n",
              Get_nvSigProd_ISM0D(j_par_12c, j_10be, 10));
      fprintf(f, "   > Get_nvSigProd_ISM0D(j_parent=j_par_12c, j_cr=j_10be, k_ekn=20) =>  %le\n",
              Get_nvSigProd_ISM0D(j_par_12c, j_10be, 20));
      fprintf(f, "\n");
   }

   Int_t j_tert_1hbar = -1;
   if (j_1hbar >= 0)
      j_tert_1hbar = MapCRIndex2INAIndex(j_1hbar);
   if (j_tert_1hbar >= 0) {
      fprintf(f, "   > j_tert_1hbar = MapCRIndex2INAIndex(j_1hbar);\n");
      fprintf(f, "   > j_tert_1hbar=%d => GetNameTertiary(j_tert_1hbar)=%s, and MapINAIndex2CRIndex(j_tert_1hbar)=%d\n",
              j_tert_1hbar, GetNameTertiary(j_tert_1hbar).c_str(), MapINAIndex2CRIndex(j_tert_1hbar));
      fprintf(f, "   > Get_nvSigTotINA_ISM0D(j_tertiary=j_tert_1hbar,k_ekn=0)  =>  %le\n",
              Get_nvSigTotINA_ISM0D(j_tert_1hbar, 0));
      fprintf(f, "   > Get_nvSigTotINA_ISM0D(j_tertiary=j_tert_1hbar,k_ekn=10) =>  %le\n",
              Get_nvSigTotINA_ISM0D(j_tert_1hbar, 10));
      fprintf(f, "   > Get_nvSigTotINA_ISM0D(j_tertiary=j_tert_1hbar,k_ekn=20) =>  %le\n",
              Get_nvSigTotINA_ISM0D(j_tert_1hbar, 20));
      fprintf(f, "\n");
      fprintf(f, "\n");
      fprintf(f, "\n");
   }



   fprintf(f, " * Check overlapping parameters for XS nuisance parameters:\n");
   XSReacPars_t reac1, reac2;
   // One XS is 'ALL' for one parameter
   ParsXS_InitReac(reac1);
   ParsXS_InitReac(reac2);
   reac1.is_all = true;
   reac1.ipar_enhancepowhe = 0;
   reac1.name = "ALL";
   reac2.i_proj = 0;
   reac2.i_targ = 0;
   reac2.is_xs_ia = true;
   reac2.ipar_xsnorm = 0;
   reac2.name = "1H+H";
   ParsXS_IsOverlappingReacs("EnhancePowHE", reac1, reac2, f, false);
   ParsXS_IsOverlappingReacs("EnhancePowHE", reac2, reac1, f, false);
   reac1.ipar_enhancepowhe = 0;
   ParsXS_IsOverlappingReacs("EnhancePowHE", reac1, reac2, f, false);
   ParsXS_IsOverlappingReacs("EnhancePowHE", reac2, reac1, f, false);
   fprintf(f, "\n");

   // Next series of tests all for same nuisance parameter 'NORM'
   ParsXS_InitReac(reac1);
   ParsXS_InitReac(reac2);
   reac1.ipar_xsnorm = 1;
   reac2.ipar_xsnorm = 1;
   //
   // 1. IA XS
   reac1.is_xs_ia = true;
   reac2.is_xs_ia = true;
   //    ANY+T vs CR+T
   reac1.is_all_proj = true;
   reac1.i_targ = 0;
   reac1.name = "ANY+H";
   reac2.i_proj = 0;
   reac2.i_targ = 0;
   reac2.name = "1H+H";
   ParsXS_IsOverlappingReacs("Norm", reac1, reac2, f, false);
   ParsXS_IsOverlappingReacs("Norm", reac2, reac1, f, false);
   //    ANY+T vs CR+T2
   reac2.i_targ = 1;
   reac2.name = "1H+HE";
   ParsXS_IsOverlappingReacs("Norm", reac1, reac2, f, false);
   ParsXS_IsOverlappingReacs("Norm", reac2, reac1, f, false);
   //    CR+ANY vs CR+T
   reac1.is_all_proj = false;
   reac1.i_proj = 0;
   reac1.is_all_targ = true;
   reac1.i_targ = -1;
   reac1.name = "1H+ANY";
   reac2.i_targ = 0;
   reac2.name = "1H+H";
   ParsXS_IsOverlappingReacs("Norm", reac1, reac2, f, false);
   ParsXS_IsOverlappingReacs("Norm", reac2, reac1, f, false);
   //    CR+ANY vs CR2+T
   reac2.i_proj = 1;
   reac2.name = "2H+H";
   ParsXS_IsOverlappingReacs("Norm", reac1, reac2, f, false);
   ParsXS_IsOverlappingReacs("Norm", reac2, reac1, f, false);
   //    reac1 = reac2
   reac2 = reac1;
   ParsXS_IsOverlappingReacs("Norm", reac1, reac2, f, false);
   fprintf(f, "\n");
   //
   // 2. INA XS
   ParsXS_InitReac(reac1);
   ParsXS_InitReac(reac2);
   reac1.ipar_xsnorm = 1;
   reac2.ipar_xsnorm = 1;
   reac1.is_xs_ina = true;
   reac2.is_xs_ina = true;
   //    CR+ANY->CR vs CR+T->CR
   reac1.is_all_targ = true;
   reac1.i_proj = 0;
   reac1.i_frag = 0;
   reac1.name = "1H-BAR+ANY->1H-BAR";
   reac2.i_targ = 0;
   reac2.i_proj = 0;
   reac2.i_frag = 0;
   reac2.name = "1H-BAR+H->1H-BAR";
   ParsXS_IsOverlappingReacs("Norm", reac1, reac2, f, false);
   ParsXS_IsOverlappingReacs("Norm", reac2, reac1, f, false);
   //    CR+ANY vs CR2+T
   reac2.i_proj = 1;
   reac2.name = "2H+H";
   ParsXS_IsOverlappingReacs("Norm", reac1, reac2, f, false);
   ParsXS_IsOverlappingReacs("Norm", reac2, reac1, f, false);
   //    reac1 = reac2
   reac2 = reac1;
   ParsXS_IsOverlappingReacs("Norm", reac1, reac2, f, false);
   fprintf(f, "\n");
   //
   // 3. Prod XS
   //  If ( (reac1.(ANY+T->ANY or CR1+ANY->ANY or ANY+ANY->CR2) and reac2.(CR1+T->CR2))
   //    or all the above with 'reac1 <-> reac2'
   // or reac1.(ANY+T->CR2) and reac2.(CR1+T->CR2 or CR1+ANY->CR2 or CR1+T->ANY)
   // or reac1.(CR1+ANY->CR2) and reac2.(CR1+T->CR2 or ANY+T->CR2 or CR1+T->ANY)
   // or reac1.(CR1+T->ANY) and reac2.(CR1+T->CR2 or ANY+T->CR2 or CR1+ANY->CR2)
   // or (and all the above with 'reac1 <-> reac2')
   ParsXS_InitReac(reac1);
   ParsXS_InitReac(reac2);
   reac1.ipar_xsnorm = 1;
   reac2.ipar_xsnorm = 1;
   reac1.is_xs_prod = true;
   reac2.is_xs_prod = true;
   //
   reac2.i_proj = 0;
   reac2.i_targ = 0;
   reac2.i_frag = 0;
   reac2.name = "2H+H->1H";
   // ANY+T->ANY vs CR1+T->CR2
   // CR1+ANY->ANY vs CR1+T->CR2
   // ANY+ANY->CR2 vs CR1+T->CR2
   reac1.is_all_proj = true;
   reac1.is_all_targ = false;
   reac1.is_all_frag = true;
   reac1.i_proj = -1;
   reac1.i_targ = 0;
   reac1.i_frag = -1;
   reac1.name = "ANY+H->ANY";
   ParsXS_IsOverlappingReacs("Norm", reac1, reac2, f, false);
   ParsXS_IsOverlappingReacs("Norm", reac2, reac1, f, false);
   //
   reac1.is_all_proj = false;
   reac1.is_all_targ = true;
   reac1.is_all_frag = true;
   reac1.i_proj = 0;
   reac1.i_targ = -1;
   reac1.i_frag = -1;
   reac1.name = "2H+ANY->ANY";
   ParsXS_IsOverlappingReacs("Norm", reac1, reac2, f, false);
   ParsXS_IsOverlappingReacs("Norm", reac2, reac1, f, false);
   //
   reac1.is_all_proj = true;
   reac1.is_all_targ = true;
   reac1.is_all_frag = false;
   reac1.i_proj = -1;
   reac1.i_targ = -1;
   reac1.i_frag = 0;
   reac1.name = "ANY+ANY->1H";
   ParsXS_IsOverlappingReacs("Norm", reac1, reac2, f, false);
   ParsXS_IsOverlappingReacs("Norm", reac2, reac1, f, false);
   //
   // ANY+T->CR2 vs (CR1+T->CR2 or CR1+ANY->CR2 or CR1+T->ANY)
   reac1.is_all_proj = true;
   reac1.is_all_targ = false;
   reac1.is_all_frag = false;
   reac1.i_proj = -1;
   reac1.i_targ = 0;
   reac1.i_frag = 0;
   reac1.name = "ANY+H->1H";
   reac2.is_all_proj = false;
   reac2.is_all_targ = false;
   reac2.is_all_frag = false;
   reac2.i_proj = 0;
   reac2.i_targ = 0;
   reac2.i_frag = 0;
   reac2.name = "2H+H->1H";
   ParsXS_IsOverlappingReacs("Norm", reac1, reac2, f, false);
   ParsXS_IsOverlappingReacs("Norm", reac2, reac1, f, false);
   //
   reac2.is_all_proj = false;
   reac2.is_all_targ = true;
   reac2.is_all_frag = false;
   reac2.i_proj = 0;
   reac2.i_targ = -1;
   reac2.i_frag = 0;
   reac2.name = "2H+ANY->1H";
   ParsXS_IsOverlappingReacs("Norm", reac1, reac2, f, false);
   ParsXS_IsOverlappingReacs("Norm", reac2, reac1, f, false);
   //
   reac2.is_all_proj = false;
   reac2.is_all_targ = false;
   reac2.is_all_frag = true;
   reac2.i_proj = 0;
   reac2.i_targ = 0;
   reac2.i_frag = -1;
   reac2.name = "2H+H->ANY";
   ParsXS_IsOverlappingReacs("Norm", reac1, reac2, f, false);
   ParsXS_IsOverlappingReacs("Norm", reac2, reac1, f, false);
   // CR1+ANY->CR2 vs (CR1+T->CR2 or ANY+T->CR2 or CR1+T->ANY)
   reac1.is_all_proj = false;
   reac1.is_all_targ = true;
   reac1.is_all_frag = false;
   reac1.i_proj = 0;
   reac1.i_targ = -1;
   reac1.i_frag = 0;
   reac1.name = "2H+ANY->1H";
   reac2.is_all_proj = false;
   reac2.is_all_targ = false;
   reac2.is_all_frag = false;
   reac2.i_proj = 0;
   reac2.i_targ = 0;
   reac2.i_frag = 0;
   reac2.name = "2H+H->1H";
   ParsXS_IsOverlappingReacs("Norm", reac1, reac2, f, false);
   ParsXS_IsOverlappingReacs("Norm", reac2, reac1, f, false);
   //
   reac2.is_all_proj = false;
   reac2.is_all_targ = true;
   reac2.is_all_frag = false;
   reac2.i_proj = 0;
   reac2.i_targ = -1;
   reac2.i_frag = 0;
   reac2.name = "2H+ANY->1H";
   ParsXS_IsOverlappingReacs("Norm", reac1, reac2, f, false);
   ParsXS_IsOverlappingReacs("Norm", reac2, reac1, f, false);
   //
   reac2.is_all_proj = false;
   reac2.is_all_targ = false;
   reac2.is_all_frag = true;
   reac2.i_proj = 0;
   reac2.i_targ = 0;
   reac2.i_frag = -1;
   reac2.name = "2H+H->ANY";
   ParsXS_IsOverlappingReacs("Norm", reac1, reac2, f, false);
   ParsXS_IsOverlappingReacs("Norm", reac2, reac1, f, false);
   // CR1+T->ANY vs (CR1+T->CR2 or ANY+T->CR2 or CR1+ANY->CR2)
   reac1.is_all_proj = false;
   reac1.is_all_targ = false;
   reac1.is_all_frag = true;
   reac1.i_proj = 0;
   reac1.i_targ = 0;
   reac1.i_frag = -1;
   reac1.name = "2H+H->ANY";
   reac2.is_all_proj = false;
   reac2.is_all_targ = false;
   reac2.is_all_frag = false;
   reac2.i_proj = 0;
   reac2.i_targ = 0;
   reac2.i_frag = 0;
   reac2.name = "2H+H->1H";
   ParsXS_IsOverlappingReacs("Norm", reac1, reac2, f, false);
   ParsXS_IsOverlappingReacs("Norm", reac2, reac1, f, false);
   //
   reac2.is_all_proj = false;
   reac2.is_all_targ = true;
   reac2.is_all_frag = false;
   reac2.i_proj = 0;
   reac2.i_targ = -1;
   reac2.i_frag = 0;
   reac2.name = "2H+ANY->1H";
   ParsXS_IsOverlappingReacs("Norm", reac1, reac2, f, false);
   ParsXS_IsOverlappingReacs("Norm", reac2, reac1, f, false);
   //
   reac2.is_all_proj = false;
   reac2.is_all_targ = false;
   reac2.is_all_frag = true;
   reac2.i_proj = 0;
   reac2.i_targ = 0;
   reac2.i_frag = -1;
   reac2.name = "2H+H->ANY";
   ParsXS_IsOverlappingReacs("Norm", reac1, reac2, f, false);
   ParsXS_IsOverlappingReacs("Norm", reac2, reac1, f, false);
   fprintf(f, "\n");
   fprintf(f, "\n");
   fprintf(f, "\n");



   fprintf(f, " * Free parameters in XS:\n");
   fprintf(f, "   > TUFreeParList *pars = new TUFreeParList();...\n");
   const Int_t n_ptypes = 5;
   string types[n_ptypes] = {"Norm", "EAxisScale", "EThresh", "SlopeLE", "EnhancePowHE"};
   string types_unit[n_ptypes] = {"-", "-", "GeV/n", "-", "-"};
   const Int_t n_reacs = 4;
   string reacs[n_reacs] = {"12C+H->10Be", "ANY+He->10Be", "10Be+ANY", "1H-BAR+H->1H-BAR"};
   Double_t p_vals[n_ptypes][n_reacs] = { {10., 0.1, 1., 2.}, {2., 5., 10., 2.}, {1., 5., 10., 1.}, { -1., 0., 1., 1.}, {2., 1., 0., 1.}};

   TUXSections *xs_ref = Clone();
   TUFreeParList *pars = new TUFreeParList();

   // Loop on reacs
   for (Int_t i_reac = 0; i_reac < n_reacs; ++i_reac) {
      fprintf(f, " ** ADD REACTION %s\n", reacs[i_reac].c_str());
      // Loop on number of types to consider
      for (Int_t i_type = 0; i_type < n_ptypes; ++i_type) {
         string par_name = types[i_type] + "_" + reacs[i_reac];
         fprintf(f, "  - add parameter name %s\n", par_name.c_str());
         // Create new par to add to XS nuisance parameters
         pars->AddPar(par_name, types_unit[i_type], p_vals[i_type][i_reac]);

         // Print parameters and values
         fprintf(f, "   > pars->PrintPars(f);\n");
         pars->PrintPars(f);

         // Add pars as XS pars (and reset XS because one should add pars only once)
         Copy(*xs_ref);
         fprintf(f, "   > ParsXS_SetPars(pars, true /*is_use_or_copy*/, true/*is_verbose*/, f);\n");
         ParsXS_SetPars(pars, true /*is_use_or_copy*/, true/*is_verbose*/, f);
         //fprintf(f, "   > GetFreePars()->PrintPars(f);\n");
         //GetFreePars()->PrintPars(f);

         // Update values
         fprintf(f, "   > ParsXS_UpdateFromFreeParsAndResetStatus((TUMediumEntry*)NULL, is_verbose=true, is_update=true, f_log=f);\n");
         ParsXS_UpdateFromFreeParsAndResetStatus((TUMediumEntry *)NULL, true, true, f);

         // Print new XS values
         for (Int_t i = 0; i < ParsXS_GetNReac(); ++i) {
            fprintf(f, "   > ParsXS_PrintXSRefvsCurrent(f, reac=%d);\n", i);
            ParsXS_PrintXSRefvsCurrent(f, i);
         }
         fprintf(f, "\n");
         fprintf(f, "\n");
         fprintf(f, "\n");
      }
   }
   delete xs_ref;
   xs_ref = NULL;

   // Copy
   fprintf(f, " * Test Copy method:\n");
   fprintf(f, "   > TUXSections xsec; xsec.Copy(*this);\n");
   TUXSections xsec;
   xsec.Copy(*this);
   fprintf(f, "   > xsec.PrintESummary(f)\n");
   xsec.PrintESummary(f);
   fprintf(f, "   > xsec.PrintTargetList(f)\n");
   xsec.PrintTargetList(f);
   fprintf(f, "   > xsec.PrintTertiaryList(f)\n");
   xsec.PrintTertiaryList(f);
   fprintf(f, "   > xsec.PrintXSecFiles(f)\n");
   xsec.PrintXSecFiles(f);
   fprintf(f, "   > (xsec.GetMedium_ISM0D())->Print(f)\n");
   (xsec.GetMedium_ISM0D())->Print(f);
   fprintf(f, "\n");

   if (j_1hbar >= 0) {
      fprintf(f, "   > Get_nvSigTotIA_ISM0D(i_1hbar, k_ekn=0)  =>  %le\n",
              Get_nvSigTotIA_ISM0D(j_1hbar, 0));
      fprintf(f, "   > Get_nvSigTotIA_ISM0D(i_1hbar, k_ekn=10) =>  %le\n",
              Get_nvSigTotIA_ISM0D(j_1hbar, 10));
      fprintf(f, "   > Get_nvSigTotIA_ISM0D(i_1hbar, k_ekn=20) =>  %le\n",
              Get_nvSigTotIA_ISM0D(j_1hbar, 20));
      fprintf(f, "\n");
   }
   if (j_tert_1hbar >= 0) {
      fprintf(f, "   > Get_nvSigTotINA_ISM0D(j_tertiary=j_tert_1hbar,k_ekn=0)  =>  %le\n",
              Get_nvSigTotINA_ISM0D(j_tert_1hbar, 0));
      fprintf(f, "   > Get_nvSigTotINA_ISM0D(j_tertiary=j_tert_1hbar,k_ekn=10) =>  %le\n",
              Get_nvSigTotINA_ISM0D(j_tert_1hbar, 10));
      fprintf(f, "   > Get_nvSigTotINA_ISM0D(j_tertiary=j_tert_1hbar,k_ekn=20) =>  %le\n",
              Get_nvSigTotINA_ISM0D(j_tert_1hbar, 20));
      fprintf(f, "\n");
   }

   // And delete pars that was indirectly used for test/copy
   delete pars;
   pars = NULL;
}

//______________________________________________________________________________
void TUXSections::UpdateTertiaryList(string commasep_tertiaries, FILE *f_log)
{
   //--- Updates fTertiaries[NTertiaries] from names found in commasep_tertiaries.
   //  commasep_tertiaries     Comma-separated list of tertiary species names
   //  f_log                   Log for USINE

   // Convert list in vector
   fTertiaries.clear();
   vector <string> tert;
   TUMisc::String2List(commasep_tertiaries, ",", tert);

   // Allocate memory and Initialises INA indices
   if (fMapCRIndex2INAIndex) delete[] fMapCRIndex2INAIndex;
   fMapCRIndex2INAIndex = new Int_t[GetNCRs()];

   for (Int_t j_cr = GetNCRs() - 1; j_cr >= 0; --j_cr)
      fMapCRIndex2INAIndex[j_cr] = -1;

   // Check that tertiaries belong to the list of CRs
   // and add it to class member fTertiaries
   for (Int_t t = 0; t < (Int_t)tert.size(); ++t) {
      TUMisc::UpperCase(tert[t]);
      Int_t j_cr = fCrE->Index(tert[t], true, f_log);
      if (j_cr < 0) continue;
      fMapCRIndex2INAIndex[j_cr] = t;
      if (!TUMisc::IsInList(fTertiaries, tert[t], false))
         fTertiaries.push_back(tert[t]);
   }

   // Now that the tertiaries are defined, we can allocate
   // fMapINAIndex2CRIndex and fill it
   if (fMapINAIndex2CRIndex) delete[] fMapINAIndex2CRIndex;
   fMapINAIndex2CRIndex = new Int_t[GetNTertiaries()];
   for (Int_t j_tert = 0; j_tert < GetNTertiaries(); ++j_tert) {
      fMapINAIndex2CRIndex[j_tert] = -1;
      for (Int_t j_cr = GetNCRs() - 1; j_cr >= 0; --j_cr) {
         if (fMapCRIndex2INAIndex[j_cr] == j_tert) {
            fMapINAIndex2CRIndex[j_tert] = j_cr;
            //cout << j_tert << " (" << GetNameTertiary(j_tert) << ")  "
            //     << j_cr << "(" << fCrE->GetCREntry(j_cr).GetName() << ")" << endl;
            break;
         }
      }
   }
}

//______________________________________________________________________________
void TUXSections::XSLinComb_Load(vector<string> files, Bool_t is_inel_or_prod, Bool_t is_verbose, FILE *f_log)
{
   //--- Loads XS files (in order to form a new XS from a combination of the loaded XS).
   //    Note that we can load inelastic and production XS in turns without 'overwriting'.
   //  files             Vector of XS files to load
   //  is_inel_or_prod   Whether they are inelastic or production XS files
   //  is_verbose        Verbose or not when class is set
   //  f_log             Log for USINE

   string indent = TUMessages::Indent(true);
   if (is_verbose)
      fprintf(f_log, "%s[TUXSections::XSLinComb_Load]\n", indent.c_str());

   // Update the number of XS to use as LinComb
   if (is_inel_or_prod)
      fXSLinComb_NInelXS = 0;
   else
      fXSLinComb_NProdXS = 0;

   for (Int_t i = 0; i < (Int_t)files.size(); ++i) {
      // Update, among all XS loaded, the desired XS (inel, prod, etc.)
      // N.B.: it is not optimal to copy everything for each new XS dataset
      // (in terms of memory), but that the simplest to code given the existing
      // class functionalities!

      // If index of file larger than size of fXSLinComb, means that we need
      // to add a copy of XS on which to work (copy of XS)
      if (i >= (Int_t)fXSLinComb.size())
         fXSLinComb.push_back(*this);


      if (is_inel_or_prod) {
         if (is_verbose)
            fprintf(f_log, "%s   - Add inel. parametrisation used in linear combination: %s\n", indent.c_str(), files[i].c_str());
         // Clear current XS file to be sure to update values
         fXSLinComb[i].ClearFilesXTotIA();
         // Load XS values for current file
         fXSLinComb[i].LoadOrUpdateTotInel(files[i], true /*is_ia_or_ina*/, false, f_log);
         ++fXSLinComb_NInelXS;
      } else {
         if (is_verbose)
            fprintf(f_log, "%s   - Add prod. parametrisation used in linear combination: %s\n", indent.c_str(), files[i].c_str());
         // Clear current XS file to be sure to update values
         fXSLinComb[i].ClearFilesXProd();
         // Load XS values for current file
         fXSLinComb[i].LoadOrUpdateProd(files[i], false, f_log);
         ++fXSLinComb_NProdXS;
      }
   }

   if (is_verbose)
      fprintf(f_log, "%s[TUXSections::XSLinComb_Load] <DONE>\n", indent.c_str());
   TUMessages::Indent(false);
}


//______________________________________________________________________________
void TUXSections::XSLinComb_UpdateXSInel(vector <Double_t> &coeffs, Int_t j_cr, Int_t t_targ)
{
   //--- Updates j_cr+t_targ inelastic XS from linear combination based on coeffs.
   //  coeffs            Coefficients for linear combination
   //  j_cr              Index of CR for reaction
   //  t_targ            Index of target for reaction

   // Check that coeffs has the same size as number of available XS
   if ((Int_t)coeffs.size() != fXSLinComb_NInelXS) {
      string message = "Not the same number of coefficients and XS"
                       + (string)Form(" (%d coeffs vs %d XS)", (Int_t)coeffs.size(), fXSLinComb_NInelXS);
      TUMessages::Error(stdout, "TUXSections", "XSLinComb_UpdateXSInel", message);
   }

   // Loop on all energies
   for (Int_t k_ekn = 0; k_ekn < GetNE(); ++k_ekn) {
      Double_t xs = 0.;
      // Form linear combination
      for (Int_t l = 0; l < fXSLinComb_NInelXS; ++l) {
         xs += coeffs[l] * fXSLinComb[l].GetTotIA(j_cr, t_targ, k_ekn);
         //if (k_ekn == GetNE()-1)
         //   cout << l << " " << coeffs[l] << " " << fXSLinComb[l].GetTotIA(j_cr, t_targ, k_ekn) << endl;
      }
      //if (k_ekn == GetNE()-1)
      //   cout << "   xs_new=" << xs << endl;

      // Update XS
      SetTotIA(j_cr, t_targ, k_ekn, xs);
   }
}
//______________________________________________________________________________
void TUXSections::XSLinComb_UpdateXSProd(vector <Double_t> &coeffs, Int_t j_parent, Int_t t_targ, Int_t j_cr)
{
   //--- Updates j_parent+t_targ->j_cr production XS from linear combination based on coeffs.
   //  coeffs            Coefficients for linear combination
   //  j_parent          Index of CR parent for reaction
   //  t_targ            Index of target for reaction
   //  j_cr              Index of fragment for reaction

   // Check that coeffs has the same size as number of available XS
   if ((Int_t)coeffs.size() != fXSLinComb_NProdXS) {
      string message = "Not the same number of coefficients and XS"
                       + (string)Form(" (%d coeffs vs %d XS)", (Int_t)coeffs.size(), fXSLinComb_NProdXS);
      TUMessages::Error(stdout, "TUXSections", "XSLinComb_UpdateXSProd", message);
   }

   // Check whether differential or total production
   if (IsProdSigmaOrdSigma(j_parent, j_cr)) {
      // Loop on all energies
      for (Int_t k_ekn = 0; k_ekn < GetNE(); ++k_ekn) {
         Double_t xs = 0.;
         // Form linear combination
         for (Int_t l = 0; l < fXSLinComb_NProdXS; ++l)
            xs += coeffs[l] * fXSLinComb[l].GetProdSig(j_parent, t_targ, j_cr, k_ekn);
         // To avoid negative XS values (coeff can be negative)
         if (xs < 0.)
            xs = 0.;
         // Update XS
         SetProdSig(j_parent, t_targ, j_cr, k_ekn, xs);
      }
   } else {
      // Loop on all energies (projectiles an fragments)
      for (Int_t k_in = 0; k_in < GetNE(); ++k_in) {
         for (Int_t k_out = 0; k_out < GetNE(); ++k_out) {
            Double_t xs = 0.;
            // Form linear combination
            for (Int_t l = 0; l < fXSLinComb_NProdXS; ++l)
               xs += coeffs[l] * fXSLinComb[l].GetProdDSigDEk(j_parent, t_targ, j_cr, k_in, k_out);
            // To avoid negative XS values (coeff can be negative)
            if (xs < 0.)
               xs = 0.;
            // Update XS
            SetProdDSigDEk(j_parent, t_targ, j_cr, k_in, k_out, xs);
         }
      }
   }
}

//______________________________________________________________________________
Double_t TUXSections::XS_EnhancementHE(Int_t j_cr, Int_t k_ekn) const
{
   //--- Parametrisation of Ln^2(E) enhancement.
   //  j_cr              Index of CR
   //  k_ekn             Index of energy


   // A log^2(E) dependence is observed in hadron-hadron data
   // (see http://pdg.web.cern.ch/pdg/2017/reviews/rpp2017-rev-cross-section-plots.pdf)
   // It is not trivial to extend the dependence for nuclei, so if you have better or
   // more motivated guesses, change below.

   // We assume a threshold and that XS arise with Etot/n in nuclei
   Double_t etotn_thresh = 50.; // [GeV/n]
   Double_t etotn = fCrE->GetEtot(j_cr, k_ekn) / Double_t(fCrE->GetCREntry(j_cr).GetA());
   if (etotn > etotn_thresh)
      return TUInteractions::Sigpp(etotn, 1) / TUInteractions::Sigpp(etotn_thresh, 1);
   else
      return 1.;
}