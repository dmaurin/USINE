// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

// C/C++ include
#include <vector>
using namespace std;
// ROOT include
// DAVID ROOT
#include "TLegend.h"
#include "TLegendEntry.h"
#include "TMultiGraph.h"
// USINE include
#include "../include/TUSrcMultiCRs.h"
#include "../include/TUSrcPointLike.h"
#include "../include/TUSrcSteadyState.h"

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TUSrcMultiCRs                                                        //
//                                                                      //
// Single source (point-like or steady-state) for any CR list.          //
//                                                                      //
// The class TUSrcMultiCRs provides a complete description of a CR      //
// source, i.e., it describes a single source of several species.       //
// Subtleties arise because of the different kind of sources the class  //
// can handle:                                                          //
//   - point-like source: the source has a single (time-dependent)
//     position, and a spectrum is provided for each CR of this source;
//   - steady-state source: as for a point-like source, it has a
//     spectrum for each CR, but now it is described by a spatial
//     distribution (different spatial distributions for different
//     CR species are enabled).
//                                                                      //
// We then have two families of sources:                                //
//   - ASTROPHYSICAL sources: depending on the model used - steady-state
//     or time-dependent -, we have to provide steady-state or point-like
//     sources. For steady-state models, this amounts to an 'average'
//     description of the sources: due to metallicity gradients, e.g.,
//     in the Galaxy (the element abundances are not constant throughout
//     the Galaxy), we need to enable different spatial distributions
//     for different elements.
//   - DARK MATTER sources: steady-state sources are usually appropriate
//     for DM-induced CRs, unless one consider point-like DM sources
//     (e.g., small DM clumps). Regardless of this, considering a DM
//     source make sense only for a small list of CR species (positrons,
//     anti-protons, anti-deuterons, gamma-rays, and neutrinos).
//                                                                      //
// Handling point-like and steady-state sources in the  same class is   //
// possible thanks to the abstract class TUSrcVirtual. The tedious part //
// is to define how to format the USINE initialisation file to fill the //
// class for a list of CRs, according to the different templates and    //
// families of sources we wish to enable. This is described below.      //
//                                                                      //
// * Class members                                                      //
//  The most important members of the class are the following:          //
//   - fSpecies: a list of species present in this source;
//   - fTemplates: a list of source templates to pick from;
//   - fSrcPerSpecies[NSpecies]: source description for each CR in list;
//   - fFreePars: list of all free parameters for this source.
//                                                                      //
// * How does this work?                                                //
//  The class is filled using the method SetClass(), whose parameters   //
//  are an initialisation file/class (TUInitParList). The method is     //
//  generic enough so that it recognises, depending on the name of the  //
//  initialisation parameters, an ASTRO or DM sources for a point-like  //
//  or steady-state sources (they obviously require a different number  //
//  of parameters to be filled). Note that free parameters for CRs are  //
//  enabled using keywords SHARED (single parameter for all species),   //
//  PERCR (one parameter per CR), PERZ (one parameter per element), or  //
//  LIST (several parameters associated to list of CRs). The selection  //
//  is set in the initialisation file, and all free parameters of the   //
//  class are collected to be accessed from the same object.            //
//                                                                      //
// * Accessing the source for a given species in the source             //
//  After everything is filled, the methods that you need to use are    //
//    - GetSrc(j_cr): returns a TUSrcVirtual object, i.e. the source
//      description for this CR species;
//    - GetSrcSpatialDist(j_cr) and GetSrcSpectrum(j_cr): returns a
//      TUValsTXYZEVirtual object, i.e., the formula or values (on
//      a grid) to describe the spatial distribution or the spectrum
//      of this CR species in this source.
//  Otherwise, ValueSrcPosition(), ValueSrcSpatialDistrib(), and        //
//  ValueSrcSpectrum() allow to get the src position, spatial distrib., //
//  and spectrum value, for a given species and energy/space/time       //
//  coordinate.                                                         //
//                                                                      //
// Free parameters and formulae:                                        //
// -+-+-+-+-+-+-+-+-+-+-+-+-+-
// Some class members may be described by formulae (which depend on     //
// free parameter values, see TUValsTXYZVirtual). Whenever a parameter  //
// value is changed, it has to be propagated to the formula. There are  //
// two ways to achieve that:
//    A. you can decide not to care about it: the class TUValsTXYZEFormula
//      will do the job automatically for you, whenever any method ValueXXX()
//      of this class is called. Indeed, the latter will invariably end up
//      calling TUValsTXYZEFormula::EvalFormula() or the like of it, and
//      then TUValsTXYZEFormula::UpdateFromFreeParsAndResetStatus()
//     to do the job.
//    B. call UpdateFromFreeParsAndResetStatus() to update all formula found
//      in this class (and reset TUFreeParList status to ' updated'.
//                                                                      //
//////////////////////////////////////////////////////////////////////////

ClassImp(TUSrcMultiCRs)

//______________________________________________________________________________
TUSrcMultiCRs::TUSrcMultiCRs()
{
   // ****** Default constructor ******

   Initialise(false);
}

//______________________________________________________________________________
TUSrcMultiCRs::TUSrcMultiCRs(TUSrcMultiCRs const &src_multicrs)
{
   // ****** Copy constructor ******
   //  src_multicrs      Object to copy from

   Initialise(false);
   Copy(src_multicrs);
}

//______________________________________________________________________________
TUSrcMultiCRs::~TUSrcMultiCRs()
{
   // ****** Default destructor ******

   Initialise(true);
}

//______________________________________________________________________________
void TUSrcMultiCRs::AddPlotRatioSrcToSSAbund(TMultiGraph *mg, TLegend *leg,
      string norm_elem, string const &model_name, Bool_t is_ssfip_or_tc) const
{
   //--- If steady-state standard source, adds CR src abundance & SSAbund*FIP plots.
   //  mg                       Multigraph in which to store plots
   //  leg                      Legend to add
   //  norm_elem                ELement to use as normalisation (if not found, set one)
   //  model_name               Extra-name extension (if needed) for plots
   //  is_ssfip_or_tc           Whether to add plot for SS_vs_FIP or SS_vs_Tc

   // If not steady-state and not astro, nothing to return!
   if (!(IsAstroOrDM() && IsSteadyStateOrPointLike()))
      return;

   if (!mg)
      TUMessages::Error(stdout, "TUSrcMultiCRs", "AddPlotRatioSrcToSSAbund",
                        "MultiGraph must exist prior to calling this function");


   // We try to normalize to H in list of CRs in sources
   fSpecies->CheckAndUpdateNormElement(norm_elem);
   Int_t z_norm = fSpecies->ElementNameToZ(norm_elem, false);

   string y_title = "GCR sources/Solar  [" + norm_elem + "#equiv 1]";
   string x_title = "First Ionization Potential  [eV]";
   if (!is_ssfip_or_tc)
      x_title = "GCR/[SS*FIP] abundances;Condensation Temperature (Tc 50%)  [K];";
   string mg_titles = "GCR/[SS*FIP] abundances;" + x_title + ";" + y_title;
   mg->SetTitle(mg_titles.c_str());
   if (leg)
      leg->AddEntry((TObject *)0, model_name.c_str(), "");


   // FIP bias graph (normed to value for z_norm element)
   if (is_ssfip_or_tc) {
      TUAxis fip_axis(2., 31., 20, "FIP", "eV", kLIN);
      TGraph *gr_fip = new TGraph();
      gr_fip->SetName("fip_bias");
      gr_fip->SetTitle("FIP bias");
      gr_fip->GetXaxis()->SetTitle(x_title.c_str());
      gr_fip->GetYaxis()->SetTitle(y_title.c_str());
      Double_t norm_fip = fCRs->FIPBias(z_norm);
      for (Int_t i = 0; i < fip_axis.GetN(); ++i)
         gr_fip->SetPoint(i, fip_axis.GetVal(i), fCRs->FIPBias(fip_axis.GetVal(i)) / norm_fip);
      mg->Add(gr_fip, "L");
      if (leg)
         leg->AddEntry(gr_fip, "FIP bias", "L");
   }

   // SS to source abundance q (normed to value for z_norm element)
   Double_t norm_q = fCRs->SSElementAbundance(z_norm) / GetNormSrcSpectrum_Element(z_norm, false);
   vector<Int_t> list_z = GetCRs()->ExtractAllZ(true);
   for (Int_t i = 0; i < (Int_t)list_z.size(); ++i) {
      Int_t z = list_z[i];
      string element_name = fCRs->ZToElementName(z);
      if (!fSpecies->IsElementAndInList(element_name))
         continue;
      TGraph *gr = NULL;
      string name = "";
      if (is_ssfip_or_tc) {
         Double_t fip = fCRs->GetFIP(z);
         Double_t ratio = 0.;
         if (fCRs->SSElementAbundance(z) > 1.e-80 && GetNormSrcSpectrum_Element(z, false) > 1.e-80)
            ratio = GetNormSrcSpectrum_Element(z, false) / fCRs->SSElementAbundance(z) * norm_q;
         gr = new TGraph(1, &fip, &ratio);
         name = model_name + "_" + fSrcName + "_SSvsFIP_" + element_name;
      } else {
         Double_t tc = fCRs->GetTcVolatility(z);
         Double_t ratio = 0.;
         if (fCRs->SSElementAbundance(z) > 1.e-80 && GetNormSrcSpectrum_Element(z, false) > 1.e-80)
            ratio = GetNormSrcSpectrum_Element(z, false)
                    / fCRs->SSElementAbundance(z) * norm_q;
         gr = new TGraph(1, &tc, &ratio);
         name = model_name + "_" + fSrcName + "_SSvsTc_" + element_name;
      }
      TUMisc::RemoveSpecialChars(name);
      gr->SetTitle("GCR/[SS*FIP] abundances");
      gr->SetName(name.c_str());
      gr->GetXaxis()->SetTitle(x_title.c_str());
      gr->GetYaxis()->SetTitle(y_title.c_str());
      gr->SetMarkerColor(TUMisc::RootColor(i));
      gr->SetMarkerStyle(TUMisc::RootMarker(i));
      gr->SetMarkerSize(0.7);
      mg->Add(gr, "P");
      if (leg) {
         TLegendEntry *entry = leg->AddEntry(gr, element_name.c_str(), "P");
         entry->SetTextColor(TUMisc::RootColor(i));
      }
   }
}

//______________________________________________________________________________
void TUSrcMultiCRs::AllocateAndFillSrcFromTemplate(string templ_name, string user_pars,
      Bool_t is_verbose, FILE *f_log, Bool_t is_spectrum_or_spatdist, string name_spectrum_normparam)
{
   //--- Fills source spectrum or spatial distribution for all species from user-parameters.
   //  templ_name               User-selected template name (search among those in fTemplate)
   //  user_pars                User-selected parameters for template (e.g., 'q[PERCR:DEFAULT=1.e-5];alpha[SHARED:2.26];eta_s[SHARED:-1.]')
   //  is_verbose               Chatter on or off
   //  f_log                    Log for USINE
   //  is_spectrum_or_spatdist  Whether we fill spectrum or spat. distrib.
   //  name_spectrum_normparam  Name of param used as norm in template (if no norm param, nothing to do)

   // Temporary storage variables (for matching template)
   Int_t i_templ = fTemplates->IndexSrcTemplate(templ_name, is_spectrum_or_spatdist);
   if (is_spectrum_or_spatdist)
      fIndexTemplSpectrum = i_templ;
   else
      fIndexTemplSpatialDist = i_templ;

   TUValsTXYZEVirtual *src_templ = fTemplates->GetSrc(i_templ);
   TUFreeParList *pars_template = fTemplates->GetFreePars(i_templ);
   Int_t i_norm_par = -1;
   if (name_spectrum_normparam != "-" && name_spectrum_normparam != "") {
      i_norm_par = -1;
      if (pars_template)
         i_norm_par = pars_template->IndexPar(name_spectrum_normparam);

      if (i_norm_par < 0) {
         string message = "For " + fSrcName + ", parameter " + name_spectrum_normparam
                          + " (in " + user_pars + ")"
                          + " not found in template parameters for " + templ_name;
         TUMessages::Error(f_log, "TUSrcMultiCRs", "AllocateAndFillSrcFromTemplate", message);
      }
   }

   //////////////////////////
   // FILL FOR GRID VALUES //
   //////////////////////////
   // Fill for grid of values (or formula with no parameters)
   if (src_templ->ValsTXYZEFormat() == 1
         || !pars_template || pars_template->GetNPars() == 0) {

      // N.B.: If grid values, no free parameters: all species
      // set to the same template!

      // For first species, create it!
      fSrcPerSpecies[0]->FillSpectrumOrSpatialDistFromTemplate(fTemplates, templ_name, is_spectrum_or_spatdist, f_log);

      // For all other ones, use the one just created (to spare memory and avoid duplicates)
      TUValsTXYZEVirtual *ref = NULL;
      if (is_spectrum_or_spatdist)
         ref = fSrcPerSpecies[0]->GetSrcSpectrum();
      else
         ref = fSrcPerSpecies[0]->GetSrcSpatialDist();

      for (Int_t i = 1; i < fSpecies->GetNCRs(); ++i)
         fSrcPerSpecies[i]->UseSpectrumOrSpatDist(ref, is_spectrum_or_spatdist);

      ref = NULL;
      src_templ = NULL;
      pars_template = NULL;
      return;
   } else if (src_templ->ValsTXYZEFormat() == 0) {
      ///////////////////////////////////////////
      // FILL FOR FORMULA WITH FREE PARAMETERS //
      ///////////////////////////////////////////
      // This is much more complicated than for a file! We follow the sequence:
      //  A. Extract user free parameters (can be universal or as many
      //     as species considered).
      //  B. Create and fill class member fFreePars: at most, there are
      //     NSpecies*NTemplatePars, but a complication is that one can
      //     wish some of the parameters to be universal (i.e., the same
      //     for all species).
      //  C. Fill formula (from template and correct associated set of
      //     parameters from fFreePars) for each CRs. Unless all parameters
      //     are set to SHARED, we must create one formula per species.


      // A. Extract parameters and values + check they are as numerous as in TEMPLATE
      //-----------------------------------------------------------------------------
      // N.B.: format of passed parameters is 'par1[XXX];par2[XXX];par3[XXX]'
      // where semi-colon-separated parameters are set for all CRs with:
      //   - a single parameter for all CRs: '[XXX]=[SHARED:val]'
      //   - one parameter per CR species:   '[XXX]=[PERCR:DEFAULT=val0,CR1=val1,CR2=val2...]'
      //   - one parameter per element:      '[XXX]=[PERZ:DEFAULT=val0,Z1=val1,Z2=val2...]'
      //   - parameters for lists of CRs/Z:  '[XXX]=[LIST:DEFAULT=val0,1H=val1;2H_He_C_N_O=val2;]'

      vector<Double_t> defval_per_par; // default value per parameter
      vector<Int_t> i_templ2par; // map index in template from index in user-par
      vector<string> keys_per_par; // Vector of keys (SHARED, PERCR, PERZ, or LIST) per parameter
      vector<vector<string> > qties_per_par; // Quantities per parameter and per list, whose associated parameter has a specific value
      vector<vector<vector<Int_t> > > i_per_par_and_qties; // Indices of CRs per parameter and list (if list contains element, requires several indices)
      vector<vector<Double_t> > valallspecies_per_par; // Value of parameter for all species

      vector<string> params_uservals;  // Vector of par_i[XXX], i={1...Nuser-pars}
      vector<string> pars_found;
      TUMisc::String2List(user_pars, ";", params_uservals);
      //cout << "**** " << user_pars << endl;
      for (Int_t i = 0; i < (Int_t) params_uservals.size(); ++i) {
         // Extract parameter values
         //cout << "    " << i << " " << params_uservals[i] << endl;
         vector<string> par_vals;
         TUMisc::String2List(params_uservals[i], "[]", par_vals);
         pars_found.push_back(par_vals[0]);
         //cout << "        -> " << par_vals[0] << endl;
         // Search index of corresponding parameter in template
         Int_t i_par = pars_template->IndexPar(par_vals[0]);
         if (i_par < 0) {
            string message = "For " + fSrcName + ", parameter "
                             + par_vals[0] + " (in " + user_pars + ")"
                             + " is not a parameter for template " + templ_name;
            TUMessages::Error(f_log, "TUSrcMultiCRs", "AllocateAndFillSrcFromTemplate", message);
         }
         // Check that same parameter not asked for twice!
         for (Int_t j = 0; j < (Int_t)i_templ2par.size(); ++j) {
            if (i_par == i_templ2par[j]) {
               string message = "For " + fSrcName + ", parameter "
                                + par_vals[0] + " (in " + user_pars + ")" + " is used twice!!!!";
               TUMessages::Error(f_log, "TUSrcMultiCRs", "AllocateAndFillSrcFromTemplate", message);
            }
         }
         i_templ2par.push_back(i_par);
         //cout << "        -> i_par = " << i_par << endl;
         //pars_template->GetParEntry(i_par)->Print();
         // Check that valid keys are used!
         if (par_vals[1].substr(0, 7) != "SHARED:" && par_vals[1].substr(0, 14) != "PERCR:DEFAULT="
               && par_vals[1].substr(0, 13) != "PERZ:DEFAULT=" && par_vals[1].substr(0, 13) != "LIST:DEFAULT=") {
            string message = (string) "Format of argument should be \'SHARED:val\', \'PERCR:DEFAULT=val0,qty1=val1,qty2=val2...\', "
                             + (string) "\'PERZ:DEFAULT=val0,qty1=val1,qty2=val2...\', or \'LIST:DEFAULT=val0,qty1_qty2_...:val1,qtyi_qtyj...:val2...\', "
                             + "whereas user value is: " + par_vals[1];
            TUMessages::Error(f_log, "TUSrcMultiCRs", "AllocateAndFillSrcFromTemplate", message);
         }

         // If we reach here, means that key is either 'shared', PERCR', 'PERZ', or 'LIST'
         // Extract parameters and initialisation value
         //  - a universal value for all CRs:  '[XXX]=[SHARED:val1]'
         //  - one parameter per CR species:   '[XXX]=[PERCR:DEFAULT=val0,CR11=val1,CR2=val2...]'
         //  - one parameter per element:      '[XXX]=[PERZ:DEFAULT=val0,Z1=val1,Z2=val2...]'
         //  - one parameter per list of CRs/Z '[XXX]=[LIST:DEFAULT=val0,qty1_qty2_...:val1,qtyi_qtyj...:val2...]

         // Extract key
         TUMisc::UpperCase(par_vals[1]);
         size_t found = par_vals[1].find_first_of(":");
         string key = par_vals[1].substr(0, found);
         //cout << "        -> key = " << key << endl;

         // We need to store information for each quantity defined by the user for this parameter
         vector<string> qties;
         Double_t default_val = 0.;
         vector<vector<Int_t> > i_species4qties; // We have different indices for each qty in 'qties'
         vector<Int_t> i_all4check; // Store all indices found to check no duplicate!
         vector<Double_t> valallspecies(fSpecies->GetNCRs(), 0.); // Value for all species

         if (key == "SHARED") {
            default_val = atof(par_vals[1].substr(7).c_str());
            defval_per_par.push_back(default_val);
            //cout << "            => default = " << default_val << endl;
         } else if (key != "PERCR" && par_vals[0] == GetNameSrcNormPar()) {
            // key 'LIST' not allowed for normalisation parameter
            string message = "The source normalisation parameter \'" + GetNameSrcNormPar()
                             + "\' cannot only be associated with keyword \'PERCR\': " + par_vals[0] + " is not valid!";
            TUMessages::Error(f_log, "TUSrcMultiCRs", "AllocateAndFillSrcFromTemplate", message);
         } else {
            // Extract quantities and values (comma-separated) for the following cases
            //   PERCR (one value per species): DEFAULT value for all except specific species, e.g. '[PERCR:DEFAULT=2,1H=2.5,4He=2.4]')
            //   PERZ (one value per Z): DEFAULT value for all except specific elements, e.g. '[PERZ:DEFAULT=2,H=2.5,He=2.4]')
            //   LIST (one value per list): DEFAULT value for all except specific list, e.g. '[LIST:DEFAULT=2,1H=2.5,2H_He_C_N=2.4]')
            vector<string> qties_vals;
            TUMisc::String2List(par_vals[1].substr(found + 1), ",", qties_vals);
            string pre_message = "For " + fSrcName + ", parameter " + par_vals[0] + " (in " + user_pars + "), ";
            // Extract qty from values in each qties_vals (syntax is qty=val)
            Int_t n_def = 0;
            //cout << par_vals[1] << endl;
            for (Int_t k = 0; k < (Int_t)qties_vals.size(); ++k) {
               //cout << "              k=" << k << "  " << qties_vals[k] << endl;
               vector<string> qty_val;
               TUMisc::String2List(qties_vals[k], "=", qty_val);
               // Check format
               if (qty_val.size() != 2) {
                  string message = pre_message + qties_vals[k] + " is badly formatted (should be DEFAULT=val or QTY=val)";
                  TUMessages::Error(f_log, "TUSrcMultiCRs", "AllocateAndFillSrcFromTemplate", message);
               }
               //cout << "                 -> " << qty_val[0] << "  " << qty_val[1] << endl;
               Double_t user_val = atof(qty_val[1].c_str());
               //cout << "                   user_val=" << user_val << endl;
               if (qty_val[0] == "DEFAULT") {
                  defval_per_par.push_back(user_val);
                  // Set all parameter values to default to start with
                  for (Int_t s = 0; s < fSpecies->GetNCRs(); ++s)
                     valallspecies[s] = user_val;
                  // Check no duplicate for DEFAULT
                  n_def += 1;
                  if (n_def > 1) {
                     string message = pre_message + qties_vals[k] + " contains two \'DEFAULT\' whereas only once allowed";
                     TUMessages::Error(f_log, "TUSrcMultiCRs", "AllocateAndFillSrcFromTemplate", message);
                  }
                  continue;
               }
               // If we reach this point, we have the qty (CR, Z or list) and its associated value,
               // but we also have to extract the corresponding CR indices in the source (fSpecies)

               // Extract corresponding indices
               vector<Int_t> indices;
               if (key == "PERCR") {
                  Int_t index = fSpecies->Index(qty_val[0], true, f_log, false);
                  if (index >= 0) {
                     indices.push_back(index);
                     valallspecies[index] = user_val;
                     // Check no duplicate for index
                     if (TUMisc::IsInList(i_all4check, index)) {
                        string message = pre_message + fSpecies->GetCREntry(index).GetName()
                                         + " cannot appear twice in " + qty_val[0];
                        TUMessages::Error(f_log, "TUSrcMultiCRs", "AllocateAndFillSrcFromTemplate", message);
                     } else {
                        //cout << " add (from " << qty_val[0] << " in PERCR) " << fSpecies->GetCREntry(index).GetName() << endl;
                        i_all4check.push_back(index);
                     }
                  }
                  //cout << "                 -> i_cr=" << index << "  " << fSpecies->GetCREntry(index).GetName() << endl;
               } else if (key == "PERZ") {
                  vector<Int_t> i_elements;
                  fSpecies->ElementNameToCRIndices(qty_val[0], i_elements, /*is_verbose*/false, f_log);
                  // Check no duplicate for all CR i_elements in element
                  for (Int_t ii = 0; ii < (Int_t)i_elements.size(); ++ii) {
                     indices.push_back(i_elements[ii]);
                     valallspecies[i_elements[ii]] = user_val;
                     //cout << "                 -> i_crs=" << i_elements[ii] << "  " << fSpecies->GetCREntry(i_elements[ii]).GetName() << endl;
                     if (TUMisc::IsInList(i_all4check, i_elements[ii])) {
                        string message = pre_message + fSpecies->GetCREntry(i_elements[ii]).GetName();
                        if (i_elements.size() > 0)
                           message += " (in " + qty_val[0] + ")";
                        message += " cannot appear twice!";
                        TUMessages::Error(f_log, "TUSrcMultiCRs", "AllocateAndFillSrcFromTemplate", message);
                     } else {
                        //cout << " add (from " << qty_val[0] << " in PERZ) " << fSpecies->GetCREntry(i_elements[ii]).GetName() << endl;
                        i_all4check.push_back(i_elements[ii]);
                     }
                  }
               } else if (key == "LIST") {
                  // Extract quantities in list (underscore-separated)
                  vector<string> list;
                  TUMisc::String2List(qty_val[0], "_", list);
                  for (Int_t l = 0; l < (Int_t)list.size(); ++l) {
                     // Qty is an element, a species, or none of the above!
                     if (fSpecies->IsElementAndInList(list[l])) {
                        // Element
                        vector<Int_t> i_elements;
                        fSpecies->ElementNameToCRIndices(list[l], i_elements, /*is_verbose*/false, f_log);
                        // Check no duplicate for all CR indices in element
                        for (Int_t ii = 0; ii < (Int_t)i_elements.size(); ++ii) {
                           indices.push_back(i_elements[ii]);
                           valallspecies[i_elements[ii]] = user_val;
                           if (TUMisc::IsInList(i_all4check, i_elements[ii])) {
                              string message = pre_message + fSpecies->GetCREntry(i_elements[ii]).GetName();
                              //cout << "                 -> i_crs=" << i_elements[ii] << "  " << fSpecies->GetCREntry(indices[ii]).GetName() << endl;
                              if (i_elements.size() > 0)
                                 message += " (in " + qty_val[0] + ")";
                              message += " cannot appear twice!";
                              TUMessages::Error(f_log, "TUSrcMultiCRs", "AllocateAndFillSrcFromTemplate", message);
                           } else {
                              //cout << " add (from " << list[l] << " in LIST) " << fSpecies->GetCREntry(i_elements[ii]).GetName() << endl;
                              i_all4check.push_back(i_elements[ii]);
                           }
                        }
                     } else {
                        Int_t index = fSpecies-> Index(list[l], false);
                        if (index >= 0) {
                           //cout << "                 -> i_cr=" << index << "  " << fSpecies->GetCREntry(index).GetName() << endl;
                           // CR species
                           indices.push_back(index);
                           valallspecies[index] = user_val;
                           // Check no duplicate for index
                           if (TUMisc::IsInList(i_all4check, index)) {
                              string message = pre_message + fSpecies->GetCREntry(index).GetName()
                                               + " cannot appear twice in " + qty_val[0];
                              TUMessages::Error(f_log, "TUSrcMultiCRs", "AllocateAndFillSrcFromTemplate", message);
                           } else {
                              //cout << " add (from " << list[l] << " in LIST) " << fSpecies->GetCREntry(index).GetName() << endl;
                              i_all4check.push_back(index);
                           }
                        } else {
                           // If neither an element nor a CR in source list, error!
                           string message = pre_message + list[l] + " neither an element or species present in the source, skip it";
                           TUMessages::Warning(f_log, "TUSrcMultiCRs", "AllocateAndFillSrcFromTemplate", message);
                        }
                     }
                  }
               }
               // Add all indices found for this parameter
               qties.push_back(qty_val[0]);
               i_species4qties.push_back(indices);
            }

            // We must add one last quantity  whose name is 'DEFAULT' (only for 'LIST')
            // which contains all remaining CR species and their default value
            if (key == "LIST") {
               vector<Int_t> i_allothers;
               for (Int_t j = 0; j < fSpecies->GetNCRs(); ++j) {
                  Int_t index = TUMisc::IndexInList(i_all4check, j);
                  if (index < 0)
                     i_allothers.push_back(j);
               }
               // Add all i_allothers found for this parameter
               qties.push_back("DEFAULT");
               i_species4qties.push_back(i_allothers);
            }
         }
         // Add all user-extracted quantities found in list per parameter
         keys_per_par.push_back(key);
         qties_per_par.push_back(qties);
         i_per_par_and_qties.push_back(i_species4qties);
         valallspecies_per_par.push_back(valallspecies);
      }
      // Check that as many user-parameters than the number expected in Template
      if (pars_template && ((Int_t)i_templ2par.size() != pars_template->GetNPars())) {
         string message = "For " + fSrcName + "(in " + user_pars + ")" + " we have " + TUMisc::List2String(pars_found, ',')
                          + " while " + pars_template->GetParNames() + " expected in template " + templ_name;
         TUMessages::Error(f_log, "TUSrcMultiCRs", "AllocateAndFillSrcFromTemplate", message);
      }

      //// Check print
      //cout << "****** CHECK VECTORS ******" << endl;
      //// Loop on parameters
      //for (Int_t p=0; p<(Int_t)keys_per_par.size(); ++p) {
      //   cout << "  - p=" << p << endl;
      //   cout << "    key=" << keys_per_par[p] << endl;
      //   cout << "    default=" << defval_per_par[p] << endl;
      //   // Loop on quantities
      //   for (Int_t q=0; q<(Int_t)qties_per_par[p].size(); ++q) {
      //      cout << "     - q=" << q << endl;
      //      cout << "       qty=" << qties_per_par[p][q] << endl;
      //      // Loop on found CR species
      //      for (Int_t s=0; s<(Int_t)i_per_par_and_qties[p][q].size(); ++s) {
      //         cout << "        - s=" << q << endl;
      //         cout << "          index=" << i_per_par_and_qties[p][q][s] << endl;
      //      }
      //   }
      //}
      //cout << "****** CHECK VALUES ******" << endl;
      //for (Int_t p=0; p<(Int_t)keys_per_par.size(); ++p) {
      //   cout << "  - p=" << p << "  -> " << pars_template->GetParEntry(i_templ2par[p])->GetName() << endl;
      //   cout << "    key=" << keys_per_par[p] << endl;
      //   cout << "    default=" << defval_per_par[p] << endl;
      //   for (Int_t j = 0; j < fSpecies->GetNCRs(); ++j) {
      //      cout << "     - i_species=" << j <<  " ->" << fSpecies->GetCREntry(j).GetName() << endl;
      //      cout << "       val=" << valallspecies_per_par[p][j] << endl;
      //   }
      //}
      //cout << "******   END CHECK   ******" << endl;

      // B. Create all free parameters and set value for this src
      //------------------------------ --------------------------
      // As many parameters created as species (unless SHARED). From the above
      // sorting, we have
      //   - keys_per_par[npars]                      => key used (SHARED, PERCR, PERZ, or LIST) for each source parameter
      //   - qties_per_par[npars][n_qties]            => for each parameter, quantities that have a specific value (last entry is 'ALL_OTHERS' and contains default value)
      //   - defval_per_par[npars]                    => default value of parameter for all quantities not specified
      //   - i_per_par_and_qties[n_pars][n_qties][n]  => indices of CR species corresponding to each of these quantities
      //   - valallspecies_per_par[n_pars][n_species] => values of parameter associated to each CR species
      //   - i_templ2par[npars]                       => map index in template from index in user-par

      // If previously not created, do it!
      if (!fFreePars) fFreePars = new TUFreeParList();
      vector<vector<Int_t> > i_in_freepars; // For each CR in fSpecies, corresponding index in free parameter
      Int_t n_pars_tmpl = 0;
      if (pars_template)
         n_pars_tmpl = pars_template->GetNPars();

      // Loop on template parameters
      for (Int_t k = 0; k < n_pars_tmpl; ++k) {
         vector<Int_t> indices(fSpecies->GetNCRs(), -1);
         Int_t i_par = i_templ2par[k];
         // If key==SHARED, create a unique parameter
         if (keys_per_par[i_par] == "SHARED") {
            fFreePars->AddPar(pars_template->GetParEntry(k)->GetName(), pars_template->GetParEntry(k)->GetUnit(false), defval_per_par[i_par]);
            indices[0] = fFreePars->GetNPars() - 1;
         } else if (keys_per_par[i_par] == "PERCR") {
            // Loop on CR species
            for (Int_t i = 0; i < GetNSpecies(); ++i) {
               string new_parname = pars_template->GetParEntry(k)->GetName() + "_" + TUMisc::RemoveChars(fSpecies->GetCREntry(i).GetName(), "-");
               fFreePars->AddPar(new_parname, pars_template->GetParEntry(k)->GetUnit(false), valallspecies_per_par[i_par][i]);
               indices[i] = fFreePars->GetNPars() - 1;
               // If index of template corresponds to spectrum normalisation index,
               // update in list of indices fIndexSrcNormInFreePar for this species
               if (k == i_norm_par) {
                  fIndexSrcNormInFreePar[i]  = fFreePars->GetNPars() - 1;
                  //cout << "   i_par=" << fFreePars->GetNPars() - 1  << " "
                  //     << fFreePars->GetParEntry(fFreePars-->GetName()>GetNPars() - 1) << endl;
                  //cout << "  k=" << k << "  i_norm_par=" << i_norm_par << " "
                  //     << fFreePars->GetNPars() - 1 << "  "
                  //     << pars_template->GetParEntry(k)->GetName() << endl;
               }
            }
         } else if (keys_per_par[i_par] == "PERZ") {
            // Extract all Z
            vector<string> elements = fSpecies->ExtractAllElements();
            // Loop on elements
            for (Int_t i = 0; i < (Int_t)elements.size(); ++i) {
               string new_parname = pars_template->GetParEntry(k)->GetName() + "_"
                                    + TUMisc::RemoveChars(elements[i], "-");
               // Use index of a species (which makes this element) to get its value
               vector<Int_t> i_tmp;
               fSpecies->ElementNameToCRIndices(elements[i], i_tmp, /*is_verbose*/false, f_log);
               fFreePars->AddPar(new_parname, pars_template->GetParEntry(k)->GetUnit(false), valallspecies_per_par[i_par][i_tmp[0]]);
               for (Int_t jj = 0; jj < (Int_t)i_tmp.size(); ++jj)
                  indices[i_tmp[jj]] = fFreePars->GetNPars() - 1;
            }
         } else if (keys_per_par[i_par] == "LIST") {
            // Loop on all quantities to create associated parameter
            for (Int_t i_qty = 0; i_qty < (Int_t)qties_per_par[i_par].size(); ++i_qty) {
               string new_parname = pars_template->GetParEntry(k)->GetName() + "_"
                                    + TUMisc::RemoveChars(qties_per_par[i_par][i_qty], "-");
               // Use index of a species (which makes this qty) to get its value
               Int_t i_tmp = i_per_par_and_qties[i_par][i_qty][0];
               fFreePars->AddPar(new_parname, pars_template->GetParEntry(k)->GetUnit(false),  valallspecies_per_par[i_par][i_tmp]);
               indices.push_back(fFreePars->GetNPars() - 1);
               for (Int_t jj = 0; jj < (Int_t)i_per_par_and_qties[i_par][i_qty].size(); ++jj)
                  indices[i_per_par_and_qties[i_par][i_qty][jj]] = fFreePars->GetNPars() - 1;
            }
         }
         i_in_freepars.push_back(indices);
      }

      // C. Clone template for each species
      //-----------------------------------
      // N.B.: this must be done for the appropriate free parameters, i.e.
      // parameters pointing to those created above! We thus must loop on
      // all species, and clone spectrum/spat.dist. from template, changing
      // its parameter name. We use below:
      //    - src_templ => TUValsTXYZEVirtual pointing to used template
      //    - par_template => parameter names in src_templ
      //    - fFreePars    => available free pars

      // Loop on all species
      for (Int_t i = 0; i < GetNSpecies(); ++i) {

         // Get formula and variables from template
         string formula_string = src_templ->GetFormulaString();
         string formula_vars = src_templ->GetFormulaVars();


         // Set free pars
         //   1. Search for non-universal user-parameters and
         //      form new TUFreeParList (pointing to params in fFreePars)
         //   2. Change in formula parameter names which are not universal
         //   3. Create formula using this free par list for this species
         //
         fFreeParsPerSpecies[i] = new TUFreeParList();
         // Loop on template parameters.
         for (Int_t k = 0; k < n_pars_tmpl; ++k) {
            Int_t i_par = i_templ2par[k];

            // If SHARED, generic par is used
            if (keys_per_par[i_par] == "SHARED")
               fFreeParsPerSpecies[i]->UseParEntry(fFreePars->GetParEntry(i_in_freepars[k][0]));
            else {
               fFreeParsPerSpecies[i]->UseParEntry(fFreePars->GetParEntry(i_in_freepars[k][i]));
               string current_par = pars_template->GetParEntry(k)->GetName();
               size_t pos = formula_string.find(current_par);
               string par_ext = "";
               if (keys_per_par[i_par] == "PERCR")
                  par_ext = fSpecies->GetCREntry(i).GetName();
               else if (keys_per_par[i_par] == "PERZ")
                  par_ext = fSpecies->CRToElementName(i);
               else if (keys_per_par[i_par] == "LIST") {
                  // Search i_qty to which species belong (index i)
                  for (Int_t i_qty = 0; i_qty < (Int_t)qties_per_par[i_par].size(); ++ i_qty) {
                     if (TUMisc::IsInList(i_per_par_and_qties[i_par][i_qty], i)) {
                        par_ext = TUMisc::RemoveChars(qties_per_par[i_par][i_qty], "-");
                        break;
                     }
                  }
               }
               formula_string.insert(pos + current_par.size(), ("_" + TUMisc::RemoveChars(par_ext, "-")));
               //cout << par_ext << endl;
            }
         }
         // Now that we have free parameters and formula, create it
         fSrcPerSpecies[i]->FillSpectrumOrSpatialDist(formula_string, is_verbose, f_log, formula_vars,
               fFreeParsPerSpecies[i], true /*is_use_or_copy_freepars*/, is_spectrum_or_spatdist);

      }
      src_templ = NULL;
      pars_template = NULL;
   } else if (src_templ->ValsTXYZEFormat() == 2) {
      ///////////////////////////////////////////
      // SPLINE TODO //
      ///////////////////////////////////////////
      TUMessages::Error(f_log, "TUSrcMultiCRs", "AllocateAndFillSrcFromTemplate", "TBD");
   } else {
      TUMessages::Error(f_log, "TUSrcMultiCRs", "AllocateAndFillSrcFromTemplate",
                        "Option not 0, 1, 2 does not exist for src_templ->ValsTXYZEFormat()");
   }
}

//______________________________________________________________________________
void TUSrcMultiCRs::Copy(TUSrcMultiCRs const &src_multicrs)
{
   //--- Copies src_multicrs in current class.
   //  src_multicrs      Object to copy from

   // Initialise
   Initialise(true);


   fIsDeleteCRs = src_multicrs.IsDeleteCRs();
   if (fIsDeleteCRs) {
      TUCRList *crs = src_multicrs.GetCRs();
      if (crs)
         fCRs = crs->Clone();
      crs = NULL;
   } else
      fCRs = src_multicrs.GetCRs();


   TUCRList *crs = src_multicrs.GetSpecies();
   if (crs)
      fSpecies = crs->Clone();
   crs = NULL;

   // Copy templates
   fIsDeleteTemplates = src_multicrs.IsDeleteTemplates();
   if (fIsDeleteTemplates) {
      TUSrcTemplates *templ = src_multicrs.GetTemplates();
      if (templ)
         fTemplates = templ->Clone();
      templ = NULL;
   } else
      fTemplates = src_multicrs.GetTemplates();

   // Copy free pars
   TUFreeParList *pars = src_multicrs.GetFreePars();
   if (pars)
      fFreePars = pars->Clone();
   pars = NULL;


   // Copy
   fSrcPerSpecies = new TUSrcVirtual*[src_multicrs.GetNSpecies()];
   for (Int_t i = 0; i < src_multicrs.GetNSpecies(); ++i) {
      TUSrcVirtual *src = src_multicrs.GetSrc(i);
      if (src)
         fSrcPerSpecies[i] = src->Clone();
      else
         fSrcPerSpecies[i] = NULL;
      src = NULL;
   }


   // Copy
   fIndexTemplSpatialDist = src_multicrs.IndexTemplSpatialDist();
   fIndexTemplSpectrum = src_multicrs.IndexTemplSpectrum();
   fIsAstroOrDM = src_multicrs.IsAstroOrDM();
   fIsSteadyStateOrPointLike = src_multicrs.IsSteadyStateOrPointLike();
   fSrcName = src_multicrs.GetSrcName();

   // Copy fIndexSrcNormInFreePar
   fIndexSrcNormInFreePar.clear();
   for (Int_t i = 0; i < src_multicrs.GetNSpecies(); ++i) {
      fIndexSrcNormInFreePar.push_back(src_multicrs.IndexSrcNormInFreePar(i));
   }
   // Copy fMapCR2SrcIndices and fMapSrc2CRIndices
   fMapCR2SrcIndices.clear();
   fMapSrc2CRIndices.clear();
   for (Int_t i = 0; i < fCRs->GetNCRs(); ++i)
      fMapCR2SrcIndices.push_back(src_multicrs.IndexCRInSrcSpecies(i));
   for (Int_t i = 0; i < src_multicrs.GetNSpecies(); ++i)
      fMapSrc2CRIndices.push_back(src_multicrs.IndexSrcSpeciesInCRList(i));
}

//______________________________________________________________________________
void TUSrcMultiCRs::ExtractIsotopeswhoseNormIsAFreePar(TUFreeParList const *pars, vector<Int_t> &i_norm_species) const
{
   //--- Extracts indices (in fSpecies) of isotopes which have normalisation as free parameter.
   // INPUT:
   //  pars              Free parameters in which to search for
   // OUTPUT:
   //  i_norm_species    Indices found

   i_norm_species.clear();

   // Loop on free pars to get norm parameters
   string norm = fNameSrcNormPar + "_";
   for (Int_t p = 0; p < pars->GetNPars(); ++p) {
      // Search if parameter is norm
      string par = pars->GetParEntry(p)->GetName();
      std::size_t found = par.find(norm);
      if (found != string::npos) {
         // If found, extract isotope name and add index in fSpecies
         par.erase(found, norm.size());
         Int_t i_species = fSpecies->Index(par, false);
         if (i_species >= 0)
            i_norm_species.push_back(i_species);
      }
   }
}

//______________________________________________________________________________
Double_t TUSrcMultiCRs::GetNormSrcSpectrum_Element(string const &element, bool is_verbose) const
{
   //--- Gets elemental abundance of the src spectrum (sum of all cr nuclei).
   //  element           Element name
   //  is_verbose        Chatter on or off

   // Extract list of CRs for element
   vector<Int_t> cr_indices;
   fSpecies->ElementNameToCRIndices(element, cr_indices, is_verbose);

   // Add abundances
   Double_t norm_element = 0.;
   for (Int_t i = 0; i < (Int_t)cr_indices.size() ; ++i)
      norm_element += GetNormSrcSpectrum(cr_indices[i]);

   return norm_element;
}

//______________________________________________________________________________
void TUSrcMultiCRs::Initialise(Bool_t is_delete, Bool_t all_but_crs)
{
   //--- Initialises class members (and delete if required).
   //  is_delete         Whether to delete or just initialise
   //  all_but_crs       If true, fSpecies is left unchanged

   if (is_delete) {
      if (fFreeParsPerSpecies) {
         for (Int_t i = 0; i < GetNSpecies(); ++i) {
            if (fFreeParsPerSpecies[i])
               delete fFreeParsPerSpecies[i];
            fFreeParsPerSpecies[i] = NULL;
         }
         delete[] fFreeParsPerSpecies;
      }

      if (fFreePars)
         delete fFreePars;

      if (fSrcPerSpecies) {
         for (Int_t j_cr = 0; j_cr < GetNSpecies(); ++j_cr) {
            if (fSrcPerSpecies[j_cr]) delete fSrcPerSpecies[j_cr];
            fSrcPerSpecies[j_cr] = NULL;
         }
         delete[] fSrcPerSpecies;
      }

      if (fIsDeleteTemplates && fTemplates)
         delete fTemplates;

      // In one case, we do not wish to delete fSpecies
      if (!all_but_crs && fSpecies)
         delete fSpecies;

      if (fIsDeleteCRs && fCRs)
         delete fCRs;
   }

   // Initialise
   fCRs = NULL;
   if (!all_but_crs)
      fSpecies = NULL;
   fFreePars = NULL;
   fFreeParsPerSpecies = NULL;
   fIndexSrcNormInFreePar.clear();
   fIndexTemplSpatialDist = -1;
   fIndexTemplSpectrum = -1;
   fIsAstroOrDM = true;
   fIsDeleteCRs = true;
   fIsDeleteTemplates = true;
   fIsSteadyStateOrPointLike = true;
   fMapCR2SrcIndices.clear();
   fMapSrc2CRIndices.clear();
   fNameSrcNormPar = "";
   fSrcName = "";
   fSrcPerSpecies = NULL;
   fTemplates = NULL;
}

//______________________________________________________________________________
void TUSrcMultiCRs::InitialiseSpectrumNorm(vector<gENUM_SRCABUND_INIT> const &src_init, Bool_t is_verbose, FILE *f_log)
{
   //--- Additional scaling of src spectrum normalisations according 'src_init' specifications.
   //    All choices are applied, so it is up to the user to decide what is physical
   //    and less physical.
   //
   //  src_init          Vector of choices (see TUEnum.h) for src abundances initialisation, e.g.
   //                       kSSISOTFRAC: to set relative isotopic abundances of elements to SS isotopic abundances
   //                       kSSISOTABUND: to set isotopic abundances of all CRs to SS isotopic abundances
   //                       kFIPBIAS: to add FIP (first ionization potential bias) to all src abundances
   //  is_verbose        Chatter is on or off
   //  f_log             Log for USINE


   string indent = TUMessages::Indent(true);
   if (is_verbose)
      fprintf(f_log, "%s### Initialise source spectrum normalisation\n", indent.c_str());


   // Set whether various options are set or not
   Bool_t is_fipbias = false;
   Bool_t is_ssisotfrac = false;
   Bool_t is_ssisotabund = false;
   for (Int_t i = 0; i < (Int_t)src_init.size(); ++i) {
      if (src_init[i] == kSSISOTABUND) {
         is_ssisotabund = true;
         if (is_verbose)
            fprintf(f_log, "%s    -> use SS isotopic abundances (%s)\n", indent.c_str(), TUEnum::Enum2Name(kSSISOTABUND).c_str());
      }
      if (src_init[i] == kFIPBIAS) {
         is_fipbias = true;
         if (is_verbose)
            fprintf(f_log, "%s    -> apply FIP bias to all elements (%s)\n", indent.c_str(), TUEnum::Enum2Name(kFIPBIAS).c_str());
      }
      if (src_init[i] == kSSISOTFRAC) {
         is_ssisotfrac = true;
         if (is_verbose)
            fprintf(f_log, "%s    -> set isotopic fraction to SS fraction within element (%s)\n",
                    indent.c_str(), TUEnum::Enum2Name(kSSISOTFRAC).c_str());
      }
   }


   // Loop on charges (may contain anti-elements)
   for (Int_t z = fSpecies->ExtractZMin(); z <= fSpecies->ExtractZMax(); ++z) {
      if (z == 0)
         continue;
      // Check that this element is in list of speices (in src)
      string element = fSpecies->TUAtomElements::ZToElementName(z);
      if (!fSpecies->IsElementAndInList(element))
         continue;

      // z-dependent quantities
      Double_t fip_bias = fCRs->FIPBias(z);
      Double_t norm_z = GetNormSrcSpectrum_Element(z, false);


      // Get list of species for this z
      vector<Int_t> indices_species;
      fSpecies->ElementNameToCRIndices(element, indices_species, false);


      // Search if some CR normalisations are free parameters: if yes, either
      //   - normalisation for all isotopes are free parameters
      //      => nothing to do
      //   - only one isotope per Z is a free parameter
      //      => ensure

      // Loop on each species for this element
      for (Int_t i = 0; i < (Int_t)indices_species.size(); ++i) {
         // N.B.: SSabundances, only in fCRs, not duplicated in fSpecies
         Int_t j_species = indices_species[i];
         Int_t j_cr = fMapSrc2CRIndices[j_species];

         // If anti-nucleus, abundance is set to 0 (no SS abundance)
         if (z < 0 && is_ssisotabund) {
            SetNormSrcSpectrum(j_species,  0.);
            continue;
         }

         // Get src abundance and apply corrections
         Double_t norm = GetNormSrcSpectrum(j_species);
         if (is_ssisotabund)
            norm = fCRs->GetCREntry(j_cr).GetSSIsotopeAbundance();
         if (is_ssisotfrac && !is_ssisotabund)
            norm = norm_z * fCRs->GetCREntry(j_cr).GetSSIsotopicFraction();
         if (is_fipbias)
            norm *= fip_bias;
         SetNormSrcSpectrum(j_species, norm);
      }
   }
   TUMessages::Indent(false);
}

//______________________________________________________________________________
TH1D *TUSrcMultiCRs::OrphanSrcAbundances(Bool_t is_cr_or_element, Bool_t is_src_or_ss,
      string const &model_name, string element_onwhich2rescale, Double_t rescaled_value) const
{
   //--- Returns histo of abundances for a steady-state standard source.
   //  is_cr_or_element         Abundances for isotopes (true) or elements (false)
   //  is_src_or_ss             Abundances for CR source (true) or SS (false)
   //  model_name               Extra-name (e.g., propagation model from which the source is taken) for plots
   //  element_onwhich2rescale  Element against which all elements are normalised
   //  rescaled_value           Normalisation value

   TH1D *h = NULL;

   // If not steady-state and not astro, nothing to return!
   if (!(IsAstroOrDM() && IsSteadyStateOrPointLike()))
      return h;


   Double_t norm = 1.;
   // Case requiring a specific normalisation to a specific element!
   if (fabs(rescaled_value - 1.) > 1.e-2) {
      // Update element to rescale
      fSpecies->CheckAndUpdateNormElement(element_onwhich2rescale);
      if (is_src_or_ss)
         norm = rescaled_value / GetNormSrcSpectrum_Element(element_onwhich2rescale, false);
      else
         norm = rescaled_value /  fCRs->SSElementAbundance(fCRs->ElementNameToZ(element_onwhich2rescale));
   }

   // Create histo according to cases
   string name;
   if (is_src_or_ss)
      name = model_name + "_abund_" + fSrcName;
   else
      name = "abund_ss";
   if (is_cr_or_element) {
      name = name + "_isotopes";
      vector<Int_t> list_jcr = fCRs->ExtractAllCRs(false);
      h = fCRs->OrphanEmptyHistoOfCRs(name.c_str(), "Labels from CR list", list_jcr);
      for (Int_t i = 0; i < (Int_t)list_jcr.size(); ++i) {
         Int_t j_cr = list_jcr[i];
         if (is_src_or_ss) {
            Double_t src_spect = 0.;
            Int_t j_src = IndexCRInSrcSpecies(j_cr);
            if (j_src >= 0)
               src_spect = GetNormSrcSpectrum(j_src) * norm;
            h->SetBinContent(i + 1, src_spect);
         } else
            h->SetBinContent(i + 1, fCRs->GetCREntry(j_cr).GetSSIsotopeAbundance()*norm);
      }
   } else {
      name = name + "_elements";
      vector<Int_t> list_z = fCRs->ExtractAllZ(false);
      h = fCRs->OrphanEmptyHistoOfElements(name.c_str(), "Labels from elements in CR list", list_z);
      for (Int_t i = 0; i < (Int_t)list_z.size(); ++i) {
         Int_t z = list_z[i];
         if (is_src_or_ss) {
            if (!fSpecies->IsElementAndInList(fCRs->ZToElementName(z))) h->SetBinContent(i + 1, 0.);
            else h->SetBinContent(i + 1, GetNormSrcSpectrum_Element(z, false)*norm);
         } else
            h->SetBinContent(i + 1, fCRs->SSElementAbundance(z)*norm);
      }
   }
   string tmp = "";
   if (rescaled_value <= 1000. && rescaled_value >= 1.01)
      tmp = Form("%.f", rescaled_value);
   else
      tmp = Form("%.3le", rescaled_value);
   string title = "Relative abundance [" + element_onwhich2rescale + " #equiv " + tmp + "]";
   h->SetYTitle(title.c_str());

   return h;
}

//______________________________________________________________________________
void TUSrcMultiCRs::PrintSrc(FILE *f, Bool_t is_header, Bool_t is_summary) const
{
   //--- Prints in f, the multi-CR source content.
   //  f                 File in which to print
   //  header            Whether to write source info

   string indent = TUMessages::Indent(true);

   if (is_header)
      fprintf(f, "%s Multi-CR source: %s (norm. parameter name \'%s\')\n",
              indent.c_str(), fSrcName.c_str(), fNameSrcNormPar.c_str());
   fprintf(f, "%s --------------- \n", indent.c_str());
   fSpecies->PrintList(f);

   indent = TUMessages::Indent(true);
   if (!is_summary)
      fprintf(f, "%s [Source template]:\n", indent.c_str());

   // Print only first entry since all rely on the same form with different parameters
   fSrcPerSpecies[0]->PrintSrc(f, is_header);
   fprintf(f, "\n");

   if (fFreePars && !is_summary)
      fFreePars->PrintPars(f);

   TUMessages::Indent(false);
   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUSrcMultiCRs::SetClass(TUInitParList *init_pars, string const &group, string const &subgroup,
                             string const &src_name, Bool_t is_verbose, FILE *f_log)
{
   //--- Sets class members for sources from 'group#subgroup' for 'src_name':
   //       - Steady state source [subgroup='SrcSteadyState']: a steady state
   //         'subgroup' source describe spectra and spatial distributions for
   //         each species in the source. Different species may share the same
   //         spectrum (resp. spatial dist.) or have a unique one (one per species).
   //       - Point-like source [subgroup='SrcPointLike': a point-like 'subgroup'
   //         source describes spectra for each species in the source (different
   //         species may share the same spectrum or have a unique one per species)
   //         for a time-dependent position, and a TStart and TStop time.
   //    N.B.: several sources can be provided in the initialisation file. They
   //    are recognised by a unique name given in the parameter associated with
   //    the #group#subgroup. The source loaded is the one matching 'src_name'
   //    among all sources available, where 'src_name' must be ASTRO_xxx (astro
   //    source) or DM_xxx (dark matter source) with xxx any string.
   //
   //  init_pars         TUInitParList class of initialisation parameters
   //  group             Group name (for parameter to load)
   //  subgroup          Subgroup name (for parameter to load)
   //  src_name          Source name to load (ASTRO_xxx or DM_xxx, where xxx is any string)
   //  is_verbose        Verbose or not when class is set
   //  f_log             Log for USINE

   string indent = TUMessages::Indent(true);
   if (is_verbose)
      fprintf(f_log, "%s[TUSrcMultiCRs::SetClass]\n", indent.c_str());

   if (is_verbose)
      fprintf(f_log, "%s### Set SrcMultiCRs class (CRs in src, src templates, source description)\n", indent.c_str());

   // Free memory
   Initialise(true);

   // Initialise fCRs and fTemplates
   fCRs = new TUCRList(init_pars, is_verbose, f_log);

   fIsDeleteCRs = true;
   fTemplates = new TUSrcTemplates();
   fTemplates->SetClass(init_pars, is_verbose, f_log);
   fIsDeleteTemplates = true;

   SetClassGen(init_pars, group, subgroup, src_name, is_verbose, f_log);

   if (is_verbose)
      fprintf(f_log, "%s[TUSrcMultiCRs::SetClass] <DONE>\n\n", indent.c_str());

   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUSrcMultiCRs::SetClass(TUInitParList *init_pars, string const &group, string const &subgroup,
                             string const &src_name, TUCRList *used_crs, TUSrcTemplates *used_templates,
                             Bool_t is_verbose, FILE *f_log)
{
   //--- Sets class members for sources from 'group#subgroup' for 'src_name':
   //       - Steady state source [subgroup='SrcSteadyState']: a steady state
   //         'subgroup' source describe spectra and spatial distributions for
   //         each species in the source. Different species may share the same
   //         spectrum (resp. spatial dist.) or have a unique one (one per species).
   //       - Point-like source [subgroup='SrcPointLike': a point-like 'subgroup'
   //         source describes spectra for each species in the source (different
   //         species may share the same spectrum or have a unique one per species)
   //         for a time-dependent position, and a TStart and TStop time.
   //    N.B.: several sources can be provided in the initialisation file. They
   //    are recognised by a unique name given in the parameter associated with
   //    the #group#subgroup. The source loaded is the one matching 'src_name'
   //    among all sources available, where 'src_name' must be ASTRO_xxx (astro
   //    source) or DM_xxx (dark matter source) with xxx any string.
   //
   //  init_pars         TUInitParList class of initialisation parameters
   //  group             Group name (for parameter to load)
   //  subgroup          Subgroup name (for parameter to load)
   //  src_name          Source name to load (ASTRO_xxx or DM_xxx, where xxx is any string)
   //  used_crs          List of CRs (to copy/use) from which to pick species for source
   //  used_templates    Templates (to copy/use) from which to fill source
   //  is_verbose        Verbose or not when class is set
   //  f_log             Log for USINE

   string indent = TUMessages::Indent(true);
   if (is_verbose)
      fprintf(f_log, "%s[TUSrcMultiCRs::SetClass]\n", indent.c_str());

   if (is_verbose)
      fprintf(f_log, "%s### Set SrcMultiCRs class (CRs in src, src templates, source description)\n", indent.c_str());

   // Free memory
   Initialise(true);


   // Initialise fCRs and fTemplates
   fCRs = used_crs;
   fIsDeleteCRs = false;
   fTemplates = used_templates;
   fIsDeleteTemplates = false;

   SetClassGen(init_pars, group, subgroup, src_name, is_verbose, f_log);

   if (is_verbose)
      fprintf(f_log, "%s[TUSrcMultiCRs::SetClass] <DONE>\n\n", indent.c_str());

   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUSrcMultiCRs::SetClassGen(TUInitParList *init_pars, string group, string subgroup, string src_name, Bool_t is_verbose, FILE *f_log)
{
   //--- Sets class members for sources from 'group#subgroup' for 'src_name':
   //       - Steady state source [subgroup='SrcSteadyState']: a steady state
   //         'subgroup' source describe spectra and spatial distributions for
   //         each species in the source. Different species may share the same
   //         spectrum (resp. spatial dist.) or have a unique one (one per species).
   //       - Point-like source [subgroup='SrcPointLike': a point-like 'subgroup'
   //         source describes spectra for each species in the source (different
   //         species may share the same spectrum or have a unique one per species)
   //         for a time-dependent position, and a TStart and TStop time.
   //    N.B.: several sources can be provided in the initialisation file. They
   //    are recognised by a unique name given in the parameter associated with
   //    the #group#subgroup. The source loaded is the one matching 'src_name'
   //    among all sources available, where 'src_name' must be ASTRO_xxx (astro
   //    source) or DM_xxx (dark matter source) with xxx any string.
   //
   //  init_pars         TUInitParList class of initialisation parameters
   //  group             Group name (for parameter to load)
   //  subgroup          Subgroup name (for parameter to load)
   //  src_name          Source name to load (ASTRO_xxx or DM_xxx, where xxx is any string)
   //  is_verbose        Verbose or not when class is set
   //  f_log             Log for USINE

   string indent = TUMessages::Indent(true);
   if (is_verbose) {
      fprintf(f_log, "%s[TUSrcMultiCRs::SetClassGen]\n", indent.c_str());
      fprintf(f_log, "%s### Set source characteristic\n", indent.c_str());
   }

   // Is it DM or ASTRO source?
   SetSrcName(src_name);
   if (is_verbose)
      fprintf(f_log, "%s   - name = %s\n", indent.c_str(), src_name.c_str());

   if (src_name.substr(0, 5) == "ASTRO")
      fIsAstroOrDM = true;
   else if (src_name.substr(0, 2) == "DM")
      fIsAstroOrDM = false;
   else {
      string message = "src_name must be ASTRO_xxx or DM_xxx (where xxx is any string): your value "
                       + src_name + " is not valid!";
      TUMessages::Error(f_log, "TUSrcMultiCRs", "SetClassGen", message);
   }
   if (is_verbose) {
      if (fIsAstroOrDM)
         fprintf(f_log, "%s   - origin = ASTRO (astrophysical)\n", indent.c_str());
      else
         fprintf(f_log, "%s   - origin = DM (dark matter)\n", indent.c_str());
   }


   // Extract init. params for steady-state or point-like sources.
   // N.B.: it is more compact to have a generic extract (because we
   // must find parameters matching the src_name), and then sort which
   // is which.
   // 1) Par names to search for
   vector<string> par_names;
   par_names.push_back("Species");        //[0]
   par_names.push_back("SpectAbundInit"); //[1]
   par_names.push_back("SpectTempl");     //[2]
   par_names.push_back("SpectValsPerCR");     //[3]

   if (subgroup == "SrcSteadyState") {
      fIsSteadyStateOrPointLike = true;
      par_names.push_back("SpatialTempl");  //[4]
      par_names.push_back("SpatialValsPerCR");  //[5]
   } else if (subgroup == "SrcPointLike") {
      fIsSteadyStateOrPointLike = false;
      par_names.push_back("SrcXPosition"); //[4]
      par_names.push_back("SrcYPosition"); //[5]
      par_names.push_back("SrcZPosition"); //[6]
      par_names.push_back("TStart");       //[7]
      par_names.push_back("TStop");        //[8]
   } else {
      string message = "subgroup " + subgroup
                       + " is not valid, it should be either SrcSteadyState or SrcPointLike";
      TUMessages::Error(f_log, "TUSrcMultiCRs", "SetClassGen", message);
   }
   if (is_verbose) {
      if (fIsSteadyStateOrPointLike)
         fprintf(f_log, "%s   - type = STEADY-STATE\n", indent.c_str());
      else
         fprintf(f_log, "%s   - type = POINT-LIKE\n", indent.c_str());
   }

   Int_t n_pars = par_names.size();
   //
   // 2) Find par name index (and position in multi-valued parameter)
   // matching 'src_name'. Find parameter and fill for given source name 'src_name'
   // N.B.: format for a src parameter is src_name|XXX (with XXX depends on the parameter)
   vector<Int_t> i_pars;
   vector<Int_t> i_multi;
   vector<vector<string> > src_pars; //[n_pars]['_'-separated entries in par]

   for (Int_t i = 0; i < n_pars; ++i) {
      i_pars.push_back(init_pars->IndexPar(group, subgroup, par_names[i]));

      // => For each par, loop on multi-parameters to find src_name
      for (Int_t j = 0; j < init_pars->GetParEntry(i_pars[i]).GetNVals(); ++j) {
         vector<string> pars;
         TUMisc::String2List(init_pars->GetParEntry(i_pars[i]).GetVal(j), "|", pars);
         if (pars[0] == src_name) {
            i_multi.push_back(j);
            src_pars.push_back(pars);
            break;
         }
      }

      // Check that parameter for 'src_name' exist
      if ((Int_t)src_pars.size() != (i + 1)) {
         string message = "parameter " + par_names[i] + " for source name '"
                          + src_name + "' missing in " + init_pars->GetFileNames();
         TUMessages::Error(f_log, "TUSrcMultiCRs", "SetClassGen", message);
      }
   }
   //
   // 3) Convert into human-readable variable names!
   string species = src_pars[0][1];
   // Format of "SpectraAbundInit" is comma-separated list gENUM_SRCABUND_INIT
   vector<gENUM_SRCABUND_INIT> enum_abund_init;
   vector<string> abund_init;
   if (src_pars[1][1] != "" && src_pars[1][1] != "-") {
      TUMisc::String2List(src_pars[1][1], ",", abund_init);
      for (Int_t i = 0; i < (Int_t)abund_init.size(); ++i) {
         gENUM_SRCABUND_INIT tmp_enum;
         TUEnum::Name2Enum(abund_init[i], tmp_enum);
         enum_abund_init.push_back(tmp_enum);
      }
   }

   // Template spectrum, normalisation, and values
   if ((Int_t)src_pars[2].size() != 3) {
      string message = "parameter " + par_names[2] + " for source name '"
                       + src_name + "' badly formatted in " + init_pars->GetFileNames();
      TUMessages::Error(f_log, "TUSrcMultiCRs", "SetClassGen", message);
   }
   string spectrum_templname = src_pars[2][1];
   fNameSrcNormPar = src_pars[2][2];
   string spectra_pars = src_pars[3][1];
   //cout << species << " " << spectrum_templname << " " << fNameSrcNormPar << endl;
   //abort();


   string spatdist_templname = "-", spatdist_pars = "-";
   string x_pos, y_pos, z_pos, tstart, tstop;
   if (fIsSteadyStateOrPointLike) {
      spatdist_templname = src_pars[4][1];
      if (spatdist_templname != "-")
         spatdist_pars = src_pars[5][1];
   } else  {
      x_pos = src_pars[4][1];
      y_pos = src_pars[5][1];
      z_pos = src_pars[6][1];
      tstart = src_pars[7][1];
      tstop = src_pars[8][1];
   }

   // Set fSpecies and allocate sources for this number of species
   if (is_verbose)
      fprintf(f_log, "%s### Set species belonging to src [from %s]\n", indent.c_str(), species.c_str());
   vector<string> crs_in_src;
   TUMisc::UpperCase(species);
   // Check which species to add in fSpecies and create it
   if (species == "ALL") {
      if (is_verbose)
         fprintf(f_log, "%s   [discard pure secondaries: %s]\n", indent.c_str(), fCRs->GetNamesPureSecondaries().c_str());
      // Need to add all CR species but secondary ones
      for (Int_t i = 0; i < fCRs->GetNCRs(); ++i) {
         if (!fCRs->IsPureSecondary(i))
            crs_in_src.push_back(fCRs->GetCREntry(i).GetName());
      }
   } else
      TUMisc::String2List(species, ",", crs_in_src);
   fSpecies = new TUCRList();
   fSpecies->SetCRList(TUMisc::GetPath(fCRs->GetFileName()), 1, crs_in_src, false, src_name);
   if (is_verbose)
      fSpecies->PrintList(f_log);


   // Allocate for N species found, and map fSpecies in to fCRs
   fIndexSrcNormInFreePar.clear();
   fIndexSrcNormInFreePar.assign(GetNSpecies(), -1);
   fSrcPerSpecies = new TUSrcVirtual*[GetNSpecies()];
   fFreeParsPerSpecies = new TUFreeParList*[GetNSpecies()];
   for (Int_t i = 0; i < GetNSpecies(); ++i) {
      fSrcPerSpecies[i] = NULL;
      fFreeParsPerSpecies[i] = NULL;
   }
   //
   // Map Src fSpecies and fCR lists
   fSpecies->MapIndices(fCRs, fMapCR2SrcIndices, false);
   fCRs->MapIndices(fSpecies, fMapSrc2CRIndices, false);

   // Allocate and fill type-specific (steady-state or point-like) fSrcPerSpecies
   if (fIsSteadyStateOrPointLike) {
      /////////////////////////
      // STEADY-STATE SOURCE //
      /////////////////////////
      // => Name, spectra, and spatial distributions to fill
      for (Int_t i = 0; i < GetNSpecies(); ++i) {
         fSrcPerSpecies[i] = new TUSrcSteadyState();
         fSrcPerSpecies[i]->SetSrcName((fSpecies->GetCREntry(i).GetName() + " " + src_name));
      }
      if (spectrum_templname != "-")
         AllocateAndFillSrcFromTemplate(spectrum_templname, spectra_pars, is_verbose, f_log,
                                        true/*is_spectrum_or_spatdist*/, fNameSrcNormPar);
      if (spatdist_templname != "-")
         AllocateAndFillSrcFromTemplate(spatdist_templname, spatdist_pars, is_verbose, f_log,
                                        false/*is_spectrum_or_spatdist*/, "");
   } else {
      ///////////////////////
      // POINT-LIKE SOURCE //
      ///////////////////////
      // => Name, spectra, and position to fill
      for (Int_t i = 0; i < GetNSpecies(); ++i) {

         TUSrcPointLike *tmp_src = new TUSrcPointLike();
         tmp_src->SetSrcName((fSpecies->GetCREntry(i).GetName() + " " + src_name));
         if (tstart != "-")
            tmp_src->SetTStart(atof(tstart.c_str()));
         if (tstop != "-")
            tmp_src->SetTStop(atof(tstop.c_str()));

         // Set position for first species (positions for any
         // species are the same in a given src)
         if (i == 0) {
            tmp_src->SetTDepPosition(0, x_pos);
            tmp_src->SetTDepPosition(1, y_pos);
            tmp_src->SetTDepPosition(2, z_pos);
         } else {
            for (Int_t j = 0; j < 3; ++j)
               tmp_src->UseTDepPosition(j, fSrcPerSpecies[0]->GetTDepPosition(j));
         }

         fSrcPerSpecies[i] = tmp_src;
         tmp_src = NULL;
      }

      // Fill source for all species from user-selected template and parameters
      if (spectra_pars != "-")
         AllocateAndFillSrcFromTemplate(spectrum_templname, spectra_pars, is_verbose, f_log,
                                        true/*is_spectrum_or_spatdist*/, fNameSrcNormPar);
   }

   // And set if necessary isotopic abundances!
   if (enum_abund_init.size() != 0)
      InitialiseSpectrumNorm(enum_abund_init, is_verbose, f_log);

   if (is_verbose)
      fprintf(f_log, "%s[TUSrcMultiCRs::SetClassGen] <DONE>\n\n", indent.c_str());
   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUSrcMultiCRs::SetNormSrcSpectrum_FixedIsotopicRelAbund(Double_t const &abund_element, string const &element)
{
   //--- Sets sum_{j_cr \in element} fSpectrumNorms[j_cr] = abund_element,
   //    leaving unchanged the relative abundances of this element.
   //  abund_element     Value of the norm. abundances to set to
   //  element           CR element name

   // Extract list of CRs for element
   vector<Int_t> isot_indices;
   fSpecies->ElementNameToCRIndices(element, isot_indices, false);
   Int_t n_isot = isot_indices.size();

   // Calculate current 'element' abundance
   Double_t qtot = 0.;
   for (Int_t i = 0; i < n_isot; ++i)
      qtot += GetNormSrcSpectrum(isot_indices[i]);

   // Calculate 'isotopic' abundances (if qtot not 0)
   vector<Double_t> relative_abund(n_isot, 1. / (Double_t)n_isot);
   if (qtot > 1.e-40) {
      for (Int_t i = 0; i < n_isot; ++i)
         relative_abund[i] = GetNormSrcSpectrum(isot_indices[i]) / qtot;
   }

   // Set isotopic abundance to match new element abundance
   for (Int_t i = 0; i < n_isot; ++i)
      SetNormSrcSpectrum(isot_indices[i], relative_abund[i]*abund_element);
}

//______________________________________________________________________________
void TUSrcMultiCRs::TEST(TUInitParList *init_pars, FILE *f)
{
   //--- Test function for this class (called in usine -t).
   //  init_pars         TUInitParList class of initialisation parameters
   //  f                 Output file for test

   string message = "Set single source (several species)point-like source (position and source spectrum)";
   TUMessages::Test(f, "TUSrcMultiCRs", message);

   string group = "ModelTEST";
   string subgroup = "SrcSteadyState";
   string src_name = "ASTRO_STD";

   // Set class and test methods
   fprintf(f, " * Base class (including for spatial dist. w/wo free params):\n");

   fprintf(f, "   > SetClass(init_pars=\"%s\", group=%s, subgroup=%s, src_name=%s, is_verbose=false, f_log=f);\n",
           init_pars->GetFileNames().c_str(), group.c_str(), subgroup.c_str(), src_name.c_str());
   SetClass(init_pars, group, subgroup, src_name, false, f);
   fprintf(f, "\n");
   fprintf(f, "   > PrintSrc(f);\n");
   PrintSrc(f);
   Int_t i_species = 8;
   fprintf(f, "   > PrintSrc(f, i_species=%d);   [species=GetSpecies()->GetCREntry(%d).GetName()=%s\n",
           i_species, i_species, GetSpecies()->GetCREntry(i_species).GetName().c_str());
   PrintSrc(f, i_species);
   fprintf(f, "   > GetNSpecies();   => %d\n",  GetNSpecies());
   fprintf(f, "   > GetNFreePars();  => %d\n",  GetNFreePars());
   fprintf(f, "   > GetSrc(=%d)->GetSrcFreePars(is_spect_or_spatdist=true)->PrintPars(f);\n", i_species);
   GetSrc(i_species)->GetSrcFreePars(true)->PrintPars(f);
   fprintf(f, "   > GetSrc(i_species=%d)->GetSrcFreePars(is_spect_or_spatdist=false)->PrintPars(f);\n", i_species);
   GetSrc(i_species)->GetSrcFreePars(false)->PrintPars(f);
   i_species = 4;
   fprintf(f, "   > PrintSrc(f, i_species=%d);   [species=GetSpecies()->GetCREntry(%d).GetName()=%s\n",
           i_species, i_species, GetSpecies()->GetCREntry(i_species).GetName().c_str());
   PrintSrc(f, i_species);
   fprintf(f, "   > GetNSpecies();   => %d\n",  GetNSpecies());
   fprintf(f, "   > GetNFreePars();  => %d\n",  GetNFreePars());
   fprintf(f, "   > GetSrc(=%d)->GetSrcFreePars(is_spect_or_spatdist=true)->PrintPars(f);\n", i_species);
   GetSrc(i_species)->GetSrcFreePars(true)->PrintPars(f);
   fprintf(f, "   > GetSrc(i_species=%d)->GetSrcFreePars(is_spect_or_spatdist=false)->PrintPars(f);\n", i_species);
   GetSrc(i_species)->GetSrcFreePars(false)->PrintPars(f);

   fprintf(f, "\n\n");

   group = "Model2DKisoVc";
   fprintf(f, "   > SetClass(init_pars=\"%s\", group=%s, subgroup=%s, src_name=%s, is_verbose=false, f_log=f);\n",
           init_pars->GetFileNames().c_str(), group.c_str(), subgroup.c_str(), src_name.c_str());
   SetClass(init_pars, group, subgroup, src_name, false, f);
   fprintf(f, "\n");
   fprintf(f, "   > PrintSrc(f);\n");
   PrintSrc(f);
   fprintf(f, "   > PrintSrc(f, i_species=%d)   [species=GetSpecies()->GetCREntry(%d).GetName()=%s\n",
           i_species, i_species, GetSpecies()->GetCREntry(i_species).GetName().c_str());
   PrintSrc(f, i_species);
   fprintf(f, "   > GetNSpecies();   => %d\n",  GetNSpecies());
   fprintf(f, "   > GetNFreePars();  => %d\n",  GetNFreePars());
   fprintf(f, "\n");
   fprintf(f, "\n");


   fprintf(f, " * Calculate spatial distribution and spectrum for various species.\n");
   fprintf(f, "   > TUCoordTXYZ coord_txyz; coord_txyz.SetValTXYZ(1., 2., 0., 3.); coord_txyz.PrintVals(f);\n");
   TUCoordTXYZ coord_txyz;
   coord_txyz.SetValTXYZ(1., 2., 0., 3.);
   coord_txyz.PrintVals(f);
   fprintf(f, "   > TUCoordE coord_e;  ... set to {2., 2., 2....} for all E coordinates...; coord_e.PrintVals(f);\n");
   TUCoordE coord_e;
   vector<string> e_vars;
   TUMisc::String2List(fSrcPerSpecies[0]->GetSrcSpectrum()->GetFormulaVars(), ",", e_vars);
   Int_t n_edep = e_vars.size() - 4;
   vector<Double_t> edep_vals(n_edep, 2.);
   coord_e.SetValsE(&edep_vals[0], n_edep, 0, true);
   fprintf(f, "   > \n");
   coord_e.PrintVals(f);
   const Int_t n_spec = 2;
   Int_t i_spec[n_spec] = {0, 4};
   for (Int_t i = 0; i < n_spec; ++i) {
      i_species = i_spec[i];
      fprintf(f, "   > ValueSrcSpatialDistrib(%d, &coord_txyz)         =>  %le;\n", i_species, ValueSrcSpatialDistrib(i_species, &coord_txyz));
      fprintf(f, "   > ValueSrcSpectrum(%d, &coord_e, &coord_txyz)     =>  %le;\n", i_species, ValueSrcSpectrum(i_species, &coord_e, &coord_txyz));
   }
   fprintf(f, "\n");
   fprintf(f, "\n");


   fprintf(f, " * N.B.: every time ValueXXX() or UpdateFromFreeParsAndResetStatus() is called, update all class members for all species.\n");
   fprintf(f, "   > GetFreePars()->GetParListStatus() = %d;\n", GetFreePars()->GetParListStatus());
   for (Int_t i = 0; i < n_spec; ++i) {
      i_species = i_spec[i];
      fprintf(f, "   > GetSrc(%d)->GetSrcFreePars(is_spect_or_spatdist=true)->GetParListStatus() = %d;\n",
              i_species, GetSrc(i_species)->GetSrcFreePars(true)->GetParListStatus());
   }
   fprintf(f, "   ---\n");
   fprintf(f, "   > GetFreePars()->SetParVal(eta_s, 3.);\n");
   GetFreePars()->SetParVal(GetFreePars()->IndexPar("eta_s"), 3.);
   fprintf(f, "   > GetFreePars()->PrintPars(f);\n");
   GetFreePars()->PrintPars(f);
   fprintf(f, "   > GetFreePars()->GetParListStatus() = %d;\n", GetFreePars()->GetParListStatus());
   for (Int_t i = 0; i < n_spec; ++i) {
      i_species = i_spec[i];
      fprintf(f, "   > GetSrc(%d)->GetSrcFreePars(is_spect_or_spatdist=true)->PrintPars(f);\n", i_species);
      GetSrc(i_species)->GetSrcFreePars(true)->PrintPars(f);
      fprintf(f, "   > GetSrc(%d)->GetSrcFreePars(is_spect_or_spatdist=true)->GetParListStatus() = %d;\n",
              i_species, GetSrc(i_species)->GetSrcFreePars(true)->GetParListStatus());
   }
   fprintf(f, "   ---\n");
   for (Int_t i = 0; i < n_spec; ++i) {
      i_species = i_spec[i];
      fprintf(f, "   > ValueSrcSpectrum(%d, &coord_e, &coord_txyz)     =>  %le;\n", i_species, ValueSrcSpectrum(i_species, &coord_e, &coord_txyz));
   }
   fprintf(f, "   > GetFreePars()->GetParListStatus() = %d;\n", GetFreePars()->GetParListStatus());
   for (Int_t i = 0; i < n_spec; ++i) {
      i_species = i_spec[i];
      fprintf(f, "   > GetSrc(%d)->GetSrcFreePars(is_spect_or_spatdist=true)->GetParListStatus() = %d;\n",
              i_species, GetSrc(i_species)->GetSrcFreePars(true)->GetParListStatus());
   }
   fprintf(f, "   ---\n");
   fprintf(f, "   > GetFreePars()->SetParVal(q_1H, 1.e5);\n");
   GetFreePars()->SetParVal(GetFreePars()->IndexPar("q_1H"), 1.e5);
   GetFreePars()->PrintPars(f);
   fprintf(f, "   > GetFreePars()->GetParListStatus() = %d;\n", GetFreePars()->GetParListStatus());
   for (Int_t i = 0; i < n_spec; ++i) {
      i_species = i_spec[i];
      fprintf(f, "   > GetSrc(%d)->GetSrcFreePars(is_spect_or_spatdist=true)->PrintPars(f);\n", i_species);
      GetSrc(i_species)->GetSrcFreePars(true)->PrintPars(f);
      fprintf(f, "   > GetSrc(%d)->GetSrcFreePars(is_spect_or_spatdist=true)->GetParListStatus() = %d;\n",
              i_species, GetSrc(i_species)->GetSrcFreePars(true)->GetParListStatus());
   }
   fprintf(f, "   > UpdateFromFreeParsAndResetStatus();\n");
   UpdateFromFreeParsAndResetStatus();
   fprintf(f, "   > GetFreePars()->GetParListStatus() = %d;\n", GetFreePars()->GetParListStatus());
   for (Int_t i = 0; i < n_spec; ++i) {
      i_species = i_spec[i];
      fprintf(f, "   > GetSrc(%d)->GetSrcFreePars(is_spect_or_spatdist=true)->GetParListStatus() = %d;\n",
              i_species, GetSrc(i_species)->GetSrcFreePars(true)->GetParListStatus());
   }
   fprintf(f, "   ---\n");
   for (Int_t i = 0; i < n_spec; ++i) {
      i_species = i_spec[i];
      fprintf(f, "   > ValueSrcSpectrum(%d, &coord_e, &coord_txyz)     =>  %le;\n", i_species, ValueSrcSpectrum(i_species, &coord_e, &coord_txyz));
   }
   fprintf(f, "\n");
   fprintf(f, "\n");




   fprintf(f, " * Mapping of 'CR list <-> src species' indices:\n");
   fprintf(f, "fMapCR2SrcIndices=%d   fMapSrc2CRIndices=%d\n", (Int_t)fMapCR2SrcIndices.size(), (Int_t)fMapSrc2CRIndices.size());
   for (Int_t i_cr = 0; i_cr < fCRs->GetNCRs(); ++i_cr) {
      Int_t i_src = IndexCRInSrcSpecies(i_cr);
      if (i_src >= 0)
         fprintf(f, "    - CR[i_cr=%2d]=%6s   => i_src=IndexCRInSrcSpecies(i_cr)=%d => i_cr=IndexSrcSpeciesInCRList(i_src)=%d  Species[i_src=%2d]=%6s\n",
                 i_cr, fCRs->GetCREntry(i_cr).GetName().c_str(), IndexCRInSrcSpecies(i_cr),
                 IndexSrcSpeciesInCRList(i_src), i_src, fSpecies->GetCREntry(i_src).GetName().c_str());
      else
         fprintf(f, "    - CR[i_cr=%2d]=%6s   => i_src=IndexCRInSrcSpecies(i_cr)=%d => species not in src!\n",
                 i_cr, fCRs->GetCREntry(i_cr).GetName().c_str(), IndexCRInSrcSpecies(i_cr));
   }
   fprintf(f, "\n");
   fprintf(f, "\n");


   // Copy
   fprintf(f, " * Test Copy method:\n");
   fprintf(f, "   > TUSrcMultiCRs src_multicrs; src_multicrs.Copy(*this);\n");
   TUSrcMultiCRs src_multicrs;
   src_multicrs.Copy(*this);
   fprintf(f, "   > src_multicrs.PrintSrc(f);\n");
   src_multicrs.PrintSrc(f);
   fprintf(f, "\n");

   fprintf(f, " * Modify parameters:\n");
   fprintf(f, "   > PrintSrc(f);\n");
   PrintSrc(f);
   fprintf(f, "   > TUI_ModifyFreeParameters();  [uncomment in TEST() to enable it]\n");
   //TUI_ModifyFreeParameters();
   fprintf(f, "   > PrintSrc(f);\n");
   PrintSrc(f);
   fprintf(f, "\n");


}

//______________________________________________________________________________
void TUSrcMultiCRs::UpdateFromFreeParsAndResetStatus()
{
   //--- Updates all formulae from updated fFreePars values and reset fFreePars status.

   // If status of parameters has changed
   if (fFreeParsPerSpecies && fFreePars && fFreePars->GetParListStatus()) {
      if (fFreePars && fFreePars->GetParListStatus()) {
         // Update Src formula for each species
         if (fSrcPerSpecies) {
            for (Int_t i = 0; i < GetNSpecies(); ++i) {
               if (fSrcPerSpecies[i])
                  fSrcPerSpecies[i]->UpdateFromFreeParsAndResetStatus();
            }
         }
         // And also update free par for this species
         if (fFreeParsPerSpecies) {
            for (Int_t i = 0; i < GetNSpecies(); ++i) {
               if (fFreeParsPerSpecies[i])
                  fFreeParsPerSpecies[i]->ResetStatusParsAndParList();
            }
         }

         fFreePars->ResetStatusParsAndParList();;
      }
   }
}
