// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

// C/C++ include
// ROOT include
// USINE include
#include "../include/TUAtomElements.h"
#include "../include/TUMessages.h"
#include "../include/TUPhysics.h"

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TUAtomElements                                                       //
//                                                                      //
// Name and Z for atomic (anti-)elements from H (Z=1) to Mt (Z=109).    //
//                                                                      //
// The class TUAtomElements provides methods to get Z from an element   //
// or isotope name (case insensitive) and vice-versa. To deal with an   //
// anti-element or an anti-isotope, use the extension "BAR" (H-BAR,     //
// 1H-BAR, 2H-BAR, etc.) and Z<0.                                       //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

ClassImp(TUAtomElements)

//______________________________________________________________________________
TUAtomElements::TUAtomElements()
{
   // ****** Default constructor ******
   // (fills the data member fName with the names of atomic elements)

   const Int_t n = 109;

   fName.reserve(n);
   string elements[n] = {
      "H", "He", "Li", "Be", "B", "C", "N", "O", "F", "Ne",
      "Na", "Mg", "Al", "Si", "P", "S", "Cl", "Ar", "K", "Ca",
      "Sc", "Ti", "V", "Cr", "Mn", "Fe", "Co", "Ni", "Cu", "Zn",
      "Ga", "Ge", "As", "Se", "Br", "Kr", "Rb", "Sr", "Y", "Zr",
      "Nb", "Mo", "Tc", "Ru", "Rh", "Pd", "Ag", "Cd", "In", "Sn",
      "Sb", "Te", "I", "Xe", "Cs", "Ba", "La", "Ce", "Pr", "Nd",
      "Pm", "Sm", "Eu", "Gd", "Tb", "Dy", "Ho", "Er", "Tm", "Yb",
      "Lu", "Hf", "Ta", "W", "Re", "Os", "Ir", "Pt", "Au", "Hg",
      "Tl", "Pb", "Bi", "Po", "At", "Rn", "Fr", "Ra", "Ac", "Th",
      "Pa", "U", "Np", "Pu", "Am", "Cm", "Bk", "Cf", "Es", "Fm",
      "Md", "No", "Lr", "Rf", "Db", "Sg", "Bh", "Hs", "Mt"
   };

   // Upper-case all names
   for (Int_t i = 0; i < n; ++i) {
      TUMisc::UpperCase(elements[i]);
      fName.push_back(elements[i]);
   }
}

//______________________________________________________________________________
TUAtomElements::~TUAtomElements()
{
   // ****** Default destructor ******
   //
}

//______________________________________________________________________________
void TUAtomElements::CheckZ(Int_t z)
{
   //--- Checks if Z corresponds to an existing element (<0 values are accepted,
   //    they correspond to anti-elements).
   //    N.B.: if Z out of range, aborts.
   //      - Z = 0 is excluded (no corresponding atomic element);
   //      - |Z| > TUPhysics::Zmax() is excluded.
   //  z                 Charge

   z = abs(z);
   if (z == 0 || z > TUPhysics::Zmax()) {
      string message = Form("Z=%d is not an element...", z);
      TUMessages::Error(stdout, "TUAtomElements", "CheckZ", message);
   }
}

//______________________________________________________________________________
Int_t TUAtomElements::ElementNameToZ(string const &element, Bool_t is_verbose, FILE *f_log) const
{
   //--- Returns for atomic element X (or anti-element X-BAR) its charge Z
   //    (returns 0 if not an element).
   //  element           Name (case insensitive)
   //  is_verbose        Chatter on or off
   //  f_log             Log file

   Int_t sign = 1;

   // Check if there is a "-" in name: if yes, it is an anti-element
   // and the sign of its charge is -1
   string::size_type loc = element.find("-", 0);
   if (loc != string::npos) sign = -1;
   string tmp = element.substr(0, loc);
   TUMisc::UpperCase(tmp);

   // Check if electron or positron
   if (tmp == "POSITRON")
      return 1;
   else if (tmp == "ELECTRON")
      return -1;

   // Find element position in list
   for (Int_t i = 0; i < GetNElements(); ++i) {
      if (tmp == fName[i]) return sign * (i + 1);
   }
   // If reaches this line, means it failed (no Z found)
   if (is_verbose) {
      string message = element + " does not correspond to any existing element";
      TUMessages::Warning(f_log, "TUAtomElements", "ElementNameToZ", message);
   }

   return 0;
}

//______________________________________________________________________________
Bool_t TUAtomElements::IsContainElementName(string const &qty) const
{
   //--- Returns true if qty (can be a ratio) contains element name.
   //  qty               Quantity to check

   string qty_uc = qty;
   TUMisc::UpperCase(qty_uc);
   for (Int_t i = 0; i < GetNElements(); ++i) {
      if (qty_uc.find(fName[i]) != string::npos) return true;
   }

   return false;
}

//______________________________________________________________________________
Bool_t TUAtomElements::IsElement(string const &qty) const
{
   //--- Returns true if qty is an element (or anti-element)
   //  qty               Quantity to check

   string qty_uc = qty;
   TUMisc::UpperCase(qty_uc);

   for (Int_t i = 0; i < GetNElements(); ++i) {
      if (qty_uc == fName[i] || qty_uc == (fName[i] + "-BAR"))
         return true;
   }

   return false;
}

//______________________________________________________________________________
string TUAtomElements::MostAbundantIsotope(string const &element, FILE *f_log) const
{
   //--- For an element name, returns the most abundant isotope.
   //    N.B.: taken from SS abundances in Lodders, ApJ 591, 1220 (2003)
   //  element           Element name
   //  f_log             Log file

   Int_t z = ElementNameToZ(element, true);

   // If neutrino, gamma, or lepton
   if (z >= -1 && z <= 1) {
      string tmp = element;
      TUMisc::UpperCase(tmp);
      if (tmp == "POSITRON" || tmp == "ELECTRON")
         return element;
   }

   const Int_t n = 30;
   string isotope[n] = {
      "1H", "4He", "7Li", "9Be", "11B", "12C", "14N", "16O", "19F", "20Ne",
      "23Na", "24Mg", "27Al", "28Si", "31P", "32S", "35Cl", "36Ar", "39K", "40Ca",
      "45Sc", "48Ti", "51V", "52Cr", "55Mn", "56Fe", "59Co", "58Ni", "63Cu", "64Zn"
   };

   if (z <= 30) {
      TUMisc::UpperCase(isotope[z - 1]);
      return isotope[z - 1];
   } else {
      string message = "Dominant isotope is not given (not always a dominant one) for "
                       + element + " (Z>30)";
      TUMessages::Warning(f_log, "TUAtomElements", "MostAbundantIsotope", message);
      return element;
   }
   return "";
}

//______________________________________________________________________________
void TUAtomElements::TEST(FILE *f) const
{
   //--- Test function for this class (called in usine -t).
   //  f                 Output file for test

   string message = "Elements Z and name (1<=Z<=109)";
   TUMessages::Test(f, "TUAtomElements", message);

   fprintf(f, "    > CRNameToA(10Be);               => %d\n", CRNameToA("10Be"));
   fprintf(f, "    > CRNameToCRElement(10Be);       => %s\n", CRNameToCRElement("10Be").c_str());
   fprintf(f, "    > CRNameToZ(10Be);               => %d\n", CRNameToZ("10Be"));
   fprintf(f, "    > ElementNameToZ(B, true, f);    => %d\n", ElementNameToZ("B", true, f));
   fprintf(f, "    > ElementNameToZ(FF, true, f)    => ");
   fprintf(f, "%d\n", ElementNameToZ("FF", true, f));
   fprintf(f, "    > GetNElements();                => %d\n", GetNElements());
   fprintf(f, "    > IsContainElementName(10Be);    => %d\n", IsContainElementName("10Be"));
   fprintf(f, "    > IsContainElementName(BO+A);    => %d\n", IsContainElementName("BO+A"));
   fprintf(f, "    > IsElement(Zv);                 => %d\n", IsElement("Zv"));
   fprintf(f, "    > MostAbundantIsotope(C, f);     => %s\n", MostAbundantIsotope("C", f).c_str());
   fprintf(f, "    > MostAbundantIsotope(U, f);      => ");
   fprintf(f, "%s\n", MostAbundantIsotope("U", f).c_str());
   fprintf(f, "    > NameToZ(12C);                  => %d\n", NameToZ("12C"));
   fprintf(f, "    > NameToZ(C);                    => %d\n", NameToZ("C"));
   fprintf(f, "    > ZToElementName(10);            => %s\n", ZToElementName(10).c_str());
   fprintf(f, "    > ZToElementNameROOT(10);        => %s\n", ZToElementNameROOT(10).c_str());
   fprintf(f, "    > ZToElementName(-1);            => %s\n", ZToElementName(-1).c_str());
   fprintf(f, "\n");
}

//______________________________________________________________________________
string TUAtomElements::ZToElementName(Int_t z) const
{
   //--- Returns for a given charge Z the associated atomic element name.
   //  z                 Charge

   // (CheckZ is used to check whether or not Z is out of range).
   CheckZ(z);

   // The i-th element has a charge Z = i-1 (see constructor);
   if (z > 0) return fName[z - 1];
   else {
      string element =  fName[abs(z) - 1];
      element += "-BAR";
      return element;
   }
}
