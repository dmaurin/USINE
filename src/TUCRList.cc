// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

// C/C++ include
#include <algorithm>
#include <cmath>
#include <fstream>
#include <iostream>
// ROOT include
// USINE include
#include "../include/TUCRList.h"
#include "../include/TUMessages.h"

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TUCRList                                                             //
//                                                                      //
// List of CRs (TUCREntry) and its list of decay/spallative parents.    //
//                                                                      //
// The class TUCRList provides TUCREntry[NCRs], i.e. charts for each    //
// CR entry, read from a chart-formatted file (see below). It inherits  //
// from TAtomicProperties (for atomic properties). For each CR in the,  //
// the following quantities are set:                                    //
//    - radioactive (BETA or EC decay) parents;
//    - spallative parents (user-defined) for 'X', i.e. the CR nuclei
//      whose fragmentation on a target gives this 'X', are also kept
//      in a vector of indices. Note that the parents must belong
//      to the list of CRs, so this spares having redundant copies.
//                                                                      //
// N.B.: if you wish to create your own charts file (for propagation    //
// purpose), ensure that the species are sorted by growing mass and     //
// charge (but for the leptons and gamma which are always at the        //
// beginning of the list).                                              //
// BEGIN_HTML
// <b>Example of charts file:</b> <a href="../inputs/crcharts_Zmax30_ghost97.dat target="_blank">$USINE/inputs/crcharts_Zmax30_ghost97.dat</a>.
// END_HTML
//                                                                      //
// As this class is of great importance for propagation models,         //
// it is further detailed below:                                        //
//                                                                      //
// I. What does 'spallative parent' mean?                               //
//   It is when a CR of index j_proj lead to a lighter CR j_cr after    //
//   a nuclear interaction on the ISM. For instance,                    //
//       - 1H + He (ISM) -> 1H-BAR   => 1H is a parent of 1H-BAR;
//       - 12C + H (ISM) -> 10B      => 12C is a parent of 10B;
//       - 1H + H (ISM) -> POSITRON  => 1H is a parent of POSITRON.
//   N.B.: all methods referring to a given parent must also specify    //
//   the CR (2 indices), e.g., GetmGeV(Int_t j_cr, Int_t j_proj).       //
//                                                                      //
// II. How to choose the parents?                                       //
//   The heaviest CR is always set to be a primary species (no parents).//
//   It is up to the user to check that his choice is meaningful. By    //
//   default, all CRs heavier than a given one are potential parents.   //
//   The user may decide otherwise (more restricted list of parents).   //
//   In principle, the list can be reduced at run time to optimise      //
//    propagation calculations (keep only the significant contributors).//
//                                                                      //
// III. Print/Print methods                                             //
//       - PrintCharts    => CRs and their Charts;
//       - PrintGhosts    => CRs/Ghosts (for cross-section calc. only)
//       - PrintList      => CRs in list;
//       - PrintParents   => Summary info for CR/parents.
//                                                                      //
//////////////////////////////////////////////////////////////////////////

ClassImp(TUCRList)

//______________________________________________________________________________
TUCRList::TUCRList() : TUAtomProperties()
{
   // ****** Default constructor ******

   Initialise(false);
}

//______________________________________________________________________________
TUCRList::TUCRList(TUCRList const &crs) : TUAtomProperties()
{
   // ****** Copy constructor ******
   //  crs               Object to copy

   Initialise(false);
   Copy(crs);
}

//______________________________________________________________________________
TUCRList::TUCRList(TUInitParList *init_pars, Bool_t is_verbose, FILE *f_log) : TUAtomProperties()
{
   // ****** Default constructor ******
   //  init_pars         TUInitParList object of USINE parameters
   //  is_verbose        Verbose or not when class is set
   //  f_log             Log for USINE

   Initialise(false);
   SetClass(init_pars, is_verbose, f_log, true);
}

//______________________________________________________________________________
TUCRList::~TUCRList()
{
   // ****** Default destructor ******
   Initialise(true);
}

//______________________________________________________________________________
void TUCRList::AllocateParentsAndFragments()
{
   //--- Allocates memory for fIndicesParents.

   if (GetNCRs() == 0) TUMessages::Error(stdout, "TUCRList", "AllocateParentsAndFragments",
                                            "you have to define first fCRs!");

   if (fIndicesFragments) delete[] fIndicesFragments;
   fIndicesFragments = new vector<Int_t>[GetNCRs()];

   if (fIndicesParents) delete[] fIndicesParents;
   fIndicesParents = new vector<Int_t>[GetNCRs()];
}

//______________________________________________________________________________
void TUCRList::CheckAndUpdateNormElement(string &element) const
{
   //--- Sometimes, it happens that some quantity has to be re-normalised to the
   //    value measured for a given element. If "element" has no isotopes in fCRs,
   //    provides an alternative element ("element" is thus used as input/output).
   // INPUT/OUTPUT:
   //  element           Name of element to norm (unchanged if found in list)


   // Check that element is in list, or return the first element found
   Int_t z = TUAtomProperties::ElementNameToZ(element, false);
   if (!IsElementAndInList(element))
      z = ExtractZMin();

   // Z for {H, He, C, O, Si, Fe, Sr, Zr, Te, Pb, Pt, Th, U}
   const Int_t n_prim = 13;
   Int_t z_prim[n_prim] = {1, 2, 6, 8, 14, 26, 38, 40, 52, 78, 82, 90, 92};
   for (Int_t i = 0; i < n_prim; ++i) {
      if (z_prim[i] < z)
         continue;
      element = TUAtomProperties::ZToElementName(z_prim[i]);
      if (IsElementAndInList(element))
         return;
   }

   // If still in function, means that did not find any quantity to normalise
   TUMessages::Error(stdout, "TUCRList", "CheckAndUpdateNormElement",
                     "Not enough elements in CR list to succeed");
}

//______________________________________________________________________________
void TUCRList::CheckParents(Int_t j_cr)
{
   //--- Checks if a parent appears several times in list: keeps only one instance.
   //  j_cr              Index of CR to check

   Int_t n = GetNParents(j_cr);
   for (Int_t i = 0; i < n; ++i) {
      for (Int_t j = i + 1; j < n; ++j) {
         if (fCRs[IndexInCRList_Parent(j_cr, i)].GetName() == fCRs[IndexInCRList_Parent(j_cr, j)].GetName()) {
            fIndicesParents[j_cr].erase(fIndicesParents[j_cr].begin() + j);
            --n;
         }
      }
   }
}

//______________________________________________________________________________
Bool_t TUCRList::ComboToCRIndices(string const &combo, vector<Int_t> &cr_indices,
                                  Int_t &n_denom, Bool_t is_verbose, FILE *f) const
{
   //--- Extracts vector of all CR indices (and number of CRs in denominator if
   //    ratio) in the quantity "combo" (return false if combo ill-defined). For
   //    instance, if quantity = B/C, extract 10B,11B,12C,13C,14C and n_denom=3
   //    (if these isotopes are present in fCRs!).
   // INPUTS:
   //  combo             Name to convert in a vector of CR names
   //  is_verbose        Print CR names on screen if true
   //  f                 Output file for chatter
   // OUTPUTS:
   //  cr_indices        Vector of CR indices
   //  n_denom           Number of CRs in denominator (0 if not a ratio)

   // Force initialisation of output variables
   cr_indices.clear();
   n_denom = 0;


   // Special case: <LNA> or ALLSPECTRUM
   string up_combo = combo;
   TUMisc::UpperCase(up_combo);
   if (up_combo == "<LNA>" || up_combo == "ALLSPECTRUM") {
      for (Int_t ll = 0; ll < GetNCRs(); ++ll)
         cr_indices.push_back(ll);
      n_denom = 0;
      return true;
   }

   // Extract the numerator and denominator (if ratio), otherwise,
   // this is the quantity under examination!
   vector<string> num_den;
   TUMisc::String2List(combo, "/", num_den);

   // Loop on num and denum
   for (Int_t i = 0; i < (Int_t)num_den.size(); ++i) {
      // Check if this is a sum of elements/CRs and adds up all CR names...
      vector<string> list;
      TUMisc::String2List(num_den[i], "+", list);

      // Loop on all quantities in sum
      for (Int_t k = 0; k < (Int_t)list.size(); k++) {
         // Check whether it is a CR, or an element, or bad name
         Int_t j_cr = Index(list[k], false, stdout);
         vector<Int_t> tmp_indices;

         Bool_t is_element = false;
         if (j_cr < 0)
            is_element = ElementNameToCRIndices(list[k], tmp_indices, is_verbose, f);
         if (j_cr < 0 && (!is_element || tmp_indices.size() == 0))
            return false;

         if (j_cr >= 0) {
            cr_indices.push_back(j_cr);
            // if belongs to denom, add 1 in n_denom
            if (i == 1) ++n_denom;
         } else {
            for (Int_t l = 0; l < (Int_t)tmp_indices.size(); ++l)
               cr_indices.push_back(tmp_indices[l]);
            // if belongs to denom, add 1 in n_denom
            if (i == 1) n_denom += tmp_indices.size();
         }
      }
   }
   return true;
}

//______________________________________________________________________________
Bool_t TUCRList::ComboToCRNames(string const &combo, vector<string> &cr_names,
                                Int_t &n_denom, Bool_t is_verbose, FILE *f) const
{
   //--- Extracts vector of all CR names (and number of CRs in denominator if
   //    ratio) in the quantity "combo" (returns false if combo ill-defined).
   // INPUTS:
   //  combo             Name to convert in a vector of CR indices
   //  f                 Output file for chatter
   //  is_verbose        Chatter on or off (to print on screen isotopes found)
   // OUTPUTS:
   //  cr_names          Vector of CR names
   //  n_denom           Number of CRs in denominator (0 if not a ratio)

   // Force initialisation of output variables
   cr_names.clear();
   n_denom = 0;

   vector<Int_t> indices;
   Bool_t is_exist = ComboToCRIndices(combo, indices, n_denom, is_verbose, f);

   for (Int_t i = 0; i < (Int_t)indices.size(); ++i)
      cr_names.push_back(fCRs[indices[i]].GetName());

   return is_exist;
}

//______________________________________________________________________________
void TUCRList::ComboToElementNames(string const &combo, vector<string> &element_names) const
{
   //--- Extracts vector of element names appearing in "combo" (return false if
   //    combo ill-defined). For instance, if quantity = Li+9Be/C, return Li, C
   //    (remove CRs and duplicate elements).
   // INPUT:
   //  combo             Name to convert in a vector of CR names
   // OUTPUT:
   //  element_names     Vector of element names

   // Force initialisation of output variables
   element_names.clear();

   // Extract the numerator and denominator (if ratio), otherwise,
   // this is the quantity under examination!
   vector<string> num_den;
   TUMisc::String2List(combo, "/", num_den);

   // Loop on num and denum
   for (Int_t i = 0; i < (Int_t)num_den.size(); ++i) {
      // Check if this is a sum of elements and adds up all element names...
      vector<string> list;
      TUMisc::String2List(num_den[i], "+", list);

      // Loop on all quantities in sum
      for (Int_t k = 0; k < (Int_t)list.size(); k++) {
         if (IsElementAndInList(list[k]))
            element_names.push_back(list[k]);
      }
   }
   TUMisc::RemoveDuplicates(element_names);
}

//______________________________________________________________________________
void TUCRList::CombosToCRNames(vector<string> &list_combos, vector<string> &cr_names) const
{
   //--- Extracts from a list of combos all CR names (no duplicates,
   //    sorted by growing Z).
   // INPUT:
   //  list_combos       List of names from which to extract CR names
   // OUTPUT:
   //  cr_names          Vector of CR names

   cr_names.clear();
   for (Int_t i = 0; i < (Int_t)list_combos.size(); ++i) {
      Int_t dummy = 0;
      vector<string> names;
      ComboToCRNames(list_combos[i], names, dummy, false);
      for (Int_t j = 0; j < (Int_t)names.size(); ++j) {
         cr_names.push_back(names[j]);
      }
   }

   TUMisc::RemoveDuplicates(cr_names);
   SortByGrowingZ(cr_names);
}

//______________________________________________________________________________
void TUCRList::CombosToElementAndCRNames(vector<string> &list_combos, vector<string> &all_names) const
{
   //--- Extracts from a list of combos the element and cr names
   //   (no duplicates, elements followed by isotopes).
   // INPUT:
   //  list_combos       List of names from which to extract names
   // OUTPUT:
   //  all_names         Vector of all names

   all_names.clear();
   CombosToElementNames(list_combos, all_names);
   vector<string> cr_names;
   CombosToCRNames(list_combos, cr_names);

   all_names.insert(all_names.end(), cr_names.begin(), cr_names.end());
   SortByGrowingZ(all_names);
}

//______________________________________________________________________________
void TUCRList::CombosToElementNames(vector<string> &list_combos, vector<string> &element_names) const
{
   //--- Extracts from a list of combos the element names (no duplicates,
   //    sorted by growing Z).
   // INPUT:
   //  list_combos       List of names from which to extract element names
   // OUTPUT:
   //  element_names     Vector of element names

   element_names.clear();
   for (Int_t i = 0; i < (Int_t)list_combos.size(); ++i) {
      vector<string> names;
      ComboToElementNames(list_combos[i], names);
      for (Int_t j = 0; j < (Int_t)names.size(); ++j)
         element_names.push_back(names[j]);
   }
   TUMisc::RemoveDuplicates(element_names);
   SortByGrowingZ(element_names);
}

//______________________________________________________________________________
void TUCRList::Copy(TUCRList const &crs)
{
   //--- Copies CR list in current class.
   //  crs               Object to copy from

   Initialise(true);

   // Copy file from which properties are read
   fFileName = crs.GetFileName();
   fIsGhosts = crs.IsGhostsLoaded();
   fIDList = crs.GetIDList();

   // Set atom properties (from file used to fill that class)
   TUAtomProperties::Copy(crs);

   // Copy CRs and parents: two steps are required
   //  1. List of CRs
   //  2. Parents (if required)
   // Loop on CRs
   for (Int_t j = 0; j < crs.GetNCRs(); ++j) {
      // Fill CR properties
      fCRs.push_back(crs.fCRs[j]);

      // Fill BETA and EC indices
      fIndexParentBETA.push_back(crs.IndexParentBETA(j));
      fIndexParentEC.push_back(crs.IndexParentEC(j));

      // Copy SS abundances
      fCRs[j].SetSSIsotopeAbundance(crs.fCRs[j].GetSSIsotopeAbundance());
      fCRs[j].SetSSIsotopicFraction(crs.fCRs[j].GetSSIsotopicFraction());
   }
   // Fill lists of BETA and EC-unstable indices
   fMapBETAIndex2CRIndex.clear();
   for (Int_t j = 0; j < crs.GetNUnstables(true); ++j) {
      pair<Int_t, Int_t> tmp;
      tmp.first = crs.IndexDecayParent(j, true);
      tmp.second = crs.IndexDecayDaughter(j, true);
      fMapBETAIndex2CRIndex.push_back(tmp);
   }
   fMapECIndex2CRIndex.clear();
   for (Int_t j = 0; j < crs.GetNUnstables(false); ++j) {
      pair<Int_t, Int_t> tmp;
      tmp.first = crs.IndexDecayParent(j, false);
      tmp.second = crs.IndexDecayDaughter(j, false);
      fMapECIndex2CRIndex.push_back(tmp);
   }

   // Copy pure secondaries
   fNamePureSecondaries =  crs.GetNamesPureSecondaries(false);
   fIndicesPureSecondaries.clear();
   for (Int_t j = 0; j < crs.GetNPureSecondaries(); ++j) {
      fIsPureSecondary.push_back(crs.IsPureSecondary(j));
      fIndicesPureSecondaries.push_back(crs.IndexPureSecondary(j));
   }


   // Copy useful indices
   fIndicesAntinuclei.clear();
   for (Int_t i = 0; i < crs.GetNLeptons(); ++i)
      fIndicesAntinuclei.push_back(crs.IndexAntinucleus(i));
   fIndicesLeptons.clear();
   for (Int_t i = 0; i < crs.GetNLeptons(); ++i)
      fIndicesLeptons.push_back(crs.IndexLepton(i));
   fIndicesNuclei.clear();
   for (Int_t i = 0; i < crs.GetNNuclei(); ++i)
      fIndicesNuclei.push_back(crs.IndexNucleus(i));

   // If more than just a list of CRs
   AllocateParentsAndFragments();
   for (Int_t j = 0; j < crs.GetNCRs(); ++j) {
      // Fill list of parents
      fIndicesParents[j].clear();
      for (Int_t p = 0; p < crs.GetNParents(j); ++p)
         fIndicesParents[j].push_back(crs.IndexInCRList_Parent(j, p));

      // Fill list of fragments
      fIndicesFragments[j].clear();
      for (Int_t f = 0; f < crs.GetNFragments(j); ++f)
         fIndicesFragments[j].push_back(crs.IndexInCRList_Fragment(j, f));

   }

   // Copy free pars
   fIsDeleteFreePars = crs.IsDeleteFreePars();
   if (fIsDeleteFreePars) {
      TUFreeParList *pars = crs.GetFreePars();
      if (pars)
         fFreePars = pars->Clone();
      pars = NULL;
   } else
      fFreePars = crs.GetFreePars();
}

//______________________________________________________________________________
Bool_t TUCRList::ElementNameToCRIndices(string const &element, vector<Int_t> &cr_indices,
                                        Bool_t is_verbose, FILE *f) const
{
   //--- Extracts a list of indices of all isotopes associated to a given element
   //    name. For instance, element "H" corresponds to isotopes "1H" and "2H"
   //    (if and only if these isotopes belong to fCRs). Returns false if element
   //    does not exist.
   //  element           Name of element
   //  is_verbose        Chatter on or off (to print on screen isotopes found)
   //  f                 Output file for chatter

   // Seeks for the element charge
   Int_t tmp_z = ElementNameToZ(element, false);

   if (tmp_z == 0) {
      if (is_verbose) {
         string message = "Element " + element + " you asked for is not in the list of CRs";
         TUMessages::Warning(f, "TUCRList", "ElementNameToCRIndices", message);
      }
      return false;
   }

   //string indent = TUMessages::Indent(true);
   if (is_verbose)
      fprintf(f, "  Element %s is: ", element.c_str());
   for (Int_t j_cr = 0; j_cr < GetNCRs(); ++j_cr) {
      if (fCRs[j_cr].GetZ() == tmp_z) {
         string cr_name = fCRs[j_cr].GetName();
         if (cr_name == "ELECTRON" || cr_name == "POSITRON")
            continue;
         cr_indices.push_back(j_cr);

         if (is_verbose)
            fprintf(f, " %s", cr_name.c_str());
      }
   }
   if (is_verbose)
      fprintf(f, "\n");

   //TUMessages::Indent(false);
   return true;
}

//______________________________________________________________________________
Bool_t TUCRList::ElementNameToCRNames(string const &element, vector<string> &cr_names,
                                      Bool_t is_verbose, FILE *f) const
{
   //--- Extracts a list of names of all isotopes associated to a given element
   //    name. For instance, element "H" corresponds to isotopes "1H" and "2H"
   //    (if and only if these isotopes belong to fCRs). Returns false if element
   //    does not exist.
   // INPUT:
   //  element           Name of element
   //  is_verbose        Chatter on or off (to print on screen isotopes found)
   //  f                 Output file for chatter
   // OUTPUT:
   //  cr_names          Vector of CR names

   vector<Int_t> indices;
   Bool_t is_exist = ElementNameToCRIndices(element, indices, is_verbose, f);

   for (Int_t i = 0; i < (Int_t)indices.size(); ++i)
      cr_names.push_back(fCRs[indices[i]].GetName());

   return is_exist;
}

//______________________________________________________________________________
vector<Int_t> TUCRList::ExtractAllCRs(Bool_t is_only_nuc, Int_t jcr_min, Int_t jcr_max) const
{
   //--- Returns a list of all CR indices found in fCRs.
   //  is_only_nuc       If true, keep in list only nuc
   //  jcr_min           Minimal CR index allowed in list
   //  jcr_max           Maximal CR index allowed in list

   vector<Int_t> list;
   for (Int_t j_cr = 0; j_cr < GetNCRs(); ++j_cr) {
      Int_t new_z = fCRs[j_cr].GetZ();
      if ((is_only_nuc && new_z <= 0) || j_cr < jcr_min || j_cr > jcr_max)
         continue;
      list.push_back(j_cr);
   }
   return list;
}

//______________________________________________________________________________
vector<string> TUCRList::ExtractAllElements() const
{
   //--- Returns a list of all element names found in fCRs.

   vector<string> list;
   for (Int_t j_cr = 0; j_cr < GetNCRs(); ++j_cr) {
      string new_el = ZToElementName(fCRs[j_cr].GetZ());
      // Check that it is not already in the list of elements
      Bool_t is_new = true;
      for (Int_t k = 0; k < (Int_t)list.size(); ++k)
         if (new_el == list[k]) is_new = false;

      if (is_new) list.push_back(new_el);
   }
   return list;
}

//______________________________________________________________________________
vector<Int_t> TUCRList::ExtractAllZ(Bool_t is_only_nuc, Int_t z_min, Int_t z_max) const
{
   //--- Returns a list of all Z values found in fCRs.
   //  is_only_nuc       If true, keep in list only nuc
   //  z_min             Minimal Z allowed in list
   //  z_max             Maximal Z allowed in list

   vector<Int_t> list;
   for (Int_t j_cr = 0; j_cr < GetNCRs(); ++j_cr) {
      Int_t new_z = fCRs[j_cr].GetZ();
      if (is_only_nuc && (new_z < 0 || fCRs[j_cr].Getmamu() < 1))
         continue;
      // Check that it is not already in the list of elements
      Bool_t is_new = true;
      for (Int_t k = 0; k < (Int_t)list.size(); ++k)
         if (new_z == list[k]) is_new = false;

      if (is_new && new_z >= z_min && new_z <= z_max)
         list.push_back(new_z);
   }

   // And sort list (CRs are not always in growing Z!)
   std::sort(list.begin(), list.end());

   return list;
}

//______________________________________________________________________________
vector<string> TUCRList::ExtractCombo(string const &commasep_list, Bool_t is_verbose, FILE *f) const
{
   //--- Extracts from a comma-separated list combo a list of combos, and check that
   //    quantities in combo are valid.
   //  commasep_list     Comma-separated list of combos to check (e.g, B.C,1H-BAR,<LNA>)
   //  is_verbose        Chatter on or off
   //  f                 Output file for chatter

   vector<string> list_combos;
   TUMisc::String2List(commasep_list, ",", list_combos);
   Bool_t is_good = true;

   // Check combos in list
   for (Int_t i = 0; i < (Int_t)list_combos.size(); i++) {
      vector<Int_t> l_test;
      Int_t n_test = 0;
      TUMisc::UpperCase(list_combos[i]);
      if (list_combos[i] != "<LNA>" && list_combos[i] != "ALLSPECTRUM")
         is_good = (is_good && ComboToCRIndices(list_combos[i], l_test, n_test, is_verbose, f));

      if (!is_good) {
         list_combos.erase(list_combos.begin() + i);
         --i;
      }
   }
   return list_combos;
}

//______________________________________________________________________________
void TUCRList::ExtractComboEtypephiff(string const &combos_etypes_phiff,
                                      vector<string> &list_combo, vector<gENUM_ETYPE> &list_etype,
                                      vector<vector<Double_t> > &list_phiff, Bool_t is_verbose, FILE *f) const
{
   //--- Extracts from a comma-separated list combo/etype a list of combos and associated etypes,
   //    and check that quantities in combo are valid.
   // INPUTS:
   //  combos_etypes_phiff Semi-column/comma separated list of combos/e-types/phiff (e.g, B.C,1H-BAR:kEKN:0.6,0.8;B/C:kR;<LnA>:0.)
   //  is_verbose        Chatter on or off
   //  f                 Output file for chatter
   // OUTPUTS:
   //  list_combo        List of combo extracted (e.g., B/C, 1H-)
   //  list_etype        E-type associated to each list_combo[i]
   //  list_phi          Vector of phiFF values associated to each list_combo[i]


   list_combo.clear();
   list_etype.clear();
   list_phiff.clear();

   // Find all sets of "qty:etype:phi_FF" demanded (semi-colon separated)
   vector<string> list;
   TUMisc::String2List(combos_etypes_phiff, ";", list);

   // Loop on the quantities
   for (Int_t i = 0; i < (Int_t)list.size(); ++i) {
      // Each combo is formatted as cr1,cr2...:etype:pi_FF
      vector<string> qtiestypephi;
      TUMisc::String2List(list[i], ":", qtiestypephi);
      if (qtiestypephi.size() != 3 && is_verbose) {
         string message = "Expected format in \'qtiestypephi\' is  \'cr1,cr2...:etype:commasep_phiff\', but extracted " + list[i];
         TUMessages::Error(f, "TUCRList", "ExtractComboEtypephiff", message);
      }

      // E-type for this list of combos
      gENUM_ETYPE e_type;
      TUEnum::Name2Enum(qtiestypephi[1], e_type);

      // phi_FF for this list of combos
      vector<Double_t> tmp_phiff;
      TUMisc::String2List(qtiestypephi[2], ",", tmp_phiff);

      // Extract list of combos
      vector<string> l_combos = ExtractCombo(qtiestypephi[0], is_verbose, f);
      for (Int_t j = 0; j < (Int_t)l_combos.size(); ++j) {
         Int_t i_list = TUMisc::IndexInList(list_combo, l_combos[j], false);
         if (i_list >= 0 && e_type == list_etype[i_list])
            continue;
         // If not already in list (matching combo and etype), add
         list_combo.push_back(l_combos[j]);
         list_etype.push_back(e_type);
         list_phiff.push_back(tmp_phiff);
      }

   }
}

//______________________________________________________________________________
string TUCRList::ExtractCRNames(vector<Int_t> &indices, char sep) const
{
   //--- Returns string of comma-separated list of names from indices.
   //  indices           List of CR indices
   //  sep               Separator to use

   string list = "";
   for (Int_t i = 0; i < (Int_t)indices.size(); ++i) {
      if (indices[i] >= 0 && indices[i] < GetNCRs()) {
         if (list == "")
            list = fCRs[indices[i]].GetName();
         else
            list = list + sep + fCRs[indices[i]].GetName();
      }
   }
   return list;
}

//______________________________________________________________________________
Int_t TUCRList::ExtractZMin() const
{
   //--- Returns the minimum charge Z in fCRs (not always the first
   //    entry as fCRs is filled with isotopes ordered by growing mass).

   Int_t z_min = 10000;
   for (Int_t j_cr = GetNCRs() - 1; j_cr >= 0; --j_cr)
      z_min = min(fCRs[j_cr].GetZ(), z_min);
   return z_min;
}

//______________________________________________________________________________
Int_t TUCRList::ExtractZMax() const
{
   //--- Returns the maximum charge Z in fCRs (not always the last entry as fCRs
   //    is filled with isotopes ordered by growing mass).

   Int_t z_max = -10000;
   for (Int_t j_cr = GetNCRs() - 1; j_cr >= 0; --j_cr)
      z_max = max(fCRs[j_cr].GetZ(), z_max);
   return z_max;
}

//______________________________________________________________________________
string TUCRList::GetNamesPureSecondaries(Bool_t is_thoseinlist) const
{
   //--- Returns name of pure secondaries.
   //  is_thoseinlist    returns those in list (true), or in fNamePureSecondaries (false)

   if (is_thoseinlist) {
      if (GetNPureSecondaries() == 0) return "";
      vector<string> list;
      for (Int_t j_pure = 0; j_pure < GetNPureSecondaries(); ++j_pure)
         list.push_back(fCRs[IndexPureSecondary(j_pure)].GetName());
      return TUMisc::List2String(list, ',');
   } else
      return fNamePureSecondaries;
}

//______________________________________________________________________________
Double_t TUCRList::SSElementAbundance(Int_t z) const
{
   //--- Returns (relative) element abundance.
   //  z                 Charge

   Double_t abundance = 0.;
   for (Int_t j_cr = 0; j_cr < GetNCRs(); ++j_cr) {
      if (fCRs[j_cr].GetZ() != z)
         continue;
      else
         abundance += fCRs[j_cr].GetSSIsotopeAbundance();
   }
   return abundance;
}

//______________________________________________________________________________
Int_t TUCRList::IndexInParentList(Int_t j_cr, string const &parent) const
{
   //--- Returns index (in the list of parents) of the parent for j_cr.
   //  j_cr              Index of CR fragment
   //  parent            Name of parent

   string par = parent;
   TUMisc::UpperCase(par);
   for (Int_t p = GetNParents(j_cr) - 1; p >= 0; --p) {
      if (fCRs[IndexInCRList_Parent(j_cr, p)].GetName() == par)
         return p;
   }
   return -1;
}

//______________________________________________________________________________
Int_t TUCRList::Index(string const &cr, Bool_t is_verbose, FILE *f_log, Bool_t is_abort) const
{
   //--- Returns index corresponding to cr. If (is_verbose=is_abort=0) and cr
   //    not found, returns -1 and keep quiet.
   //  cr                Name of CR
   //  is_verbose        Chatter on or off (if CR name not found)
   //  f_log             Log for USINE
   //  is_abort          Abort on or off (if CR name not found)

   string cr_up = cr;
   TUMisc::UpperCase(cr_up);
   // Search and convert if alternative name is used
   TUMisc::FormatCRQtyName(cr_up);

   for (Int_t j_cr = GetNCRs() - 1; j_cr >= 0; --j_cr) {
      if (fCRs[j_cr].GetName() == cr_up)
         return j_cr;
   }

   if (is_abort) {
      string message = "could not find in list CR with name " + cr;
      TUMessages::Error(f_log, "TUCRList", "IndexOfCR", message);
   }

   if (is_verbose) {
      string message = "could not find in list CR with name " + cr;
      TUMessages::Warning(f_log, "TUCRList", "IndexOfCR", message);
   }
   return (-1);
}

//______________________________________________________________________________
void TUCRList::Indices(string commasep_list_crs, vector<Int_t> &cr_indices, Bool_t is_inlist) const
{
   //--- Extracts indices of CRs in [or not in] list (no duplicates).
   // INPUT:
   //  commasep_list_crs List of names from which to extract element names
   //  is_inlist         If false, form list of CRs not in commasep_list_crs
   // OUTPUT:
   //  cr_indices        List of CR indices

   vector<string> l_crs;
   TUMisc::String2List(commasep_list_crs, ",", l_crs);
   TUMisc::RemoveDuplicates(l_crs);

   cr_indices.clear();
   if (is_inlist) {  // Find CR in comma-sep list
      for (Int_t i = 0; i < (Int_t)l_crs.size(); ++i) {
         Int_t j_cr = Index(l_crs[i], false, stdout);
         if (j_cr >= 0)
            cr_indices.push_back(j_cr);
      }
   } else {   // Find CR not in comma-sep list
      // Find list of indices
      vector<Int_t> cr_in;
      Indices(commasep_list_crs, cr_in, true);
      for (Int_t i = 0; i < GetNCRs(); ++i) {
         Bool_t is_in = false;
         for (Int_t j = 0; j < (Int_t)cr_in.size(); ++j) {
            if (i == cr_in[j]) {
               is_in = true;
               break;
            }
         }
         if (!is_in)
            cr_indices.push_back(i);
      }
   }
}

//______________________________________________________________________________
void TUCRList::IndicesForGhost(string ghost_name, vector<Int_t> &cr_indices, vector<Int_t> &ghost_indices) const
{
   //--- Extracts indices of CRs having as ghost 'ghost_name' (because of possibly different
   //    decay channels, several CRs can have the same ghost, though with a different branching
   //    ratio).
   // INPUT:
   //  ghost_name        Ghost to search for
   // OUTPUT:
   //  cr_indices        List of CR indices
   //  ghost_indices     List of ghost indices (for corresponding CR index)

   cr_indices.clear();
   ghost_indices.clear();

   if (!IsGhostsLoaded())
      return;

   TUMisc::UpperCase(ghost_name);
   // N.B.: we stop at NCR-2, because the heaviest CR is a primary
   // and no ghost can be associatd to it.
   for (Int_t j_cr = 0; j_cr < GetNCRs() - 1; ++j_cr) {
      for (Int_t g = 0; g < fCRs[j_cr].GetNGhosts(); ++ g) {
         if (fCRs[j_cr].GetGhost(g).GetName() == ghost_name) {
            cr_indices.push_back(j_cr);
            ghost_indices.push_back(g);
         }
      }
   }
}

//______________________________________________________________________________
void TUCRList::Initialise(Bool_t is_delete)
{
   //--- Initialises and frees memory of object members (if is_delete=true).
   //  is_delete         Whether to free memory of all dynamic data members or not

   if (is_delete) {
      if (fIndicesFragments)
         delete[] fIndicesFragments;
      if (fIndicesParents)
         delete[] fIndicesParents;
      if (fIsDeleteFreePars && fFreePars)
         delete fFreePars;
   }
   TUAtomProperties::Initialise();

   fCRs.clear();
   fFileName = "";
   fFreePars = NULL;
   fIndexParentBETA.clear();
   fIndexParentEC.clear();
   fIndicesFragments = NULL;
   fIndicesParents = NULL;
   fIndicesAntinuclei.clear();
   fIndicesLeptons.clear();
   fIndicesNuclei.clear();
   fIndicesPureSecondaries.clear();
   fIsDeleteFreePars = true;
   fIsGhosts = false;
   fIsPureSecondary.clear();
   fMapBETAIndex2CRIndex.clear();
   fMapECIndex2CRIndex.clear();
   fIDList =  "";
   fNamePureSecondaries = "";
   fParsHalfLife_CRIndices.clear();
   fParsHalfLife_IsBETAOrEC.clear();
   fParsHalfLife_RefVals.clear();
}

//______________________________________________________________________________
Bool_t TUCRList::IsIndex(Int_t j_cr, FILE *f) const
{
   //--- Checks whether j_cr exists in fCRs (if not, returns false + warning message).
   //  j_cr              Index of CR
   //  f                 Output file for chatter

   if ((j_cr >= 0) && (j_cr < GetNCRs()))
      return true;

   // if not in list
   string message = Form("index %d you asked for is not in the list", j_cr);
   TUMessages::Warning(f, "TUCRList", "IsIndex", message);
   return false;
}

//______________________________________________________________________________
Bool_t TUCRList::IsIndex(Int_t j_cr, Int_t j_proj, FILE *f) const
{
   //--- Checks j_cr (in fCRs) and p-th parent of j_cr exist in fIndicesParents.
   //  j_cr              Index of CR (in list of CRs)
   //  j_proj            Index of parent (in list of parents)
   //  f                 Output file for chatter

   if (IsIndex(j_cr)) {
      if ((j_proj >= 0) && (j_proj < GetNParents(j_cr)))
         return true;
      else {
         string message = Form("index %d for CR %d is not in the list", j_proj, j_cr);
         TUMessages::Warning(f, "TUCRList", "IsIndex", message);
         return false;
      }
   } else
      return false;
}

//______________________________________________________________________________
Bool_t TUCRList::IsElementAndInList(string const &qty) const
{
   //--- Checks if "element" is in list of CRs
   //  quantity          Name of quantity to check

   // Check first if this is an element
   if (!TUAtomElements::IsElement(qty))
      return false;

   vector<string> cr_names;
   ElementNameToCRNames(qty, cr_names, false);
   if (cr_names.size() == 0)
      return false;
   else
      return true;
}

//______________________________________________________________________________
void TUCRList::LoadSSAbundances(string const &file_ssabund)
{
   //--- Sets Solar System abundances (relative and isotopic fraction).
   //  file_ssabund      File of Solar system abundances

   // If input file exist, read it (else abort)
   ifstream f_read(file_ssabund.c_str());
   TUMisc::IsFileExist(f_read, file_ssabund);

   // Read all lines
   vector<string> names;
   vector<Double_t> fracs;
   vector<Double_t> abunds;

   string line;
   while (getline(f_read, line)) {
      // Skip comments (start with '#') and empty lines
      string tmp = TUMisc::RemoveBlancksFromStartStop(line);
      if (tmp[0] == '#' || tmp.size() == 0) continue;

      vector<string> list;
      TUMisc::String2List(tmp, " \t", list);
      if (list[0] == "-->") continue;
      TUMisc::UpperCase(list[0]);
      names.push_back(list[0]);
      fracs.push_back(atof(list[1].c_str()));
      abunds.push_back(atof(list[2].c_str()));
   }

   // Loop on all CRs in list and find isotopic SS abundance
   for (Int_t j_cr = 0; j_cr < GetNCRs(); ++j_cr) {
      // Initialise
      fCRs[j_cr].SetSSIsotopeAbundance(0.);
      fCRs[j_cr].SetSSIsotopicFraction(0.);

      if (fCRs[j_cr].GetFamily() == kLEPTON)
         continue;

      // Find index of line containing information
      // N.B.: given the file format, SS isotopic fraction
      // is obtained by dividing read abundance by elemental
      // abundance.
      string crname = fCRs[j_cr].GetName();
      Int_t i_line = TUMisc::IndexInList(names, crname, false);
      if (i_line >= 0)
         fCRs[j_cr].SetSSIsotopeAbundance(abunds[i_line]);
   }

   // Loop on all elements (and anti-elements) to set isotopic fraction
   for (Int_t z = ExtractZMin(); z <= ExtractZMax(); ++z) {
      if (z == 0) continue;
      string elem = TUAtomElements::ZToElementName(z);
      vector<Int_t> cr_indices;
      ElementNameToCRIndices(elem, cr_indices, false);
      // Calculate element abundance
      Double_t elem_abund = 0.;
      for (Int_t n = 0; n < (Int_t)cr_indices.size(); ++n)
         elem_abund += fCRs[cr_indices[n]].GetSSIsotopeAbundance();
      for (Int_t n = 0; n < (Int_t)cr_indices.size(); ++n) {
         Int_t j_cr = cr_indices[n];
         if (elem_abund > 1.e-50)
            fCRs[j_cr].SetSSIsotopicFraction(fCRs[j_cr].GetSSIsotopeAbundance() / elem_abund);
      }
   }
}

//______________________________________________________________________________
Bool_t TUCRList::MapIndices(TUCRList *newlist, vector<Int_t> &indices_newlist_inclasslist,
                            Bool_t is_verbose, FILE *f) const
{
   //--- Finds matching index and fills (in indices_in_new), for each CR in
   //    newlist, the corresponding index in this list (index set to -1 if CR in
   //    newlist not this list!). Returns true if all CR indices in newlist match
   //    those in the class list (it does not matter if the current list has more
   //    entries than newlist list).
   //  newlist                      List of CR to match
   //  indices_newlist_inclasslist  Indices of 'newlist' in class
   //  is_verbose                   If true, print resulting mapping
   //  f                            Output file for chatter

   indices_newlist_inclasslist.clear();

   string indent = TUMessages::Indent(true);
   Int_t is_same_list = true;

   if (is_verbose) {
      fprintf(f, "%s Map\n", indent.c_str());
      newlist->PrintList(f, false);
      fprintf(f, "%s into\n", indent.c_str());
      PrintList(f, false);
   }

   // Loop on all CRs in cr_list and find their matching index in current class
   for (Int_t i = 0; i < newlist->GetNCRs(); ++i) {
      Int_t index = Index(newlist->GetCREntry(i).GetName(), false, f);
      indices_newlist_inclasslist.push_back(index);
      if (i != index)
         is_same_list = false;

      if (is_verbose) {
         if (i == 0)
            fprintf(f, "%s => \n", indent.c_str());
         if (index >= 0)
            fprintf(f, "%s   [%-8s]  %2d -> %2d  [%s]\n", indent.c_str(),
                    newlist->GetCREntry(i).GetName().c_str(), i, indices_newlist_inclasslist[i],
                    GetCREntry(index).GetName().c_str());
         else
            fprintf(f, "%s   [%-8s]  %2d -> %2d  [NOT IN CURRENT LIST]\n", indent.c_str(),
                    newlist->GetCREntry(i).GetName().c_str(), i, indices_newlist_inclasslist[i]);
      }
   }
   TUMessages::Indent(false);
   return is_same_list;
}

//______________________________________________________________________________
TH1D *TUCRList::OrphanEmptyHistoOfCRs(string const &h_name, string const &title, vector<Int_t> const &list_jcr) const
{
   //--- Returns empty TH1D, the label of which are CR names in [cr_first,cr_last].
   //  histo_name         Name of ROOT histogram
   //  title              Title of ROOT histogram
   //  list_jcr           List of CR indices to use

   Int_t n_jcr = list_jcr.size();
   string cr_first = fCRs[list_jcr[0]].GetName();
   string cr_last = fCRs[list_jcr[n_jcr - 1]].GetName();

   // Add first and last CR in name (to have unique name)
   string h_full_name = h_name + "_" + cr_first + "to" + cr_last;
   TUMisc::RemoveSpecialChars(h_full_name);

   // create histo
   TH1D *h_i = new TH1D(h_full_name.c_str(), title.c_str(), n_jcr, list_jcr[0], list_jcr[n_jcr - 1]);
   h_i->SetStats(0);
   h_i->SetFillColor(kWhite);
   for (Int_t i = 0; i < n_jcr; i++)
      h_i->GetXaxis()->SetBinLabel(i + 1, GetNameROOT(list_jcr[i]).c_str());
   h_i->SetXTitle("Isotope");
   h_i->SetLabelSize(0.07);
   h_i->SetLabelOffset(0.01);
   h_i->GetXaxis()->CenterTitle();
   h_i->GetYaxis()->CenterTitle();

   return h_i;
}

//______________________________________________________________________________
TH1D *TUCRList::OrphanEmptyHistoOfCRs(string const &h_name, string const &title,
                                      string cr_first, string cr_last) const
{
   //--- Returns empty TH1D, the label of which are CR names in [cr_first,cr_last].
   //  histo_name         Name of ROOT histogram
   //  title              Title of ROOT histogram
   //  cr_first           First CR in histo label
   //  cr_last            Last CR in histo label

   TUMisc::UpperCase(cr_first);
   TUMisc::UpperCase(cr_last);

   // Find pos of the cr (if does not exist or does not belong
   // to the list, use default list!)
   Int_t first = 0;
   Int_t last = GetNCRs() - 1;
   if (cr_first != "DEFAULT") {
      first = Index(cr_first);
      if (first == -1 || first > last)
         first = 0;
   }
   if (cr_first != "DEFAULT") {
      last = Index(cr_last);
      if (last == -1 || first > last)
         last = GetNCRs() - 1;
   }
   cr_first = fCRs[first].GetName();
   cr_last = fCRs[last].GetName();

   vector<Int_t> list_jcr = ExtractAllCRs(false, first, last);
   return OrphanEmptyHistoOfCRs(h_name, title, list_jcr);
}

//______________________________________________________________________________
TH1D *TUCRList::OrphanEmptyHistoOfElements(string const &h_name, string const &title, vector<Int_t> const &list_z) const
{
   //--- Returns empty TH1D, whose labels are element names in list_z.
   // histo_name         Name of ROOT histogram
   // title              Title of ROOT histogram
   // list_z             List of Z to use

   Int_t n_z = list_z.size();
   Int_t z_min = list_z[0];
   Int_t z_max = list_z[n_z - 1];

   // Add first and last element in name (to have unique name)
   string h_full_name = h_name + "_" + ZToElementName(z_min) + "to" + ZToElementName(z_max);
   TUMisc::RemoveSpecialChars(h_full_name);

   // create histo
   TH1D *h_i = new TH1D(h_full_name.c_str(), title.c_str(), n_z, z_min, z_max);
   h_i->SetStats(0);

   // set
   h_i->SetFillColor(kWhite);
   for (Int_t i = 0; i < n_z; ++i)
      h_i->GetXaxis()->SetBinLabel(i + 1, ZToElementNameROOT(list_z[i]).c_str());

   h_i->SetXTitle("Element");
   h_i->SetLabelSize(0.07);
   h_i->SetLabelOffset(0.01);
   h_i->GetXaxis()->CenterTitle();
   h_i->GetYaxis()->CenterTitle();

   return h_i;
}

//______________________________________________________________________________
TH1D *TUCRList::OrphanEmptyHistoOfElements(string const &h_name, string const &title,
      string element_first, string element_last) const
{
   //--- Returns empty TH1D, whose labels are element names [element_first,element_last].
   // histo_name         Name of ROOT histogram
   // title              Title of ROOT histogram
   // element_first      First element in histo label
   // element_last       Last CR element histo label

   TUMisc::UpperCase(element_first);
   TUMisc::UpperCase(element_last);
   Int_t z_min = 0;
   Int_t z_max = 0;
   // Extract/check first element
   if (element_first == "DEFAULT") {
      // Default value for z_min (first (anti)-nucleus in list skipping leptons)
      for (Int_t j_cr = 0; j_cr < GetNCRs(); ++j_cr) {
         if (fCRs[j_cr].Getmamu() >= 1.0) {
            z_min = fCRs[j_cr].GetZ();
            break;
         }
      }
   } else {
      z_min = ElementNameToZ(element_first, true);
      // If Z==0, search for first non-negative value
      if (z_min == 0) {
         for (Int_t j_cr = 0; j_cr < GetNCRs(); ++j_cr) {
            if (fCRs[j_cr].Getmamu() >= 1.0 && fCRs[j_cr].GetZ() > 0) {
               z_min = fCRs[j_cr].GetZ();
               break;
            }
         }
      }
   }
   // Extract/check last element
   if (element_last == "DEFAULT") {
      // Default value for z_max (last in list)
      z_max = fCRs[GetNCRs() - 1].GetZ();
   } else {
      z_max = ElementNameToZ(element_last, true);
      if (z_max <= 0 || z_min > z_max)
         z_max = fCRs[GetNCRs() - 1].GetZ();
   }

   // Extract list of Z
   vector<Int_t> list_z = ExtractAllZ(false, z_min, z_max);
   return OrphanEmptyHistoOfElements(h_name, title, list_z);
}

//______________________________________________________________________________
void TUCRList::ParsHalfLife_AddPar(TUFreeParList *pars, Int_t i_par, Bool_t is_use_or_copy, FILE *f_log)
{
   //--- Checks whether pars contain valid half-life parameters, and add
   //    in fParsHalfLife_CRIndices, fParsHalfLife_IsBETAOrEC, and fFreePars.
   // INPUTS:
   //  pars              Free parameters
   //  i_par             Index of parameter to check (in 'pars')
   //  is_use_or_copy    Whether to use or copy CR-related pars in fFreePars
   //  f_log             Log for USINE

   // Parameter for half-life should be formatted as "HalfLife_CR"
   string par_name = pars->GetParEntry(i_par)->GetName();
   vector<string> par_split;
   TUMisc::String2List(par_name, "_", par_split);
   // If format not correct, return
   if (par_split.size() != 2)
      return;

   // Check if CR name exists
   Int_t i_cr = Index(par_split[1]);
   if (i_cr < 0) {
      string message = Form("%s is not a valid CR, cannot add CR parameter %s", par_split[1].c_str(), par_name.c_str());
      TUMessages::Warning(f_log, "TUCRList", "ParsHalfLife_AddPar", message);
      return;
   }

   // Find if HALFLIFE (if something else, return)
   TUMisc::UpperCase(par_split[0]);
   Bool_t is_beta_or_ec = false;
   Double_t ref_halflife = 0.;
   if (par_split[0].substr(0, 8) == "HALFLIFE") {
      // Check if CR is unstable
      if (!fCRs[i_cr].IsUnstable()) {
         string message = Form("%s is not an unstable CR, cannot add CR parameter %s", par_split[1].c_str(), par_name.c_str());
         TUMessages::Warning(f_log, "TUCRList", "ParsHalfLife_AddPar", message);
      } else {
         if (par_split[0].substr(8, 4) == "BETA") {
            is_beta_or_ec = true;
            ref_halflife = fCRs[i_cr].GetBETAHalfLife();
         } else if (par_split[0].substr(8, 2) == "EC") {
            is_beta_or_ec = false;
            ref_halflife = fCRs[i_cr].GetECHalfLife();
         } else {
            string message = Form("%s is not a valid keyword, should be \'HalfLifeBETA\' or \'HalfLifeEC\', cannot add CR parameter %s", par_split[1].c_str(), par_name.c_str());
            TUMessages::Warning(f_log, "TUCRList", "ParsHalfLife_AddPar", message);
         }
      }
   } else
      return;

   // Find if already in list of parameters
   if (fFreePars->IndexPar(par_name) >= 0) {
      string message = Form("%s is already in list of CR parameters, skip it", par_name.c_str());
      TUMessages::Warning(f_log, "TUCRList", "ParsHalfLife_AddPar", message);
      return;
   }

   // Add as new parameter
   fFreePars->AddPar(pars->GetParEntry(i_par), is_use_or_copy);
   fParsHalfLife_CRIndices.push_back(i_cr);
   fParsHalfLife_IsBETAOrEC.push_back(is_beta_or_ec);
   fParsHalfLife_RefVals.push_back(ref_halflife);

   return;
}

//______________________________________________________________________________
void TUCRList::ParsHalfLife_AddPars(TUFreeParList *pars, Int_t p_first, Int_t p_last, Bool_t is_use_or_copy, Bool_t is_verbose, FILE *f_log)
{
   //--- Fill fParHalfLife_... parameters from 'pars', and use/copy CR-related (of pars) in fFreePars.
   //  pars              List of free parameters in which CR-related parameters are sought
   //  p_first           First parameter in pars to add
   //  p_last            Last parameter in pars to add
   //  is_use_or_copy    Whether to use or copy CR-related pars in fFreePars
   //  is_verbose        Verbose or not when class is set
   //  f_log             Log for USINE

   string indent = TUMessages::Indent(true);
   if (is_verbose)
      fprintf(f_log, "%s[TUCRList::ParsHalfLife_AddPars]\n", indent.c_str());

   // Initialise quantities used if CR free pars
   fParsHalfLife_CRIndices.clear();
   fParsHalfLife_IsBETAOrEC.clear();
   fParsHalfLife_RefVals.clear();

   // Reset fFreePars
   if (fIsDeleteFreePars && fFreePars)
      delete fFreePars;
   fFreePars = NULL;
   fIsDeleteFreePars = is_use_or_copy;

   // If no free pars, nothing to do
   if (!pars || pars->GetNPars() == 0) {
      string message = "pars is empty, nothing to do!";
      TUMessages::Warning(f_log, "TUCRList", "ParsHalfLife_AddPars", message);
      fprintf(f_log, "%s[TUCRList::ParsHalfLife_AddPars] <DONE>\n\n", indent.c_str());
      TUMessages::Indent(false);
      return;
   }

   // Loop on pars, and in fParsHalfLife_ReacIndices and fFreePars if valid reaction/parameter
   fFreePars = new TUFreeParList();
   for (Int_t p = max(0, p_first); p < min(p_last + 1, pars->GetNPars()); ++p)
      ParsHalfLife_AddPar(pars, p, is_use_or_copy, f_log);

   // If no parameter found, nothing to do
   if (fFreePars->GetNPars() == 0) {
      string message = "No valid CR parameter found in pars, nothing to do!";
      TUMessages::Warning(f_log, "TUCRList", "ParsHalfLife_AddPars", message);
      fprintf(f_log, "%s[TUCRList::ParsHalfLife_AddPars] <DONE>\n\n", indent.c_str());
      TUMessages::Indent(false);
      return;
   }

   if (is_verbose)
      fprintf(f_log, "%s[TUCRList::ParsHalfLife_AddPars] <DONE>\n\n", indent.c_str());
   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUCRList::ParsHalfLife_Print(FILE *f, Int_t p_halflife) const
{
   //--- Prints parameter p_halflife (name and value).
   //  f                 File in which to print

   // Print free par value
   if (p_halflife >= 0 && p_halflife < ParsHalfLife_GetN()) {
      Int_t j_cr = fParsHalfLife_CRIndices[p_halflife];
      Double_t halflife = 0.;
      string channel = "";
      if (fParsHalfLife_IsBETAOrEC[p_halflife]) {
         channel = "BETA";
         halflife = fCRs[j_cr].GetBETAHalfLife();
      } else {
         channel = "EC";
         halflife = fCRs[j_cr].GetECHalfLife();
      }
      fprintf(f, "  %5s  %4s half-life:     ref=%le [Myr]   current=%le [Myr]\n",
              fCRs[j_cr].GetName().c_str(), channel.c_str(), fParsHalfLife_RefVals[p_halflife], halflife);
   }
}

//______________________________________________________________________________
void TUCRList::ParsHalfLife_UpdateFromFreeParsAndResetStatus()
{
   //--- Updates all relevant XS from updated fFreePars values and reset fFreePars status.

   if (ParsHalfLife_GetN() == 0)
      return;

   // If status of parameters has changed
   if (fFreePars && fFreePars->GetParListStatus()) {
      // Loop on half-lives
      for (Int_t p_halflife = 0; p_halflife < ParsHalfLife_GetN(); ++p_halflife) {
         Double_t new_halflife = fFreePars->GetParEntry(p_halflife)->GetVal(false);
         Int_t j_cr = fParsHalfLife_CRIndices[p_halflife];
         UpdateHalfLife(j_cr, new_halflife, fParsHalfLife_IsBETAOrEC[p_halflife]);
      }
      fFreePars->ResetStatusParsAndParList();
   }
}

//______________________________________________________________________________
void TUCRList::PrintCharts(FILE *f, string crname_or_type) const
{
   //--- Prints in file f CR charts found in fCRs (for a given CR name or type).
   //  f                 File in which to print
   //  crname_or_type    CR name or type (STABLE, BETA, EC, MIX (-FED), or ALL)

   TUMessages::Separator(f, "CRs Charts");

   string indent = TUMessages::Indent(true);

   if (GetNCRs() == 0) {
      TUMessages::Error(f, "TUCRList", "PrintCharts", "Charts not filled!");
   }

   fprintf(f, "%sList of \"%s\"-type CR\n", indent.c_str(), crname_or_type.c_str());

   TUMisc::UpperCase(crname_or_type);
   fprintf(f, "%s      Z    A       Name       mass [GeV]    Rrms [fm]\n", indent.c_str());
   for (Int_t j_cr = 0; j_cr < GetNCRs(); ++j_cr) {
      string type2 = fCRs[j_cr].GetType();
      TUMisc::UpperCase(type2);
      if ((crname_or_type == "ALL") || (crname_or_type == type2)
            || (crname_or_type == "BETA" && type2 == "MIX")
            || (crname_or_type == "EC" && type2 == "MIX")
            || (crname_or_type == "BETA-FED" && type2 == "MIX-FED")
            || (crname_or_type == "EC-FED" && type2 == "MIX-FED")
            || (crname_or_type == "STABLE" && (type2 == "BETA-FED" ||
                  type2 == "EC-FED" || type2 == "MIX-FED")))
         fCRs[j_cr].Print(f);
   }
   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUCRList::PrintElements(FILE *f, Bool_t is_separator, Bool_t is_summary) const
{
   //--- Prints in file f element names for CRs in list.
   //  f                 File in which to print
   //  is_separator      Switch on or off separator
   //  is_summary        Print comma-separated list

   string indent = TUMessages::Indent(true);

   if (is_separator) {
      //TUMessages::Separator(stdout, "CRs List");
      fprintf(f, "%s Loaded from %s\n %s -> %d CRs in %s\n", indent.c_str(),
              fFileName.c_str(), indent.c_str(), GetNCRs(), fIDList.c_str());
   }

   vector<string> list = ExtractAllElements();
   if (!is_summary) {
      for (Int_t e = 0; e < (Int_t)list.size(); e++)
         fprintf(f, "%s  %3d %8s\n", indent.c_str(), e, list[e].c_str());
   } else
      fprintf(f, "%s  %8s\n", indent.c_str(), TUMisc::List2String(list, ',').c_str());

   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUCRList::PrintFragments(FILE *f, Int_t j_cr) const
{
   //--- Prints in file f fragments of j_cr.
   //  f                 File in which to print
   //  j_cr              CR index

   string indent = TUMessages::Indent(true);

   if (GetNCRs() == 0 || !fIndicesFragments)
      TUMessages::Error(f, "TUCRList", "PrintFragments", "fCRs or fIndicesFragments not filled!");

   // No fragments
   if (GetNFragments(j_cr) == 0)
      fprintf(f, "%s  -- %3d:  %8s (no fragments)\n",
              indent.c_str(), j_cr, fCRs[j_cr].GetName().c_str());

   // If only one parent
   else if (GetNFragments(j_cr) == 1)
      fprintf(f, "%s  -- %3d:  %8s (only one fragment: %7s)\n",
              indent.c_str(), j_cr, fCRs[j_cr].GetName().c_str(),
              fCRs[IndexInCRList_Fragment(j_cr, 0)].GetName().c_str());

   // If standard parents (all heavier CRs)
   else if (GetNFragments(j_cr) == (IndexInCRList_Fragment(j_cr, 0) - IndexInCRList_Fragment(j_cr, GetNFragments(j_cr) - 1) + 1))
      fprintf(f, "%s  -- %3d:  %8s (fragments: %7s -> %s)\n",
              indent.c_str(), j_cr, fCRs[j_cr].GetName().c_str(),
              fCRs[IndexInCRList_Fragment(j_cr, 0)].GetName().c_str(),
              fCRs[IndexInCRList_Fragment(j_cr, GetNFragments(j_cr) - 1)].GetName().c_str());

   // List of parents...
   else {
      fprintf(f, "%s  -- %3d:  %8s (fragments: ",
              indent.c_str(), j_cr, fCRs[j_cr].GetName().c_str());

      for (Int_t frag = 0; frag < GetNFragments(j_cr); ++frag) {
         string fragment = fCRs[IndexInCRList_Fragment(j_cr, frag)].GetName();
         if (frag == 0) fprintf(f, "%8s", fragment.c_str());
         else fprintf(f, "%s", fragment.c_str());
         if (frag < GetNFragments(j_cr) - 1)
            fprintf(f, ",");
      }
      fprintf(f, ")\n");
   }
   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUCRList::PrintFragments(FILE *f) const
{
   //--- Prints in file f fragments for each CR.
   //  f                 File in which to print

   if (GetNCRs() == 0 || !fIndicesFragments)
      TUMessages::Error(f, "TUCRList", "PrintFragments", "fCRs or fIndicesFragments not filled!");

   for (Int_t j_cr = 0; j_cr < GetNCRs(); ++j_cr)
      PrintFragments(f, j_cr);
}

//______________________________________________________________________________
void TUCRList::PrintGhosts(FILE *f, Int_t z_first, Int_t z_last, string type,
                           Double_t br_min, Double_t br_max) const
{
   //--- Prints in file f list of 'ghost' nuclei for each CR (ghosts are short
   //    lived nuclei used to calculate nuclear production cross sections).
   //    Default for all CRs and ghosts is PrintGhosts().
   //  f                 File in which to print
   //  z_first           First CR charge for which to print ghosts
   //  z_last            Last CR charge for which to print ghosts
   //  Type              CR type: STABLE, BETA, EC, MIX, BETA-FED, EC-FED or MIX-FED
   //  br_min            Prints only ghosts for which the branching ratio > br_min
   //  br_max            Prints only ghosts for which the branching ratio < br_max

   if (!fIsGhosts) {
      fprintf(f, "Ghost nuclei not loaded in list => nothing to print\n");
      return;
   }

   // Check z_min and z_max OK
   Int_t z_min = ExtractZMin();
   Int_t z_max = ExtractZMax();
   if (z_first < z_min  ||  z_first > z_max) z_first = z_min;
   if (z_last > z_max  ||  z_last < z_min) z_last = z_max;

   TUMisc::UpperCase(type);

   // Check br_min and br_max OK
   if (br_min < 0.) br_min = 0.;
   if (br_max > 100.) br_max = 100.;

   fprintf(f, "List of \"%s\" ghost nuclei having Z=[%d-%d] and (%4.2f<Br<%4.2f) from \"%s\":\n",
           type.c_str(), z_first, z_last, br_min, br_max, fFileName.c_str());

   for (Int_t j_cr = 0; j_cr < GetNCRs() ; ++j_cr) {
      if (fCRs[j_cr].GetZ() < z_first) continue;
      else if (fCRs[j_cr].GetZ() > z_last) continue;

      if ((type == "ALL") || (fCRs[j_cr].GetType() == type)) {
         Int_t compt = 0;
         // Count n_ghosts satisfying "br_min<Br<br_max"
         for (Int_t g = 0; g < fCRs[j_cr].GetNGhosts(); g ++) {
            Double_t br = fCRs[j_cr].GetGhostBr(g);
            if (br >= br_min && br <= br_max) compt++;
         }
         fprintf(f, "     %-6s <= ", fCRs[j_cr].GetName().c_str());

         // Loop over Ghost list
         for (Int_t g = 0; g < fCRs[j_cr].GetNGhosts(); g ++) {
            Double_t br = fCRs[j_cr].GetGhostBr(g);
            if (br >= br_min && br <= br_max)
               fprintf(f, " %-4s(%2.1f%%)",
                       fCRs[j_cr].GetGhost(g).GetName().c_str(),
                       fCRs[j_cr].GetGhostBr(g));
         }
         fprintf(f, "\n");
      }
   }
   fprintf(f, "\n");
}

//______________________________________________________________________________
void TUCRList::PrintGhosts(FILE *f, Int_t j_cr) const
{
   //--- Prints in file f list of 'ghost' nuclei for j_cr (ghosts are short
   //    lived nuclei used to calculate nuclear production cross sections).
   //  f                 File in which to print
   //  j_cr              CR index

   if (!fIsGhosts) {
      fprintf(f, "Ghost nuclei not loaded in list => nothing to print\n");
      return;
   }

   fprintf(f, "Ghost for %s:", fCRs[j_cr].GetName().c_str());
   for (Int_t g = 0; g < fCRs[j_cr].GetNGhosts(); g ++)
      fprintf(f, " %-5s(%.1f%%)", fCRs[j_cr].GetGhost(g).GetName().c_str(),
              fCRs[j_cr].GetGhostBr(g));
   fprintf(f, "\n");
}

//______________________________________________________________________________
void TUCRList::PrintList(FILE *f, Bool_t is_separator, Bool_t is_summary) const
{
   //--- Prints in file f CR names of the class.
   //  f                 File in which to print
   //  is_separator      Switch on or off separator
   //  is_summary        Print comma-separated list

   string indent = TUMessages::Indent(true);

   if (is_separator) {
      //TUMessages::Separator(stdout, "CRs List");
      fprintf(f, "%s Loaded from %s\n %s -> %d CRs in %s\n", indent.c_str(),
              fFileName.c_str(), indent.c_str(), GetNCRs(), fIDList.c_str());
   }

   vector<string> list;
   for (Int_t j_cr = 0; j_cr < GetNCRs(); j_cr++) {
      if (!is_summary)
         fprintf(f, "%s  %3d %8s\n", indent.c_str(), j_cr, fCRs[j_cr].GetName().c_str());
      else
         list.push_back(fCRs[j_cr].GetName());
   }

   if (is_summary)
      fprintf(f, "%s  %8s\n", indent.c_str(), TUMisc::List2String(list, ',').c_str());

   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUCRList::PrintListForGhost(FILE *f, string ghost_name) const
{
   //--- Prints in file f CR names having 'ghost_name' (because of possibly different
   //    decay channels, several CRs can have the same ghost, though with a different
   //    branching ratio).
   //  f                 File in which to print
   //  ghost_name        Ghost to search for

   string indent = TUMessages::Indent(true);

   vector<Int_t> cr_indices, ghost_indices;
   IndicesForGhost(ghost_name, cr_indices, ghost_indices);
   string cr_names = ExtractCRNames(cr_indices);
   string g_indices = TUMisc::List2String(ghost_indices, ',');
   //TUMessages::Separator(stdout, "CRs List");
   fprintf(f, "%s %d CR (%s) found having ghost=%s (with ghost index %s)\n", indent.c_str(),
           (Int_t)cr_indices.size(), cr_names.c_str(), ghost_name.c_str(), g_indices.c_str());

   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUCRList::PrintParents(FILE *f, Int_t j_cr) const
{
   //--- Prints in file f parents of j_cr.
   //  f                 File in which to print
   //  j_cr              CR index

   string indent = TUMessages::Indent(true);

   if (GetNCRs() == 0)
      TUMessages::Error(f, "TUCRList", "PrintParents", "fCRs not filled!");

   // No parents (primary)
   if (GetNParents(j_cr) == 0)
      fprintf(f, "%s  -- %3d:  %8s (primary - no parents)\n",
              indent.c_str(), j_cr, fCRs[j_cr].GetName().c_str());

   // If only one parent
   else if (GetNParents(j_cr) == 1)
      fprintf(f, "%s  -- %3d:  %8s (only one parent: %7s)\n",
              indent.c_str(), j_cr, fCRs[j_cr].GetName().c_str(),
              fCRs[IndexInCRList_Parent(j_cr, 0)].GetName().c_str());

   // If standard parents (all heavier CRs)
   else if (GetNParents(j_cr) == (GetNCRs() - (j_cr + 1)))
      fprintf(f, "%s  -- %3d:  %8s (parents: %7s -> %s)\n",
              indent.c_str(), j_cr, fCRs[j_cr].GetName().c_str(),
              fCRs[IndexInCRList_Parent(j_cr, 0)].GetName().c_str(),
              fCRs[IndexInCRList_Parent(j_cr, GetNParents(j_cr) - 1)].GetName().c_str());

   // List of parents...
   else {
      fprintf(f, "%s  -- %3d:  %8s (parents: ",
              indent.c_str(), j_cr, fCRs[j_cr].GetName().c_str());

      for (Int_t p = 0; p < GetNParents(j_cr); ++p) {
         Int_t j_par_loc = IndexInCRList_Parent(j_cr, p);
         if (p == 0) fprintf(f, "%8s", fCRs[j_par_loc].GetName().c_str());
         else fprintf(f, "%s", fCRs[j_par_loc].GetName().c_str());
         if (p < GetNParents(j_cr) - 1)
            fprintf(f, ",");
      }
      fprintf(f, ")\n");
   }
   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUCRList::PrintParents(FILE *f) const
{
   //--- Prints in file f parents for each CR.
   //  f                 File in which to print

   for (Int_t j_cr = 0; j_cr < GetNCRs(); ++j_cr)
      PrintParents(f, j_cr);
}

//______________________________________________________________________________
void TUCRList::PrintPureSecondaries(FILE *f) const
{
   //--- Prints in file f CRs that are set to be pure secondaries.
   //  f                 File in which to print

   string indent = TUMessages::Indent(true);

   if (GetNCRs() == 0)
      TUMessages::Error(f, "TUCRList", "PrintPureSecondaries",
                        "fCRs or fIndicesParents not filled!");

   string list_found = GetNamesPureSecondaries(true);
   string list_init = GetNamesPureSecondaries(false);
   if (list_found == "") list_found = "NONE";

   fprintf(f, "%s   - Pure secondaries (found) in CR list: %s (from %s)\n",
           indent.c_str(), list_found.c_str(), list_init.c_str());

   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUCRList::PrintReactions(FILE *f, string const &cr_heavier, string const &cr_lighter, Bool_t is_print_az, Bool_t is_use_parentlist) const
{
   //--- Prints in file f list of all reactions (and ghosts) leading to CR
   // (from cr_start to cr_stop), and whose ghosts are not heavier than cr_stop.
   //  f                 File in which to print
   //  cr_heavier        Heavier CR for which to print reaction
   //  cr_lighter        Lighter CR for which to write reaction
   //  is_print_az       Does what it says...
   //  is_use_parentlist Use parent list or all possible reactions

   Int_t j_heavier = Index(cr_heavier, true, f, true);
   Int_t j_lighter = Index(cr_lighter, true, f, true);
   Int_t a_max = fCRs[j_heavier].GetA();

   // Loop on CRs as parents
   // Loop on CRs as fragments
   if (is_print_az)
      fprintf(f, "# NUC   A   Z  ->   NUC   A   Z [NUC-TYPE]\n");
   else
      fprintf(f, "# NUC -> NUC\t[NUC-TYPE]\n");


   vector<string> reacs, reacs2print;
   for (Int_t j = j_heavier; j >= j_lighter; --j) {
      // Check if j is a parent for j_cr
      Int_t jcr_max = j;
      if (is_use_parentlist)
         jcr_max = j_heavier;

      for (Int_t j_cr = jcr_max; j_cr >= j_lighter; --j_cr) {
         Int_t j_parent = IndexInParentList(j_cr, fCRs[j].GetName());
         if ((is_use_parentlist && j_parent < 0) || (!is_use_parentlist && j_cr > j))
            continue;

         if (is_use_parentlist || j_cr < j) {
            reacs.push_back(fCRs[j].GetName() + fCRs[j_cr].GetName());
            if (is_print_az)
               reacs2print.push_back(Form("%5s %3d %3d  -> %5s %3d %3d\t[CR_NUC]\n",
                                          fCRs[j].GetName().c_str(), fCRs[j].GetA(), fCRs[j].GetZ(),
                                          fCRs[j_cr].GetName().c_str(), fCRs[j_cr].GetA(), fCRs[j_cr].GetZ()));
            else
               reacs2print.push_back(Form("%s -> %s \t[CR]\n", fCRs[j].GetName().c_str(), fCRs[j_cr].GetName().c_str()));
         }
         // Loop on ghost for this CR
         for (Int_t g_ghost = fCRs[j_cr].GetNGhosts() - 1; g_ghost >= 0; --g_ghost) {
            // Check that ghost is not more massive than first
            if (fCRs[j_cr].GetGhost(g_ghost).GetA() > a_max
                  || fCRs[j_cr].GetGhost(g_ghost).GetA() > fCRs[j].GetA())
               continue;

            string reac = fCRs[j].GetName() + fCRs[j_cr].GetGhost(g_ghost).GetName();
            Int_t i_reac = TUMisc::IndexInList(reacs, reac, false);
            string ghost_br = Form("_%s@%.1f%%", fCRs[j_cr].GetName().c_str(),
                                   fCRs[j_cr].GetGhostBr(g_ghost));
            if (i_reac >= 0)
               reacs2print[i_reac].insert(Int_t(reacs2print[i_reac].size() - 2), ghost_br);
            else {
               reacs.push_back(reac);
               if (is_print_az)
                  reacs2print.push_back(Form("%5s %3d %3d  -> %5s %3d %3d\t[DECAY_TO%s]\n",
                                             fCRs[j].GetName().c_str(), fCRs[j].GetA(), fCRs[j].GetZ(), fCRs[j_cr].GetGhost(g_ghost).GetName().c_str(),
                                             fCRs[j_cr].GetGhost(g_ghost).GetA(), fCRs[j_cr].GetGhost(g_ghost).GetZ(), ghost_br.c_str()));
               else
                  reacs2print.push_back(Form("%s -> %s\t[DECAY_INTO%s]\n", fCRs[j].GetName().c_str(),
                                             fCRs[j_cr].GetGhost(g_ghost).GetName().c_str(), ghost_br.c_str()));
            }
         }
      }
   }

   // Print
   for (Int_t i = 0; i < (Int_t)reacs2print.size(); ++i)
      fprintf(f, "%s", reacs2print[i].c_str());
   fprintf(f, "\n");
}

//______________________________________________________________________________
void TUCRList::PrintSSIsotopeAbundances(FILE *f) const
{
   //--- Prints in file f Solar system isotope (relative) abundances.
   //  f                 File in which to print

   string indent = TUMessages::Indent(true);

   TUMessages::Separator(f, "Solar System isotope abundances");
   for (Int_t j_cr = 0; j_cr < GetNCRs(); j_cr++) {
      fprintf(f, "%s %3d %8s  %le\n", indent.c_str(), j_cr,
              fCRs[j_cr].GetName().c_str(), fCRs[j_cr].GetSSIsotopeAbundance());
   }

   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUCRList::PrintSSIsotopicFractions(FILE *f) const
{
   //--- Prints in file f Solar system isotopic fractions.
   //  f                 File in which to print

   string indent = TUMessages::Indent(true);

   TUMessages::Separator(f, "Solar System isotopic fractions");
   for (Int_t j_cr = 0; j_cr < GetNCRs(); j_cr++) {
      fprintf(f, "%s %3d %8s  %le\n", indent.c_str(), j_cr,
              fCRs[j_cr].GetName().c_str(), fCRs[j_cr].GetSSIsotopicFraction());
   }

   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUCRList::PrintSSIsotopicFractions(FILE *f, Int_t z) const
{
   //--- Prints in file f Solar system isotopic fractions for element z.
   //  f                 File in which to print
   //  z                 Charge
   string indent = TUMessages::Indent(true);

   vector<Int_t> cr_indices;
   string element = TUAtomElements::ZToElementName(z);
   string message = Form("Solar System isotopic fraction for Z=%d (%s)", z, element.c_str());
   TUMessages::Separator(f, (string)message);
   if (!ElementNameToCRIndices(element, cr_indices, false))
      return;
   else {
      for (Int_t i = 0; i < (Int_t)cr_indices.size(); i++) {
         Int_t j_cr = cr_indices[i];
         fprintf(f, "%s %3d %8s  %le\n", indent.c_str(), j_cr,
                 fCRs[j_cr].GetName().c_str(), fCRs[j_cr].GetSSIsotopicFraction());
      }
   }

   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUCRList::PrintUnstables(FILE *f, Bool_t is_beta_or_ec) const
{
   //--- Prints in file f CRs that are unstables.
   //  f                 File in which to print
   //  is_beta_or_ec     BETA-unstable or EC-unstable

   string indent = TUMessages::Indent(true);

   if (GetNCRs() == 0)
      TUMessages::Error(f, "TUCRList", "PrintUnstables", "fCRs not filled!");

   // Get number of unstable
   if (GetNUnstables(is_beta_or_ec) == 0) {
      if (is_beta_or_ec)
         TUMessages::Warning(f, "TUCRList", "PrintUnstables", "No BETA-unstable species in list");
      else
         TUMessages::Warning(f, "TUCRList", "PrintUnstables", "No EC-unstable species in list");
      return;
   }

   if (is_beta_or_ec)
      fprintf(f, "%s   - List of BETA-unstable species in CR list:\n", indent.c_str());
   else
      fprintf(f, "%s   - List of EC-unstable species in CR list:\n", indent.c_str());
   fprintf(f, "%s             ( Z,   A,  m [GeV])  ---[t_1/2 Myr]-->        ( Z,   A,  m [GeV])\n", indent.c_str());
   for (Int_t j = 0; j < GetNUnstables(is_beta_or_ec); ++j) {
      Int_t j_cr = IndexDecayParent(j, is_beta_or_ec);
      Int_t j_daugther = IndexDecayDaughter(j, is_beta_or_ec);
      Double_t hl_myr = 0., err_hl_myr = 0.;
      if (is_beta_or_ec) {
         hl_myr = fCRs[j_cr].GetBETAHalfLife();
         err_hl_myr = fCRs[j_cr].GetBETAHalfLifeErr();
      } else {
         hl_myr = fCRs[j_cr].GetECHalfLife();
         err_hl_myr = fCRs[j_cr].GetECHalfLifeErr();
      }

      if (j_daugther >= 0)
         fprintf(f, "%s      %5s  (%2d, %3d, %.2e)  [%.1e+/-%.e] %5s  (%2d, %3d, %.2e)\n",
                 indent.c_str(), fCRs[j_cr].GetName().c_str(), fCRs[j_cr].GetA(), fCRs[j_cr].GetZ(), fCRs[j_cr].GetmGeV(), hl_myr, err_hl_myr,
                 fCRs[j_daugther].GetName().c_str(), fCRs[j_daugther].GetA(), fCRs[j_daugther].GetZ(), fCRs[j_daugther].GetmGeV());
      else
         fprintf(f, "%s      %5s  (%2d, %3d, %.2e)  [%.1e+/-%.e] DAUGHTER NOT IN LIST\n",
                 indent.c_str(), fCRs[j_cr].GetName().c_str(), fCRs[j_cr].GetA(), fCRs[j_cr].GetZ(), fCRs[j_cr].GetmGeV(), hl_myr, err_hl_myr);

   }
   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUCRList::SetClass(TUInitParList *init_pars, Bool_t is_verbose, FILE *f_log, Bool_t is_reload_atom)
{
   //--- Updates content of class.
   //  init_pars           TUInitParList object of initialisation parameters
   //  is_verbose          Verbose or not when class is set
   //  f_log               Log for USINE
   //  is_reload_atom      If true, reloads TUAtomProperties

   string indent = TUMessages::Indent(true);
   TUMessages::Indent(false);
   if (is_verbose)
      fprintf(f_log, "%s[TUCRList::SetClass]\n", indent.c_str());

   UpdateCRs(init_pars, is_verbose, f_log);

   UpdateParents(init_pars, is_verbose, f_log);
   UpdatePureSecondaries(init_pars, is_verbose, f_log);

   // And set SS abundances
   if (is_reload_atom)
      TUAtomProperties::SetClass(init_pars, is_verbose, f_log);
   string file = init_pars->GetParEntry(init_pars->IndexPar("Base", "ListOfCRs", "SSRelativeAbund")).GetVal();
   if (is_verbose) {
      fprintf(f_log, "%s### Set Solar system isotopic fraction\n", indent.c_str());
      fprintf(f_log, "%s   - Load from %s\n", indent.c_str(), file.c_str());
   }
   LoadSSAbundances(TUMisc::GetPath(file));

   if (is_verbose)
      fprintf(f_log, "%s[TUCRList::SetClass] <DONE>\n\n", indent.c_str());
}

//______________________________________________________________________________
void TUCRList::SetCRList(string const &f_charts, string const &commasep_list,
                         Bool_t is_fill_ghosts, string const &list_name)
{
   //--- Updates content of fCRs with all CRs found in f_charts.
   //  f_charts          File of CR properties
   //  commasep_list     Comma-separated list of CRs to load
   //  is_fill_ghosts    Are 'ghosts' properties loaded?
   //  list_name         Name given to this list

   vector<string> crs;
   TUMisc::String2List(commasep_list, ",", crs);
   SetCRList(f_charts, 1, crs, is_fill_ghosts, list_name);
}

//______________________________________________________________________________
void TUCRList::SetCRList(string const &f_charts, Int_t cr_switch, vector<string> &crs,
                         Bool_t is_fill_ghosts, string const &list_name)
{
   //--- Updates content of fCRs with all CRs found in f_charts.
   //  f_charts          File of CR properties
   //  cr_switch         Tells how to fill class (see below)
   //  crs               Content depends on cr_switch:
   //                       - cr_switch=0: empty (use all CRs found in file)
   //                       - cr_switch=1: list of CRs to load
   //                       - cr_switch=2: all CRs found between crs[0] and crs[1]
   //  is_fill_ghosts    Are 'ghosts' properties loaded?
   //  list_name         Name given to this list

   // If input file exist, read it (else abort)
   ifstream f_read(f_charts.c_str());
   TUMisc::IsFileExist(f_read, f_charts);
   Initialise(true);

   // Find if $USINE, if yes, we replace it in file
   string file = f_charts;
   TUMisc::SubstituteEnvInPath(file, "$USINE");

   fFileName = file;
   fIsGhosts = is_fill_ghosts;
   fIDList = list_name;

   // Upper-case all names
   for (Int_t i = 0; i < (Int_t)crs.size(); ++i)
      TUMisc::UpperCase(crs[i]);

   // Read chart-formatted line
   string line;
   TUAtomProperties atom;
   Bool_t is_started = false;
   Bool_t is_stopped = false;
   while (getline(f_read, line)) {
      // Skip comments (start with '#') and empty lines
      string chart_line = TUMisc::RemoveBlancksFromStartStop(line);
      if (chart_line[0] == '#' || chart_line.size() == 0) continue;

      TUCREntry cr;
      cr.SetEntry(chart_line, *this, is_fill_ghosts);
      if (cr.IsEmpty()) continue;

      // Load CRs
      if (cr_switch == 0) {        // All
         fCRs.push_back(cr);
      } else if (cr_switch == 1) { // Those in vector
         // If this entry name matches one of the name in cr_list, add to fCRs
         string crname = cr.GetName();
         Int_t i_found = TUMisc::IndexInList(crs, crname, false);
         if (i_found >= 0)
            fCRs.push_back(cr);
      } else if (cr_switch == 2) { // Those between crs[0] and crs[1]
         if ((GetNCRs()) == 0 && (cr.GetName() == crs[0]))
            is_started = true;
         if (is_started)
            fCRs.push_back(cr);
         if (crs[1] == cr.GetName()) {
            is_stopped = true;
            break;
         }
      }
   };

   // Messages
   if (cr_switch == 1) {
      string list = TUMisc::List2String(crs, ',');
      if (GetNCRs() == 0) {
         string message = "in " + list + ", no matching CR found!";
         TUMessages::Warning(stdout, "TUCRList", "SetCRList", message);
      } else if (GetNCRs() < (Int_t)crs.size()) {
         string message = Form("in %s, only %d out of %d CR found!",
                               list.c_str(), GetNCRs(), (Int_t)crs.size());
         TUMessages::Warning(stdout, "TUCRList", "SetCRList", message);
      }
   } else if (cr_switch == 2) {
      if (!is_started) {
         string message = "No matching CR found for " + crs[0];
         TUMessages::Warning(stdout, "TUCRList", "SetCRList", message);
      }
      if (!is_stopped) {
         string message = "No matching CR found for " + crs[1];
         TUMessages::Warning(stdout, "TUCRList", "SetCRList", message);
      }
   }

   UpdateIndicesOfUnstables();
}

//______________________________________________________________________________
vector<Int_t> TUCRList::SortByGrowingZ(vector<Int_t> &cr_indices) const
{
   //--- Sort by growing Z cr indices in list (remove duplicates) and returns list of Z found.
   // INPUT:
   //  cr_indices        Unsorted list of CRs
   // OUTPUT:
   //  cr_indices        Sorted list of CRs

   vector<Int_t> list_z;

   if (cr_indices.size() == 0)
      return list_z;

   // First step: sort by growing index
   std::sort(cr_indices.begin(), cr_indices.end());

   // Second step: sort by growing Z
   vector<Int_t> tmp;
   do {
      Int_t i_zmin = -1;
      Int_t zmin = 1000;
      for (Int_t i = 0; i < (Int_t)cr_indices.size(); ++i) {
         list_z.push_back(fCRs[i].GetZ());
         if (fCRs[i].GetZ() < zmin) {
            zmin = fCRs[i].GetZ();
            i_zmin = i;
         }
      }
      if (i_zmin >= 0) {
         tmp.push_back(cr_indices[i_zmin]);
         cr_indices.erase(cr_indices.begin() + i_zmin, cr_indices.begin() + i_zmin + 1);
      } else
         break;
   } while (1);

   TUMisc::RemoveDuplicates(tmp);
   TUMisc::RemoveDuplicates(list_z);
   cr_indices.clear();
   cr_indices = tmp;

   return list_z;
}

//______________________________________________________________________________
vector<Int_t> TUCRList::SortByGrowingZ(vector<string> &crs_and_elements) const
{
   //--- Sort by growing Z elements and crs in list (remove duplicates) and returns list of Z found.
   // INPUT:
   //  crs_and_elements  Unsorted list of CRs and elements
   // OUTPUT:
   //  crs_and_elements  Sorted list of elements and CRs

   vector<Int_t> list_z;

   if (crs_and_elements.size() == 0)
      return list_z;

   // Extract all z and all indices
   vector<Int_t> z_elem, z_isot;
   vector<Int_t> tmp_index;
   for (Int_t i = 0; i < (Int_t)crs_and_elements.size(); ++i) {
      Int_t j_cr = Index(crs_and_elements[i], false, stdout);
      if (j_cr >= 0) {
         tmp_index.push_back(j_cr);
         z_isot.push_back(fCRs[j_cr].GetZ());
      } else if (IsElementAndInList(crs_and_elements[i]))
         z_elem.push_back(TUAtomProperties::ElementNameToZ(crs_and_elements[i]));
   }


   // Sort lists of elements and CRs
   crs_and_elements.clear();
   std::sort(z_elem.begin(), z_elem.end());
   TUMisc::RemoveDuplicates(z_elem);
   //
   std::sort(z_isot.begin(), z_isot.end());
   TUMisc::RemoveDuplicates(z_isot);

   SortByGrowingZ(tmp_index);
   TUMisc::RemoveDuplicates(tmp_index);

   Int_t z_min = 1000;
   Int_t z_max = -10;
   if (z_elem.size() != 0) {
      z_min = min(z_min, z_elem[0]);
      z_max = max(z_max, z_elem[z_elem.size() - 1]);
   }
   if (z_isot.size() != 0) {
      z_min = min(z_min, z_isot[0]);
      z_max = max(z_max, z_isot[z_isot.size() - 1]);
   }

   Int_t i_zstart = 0;
   Int_t i_crstart = 0;
   for (Int_t z = z_min; z <= z_max; ++z) {
      // Add element first
      for (Int_t i = i_zstart; i < (Int_t)z_elem.size(); ++i) {
         if (z_elem[i] == z) {
            crs_and_elements.push_back(TUAtomProperties::ZToElementName(z_elem[i]));
            list_z.push_back(z);
            i_zstart = i;
            break;
         }
      }
      // Add CRs
      for (Int_t i = i_crstart; i < (Int_t)tmp_index.size(); ++i) {
         if (fCRs[tmp_index[i]].GetZ() == z) {
            crs_and_elements.push_back(fCRs[tmp_index[i]].GetName());
            list_z.push_back(z);
         }
      }
   }
   TUMisc::RemoveDuplicates(list_z);
   return list_z;
}

//______________________________________________________________________________
void TUCRList::TEST(TUInitParList *init_pars, FILE *f)
{
   //--- Test function for this class (called in usine -t).
   //  init_pars         TUInitParList class of initialisation parameters
   //  f                 Output file for test

   string message = "User-selected list of CRs (i.e. list of TUCREntry) and parents\n"
                    "   - inherits from TUAtomProperties (i.e. atomic properties)\n"
                    "   - CR charts read from input file";
   TUMessages::Test(f, "TUCRList", message);

   // Force to use ghosts
   Int_t i_switch_ghost = init_pars->IndexPar("Base", "ListOfCRs", "IsLoadGhosts");
   string switch_ref = init_pars->GetParEntry(i_switch_ghost).GetVal();
   init_pars->SetVal(i_switch_ghost, "1" /*true*/);

   // Set class and test methods
   fprintf(f, " * Base class:\n");
   fprintf(f, "   > SetClass(init_pars=\"%s\", is_verbose=false, f_log = f, true);\n", init_pars->GetFileNames().c_str());
   SetClass(init_pars, false, f, true);
   fprintf(f, "\n");
   fprintf(f, "   > PrintGhosts(f, Zstart=10, Zstop=15, type=ALL, Br_min=20, Br_max=100);"
           "    => Ghosts 10<=Z<=15 for which 20<Br<100\n");
   PrintGhosts(f, 10, 15, "ALL", 20., 100.);
   fprintf(f, "\n");
   fprintf(f, "   > PrintListForGhost(f, ghost_name=10C);\n");
   PrintListForGhost(f, "10C");
   fprintf(f, "   > PrintListForGhost(f, ghost_name=11Li);\n");
   PrintListForGhost(f, "11Li");
   fprintf(f, "   > PrintListForGhost(f, ghost_name=11Be);\n");
   PrintListForGhost(f, "11Be");
   fprintf(f, "\n");
   fprintf(f, "\n");

   fprintf(f, "   > PrintElements(f, is_separator=false, is_summary = false);\n");
   PrintElements(f, false, false);
   fprintf(f, "   > PrintUnstables(f, is_beta_or_ec=true);\n");
   PrintUnstables(f, true);
   fprintf(f, "\n");
   fprintf(f, "   > PrintUnstables(f, is_beta_or_ec=false);\n");
   PrintUnstables(f, false);
   fprintf(f, "\n");
   fprintf(f, "   > PrintParents(f);\n");
   PrintParents(f);
   fprintf(f, "\n");
   fprintf(f, "   > PrintPureSecondaries(f);\n");
   PrintPureSecondaries(f);
   fprintf(f, "\n");
   fprintf(f, "   > PrintSSIsotopeAbundances(f);\n");
   PrintSSIsotopeAbundances(f);
   fprintf(f, "\n");
   fprintf(f, "   > PrintSSIsotopicFractions(f);\n");
   PrintSSIsotopicFractions(f);
   fprintf(f, "\n");
   fprintf(f, "   > PrintSSIsotopicFractions(f, z=3);\n");
   PrintSSIsotopicFractions(f, 3);
   string cr_heavier = "12C", cr_lighter = "7Be";
   fprintf(f, "   > PrintReactions(f, cr_heavier=%s, cr_lighter=%s, is_print_az=true, is_use_parentlist=true);\n",
           cr_heavier.c_str(), cr_lighter.c_str());
   PrintReactions(f, cr_heavier, cr_lighter, true, true);
   fprintf(f, "   > PrintReactions(f, cr_heavier=%s, cr_lighter=%s, is_print_az=true, is_use_parentlist=false);\n",
           cr_heavier.c_str(), cr_lighter.c_str());
   PrintReactions(f, cr_heavier, cr_lighter, true, false);
   if (1 == 0) {
      FILE *fp = fopen("Leslie_XSreacs_new.txt", "w");
      PrintReactions(fp, "30Si", "6Li", true, false);
      fclose(fp);
   }
   fprintf(f, "\n");
   // Copy
   fprintf(f, " * Test Copy method:\n");
   fprintf(f, "   > TUCRList cr; cr.Copy(*this);\n");
   TUCRList cr;
   cr.Copy(*this);
   fprintf(f, "   > cr.PrintGhosts(f, Zstart=10, Zstop=15, type=ALL, Br_min=20, Br_max=100);\n");
   cr.PrintGhosts(f, 10, 15, "ALL", 20., 100.);
   fprintf(f, "   > cr.PrintParents(f);\n");
   cr.PrintParents(f);
   fprintf(f, "\n");
   fprintf(f, "   > cr.PrintFragments(f);\n");
   cr.PrintFragments(f);
   fprintf(f, "\n");
   fprintf(f, "   > PrintUnstables(f, is_beta_or_ec=true);\n");
   PrintUnstables(f, true);
   fprintf(f, "\n");
   fprintf(f, "   > PrintUnstables(f, is_beta_or_ec=false);\n");
   PrintUnstables(f, false);
   fprintf(f, "\n");
   fprintf(f, "\n");


   // Simple identification functions
   fprintf(f, "\n");
   fprintf(f, " * Simple identification functions:\n");
   fprintf(f, "   > CRToElementName(i_cr=5);       => %s\n", CRToElementName(5).c_str());
   fprintf(f, "   > GetCREntry(i_cr=5).GetA());    => %d\n", GetCREntry(5).GetA());
   fprintf(f, "   > GetCREntry(i_cr=5).GetName();  => %s\n", GetCREntry(5).GetName().c_str());
   fprintf(f, "   > ExtractZMin();                 => %d\n", ExtractZMin());
   fprintf(f, "   > ExtractZMax();                 => %d\n", ExtractZMax());
   fprintf(f, "   > Index(11Be, true, f);          => %d\n", Index("11Be", true, f));
   fprintf(f, "   > Index(10Be, true, f);          => %d\n", Index("10Be", true, f));
   fprintf(f, "   > IndexParentBETA(Index(10B));   => %d\n", IndexParentBETA(Index("10B")));
   fprintf(f, "   > IsIndex(i_cr=0, f_out=f);      => %d\n", IsIndex(0, f));
   fprintf(f, "   > IsIndex(i_cr=220, f_out=f);    => %d\n", IsIndex(220, f));
   fprintf(f, "   > IsAtLeastOneAntinuc();         => %d\n", IsAtLeastOneAntinuc());
   fprintf(f, "   > IsAtLeastOneLepton();          => %d\n", IsAtLeastOneLepton());
   fprintf(f, "   > IsAtLeastOneNucleus();         => %d\n", IsAtLeastOneNucleus());
   fprintf(f, "   > IsOnlyNucAndAntinucInList();   => %d\n", IsOnlyNucAndAntinucInList());
   fprintf(f, "\n");
   fprintf(f, "\n");

   // Combo id.
   fprintf(f, " * Element/combo identification:\n");
   vector<string> l_crs;
   fprintf(f, "   > ElementNameToCRNames(\"B\", vector<string>, is_verbose=true, f_out=f);    [Extract list of crs]\n");
   ElementNameToCRNames("B", l_crs, true, f);
   vector<Int_t> li_crs;
   fprintf(f, "   > ElementNameToCRIndices(\"B\", vector<Int_t>, is_verbose=true, f_out=f)    [Extract indices of crs]\n");
   ElementNameToCRIndices("B", li_crs, true, f);
   Int_t n_denom;
   fprintf(f, "   > bool is = ComboToCRNames(combo=B/C, [out]=vector<string>, "
           "[out]=n_denom, is_verbose=true, f_out=f)   [Extract crs in list and get "
           "#crs in denominator (if ratio)}\n");
   Bool_t is = ComboToCRNames("B/C", l_crs, n_denom, true, f);
   fprintf(f, "      => [is=%d]  ", is);
   for (Int_t i = 0; i < (Int_t)l_crs.size(); ++i) fprintf(f, "       %s", l_crs[i].c_str());
   fprintf(f, "  n_denom=%d\n\n", n_denom);
   //
   fprintf(f, "   > ComboToElementNames(combo=12C+B+C/O+C, [out]=vector<string>);\n");
   ComboToElementNames("12C+B+C/O+C", l_crs);
   for (Int_t i = 0; i < (Int_t)l_crs.size(); ++i) fprintf(f, "       %s", l_crs[i].c_str());
   fprintf(f, "\n\n");
   //
   const Int_t n = 6;
   string array[n] = {"O", "12C", "B", "C", "14N", "C"};
   vector<string> names(array, array + n);
   const Int_t n2 = 2;
   string array2[n] = {"10B", "10Be"};
   vector<string> names2(array2, array2 + n2);
   vector<string> cr_names;
   fprintf(f, "   > CombosToCRNames({\"O\",\"12C\",\"B\",\"C\",\"14N\",\"C\"}, vector<string> &cr_names);\n");
   CombosToCRNames(names, cr_names);
   for (Int_t i = 0; i < (Int_t)cr_names.size(); ++i) fprintf(f, " %s", cr_names[i].c_str());
   vector<string> element_names;
   fprintf(f, "\n");
   fprintf(f, "   > CombosToElementNames({\"O\",\"12C\",\"B\",\"C\",\"14N\",\"C\"}, vector<string> &element_names);\n");
   CombosToElementNames(names, element_names);
   for (Int_t i = 0; i < (Int_t)element_names.size(); ++i) fprintf(f, " %s", element_names[i].c_str());
   fprintf(f, "\n");
   fprintf(f, "   > SortByGrowingZ(INPUT/OUTPUT={\"O\",\"12C\",\"B\",\"C\",\"14N\",\"C\");\n");
   SortByGrowingZ(names);
   for (Int_t i = 0; i < (Int_t)names.size(); ++i) fprintf(f, " %s", names[i].c_str());
   fprintf(f, "\n");
   fprintf(f, "   > ComboToCRIndices(combo=Li+Be+B/C+O, [out]=vector<int>, [out]=n_denom,"
           " is_verbose=true, f_out=f)    [Extract crs in list and get #crs in denominator (if ratio)]\n");
   is = ComboToCRIndices("Li+Be+B/C+O", li_crs, n_denom, true, f);
   fprintf(f, "      => [is=%d]  ", is);
   for (Int_t i = 0; i < (Int_t)li_crs.size(); ++i) fprintf(f, " %d", li_crs[i]);
   fprintf(f, "\n");
   fprintf(f, "  n_denom=%d\n", n_denom);
   fprintf(f, "   > ComboToCRIndices(combo=B/C, [out]=vector<int>, [out]=n_denom, is_verbose=true, f_out=f)"
           "    [Extract crs in list and get #crs in denominator (if ratio)]\n");
   is = ComboToCRIndices("B/Th", li_crs, n_denom, true, f);
   fprintf(f, "      => [is=%d]  ", is);
   for (Int_t i = 0; i < (Int_t)li_crs.size(); ++i) fprintf(f, " %d", li_crs[i]);
   fprintf(f, "  n_denom=%d\n", n_denom);
   fprintf(f, "\n");
   fprintf(f, "   > ComboToCRIndices(combo=ALLSPECTRUM, [out]=vector<int>, [out]=n_denom, is_verbose=true, f_out=f)"
           "    [Extract crs in list and get #crs in denominator (if ratio)]\n");
   ComboToCRIndices("ALLSPECTRUM", li_crs, n_denom, true, f);
   for (Int_t i = 0; i < (Int_t)li_crs.size(); ++i) fprintf(f, " %d", li_crs[i]);
   fprintf(f, "  n_denom=%d\n", n_denom);
   fprintf(f, "   > ComboToCRIndices(combo=<LNA>, [out]=vector<int>, [out]=n_denom, is_verbose=true, f_out=f)"
           "    [Extract crs in list and get #crs in denominator (if ratio)]\n");
   ComboToCRIndices("<LNA>", li_crs, n_denom, true, f);
   for (Int_t i = 0; i < (Int_t)li_crs.size(); ++i) fprintf(f, " %d", li_crs[i]);
   fprintf(f, "  n_denom=%d\n", n_denom);
   vector<string> test;
   fprintf(f, "   > CombosToElementNames({\"10B\",\"10Be\"}, vector<string> &test);\n");
   CombosToElementNames(names2, test);
   for (Int_t i = 0; i < (Int_t)test.size(); ++i) fprintf(f, " %s", test[i].c_str());
   fprintf(f, "   > CombosToCRNames({\"10B\",\"10Be\"}, vector<string> &test);\n");
   CombosToCRNames(names2, test);
   for (Int_t i = 0; i < (Int_t)test.size(); ++i) fprintf(f, " %s", test[i].c_str());
   fprintf(f, "\n");
   fprintf(f, "   > CombosToElementAndCRNames({\"10B\",\"10Be\"}, vector<string> &test);\n");
   CombosToElementAndCRNames(names2, test);
   for (Int_t i = 0; i < (Int_t)test.size(); ++i) fprintf(f, " %s", test[i].c_str());


   fprintf(f, "\n");
   fprintf(f, "   > vector<Int_t> list;\n");
   vector<Int_t> list;
   fprintf(f, "   > list = ExtractAllZ(is_only_nuc=false);\n");
   list = ExtractAllZ(false);
   for (Int_t i = 0; i < (Int_t)list.size(); ++i) fprintf(f, " %d", list[i]);
   fprintf(f, "\n");
   fprintf(f, "   > list = ExtractAllZ(is_only_nuc=true, z_min=2, z_max=10);\n");
   list = ExtractAllZ(true, 2, 10);
   for (Int_t i = 0; i < (Int_t)list.size(); ++i) fprintf(f, " %d", list[i]);
   fprintf(f, "\n");
   fprintf(f, "   > list = ExtractAllCRs(is_only_nuc=true, jcr_min=2, j_crmax=9);\n");
   list = ExtractAllCRs(true, 2, 9);
   for (Int_t i = 0; i < (Int_t)list.size(); ++i) fprintf(f, " %d", list[i]);
   fprintf(f, "\n");
   fprintf(f, "\n");

   // Others
   fprintf(f, " * Other functions for :\n");
   fprintf(f, "   > vector<string> list_combos = ExtractCombo(\"B/C,<LNA>,H\", is_verbose=true, f_out=f)\n");
   vector<string> list_combos = ExtractCombo("B/C,<LNA>,H", true, f);
   fprintf(f, "       => found %s\n", TUMisc::List2String(list_combos, ',').c_str());
   fprintf(f, "   > vector<string> list_combo; vector<gENUM_ETYPE> list_etype;\n");
   string qtiestypesphiff = "B/C,<LNA>,H:kEKN:0.6,0.2;B/C,O:kR:0.,1.1,1.2;H,He:kEKN:0.3";
   fprintf(f, "   > ExtractComboEtypephiff(\"%s\", list_combo, list_etype, list_phi, is_verbose=true, f_out=f)\n", qtiestypesphiff.c_str());
   vector<string> list_combo;
   vector<gENUM_ETYPE> list_etype;
   vector<vector<Double_t> > list_phiff;
   ExtractComboEtypephiff(qtiestypesphiff, list_combo, list_etype, list_phiff, true, f);
   for (Int_t i = 0; i < (Int_t)list_combo.size(); ++i)
      fprintf(f, "   combo/etype/phi[%d] = %-5s %s %s\n", i, list_combo[i].c_str(),
              TUEnum::Enum2Name(list_etype[i]).c_str(), TUMisc::List2String(list_phiff[i], ',', 3).c_str());
   Int_t i_cr = Index("10B");
   Int_t i_parent = IndexInParentList(i_cr, "12C");
   fprintf(f, "   > int i_cr = Index(10B)  =>  %d\n", i_cr);
   fprintf(f, "   > int i_parent = IndexInParentList(i_cr, parent_name=12C)  =>  %d\n", i_parent);
   fprintf(f, "   > GetCREntry(IndexInCRList_Parent(i_cr, i_parent)).GetName() => %s\n",
           GetCREntry(IndexInCRList_Parent(i_cr, i_parent)).GetName().c_str());
   fprintf(f, "\n");
   fprintf(f, "\n");

   fprintf(f, " * Map 2 lists of CRs:\n");
   const Int_t gg = 3;
   string gr_sub_name[gg] = {"Base", "ListOfCRs", "fChartsForCRs"};
   string f_charts = init_pars->GetParEntry(init_pars->IndexPar(gr_sub_name)).GetVal();
   fprintf(f, "  - Use f_charts in init_pars: %s\n", f_charts.c_str());
   f_charts = TUMisc::GetPath(f_charts);
   string commasep_list = "1H,4He,12C,16O";
   fprintf(f, "  - Create new list of CRs (CR list only, no secondaries, etc.): commasep_list=%s\n", commasep_list.c_str());
   fprintf(f, "   > TUCRList list;\n");
   TUCRList new_list;
   string name_list = "New list";
   fprintf(f, "   > new_list.SetCRList(f_charts, commasep_list, false, name_list=\"New List\");\n");
   new_list.SetCRList(f_charts, commasep_list, false, name_list);
   fprintf(f, "   > new_list.PrintList(f)\n");
   new_list.PrintList(f);
   vector<Int_t> indices_newlist_inclasslist;
   fprintf(f, "\n");
   fprintf(f, "   >  MapIndices(new_list, vector<Int_t> &indices_newlist_inclasslist, Bool_t is_verbose=true, f_out=f);\n");
   MapIndices(&new_list, indices_newlist_inclasslist, true, f);
   fprintf(f, "\n");
   fprintf(f, "   >  new_list.MapIndices(this, vector<Int_t> &indices_newlist_inclasslist, Bool_t is_verbose=true, f_out=f);\n");
   new_list.MapIndices(this, indices_newlist_inclasslist, true, f);
   fprintf(f, "\n");
   fprintf(f, "\n");
   fprintf(f, "\n");

   fprintf(f, " * Free parameters in CRs:\n");
   fprintf(f, "   > TUFreeParList *pars = new TUFreeParList();...\n");
   TUFreeParList *pars = new TUFreeParList();
   pars->AddPar("HalfLifeBETA_10Be", "Myr", 1.3);
   pars->AddPar("HalfLifeBETA_26Al", "Myr", 0.7);
   pars->AddPar("HalfLifeEC_26Al", "Myr", 4.);
   fprintf(f, "   > pars->PrintPars(f);\n");
   pars->PrintPars(f);
   fprintf(f, "   > ParsHalfLife_AddPars(pars, 1, 3, true /*is_use_or_copy*/, true/*is_verbose*/, f);\n");
   ParsHalfLife_AddPars(pars, 1, 2, true /*is_use_or_copy*/, true/*is_verbose*/, f);
   for (Int_t i = 0; i < 2; ++i) {
      fprintf(f, "   > ParsHalfLife_Print(f, %d);\n", i);
      ParsHalfLife_Print(f, i);
   }
   fprintf(f, "   > ParsHalfLife_UpdateFromFreeParsAndResetStatus();\n");
   ParsHalfLife_UpdateFromFreeParsAndResetStatus();
   for (Int_t i = 0; i < 2; ++i) {
      fprintf(f, "   > ParsHalfLife_Print(f, %d);\n", i);
      ParsHalfLife_Print(f, i);
   }
   fprintf(f, "\n");
   fprintf(f, "\n");

   // Simple CR list (no secondaries)
   fprintf(f, " * Test pure CR list (no secondaries):\n");
   fprintf(f, "   > SetCRList(f_charts=TUMisc::GetPath(GetFileName()), commasep_list=\"10Be,12C,16O\", is_fill_ghosts=false, list_name=pure_list);\n");
   SetCRList(TUMisc::GetPath(GetFileName()), "10Be,12C,16O", false, "pure_list");
   fprintf(f, "   > PrintList(f);\n");
   PrintList(f);
   fprintf(f, "   > PrintCharts(f);\n");
   PrintCharts(f);
   fprintf(f, "   > PrintGhosts(f, z_min=1, z_max=TUPhysics::Zmax());\n");
   PrintGhosts(f, 1, TUPhysics::Zmax());
   fprintf(f, "   > PrintParents(f);\n");
   PrintParents(f);

   // Copy
   fprintf(f, " * Test Copy method:\n");
   fprintf(f, "   > TUCRList cr; cr.Copy(*this);\n");
   cr.Copy(*this);
   fprintf(f, "   > cr.PrintList(f);\n");
   cr.PrintList(f);
   fprintf(f, "   > cr.PrintCharts(f);\n");
   cr.PrintCharts(f);
   fprintf(f, "   > cr.PrintGhosts(f, z_min=1, z_max=TUPhysics::Zmax());\n");
   cr.PrintGhosts(f, 1, TUPhysics::Zmax());
   fprintf(f, "   > cr.PrintParents(f);\n");
   cr.PrintParents(f);
   fprintf(f, "\n");

   // Rest init_pars to ref
   init_pars->SetVal(i_switch_ghost, switch_ref);

   delete pars;
   pars = NULL;
}

//______________________________________________________________________________
Double_t TUCRList::TUI_SelectCombosEAxisphiFF(string &combos_etypes_phiff, Bool_t is_epower) const
{
   //--- Text User Interface to select combos/etypes/modulation values to display.
   //    For instance, a valid string to ask for is:
   //       - "10B+11B,B/C,O:kEKN:0.5,0.75;C:kR:0.77"
   //       - keywords strings "<LNA>" and "ALLSPECTRUM" are also accepted
   //    The method checks if combo asked for is available in the list.
   //       - if not => ask again;
   //       - if OK, returns as vectors.
   // INPUT:
   //  is_epower         Whether to enable flux multiplication by E^eindex or not
   // OUTPUT:
   //  combos_etypes_phiff String of combos/etypes/phiff selected

   if (GetNCRs() == 0)
      TUMessages::Error(stdout, "TUCRList", "TUI_SelectCombosEAxisphiFF", "you have to first define a list of CRs!");

   string indent = TUMessages::Indent(true);
   TUMessages::Indent(false);

   do {
      cout << endl;
      combos_etypes_phiff = "";
      cout << indent << "   ### Select a list of quantities/etypes/phi_FF (case insensitive), e.g. B/C,C:EKN:0.5,0.75;<LNA>,ALLSPECTRUM:ETOT:1.2" << endl;
      cout << indent << "           [0/Q= Quit]   [\'LIST\'= display list of available CRs and elements]" << endl;
      cout << indent << "    >>  ";
      cin >> combos_etypes_phiff;

      TUMisc::UpperCase(combos_etypes_phiff);
      if (combos_etypes_phiff.find("LIST") != string::npos) {
         PrintList(stdout, false);
         PrintElements(stdout, false);
      } else if (combos_etypes_phiff[0] == 'Q' || combos_etypes_phiff[0] == '0') {
         combos_etypes_phiff = "";
         return 0.;
      } else {
         // Check combo is OK
         vector<string> list_combos;
         vector<gENUM_ETYPE> list_etypes;
         vector<vector<Double_t> > list_phiff;
         ExtractComboEtypephiff(combos_etypes_phiff, list_combos, list_etypes, list_phiff, false);
         if (list_combos.size() == 0) {
            cout << indent << "   ... NOT A VALID OPTION, TRY AGAIN!" << endl;
            continue;
         }
      }

      // Optional: flux is multiplied by E^a
      Double_t e_power = 0.;
      if (is_epower) {
         cout << indent << "   ### Fluxes will be multiplied by E^a, enter a:" << endl;
         cout << indent << "    >>  ";
         cin >> e_power;
         return e_power;
      }
   } while (1);
   return 0.;
}

//______________________________________________________________________________
Double_t TUCRList::TUI_SelectCombosEAxisphiFF(vector<string> &list_combos, vector<gENUM_ETYPE> &list_etypes,
      vector<vector<Double_t> > &list_phiff, Bool_t is_epower) const
{
   //--- Text User Interface to select combos/etypes/modulation values to display.
   //    For instance, a valid string to ask for is:
   //       - "10B+11B,B/C,O:kEKN:0.5,0.75;C:kR:0.77"
   //       - keywords strings "<LNA>" and "ALLSPECTRUM" are also accepted
   //    The method checks if combo asked for is available in the list.
   //       - if not => ask again;
   //       - if OK, returns as vectors.
   // INPUT:
   //  is_epower         Whether to enable flux multiplication by E^eindex or not
   // OUTPUTS:
   //  list_combos       Vector of combos selected
   //  list_etypes       Vector of associated etypes (as many entries as in list_combos)
   //  list_phiff        Vector of list of associated solar modulation level phi (here phi=A/|Z|/*Phi)

   // Get combos
   list_combos.clear();
   list_etypes.clear();
   list_phiff.clear();

   string combos_etypes_phiff = "";
   Double_t e_power = TUI_SelectCombosEAxisphiFF(combos_etypes_phiff, is_epower);

   // If quit, returns Q
   if (combos_etypes_phiff == "") {
      list_combos.clear();
      list_combos.push_back("Q");
      return 0.;
   }

   ExtractComboEtypephiff(combos_etypes_phiff, list_combos, list_etypes, list_phiff, false);
   return e_power;
}

//______________________________________________________________________________
void TUCRList::UpdateCRs(TUInitParList *init_pars, Bool_t is_verbose, FILE *f_log)
{
   //--- Updates content of fCRs.
   //  init_pars         TUInitParList object of USINE parameters
   //  is_verbose        Verbose or not when class is set
   //  f_log             Log for USINE

   //--- Set list of CRs
   const Int_t gg = 3;
   string gr_sub_name[gg] = {"Base", "ListOfCRs", "IsLoadGhosts"};
   // Check whether ghosts must be loaded
   Bool_t is_fill_ghosts =  TUMisc::String2Bool(init_pars->GetParEntry(init_pars->IndexPar(gr_sub_name)).GetVal().c_str());
   string indent = TUMessages::Indent(true);
   if (is_verbose) {
      if (is_fill_ghosts)
         fprintf(f_log, "%s### Set list of propagated CRs (with charts and ghosts)\n", indent.c_str());
      else
         fprintf(f_log, "%s### Set list of propagated CRs (with charts)\n", indent.c_str());
   }
   vector<string> crs;
   gr_sub_name[2] = "ListOfCRs";
   string list_crs = init_pars->GetParEntry(init_pars->IndexPar(gr_sub_name)).GetVal();
   gr_sub_name[2] = "fChartsForCRs";
   string f_charts = init_pars->GetParEntry(init_pars->IndexPar(gr_sub_name)).GetVal();
   if (is_verbose)
      fprintf(f_log, "%s   - Load %s from %s\n",
              indent.c_str(), list_crs.c_str(), f_charts.c_str());
   f_charts = TUMisc::GetPath(f_charts);


   // Update CRs depending on how 'list_crs' is formatted
   TUMisc::UpperCase(list_crs);
   if (list_crs == "ALL")
      SetCRList(f_charts, 0, crs,  is_fill_ghosts);
   else {
      TUMisc::RemoveBlancksFromStartStop(list_crs);
      Bool_t is_range = false;
      if (list_crs[0] == '[' && list_crs[list_crs.size() - 1] == ']') {
         is_range = true;
         list_crs = list_crs.substr(1, list_crs.size() - 2);
      }

      TUMisc::String2List(list_crs, ",", crs);
      // Fill CRs
      if (is_range)
         SetCRList(f_charts, 2, crs, is_fill_ghosts);
      else
         SetCRList(f_charts, 1, crs, is_fill_ghosts);
   }

   // Print list of radioactive
   if (is_verbose) {
      PrintUnstables(f_log, true);
      PrintUnstables(f_log, false);
   }

   // Set what the list if made of
   fIndicesAntinuclei.clear();
   fIndicesLeptons.clear();
   fIndicesNuclei.clear();
   for (Int_t i = 0; i < GetNCRs(); ++i) {
      if (fCRs[i].GetFamily() == kNUC)
         fIndicesNuclei.push_back(i);
      else if (fCRs[i].GetFamily() == kANTINUC)
         fIndicesAntinuclei.push_back(i);
      else if (fCRs[i].GetFamily() == kLEPTON)
         fIndicesLeptons.push_back(i);
   }
   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUCRList::UpdateFragments()
{
   //--- Extracts and update fIndicesFragments for all CRs.

   // Look for fragments for each parents
   for (Int_t j_cr = 0; j_cr < GetNCRs(); ++j_cr) {
      fIndicesFragments[j_cr].clear();

      // Search for all parents
      for (Int_t i_cr = 0; i_cr < GetNCRs(); ++i_cr) {
         for (Int_t j_p = 0; j_p < GetNParents(i_cr); ++j_p) {
            if (IndexInCRList_Parent(i_cr, j_p) == j_cr)
               fIndicesFragments[j_cr].push_back(i_cr);
         }
      }

      // Sort by decreasing order
      sort(fIndicesFragments[j_cr].begin(), fIndicesFragments[j_cr].end());
      reverse(fIndicesFragments[j_cr].begin(), fIndicesFragments[j_cr].end());
   }
}

//______________________________________________________________________________
void TUCRList::UpdateIndicesOfUnstables()
{
   //--- Updates content of fIndexParentBETA[NCRs] and fIndexParentEC[NCRs],
   //    which contain respectively (for each CR in fCRs) the index of the associated
   //    BETA-decay and of the EC-decay parent if exists (-1 otherwise). Also fills the
   //    content of fMapBETAIndex2CRIndex[NDecayBETA] and fMapECIndex2CRIndex[NDecayEC],
   //    which contain respectively indices of BETA (and EC) unstable CRs (in the list
   //    of CRs) in a pair (unstable/daughter).

   fIndexParentBETA.clear();
   fIndexParentEC.clear();
   fMapBETAIndex2CRIndex.clear();
   fMapECIndex2CRIndex.clear();

   for (Int_t j_cr = 0; j_cr < GetNCRs(); ++j_cr) {
      TUCREntry cr = fCRs[j_cr];

      // List of parents
      if (cr.GetType() == "MIX-FED") {
         fIndexParentBETA.push_back(Index(cr.GetBETACRFriend(), false, stdout, false));
         fIndexParentEC.push_back(Index(cr.GetECCRFriend(), false, stdout, false));
      } else if (cr.GetType() == "BETA-FED") {
         fIndexParentBETA.push_back(Index(cr.GetBETACRFriend(), false, stdout, false));
         fIndexParentEC.push_back(-1);
      } else if (cr.GetType() == "EC-FED") {
         fIndexParentBETA.push_back(-1);
         fIndexParentEC.push_back(Index(cr.GetECCRFriend(), false, stdout, false));
      } else {
         fIndexParentBETA.push_back(-1);
         fIndexParentEC.push_back(-1);
      }


      if (cr.GetType() == "MIX") {
         pair<Int_t, Int_t> tmp;
         // BETA
         tmp.first = j_cr;
         tmp.second = Index(cr.GetBETACRFriend(), false, stdout, false);
         fMapBETAIndex2CRIndex.push_back(tmp);
         // EC
         tmp.first = j_cr;
         tmp.second = Index(cr.GetECCRFriend(), false, stdout, false);
         fMapECIndex2CRIndex.push_back(tmp);
      } else if (cr.GetType() == "BETA") {
         pair<Int_t, Int_t> tmp;
         tmp.first = j_cr;
         tmp.second = Index(cr.GetBETACRFriend(), false, stdout, false);
         fMapBETAIndex2CRIndex.push_back(tmp);
      } else if (cr.GetType() == "EC") {
         pair<Int_t, Int_t> tmp;
         tmp.first = j_cr;
         tmp.second = Index(cr.GetECCRFriend(), false, stdout, false);
         fMapECIndex2CRIndex.push_back(tmp);
      }
   }
}

//______________________________________________________________________________
void TUCRList::UpdateParents()
{
   //--- Updates content of fIndicesParents for all CRs. In this method,
   //    parents for a CR j_cr are all CRs heavier than j_cr. Note that the heaviest
   //    CR is always a primary!

   AllocateParentsAndFragments();
   for (Int_t j_cr = GetNCRs() - 2; j_cr >= 0; --j_cr) {
      vector<string> dummy;
      UpdateParents(j_cr, 0, dummy);
   }
}

//______________________________________________________________________________
void TUCRList::UpdateParents(TUInitParList *init_pars, Bool_t is_verbose, FILE *f_log)
{
   //--- Updates content of fIndicesParents[n_crs].
   //  init_pars        TUInitParList of USINE parameters
   //  is_verbose        Verbose or not when class is set
   //  f_log             Log for USINE

   if (GetNCRs() == 0)
      UpdateCRs(init_pars, is_verbose, f_log);

   string group = "Base";
   string subgroup = "ListOfCRs";

   // Set list of CR parents
   string indent = TUMessages::Indent(true);
   if (is_verbose)
      fprintf(f_log, "%s### Set list of CR parents\n", indent.c_str());

   // Set user-defined parents
   // (formatted as: "CR1,CR2:CR3,CR4,CR5;CR2:CR4|CR10;CR3:Primary")
   string list_parents = init_pars->GetParEntry(init_pars->IndexPar(group, subgroup, "ListOfParents")).GetVal();
   TUMisc::UpperCase(list_parents);

   // Initialise with all heavier CRs (unless keyword "PRIMARIES" is used)
   if (list_parents == "PRIMARIES") {
      // Info on CR and their parents
      AllocateParentsAndFragments();
      if (is_verbose)
         PrintParents(f_log);
      return;
   } else
      UpdateParents();

   // If user-defined parents
   if (list_parents != "ALL") {
      vector<string> user_parents;
      TUMisc::String2List(list_parents, ";", user_parents);
      for (Int_t i = 0; i < (Int_t)user_parents.size(); i++) {
         // We now have user_parents[i] = CR:parent1,parent2...
         //   => extract CR and parents
         vector<string> cr_parents;
         TUMisc::String2List(user_parents[i], ":", cr_parents);

         // Note that CR can actually be CR1,CR2,CR3 (i.e., several CRs have
         // the same list of parents). We thus have to check if 'cr_parents[0]'
         // is a list or not and take the appropriate action! For that, we
         //   i) extract the list of indices of CRs for which to change their parents
         //   ii) Depending on the parents, we set them for all this list of CRs
         vector<string> crs_sameparents;
         TUMisc::String2List(cr_parents[0], ",", crs_sameparents);
         vector<Int_t> i_crs_sameparents;
         for (Int_t j = 0; j < (Int_t)crs_sameparents.size(); ++j) {
            Int_t i_cr = Index(crs_sameparents[j], true, f_log);
            if (i_cr >= 0) i_crs_sameparents.push_back(i_cr);
         }
         Int_t n_crs = i_crs_sameparents.size();
         if (n_crs <= 0) continue;

         // Update parents depending on how 'user_parents' is formatted
         string list_parents_i = cr_parents[1];
         TUMisc::UpperCase(list_parents_i);

         if (list_parents_i == "PRIMARY") {
            for (Int_t i_cr = 0; i_cr < n_crs; ++i_cr)
               fIndicesParents[i_crs_sameparents[i_cr]].clear();
         } else {
            vector<string> parents;
            TUMisc::String2List(list_parents_i, "[,]", parents);

            if (list_parents_i.find("[]") == string::npos) { // comma-separated list
               for (Int_t i_cr = 0; i_cr < n_crs; ++i_cr)
                  UpdateParents(i_crs_sameparents[i_cr], 1, parents);
            } else if ((Int_t)parents.size() == 2) {
               for (Int_t i_cr = 0; i_cr < n_crs; ++i_cr)
                  UpdateParents(i_crs_sameparents[i_cr], 2, parents);
            } else {
               string message = "wrong format for user-defined parents";
               TUMessages::Error(f_log, "TUCRList", "UpdateParents", message);
            }
         }
      }
   }
   // Info on CR and their parents
   if (is_verbose)
      PrintParents(f_log);

   // Update fragments
   UpdateFragments();

   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUCRList::UpdateParents(Int_t j_cr, Int_t cr_switch, vector<string> &parents)
{
   //--- Updates content of fIndicesParents[j_cr], depending on cr_switch.
   //  j_cr              CR index for which to update parents
   //  cr_switch         Tells how to parents are filled (see below)
   //  parents           Content depends on cr_switch: parents are
   //                       - cr_switch=0: all CRs heavier than j_cr
   //                       - cr_switch=1: names given in 'parents' (comma-separated list)
   //                       - cr_switch=2: all CRs found between parents[0] and parents[1]

   if (!IsIndex(j_cr)) return;
   if (j_cr == GetNCRs() - 1) return;

   // Upper-case all names
   for (Int_t i = 0; i < (Int_t)parents.size(); ++i)
      TUMisc::UpperCase(parents[i]);

   // Reset if previously filled
   fIndicesParents[j_cr].clear();

   if (cr_switch == 0) {        // Parents are all heavier CRs
      // Allocate number of parents and fill
      //  - parent_first has index j_cr+1
      //  - parent_last has index GetNCRs()-1
      Int_t n_parents = (GetNCRs() - 1) - (j_cr + 1) + 1;
      for (Int_t p = 0; p < n_parents; ++p)
         fIndicesParents[j_cr].push_back(j_cr + 1 + p);

   } else if (cr_switch == 1) { // Parents are those in comma-separated list
      for (Int_t j = 0; j < (Int_t)parents.size(); ++j) {
         Int_t ii = Index(parents[j]);
         // Add only those found in the list
         if (ii >= 0)
            fIndicesParents[j_cr].push_back(ii);
      }

      // Messages
      string list = TUMisc::List2String(parents, ',');
      if ((Int_t)fIndicesParents[j_cr].size() == 0) {
         string message = "in " + list + ", no matching parents found!";
         TUMessages::Warning(stdout, "TUCRList", "UpdateParents", message);
         return;
      } else if (fIndicesParents[j_cr].size() < parents.size()) {
         string message = Form("in %s, only %d out of %d parents found!", list.c_str(),
                               (Int_t)fIndicesParents[j_cr].size(), (Int_t)parents.size());
         TUMessages::Warning(stdout, "TUCRList", "UpdateParents", message);
      }
   } else if (cr_switch == 2) {  // Parents are those in start=parent[0] - stop=parents[1]
      // Parents are those in comma-separated list   // Find indices of these two parents
      Int_t i_first = Index(parents[0]);
      Int_t i_last = Index(parents[1]);

      // Messages
      if (i_first < 0) {
         string message = parents[0] + "not found!";
         TUMessages::Warning(stdout, "TUCRList", "UpdateParents", message);
         return;
      } else if (i_last < 0) {
         string message = parents[1] + "not found!";
         TUMessages::Warning(stdout, "TUCRList", "UpdateParents", message);
         return;
      } else {
         // Allocate number of parents and fill
         //  - parent_first has index i_first
         //  - parent_last has index i_last
         for (Int_t j = i_first; j <= i_last; ++j)
            fIndicesParents[j_cr].push_back(j);
      }
   }

   CheckParents(j_cr);
}

//______________________________________________________________________________
void TUCRList::UpdatePureSecondaries(TUInitParList *init_pars, Bool_t is_verbose, FILE *f_log)
{
   //--- Updates content of fNamePureSecondaries and fIndicesPureSecondaries.
   //  init_pars         TUInitParList of USINE parameters
   //  is_verbose        Verbose or not when class is set
   //  f_log             Log for USINE

   if (GetNCRs() == 0)
      UpdateCRs(init_pars, is_verbose, f_log);

   // Set list of pure secondaries
   string indent = TUMessages::Indent(true);
   if (is_verbose)
      fprintf(f_log, "%s### Set list of pure secondaries [Base#ListOfCRs#PureSecondaries]\n", indent.c_str());

   fNamePureSecondaries = init_pars->GetParEntry(init_pars->IndexPar("Base", "ListOfCRs", "PureSecondaries")).GetVal();
   fIndicesPureSecondaries.clear();

   // If no pure secondaries specified
   if (fNamePureSecondaries == "-") {
      fNamePureSecondaries =  "";
      if (is_verbose)
         fprintf(f_log, "%s   - List selected: NONE\n", indent.c_str());
      return;
   }
   if (is_verbose)
      fprintf(f_log, "%s   - List selected: %s\n", indent.c_str(), fNamePureSecondaries.c_str());

   // Extract CR indices
   vector<string> list;
   TUMisc::String2List(fNamePureSecondaries, ",", list);
   for (Int_t i = 0; i < (Int_t)list.size(); ++i) {
      TUMisc::UpperCase(list[i]);
      vector<Int_t> cr_indices;

      // For each qty, find associated CR indices in CR list
      // (depends whether this is an element or a CR).
      Bool_t is_element = TUAtomElements::IsElement(list[i]);
      if (is_element) {
         // Check that there exist at least one CR in list for
         // this element and add those found in list of indices

         Int_t z = ElementNameToZ(list[i], false);
         if (z < ExtractZMin() || z > ExtractZMax())
            continue;
         else
            ElementNameToCRIndices(list[i], cr_indices, false);
      } else {
         // check that CR name exist before to add it
         Int_t j_cr = Index(list[i], false, f_log, false);
         if (j_cr >= 0)
            cr_indices.push_back(j_cr);
      }
      // Add in list found
      for (Int_t j = 0; j < (Int_t)cr_indices.size(); ++j) {
         fIndicesPureSecondaries.push_back(cr_indices[j]);
      }
   }

   // Set if pure secondary or not
   for (Int_t j_cr = 0; j_cr < GetNCRs(); ++j_cr) {
      Bool_t is_sec = false;
      for (Int_t i = 0; i < GetNPureSecondaries(); ++i) {
         if (IndexPureSecondary(i) == j_cr) {
            is_sec = true;
            break;
         }
      }
      fIsPureSecondary.push_back(is_sec);
   }

   // Print result
   if (is_verbose)
      PrintPureSecondaries(f_log);

   TUMessages::Indent(false);
}

