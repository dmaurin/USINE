// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

// C/C++ include
// ROOT include
// USINE include
#include "../include/TUMath.h"
#include "../include/TUSolMod0DFF.h"
#include "../include/TUValsTXYZECr.h"

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TUValsTXYZECr                                                        //
//                                                                      //
// Multi-purpose array of TUValsTXYZEVirtual[NCRs] (e.g., CR fluxes).   //
//                                                                      //
// The class TUValsTXYZECr is a vector of NCRs TUValsTXYZEVirtual       //
// (generic handler for a space-time and energy dependent quantities,   //
// formula or values on grid), along with two additional members:       //
//    - TUAxesTXYZ: space-time coord. T,X,Y,Z (from 0D to 3D+1);
//    - TUAxesCrE: CR charts and energy-grid values (one per CR family).
//                                                                      //
// The most useful members of this class are prints and plots. It is    //
// particularly well-suited to describe CR fluxes, as the following     //
// two capabilities are enabled: (i) flux value for any combination     //
// of CR quantities (isotope, element, ratio...), and (ii) Solar        //
// modulation of interstellar (IS) fluxes. In practice, IS or TOA       //
// (top-of-atmosphere) fluxes can be obtained for:                      //
//    - a ratio, element, sum of isotopes, etc.;
//    - a selected E-axis (EKN, EK, ETOT, R);
//    - a given modulation model and its modulation parameters.
//                                                                      //
// N.B.: do not forget that, for each CR element, you have access to    //
// all the methods inherited from TUValsTXYZEVirtual (e.g., fill flux   //
// values, add, subtract, if they were created from TUValsTXYZEGrid).   //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

ClassImp(TUValsTXYZECr)

//______________________________________________________________________________
TUValsTXYZECr::TUValsTXYZECr() : TUFreeParList()
{
   // ****** Default constructor ******

   Initialise(false);
}

//______________________________________________________________________________
TUValsTXYZECr::TUValsTXYZECr(TUValsTXYZECr const &vals_txyzcre) : TUFreeParList()
{
   // ****** Copy constructor ******
   //  vals_txyzcre      Object to copy

   Initialise(false);
   Copy(vals_txyzcre);
}

//______________________________________________________________________________
TUValsTXYZECr::~TUValsTXYZECr()
{
   // ****** Default destructor ******
   // (delete only if required)

   Initialise(true);
}

//______________________________________________________________________________
void TUValsTXYZECr::AllocateVals(Int_t bincr, gENUM_FCNTYPE fcn_type)
{
   //--- Allocates fValsTXYZE for bincr.
   //  bincr             CR index
   //  fcn_type          kFORMULA, kGRID, kSPLIE (see TUEnum.h)

   if (fValsTXYZE[bincr]) delete fValsTXYZE[bincr];
   fValsTXYZE[bincr] = NULL;

   // Allocate fValsTXYZEFormula or fValsTXYZEGrid
   if (fcn_type == kFORMULA)
      fValsTXYZE[bincr] = new TUValsTXYZEFormula();
   else if (fcn_type == kGRID)
      fValsTXYZE[bincr] = new TUValsTXYZEGrid();
   else {
      TUMessages::Error(stdout, "TUValsTXYZECr", "AllocateVals",
                        "Option not 0, 1, 2 does not exist");
   }

   fValsTXYZEFormat[bincr] = fcn_type;
}

//______________________________________________________________________________
void TUValsTXYZECr::AllocateForAxesTXYZCrE(TUAxesTXYZ *axes_txyz, TUAxesCrE *axes_cre, Bool_t is_use_or_copy)
{
   //--- Set axes (used or copied) and allocate pointers for NCRs.
   //    N.B.: after this stage, fValsTXYZE[j_cr] remains to be allocated!
   //  axes_txyz         TXYZ axes (TUAxesTXYZ object)
   //  axes_cre          List of CRs and E axis (TUAxesCrE object)
   //  is_use_or_copy    Whether axes are copied (deleted in destructor) or only used (from existing ones)

   Initialise(true);

   // Whether this is a copy or only use axes
   if (is_use_or_copy) {
      fAxesCrE = axes_cre;
      fAxesTXYZ = axes_txyz;
      fIsDeleteCrE = false;
      fIsDeleteTXYZ = false;
   } else {
      // Create and Copy class content for CR and E axes
      fAxesCrE = new TUAxesCrE();
      fAxesCrE->Copy(*axes_cre);

      // Create and Copy class content for TXYZ axes
      if (axes_txyz) {
         fAxesTXYZ = new TUAxesTXYZ();
         fAxesTXYZ->Copy(*axes_txyz);
      }
      fIsDeleteCrE = true;
      fIsDeleteTXYZ = true;
   }
   // Allocates fValsTXYZE and fValsTXYZEFormat for NCRs
   if (fValsTXYZE) delete[] fValsTXYZE;
   fValsTXYZE = NULL;

   if (fValsTXYZEFormat) delete[] fValsTXYZEFormat;
   fValsTXYZEFormat = NULL;

   fValsTXYZE = new TUValsTXYZEVirtual*[fAxesCrE->GetNCRs()];
   fValsTXYZEFormat = new gENUM_FCNTYPE[fAxesCrE->GetNCRs()];
   for (Int_t bincr = 0; bincr < fAxesCrE->GetNCRs(); ++bincr) {
      fValsTXYZE[bincr] = NULL;
      fValsTXYZEFormat[bincr] = kFORMULA;
   }
}

//______________________________________________________________________________
void TUValsTXYZECr::AllocateForAxesTXYZCrE(TUInitParList *init_pars, string const &group, FILE *f_log)
{
   //--- Sets flux coordinates (deleted when destructor called) from group (subgroup="Geometry").
   //  init_pars         TUInitParList class of initialisation parameters
   //  group             Group name (should be 'Propag')
   //  f_log             Log for USINE

   // Create cre and txyz axes
   TUAxesCrE *tmp_cre = new TUAxesCrE();
   tmp_cre->SetClass(init_pars, false, f_log);
   TUAxesTXYZ *tmp_txyz = new TUAxesTXYZ();
   tmp_txyz->SetClass(init_pars, group, "Geometry", false, f_log);

   // Set class axes
   AllocateForAxesTXYZCrE(tmp_txyz, tmp_cre, false);

   // Delete temporary axes
   delete tmp_cre;
   tmp_cre = NULL;
   delete tmp_txyz;
   tmp_txyz = NULL;
}

//______________________________________________________________________________
void TUValsTXYZECr::Copy(TUValsTXYZECr const &vals_txyzcre)
{
   //--- Copies object in class.
   //  vals_txyzcre      Object to copy from

   Initialise(true);

   // Copy CR and E axes
   fIsDeleteCrE = vals_txyzcre.IsDeleteCrE();
   if (fIsDeleteCrE) {
      TUAxesCrE *cre = vals_txyzcre.GetAxesCrE();
      if (cre)
         fAxesCrE = cre->Clone();
      cre = NULL;
   } else
      fAxesCrE = vals_txyzcre.GetAxesCrE();

   // Copy TXYZ axes
   fIsDeleteTXYZ = vals_txyzcre.IsDeleteTXYZ();
   if (fIsDeleteTXYZ) {
      TUAxesTXYZ *txyz = vals_txyzcre.GetAxesTXYZ();
      if (txyz)
         fAxesTXYZ = txyz->Clone();
      txyz = NULL;
   } else
      fAxesTXYZ = vals_txyzcre.GetAxesTXYZ();

   // Copy fValsTXYZEFormat and fValsTXYZE
   fValsTXYZEFormat = new gENUM_FCNTYPE[GetNCRs()];
   fValsTXYZE = new TUValsTXYZEVirtual*[GetNCRs()];
   for (Int_t i = 0; i < GetNCRs(); ++i) {
      fValsTXYZEFormat[i] = vals_txyzcre.ExtractFormat(i);
      TUValsTXYZEVirtual *tmp = vals_txyzcre.GetValsTXYZE(i);
      if (tmp)
         fValsTXYZE[i] = tmp->Clone();
      else
         fValsTXYZE[i] = NULL;
      tmp = NULL;
   }
}

//______________________________________________________________________________
void TUValsTXYZECr::Initialise(Bool_t is_delete)
{
   //--- Initialises class members.
   //  is_delete         Whether to delete class members or not

   if (is_delete) {
      for (Int_t i = 0; i < GetNCRs(); ++i) {
         if (fValsTXYZE && fValsTXYZE[i]) {
            delete fValsTXYZE[i];
            fValsTXYZE[i] = NULL;
         }
      }

      if (fValsTXYZEFormat) delete[] fValsTXYZEFormat;
      if (fIsDeleteCrE) delete fAxesCrE;
      if (fIsDeleteTXYZ) delete fAxesTXYZ;
      if (fValsTXYZE) delete[] fValsTXYZE;
   }

   fValsTXYZEFormat = NULL;
   fValsTXYZE = NULL;
   fAxesCrE = NULL;
   fAxesTXYZ = NULL;
   fIsDeleteCrE = true;
   fIsDeleteTXYZ = true;
   TUFreeParList::Initialise(is_delete);
}

//______________________________________________________________________________
Double_t TUValsTXYZECr::IntegratedValOnERange(string &combo, gENUM_ETYPE e_type,
      Int_t n_extrapts4integ,
      Double_t &e_min, Double_t &e_max,
      Double_t const &e_power, TUCoordTXYZ *coord_txyz,
      TUSolModVirtual *model_solmod)
{
   //--- Returns 'value*e^{e_power}' for a generic quantity 'combo' (combination of CR
   //    fluxes stored in fValsTXYZE) at a given position (can also be modulated using
   //    model_solmod and par_solmod), where value is either:
   //       - flux calculation at e_min (if e_min=e_max)
   //       - flux integrated in [e_min,e_max] interpolated as power-law in this range,
   //         and integrated separately for all isotopes that are part of 'combo'.
   //    Energy units in the code are kEKN (or kEK for leptons), but the output of
   //    this function can be returned for 'e_type'. The Y-axis is set
   //    correspondingly (flux is in particles/e_unit/m2/s/sr, ratio has no unit).
   //  combo             Quantity to consider, can be:
   //                          - single CR
   //                          - element
   //                          - a ratio (num/denom)
   //                          - keyword <LNA>
   //                          - keyword ALLSPECTRUM
   //  e_type            Energy type of e_grid (among gENUM_ETYPE=kEKN,kR,...)
   //  n_extrapts4integ  Number of intermediate points used for power-law interpolation
   //                       -1 => do not integrate, returns calculation at e_min
   //                        0 => integrate with power-law assumption between e_min,e_max
   //                        n>0 => integrate using n logarithmically spaced points
   //                               between e_min and e_max, assuming power-law in
   //                               all segments
   //  e_min             Lower bound of integration range
   //  e_max             Upper bound of integration range
   //  e_power           Fluxes are multiplied by e_type^{e_power} (if not ratio or <LnA>)
   //  coord_txyz        Coordinates (bins[4] or values[4]) for T, X, Y, Z
   //  model_solmod      Modulation model used to modulate flux (set to NULL for IS fluxes)

   vector<Double_t> l_emin(1, e_min);
   vector<Double_t> l_emax(1, e_max);
   vector<Double_t> res = IntegratedValOnERange(combo, e_type, n_extrapts4integ, l_emin, l_emax, e_power, coord_txyz, model_solmod);
   return res[0];
}
//______________________________________________________________________________
vector<Double_t> TUValsTXYZECr::IntegratedValOnERange(string &combo, gENUM_ETYPE e_type,
      Int_t n_extrapts4integ,
      vector<Double_t> &l_emin, vector<Double_t> &l_emax,
      Double_t const &e_power, TUCoordTXYZ *coord_txyz,
      TUSolModVirtual *model_solmod)
{
   //--- Returns 'value*e^{e_power}' for a generic quantity 'combo' (combination of CR
   //    fluxes stored in fValsTXYZE) at a given position (can also be modulated using
   //    model_solmod and par_solmod), where value is either:
   //       - flux calculation at e_min (if e_min=e_max)
   //       - flux integrated in [e_min,e_max] interpolated as power-law in this range,
   //         and integrated separately for all isotopes that are part of 'combo'.
   //    Energy units in the code are kEKN (or kEK for leptons), but the output of
   //    this function can be returned for 'e_type'. The Y-axis is set
   //    correspondingly (flux is in particles/e_unit/m2/s/sr, ratio has no unit).
   //  combo             Quantity to consider, can be:
   //                          - single CR
   //                          - element
   //                          - a ratio (num/denom)
   //                          - keyword <LNA>
   //                          - keyword ALLSPECTRUM
   //  e_type            Energy type of e_grid (among gENUM_ETYPE=kEKN,kR,...)
   //  n_extrapts4integ  Number of intermediate points used for power-law interpolation
   //                       -1 => do not integrate, returns calculation at e_min
   //                        0 => integrate with power-law assumption between e_min,e_max
   //                        n>0 => integrate using n logarithmically spaced points
   //                               between e_min and e_max, assuming power-law in
   //                               all segments
   //  e_min             Lower bound of integration range
   //  e_max             Upper bound of integration range
   //  e_power           Fluxes are multiplied by e_type^{e_power} (if not ratio or <LnA>)
   //  coord_txyz        Coordinates (bins[4] or values[4]) for T, X, Y, Z
   //  model_solmod      Modulation model used to modulate flux (set to NULL for IS fluxes)
   //

   Int_t n_pts = l_emin.size();

   // Check l-emin and l-emax have same size
   if (n_extrapts4integ >= 0 && l_emin.size() != l_emax.size())
      TUMessages::Error(stdout, "TUValsTXYZECr", "IntegratedValOnERange",
                        "Not same number of entries in l_emin and l_emax");

   // Retrieve common e_grid fr combo and e_type
   TUAxis *e_grid = OrphanGetECombo(combo, e_type);
   vector<Double_t> res(n_pts, 0.);

   // Get modulated flux
   Double_t *jtoa = OrphanVals(combo, e_grid, e_type, e_power, coord_txyz, model_solmod);
   //PrintVals(stdout, combo, e_grid, e_type, e_power, coord_txyz, model_solmod);

   //--- If no integration, uses values in l_emin
   if (n_extrapts4integ < 0) {

      // Calculate for all desired energies
      for (Int_t p = 0; p < n_pts; ++p) {
         Int_t kk = TUMath::BinarySearch(e_grid->GetN(), e_grid->GetVals(), l_emin[p]);
         res[p] = TUMath::Interpolate(l_emin[p], e_grid->GetVal(kk), e_grid->GetVal(kk + 1), jtoa[kk], jtoa[kk + 1], kLOGLOG);
         //cout << p << " " << res[p] << endl;
      }

      // Free memory
      delete e_grid;
      e_grid = NULL;
      delete[] jtoa;
      jtoa = NULL;

      return res;
   }

   //--- If not point calculation, we need to integrate
   // Extract index of each isotope in combo to perform proper calculation
   vector<Int_t> cr_indices;
   Int_t n_denom = 0.;
   vector<Double_t *> jtoa_percr;

   // Extract indices of all isotopes in combo
   // and retrieve their flux on e_grid
   fAxesCrE->ComboToCRIndices(combo, cr_indices, n_denom, false);
   for (Int_t cr = 0; cr < (Int_t)cr_indices.size(); ++cr) {
      jtoa_percr.push_back(OrphanVals(fAxesCrE->GetCREntry(cr_indices[cr]).GetName(), e_grid, e_type, 0., coord_txyz, model_solmod));
      //PrintVals(stdout, fPropagModels[0]->TUAxesCrE::GetCREntry(cr_indices[cr]).GetName(), e_grid, e_type, 0., coord_txyz, model_solmod);
   }

   for (Int_t p = 0; p < n_pts; ++p) {
      Double_t num = 0.;
      Double_t denom = 0.;

      // Proceed only if emin<emax
      if (l_emin[p] > l_emax[p] * 1.00000000001) {
         TUMessages::Error(stdout, "TUValsTXYZECr", "IntegratedValOnERange",
                           Form("l_emin[i]>l_emax[i] for i=%i", p));
      } else if (fabs(l_emin[p] - l_emax[p]) / sqrt(l_emax[p]*l_emin[p]) < 1.e-6) {
         // Sometimes data do not provide an interval
         Int_t kk = TUMath::BinarySearch(e_grid->GetN(), e_grid->GetVals(), l_emin[p]);
         res[p] = TUMath::Interpolate(l_emin[p], e_grid->GetVal(kk), e_grid->GetVal(kk + 1), jtoa[kk], jtoa[kk + 1], kLOGLOG);
      } else {
         // Experiments provide the isotropic flux in the ith E bin as
         // the number of events divided by the bin size. Rather than
         // use the model value at the data mean energy, a more
         // accurate calculation is to compare the data to
         //    model_i = int_{E_i}^{E_{i+1}} { model(E)dE } / (E_{i+1} - E_i)
         //
         // Note that for a combo (e.g., B/C), the above calculation must
         // be performed for all isotopes as they may have different energy
         // dependences.
         //
         // Here, we assume that we have a power-law in the bin, J=a*E^b,
         // so that, at a first approximation,
         //    model_i = [J_{i+1}*E_{i+1}-J_{i}*E_{i}]/{(b+1)*E_{i+1} - E_i}}
         // with
         //    b = ln(J_{i+1}/J_i)/ln(E_{i+1}/E_i)
         // Better approximations should rely on real integration.
         // A simpler approach is to use intermediate points on which
         // the power-law approximation will be more valid: this is
         // controlled by the parameter 'fNExtraInBinRange', so that:
         //    model_i = sum_ranges { [J^r_{i+1}*E^r_{i+1}-J^r_{i}*E^r_{i}]/(b^r+1) } /  (E_up - E_lo)

         // Create ranges in bin for calculation
         TUAxis axis_erange(l_emin[p], l_emax[p], 2 + n_extrapts4integ, "Eaxis", "-", kLOG);

         // Loop on each range
         for (Int_t r = 0; r < axis_erange.GetN() - 1; ++r) {
            Double_t e_lo = axis_erange.GetVal(r);
            Double_t e_up = axis_erange.GetVal(r + 1);

            Int_t kk_lo = TUMath::BinarySearch(e_grid->GetN(), e_grid->GetVals(), e_lo);
            Int_t kk_up = TUMath::BinarySearch(e_grid->GetN(), e_grid->GetVals(), e_up);

            for (Int_t cr = 0; cr < (Int_t)cr_indices.size(); ++cr) {
               Double_t j_lo = 0., j_up = 0., local_slope = 0.;
               // If flux not zero, log-log interpolation
               if (jtoa_percr[cr][kk_lo] > 1.e-50 && jtoa_percr[cr][kk_lo + 1] > 1.e-50 && jtoa_percr[cr][kk_up] > 1.e-50 && jtoa_percr[cr][kk_up + 1] > 1.e-50) {
                  j_lo = TUMath::Interpolate(e_lo, e_grid->GetVal(kk_lo), e_grid->GetVal(kk_lo + 1), jtoa_percr[cr][kk_lo], jtoa_percr[cr][kk_lo + 1], kLOGLOG);
                  j_up = TUMath::Interpolate(e_up, e_grid->GetVal(kk_up), e_grid->GetVal(kk_up + 1), jtoa_percr[cr][kk_up], jtoa_percr[cr][kk_up + 1], kLOGLOG);
                  local_slope = log(j_up / j_lo) / log(e_up / e_lo);
               }
               if (cr < ((Int_t)cr_indices.size() - n_denom))
                  num += (j_up * e_up - j_lo * e_lo) / ((local_slope + 1.));
               else
                  denom += (j_up * e_up - j_lo * e_lo) / ((local_slope + 1.));
               //printf("  local-slope (%s in [%.2le,%.2le]) = %le\n", fPropagModels[0]->TUAxesCrE::GetCREntry(cr_indices[cr]).GetName().c_str(), e_lo, e_up, local_slope);
            }
         }

         // -> Divide by bin size (except if ratio)
         // -> Multiply by e_mean^e_power (except if ratio)
         if (n_denom == 0) {
            res[p] = num / (l_emax[p] - l_emin[p]);
            if (fabs(e_power) > 1.e-3)
               res[p] *= pow(sqrt(l_emin[p] * l_emax[p]), e_power);
         } else if (denom > 1.e-50)
            res[p] = num / denom;
      }
   }
   // Free memory
   for (Int_t cr = 0; cr < (Int_t)cr_indices.size(); ++cr) {
      delete[] jtoa_percr[cr];
      jtoa_percr[cr] = NULL;
   }
   jtoa_percr.clear();
   delete[] jtoa;
   jtoa = NULL;
   delete e_grid;
   e_grid = NULL;

   return res;
}

////______________________________________________________________________________
//TH1D *TUValsTXYZECr::OrphanNuclearFraction(Double_t ekn_gevn, TUCoordTXYZ *coord_txyz,
//      string start, string stop, Bool_t is_element_or_isotope)
//{
//   //--- Returns histogram of element (resp. isotopic) fraction [%] for all
//   //    elements (resp. CRs) at ekn_gevn and position.
//   //  ekn_gevn          Kinetic energy per nucleon [GeV/n]
//   //  coord_txyz        Coordinates (bins[4] or values[4]) for T, X, Y, Z
//   //  start             CR/element name of first entry in histo
//   //  stop              CR/element name of last entry in histo
//   //  is_nuc_or_element Whether histo is formed for isotopic fraction or elemental fraction
//
//   // Get index closest to energy asked for (display purpose)
//   string ekn_name = Form("%.2fGeVn", ekn_gevn);
//   string ekn_title = Form("%.2f [GeV/n]", ekn_gevn);
//   string cr_list = start + "_" + stop;
//
//   string title;
//   string name;
//   TH1D *h = NULL;
//
//   if (is_element_or_isotope) {
//      name = "vals_Zfrac_" + cr_list + "_" + (string)ekn_name;
//      title = "Elemental fraction @ " + (string)ekn_title;
//      Int_t z_min = GetAxesCrE()->ElementNameToZ(start, false, stdout);
//      Int_t z_max = GetAxesCrE()->ElementNameToZ(stop, false, stdout);
//      vector<Int_t> list_z = GetAxesCrE()->ExtractAllZ(false, z_min, z_max);
//      h = fAxesCrE->OrphanEmptyHistoOfElements(name, title, list_z);
//      for (Int_t j = 0; j < (Int_t)list_z.size(); ++j)
//         h->SetBinContent(j + 1, ValueElementFraction(list_z[j], ekn_gevn, coord_txyz));
//      h->SetYTitle("Elemental fraction (%)");
//   } else {
//      name = "vals_isotfrac_" + cr_list + "_" + (string)ekn_name;
//      title = "Isotopic fraction @ " + (string)ekn_title;
//      Int_t jcr_start = GetAxesCrE()->Index(start, true, stdout, true);
//      Int_t jcr_stop = GetAxesCrE()->Index(stop, true, stdout, true);
//      vector<Int_t> list_jcr = GetAxesCrE()->ExtractAllCRs(false, jcr_start, jcr_stop);
//      h = fAxesCrE->OrphanEmptyHistoOfCRs(name, title, list_jcr);
//      for (Int_t j = 0; j < (Int_t)list_jcr.size(); ++j)
//         h->SetBinContent(j + 1, ValueIsotopicFraction(list_jcr[j], ekn_gevn, coord_txyz));
//      h->SetYTitle("Isotopic fraction (%)");
//   }
//
//   h->SetLabelSize(0.05);
//   h->SetMinimum(0.);
//   h->SetMaximum(100.);
//
//   return h;
//}

////______________________________________________________________________________
//TGraph *TUValsTXYZECr::OrphanNuclearFraction(Int_t z_or_index_cr, TUCoordTXYZ *coord_txyz, Bool_t is_element_or_isotope)
//{
//   //--- Returns ROOT TGraph display for element (resp. isotopic) fraction [%] for a
//   //    given element (resp. cr) at a selected position for all energies.
//   //  z_or_index_cr     Z or index of CR to plot
//   //  coord_txyz        Coordinates (bins[4] or values[4]) for T, X, Y, Z
//   //  is_nuc_or_element Whether histo is formed for isotopic fraction or elemental fraction
//
//   // Create and fill TGraph (for all energies)
//   TGraph *gr = new TGraph(fAxesCrE->GetNE());
//   TUAxis *e_grid = fAxesCrE->GetEFamily(kNUC);
//   string cr_name;
//   string title;
//   string name;
//
//   if (is_element_or_isotope) {
//      cr_name = fAxesCrE->ZToElementName(z_or_index_cr);
//      name = "ElementFraction_" + cr_name;
//      title = cr_name + " primary fraction (%)";
//      for (Int_t k = 0; k < e_grid->GetN(); ++k) {
//         Double_t ekn_gevn = e_grid->GetVal(k);
//         gr->SetPoint(k, ekn_gevn, ValueElementFraction(z_or_index_cr, ekn_gevn, coord_txyz));
//      }
//      gr->GetYaxis()->SetTitle("Elemental fraction (%)");
//   } else {
//      cr_name = fAxesCrE->GetCREntry(z_or_index_cr).GetName();
//      name = "IsotopicFraction_" + cr_name;
//      title = cr_name + "primary fraction (%)";
//      for (Int_t k = 0; k < e_grid->GetN(); ++k) {
//         Double_t ekn_gevn = e_grid->GetVal(k);
//         gr->SetPoint(k, ekn_gevn, ValueIsotopicFraction(z_or_index_cr, ekn_gevn, coord_txyz));
//      }
//   }
//
//   e_grid = NULL;
//   gr->SetName(name.c_str());
//   gr->SetTitle(title.c_str());
//   gr->GetXaxis()->SetTitle("Ekn [GeV/n]");
//   gr->GetXaxis()->SetTitleOffset(1.5);
//   gr->GetXaxis()->SetTitleSize(0.03);
//   gr->GetYaxis()->SetTitle(title.c_str());
//   gr->GetYaxis()->SetTitleOffset(2.);
//   gr->GetYaxis()->SetTitleSize(0.03);
//
//   return gr;
//}

//______________________________________________________________________________
Double_t *TUValsTXYZECr::OrphanVals(Int_t bincr, TUAxis *e_grid, gENUM_ETYPE e_type,
                                    Double_t const &e_power, TUCoordTXYZ *coord_txyz)
{
   //--- Returns an array (to be deleted after use) of vals(e)*e^{e_power} for the
   //    bincr-th CR in list at a given time/position. Energy units in the code are
   //    kEKN (or kEK for leptons), but the output of this function can be returned
   //    for an 'e_grid' of 'e_type'. The Y-axis is set correspondingly (flux is in
   //    particles/e_unit/m2/s/sr).
   //  bincr             CR index
   //  e_grid            Energy grid on which to perform calculation
   //  e_type            Energy type of e_grid (among gENUM_ETYPE=kEKN,kR,...)
   //  e_power           Fluxes are multiplied by e_type^{e_power} (if not ratio or <LnA>)
   //  coord_txyz        Coordinates (bins[4] or values[4]) for T, X, Y, Z

   if (!fAxesCrE->GetCREntry(bincr).IsChargedCR()) {
      string message = fAxesCrE->GetCREntry(bincr).GetName() + " is not a charged CR: method non applicable";
      TUMessages::Warning(stdout, "TUValsTXYZECr", "OrphanVals", message);
   }

   // Check that e_grid is not out of native E grid
   fAxesCrE->CheckEGridInERange(bincr, e_grid, e_type, true);

   // Calculate values on this e_grid of e_type
   return OrphanValsCR(bincr, e_grid, e_type, e_power, coord_txyz);
}

//______________________________________________________________________________
Double_t *TUValsTXYZECr::OrphanVals(string combo, TUAxis *e_grid, gENUM_ETYPE e_type,
                                    Double_t const &e_power, TUCoordTXYZ *coord_txyz,
                                    TUSolModVirtual *model_solmod)
{
   //--- Returns an array (to be deleted after use) of vals(e)*e^{e_power} for a
   //    generic quantity 'combo' (combination of CR fluxes stored in fValsTXYZE)
   //    at a given position (can also be modulated using model_solmod and par_solmod).
   //    Energy units in the code are kEKN (or kEK for leptons), but the output of
   //    this function can be returned for an 'e_grid' of 'e_type'. The Y-axis is set
   //    correspondingly (flux is in particles/e_unit/m2/s/sr).
   //    N.B.: this function is the one called for any calculation of the TOA or IS flux.
   //    To ensure the appropriate and 'optimal' e_type is used for the given combo,
   //    the method FillEAxis(combo, e_type) is called (but only once).
   //  combo             Quantity to consider
   //                          - if single CR
   //                          - if not a ratio (does not contain "/")
   //                          - if ratio (num/denom)
   //                          - if <LNA>
   //                          - if ALLSPECTRUM
   //  e_grid            Energy grid on which to perform calculation
   //  e_type            Energy type of e_grid (among gENUM_ETYPE=kEKN,kR,...)
   //  e_power           Fluxes are multiplied by e_type^{e_power} (if not ratio or <LnA>)
   //  coord_txyz        Coordinates (bins[4] or values[4]) for T, X, Y, Z
   //  model_solmod      Modulation model used to modulate flux (set to NULL for IS fluxes)
   //
   // N.B.: combo=<LNA> returns an array (to be deleted after use) of
   //       <LnA>(e)*e^{e_power} at a given position (fluxes modulated
   //       using model_solmod and par_solmod)
   //       BEGIN_LATEX
   //         <ln A>(E_{axis}) = #frac{#sum_i^{all~CR~nuclei} #Phi_i(E_{axis}) ln A_i}{#sum_i^{all~CRs} #Phi_i(E_{axis})}
   //       END_LATEX
   // N.B.: combo=ALLSPECTRUM returns an array (to be deleted after use)
   //       of the all-spectra flux at a given position (fluxes modulated
   //       using model_solmod and par_solmod)
   //       BEGIN_LATEX
   //         #Phi_{all}(E_{tot}) = #sum_i^{all~CR~nuclei} #Phi_i(E_{tot})  in [GeV m^{2} s sr]^{-1}
   //       END_LATEX


   // Extract list of CR indices for this combo (and #CRs in denominator - 0 if not ratio)
   vector<Int_t> cr_indices;
   Int_t n_denom = 0;
   if (!fAxesCrE->ComboToCRIndices(combo, cr_indices, n_denom, false)) {
      string message = "Combo " + combo + " you ask for is not defined";
      TUMessages::Warning(stdout, "TUValsTXYZECr", "OrphanVals", message);
      return NULL;
   }

   // Initialise flux
   Double_t *flux = new Double_t[GetNE()];
   for (Int_t k = GetNE() - 1; k >= 0; --k) flux[k] = 0.;
   //----------------------------//
   //--- <LNA> or ALLSPECTRUM ---//
   //----------------------------//

   string cr_combo = combo;
   TUMisc::UpperCase(cr_combo);
   if (cr_combo == "<LNA>" || cr_combo == "ALLSPECTRUM") {
      // Find indices of CR nuclei
      for (Int_t i = 0; i < GetNCRs(); ++i) {
         // Skip every component that is not of nuclear origin
         if (fAxesCrE->GetCREntry(i).GetFamily() == kNUC)
            cr_indices.push_back(i);
      }

      if (cr_combo == "<LNA>") {
         // Allocate and initialize result;
         Double_t *num = new Double_t[GetNE()];
         Double_t *den = new Double_t[GetNE()];
         for (Int_t k = GetNE() - 1; k >= 0; --k) {
            num[k] = 0.;
            den[k] = 0.;
         }

         for (Int_t i = (Int_t) cr_indices.size() - 1; i >= 0; --i) {
            // For common E-grid, calculate corresponding Ekn grid for current CR
            Double_t *tmp = OrphanValsCR(cr_indices[i], e_grid, e_type, 0., coord_txyz, model_solmod);
            for (Int_t k = GetNE() - 1; k >= 0; --k) {
               num[k] += tmp[k] * log(Double_t(fAxesCrE->GetCREntry(cr_indices[i]).GetA()));
               den[k]  += tmp[k];
            }
            delete[] tmp;
            tmp = NULL;
         }
         for (Int_t k = GetNE() - 1; k >= 0; --k)
            flux[k] = num[k] / den[k];

         delete[] num;
         num = NULL;
         delete[] den;
         den = NULL;
         return flux;

      } else if (cr_combo == "ALLSPECTRUM") {
         // Loop on all nuclei in list
         for (Int_t i = (Int_t) cr_indices.size() - 1; i >= 0; --i) {
            Double_t *tmp = OrphanValsCR(cr_indices[i], e_grid, e_type, e_power, coord_txyz, model_solmod);
            for (Int_t k = GetNE() - 1; k >= 0; --k)
               flux[k] += tmp[k];
            delete[] tmp;
            tmp = NULL;
         }
         return flux;
      }
   }


   //-----------------------//
   //--- ANY OTHER COMBO ---//
   //-----------------------//

   // Add all contributions
   Int_t n_num = (Int_t)cr_indices.size() - n_denom;
   // Numerator
   for (Int_t i = 0; i < n_num; ++i) {
      Double_t *tmp = OrphanValsCR(cr_indices[i], e_grid, e_type, e_power, coord_txyz, model_solmod);
      for (Int_t k = GetNE() - 1; k >= 0; --k)
         flux[k] +=  tmp[k];
      delete[] tmp;
      tmp = NULL;
   }

   // If denominator exists
   if (n_denom != 0) {
      Double_t *flux_denom = new Double_t[GetNE()];
      for (Int_t k = GetNE() - 1; k >= 0; --k) flux_denom[k] = 0.;

      // Calculate denom
      for (Int_t i = n_num; i < (Int_t)cr_indices.size(); ++i) {
         Double_t *tmp = OrphanValsCR(cr_indices[i], e_grid, e_type, e_power, coord_txyz, model_solmod);
         for (Int_t k = GetNE() - 1; k >= 0; --k)
            flux_denom[k] +=  tmp[k];
         delete[] tmp;
         tmp = NULL;
      }

      // Calculate ratio
      for (Int_t k = GetNE() - 1; k >= 0; --k)
         flux[k] /=  flux_denom[k];
      delete[] flux_denom;
      flux_denom = NULL;
   }
   return flux;
}

//______________________________________________________________________________
Double_t *TUValsTXYZECr::OrphanValsCR(Int_t bincr, TUAxis *e_grid, gENUM_ETYPE e_type,
                                      Double_t const &e_power, TUCoordTXYZ *coord_txyz,
                                      TUSolModVirtual *model_solmod)
{
   //--- Returns TOA vals (to be deleted after use) for CR bincr at a given
   //    time/position (vals are modulated using model_solmod and par_solmod).
   //    Energy units in the code are kEKN (or kEK for leptons), but the output of
   //    this function can be returned for an 'e_grid' of 'e_type'. The Y-axis is set
   //    correspondingly (flux is in particles/e_unit/m2/s/sr):
   //       - kEKN: [dflux/dEkn] = particles/(GeV/n)/m2/s/sr  (native unit of flux);
   //       - kEK: [dflux/dEk] = particles/GeV/m2/s/sr  with dflux/dEK = (1/A) * dflux/dEkn;
   //       - kR: [dflux/dR] = particles/GV/m2/s/sr,  with dflux/dR = (beta*Z/A) * dflux/dEkn;
   //       - kETOT: [dflux/dETot] = particles/GeV/m2/s/sr,  with dflux/dEtot = (1/A) * dflux/dEkn.
   //  e_grid            Energy grid on which to perform calculation
   //  e_type            Energy type of e_grid (among gENUM_ETYPE=kEKN,kR,...)
   //  bincr             CR index
   //  e_power           Fluxes are multiplied by e_type^{e_power} (if not ratio or <LnA>)
   //  coord_txyz        Coordinates (bins[4] or values[4]) for T, X, Y, Z
   //  model_solmod      Modulation model used to modulate flux (set to NULL for IS fluxes)


   // Allocate memory for flux to be returned (and deleted after usage)
   // N.B.: flux_ekn contains the result on an Ekn grid, whether it is IS or TOA
   Double_t *flux_ekn = new Double_t[GetNE()];

   // Create E-coordinate and TXYZ coordinates (if necessary)
   TUCoordE *coord_e = new TUCoordE();
   //----------------------------
   //--- STEP 1: Is it IS or TOA?
   //----------------------------
   // => use IS flux or call modulation model
   // N.B.: if IS flux, we can use directly the values at bin, unless different E-grids
   if (!model_solmod) {
      // Get IS flux (vs Ekn)
      if (coord_txyz) {
         for (Int_t bine = 0; bine < GetNE(); ++bine) {
            // Update coord_e
            coord_e->SetBinE(bine);
            // If formula or spline-based, requires EDepVals
            if (ExtractFormat(bincr) == kFORMULA || ExtractFormat(bincr) == kSPLINE)
               fAxesCrE->ConvertBinE2ValsE(bincr, coord_e);
            flux_ekn[bine] = ValueETXYZ(bincr, coord_e, coord_txyz);
         }
      } else {
         for (Int_t bine = 0; bine < GetNE(); ++bine) {
            // Update coord_e
            coord_e->SetBinE(bine);
            // If formula or spline-based, requires EDepVals
            if (ExtractFormat(bincr) == kFORMULA || ExtractFormat(bincr) == kSPLINE)
               fAxesCrE->ConvertBinE2ValsE(bincr, coord_e);
            flux_ekn[bine] = ValueE(bincr, coord_e);
         }
      }
   } else {
      // Get TOA flux (vs Ekn) on 'native' E-grid for bincr (not yet on grid 'eaxis_wanted')
      Double_t *flux_is = new Double_t[GetNE()];

      if (coord_txyz) {
         for (Int_t bine = 0; bine < GetNE(); ++bine) {
            // Update coord_e
            coord_e->SetBinE(bine);
            // If formula or spline-based, requires EDepVals
            if (ExtractFormat(bincr) == kFORMULA || ExtractFormat(bincr) == kSPLINE)
               fAxesCrE->ConvertBinE2ValsE(bincr, coord_e);
            flux_is[bine] = ValueETXYZ(bincr, coord_e, coord_txyz);
         }
      } else {
         for (Int_t bine = 0; bine < GetNE(); ++bine) {
            // Update coord_e
            coord_e->SetBinE(bine);
            // If formula or spline-based, requires EDepVals
            if (ExtractFormat(bincr) == kFORMULA || ExtractFormat(bincr) == kSPLINE)
               fAxesCrE->ConvertBinE2ValsE(bincr, coord_e);
            flux_is[bine] = ValueE(bincr, coord_e);
         }
      }
      // Modulate
      model_solmod->IStoTOA(fAxesCrE->GetCREntry(bincr), GetE(bincr), flux_is, flux_ekn);
      delete[] flux_is;
      flux_is = NULL;
   }

   // Free memory of coord_e and coord_txyz
   delete coord_e;
   coord_e = NULL;
   if (coord_txyz)
      delete coord_txyz;
   coord_txyz = NULL;


   //--------------------------------------
   //--- STEP 2: Convert to 'e_grid|e_axis'
   //--------------------------------------
   // => nothing to do if e_grid=native and e_type=native, otherwise convert/interpolate.
   // N.B.: - GetE(bincr) gives 'native' energy grid (and type);

   // TOA flux returned
   Double_t *fluxtoa_eaxis = NULL;

   // If native units on native E grid, nothing to do!
   // Check if grid asked for 'e_grid' matches the grid for bincr
   Bool_t is_same_egrid = e_grid->IsSameGridValues(GetE(bincr));
   if (e_type != fAxesCrE->GetEType(bincr))
      is_same_egrid = false;
   if (is_same_egrid) {
      fluxtoa_eaxis = flux_ekn;
      flux_ekn = NULL;
   } else {
      // METHOD1: convert and then interpolate
      // -------
      // Convert flux_ekn fluxes from dNdEkn to dNdEtype (on Ekn grid)
      vector<Double_t> eaxis_type(GetNE(), 0.);
      for (Int_t k = 0; k < GetNE(); ++k) {
         Double_t factor = TUPhysics::dNdEin_to_dNdEout(fAxesCrE->GetE(bincr)->GetVal(k), fAxesCrE->GetEType(bincr),
                           e_type, fAxesCrE->GetCREntry(bincr).GetA(), fAxesCrE->GetCREntry(bincr).GetZ(),
                           fAxesCrE->GetCREntry(bincr).GetmGeV());
         flux_ekn[k] *= factor;
         eaxis_type[k] = TUPhysics::ConvertE(fAxesCrE->GetE(bincr)->GetVal(k), fAxesCrE->GetEType(bincr),
                                             e_type, fAxesCrE->GetCREntry(bincr).GetA(), fAxesCrE->GetCREntry(bincr).GetZ(),
                                             fAxesCrE->GetCREntry(bincr).GetmGeV());
      }
      // Interpolate
      fluxtoa_eaxis = new Double_t[GetNE()];
      for (Int_t k = 0; k < GetNE(); ++k) {
         Int_t k_interp = TUMath::BinarySearch(GetNE(), &eaxis_type[0], e_grid->GetVal(k));
         if (k_interp < 0)
            k_interp = 0;
         else if (k_interp > GetNE() - 2)
            k_interp = GetNE() - 2;
         fluxtoa_eaxis[k] = TUMath::Interpolate(e_grid->GetVal(k), eaxis_type[k_interp], eaxis_type[k_interp + 1],
                                                flux_ekn[k_interp], flux_ekn[k_interp + 1], kLOGLOG);
         //printf("R_val=%le in [%le,%le]   flux=%le\n", e_grid->GetVal(k), eaxis_type[k_interp], eaxis_type[k_interp+1],fluxtoa_eaxis[k]);
      }

      /*
      // METHOD2: interpolate and then convert (less accurate)
      // -------
      // We must extrapolate flux values calculated on native E-grid (for bincr)
      // to values on 'e_grid' of 'e_type'. This is done in several steps:
      //    1. Seek k_ekn (on native grid) closest to ekn_val=e_grid[bine]
      //    2. Interpolate dflux/dEkn for e_type
      fluxtoa_eaxis = new Double_t[GetNE()];
      for (Int_t k = 0; k < GetNE(); ++k) {
         // Ekn value from 'e_grid' in 'e_type'
         Double_t ekn_val = TUPhysics::ConvertE(e_grid->GetVal(k), e_type,
                                                fAxesCrE->GetEType(bincr), fAxesCrE->GetCREntry(bincr).GetA(),
                                                fAxesCrE->GetCREntry(bincr).GetZ(), fAxesCrE->GetCREntry(bincr).GetmGeV());
         TUAxis *e_cr = GetE(bincr);
         // Get closest index, but cannot be last bin (otherwise interpolation crashes)
         Int_t k_ekn = e_cr->IndexClosest(ekn_val);
         if (k_ekn < 0)
            k_ekn = 0;
         else if (k_ekn > GetNE() - 2)
            k_ekn = GetNE() - 2;

         fluxtoa_eaxis[k] = TUMath::Interpolate(ekn_val, e_cr->GetVal(k_ekn),
                                                e_cr->GetVal(k_ekn + 1), flux_ekn[k_ekn], flux_ekn[k_ekn + 1], kLOGLOG);
         e_cr = NULL;
         Double_t factor = TUPhysics::dNdEin_to_dNdEout(ekn_val, fAxesCrE->GetEType(bincr),
                           e_type, fAxesCrE->GetCREntry(bincr).GetA(), fAxesCrE->GetCREntry(bincr).GetZ(),
                           fAxesCrE->GetCREntry(bincr).GetmGeV());
         //if (k == 0 || k % 50 == 0 || k == fAxesCrE->GetNE() - 1)
         //   cout << fAxesCrE->GetCREntry(bincr).GetName() << "    k=" << k << "  " << ekn_val << " " << e_grid->GetVal(k) << " " << factor << endl;
         fluxtoa_eaxis[k] *= factor;
      }*/

      // Free memory
      delete[] flux_ekn;
      flux_ekn = NULL;
   }


   //-----------------------------------------------------
   //--- STEP 3: do we have to multiply by e_type^e_power?
   //-----------------------------------------------------
   TUMath::PowerRescale(e_grid->GetVals(), fluxtoa_eaxis, GetNE(), e_power);

   return fluxtoa_eaxis;
}

//______________________________________________________________________________
void TUValsTXYZECr::PrintVals(FILE *f, string const &combo, TUAxis *e_grid, gENUM_ETYPE e_type,
                              Double_t const &e_power, TUCoordTXYZ *coord_txyz,
                              TUSolModVirtual *model_solmod)
{
   //--- Prints Top-Of-Atmosphere(TOA) 'combo' vals (at all energies) for a
   //    given time/position for a given Solar modulation models.
   //    Energy units in the code are kEKN (or kEK for leptons), but the output
   //    of this function can be returned for an 'e_grid' of 'e_type'. The Y-axis
   //    is set correspondingly (flux is in particles/e_unit/m2/s/sr).
   //  f                 File in which to print
   //  combo             CR combination (ratio, sum of isotopes/elements, etc.)
   //  e_grid            Energy grid on which to perform calculation
   //  e_type            Energy type of e_grid (among gENUM_ETYPE=kEKN,kR,...)
   //  e_power           Fluxes are multiplied by e_type^{e_power} (if not ratio, or <LnA>)
   //  coord_txyz        Coordinates (bins[4] or values[4]) for T, X, Y, Z
   //  model_solmod      Vector of Modulation models used to modulate flux

   // Create vector of one Solar modulation model
   vector<TUSolModVirtual *> models_solmod;
   models_solmod.push_back(model_solmod);
   PrintVals(f, combo, e_grid, e_type, e_power, coord_txyz, models_solmod);

   // Clear vector (N.B.: this does not delete the pointers in the vector)
   models_solmod[0] = NULL;
   models_solmod.clear();
}

//______________________________________________________________________________
void TUValsTXYZECr::PrintVals(FILE *f, string const &combo, TUAxis *e_grid, gENUM_ETYPE e_type,
                              Double_t const &e_power, TUCoordTXYZ *coord_txyz,
                              vector<TUSolModVirtual *> &models_solmod)
{
   //--- Prints Top-Of-Atmosphere(TOA) 'combo' vals (at all energies) for a
   //    given time/position using different Solar modulation models.
   //    Energy units in the code are kEKN (or kEK for leptons), but the output
   //    of this function can be returned for an 'e_grid' of 'e_type'. The Y-axis
   //    is set correspondingly (flux is in particles/e_unit/m2/s/sr).
   //  f                 File in which to print
   //  combo             CR combination (ratio, sum of isotopes/elements, etc.)
   //  e_grid            Energy grid on which to perform calculation
   //  e_type            Energy type of e_grid (among gENUM_ETYPE=kEKN,kR,...)
   //  e_power           Fluxes are multiplied by e_type^{e_power} (if not ratio, or <LnA>)
   //  coord_txyz        Coordinates (bins[4] or values[4]) for T, X, Y, Z
   //  models_solmod     Vector of Modulation models used to modulate flux

   //fprintf(f, "\n   %s\n", combo.c_str());

   // Print combo, sol.mod., and fluxes
   Int_t n = models_solmod.size();
   Double_t **flux = new Double_t *[n];
   fprintf(f, "# %s\n", combo.c_str());
   for (Int_t i = 0; i < n; ++i) {
      if (models_solmod[i])
         fprintf(f, "# %s\n", models_solmod[i]->UID().c_str());
      flux[i] = OrphanVals(combo, e_grid, e_type, e_power, coord_txyz, models_solmod[i]);
   }

   string unit = "[-]";
   if (!TUMisc::IsRatio(combo))
      unit = TUEnum::CRQUnit(e_type, true);
   if (models_solmod.size() != 0)
      fprintf(f, "# %-10s   TOA %s\n", TUEnum::Enum2NameUnit(e_type).c_str(), unit.c_str());
   else
      fprintf(f, "# %-10s   IS %s\n", TUEnum::Enum2NameUnit(e_type).c_str(), unit.c_str());


   for (Int_t k = 0; k < GetNE(); ++k) {
      fprintf(f, "%.5le    ", e_grid->GetVal(k));
      for (Int_t i = 0; i < n; ++i)
         fprintf(f, "%.5le    ", flux[i][k]);
      fprintf(f, "\n");
   }
   fprintf(f, "\n");

   for (Int_t i = 0; i < n; ++i) {
      delete[] flux[i];
      flux[i] = NULL;
   }
   delete[] flux;
   flux = NULL;
}

//______________________________________________________________________________
void TUValsTXYZECr::PrintVals(FILE *f, Int_t bincr, TUAxis *e_grid, gENUM_ETYPE e_type,
                              Double_t const &e_power, TUCoordTXYZ *coord_txyz)
{
   //--- Prints Inter-stellar (IS) CR flux (at all energies) for a given time/position.
   //    Energy units in the code are kEKN (or kEK for leptons), but the output
   //    of this function can be returned for an 'e_grid' of 'e_type'. The Y-axis
   //    is set correspondingly (flux is in particles/e_unit/m2/s/sr).
   //  f                 File in which to print
   //  bincr             CR index
   //  e_grid            Energy grid on which to perform calculation
   //  e_type            Energy type of e_grid (among gENUM_ETYPE=kEKN,kR,...)
   //  e_power           Fluxes are multiplied by e_type^{e_power} (if not ratio, or <LnA>)
   //  coord_txyz        Coordinates (bins[4] or values[4]) for T, X, Y, Z

   if (bincr < 0 && bincr >= GetNCRs())
      TUMessages::Warning(f, "TUValsTXYZECr", "PrintIS", "bincr is out of range!");

   Double_t *flux = OrphanVals(bincr, e_grid, e_type, e_power, coord_txyz);
   fprintf(f, "# %s\n", (fAxesCrE->GetCREntry(bincr).GetName()).c_str());
   fprintf(f, "# %-10s   IS %s\n", TUEnum::Enum2NameUnit(e_type).c_str(), TUEnum::CRQUnit(e_type, true).c_str());
   for (Int_t k = 0; k < GetNE(); ++k)
      fprintf(f, "%.5le    %.5le    \n", e_grid->GetVal(k), flux[k]);
   fprintf(f, "\n");

   delete[] flux;
   flux = NULL;
}


//______________________________________________________________________________
void TUValsTXYZECr::PrintVals(FILE *f, TUCoordTXYZ *coord_txyz)
{
   //--- Prints Inter-stellar (IS) CR fluxes (at all energies, all species)
   //    for a given time/position.
   //  f                 File in which to print
   //  coord_txyz        Coordinates (bins[4] or values[4]) for T, X, Y, Z

   for (Int_t j_cr = GetNCRs() - 1; j_cr >= 0; --j_cr)
      PrintVals(f, j_cr, GetE(j_cr), fAxesCrE->GetEType(j_cr), 0., coord_txyz);
}

//______________________________________________________________________________
void TUValsTXYZECr::SetValsE_Formula(Int_t bincr, string const &fparser_formula,
                                     Bool_t is_verbose, FILE *f_log, string const &commasep_vars,
                                     TUFreeParList *free_pars, Bool_t is_use_or_copy_freepars)
{
   //--- Sets Vals description (formula) for CR bincr (no spatial dependence, only E).
   //  bincr                    CR index for which to apply formula
   //  is_verbose               Verbose on or off
   //  f_log                    Log for USINE
   //  fparser_formula          Formula to implement (see http://warp.povusers.org/FunctionParser/fparser.html)
   //  commasep_vars            List of comma-separated variables, e.g., "t,x,y,z" (default is no var)
   //  free_pars                List of free parameters (used only if formula and free_pars!=NULL)
   //  is_use_or_copy_freepars  Whether the parameters passed must be added (copied) or just used (pointed at)

   if (fValsTXYZE[bincr]) delete fValsTXYZE[bincr];
   fValsTXYZE[bincr] = new TUValsTXYZEFormula();
   fValsTXYZE[bincr]->SetClass(fparser_formula, is_verbose, f_log, commasep_vars, free_pars, is_use_or_copy_freepars);
   fValsTXYZEFormat[bincr] = kFORMULA;
}

//______________________________________________________________________________
void TUValsTXYZECr::SetValsE_Grid(Int_t bincr, string const &file, Bool_t is_verbose, FILE *f_log)
{
   //--- Sets values (from file) for CR bincr (no spatial dependence, only E).
   //  bincr             CR index for which to apply formula
   //  file              File name
   //  is_verbose        Verbose on or off
   //  f_log                    Log for USINE

   if (fValsTXYZE[bincr]) delete fValsTXYZE[bincr];
   fValsTXYZE[bincr] = new TUValsTXYZEGrid();
   fValsTXYZE[bincr]->SetClass(file, is_verbose, f_log);
   fValsTXYZEFormat[bincr] = kGRID;
}

//______________________________________________________________________________
void TUValsTXYZECr::SetValsE_Grid(Int_t bincr, TUValsTXYZEGrid *vals_grid, Bool_t is_use_or_copy)
{
   //--- Sets values (from file) for CR bincr (no spatial dependence, only E).
   //  bincr             CR index for which to apply formula
   //  vals_grid         Grid values (must be same as class grid)
   //  is_use_or_copy    Whether to use vals_grid or copy it
   //  is_verbose        Verbose on or off

   if (fValsTXYZE[bincr]) delete fValsTXYZE[bincr];
   if (is_use_or_copy)
      fValsTXYZE[bincr] = vals_grid;
   else
      fValsTXYZE[bincr] = vals_grid->Clone();
   fValsTXYZEFormat[bincr] = kGRID;
}

//______________________________________________________________________________
void TUValsTXYZECr::SetValsE_Grid(Int_t bincr, vector<Double_t> const &vals)
{
   //--- Sets vals (no spatial dependence, only E) for CR bincr from a vector.
   //  bincr                    CR index for which to apply formula
   //  vals                     [N] values to copy

   // Allocate NE for energy grid
   TUValsTXYZEGrid *grid = new TUValsTXYZEGrid();
   grid->AllocateVals(GetNE());
   grid->SetValsE(fAxesCrE, bincr, vals);

   // USe grid
   fValsTXYZE[bincr] = grid;
   fValsTXYZEFormat[bincr] = kGRID;
   grid = NULL;
}

//______________________________________________________________________________
void TUValsTXYZECr::Spectrum(string combo, TUAxis *e_grid, gENUM_ETYPE e_type, Double_t const &e_power,
                             vector<Double_t> &x, vector<Double_t> &y,
                             TUCoordTXYZ *coord_txyz, TUSolModVirtual *model_solmod)
{
   //--- Returns fluxes for given combo, position, energy grid, and solar modulation model.
   // INPUTS:
   //  combo             CR combination (ratio, sum of isotopes/elements, etc.)
   //  e_grid            Energy grid on which to calculate (TUAxis)
   //  e_type            Energy type of e_grid (among gENUM_ETYPE=kEKN,kR,...)
   //  e_power           Fluxes are multiplied by e_type^{e_power} (if not ratio or <LnA>)
   //  coord_txyz        Coordinates at which to calculate (pass NULL to calculate flux at SS position)
   //  model_solmod      Modulation model used to modulate flux (set to NULL for IS fluxes)
   // OUTPUTS:
   //  x                [NE] Energy values (in e_type unit)
   //  y                [NE] TOA combo flux

   x.clear();
   y.clear();

   // Extract energy grid
   for (Int_t i = 0; i < e_grid->GetN(); ++i)
      x.push_back(e_grid->GetVal(i));

   // Retrieve flux and then copy in vector<Double_t> (and delete orphan)
   Double_t *tmp = OrphanVals(combo, e_grid, e_type, e_power, coord_txyz, model_solmod);
   y = TUMisc::Array2Vector(tmp, e_grid->GetN());
   delete[] tmp;
   tmp = NULL;
}

//______________________________________________________________________________
void TUValsTXYZECr::TEST(TUInitParList *init_pars, FILE *f)
{
   //--- Test function for this class (called in usine -t).
   //  init_pars         TUInitParList class of initialisation parameters
   //  f                 Output file for test

   string message = "Stores space-time dependent IS (interstellar)\n"
                    "fluxes for all CR species and energy grids 'fAxesCrE'.\n"
                    "The class enables to retrieve a flux or a ratio formed\n"
                    "from any combination of CR in list (e.g. B/C, C+O, etc.).\n"
                    "The class also contains methods using Solar modulation\n"
                    "routines to provide TOA (top of atmosphere) fluxes for\n"
                    "these combinations.";
   TUMessages::Test(f, "TUValsTXYZECr", message);
   // Set class and test methods
   fprintf(f, " * Set and print base axes\n");
   fprintf(f, "   > AllocateForAxesTXYZCrE(init_pars=\"%s\", group=Model0DLeakyBox, f_out=f);\n",
           init_pars->GetFileNames().c_str());
   AllocateForAxesTXYZCrE(init_pars, "Model0DLeakyBox", f);
   fprintf(f, "\n");

   fprintf(f, " * Allocate and set vals for 1H, 2H, 3He et 4He (TXYZ=NULL)\n");
   Int_t i_p = fAxesCrE->Index("1H");
   fprintf(f, "   > int i_p = fAxesCrE->Index(1H);     => i_p  = %d\n", i_p);
   Int_t i_2h = fAxesCrE->Index("2H");
   fprintf(f, "   > int i_2h = fAxesCrE->Index(2H);    => i_2h = %d\n", i_2h);
   Int_t i_3he = fAxesCrE->Index("3He");
   fprintf(f, "   > int i_3he = fAxesCrE->Index(3He);  => i_3he= %d\n", i_3he);
   Int_t i_4he = fAxesCrE->Index("4He");
   fprintf(f, "   > int i_4he = fAxesCrE->Index(4He);  => i_4he= %d\n", i_4he);

   if (i_p >= 0 && i_2h >= 0 && i_3he >= 0 && i_4he >= 0) {
      vector<Double_t> vals_p, vals_2h, vals_3he, vals_4he;
      for (Int_t k = 0; k < fAxesCrE->GetNE(); ++k) {
         vals_p.push_back(pow(fAxesCrE->GetE(i_p, k, kR), -2.8));
         vals_2h.push_back(0.5 * pow(fAxesCrE->GetE(i_2h, k, kR), -3.3));
         vals_3he.push_back(0.05 * pow(fAxesCrE->GetE(i_3he, k, kR), -3.3));
         vals_4he.push_back(0.1 * pow(fAxesCrE->GetE(i_4he, k, kR), -2.8));
      }

      fprintf(f, "   > SetValsE_Grid(i_p, vals_from_pow(R,-2.8));\n");
      SetValsE_Grid(i_p, vals_p);
      fprintf(f, "   > SetValsE_Grid(i_2h, vals_from_0.5*pow(R,-3.3));\n");
      SetValsE_Grid(i_2h, vals_2h);
      fprintf(f, "   > SetValsE_Grid(i_3he, vals_from_0.05*pow(x,-3.3));\n");
      SetValsE_Grid(i_3he, vals_3he);
      fprintf(f, "   > SetValsE_Grid(i_4he, vals_from_0.1*pow(x,-2.8);\n");
      SetValsE_Grid(i_4he, vals_4he);
   }

   TUCoordTXYZ *coord_txyz = NULL;
   fprintf(f, " * Create Force-field model and check print (modulate or IS, assuming vals is a flux)\n");
   fprintf(f, "   > TUSolMod0DFF *ff = new TUSolMod0DFF();\n");
   TUSolMod0DFF *ff = new TUSolMod0DFF();
   fprintf(f, "   > ff->SetClass(init_pars);\n");
   ff->SetClass(init_pars, false, f);

   if (i_p >= 0 || i_2h >= 0) {
      fprintf(f, "   > TUAxis *r_grid = fAxesCrE->OrphanGetE(i_p, kR, kLOG);\n");
      TUAxis *r_grid = fAxesCrE->OrphanGetE(i_p, kR, kLOG);
      fprintf(f, "   > TUAxis *native_grid = fAxesCrE->GetE(i_p);\n");
      TUAxis *native_grid = fAxesCrE->GetE(i_p);
      fprintf(f, "   > PrintVals(f, i_p, r_grid, kR, e_power=0);\n");
      PrintVals(f, i_p, r_grid, kR);
      fprintf(f, "   > PrintVals(f, i_p, native_grid, kEKN, e_power=0);\n");
      PrintVals(f, i_p, native_grid, kEKN);
      fprintf(f, "   > PrintVals(f, i_p, r_grid, kR, e_power=2.8);\n");
      PrintVals(f, i_p, r_grid, kR, 2.8);
      fprintf(f, "   > PrintVals(f, H, native_grid, kEKN, e_power=2.8);\n");
      PrintVals(f, "H", native_grid, kEKN, 2.8);

      fprintf(f, "   > PrintVals(f, combo=1H, native_grid, kEKN, index=0, coord_txyz=NULL, ff);\n");
      PrintVals(f, "1H", native_grid, kEKN, 0., coord_txyz, ff);
      fprintf(f, "   > PrintVals(f, combo=1H, native_grid, kEKN, index=2, coord_txyz=NULL, ff);\n");
      PrintVals(f, "1H", native_grid, kEKN, 2., coord_txyz, ff);
      fprintf(f, "   > PrintVals(f, combo=1H, r_grid, kR, index=0, coord_txyz=NULL, ff);\n");
      PrintVals(f, "1H", r_grid, kR, 0., coord_txyz, ff);
      fprintf(f, "   > PrintVals(f, combo=H, native_grid, kEKN, index=0, coord_txyz=NULL, ff);\n");
      PrintVals(f, "H", native_grid, kEKN, 0., coord_txyz, ff);
      fprintf(f, "   > PrintVals(f, combo=H, native_grid, kEKN, index=0, coord_txyz=NULL, ff);\n");
      PrintVals(f, "H", native_grid, kEKN, 0., coord_txyz, ff);

      fprintf(f, "   > TUAxis *combo_grid = fAxesCrE->OrphanGetECombo(H/He, kR, f);\n");
      TUAxis *combo_grid = fAxesCrE->OrphanGetECombo("H/He", kR, f);
      fprintf(f, "   > PrintVals(f, combo=H/He, combo_grid, kR, index=0, coord_txyz=NULL, ff);\n");
      PrintVals(f, "H/He", combo_grid, kR, 0., coord_txyz, ff);
      fprintf(f, "\n");
      delete combo_grid;
      delete r_grid;
   }
   fprintf(f, "\n");

   // Copy
   fprintf(f, " * Test Copy method:\n");
   fprintf(f, "   > TUValsTXYZECr vals; vals.Copy(*this);\n");
   TUValsTXYZECr vals;
   vals.Copy(*this);
   if (i_p >= 0 || i_2h >= 0) {
      TUAxis *combo_grid = fAxesCrE->OrphanGetECombo("H/He", kR, f);
      fprintf(f, "   > vals.PrintVals(f, combo=H/He, combo_grid, kR, index=0, coord_txyz=NULL, ff);\n");
      vals.PrintVals(f, "H/He", combo_grid, kR, 0., coord_txyz, ff);
      fprintf(f, "\n");
      delete combo_grid;
   }
   delete ff;
   ff = NULL;
}

//______________________________________________________________________________
void TUValsTXYZECr::UpdateFromFreeParsAndResetStatus()
{
   //--- Updates all formulae from updated fFreePars values and reset fFreePars status.


   // If status of parameters has changed
   if (fValsTXYZE && GetFreePars() && GetFreePars()->GetParListStatus()) {

      // Update for all CRs
      for (Int_t i = 0; i < GetNCRs(); ++i) {
         if (fValsTXYZE[i])
            fValsTXYZE[i]->UpdateFromFreeParsAndResetStatus(true);
      }
      GetFreePars()->ResetStatusParsAndParList();
   }
}

//______________________________________________________________________________
Double_t TUValsTXYZECr::ValueElementFraction(Int_t z_element, Double_t const &ekn_gevn,
      TUCoordTXYZ *coord_txyz)
{
   //--- Returns element fraction [%] for z_element@(ekn,coord_txyz):
   //       frac=val(Z)/sum_{all Z} val(Z).
   //  z_element         Charge of the CR element
   //  ekn_gevn          Kinetic energy per nucleon [GeV/n]
   //  coord_txyz        Coordinates (bins[4] or values[4]) for T, X, Y, Z

   // Get index closest to energy asked (to fill E-coordinate)
   TUAxis *e_grid = fAxesCrE->GetEFamily(kNUC);
   TUCoordE coord_e;
   coord_e.SetBinE(e_grid->IndexClosest(ekn_gevn));
   e_grid = NULL;

   // Loop on all crs (to calculate total contrib.)
   Double_t tot = 0.;
   for (Int_t j_cr = 0; j_cr < fAxesCrE->GetNCRs(); ++j_cr) {
      // Update E-coord and fill
      fAxesCrE->ConvertBinE2ValsE(j_cr, &coord_e, true);
      tot += ValueETXYZ(j_cr, &coord_e, coord_txyz);
   }

   // Find isotope indices (for element z_element)
   string element = fAxesCrE->ZToElementName(z_element);
   vector<Int_t> cr_indices;
   fAxesCrE->ElementNameToCRIndices(element, cr_indices, false);

   // Loop on isotopes to calculate total contrib.
   Double_t elem = 0.;
   for (Int_t j = 0; j < (Int_t)cr_indices.size(); ++j) {
      // Update E-coord and fill
      fAxesCrE->ConvertBinE2ValsE(j, &coord_e, true);
      elem += ValueETXYZ(cr_indices[j], &coord_e, coord_txyz);
   }

   return elem / tot * 100.;
}

//______________________________________________________________________________
Double_t TUValsTXYZECr::ValueIsotopicFraction(Int_t j_cr, Double_t const &ekn_gevn, TUCoordTXYZ *coord_txyz)
{
   //--- Returns isotopic fraction [%] for j_cr@(ekn,coord_txyz):
   //       frac=val(j_cr)/(sum_{i_cr} val(Z(i_cr)=Z(j_cr))).
   //  j_cr              CR index
   //  ekn_gevn          Kinetic energy per nucleon [GeV/n]
   //  coord_txyz        Coordinates (bins[4] or values[4]) for T, X, Y, Z

   // Get index closest to energy asked (to fill E-coordinate)
   TUAxis *e_grid = fAxesCrE->GetEFamily(kNUC);
   TUCoordE coord_e;
   coord_e.SetBinE(e_grid->IndexClosest(ekn_gevn));
   e_grid = NULL;

   // Find isotope indices (for j_cr)
   string element = fAxesCrE->CRToElementName(j_cr);
   vector<Int_t> cr_indices;
   fAxesCrE->ElementNameToCRIndices(element, cr_indices, false);

   // Loop on isotopes to calculate total contrib.
   Double_t tot = 0.;
   for (Int_t j = 0; j < (Int_t)cr_indices.size(); ++j) {
      // Update E-coord and fill
      fAxesCrE->ConvertBinE2ValsE(cr_indices[j], &coord_e, true);
      tot += ValueETXYZ(cr_indices[j], &coord_e, coord_txyz);
   }

   fAxesCrE->ConvertBinE2ValsE(j_cr, &coord_e, true);
   return ValueETXYZ(j_cr, &coord_e, coord_txyz) * 100. / tot;
}