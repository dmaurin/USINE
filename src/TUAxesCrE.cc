// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

// C/C++ include
#include <iostream>
// ROOT include
// USINE include
#include "../include/TUAxesCrE.h"
#include "../include/TUCRList.h"
#include "../include/TUMath.h"
#include "../include/TUMessages.h"

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TUAxesCrE                                                            //
//                                                                      //
// CR list (charts and parents) and energy axes for each CR family.     //
//                                                                      //
// The class TUAxesCrE provides energy grids (based on TUAxis) and      //
// a CR grid (inherited from TUCRList). Note that there are gN_CRFAMILY //
// energy grids for as many CR families (see TUEnum.h and below), //
// but with the same number of bins for each grid:                      //
//    - kNUC => grid for CR nuclei;
//    - kANTINUC => grid for anti-nuclei (because 1H-bar
//      are produced from higher energy 1H and 4He);
//    - kLEPTON => common grid for electrons and positrons;
// Obviously, these grids are filled only if the CR list contains the   //
// appropriate family.                                                  //
//                                                                      //
// Other important members are :                                        //
//    - fEVarsNames: names of the energy-dependant variable that can be
//      used in USINE formula (for spectra, IS fluxes, etc.). Currently,
//      we have beta, gamma, p, Rig, Ek, Ekn, Etot.
//    - fEVars: for each CR, values of the above variables (fEvarsNames)
//      on their respective energy grids.
//    - fEVarsHalfBin: same, but at half-step (-1/2,1/2,...N+1/2);
//      half-upper bins are for instance used when solving second-order
//      differential equation on an energy grid.
//                                                                      //
// Among the methods of the class, energy axes can be reached from the  //
// CR name, or the CR family. It is also possible to get for a CR name, //
// a CR family, or a combo (of CRs), the 'axis' given as a function of  //
// (see E type in TUEnum.h, i.e. gENUM_ETYPE):                          //
//    - kEKN => Kinetic energy per nucleon;
//    - kEN => Kinetic energy;
//    - kR => Rigidity (=p/Z);
//    - kETOT => Total energy.
// This is useful for display purposes (and comparison to data). For    //
// leptons, kEKN stands for the kinetic energy.                         //
//                                                                      //
// Note that along with the spatial and time axes given elsewhere       //
// (TUAxesTXYZ), this fully characterise all the dimensions of the      //
// arrays used for propagation (NT, NX, NY, NZ, NE[gN_CRFAMILY], NCRs). //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

ClassImp(TUAxesCrE)

//______________________________________________________________________________
TUAxesCrE::TUAxesCrE() : TUCRList()
{
   // ****** Default constructor ******

   InitialiseE(false);
}

//______________________________________________________________________________
TUAxesCrE::TUAxesCrE(TUAxesCrE const &axes_cre) : TUCRList()
{
   // ****** Copy constructor ******
   //  axes_cr           Object to copy from

   InitialiseE(false);
   Copy(axes_cre);
}

//______________________________________________________________________________
TUAxesCrE::~TUAxesCrE()
{
   // ****** Default destructor ******
   // (delete only if required)

   InitialiseE(true);
}

//______________________________________________________________________________
void TUAxesCrE::AllocateAndFillENamesAndEVars()
{
   //--- Allocates memory and fills fEVars[NCRs*NE*NEVars] for a E-dependent variables.


   // Load Evar names and units
   LoadEVarsNamesAndUnits(false, stdout);

   if (GetNCRs()*GetNE()*GetNEVars() == 0)
      return;

   // Allocate and initialise (with -1)
   if (fEVars) delete[] fEVars;
   if (fEVarsHalfBin) delete[] fEVarsHalfBin;
   fEVars = new Double_t[GetNCRs()*GetNE()*GetNEVars()];
   fEVarsHalfBin = new Double_t[GetNCRs() * (GetNE() + 1)*GetNEVars()];

   // Fill or all CRs
   for (Int_t j = 0; j < GetNCRs(); ++j) {
      for (Int_t i_evar = 0; i_evar < GetNEVars(); ++i_evar) {
         for (Int_t k = 0; k < GetNE(); ++k) {
            SetEVar(j, k, i_evar, CalculateEVarsVal_Rules(j, GetE(j)->GetVal(k), i_evar));
            SetEVarHalfBin(j, k, i_evar, CalculateEVarsVal_Rules(j, GetE(j)->GetVal(k, -1), i_evar));
         }
         // Last entry (fEVarsHalfBin has N+1 energies) is the half-upper bin!
         SetEVarHalfBin(j, GetNE(), i_evar, CalculateEVarsVal_Rules(j, GetE(j)->GetVal(GetNE() - 1, +1), i_evar));
      }
   }
}

//______________________________________________________________________________
Double_t TUAxesCrE::CalculateEVarsVal_Rules(Int_t bincr, Double_t const &e_native, Int_t i_evar) const
{
   //--- Calculates value of i_evar-th E variable at a given energy per nucleon for a CR.
   //  bincr             CR index
   //  e_native          'Energy' in native unit for bincr species (Ekn for NUCLEI, Ek for leptons...)
   //  i_evar            Index of E-dep variable

   // (anti-)nuclei and leptons (for which we set Ekn=Ek)
   if (i_evar == fIndexBeta)
      return TUPhysics::Beta_mAEkn(GetCREntry(bincr).GetmGeV(), GetCREntry(bincr).GetA(), e_native);
   else if (i_evar == fIndexGamma)
      return TUPhysics::Gamma_mAEkn(GetCREntry(bincr).GetmGeV(), GetCREntry(bincr).GetA(), e_native);
   else if (i_evar == fIndexp)
      return TUPhysics::Ekn_to_p(e_native, GetCREntry(bincr).GetA(), GetCREntry(bincr).GetmGeV());
   else if (i_evar == fIndexRig)
      return TUPhysics::Ekn_to_R(e_native, GetCREntry(bincr).GetA(), GetCREntry(bincr).GetmGeV(), GetCREntry(bincr).GetZ());
   else if (i_evar == fIndexEk)
      return TUPhysics::Ekn_to_Ek(e_native, GetCREntry(bincr).GetA());
   else if (i_evar == fIndexEkn)
      return e_native;
   else if (i_evar == fIndexEtot)
      return TUPhysics::Ekn_to_E(e_native, GetCREntry(bincr).GetA(), GetCREntry(bincr).GetmGeV());
   return -1;
}

//______________________________________________________________________________
void TUAxesCrE::CheckEGridInERange(Int_t bincr, TUAxis *e_grid,  gENUM_ETYPE e_type, Bool_t is_abort) const
{
   //--- Checks that 'e_grid' of type 'e_type' is in CR (bincr) native E-grid range.
   //    If not, abort (or not :-).
   //  bincr             CR index
   //  e_grid            Energy grid
   //  e_type            Selects e axis among gENUM_ETYPE (kEKN, kR,...)
   //  is_abort          If check fails, abort


   if (!e_grid)
      TUMessages::Error(stdout, "TUAxesCrE", "CheckEGridInERange", "Cannot check, e_grid=NULL!");

   Double_t emin = GetE(bincr, 0, e_type);
   Double_t emax = GetE(bincr, GetNE() - 1, e_type);

   if (e_grid->GetMin() < emin || e_grid->GetMax() > emax) {
      string message = "e_grid of type " + TUEnum::Enum2Name(e_type)
                       + " out of range of native energy grid";
      cout << "e_grid asked for:" << endl;
      e_grid->PrintSummary();
      cout << "Reference grid:" << endl;
      GetE(bincr)->PrintSummary();
      if (is_abort)
         TUMessages::Error(stdout, "TUAxesCrE", "CheckEGridInERange", message);
      else
         TUMessages::Warning(stdout, "TUAxesCrE", "CheckEGridInERange", message);
   }
}

//______________________________________________________________________________
void TUAxesCrE::ConvertBinE2ValsE(Int_t bincr, TUCoordE *coord_e, Bool_t is_use_or_copy) const
{
   //--- Calculates and set in coord_e 'ValsE' from its BinE and CR index.
   // INPUTS:
   //  bincr             CR index
   //  coord_e           Energy coordinate index
   //  is_use_or_copy    Whether ValsE points to existing value or is copied
   // OUTPUT:
   //  coord_e           Energy coordinate 'ValsE'

   // Nothing to do if BinE in coord_e not assigned
   if (coord_e->GetBinE() < 0)
      return;

   // If half-lower bin (E) for BinE = 0, not tabulated
   // in axes_cre, must be explicitly calculated
   coord_e->SetValsE(GetEVarsVals(bincr, coord_e->GetBinE(), coord_e->GetHalfBinStatus()),
                     GetNEVars(), coord_e->GetHalfBinStatus(), is_use_or_copy);
}

//______________________________________________________________________________
void TUAxesCrE::Copy(TUAxesCrE const &axes_cre)
{
   //--- Copies axes_cre in current class.
   //  axes_cre          Object to copy from

   // Copy CR list
   TUCRList::Copy(axes_cre);

   // Copy E axes
   InitialiseE(true);
   for (Int_t i = 0; i < gN_CRFAMILY; ++i) {
      fE[i] = new TUAxis();
      TUAxis *tmp = axes_cre.GetEFamily(TUEnum::gENUM_CRFAMILY_LIST[i]);
      if (tmp)
         fE[i]->Copy(*tmp);
      else
         fE[i] = NULL;
      tmp = NULL;
   }

   // Names, indices and unit are the same from on TUAxesCrE to another
   if (fEVars) delete[] fEVars;
   if (fEVarsHalfBin) delete[] fEVarsHalfBin;
   LoadEVarsNamesAndUnits(false, stdout);
   Int_t ne = axes_cre.GetNE();
   fEVars = new Double_t[GetNCRs()*ne * GetNEVars()];
   fEVarsHalfBin = new Double_t[GetNCRs() * (ne + 1)*GetNEVars()];
   // Fill or all CRs
   for (Int_t j = 0; j < GetNCRs(); ++j) {
      for (Int_t i_evar = 0; i_evar < GetNEVars(); ++i_evar) {
         for (Int_t k = 0; k < ne; ++k) {
            SetEVar(j, k, i_evar, axes_cre.GetEVarVal(j, k, i_evar, 0));
            SetEVarHalfBin(j, k, i_evar, axes_cre.GetEVarVal(j, k, i_evar, -1));
         }
         // Last entry (fEVarsHalfBin has N+1 energies) is the half-upper bin!
         SetEVarHalfBin(j, ne, i_evar, axes_cre.GetEVarVal(j, ne - 1, i_evar, +1));
      }
   }
}

//______________________________________________________________________________
void TUAxesCrE::EvalEVarsVals(Int_t bincr, Double_t ekn_gevn, vector<Double_t> &evars_vals) const
{
   //--- Fills 'evars_vals' for ekn_gevn (useful if not on the class energy grid).
   // INPUTS:
   //  bincr             CR index
   //  ekn_gevn          Energy per nucleon [GeV/n]
   // OUTPUT:
   //  evars_vals        EVars[NEvars] values @ Ekn

   evars_vals.clear();
   for (Int_t i_evar = 0; i_evar < GetNEVars(); ++i_evar)
      evars_vals.push_back(CalculateEVarsVal_Rules(bincr, ekn_gevn, i_evar));
}

//______________________________________________________________________________
string TUAxesCrE::FormEVarsValsE(Int_t bincr, Int_t bine, Int_t halfbin_status) const
{
   //--- Returns string of fValsE.
   //  bincr             CR index
   //  bine              E index
   //  halfbin_status    Evaluated at bine (0), bine-1/2 (-1), or bine+1/2 (+1)

   TUCoordE e;
   e.SetValsE(GetEVarsVals(bincr, bine, halfbin_status), GetNEVars(), halfbin_status, false);
   return e.FormValsE();
}

//______________________________________________________________________________
Double_t TUAxesCrE::GammaRadBETA_perMyr(Int_t j_cr, Int_t k_ekn) const
{
   //--- Returns gamma_rad= 1/(gamma_lorentz(Ekn)*mean-lifetime) for BETA decay [/Myr]
   //    The mean-lifetime tau and half-live t_{1/2} are related by tau=t_{1/2}/ln(2),
   //       gamma_rad = ln(2) / (gamma_lorentz * t_{1/2}).
   //  j_cr              CR index
   //  k_ekn             CR energy

   if (GetCREntry(j_cr).GetBETAHalfLife() < 1.e-40)
      return 0.;
   else
      return TUMath::Ln2() / (GetGamma(j_cr, k_ekn) * GetCREntry(j_cr).GetBETAHalfLife());
}

//______________________________________________________________________________
Double_t TUAxesCrE::GammaRadEC_perMyr(Int_t j_cr, Int_t k_ekn) const
{
   //--- Returns gamma_rad= 1/(gamma_lorentz(Ekn)*mean-lifetime) for EC decay [/Myr]
   //    The mean-lifetime tau and half-live t_{1/2} are related by tau=t_{1/2}/ln(2),
   //       gamma_rad = ln(2) / (gamma_lorentz * t_{1/2}).
   //
   //  j_cr              CR index
   //  k_ekn             CR energy

   if (GetCREntry(j_cr).GetECHalfLife() < 1.e-40)
      return 0.;
   else
      return TUMath::Ln2() / (GetGamma(j_cr, k_ekn) * GetCREntry(j_cr).GetECHalfLife());
}

//______________________________________________________________________________
Double_t TUAxesCrE::GetE(Int_t bincr, Int_t bine, gENUM_ETYPE e_type, Int_t halfbin_status) const
{
   //--- Returns E-axis value at 'bine' depending on CR and e-axis selected.
   //    We remind that 'native' units are
   //       - Ekn [GeV/n] for kNUC and kANTINUC;
   //       - Ek [GeV] for kLEPTON ('Ekn_lepton'=Ek/1 is also OK);
   //  bincr             CR index
   //  bine              E-axis index
   //  e_type            Selects e axis among gENUM_ETYPE (kEKN, kR,...)
   //  halfbin_status    Evaluated at bine (0), bine-1/2 (-1), or bine+1/2 (+1)

   // Find native EType
   gENUM_CRFAMILY cr_family = GetCREntry(bincr).GetFamily();
   gENUM_ETYPE e_native = GetEType(bincr);
   Double_t e_val = fE[cr_family]->GetVal(bine, halfbin_status);
   return TUPhysics::ConvertE(e_val, e_native, e_type, GetCREntry(bincr).GetA(),
                              GetCREntry(bincr).GetZ(), GetCREntry(bincr).GetmGeV());
}

//______________________________________________________________________________
gENUM_ETYPE TUAxesCrE::GetEType(Int_t bincr) const
{
   //--- Returns EType (energy unit) used implicitly for calculations in USINE
   //    for the various CR species.

   if (GetCREntry(bincr).GetFamily() == kNUC || GetCREntry(bincr).GetFamily() == kANTINUC)
      return kEKN;
   else if (GetCREntry(bincr).GetFamily() == kLEPTON)
      return kEK;
   else {
      string message = "No default E-type defined for " + TUEnum::Enum2Name(GetCREntry(bincr).GetFamily());
      TUMessages::Error(stdout, "TUAxesCrE", "GetEType", message);
   }
   return kEKN;
}

//______________________________________________________________________________
void TUAxesCrE::Initialise(Bool_t is_delete)
{
   //--- Initialises all class members (including inherited TUCRList).
   //  is_delete         Whether to delete or just initialise

   InitialiseE(is_delete);
   TUCRList::Initialise(is_delete);
   fEVarsNames.clear();
   fEVarsUnits.clear();
   fIndexBeta  = -1;
   fIndexGamma = -1;
   fIndexp     = -1;
   fIndexRig   = -1;
   fIndexEk    = -1;
   fIndexEkn   = -1;
   fIndexEtot  = -1;
}

//______________________________________________________________________________
void TUAxesCrE::InitialiseE(Bool_t is_delete)
{
   //--- Initialises E-dependent members.
   //  is_delete         Whether to delete or just initialise

   if (is_delete) {
      if (fEVars) delete[] fEVars;
      if (fEVarsHalfBin) delete[] fEVarsHalfBin;
      for (Int_t i = 0; i < gN_CRFAMILY; ++i) {
         if (fE[i])
            delete fE[i];
      }
   }

   for (Int_t i = 0; i < gN_CRFAMILY; ++i)
      fE[i] = NULL;

   fEVars = NULL;
   fEVarsHalfBin = NULL;
}

//______________________________________________________________________________
Bool_t TUAxesCrE::IsComboPossibleForEGrids(string const &combo, gENUM_CRFAMILY &common_family, FILE *f) const
{
   //--- Set fEVarsNames (names and units) from 'init_pars'.
   // INPUT:
   //  combo             CR combination to test
   //  f                 Output file for chatter
   // OUTPUT:
   //  common_family     CR family energy grid to use

   // Extract all indices of CR involved
   vector<Int_t> cr_indices;
   Int_t n_denom = 0;

   TUCRList::ComboToCRIndices(combo, cr_indices, n_denom, false);
   if (cr_indices.size() == 0) {
      common_family = kNUC;
      return false;
   } else if (cr_indices.size() == 1) {
      common_family = TUCRList::GetCREntry(cr_indices[0]).GetFamily();
      return true;
   }

   // Check that on same E grid for all families found
   // N.B.: first CR is the reference
   TUAxis *ref = GetE(cr_indices[0]);
   common_family = TUCRList::GetCREntry(cr_indices[0]).GetFamily();

   for (Int_t i = 1; i < (Int_t)cr_indices.size(); ++i) {
      TUAxis *axis = GetE(cr_indices[i]);
      if (!ref->IsSameGridValues(axis)) {
         string message = "In combo " + combo + ", " + TUCRList::GetCREntry(cr_indices[0]).GetName()
                          + " (" + TUEnum::Enum2Name(common_family)
                          + ") is not on the same E grid as " + TUCRList::GetCREntry(cr_indices[1]).GetName()
                          + " (" + TUEnum::Enum2Name(TUCRList::GetCREntry(cr_indices[1]).GetFamily())
                          + ") => change E grids in initialisation file to make it work!";
         TUMessages::Warning(f, "TUAxesCrE", "IsComboPossibleForEGrids", message);
         ref = NULL;
         axis = NULL;
         return false;
      }
      axis = NULL;
   }

   ref = NULL;
   return true;
}

//______________________________________________________________________________
void TUAxesCrE::LoadEVarsNamesAndUnits(Bool_t is_verbose, FILE *f_log)
{
   //--- Loads names and units for Energy variables.
   //  is_verbose        Verbose or not when names are set
   //  f_log             Log for USINE

   // Set variable names and indices
   fEVarsNames.clear();
   fEVarsUnits.clear();
   fIndexBeta = 0;
   fEVarsNames.push_back("beta");
   fEVarsUnits.push_back("-");
   fIndexGamma = 1;
   fEVarsNames.push_back("gamma");
   fEVarsUnits.push_back("-");
   fIndexp = 2;
   fEVarsNames.push_back("p");
   fEVarsUnits.push_back("GeV");
   fIndexRig = 3;
   fEVarsNames.push_back("Rig");
   fEVarsUnits.push_back("GV");
   fIndexEk = 4;
   fEVarsNames.push_back("Ek");
   fEVarsUnits.push_back("GeV");
   fIndexEkn = 5;
   fEVarsNames.push_back("Ekn");
   fEVarsUnits.push_back("GeV/n");
   fIndexEtot = 6;
   fEVarsNames.push_back("Etot");
   fEVarsUnits.push_back("GeV");
   string indent = TUMessages::Indent(true);
   if (is_verbose)
      fprintf(f_log, "%s### Energy variables [in TUAxesCrE.cc] available for formulae: %s\n",
              indent.c_str(), GetEVarNames().c_str());
   TUMessages::Indent(false);
}

//______________________________________________________________________________
TUAxis *TUAxesCrE::OrphanGetECombo(string const &combo, gENUM_ETYPE e_type, FILE *f) const
{
   //--- Creates (orphan to be deleted after use) and fills TUAxis of type e_type
   //    common for all CRs in 'cr_indices' (and covering the maximal range for
   //    the CRs in list).
   //  combo             Combination - e.g. B/C, C+N+O, etc. - to consider
   //  e_type            Selects e axis among gENUM_ETYPE (kEKN, kR,...)
   //  f                 Output file for chatter

   // Extract list of CR indices for this combo
   // (and #CRs in denominator - 0 if not ratio)
   vector<Int_t> cr_indices;
   Int_t n_denom = 0;
   if (!ComboToCRIndices(combo, cr_indices, n_denom, false)) {
      string message = "Combo " + combo + " you ask for is not defined";
      TUMessages::Warning(f, "TUAxesCrE", "OrphanGetECombo", message);
      return NULL;
   }

   // Specific case:
   //   - combo = <LNA>
   //   - combo = ALLSPECTRUM
   // => cr_indices = all CR nuclei
   string cr_combo = combo;
   TUMisc::UpperCase(cr_combo);
   if (cr_combo == "<LNA>" || cr_combo == "ALLSPECTRUM") {
      for (Int_t i = 0; i < GetNCRs(); ++i) {
         if (GetCREntry(i).GetFamily() == kNUC)
            cr_indices.push_back(i);
      }
   }

   return OrphanGetECRs(cr_indices, e_type);
}

//______________________________________________________________________________
TUAxis *TUAxesCrE::OrphanGetECRs(vector<Int_t> const &cr_indices, gENUM_ETYPE e_type) const
{
   //--- Creates (orphan to be deleted after use) and fills TUAxis of type e_type
   //    common for all CRs in 'cr_indices' (and covering the maximal range for
   //    the CRs in list).
   //  cr_indices        Indices (in CR list) of CRs to consider
   //  e_type            Selects e axis among gENUM_ETYPE (kEKN, kR,...)

   if (cr_indices.size() == 0)
      TUMessages::Error(stdout, "TUAxesCrE", "OrphanGetECRs", "No CR index in list, cannot proceed!");

   Double_t e_min = -1.e40;
   Double_t e_max = 1.e40;
   for (Int_t k = 0; k < (Int_t)cr_indices.size(); ++k) {
      Int_t i = cr_indices[k];
      // Check that family is the same for all CRs in list
      e_min = max(e_min, GetE(i, 0, e_type));
      e_max = min(e_max, GetE(i, GetNE() - 1, e_type));
   }

   TUAxis *grid = new TUAxis();
   grid->SetClass(e_min, e_max, GetNE(), TUEnum::Enum2Name(e_type), TUEnum::Enum2Unit(e_type, false), kLOG);

   return grid;
}

//______________________________________________________________________________
void TUAxesCrE::PrintESummary(FILE *f) const
{
   //--- Prints summary of energy axes (for allocated families).
   //  f                 File in which to print

   string indent = TUMessages::Indent(true);
   for (Int_t i = 0; i < gN_CRFAMILY; ++i) {
      gENUM_CRFAMILY cr_family = TUEnum::gENUM_CRFAMILY_LIST[i];
      // Print E axis only if in list of CR
      if ((i == kNUC && !TUCRList::IsAtLeastOneNucleus())
            || (i == kANTINUC && !TUCRList::IsAtLeastOneAntinuc())
            || (i == kLEPTON && !TUCRList::IsAtLeastOneLepton()))
         continue;

      if (fE[i]) {
         fprintf(f, "%s   => %-12s   ", indent.c_str(), TUEnum::Enum2Name(cr_family).c_str());
         fE[i]->PrintSummary(f);
      }
   }
   fprintf(f, "%s   => E-dependent variables: %s  (%d E-variables)\n",
           indent.c_str(), GetEVarNames().c_str(), GetNEVars());
   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUAxesCrE::PrintEVarsValues(FILE *f, Int_t bincr, Bool_t is_print_halfbin) const
{
   //--- Prints in file f Evars values for bincr at all energies.
   //  f                 File in which to print
   //  bincr             CR index
   //  is_print_halfbin  Whether to also print (or not) EVars for half bins

   fprintf(f, "CR(%d) = %s\n", bincr, (GetCREntry(bincr).GetName()).c_str());
   fprintf(f, "EVarNames = %s\n", GetEVarNames().c_str());
   fprintf(f, "k   ");

   for (Int_t i_evar = 0; i_evar < GetNEVars(); ++i_evar) {
      fprintf(f, "  %-11s", (GetEVarName(i_evar, true)).c_str());
   }
   fprintf(f, "\n");
   if (is_print_halfbin)
      fprintf(f, "  [* values correspond to half-upper bin values]\n");

   for (Int_t k = GetNE() - 1; k >= 0; k--) {
      // Print Ekn and EVarVals at half-upper bin
      if (is_print_halfbin) {
         fprintf(f, "%3d*  ", k + 1);
         for (Int_t i_evar = 0; i_evar < GetNEVars(); ++i_evar)
            fprintf(f, "%.3le    ", GetEVarVal(bincr, k, i_evar, +1));
         fprintf(f, "\n");
      }

      // Print Ekn and EVarVals
      fprintf(f, "%3d   ", k);
      for (Int_t i_evar = 0; i_evar < GetNEVars(); ++i_evar)
         fprintf(f, "%.3le    ", GetEVarVal(bincr, k, i_evar, 0));
      fprintf(f, "\n");
   }
   // Last entry is half-lower bin at 0
   if (is_print_halfbin) {
      fprintf(f, "%3d*  ", 0);
      for (Int_t i_evar = 0; i_evar < GetNEVars(); ++i_evar)
         fprintf(f, "%.3le    ", GetEVarVal(bincr, 0, i_evar, -1));
      fprintf(f, "\n");
   }
   fprintf(f, "\n");
}

//______________________________________________________________________________
void TUAxesCrE::PrintEVarsValues(FILE *f, Int_t bincr_first, Int_t bincr_last, Bool_t is_print_halfbin) const
{
   //--- Prints in file f Evars values for bincr at all energies.
   //  f                 File in which to print
   //  bincr_first       CR in list from which to start printing Evars values
   //  bincr_last        CR in list from which to stop printing Evars Values
   //  is_print_halfbin  Whether to also print (or not) EVars for half-upper bin

   if (bincr_first < 0 || bincr_first > GetNCRs() - 1) bincr_first = 0;
   if (bincr_last < 0 || bincr_last > GetNCRs()) bincr_last = GetNCRs();
   for (Int_t bincr = bincr_first; bincr < bincr_last; ++bincr)
      PrintEVarsValues(f, bincr, is_print_halfbin);
}

//______________________________________________________________________________
void TUAxesCrE::SetClass(TUInitParList *init_pars, Bool_t is_verbose, FILE *f_log)
{
   //--- Sets class parameters.
   //  init_pars         TUInitParList object of USINE parameters
   //  is_verbose        Verbose or not when class is set
   //  f_log             Log for USINE


   string indent = TUMessages::Indent(true);
   if (is_verbose)
      fprintf(f_log, "%s[TUAxesCrE::SetClass]\n", indent.c_str());
   InitialiseE(true);

   // Update CR list
   TUCRList::SetClass(init_pars, is_verbose, f_log, true);

   // Set energy grids
   TUAxis dummy;
   dummy.SetEknGrids(init_pars, fE, is_verbose, f_log);

   // Load Edep variables
   AllocateAndFillENamesAndEVars();

   if (is_verbose)
      fprintf(f_log, "%s[TUAxesCrE::SetClass] <DONE>\n\n", indent.c_str());
   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUAxesCrE::TEST(TUInitParList *init_pars, FILE *f)
{
   //--- Test function for this class (called in usine -t).
   //  init_pars         TUInitParList class of initialisation parameters
   //  f                 Output file for test

   string message =
      "Handles CR and E multiple axes (for families kNUC,\n"
      "kANTINUC, and kLEPTON) for CR propagation.";
   TUMessages::Test(f, "TUAxesCrE", message);

   // Set class and summary content
   fprintf(f, " * Set class and print summary\n");
   fprintf(f, "   > SetClass(init_pars=\"%s\", is_verbose=false, f_log=stdout);\n", init_pars->GetFileNames().c_str());
   SetClass(init_pars, false, stdout);
   fprintf(f, "   > PrintESummary(f);\n");
   PrintESummary(f);
   fprintf(f, "\n");
   Int_t bincr = 7;
   Int_t bine = 2;
   fprintf(f, "   > PrintEVarsValues(f, bincr=%d, true)\n", bincr);
   PrintEVarsValues(f, bincr, true);
   fprintf(f, "   > FormEVarsVals(bincr=%d, bine=%d, halfbin_status=0) => %s\n",
           bincr, bine, FormEVarsValsE(bincr, bine, 0).c_str());
   fprintf(f, "   > FormEVarsVals(bincr=%d, bine=%d, halfbin_status=-1)  => %s\n",
           bincr, bine, FormEVarsValsE(bincr, bine, -1).c_str());
   fprintf(f, "   > FormEVarsVals(bincr=%d, bine=%d, halfbin_status=+1)  => %s\n",
           bincr, bine, FormEVarsValsE(bincr, bine, +1).c_str());
   fprintf(f, "   > TUCoordE coord_e;\n");
   TUCoordE coord_e;
   fprintf(f, "   > coord_e.SetBinE(bine, halfbin_status=0);\n");
   coord_e.SetBinE(bine, 0);
   fprintf(f, "   > ConvertBinE2ValsE(bincr, &coord_e, is_use_or_copy=true);\n");
   ConvertBinE2ValsE(bincr, &coord_e, true);
   fprintf(f, "   > coord_e.FormValsE() => %s;\n", coord_e.FormValsE().c_str());
   fprintf(f, "   > coord_e.SetBinE(bine, halfbin_status=1);\n");
   coord_e.SetBinE(bine, 1);
   fprintf(f, "   > ConvertBinE2ValsE(bincr, &coord_e, is_use_or_copy=true);\n");
   ConvertBinE2ValsE(bincr, &coord_e, true);
   fprintf(f, "   > coord_e.FormValsE() => %s;\n", coord_e.FormValsE().c_str());
   fprintf(f, "\n");
   gENUM_CRFAMILY combo_family = kNUC;
   const Int_t n_combo = 7;
   string combo[n_combo] = {"ELECTRON", "1H-BAR", "H", "B/C", "ELECTRON/POSITRON",
                            "1H-BAR/1H", "ELECTRON/1H"
                           };
   for (Int_t i = 0; i < n_combo; ++i) {
      fprintf(f, "   > IsComboPossibleForEGrids(combo=%s, &combo_family, f)\n",
              combo[i].c_str());
      IsComboPossibleForEGrids(combo[i], combo_family, f);
      fprintf(f, "      =>  combo_family = %s\n", TUEnum::Enum2Name(combo_family).c_str());
   }
   fprintf(f, "\n");

   // Copy
   fprintf(f, " * Test Copy method:\n");
   fprintf(f, "   > TUAxesCrE axes_cre; axes_cre.Copy(*this);\n");
   TUAxesCrE axes_cre;
   axes_cre.Copy(*this);
   fprintf(f, "   > axes_cre.PrintESummary(f);\n");
   axes_cre.PrintESummary(f);
   fprintf(f, "\n");
   fprintf(f, "   > axes_cre.PrintParents(f);\n");
   axes_cre.PrintParents(f);
   fprintf(f, "   > PrintParents(f);\n");
   PrintParents(f);
   fprintf(f, "\n");


   // Various ways to print E axis (for various families)
   fprintf(f, "* Print each E axis, and E grid for a family of CRs...\n");
   Int_t i_pbar = Index("1H-BAR", true, f);
   fprintf(f, "   > Index(1H-BAR, true, f);  => i_pbar=%d\n", i_pbar);
   fprintf(f, "   > GetE(i_pbar)->PrintSummary(f);\n");
   GetE(i_pbar)->PrintSummary(f);
   fprintf(f, "   > GetEFamily(kANTINUC)->PrintSummary(f);\n");
   GetEFamily(kANTINUC)->PrintSummary(f);

   Int_t i_p = Index("1H", true, f);
   if (i_p >= 0) {
      fprintf(f, "   > Index(1H, true, f);  => i_p=%d", i_p);
      fprintf(f, "   > GetE(i_p)->PrintSummary(f);\n");
      GetE(i_p)->PrintSummary(f);
   }
   Int_t i_elect = Index("ELECTRON", true, f);
   if (i_elect >= 0) {
      fprintf(f, "   > Index(ELECTRON, true, f);  => i_elect=%d\n", i_elect);
      fprintf(f, "   > GetE(i_elect)->PrintSummary(f);\n");
      GetE(i_elect)->PrintSummary(f);
   }

   Int_t i_10be = Index("10Be", true, f);
   fprintf(f, "   > Index(10Be, true, f);  => i_10be=%d\n", i_10be);
   if (i_10be >= 0) {
      fprintf(f, "   > GetE(i_10be)->PrintSummary(f);\n");
      GetE(i_10be)->PrintSummary(f);
      Int_t k_05 = IndexClosest(i_10be, 0.5);
      Int_t k_1 = IndexClosest(i_10be, 1.);
      Int_t k_5 = IndexClosest(i_10be, 5.);
      fprintf(f, "   > IndexClosest(i_10be, 0.5 GeV/n);  => k_05=%d  => GetEkn(i_10be,k_05)=%le\n", k_05, GetEkn(i_10be, k_05));
      fprintf(f, "   > IndexClosest(i_10be, 1.0 GeV/n);  => k_1=%d  => GetEkn(i_10be,k_1)=%le\n", k_1, GetEkn(i_10be, k_1));
      fprintf(f, "   > IndexClosest(i_10be, 5.0 GeV/n);  => k_5=%d  => GetEkn(i_10be,k_5)=%le\n", k_5, GetEkn(i_10be, k_5));
      fprintf(f, "   > GammaRadBETA_perMyr(i_10be, k_05);  => %le\n", GammaRadBETA_perMyr(i_10be, k_05));
      fprintf(f, "   > GammaRadBETA_perMyr(i_10be, k_1);  => %le\n", GammaRadBETA_perMyr(i_10be, k_1));
      fprintf(f, "   > GammaRadBETA_perMyr(i_10be, k_5);  => %le\n", GammaRadBETA_perMyr(i_10be, k_5));
   }
   fprintf(f, "\n");


   fprintf(f, "* Ekn axis range valid for combo=B/C\n");
   // E axes interpolated to match maximum range of combo, in E, R, Ekn...
   fprintf(f, "   > TUAxis *ekn_combo=OrphanGetECombo(combo=B/C, axis=kEKN, f_out=f);\n");
   TUAxis *ekn_combo = OrphanGetECombo("B/C", kEKN, f);
   fprintf(f, "   > ekn_combo->PrintSummary(f);\n");
   ekn_combo->PrintSummary(f);
   delete ekn_combo;
   fprintf(f, "\n");

   fprintf(f, "* ETOT axis range valid for combo=B/C\n");
   fprintf(f, "   > TUAxis *etot_combo=OrphanGetECombo(combo=B/C, axis=kETOT, f_out=f);\n");
   TUAxis *etot_combo = OrphanGetECombo("B/C", kETOT, f);
   fprintf(f, "   > etot_combo->PrintSummary(f);\n");
   etot_combo->PrintSummary(f);
   delete etot_combo;
   fprintf(f, "\n");

   fprintf(f, "* R axis range valid for combo=B/C\n");
   fprintf(f, "   > TUAxis *r_combo=OrphanGetECombo(combo=B/C, axis=kR, f_out=f);\n");
   TUAxis *r_combo = OrphanGetECombo("B/C", kR, f);
   fprintf(f, "   > r_combo->PrintSummary(f);\n");
   r_combo->PrintSummary(f);
   delete r_combo;
   fprintf(f, "\n");
}
