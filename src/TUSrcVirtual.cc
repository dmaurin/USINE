// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

// C/C++ include
// ROOT include
// USINE include
#include "../include/TUSrcVirtual.h"

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TUSrcVirtual                                                         //
//                                                                      //
// Pure virtual (abstract) class for source of a single species.        //
//                                                                      //
// The abstract class TUSrcVirtual provides a virtual class to be used  //
// for the two possible source description (for a single species):      //
//    - TUSrcPointLike: point-like src (name, position, spectrum);
//    - TUSrcSteadyState: steady-state src (name, spect., spatial dist.).
//                                                                      //
// This abstract class allows to use a single 'generic' object, a       //
// TUSrcVirtual object, which can either point to a TUSrcPointLike or   //
// a TUSrcSteadyState. This source can then be used as a single source  //
// for a single CR species, or as a single source for a list of CR      //
// species (see TUSrcMultiCRs).                                         //
//                                                                      //
// The most important virtual methods (we remind that models deriving   //
// from this class must have them at least) of the class are:           //
//   - FillSpectrumOrSpatialDistFromTemplates() fills from template;
//   - Clone() and Create();
//   - GetSrc() and GetSrcFreePars() returns the spectrum or spat.dist.
//                                                                      //
// N.B.: formula-based source descriptions can have free parameters.    //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

ClassImp(TUSrcVirtual)

//______________________________________________________________________________
TUSrcVirtual::TUSrcVirtual()
{

}

//______________________________________________________________________________
TUSrcVirtual::~TUSrcVirtual()
{

}
