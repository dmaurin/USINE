// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

// C/C++ include
// ROOT include
// USINE include
#include "../include/TUInteractions.h"
#include "../include/TUMath.h"
#include "../include/TUModel1DKisoVc.h"
#include "../include/TUNumMethods.h"

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TUModel1DKisoVc                                                      //
//                                                                      //
// Thin disc/halo 1D model with constant wind + homogeneous diffusion.  //
//                                                                      //
// The 1DKisoVc model (isotropic diffusion, constant Galactic wind) is  //
// a 1D propagation model (steady-state, infinite disc with a diffusive //
// halo). It is semi-analytical because the solution without E gains or //
// losses) is algebraic, the general equation being 2nd order in energy //
// (solved using an explicit numerical method, as discussed, e.g., in   //
// TUNumMethods::SolveEq_1D2ndOrder_Explicit).                          //
//                                                                      //
// See the online documentation for more details and references.        //
//////////////////////////////////////////////////////////////////////////

ClassImp(TUModel1DKisoVc)

//______________________________________________________________________________
TUModel1DKisoVc::TUModel1DKisoVc() : TUModelBase()
{
   // ****** Default constructor ******

   Initialise(false);
}

//______________________________________________________________________________
TUModel1DKisoVc::~TUModel1DKisoVc()
{
   // ****** Default destructor ******

   Initialise(true);
}

//______________________________________________________________________________
Double_t TUModel1DKisoVc::A1D(Int_t j_cr, Int_t k_ekn, Double_t const &kdiff)
{
   //--- Returns A1D = 2.*h*gamma_inel + Vc + K*S1D*coth(S1D*L/2.)  [kpc/Myr]
   //    [Vc]=kpc/Myr, [kdiff]=kpc^2/Myr, [L]=kpc, [S1D]=/kpc.
   //
   //  j_cr              CR index
   //  k_ekn             Energy index
   //  kdiff             Diffusion coefficient for this CR and E [kpc^2/Myr]

   Double_t s1d = S1D(j_cr, k_ekn, kdiff);
   return A1D(s1d, j_cr, k_ekn, kdiff);
}

//______________________________________________________________________________
Double_t TUModel1DKisoVc::A1D(Int_t j_cr, Int_t k_ekn)
{
   //--- Returns A1D = 2.*h*gamma_inel + Vc + K*S1D*coth(S1D*L/2.)  [kpc/Myr]
   //
   //  j_cr              CR index
   //  k_ekn             Energy index

   return A1D(j_cr, k_ekn, ValueK(j_cr, k_ekn));
}

//______________________________________________________________________________
Double_t TUModel1DKisoVc::A1D(Double_t const &s1d, Int_t j_cr, Int_t k_ekn, Double_t const &kdiff)
{
   //--- Returns A1D = 2.*h*gamma_inel + Vc + K*S1D*coth(S1D*L/2.)  [kpc/Myr]
   //    [Vc]=kpc/Myr, [kdiff]=kpc^2/Myr, [L]=kpc, [S1D]=/kpc.
   //
   //  s1d               S1D term
   //  j_cr              CR index
   //  k_ekn             Energy index
   //  kdiff             Diffusion coefficient for this CR and energy

   // If inelastic interaction
   Double_t gamma_inel = 0.;
   if (TUPropagSwitches::IsDestruction())
      gamma_inel = TUXSections::Get_nvSigTotIA_ISM0D(j_cr, k_ekn);

   // N.B.: requires a specific case if equals 0, because of the divergent
   // 1/sh(S1D) term in A1D. In that case, the function TUMath::Coth (which
   // includes an expansion for large arguments) is preferred to the divergent coth.
   if (fabs(s1d) < 1.e-40)
      return 2.* GetParVal_h() * gamma_inel
             + 2.*kdiff / GetParVal_L();
   else
      return 2.* GetParVal_h() * gamma_inel + ValueVc_kpcperMyr()
             + kdiff * s1d * TUMath::Coth(s1d * GetParVal_L() * 0.5);
}

//______________________________________________________________________________
Bool_t TUModel1DKisoVc::CalculateChargedCRs(Int_t jcr_start, Int_t jcr_stop, Bool_t is_verbose, FILE *f_log)
{
   //--- Computes differential density N0^j at z=0 (j=(anti-) nuclei index) required
   //    to evaluate the differential density for any z in this 1D diffusion model.
   //    N.B.: N(z) is constructed from N(0) elsewhere.
   //  jcr_start         Index of CR for first to calculate
   //  jcr_stop          Index of CR for last to calculate
   //  is_verbose        Verbose or not when class is set
   //  f_log             Log for USINE
   //    For each nucleus j, the differential equation for N0^j with E losses (B^j),
   //    gains (C^j), and source term (S^j) reads (see class documentation):
   //       A1D^j(Ekn)*N0^j(Ekn) + 2hd/dEtot( B^j*N0^j - C^j dN0^j/dEtot ) = S1D^j(Ekn)
   //    which is recast into the more-convenient form (we drop j, the CR index)
   //       N0 + (fCoefPrefactor * d/dLnEkn(fCoef1stOrder*N0 -  fCoef2ndOrderHalfBin * dN0/dLnEkn) = Sterm/A
   //    with
   //       fCoefPrefactor = 1/(A*Ek)
   //       fCoef1stOrder  = B
   //       fCoef2ndOrderHalfBin  = C/Ek
   //    N.B.: if neither losses nor energy gains (i.e., B^j=C^j=0), dNdEkn=Sterm;
   //          otherwise a numerical inversion is required.
   //    N.B.: if for some reason, some parameters of the models have been changed
   //          (e.g. K), do not forget to update FillCoefPreFactorAndA1DTerm().
   //
   //    In the following, Sterm^j is the sum of various contributions (all taken
   //    at z=0) that includes
   //       (1) Secondary contributions
   //       (2) Primary standard (in disc) contributions
   //       (3) BETA-radioactive contribution
   //       (4) EC-radioactive contribution [TODO]
   //    N.B.: We chose [Sterm^j]=[N0] = (GeV/n)^{-1}.m^{-3}


   string indent = TUMessages::Indent(true);

   // Check indices to run
   if (jcr_start < 0)
      jcr_start = 0;
   if (jcr_stop > GetNCRs() - 1)
      jcr_stop = GetNCRs() - 1;
   if (jcr_start > jcr_stop)
      TUMessages::Warning(f_log, "TUModel2DKisoVc", "CalculateChargedCRs", "jcr_start>jcr_stop, nothing to do");

   Bool_t is_check = false;
   if (is_verbose && is_check)
      fprintf(f_log, "%sTUModel1DKisoVc::CalculateChargedCRs\n", indent.c_str());


   // Allocate source term
   Double_t *sterm_tot = new Double_t[GetNE()];
   Double_t *sterm_prim = new Double_t[GetNE()];

   // Loop on CRs (heaviest to lightest)
   for (Int_t j_cr = jcr_stop; j_cr >= jcr_start; --j_cr) {
      // If BETA rad parent for j_cr (and BETA decay mode
      // switched on), store its index for further usage
      Int_t j_radbeta_parent = -1;
      if (TUPropagSwitches::IsDecayFedBETA())
         j_radbeta_parent = TUCRList::IndexParentBETA(j_cr);


      /*****************************/
      /* A. Calculate source terms */
      /*****************************/
      // Sterm = (2hQ_prim+2hQ_sec+Q_rad) / Aterm
      // [/(GeV/n m3)] = [kpc/(GeV/n m3 Myr)] / [kpc/Myr]
      // N.B.: in practice we calculate sterm, and multiply by 2h/Aterm
      // after having added all contributions (in order to get Sterm)

      // Initialise [Sterm] = [N0] = (GeV/n)^{-1}.m^{-3}
      // first used as [sterm] = (GeV/n)^{-1}.m^{-3} / Myr
      for (Int_t k_ekn = GetNE() - 1; k_ekn >= 0; --k_ekn) {
         sterm_tot[k_ekn] = 0.;
         sterm_prim[k_ekn] = 0.;
      }


      // A(0) Secondary contrib. (parent+targ->j_cr)
      //-------------------------------------------
      if (TUPropagSwitches::IsSecondaries()) {
         // N.B.: valid whether straight-ahead approx. or differential production
         //   sterm_sec  =  Q_sec
         // [/(GeV/n m3 Myr)] = [/(GeV/n m3 Myr)]

         // Loop on list of parents (for j_cr)
         for (Int_t k_in_lop = TUCRList::GetNParents(j_cr) - 1; k_in_lop >= 0; --k_in_lop) {
            // If only zeros in X-sec, skip it!
            if (TUModelBase::IsProdNull(k_in_lop, j_cr))
               continue;

            Int_t k_in_lon = TUCRList::IndexInCRList_Parent(j_cr, k_in_lop);

            // Adds in sterm_tot secondary production from n0_proj. Two cases:
            //    1: secondary production from a BETA parent with local underdensity
            //    2: no underdensity (regular 1D model)
            if (TUPropagSwitches::IsDecayFedBETA() && TUCRList::GetCREntry(k_in_lon).IsUnstable(true)
                  && GetParVal_rh() > 1.e-5) {
               // Local underdensity and secondary production from a radioactive parent
               // N.B.: watch out, this is tricky!
               // If rhole>0, a damping factor exp(-r_hole/sqrt(K/gamma_rad)) was
               // applied to the flux of the radioactive species in the disc (z=0),
               // see below. This damping does not apply outside the local bubble:
               // as secondary production in the thin disc comes from a spatial region
               // much larger than r_hole (Taillet and Maurin, 2003), the spallative
               // contribution from a radioactive parent must consider the un-damped flux.
               Double_t *n0_proj = new Double_t[GetNE()];
               for (Int_t k_ekn = GetNE() - 1; k_ekn >= 0; --k_ekn) {
                  Double_t damping =  exp(-GetParVal_rh() / sqrt(ValueK(k_in_lon, k_ekn)
                                          / TUAxesCrE::GammaRadBETA_perMyr(k_in_lon, k_ekn)));
                  n0_proj[k_ekn] = GetN0Tot(k_in_lon, k_ekn) / damping;
               }
               TUXSections::SecondaryProduction(n0_proj, k_in_lop, j_cr, fISMEntry,
                                                sterm_tot, GetEpsIntegr(), true /*is_ism0D*/, f_log);
               delete[] n0_proj;
               n0_proj = NULL;
            } else {
               // Otherwise, standard calculation
               Double_t *n0_proj = GetN0Tot(k_in_lon);
               TUXSections::SecondaryProduction(n0_proj, k_in_lop, j_cr, fISMEntry,
                                                sterm_tot, GetEpsIntegr(), true /*is_ism0D*/, f_log);
               n0_proj = NULL;
            }
         }

         // If current nucleus BETA-radioactive and local bubble, then there is a damping
         // factor to account for (see Donato et al. 2003 or Putze et al., 2010).
         if (TUPropagSwitches::IsDecayBETA() && TUCRList::GetCREntry(j_cr).IsUnstable(true)
               && GetParVal_rh() > 1.e-5) {
            for (Int_t k_ekn = GetNE() - 1; k_ekn >= 0; --k_ekn) {
               Double_t damping = exp(-GetParVal_rh() / sqrt(ValueK(j_cr, k_ekn)
                                      / TUAxesCrE::GammaRadBETA_perMyr(j_cr, k_ekn)));
               sterm_tot[k_ekn] *= damping;
            }
         }
      }

      // All other contributions (within an ekn loop)
      for (Int_t k_ekn = GetNE() - 1; k_ekn >= 0; --k_ekn) {


         // A(2) Primary contrib.
         //---------------------
         if (TUPropagSwitches::IsPrimStandard() && !TUAxesCrE::IsPureSecondary(j_cr)) {
            Double_t src_contrib = 0.;

            // Loop on all available astrophysical sources
            // N.B.: normalisation for source spectrum must be in [/(GeV/n m3 Myr)]
            for (Int_t s_src = 0; s_src < TUModelBase::GetNSrcAstro(); ++s_src) {

               // Skip if j_cr is not present in the s_src-th astro source
               if (!TUModelBase::IsExistSrc(s_src, j_cr, true))
                  continue;

               // Update energy coordinate and calculate spectrum
               TUModelBase::UpdateCoordE(j_cr, k_ekn);
               src_contrib += TUModelBase::ValueSrcAstroSpectrum(s_src, j_cr, TUModelBase::GetCoordE());
            }
            // Add in total (all contribs) and prim (primary contrib only)
            // N.B.: we divide by Aterm after having added all contributions
            sterm_tot[k_ekn] += src_contrib;
            sterm_prim[k_ekn] += src_contrib;
         }


         Double_t radinhalo_contrib = 0.;
         // A(3) BETA-rad. parent contrib.
         //-------------------------------
         //     Q_rad  =  Nz_WithParentBETA(z=0)
         //            = [/(GeV/n m3)]
         if (j_radbeta_parent != -1)
            radinhalo_contrib = Nz_WithParentBETA(j_radbeta_parent, j_cr, k_ekn, 0.);

         // A(4) EC-rad. parent contrib.
         //-----------------------------
         // TO DO


         // All contributions are now added:
         //         Sterm =  2h  *  sterm  /  Aterm
         // [/(GeV/n m3)] = [kpc]*[/(GeV/n m3 Myr)]/[kpc/Myr]
         Double_t mult = 2.*GetParVal_h() / GetA1DTerm(j_cr, k_ekn);
         sterm_prim[k_ekn] = sterm_prim[k_ekn] * mult;
         sterm_tot[k_ekn] = sterm_tot[k_ekn] * mult + radinhalo_contrib;
         //cout << GetCREntry(j_cr).GetName() << "  " << k_ekn << "  " << sterm_tot[k_ekn] << "  " << GetA1DTerm(j_cr, k_ekn) << endl;
      }


      /*******************************/
      /* B. Solve N0 for source term */
      /*******************************/
      InvertForELossGain(j_cr, fISMEntry, GetCoefPreFactor(j_cr), sterm_tot, sterm_prim, fInvertScheme, GetN0Tot(j_cr), GetN0Prim(j_cr));

      /*********************************/
      /* C. Add tertiaries (iterative) */
      /*********************************/
      if (TUPropagSwitches::IsTertiaries() && TUXSections::IsTertiary(j_cr))
         IterateTertiariesN0(j_cr, sterm_tot, sterm_prim, TUModelBase::GetEpsIterConv(), is_verbose, f_log);
   }

   // Free memory
   delete[] sterm_tot;
   sterm_tot = NULL;
   delete[] sterm_prim;
   sterm_prim = NULL;
   if (is_verbose && is_check)
      fprintf(f_log, "%sTUModel1DKisoVc::CalculateChargedCRs <DONE>\n", indent.c_str());

   TUMessages::Indent(false);
   return true;
}

//______________________________________________________________________________
Double_t TUModel1DKisoVc::CoeffReac1stOrder(Int_t j_cr, Int_t k_ekn)
{
   //--- Returns first order term from reacceleration.
   //  j_cr              Index of CR
   //  k_ekn             Index of energy (here running from -1 to NE)

   if (k_ekn >= 0 && k_ekn < GetNE())
      return (1. + GetBeta(j_cr, k_ekn) * GetBeta(j_cr, k_ekn)) * ValueKpp(j_cr, k_ekn) / GetEtot(j_cr, k_ekn);
   else {
      Double_t ekn = TUAxesCrE::GetE(j_cr)->EvalOutOfRange(k_ekn);
      Double_t beta = TUPhysics::Beta_mAEkn(TUAxesCrE::GetCREntry(j_cr).GetmGeV(), TUAxesCrE::GetCREntry(j_cr).GetA(), ekn);
      Double_t etot = TUPhysics::Ekn_to_E(ekn, TUAxesCrE::GetCREntry(j_cr).GetA(), TUAxesCrE::GetCREntry(j_cr).GetmGeV());
      TUModelBase::UpdateCoordE(j_cr, ekn);
      return
         (1. + beta * beta) * TUModelBase::ValueKpp(TUModelBase::GetCoordE()) / etot;
   }
}

//______________________________________________________________________________
Double_t TUModel1DKisoVc::CoeffReac2ndOrderLowerHalfBin(Int_t j_cr, Int_t k_ekn)
{
   //--- Returns second order term from reacceleration: evaluated at lower half-bin.
   //  j_cr              Index of CR
   //  k_ekn             Index of energy (here running from 0 to NE)

   if (k_ekn != GetNE())  // Return at k_ekn-1/2
      return TUAxesCrE::GetBeta(j_cr, k_ekn, -1) * TUAxesCrE::GetBeta(j_cr, k_ekn, -1)
             * ValueKpp(j_cr, k_ekn, -1) / TUAxesCrE::GetEk(j_cr, k_ekn, -1);
   else  // Return at NE-1/2 = (NE-1) + 1/2
      return TUAxesCrE::GetBeta(j_cr, GetNE() - 1, 1) * TUAxesCrE::GetBeta(j_cr, GetNE() - 1, 1)
             * ValueKpp(j_cr, GetNE() - 1, 1) / TUAxesCrE::GetEk(j_cr, GetNE() - 1, 1);
}

//______________________________________________________________________________
Double_t TUModel1DKisoVc::EvalFlux(Int_t j_cr, Int_t k_ekn, Double_t z, Int_t tot0_prim1_sec2)
{
   //--- Returns for j_cr at position z (solution is even) total, primary, or
   //    secondary flux [#part/(m2.s.sr.GeV/n)].
   //       - flux = v/(4*pi)*dN/dE  [dNdE]=# part/(m^3.GeV/n), [c]=m/s;
   //       - if radioactive parent, more tricky (see, Putze et al., 2010).
   //  j_cr              CR index
   //  k_ekn             Energy index
   //  z                 X coordinate = z (height in the halo)
   //  tot0_prim1_sec2   Switch to have total(0), primary(1) or secondary(2) contrib.

   // Solution is even
   z = fabs(z);

   // If outside geometry, flux is 0.
   if (z > GetParVal_L() - 1.e-3)
      return 0.;

   // Factor to convert differential density to flux = [v/(4*pi)]
   Double_t factor = TUAxesCrE::GetBeta(j_cr, k_ekn)
                     * TUPhysics::C_mpers() / (4.*TUMath::Pi());

   // N.B.: if rhole>0 and radioactive nucleus, calculation applies only for z=0!
   if (z > 1.e-10 && TUPropagSwitches::IsDecayBETA()
         && GetParVal_rh() > 1.e-5
         && TUAxesCrE::GetCREntry(j_cr).GetBETAHalfLife() > 1.e-40) {
      string message = "Flux for a BETA-unstable " + TUAxesCrE::GetCREntry(j_cr).GetName()
                       + " for r_hole>0 is not valid for z>0!";
      TUMessages::Warning(stdout, "TUModel1DKisoVc", "EvalFlux", message);
      return 0.;
   }

   if (tot0_prim1_sec2 == 0) {
      // N.B.: if nucleus has a radioactive parent, requires a special case.
      // The index of the BETA-parent is j_beta (=-1 if no BETA-decay parent)
      Int_t j_beta = TUAxesCrE::TUCRList::IndexParentBETA(j_cr);
      if (j_beta != -1) {
         if (z > 1.e-10)
            return factor * Nz_WithParentBETA(j_beta, j_cr, k_ekn, z);
         else // In this specific case (z=0), it is faster and yet correct
            // not to call 'Nz_WithParentBETA'
            return factor * Nz_WithoutParentBETA(GetN0Tot(j_cr, k_ekn), j_cr, k_ekn, z);
      } else
         return factor * Nz_WithoutParentBETA(GetN0Tot(j_cr, k_ekn), j_cr, k_ekn, z);
   } else if (tot0_prim1_sec2 == 1)
      return factor * Nz_WithoutParentBETA(GetN0Prim(j_cr, k_ekn), j_cr, k_ekn, z);
   else if (tot0_prim1_sec2 == 2)
      return EvalFlux(j_cr, k_ekn, z, 0) - EvalFlux(j_cr, k_ekn, z, 1);
   else
      TUMessages::Error(stdout, "TUModel1DKisoVc", "EvalFlux",
                        "Allowed values for tot0_prim1_sec2 are 0, 1, and 2");

   return 0.;
}

//______________________________________________________________________________
void TUModel1DKisoVc::FillCoefPreFactorAndA1DTerm()
{
   //--- Fills fCoefPreFactor=2h/(A1D*Ek) [Myr/GeV] and A1DTerm [kpc/Myr].

   // Allocate memory
   if (fCoefPreFactor) delete[] fCoefPreFactor;
   fCoefPreFactor = new Double_t[GetNE()*GetNCRs()];

   if (fA1DTerm) delete[] fA1DTerm;
   fA1DTerm = new Double_t[GetNE()*GetNCRs()];

   // Loop on all CRs to fill fA1DTerm and fCoefPreFactor
   for (Int_t j_cr = GetNCRs() - 1; j_cr >= 0; --j_cr) {
      for (Int_t k_ekn = GetNE() - 1; k_ekn >= 0; --k_ekn) {
         SetA1DTerm(j_cr, k_ekn, A1D(j_cr, k_ekn));
         Double_t factor = 2 * GetParVal_h()
                           / (GetA1DTerm(j_cr, k_ekn) * TUAxesCrE::GetEk(j_cr, k_ekn));
         SetCoefPreFactor(j_cr, k_ekn, factor);
      }
   }
}

//______________________________________________________________________________
void TUModel1DKisoVc::Initialise(Bool_t is_delete)
{
   //--- Initialises members specific to this model.
   //    N.B.: fISMEntry never needs to be deleted, because it relies on
   //    TUModelBase::fISM which is created and deleted elsewhere.
   //
   //  is_delete         Whether to delete or just initialise

   if (is_delete) {
      if (fA1DTerm) delete[] fA1DTerm;
      if (fCoefPreFactor) delete[] fCoefPreFactor;
      if (fN0Prim) delete[] fN0Prim;
      if (fN0Tot) delete[] fN0Tot;
   }

   fA1DTerm = NULL;
   fCoefPreFactor = NULL;
   fIndexPar_h = -1;
   fIndexPars_L = -1;
   fIndexPars_rh = -1;
   fInvertScheme = kTRID;
   fISMEntry = NULL;
   fN0Prim = NULL;
   fN0Tot = NULL;
}

//______________________________________________________________________________
void TUModel1DKisoVc::InitialiseForCalculation(Bool_t is_init_or_update, Bool_t is_force_update, Bool_t is_verbose, FILE *f_log)
{
   //--- Initialises and/or update (optimised or not) model members and values
   //    required for model calculation.
   //
   //  is_init_or_update Whether initialise class or update (look for changed parameters)
   //  is_force_update   If is_init_or_update=false (update), force to to it even if no parameter changed (useful in a few occasions)
   //  is_verbose        Verbose or not
   //  f_log             Log for USINE

   string indent = TUMessages::Indent(true);

   // Model-specific quantities: only for initialisation
   if (is_init_or_update) {
      if (is_verbose)
         fprintf(f_log, "%s[TUModel1DKisoVc::InitialiseForCalculation]\n", indent.c_str());

      // Set model parameter indices
      if (is_verbose)
         fprintf(f_log, "%s   - Retrieve h, L, rhole indices (in TUFreeParList)\n", indent.c_str());
      fIndexPar_h = TUModelBase::TUFreeParList::IndexPar("h");
      fIndexPars_L = TUModelBase::TUFreeParList::IndexPar("L");
      fIndexPars_rh = TUModelBase::TUFreeParList::IndexPar("rhole");
      // Set TXYZ axes
      TUModelBase::GetAxesTXYZ()->UpdateFromFreePars();
      if (TUModelBase::GetAxesTXYZ()->GetFreePars())
         TUModelBase::GetAxesTXYZ()->GetFreePars()->ResetStatusParsAndParList();


      // Allocate memory for N0 coeffs
      if (is_verbose)
         fprintf(f_log, "%s   - Allocate fN0Tot[NCRs*NE] and fN0PrimNCRs*NE\n", indent.c_str());
      if (fN0Tot)  delete[] fN0Tot;
      fN0Tot = new Double_t[GetNE()*GetNCRs()];
      if (fN0Prim) delete[] fN0Prim;
      fN0Prim = new Double_t[GetNE()*GetNCRs()];
      // And initialise
      for (Int_t i = 0; i < GetNE()*GetNCRs(); ++i) {
         fN0Tot[i] = 0.;
         fN0Prim[i] = 0.;
      }

      // If initialisation, propagation is mandatory (set to not propagated)
      TUModelBase::SetPropagated(false);
   } else {
      // If update, check whether model is updated
      TUModelBase::TUFreeParList::UpdateParListStatusFromIndividualStatus();
      if (TUModelBase::TUFreeParList::GetParListStatus())
         TUModelBase::SetPropagated(false);
   }

   // Update par list status for all parameters
   TUModelBase::UpdateAllParListStatusFromIndividualParStatus();

   // ISM: initialisation, update (if ISM was modified), or forced update
   if (is_init_or_update || TUModelBase::GetFreeParsStatusISM() || is_force_update) {
      // Set fISMEntry to TUMediumTXYZ entry
      if (is_verbose) {
         if (is_init_or_update)
            fprintf(f_log, "%s   - Set fISMEntry (thin disc) from TUMediumTXYZ\n", indent.c_str());
         else
            fprintf(f_log, "%s   - Update fISMEntry (thin disc)\n", indent.c_str());
      }

      TUModelBase::GetISM()->UpdateFromFreeParsAndResetStatus();
      TUCoordTXYZ *dummy_txyz = NULL;
      TUMediumTXYZ *medium = TUModelBase::GetISM();
      medium->FillMediumEntry(dummy_txyz);
      fISMEntry = medium->GetMediumEntry();
      medium = NULL;
      if (is_init_or_update && is_verbose)
         fISMEntry->Print(f_log);

      // Fill or update nuclear rates (because ISM was changed)
      if (is_verbose) {
         if (is_init_or_update)
            fprintf(f_log, "%s   - Fill nuclear rates (optimised for constant density ISM)\n", indent.c_str());
         else
            fprintf(f_log, "%s   - Update nuclear rates (optimised for constant density ISM)\n", indent.c_str());
      }
      TUXSections::FillInterRate_ISM0D(fISMEntry, true);
   }


   // XSection: initialisation, update (if XS were modified), or force update
   if (is_init_or_update || TUModelBase::GetFreeParsStatusXSections() || is_force_update) {

      // Set/Update XS from free parameters
      if (is_verbose) {
         if (!is_init_or_update)
            fprintf(f_log, "%s   - Update XSections and nuclear rates (optimised for constant density ISM)\n", indent.c_str());
      }
      TUModelBase::TUXSections::ParsXS_UpdateFromFreeParsAndResetStatus(fISMEntry);

      // We have to force update for the remaining parameters (coeff to calculate are related to rates)
      is_force_update = true;
   }


   // Transport: initialisation, update (if transport or half-lives were modified), or force update
   if (is_init_or_update || TUModelBase::GetFreeParsStatusTransport()
         || TUModelBase::GetFreeParsStatusHalfLives() || is_force_update) {
      if (is_verbose) {
         if (is_init_or_update)
            fprintf(f_log, "%s   - Fill prefactor terms\n", indent.c_str());
         else
            fprintf(f_log, "%s   - Update prefactor terms\n", indent.c_str());
      }
      TUModelBase::GetTransport()->UpdateFromFreeParsAndResetStatus();
      TUModelBase::TUAxesCrE::TUCRList::ParsHalfLife_UpdateFromFreeParsAndResetStatus();
      FillCoefPreFactorAndA1DTerm();
   }


   // Sources: only for update (if sources were modified)
   if (!is_init_or_update) {
      for (Int_t i = 0; i < GetNSrcAstro(); ++i)
         TUModelBase::GetSrcAstro(i)->UpdateFromFreeParsAndResetStatus();
      for (Int_t i = 0; i < GetNSrcDM(); ++i)
         TUModelBase::GetSrcDM(i)->UpdateFromFreeParsAndResetStatus();
   }


   // Propagation switches: just reset status
   TUPropagSwitches::SetStatus(false);
   // Update and reset model parameters
   TUModelBase::ResetStatusParsAndParList();
   if (is_init_or_update && is_verbose)
      fprintf(f_log, "%s[TUModel1DKisoVc::InitialiseForCalculation] <DONE>\n", indent.c_str());

   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUModel1DKisoVc::IterateTertiariesN0(Int_t j_cr, Double_t *sterm_tot,
      Double_t *sterm_prim, Double_t const &eps,
      Bool_t is_verbose, FILE *f_log)
{
   //--- Solves iteratively E redistribution from inel. non-annihil. rescattering (INA).
   //    With INA iterations, the diffusion equation becomes an integro-differential
   //    equation (2nd order derivative in E, one integration for E redistribution).
   //    The integral term involves the equilibrium spectrum, which is calculated
   //    iteratively (convergence is reached after a couple of iterations in practice).
   //    The formula below is slightly changed in practise (see TUXSections::TertiaryProduction)
   //    to perform the calculation on a log-scale energy:
   //       - step 1: calculate tertiary contrib. (over ISM components) from current dN/dEkn:BEGIN_LATEX(fontsize=14, separator='=', align='lcl')
   //          S_{ter} = #frac{Q_{ter}}{A_{term}}, with
   //          Q_{ter}(E) = #int_{E}^{#infty} n v' #frac{d#sigma^{INA}_{CR+ISM -> CR+X+ISM}}{dE} (E' -> E) #frac{dN}{dEkn}(E')dE' - nv#sigma^{INA}_{CR+ISM -> CR+X+ISM} (E) #frac{dN}{dEkn}(E)
   //END_LATEX
   //       - step 2: calculate new equilibrium spectra dN/dEkn (adding tertiary term)
   //       - step 3: go back to step 1 and iterate with new dN/dEkn until convergence reached
   //
   // INPUTS:
   //  j_cr              CR index
   //  sterm_tot         Source term (prim+sec contribs.) before tertiaries
   //  sterm_prim        Source term (prim. only) before tertiaries
   //  eps               Precision sought before stopping iterations
   //  is_verbose        Chatter on or off
   //  f_log             Log for USINE
   // OUTPUTS:
   //  sterm_tot         Source term (prim+sec contribs.) including tertiaries
   //  sterm_prim        Source term (prim. only) including tertiaries


   // If j_cr not a tertiary, nothing to do
   if (!TUXSections::IsTertiary(j_cr))
      return;

   string indent = TUMessages::Indent(true);
   TUMessages::Indent(false);

   // Set maximum iteration number
   const Int_t n_iter_max = 100;

   // Allocate and initialise sterm (enable comparison before/after iteration)
   Double_t max_dev = 0.;
   Double_t *n0_ref = new Double_t[TUModelBase::GetNE()];
   Double_t *stermtot_ter = new Double_t[TUModelBase::GetNE()];
   Double_t *stermtot_ter_old = new Double_t[TUModelBase::GetNE()];

   // Update new/old Sterm for primary contrib. only (if required)
   Double_t *stermprim_ter = NULL;
   Double_t *stermprim_ter_old = NULL;
   if (!TUAxesCrE::IsPureSecondary(j_cr)) {
      stermprim_ter = new Double_t[TUModelBase::GetNE()];
      stermprim_ter_old = new Double_t[TUModelBase::GetNE()];
      for (Int_t k = TUModelBase::GetNE() - 1; k >= 0; --k) {
         stermtot_ter[k] = 0.;
         stermprim_ter[k] = 0.;
         n0_ref[k] = GetN0Tot(j_cr, k);
      }
   } else {
      for (Int_t k = TUModelBase::GetNE() - 1; k >= 0; --k) {
         stermtot_ter[k] = 0.;
         n0_ref[k] = GetN0Tot(j_cr, k);
      }
   }

   // Iterate until convergence for any energy:
   //    |(N_{iter i+1} - N_{iter i})| / N_{iter i} < eps
   Int_t counter = 0;
   Int_t k_converged = 0;
   do {
      /******************************/
      /* Step 1: calculate tertiary */
      /******************************/
      ++counter;
      Int_t k_converged_iter = TUModelBase::GetNE() - 1;

      // Max difference after/before iteration (over all CR energies)
      max_dev = 0.;

      // Check convergence on total calculated flux: limiting convergence
      // is expected for higher energy points (less points to integrate
      // on tertiary production)
      for (Int_t k = TUModelBase::GetNE() - 1 ; k >= 0; --k) {
         // sterm_ter = 2h*Qterm/Aterm   [sterm_ter]=[sterm]= [N0]

         Bool_t is_converged = false;

         // Update new/old Sterm (primary and secondary contrib.)
         stermtot_ter_old[k] = stermtot_ter[k];
         stermtot_ter[k] = 2. * GetParVal_h() / GetA1DTerm(j_cr, k)
                           * TUXSections::TertiaryProduction(GetN0Tot(j_cr), j_cr, k, fISMEntry, eps, true, is_converged);

         Double_t delta = stermtot_ter[k] - stermtot_ter_old[k];
         Double_t rel_diff = 0.;
         if (sterm_tot[k] + delta < 0.)
            sterm_tot[k] = 0.;
         else {
            sterm_tot[k] += delta;
            // Check convergence of integration at this energy only if rel_dif>eps
            rel_diff = fabs((delta) / fabs(sterm_tot[k]));
         }
         // Check if converged (contrib to tertiary needs to be significant and flux non null)
         //cout << "         k=" << k << "   is_converged=" << is_converged
         //     << "   sterm_tot[k]=" << sterm_tot[k] << "   rel_diff=" << rel_diff << endl;
         if (!is_converged && sterm_tot[k] > 1.e-40 && rel_diff > eps)
            k_converged_iter = k;

         // Keep only maximum difference to check overall convergence of this iteration
         max_dev = TMath::Max(max_dev, rel_diff);

         // Update new/old Sterm for primary contrib. only (if required)
         if (!TUAxesCrE::IsPureSecondary(j_cr)) {
            stermprim_ter_old[k] = stermprim_ter[k];
            stermprim_ter[k] = 2. * GetParVal_h() / GetA1DTerm(j_cr, k)
                               * TUXSections::TertiaryProduction(GetN0Prim(j_cr), j_cr, k, fISMEntry, eps, true, is_converged);
            sterm_prim[k] += stermprim_ter[k] - stermprim_ter_old[k];
         }
      }

      /****************************/
      /* Step 2: calculate new N0 */
      /****************************/
      InvertForELossGain(j_cr, fISMEntry, GetCoefPreFactor(j_cr), sterm_tot, sterm_prim, fInvertScheme, GetN0Tot(j_cr), GetN0Prim(j_cr));

      k_converged = max(k_converged, k_converged_iter);
      // cout << "****** iter=" << counter << "   k_converged_iter="
      //      << k_converged_iter << "   k_converged=" << k_converged << endl;

      // Stop if reached maximum allowed number of iterations
      if (counter > n_iter_max) break;

      // Stop if convergence reached
   } while (max_dev > eps);

   // Warning message if convergence of numerical integration in tertiary
   // contribution (that matters, i.e. for which the difference w/o tertiary
   // is larger than required precision) is not reached
   Int_t k_matters = TUModelBase::GetNE();
   for (Int_t k = TUModelBase::GetNE() - 1 ; k >= 0; --k) {
      if (n0_ref[k] > 0. && fabs(n0_ref[k] - GetN0Tot(j_cr, k)) / GetN0Tot(j_cr, k) > eps) {
         k_matters = k;
         break;
      }
   }

   if (k_converged < TUModelBase::GetNE() && k_matters > k_converged) {
      string message = Form("Precision eps=%f (for tertiary numerical integration)"
                            " not reached for %s for all Ekn in range [%.3le,%.3le] [GeV/n]",
                            eps, TUAxesCrE::GetCREntry(j_cr).GetName().c_str(), TUAxesCrE::GetEkn(j_cr, k_converged),
                            TUAxesCrE::GetEkn(j_cr, k_matters));
      TUMessages::Warning(f_log, "TUModel1DKisoVc", "IterateTertiariesN0", string(message));
      fprintf(f_log, "\n");
   }

   if (is_verbose) {
      string qty = TUCRList::GetCREntry(j_cr).GetName();
      if (counter <= n_iter_max)
         fprintf(f_log, "%s    %s: tertiary contribution convergence (%.2le) reached after %d iterations\n",
                 indent.c_str(), qty.c_str(), eps, counter);
      else {
         string message = Form("%s%s: WARNING -- tertiary contribution HAS NOT CONVERGED after %d iterations",
                               indent.c_str(), qty.c_str(), counter);
         TUMessages::Warning(f_log, "TUModel1DKisoVc", "IterateTertiariesN0", string(message));
      }
   }

   // Free memory
   delete[] stermtot_ter;
   stermtot_ter = NULL;
   delete[] stermtot_ter_old;
   stermtot_ter_old = NULL;

   delete[] n0_ref;
   n0_ref = NULL;

   if (stermprim_ter)
      delete[] stermprim_ter;
   stermprim_ter = NULL;
   if (stermprim_ter_old)
      delete[] stermprim_ter_old;
   stermprim_ter_old = NULL;
}

//______________________________________________________________________________
Double_t TUModel1DKisoVc::Nz_WithoutParentBETA(Double_t const &n0, Int_t j_cr,
      Int_t k_ekn, Double_t z)
{
   //--- Returns N(z) from N(0) when CR has no BETA-unstable parent [(GeV/n)^{-1} m^{-3}]
   //    (see Nz_WithParentBETA otherwise). The standard formula is (see, e.g.,
   //    Putze et al, 2010): BEGIN_LATEX
   //      N(z)=N(0) #times exp#left{#frac{V_{c}z}{2K}#right) #times #frac{sinh(S1D[L-z]/2)}{sinh(S1D L/2)}
   //    END_LATEX
   //    where N(0) is the solution at z=0 with E gains and losses.
   //    N.B.: if S1D=0 (no wind and for a stable species), the solution becomes BEGIN_LATEX
   //      N(z)=N(0) #times #frac{L-z}{L}.
   //    END_LATEX
   //
   //  n0                Differential density N(z=0)
   //  j_cr              Index of CR in list
   //  k_ekn             Index of kinetic energy per nucleon
   //  z                 Position in the disc/halo [kpc]


   // The solution is even
   z = fabs(z);
   if (fabs(z) < 1.e-20) return n0;


   // Two cases depending on S1D
   Double_t k_diff = ValueK(j_cr, k_ekn);
   Double_t s1d = S1D(j_cr, k_ekn, k_diff);
   if (fabs(s1d) < 1.e-40)
      return n0 * (GetParVal_L() - z) / GetParVal_L();
   else
      return n0 * exp(0.5 * ValueVc_kpcperMyr() * z / k_diff)
             * TUMath::SinhRatio(s1d * GetParVal_L() * 0.5, s1d * z * 0.5);
}

//______________________________________________________________________________
Double_t TUModel1DKisoVc::Nz_WithParentBETA(Int_t j_parent, Int_t j_cr, Int_t k_ekn, Double_t z)
{
   //--- Returns N(z) if CR has a BETA-unstable parent  [(GeV/n)^{-1} m^{-3}]
   //    (see Nz_WithoutParentBETA otherwise).
   //    N.B: the formula are tricky, especially when a local bubble underdensity
   //    is enabled (i.e., rh>0). Details of the formulae are given in Putze et
   //    al. (2010).
   //
   //  j_parent          Index of the radioactive parent (in list of CRs)
   //  j_cr              Index of CR in list
   //  k_ekn             Index of kinetic energy per nucleon
   //  z                 Position in the disc/halo [kpc]

   // The solution is even
   z = fabs(z);

   // Base variables required for calculation
   Double_t vc = ValueVc_kpcperMyr();
   Double_t kdiff_daughter = ValueK(j_cr, k_ekn);
   Double_t kdiff_parent = ValueK(j_parent, k_ekn);
   Double_t s1d_daughter = S1D(j_cr, k_ekn, kdiff_daughter);
   Double_t s1d_parent = S1D(j_parent, k_ekn, kdiff_parent);
   Double_t gamma_inel_daughter = 0.;
   if (TUPropagSwitches::IsDestruction())
      gamma_inel_daughter = TUXSections::Get_nvSigTotIA_ISM0D(j_cr, k_ekn);
   Double_t gamma_rad_daughter = 0.;
   Double_t gamma_rad_parent = 0.;
   if (TUAxesCrE::GetCREntry(j_cr).GetBETAHalfLife() > 1.e-40)
      gamma_rad_daughter = TUAxesCrE::GammaRadBETA_perMyr(j_cr, k_ekn);
   if (TUAxesCrE::GetCREntry(j_parent).GetBETAHalfLife() > 1.e-40)
      gamma_rad_parent = TUAxesCrE::GammaRadBETA_perMyr(j_parent, k_ekn);
   Double_t invsh_parent = TUMath::InvSinh(s1d_parent * GetParVal_L() * 0.5);
   Double_t ch_daughter = cosh(s1d_daughter * GetParVal_L() * 0.5);
   Double_t coth_parent = TUMath::Coth(s1d_parent * GetParVal_L() * 0.5);


   // N(0)^parent if rh\neq0 (local underdensity)
   // -------------------------------------------
   // By construction in the code, the flux Nr of a BETA-unstable CR includes
   // a damping factor, see Donato, Maurin, Taillet, A&A 381, 539 (2002),
   //     damping = exp(-r_hole/sqrt(Kdiff*gamma*tau))
   //             = exp(-r_hole/sqrt(Kdiff/Gamma_rad))
   // to take into account the effect of the local bubble. By construction,
   // the factor is applied to all positions whereas it should apply only
   // in the solar neighbourhood. The daughter of such a nucleus is stable
   // and thus the effective diffusive zone where it contributes to the flux
   // (see, e.g., Taillet and Maurin, 2003) does not feel this damping factor.
   // We thus correct from this 'inappropriate' correction below:
   Double_t damping = 1.;
   if (GetParVal_rh() > 1.e-5) {
      damping = exp(-GetParVal_rh() / sqrt(kdiff_parent / gamma_rad_parent));
      //cout  << GetCREntry(j_cr).GetName() << " " << k_ekn << " damping=" << damping << endl;
   }
   Double_t n0_parent = GetN0Tot(j_parent, k_ekn) / damping;


   // Some useful variables (k=index for BETA-rad parent, j=index for CR daughter)
   /*-------------------------------------------------------------------|
   |  Qty                 Expression                     |     Unit     |
   |--------------------------------------------------------------------|
   | bob                  1/K^k-1/K^j                    | kpc^{-2} Myr |
   | alpha            exp[ VcL/2*bob) ]                  |      1       |
   | a     Vc^2/(2K^k)*bob+gamma_r^k/K^k-gamma_r^j/K^j   |   kpc^{-2}   |
   | ai            S1D^kVc/2 * (1/K^k-1/K^j)             |   kpc^{-2}   |
   | theta   -gamma_r^k*N^k/[sinh(SiL/2)*K^j*(a^2-ai^2)] |   kpc^2*[N]  |
   | theta_shparent    theta/sinh(SiL/2)                 |   kpc^2*[N]  |
   | fact1           Vc + 2*h*gamma_inel^j               | kpc Myr^{-1} |
   | fact2      Vc*(2-K^j/K^k) + 2*h*gamma_inel^j        | kpc Myr^{-1} |
   |-------------------------------------------------------------------*/
   Double_t bob = (1. / kdiff_parent - 1. / kdiff_daughter);
   Double_t alpha = exp((vc * GetParVal_L() * 0.5) * bob);
   Double_t a = (0.5 * TUMath::Sqr(vc) / kdiff_parent) * bob
                + gamma_rad_parent / kdiff_parent
                - gamma_rad_daughter / kdiff_daughter;
   Double_t ai = 0.5 * vc * s1d_parent * bob;
   Double_t theta = -gamma_rad_parent * n0_parent * invsh_parent
                    / (kdiff_daughter * (a * a - ai * ai));
   Double_t theta_shparent = -gamma_rad_parent * n0_parent
                             / (kdiff_daughter * (a * a - ai * ai));
   Double_t fact1 = vc + 2.* GetParVal_h() * gamma_inel_daughter;
   Double_t fact2 = vc * (2. - kdiff_daughter / kdiff_parent)
                    + 2.* GetParVal_h() * gamma_inel_daughter;

   // From these variables, we define xi and wi (see Putze et al., 2010):
   //    xi = theta*(-alpha*ai/ch_daughter + a*sh_parent + ai*ch_parent);
   //    wi = theta*(  alpha*ai/ch_daughter*fact1
   //            -  sh_parent*(a*fact2 + ai*kdiff_daughter*s1d_parent)
   //           -  ch_parent*(ai*fact2 + a*kdiff_daughter*s1d_parent)  );
   // which are actually rewritten using theta_shparent instead of theta
   // (to avoid numerical issues in xi and wi for large sinh arguments:
   //    xi = theta_shparent*(-alpha*ai/ch_daughter/sh_parent + a + ai*coth_parent);
   //    wi = theta_shparent*(  alpha*ai/ch_daughter/sh_parent*fact1
   //             -              (a*fact2 + ai*kdiff_daughter*s1d_parent)
   //             -  coth_parent*(ai*fact2 + a*kdiff_daughter*s1d_parent)  );
   Double_t xi = theta_shparent
                 * (-alpha * ai / ch_daughter * invsh_parent + a + ai * coth_parent);
   Double_t wi = theta_shparent
                 * (alpha * ai / ch_daughter * invsh_parent * fact1
                    - (a * fact2 + ai * kdiff_daughter * s1d_parent)
                    - coth_parent * (ai * fact2 + a * kdiff_daughter * s1d_parent));


   // Solution
   //---------
   if (z < 1.e-3) {  // Case z=0
      // What we are looking for in this case is the specific contribution to
      // add to Sterm (where the primary Qi and the secondary 2hGamma^{k->j}N^k
      // contributions have already been taken into account; see in
      // CalculateChargedCRs...). The dimension [Sterm_rad] should be the same
      // as [Sterm].
      //
      // The solution when radioactive losses exist is expressed as
      //    N(0) = Ci + xi = [ (Qi+2hGamma^{k->j}N^k(0))/A1D + wi/A1D] + xi
      // The relevance of using a separate term xi does probably not appear clearly
      // here, but it cannot be avoided (see Putze et al., 2010). Note also that we
      // have isolated the explicit contribution from the radioactive term).
      // Actually, in CalculateChargedCRs, the standard contributions (standard
      // primary + secondary) have already been taken into account... the remaining
      // quantity to return is thus
      //    Sterm_rad = wi/A1D + xi

      Double_t N0_rad = (wi / GetA1DTerm(j_cr, k_ekn) + xi);
      // [N0_rad]= (GeV/n)^{-1} m^{-3} = [dNdE]
      // N.B.: in z=0, only returns the radioactive contribution
      //(not the primary and secondary one)
      return N0_rad;
   } else {  // Case z!=0
      // In this case, it is assumed that the N^j(0) have been calculated in a
      // previous step, so it really contains all the contributions (primary,
      // secondary, radioactive...) when GetN0Tot(j_cr,k_ekn) is called.
      //
      // For the z-dependent formula,
      // N^j(z)= exp(Vc*z/2K^j)*
      //            (   [N^j(0)-xi] * [sinh(S1D^j(L-z)/2)/sinh(S1D^jL/2)]
      //               - alpha*theta*ai * cosh(S1D^j z/2.)/cosh(S1D^j L/2.)
      //            )
      //        + theta*exp(Vc*z/2K^k)* ( a*sinh(S1D^k(L-z)/2.) + ai*cosh(S1D^k(L-z)/2.) )
      //
      // However, if Vc=0 and gamma_rad_daughter=0, then s1d_daughter=0,
      // and to avoid a numerical problem, we have to make the subsitution
      //   TUMath::SinhRatio(s1d_daughter*L*0.5,s1d_daughter*z*0.5) => (L-z)/L
      Double_t z_trick = 0.;
      if (fabs(s1d_daughter) < 1.e-40)
         z_trick = (GetParVal_L() - z) / GetParVal_L();
      else
         z_trick = TUMath::SinhRatio(s1d_daughter * GetParVal_L() * 0.5, s1d_daughter * z * 0.5);

      //Double_t Nz_rad= exp(vc*z*0.5/kdiff_daughter)*
      //    (  (GetN0Tot(j_cr,k_ekn) -xi) * z_trick
      //       - alpha*theta*ai * cosh(s1d_daughter*z*0.5)/cosh(s1d_daughter*L*0.5) )
      //   + theta*exp(vc*z*0.5/kdiff_parent)
      //       * ( a*sinh(s1d_parent*(L-z)*0.5) + ai*cosh(s1d_parent*(L-z)*0.5) );
      Double_t Nz_rad = exp(vc * z * 0.5 / kdiff_daughter) *
                        ((GetN0Tot(j_cr, k_ekn) - xi) * z_trick
                         - alpha * theta * ai * cosh(s1d_daughter * z * 0.5) / cosh(s1d_daughter * GetParVal_L() * 0.5))
                        + theta_shparent * exp(vc * z * 0.5 / kdiff_parent)
                        * (a * TUMath::SinhRatio(s1d_parent * GetParVal_L() * 0.5, s1d_parent * z * 0.5)
                           + ai * TUMath::CoshToSinh(s1d_parent * GetParVal_L() * 0.5, s1d_parent * z * 0.5));
      // [Nz_rad]= (GeV/n)^{-1} m^{-3} = [dNdE]
      return Nz_rad;
   }
}

//______________________________________________________________________________
Bool_t TUModel1DKisoVc::SetClass_ModelSpecific(TUInitParList *init_pars, Bool_t is_verbose, FILE *f_log)
{
   //--- Set class members specific to model selected.
   //  init_pars         TUInitParList class of initialisation parameters
   //  is_verbose        Verbose or not when class is set

   if (is_verbose) {
      string indent = TUMessages::Indent(true);
      fprintf(f_log, "%s[TUModel1DKisoVc::SetClass_ModelSpecific]\n", indent.c_str());
      fprintf(f_log, "%s   => No model-specific action required to set the class\n", indent.c_str());
      fprintf(f_log, "%s[TUModel1DKisoVc::SetClass_ModelSpecific] <DONE>\n", indent.c_str());
      TUMessages::Indent(false);
   }

   return true;
}

//______________________________________________________________________________
Double_t TUModel1DKisoVc::S1D(Int_t j_cr, Int_t k_ekn, Double_t const &kdiff)
{
   //--- Returns S1D=sqrt( (Vc/K)^2 + 4*gamma_rad/K )    [/kpc]
   //    with [Vc]=kpc/Myr and [kdiff]=kpc^2/Myr
   //
   //  j_cr              CR index
   //  k_ekn             Energy index
   //  kdiff             Diffusion coefficient for this CR and E [kpc^2/Myr]

   // Is is a BETA-radioactive CR? Is is taken into account for propagation?
   Double_t rad_beta = 0.;
   if (GetCREntry(j_cr).GetBETAHalfLife() > 1.e-40 && TUPropagSwitches::IsDecayBETA())
      rad_beta =  4.* TUAxesCrE::GammaRadBETA_perMyr(j_cr, k_ekn) / kdiff;

   return sqrt(TUMath::Sqr((ValueVc_kpcperMyr() / kdiff)) + rad_beta);
}

//______________________________________________________________________________
Double_t TUModel1DKisoVc::S1D(Int_t j_cr, Int_t k_ekn)
{
   //--- Returns S1D=sqrt( (Vc/K)^2 + 4*gamma_rad/K )    [/kpc]
   //    with [Vc]=kpc/Myr and [kdiff]=kpc^2/Myr
   //
   //  j_cr              CR index
   //  k_ekn             Energy index

   return S1D(j_cr, k_ekn, ValueK(j_cr, k_ekn));
}

//______________________________________________________________________________
void TUModel1DKisoVc::TEST(TUInitParList *init_pars, FILE *f)
{
   //--- Test function for this class (called in usine -t).
   //  init_pars         TUInitParList class of initialisation parameters
   //  f                 Output file for test

   string message = "Load 1DKisoVc Model and test.";
   TUMessages::Test(f, "TUModel1DKisoVc", message);

   // Set class and test methods
   fprintf(f, " * Load class\n");
   fprintf(f, "   > TUModelBase::SetClass(init_pars=\"%s\", Model1DKisoVc, is_verbose=false, f_log=f);\n", init_pars->GetFileNames().c_str());
   TUModelBase::SetClass(init_pars, "Model1DKisoVc", false, f);

   fprintf(f, " * Basic prints\n");
   fprintf(f, "   > PrintSummaryBase(f)\n");
   PrintSummaryBase(f);
   fprintf(f, "   > PrintPars(f)\n");
   PrintPars(f);
   fprintf(f, "   > PrintSummaryModel(f)\n");
   PrintSummaryModel(f);
   fprintf(f, "   > PrintSummaryTransport(f)\n");
   PrintSummaryTransport(f);
   fprintf(f, "   > PrintSources(f, true)\n");
   PrintSources(f, true);
   fprintf(f, "\n");

   fprintf(f, "   > PrintSummary(f)\n");
   PrintSummary(f);
   fprintf(f, "\n");

   fprintf(f, "N.B.: run ./bin/usine to see the model in action!");
   fprintf(f, "\n");
}

//______________________________________________________________________________
Double_t TUModel1DKisoVc::ValueK(Int_t j_cr, Int_t k_ekn, Int_t halfbin_status)
{
   //--- Returns K(E) for a given CR and energy bin [kpc^2/Myr].
   //  j_cr              CR index
   //  k_ekn             Energy index
   //  halfbin_status    Evaluated at bine (0), bine-1/2 (-1), or bine+1/2 (+1)
   // Update E coordinate
   TUModelBase::UpdateCoordE(j_cr, k_ekn, halfbin_status);

   return TUModelBase::ValueK(TUModelBase::GetCoordE());
}

//______________________________________________________________________________
Double_t TUModel1DKisoVc::ValueKpp(Int_t j_cr, Int_t k_ekn, Int_t halfbin_status)
{
   //--- Returns Kpp(E) for a given CR and energy bin [GeV^2/Myr].
   //  j_cr              CR index
   //  k_ekn             Energy index
   //  halfbin_status    Evaluated at bine (0), bine-1/2 (-1), or bine+1/2 (+1)
   // Update E coordinate
   TUModelBase::UpdateCoordE(j_cr, k_ekn, halfbin_status);

   return TUModelBase::ValueKpp(TUModelBase::GetCoordE());
}
