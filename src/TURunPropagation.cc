// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

// C/C++ include
#include <utility>
#include <fstream>
// ROOT include
#include <TApplication.h>
#include <TAxis.h>
#include <TFile.h>
#include <TGaxis.h>
#include <TLatex.h>
#include <TLegend.h>
#include <TLegendEntry.h>
#include <TMath.h>
#include <TMatrixTSym.h>
#include <TROOT.h>
// USINE include
#include "../include/TUMath.h"
#include "../include/TUMessages.h"
#include "../include/TUMisc.h"
#include "../include/TUModel0DLeakyBox.h"
#include "../include/TUModel1DKisoVc.h"
#include "../include/TUModel2DKisoVc.h"
#include "../include/TURunPropagation.h"
#include "../include/TUSolMod0DFF.h"

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TURunPropagation                                                     //
//                                                                      //
// Run propagation models (command line and/or text-user interface).    //
//                                                                      //
// The class TURunPropagation handles multiple runs (propag. models)    //
// at the same time if necessary. It provides the user with several     //
// options to run, display, and check the results of propagation runs   //
// in different configurations.                                         //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

ClassImp(TURunPropagation)

//______________________________________________________________________________
TURunPropagation::TURunPropagation()
{
   // ****** Default constructor ******

   fIsSaveInFiles = false;
   fIsShowPlots = false;
   fOutputDir = "$USINE";
   fOutputLog = NULL;

   Initialise(false);
}

//______________________________________________________________________________
TURunPropagation::~TURunPropagation()
{
   // ****** Default destructor ******

   Initialise(true);
}

//______________________________________________________________________________
Double_t TURunPropagation::Chi2Cov(vector<Double_t> &model, vector<Double_t> &data, TMatrixTSym <Double_t> &cov) const
{
   //--- Calculates chi2 between model and data given a covariance matrix of errors.
   //  model                   Vector of model points
   //  data                    Vector of data points
   //  cov                     Covariance matrix (symmetric) of errors


   // Check sizes
   Int_t n_data = data.size();
   if (model.size() != n_data)
      TUMessages::Error(stdout, "TURunPropagation", "Chi2Data",
                        Form("Not same number of entries for model (%d) and data (%d)",
                             (Int_t)model.size(), n_data));
   else if (cov.GetNrows() != 0 && model.size() != cov.GetNrows())
      TUMessages::Error(stdout, "TURunPropagation", "Chi2Data",
                        Form("Not same size for model (%d) and covariance matrix of error (%d)",
                             (Int_t)model.size(), (Int_t)cov.GetNrows()));

   // chi2 with covariance matrix of error
   //     chi2_cov = sum_{i=0,n_data and j=0,n_data} (data_i-model_i) (CoV)^{-1}_ij (data_j-model_j)
   // which requires to invert the covariance matrix

   // i) Invert matrix
   TMatrixTSym <Double_t> inv_cov(n_data);
   inv_cov = cov.Invert();
   //inv_cov = cov.InvertFast();

   // ii) Add in chi2
   Double_t chi2 = 0.;
   for (Int_t p = 0; p < n_data; ++p) {
      Double_t chi2_p = 0.;
      for (Int_t q = 0; q < n_data; ++q)
         chi2_p += (data[p] - model[p]) * inv_cov[p][q] * (data[q] - model[q]);
      chi2 += chi2_p;
   }

   return chi2;
}

//______________________________________________________________________________
Double_t TURunPropagation::Chi2FromNuisance(Double_t const &xs_lc_constraint) const
{
   //--- If nuisance parameters (all assumed to be Gaussian), returns associated chi2_nuis.
   //  xs_lc_constraint   Constraint to enforce on the sum of XS linear combination coefficients (if applies)

   Double_t chi2_nuis = 0.;

   // Check whether specific case of XS LC parameters
   Bool_t is_xs_lc = false;
   if (fPropagModels[0]->TUXSections::ParsXS_LinComb_GetNReac(0) != 0 ||
         fPropagModels[0]->TUXSections::ParsXS_LinComb_GetNReac(1) != 0)
      is_xs_lc = true;


   // If no XS LC parameters, standard calculation of chi2_nuis
   for (Int_t l = 0; l < fFitPars->GetNPars(); ++l) {
      //par->Print();

      // If not a NUISANCE parameter, skip
      if (fFitPars->GetParEntry(l)->GetFitType() != kNUISANCE)
         continue;

      // NUISANCE parameter
      TUFreeParEntry *par = fFitPars->GetParEntry(l);
      Double_t par_val = par->GetVal(par->GetFitSampling());
      // Its chi2 contribution (Gaussian-distributed parameter) is
      //    chi2_nuis = [(val-mu)/sigma]^2 if in range
      //    chi2_nuis >> 1                 outside user-specified range
      if (par_val < par->GetFitInitMin() || par_val > par->GetFitInitMax())
         return 1.e40;
      else if (par->GetFitName().find("LCInel") == string::npos
               && par->GetFitName().find("LCProd") == string::npos) {
         // Standard nuisance parameter
         // N.B.: if nuisance parameter is related to 'Linear Combination of XS', i.e.,
         // contains the keyword "LCInel" or ""LCProd", instead of adding a penalty on
         // each parameter, we have a penalty on the sum of CL parameters (see below)
         Double_t tmp = (par_val - par->GetFitInit()) / par->GetFitInitSigma();
         chi2_nuis += tmp * tmp;
         //cout << " Val.test=" << par_val << "   Val.init=" << par->GetFitInit()
         //     << "   Sigma.init=" << par->GetFitInitSigma() << "   Contrib. to chi2_nuis="
         //     << tmp*tmp << endl;
      }
      par = NULL;
   }

   // For the specific case of Linear Combination of XS, add constraint
   if (is_xs_lc) {
      const Int_t n_xstype = 2;
      vector<Double_t> sum_lc_pars[n_xstype]; // Sum for inel. and prod. pars
      vector<Double_t> sigma_lc_pars[n_xstype]; // sigma for inel. and prod. pars
      vector<string> reac_lc_pars[n_xstype]; // Reac names for inel. and prod. pars
      Int_t n_lc_reacs[n_xstype] = {fPropagModels[0]->TUXSections::ParsXS_LinComb_GetNReac(0),
                                    fPropagModels[0]->TUXSections::ParsXS_LinComb_GetNReac(1)
                                   };

      // Retrieve Sum and sigma for all reactions in LC (and names)
      // to add, for each reaction, the constraint:
      //     \sum_ coeff = xs_lc_constraint +/- sigma
      SumCiAndSigmaForXSLC(&sum_lc_pars[0], &sigma_lc_pars[0], &reac_lc_pars[0]);
      if (n_lc_reacs[0] != 0 || n_lc_reacs[1] != 0) {
         // Loop on inelastic and production XS
         for (Int_t is_inel_ow_prod = 1; is_inel_ow_prod >= 0; --is_inel_ow_prod) {
            // Loop on all reactions
            for (Int_t i_r = 0; i_r < n_lc_reacs[is_inel_ow_prod]; ++i_r) {
               //cout << "   ... lin.comb.XS: sum coeffs ["
               //     << fPropagModels[0]->TUXSections::ParsXS_LinComb_GetNameReac(i_r,is_inel_ow_prod)
               //     << "] = " << sum_lc_pars[is_inel_ow_prod][i_r]
               //     <<  " (constraint on sum is " << xs_lc_constraint
               //     << " +/- " <<  sigma_lc_pars[is_inel_ow_prod][i_r] << ")" << endl;
               Double_t tmp = (sum_lc_pars[is_inel_ow_prod][i_r] - xs_lc_constraint) / sigma_lc_pars[is_inel_ow_prod][i_r];
               chi2_nuis += tmp * tmp;
            }
         }
      }
   }

   return chi2_nuis;
}

//______________________________________________________________________________
Double_t TURunPropagation::Chi2_TOAFluxes(const Double_t *pars)
{
   //--- Returns Chi2 value over all analysed experiments. This is the function to minimise
   //    to extract the propagation model parameters.
   //
   //  pars   Parameters[N] to minimise, N=GetNFittedPars()

   //clock_t tttt = clock();

   // If model is not Force-Field, cannot proceeds here
   if (fSolModModels[0]->GetModel() != kSOLMOD0DFF) {
      string message = "Only Force-Field enabled for now here!";
      TUMessages::Error(fOutputLog, "TURunPropagation", "Chi2_TOAFluxes", message);
   }

   // 1) Update fFreePars from new 'pars'
   for (Int_t l = 0; l < fFitPars->GetNPars(); ++l)
      fFitPars->GetParEntry(l)->SetVal(pars[l], fFitPars->GetParEntry(l)->GetFitSampling());

   // 2) Calculate chi2 contribution from nuisance parameters
   Double_t chi2 = 0.;
   chi2 += Chi2FromNuisance();

   // 3) Update propagation and get local IS fluxes
   // N.B.: FillSolarSystemFluxes() is called every time re-normalisation to data
   // is enforced, but if not, we have to ensure here that we fill local spectra!
   Propagate(fIsUseNormList, false);
   fPropagModels[0]->FillSolarSystemFluxes();
   TUValsTXYZECr *fluxes = fPropagModels[0]->GetSolarSystemFluxesTot();
   //fluxes->PrintVals(stdout);

   // Loop on sub-experiments (one modulation level per exp)
   //cout << "    ** Nexp=" << fFitData->GetNExps() << endl;
   for (Int_t i_exp = 0; i_exp < fFitData->GetNExps(); ++i_exp) {

      // Get/update modulation model
      Int_t i_exp_in_freepars = fSolModModels[0]->ParsModExps_Index(fFitData->GetExp(i_exp));
      if (i_exp_in_freepars < 0) {
         // This is not a free par => use phi value from CR data file
         // Find data for fist non-null e_type (for same i_exp, different e_type have same modulation level)
         Double_t phi = 0.;
         for (Int_t e = 0; e < gN_ETYPE; ++e) {
            gENUM_ETYPE e_type = TUEnum::gENUM_ETYPE_LIST[e];
            Int_t i_data = fFitData->IndexData(i_exp, e_type);
            if (i_data >= 0)
               phi = fFitData->GetEntry(i_data)->GetExpphi();
         }
         fSolModModels[0]->SetParVal(0, phi);
      } else {
         // This is a fitted value, whose updated fit parameter values (in fFitPars)
         // points to "i_exp_in_freepars". So we just have to update sol.mod.class
         // to this in order to use the desired modulation parameters
         fSolModModels[0]->ParsModExps_UpdateSolModPars(i_exp_in_freepars);
      }
      Double_t chi2_exp = 0.;
      // Loop on CR quantities (several per sub-exp)
      vector<Int_t> indices_qties;
      fFitData->IndicesQtiesInExp(i_exp, indices_qties);
      //cout << "      * For i_exp=" << i_exp << " " << fFitData->GetExp(i_exp) << ", Nqties=" << indices_qties.size() << endl;
      for (Int_t i = 0; i < (Int_t)indices_qties.size(); ++i) {
         Int_t i_qty = indices_qties[i];
         // Get maximal E_axis range for combo of e_type
         string qty = fFitData->GetQty(i_qty);

         // Loop on Etypes
         for (Int_t e = 0; e < gN_ETYPE; ++e) {
            gENUM_ETYPE e_type = TUEnum::gENUM_ETYPE_LIST[e];
            Int_t i_data = fFitData->IndexData(i_exp, i_qty, e_type);
            if (i_data < 0)
               continue;

            // Model calculation for all data points:
            Int_t n_data = fFitData->GetNData(i_exp, i_qty, e_type);
            vector<Double_t> l_emin(n_data, 0.);
            vector<Double_t> l_emax(n_data, 0.);
            vector<Double_t> l_emean(n_data, 0.);
            // Retrieve desired data points
            for (Int_t p = 0; p < n_data; ++p) {
               l_emin[p] = fFitData->GetEntry(i_data + p)->GetEBinL();
               l_emax[p] = fFitData->GetEntry(i_data + p)->GetEBinU();
               l_emean[p] = fFitData->GetEntry(i_data + p)->GetEMean();
            }
            // Calculate model
            TUCoordTXYZ *coord_txyz = NULL;
            vector<Double_t> vmodel;
            if (fIsUseBinRange)
               vmodel = fluxes->IntegratedValOnERange(qty, e_type, fNExtraInBinRange, l_emin, l_emax, 0., coord_txyz, fSolModModels[0]);
            else
               vmodel = fluxes->IntegratedValOnERange(qty, e_type, -1, l_emean, l_emean, 0., coord_txyz, fSolModModels[0]);
            // If at least one error type set as nuisance, model=model_calc*model_bias'
            if (fFitData->GetEntry(i_data)->IsAtLeast1NuisPar()) {
               for (Int_t p = 0; p < n_data; ++p)
                  vmodel[p] *= fFitData->ParsData_ModelBias(i_data + p);
               //cout << "bias " << p_data << " = " << fFitData->ParsData_ModelBias(i_data + p) << endl;
            }

            Double_t chi2_exp_qty = 0.;

            // Calculate  chi2: depends whether covariance matrix is used
            Bool_t is_cov = false;
            if (fFitDataErrType == kERRCOV && fFitData->GetEntry(i_data)->GetNCovE() != 0)
               is_cov = true;

            if (is_cov) {
               // Cov_ij = \sum_{types} rho_ij^{type}*sigma_i^{type}*sigma_j^{type}*model_i*model_j
               //        = \sum_{types} fFitData->GetEntry(i)->GetYErrRelCov(type, j) *model_i*model_j
               vector<Double_t> vdata(n_data, 0.);
               TMatrixTSym <Double_t> cov(n_data);
               for (Int_t t = 0; t < fFitData->GetEntry(i_data)->GetNCovTypes(); ++t) {
                  // If type used as nuisance parameter, do not use it in covariance!
                  if (fFitData->GetEntry(i_data)->IsCovTypeNuisPar(t))
                     continue;
                  //cout << fFitData->GetEntry(i_data)->GetYErrRelCovType(t) << " " << fFitData->GetEntry(i_data)->IsCovTypeNuisPar(t) << endl;
                  for (Int_t ii = 0; ii < n_data; ++ii) {
                     vdata[ii] = fFitData->GetEntry(ii + i_data) ->GetY();
                     for (Int_t j = 0; j < n_data; ++j) {
                        Double_t valy_ij = fFitData->GetEntry(i_data + ii)->GetY() * fFitData->GetEntry(i_data + j)->GetY();
                        // If stat. err. use data, otherwise use model
                        if (fFitData->GetEntry(ii + i_data)->GetYErrRelCovType(t) != "STAT" && fIsModelOrDataForRelCov)
                           valy_ij = vmodel[ii] * vmodel[j];

                        if (t == 0) // first time accessed
                           cov[ii][j] = fFitData->GetEntry(ii + i_data)->GetYErrRelCov(t, j) * valy_ij;
                        else
                           cov[ii][j] += fFitData->GetEntry(ii + i_data)->GetYErrRelCov(t, j) * valy_ij;
                     }
                  }
               }
               chi2_exp_qty = Chi2Cov(vmodel, vdata, cov);
            } else {
               // chi2 = sum_i (data_i-model_i)^2/(data_sigma_i)^2
               // N.B.: data_sigma_UP (resp. LO) is used if model above (resp. below) data
               for (Int_t p = 0; p < n_data; ++p) {
                  Double_t data_sigma = 0.;
                  TUDataEntry *data = fFitData->GetEntry(i_data + p);
                  if (vmodel[p] > data->GetY())
                     data_sigma = fabs(data->GetYErrU(fFitDataErrType));
                  else
                     data_sigma = fabs(data->GetYErrL(fFitDataErrType));
                  chi2_exp_qty += (data->GetY() - vmodel[p]) * (data->GetY() - vmodel[p]) / (data_sigma * data_sigma);
                  //cout << " p=" << p
                  //     << " data_p=" << data->GetY() << " model_p=" << vmodel[p]
                  //     << " datasigma_p=" << data_sigma << endl;
                  data = NULL;
               }
            }
            chi2_exp += chi2_exp_qty;
            chi2 += chi2_exp_qty;

            //cout << "       "
            //     << fFitData->  GetExp(i_exp) << " " << qty
            //     << " @ phi=" << fSolModModels[0]->GetParEntry(0)->GetVal()
            //     << "  => chi2_exp_qty=" <<  chi2_exp_qty << endl;
         }
         //cout << "    "
         //     << fFitData->  GetExp(i_exp) << " (all quantities)"
         //     << " @ phi=" << fSolModModels[0]->GetParEntry(0)->GetVal()
         //     << "  => chi2_exp=" <<  chi2_exp<< endl;
      }
   }

   //tttt = clock() - tttt;
   //printf("... %f seconds)\n",((float)tttt)/CLOCKS_PER_SEC);

   //cout << " All CRs => chi2=" <<  chi2 << endl;
   fluxes = NULL;
   return chi2;
}

//______________________________________________________________________________
void TURunPropagation::ClearScansOrProfiles(Bool_t is_scan_or_prof)
{
   //--- Initialize/Free memory of fScans or fProfiles
   //  is_scan_or_prof   Act on fScans(true) or fProfiles (false)

   if (is_scan_or_prof) {
      for (Int_t i = 0; i < GetNScans(); ++i) {
         if (fScans[i].gr)
            delete fScans[i].gr;
         fScans[i].gr = NULL;
      }
      fScans.clear();
   } else {
      for (Int_t i = 0; i < GetNProfiles(); ++i) {
         if (fProfiles[i].gr)
            delete fProfiles[i].gr;
         fProfiles[i].gr = NULL;
      }
      fProfiles.clear();
   }
}

//______________________________________________________________________________
void TURunPropagation::ExtractFromFitData(Int_t i_solmod, TURunOutputs &outputs, Bool_t is_verbose, FILE *f_out) const
{
   //--- Extract from queried quantities in 'outputs' (if fFitData) the modulation levels
   //    (from native data file) by best-fit modulation values whenever enabled as nuisance
   //    parameters.
   //    N.B.: there can be only unique combo[i]/etype[i] combination (but allows different
   //    e_types for same combo)
   // INPUTS:
   //  i_solmod          Index of solar modulation model to use
   //  outputs           List of combo/Etypes/phiff pre-loaded
   //  is_verbose        Chatter on or off
   //  f_out             Output file for chatter
   // OUTPUTS:
   //  outputs           Updated phiFF, but also iexp_fitdata and is_phinuis


   vector<vector<Double_t> > list_phiff;
   vector<vector<Int_t> > list_iexp_fitdata;
   vector<vector<Bool_t> > list_isphinuis;

   for (Int_t i = 0; i < outputs.GetNCombos(); ++i) {
      Int_t i_qty = fFitData->IndexInQties(outputs.GetCombo(i));
      // Loop on all experiments and search for phi
      vector<Int_t> exps;
      vector<Double_t> phis;
      vector<Bool_t> is_nuis;
      for (Int_t i_exp = 0; i_exp < fFitData->GetNExps(); ++i_exp) {
         Int_t i_data = fFitData->IndexData(i_exp, i_qty, outputs.GetEType(i));
         if (i_data < 0)
            continue;
         else {
            string exp_name = fFitData->GetEntry(i_data)->GetExpName();
            TUMisc::RemoveSpecialChars(exp_name);
            Double_t phi_val = fFitData->GetEntry(i_data)->GetExpphi();
            Int_t i_parmod = fSolModModels[i_solmod]->ParsModExps_Index(exp_name);
            Bool_t is = false;
            if (i_parmod >= 0) {
               string name_par = fSolModModels[i_solmod]->ParsModExps_Get(i_parmod)->GetParEntry(0)->GetName();
               Int_t i_freepar = fFitPars->IndexPar(name_par);
               if (i_freepar >= 0) {
                  phi_val = fFitPars->GetParEntry(i_freepar)->GetFitValBest();
                  is = true;
               }
            }
            exps.push_back(i_exp);
            is_nuis.push_back(is);
            phis.push_back(phi_val);
         }
      }
      list_phiff.push_back(phis);
      list_iexp_fitdata.push_back(exps);
      list_isphinuis.push_back(is_nuis);
   }

   // Check that correct number of quantities to display
   for (Int_t i = 0; i < outputs.GetNCombos(); ++i) {
      if (fFitData && (list_isphinuis[i].size() != list_phiff[i].size()
                       || list_isphinuis[i].size() != list_iexp_fitdata[i].size())) {
         string message = Form("For combo[i=%d]=%s not the same number of entries in list_isphinuis[i], list_phiff[i], and list_iexp_fitdata[i], this should not happen!",
                               i, outputs.GetCombo(i).c_str());
         TUMessages::Error(fOutputLog, "TURunPropagation", "ExtractFromFitData", message);
      }
   }

   // Update in outputs
   outputs.SetFitDataRelated(fFitData, list_phiff, list_iexp_fitdata, list_isphinuis);
}

//______________________________________________________________________________
void TURunPropagation::ExtractIndicesContours(TUInitParList *init_pars, Int_t i_initpar)
{
   //--- Extracts from index of initialisation parameters the indices in fContours.
   //  init_pars         Initialisation parameters
   //  i_initpars        Index of parameter in init_par

   if (i_initpar < 0)
      return;

   // If free parameters, loop on parameter names
   for (Int_t m = 0; m < init_pars->GetParEntry(i_initpar).GetNVals(); ++m) {
      string par_entry = init_pars->GetParEntry(i_initpar).GetVal(m);
      // Extract
      vector<string> par_n;
      vector<string> pars;
      vector<Double_t> cls;
      TUMisc::String2List(par_entry, ":", par_n);

      // Check format
      if (par_n.size() != 3) {
         string message = "Ill-formatted entry = " + par_entry
                          + ", should be \"X:N:{CL1,CL2...}\" with X=\'ALL\' or parname1,parname2, N the number of contour points, and CL the confidence levels";
         TUMessages::Error(fOutputLog, "TURunPropagation", "ExtractIndicesContours", message);
      } else {
         // Check format for 1st argument
         TUMisc::String2List(par_n[0], ",", pars);
         if (pars[0] != "ALL" && pars.size() != 2) {
            string message = "Ill-formatted entry = " + par_entry
                             + ", should be \"X:N:{CL1,CL2...}\" with X=\'ALL\' or parname1,parname2";
            TUMessages::Error(fOutputLog, "TURunPropagation", "ExtractIndicesContours", message);
         }
         // Check format for 3rd argument
         TUMisc::String2List(par_n[2], "{,}", cls);
         if (cls.size() == 0) {
            string message = "Ill-formatted entry = " + par_entry
                             + ", should be \"X:N:{CL1,CL2...}\" with at least one CL";
            TUMessages::Error(fOutputLog, "TURunPropagation", "ExtractIndicesContours", message);
         }
      }

      // Fill contour structure
      Contour_t contour;
      contour.n_pts = atoi(par_n[1].c_str());
      if (contour.n_pts < 4)
         TUMessages::Error(fOutputLog, "TURunPropagation", "ExtractIndicesContours", "Need at least 4 points for contour");

      contour.cls = cls;
      if (par_n[0] == "ALL") {
         // If keyword 'ALL' is used, add all possible pairs of FIT parameters in contour
         // Clear to avoid double-counting
         ClearContours();
         vector<Int_t> indices_fit = fFitPars->IndicesPars(kFIT);
         for (Int_t i = 0; i < (Int_t)indices_fit.size(); ++i) {
            for (Int_t j = i + 1; j < (Int_t)indices_fit.size(); ++j) {
               contour.i_par1 = indices_fit[i];
               contour.i_par2 = indices_fit[j];
               contour.mg = NULL;
               contour.leg = NULL;
               fContours.push_back(contour);
            }
         }
         // Break to avoid double-counting
         break;
      } else {
         // Check par name is a valid parameter
         Int_t i_fitpar1 = fFitPars->IndexPar(pars[0]);
         Int_t i_fitpar2 = fFitPars->IndexPar(pars[1]);
         if (i_fitpar1 < 0 || fFitPars->GetParEntry(i_fitpar1)->GetFitType() != kFIT) {
            string message = "In " + par_entry + ", first parameter " + pars[0] + " not found as \'FIT\' parameter => skipped";
            TUMessages::Error(fOutputLog, "TURunPropagation", "ExtractIndicesContours", message);
         } else if (i_fitpar2 < 0 || fFitPars->GetParEntry(i_fitpar1)->GetFitType() != kFIT) {
            string message = "In " + par_entry + ", second parameter " + pars[1] + " not found as \'FIT\' parameter => skipped";
            TUMessages::Error(fOutputLog, "TURunPropagation", "ExtractIndicesContours", message);
         } else {
            // Check not previously added
            Bool_t is_inlist = false;
            for (Int_t kk = 0; kk < GetNContours(); ++kk) {
               if (fContours[kk].i_par1 == i_fitpar1 && fContours[kk].i_par2 == i_fitpar2) {
                  is_inlist = true;
                  break;
               }
            }
            if (is_inlist) {
               string message = "Contour " + par_entry + " already loaded once => skipped";
               TUMessages::Warning(fOutputLog, "TURunPropagation", "ExtractIndicesContours", message);
            } else {
               contour.i_par1 = i_fitpar1;
               contour.i_par2 = i_fitpar2;
               contour.mg = NULL;
               contour.leg = NULL;
               fContours.push_back(contour);
            }
         }
      }
   }
}


//______________________________________________________________________________
void TURunPropagation::ExtractIndicesScansOrProfiles(TUInitParList *init_pars, Int_t i_initpar, Bool_t is_scan_or_prof)
{
   //--- Extracts from index of initialisation parameters the indices in fScans (or fProfiles).
   //  init_pars         Initialisation parameters
   //  i_initpars        Index of parameter in init_par
   //  is_scan_or_prof   Extract indices for fScans(true) or fProfiles (false)

   if (i_initpar < 0)
      return;

   string word = (is_scan_or_prof ? "scan" : "profile");

   // If free parameters, loop on parameter names
   for (Int_t m = 0; m < init_pars->GetParEntry(i_initpar).GetNVals(); ++m) {
      string par_entry = init_pars->GetParEntry(i_initpar).GetVal(m);
      // Extract
      vector<string> par_n;
      TUMisc::String2List(par_entry, ":", par_n);
      if (par_n.size() != 2) {
         string message = "Ill-formatted entry = " + par_entry
                          + ", should be \"X:N\" with X=\'ALL\' or parameter name, and N the number of points for the " + word;
         TUMessages::Error(fOutputLog, "TURunPropagation", "ExtractIndicesScansOrProfiles", message);
      }
      Scan_t scan;
      scan.n_pts = atoi(par_n[1].c_str());
      if (scan.n_pts < 4) {
         string message = "Need at least 4 points for " + word;
         TUMessages::Error(fOutputLog, "TURunPropagation", "ExtractIndicesScansOrProfiles", message);
      }

      if (par_n[0] == "ALL") {
         // If keyword 'ALL' is used, add all FIT parameters in scan
         // Clear to avoid double-counting
         ClearScansOrProfiles(is_scan_or_prof);
         for (Int_t i = 0; i < fFitPars->GetNPars(); ++i) {
            if (fFitPars->GetParEntry(i)->GetFitType() == kFIT || fFitPars->GetParEntry(i)->GetFitType() == kNUISANCE) {
               scan.i_par = i;
               scan.gr = NULL;
               if (is_scan_or_prof)
                  fScans.push_back(scan);
               else
                  fProfiles.push_back(scan);
            }
         }
         // Break to avoid double-counting
         break;
      } else {
         // Check par name is a valid parameter
         Int_t i_fitpar = fFitPars->IndexPar(par_n[0]);
         if (i_fitpar < 0 || (fFitPars->GetParEntry(i_fitpar)->GetFitType() != kFIT && fFitPars->GetParEntry(i_fitpar)->GetFitType() != kNUISANCE)) {
            string message = "In " + par_entry + ", parameter " + par_n[0] + " not found as \'FIT\' or \'NUISANCE\' parameter => skipped";
            TUMessages::Error(fOutputLog, "TURunPropagation", "ExtractIndicesScansOrProfiles", message);
         } else {
            Bool_t is_inlist = false;
            if (is_scan_or_prof) {
               for (Int_t kk = 0; kk < GetNScans(); ++kk) {
                  if (fScans[kk].i_par == i_fitpar) {
                     is_inlist = true;
                     break;
                  }
               }
            } else {
               for (Int_t kk = 0; kk < GetNProfiles(); ++kk) {
                  if (fProfiles[kk].i_par == i_fitpar) {
                     is_inlist = true;
                     break;
                  }
               }
            }
            if (is_inlist) {
               string message = word + " " + par_entry + " already loaded once => skipped";
               TUMessages::Warning(fOutputLog, "TURunPropagation", "ExtractIndicesScansOrProfiles", message);
            } else {
               scan.i_par = i_fitpar;
               scan.gr = NULL;
               if (is_scan_or_prof)
                  fScans.push_back(scan);
               else
                  fProfiles.push_back(scan);
            }
         }
      }
   }
}

//______________________________________________________________________________
Bool_t TURunPropagation::FillGraphsContours(Int_t i_cont)
{

   //--- Extract graph of contours for parameter i_contour and add it in fContours.
   //  i_cont            Index of contour in fContours

   // Nothing to do if no minimizer, or bad index
   if (!fMinimizer || i_cont < 0 || i_cont >= GetNContours() || fContours[i_cont].cls.size() == 0)
      return false;

   // ROOT::Minimizer::Contour
   //    bool Contour(unsigned int ivar, unsigned int jvar, unsigned int& npoints, double* xi, double* xj)
   //  => find the contour points (xi, xj) of the function for parameter ivar and jvar around the minimum
   //     The contour is searched for value of the function = Min + ErrorUp()
   //
   // From CLs to ErrDef - set with function fMinimizer->SetErrorDef(err_def)
   //      "Excerpt from Minuit documentation" (hence, does not automatically apply for other minimizers)
   //   Whatever method is used to calculate the parameter errors, they will depend
   //   on the overall (multiplicative) normalization of FCN , in the sense that if
   //   the value of FCN is everywhere multiplied by a constant beta, then the errors
   //   will be decreased by a factor sqrt(beta). [...] The SetErrorDef allows to define
   //   what is meant by one “error”. If FCN is
   //      - chi2 (usual chisquare): set ErrDef to 1. (default value) for 1sigma, to 4. for 2-sigma, etc.
   //      - negative-log-likelihood: set ErrDef to 0.5.


   // For contour, need n_pts+1 (last point is first point)
   UInt_t n_pts = fContours[i_cont].n_pts;
   Double_t *x1 = new Double_t[n_pts + 1];
   Double_t *x2 = new Double_t[n_pts + 1];


   // Create multigraph for this contour
   TUFreeParEntry *par1 = fFitPars->GetParEntry(fContours[i_cont].i_par1);
   TUFreeParEntry *par2 = fFitPars->GetParEntry(fContours[i_cont].i_par2);
   string mg_name = Form("contours_%s_%s_%dpts_CL%s",
                         par1->GetFitName().c_str(), par2->GetFitName().c_str(),
                         n_pts, TUMisc::List2String(fContours[i_cont].cls, '_', 1).c_str());
   if (fContours[i_cont].mg)
      delete fContours[i_cont].mg;
   fContours[i_cont].mg = new TMultiGraph();
   string x_title = par1->GetFitName() + " " + par1->GetUnit(true);
   string y_title = par2->GetFitName() + " " + par2->GetUnit(true);
   string mg_title = ";" + x_title + ";" + y_title;
   fContours[i_cont].mg->SetName(mg_name.c_str());
   fContours[i_cont].mg->SetTitle(mg_title.c_str());

   // Create associated legend
   if (fContours[i_cont].leg)
      delete fContours[i_cont].leg;
   fContours[i_cont].leg = new TLegend(0.6, 0.6, 0.94, 0.95);

   // Add best fit
   TGraph *gr_best = new TGraph(1);
   gr_best->SetPoint(0, par1->GetFitValBest(), par2->GetFitValBest());
   gr_best->SetName(Form("gr_bestfit_%s_%s", par1->GetFitName().c_str(), par2->GetFitName().c_str()));
   gr_best->SetTitle("");
   gr_best->GetXaxis()->SetTitle(x_title.c_str());
   gr_best->GetYaxis()->SetTitle(y_title.c_str());
   gr_best->GetXaxis()->CenterTitle();
   gr_best->GetYaxis()->CenterTitle();
   gr_best->SetMarkerColor(kBlue);
   gr_best->SetMarkerStyle(kStar);
   gr_best->SetMarkerSize(2);
   fContours[i_cont].mg->Add(gr_best, "P");
   TLegendEntry *entry = fContours[i_cont].leg->AddEntry(gr_best, "Best fit", "P");
   entry->SetTextColor(kBlue);
   entry = NULL;

   // Contours require extra call to minimizer, which changes the best-fit value
   // and/or value in fFitPars. As the latter values are later used to draw plots,
   // we keep a reference and reset to the desired value afterwards.
   TUFreeParList fitpars_ref(*fFitPars);


   // Loop on CLs for contours
   Bool_t is_one_ok = false;
   for (Int_t i = 0; i < (Int_t)fContours[i_cont].cls.size(); ++i) {
      // Update variable to force minimizer run
      //fMinimizer->SetVariableValue(fContours[i_cont].i_par1, par1->GetFitInit());
      //fMinimizer->SetVariableValue(fContours[i_cont].i_par2, par2->GetFitInit());
      // Update CL value to use
      fMinimizer->SetErrorDef(fContours[i_cont].cls[i]*fContours[i_cont].cls[i]);
      // Extract contour (fail quite often)
      Bool_t ok = fMinimizer->Contour(fContours[i_cont].i_par1, fContours[i_cont].i_par2, n_pts, x1, x2);
      if (!ok) {
         fFitPars->ResetToReference(&fitpars_ref, stdout);
         continue;
      } else
         is_one_ok = true;

      // Add first point at end to get closed polyline
      x1[n_pts] = x1[0];
      x2[n_pts] = x2[0];
      // Fill graph
      TGraph *gr = new TGraph(n_pts + 1, x1, x2);
      gr->Print();
      string gr_name = Form("contour_%s_%s_%dpts_%.1fsigma", par1->GetFitName().c_str(),
                            par2->GetFitName().c_str(), n_pts, fContours[i_cont].cls[i]);
      TUMisc::RemoveSpecialChars(gr_name);
      gr->SetName(gr_name.c_str());
      gr->SetTitle("");
      gr->GetXaxis()->SetTitle(x_title.c_str());
      gr->GetYaxis()->SetTitle(y_title.c_str());
      gr->GetXaxis()->CenterTitle();
      gr->GetYaxis()->CenterTitle();
      gr->SetLineColor(TUMisc::RootColor(i));
      gr->SetLineStyle(i + 1);
      gr->SetLineWidth(2);
      fContours[i_cont].mg->Add(gr, "L");
      entry = fContours[i_cont].leg->AddEntry(gr, Form("%.1f #sigma", fContours[i_cont].cls[i]), "L");
      entry->SetTextColor(TUMisc::RootColor(i));
      entry = NULL;

      fFitPars->ResetToReference(&fitpars_ref, stdout);
   }
   // Following 2 lines commented, because crash on some systems...
   //fContours[i_cont].mg->GetXaxis()->CenterTitle();
   //fContours[i_cont].mg->GetYaxis()->CenterTitle();


   // Free memory
   par1 = NULL;
   par2 = NULL;
   delete[] x1;
   x1 = NULL;
   delete [] x2;
   x2 = NULL;

   // If no contour found, delete!
   if (!is_one_ok) {
      delete fContours[i_cont].mg;
      fContours[i_cont].mg = NULL;
      return false;
   } else
      return true;
}


//______________________________________________________________________________
void TURunPropagation::FillGraphsScansOrProfiles(Int_t i_scan, Bool_t is_scan_or_prof)
{
   //--- Extract graph of scan around minimum for parameter i_scan (and add in fScans),
   //    or extract graph of profile likelihood for parameter i_scan (and add it fProfiles).
   //  i_scan            Index of scan in fScans (of fProfiles)
   //  is_scan_or_prof   Act on fScans(true) or fProfiles (false)

   // Nothing to do if no minimizer, or bad index
   if (!fMinimizer || i_scan < 0 || i_scan >= GetNScansOrProfiles(is_scan_or_prof))
      return;

   TUFreeParEntry *par = NULL;
   string name = "";
   UInt_t n_pts = 0;
   vector<Double_t> x, y;


   // Scans and profile likelihood require extra call to minimizer, which
   // change the best-fit value and/or value in fFitPars. As the latter
   // values are later used to draw plots, we keep a reference and reset
   // to the desired value afterwards.
   TUFreeParList fitpars_ref(*fFitPars);

   // Extract x,y for scan or profile
   if (is_scan_or_prof) {
      // SCAN: starting from best-fit (for all parameters), calculate chi2(param) [no minimization]
      n_pts = fScans[i_scan].n_pts;
      par = fFitPars->GetParEntry(fScans[i_scan].i_par);
      name = Form("scan_%s_%dpts", par->GetFitName().c_str(), n_pts);
      x.assign(n_pts, 0);
      y.assign(n_pts, 0);

      // ROOT::Minimizer::Scan
      //    bool Scan(unsigned int ivar, unsigned int& nstep, double* x, double* y, double xmin = 0, double xmax = 0)
      //  => scan function minimum for variable ivar with nstep. Return false if an error or if minimizer does not support this functionality
      fMinimizer->Scan(fScans[i_scan].i_par, n_pts, &x[0], &y[0]);
      fFitPars->ResetToReference(&fitpars_ref, stdout);
   } else {
      // PROFILE (likelihood): fix parameter to value and get new best-chi2 [minimization for each x]
      n_pts = fProfiles[i_scan].n_pts;
      par = fFitPars->GetParEntry(fProfiles[i_scan].i_par);
      name = Form("profiles_%s_%dpts", par->GetFitName().c_str(), n_pts);

      // Loop on x-values taken by the parameter:
      //   => use errors (or asymm errors) to reach 3 sigma each side
      Double_t x_min = 0., x_max = 0.;
      if (fIsMINOS) {
         x_min = par->GetFitValBest() - 3.*fabs(par->GetFitValErrAsymmLo());
         x_max = par->GetFitValBest() + 3.*fabs(par->GetFitValErrAsymmUp());
      } else {
         x_min = par->GetFitValBest() - 3.*fabs(par->GetFitValErr());
         x_max = par->GetFitValBest() + 3.*fabs(par->GetFitValErr());
      }
      TUAxis x_val(x_min, x_max, n_pts, "dummy", "dummy", kLIN);
      x.assign(n_pts, 0);
      y.assign(n_pts, 0);

      // Fix parameter
      par->SetFitType(kFIXED);
      fMinimizer->FixVariable(fProfiles[i_scan].i_par);
      TUFreeParList fitpars_start(fitpars_ref);
      fitpars_start.GetParEntry(fProfiles[i_scan].i_par)->SetFitType(kFIXED);

      // To optimise minimisation, start from point closest to best-fit,
      // then go left till minimum value, then right up to maximum value,
      // starting from the best-fit obtained from the previous iteration
      //   - find value closest to best-fit
      Double_t x_best = fitpars_ref.GetParEntry(fProfiles[i_scan].i_par)->GetFitValBest();
      Int_t i_closest = x_val.IndexClosest(x_best);
      if (i_closest < 0)
         i_closest = 0;
      else if (i_closest >= x_val.GetN())
         i_closest = x_val.GetN() - 1;
      //  **** EXPLORE 'left' ****
      for (Int_t i = i_closest; i >= 0; --i) {
         // Go to next value
         x[i] = x_val.GetVal(i);
         PrintLog(Form("\n      - minimize for %s = %le (%d/%d)", par->GetFitName().c_str(), x[i], i + 1, n_pts), true);
         // Re-initialize val, fitinit, and minimizer (must be valid whether sampling is kLIN or kLOG)
         par->SetVal(x[i], par->GetFitSampling());
         // Copy init.val only for fit parameters
         par->CopyValInFitInit();
         fMinimizer->SetVariableValue(fProfiles[i_scan].i_par, par->GetFitInit());
         // Minimize for this new setup
         fMinimizer->Minimize();
         // Store new best-chi2 for this value
         y[i] = fMinimizer->MinValue();
      }

      //  - explore 'right' (reset to best-fit 'start')
      fFitPars->ResetToReference(&fitpars_start, stdout);
      for (Int_t i = i_closest + 1; i < x_val.GetN(); ++i) {
         // Go to next value
         x[i] = x_val.GetVal(i);
         PrintLog(Form("\n      - minimize for %s = %le (%d/%d)", par->GetFitName().c_str(), x[i], i + 1, n_pts), true);
         // Re-initialize val, fitinit, and minimizer (must be valid whether sampling is kLIN or kLOG)
         par->SetVal(x[i], par->GetFitSampling());
         // Copy init.val only for fit parameters
         par->CopyValInFitInit();
         fMinimizer->SetVariableValue(fProfiles[i_scan].i_par, par->GetFitInit());
         // Minimize for this new setup
         fMinimizer->Minimize();
         // Store new best-chi2 for this value
         y[i] = fMinimizer->MinValue();
      }

      // Release parameter
      par->SetFitType(kFIT);
      fMinimizer->ReleaseVariable(fProfiles[i_scan].i_par);
      fFitPars->ResetToReference(&fitpars_ref, stdout);
   }

   // Calculate chi2/dof from chi2
   Int_t ndof = GetNdof();
   if (!is_scan_or_prof)
      ndof += 1; // For profile likelihood, fit on one less parameter

   // Create graph
   TGraph *gr = new TGraph(n_pts, &x[0], &y[0]);

   // Set graph name
   TUMisc::RemoveSpecialChars(name);
   gr->SetName(name.c_str());
   gr->SetTitle("");
   gr->GetXaxis()->SetTitle((par->GetFitName() + " " + par->GetUnit(true)).c_str());
   gr->GetYaxis()->SetTitle(Form("#chi^{2}  [dof=%d]", ndof));
   gr->GetXaxis()->CenterTitle();
   gr->GetYaxis()->CenterTitle();

   if (is_scan_or_prof) {
      if (fScans[i_scan].gr)
         delete fScans[i_scan].gr;
      fScans[i_scan].gr = gr;
   } else {
      if (fProfiles[i_scan].gr)
         delete fProfiles[i_scan].gr;
      fProfiles[i_scan].gr = gr;
   }
   gr = NULL;
   par = NULL;
}

//______________________________________________________________________________
void TURunPropagation::FillSpectra(TURunOutputs &results, string const &combos_etypes_phiff, Double_t e_power, TUCoordTXYZ *coord_txyz, Bool_t is_print, FILE *f_print)
{
   //--- Calculates local (Solar system) fluxes for any quantity/modul level and saves in results.
   // INPUTS
   //  combos_etypes_phiff   List of CR fluxes/ratios and associated E types/phi_FF for display (e.g., B/C,O:kEKN:0.73,0.5;C,N,O:kR:0.38)
   //  e_power               Fluxes are multiplied by E^e_power, where E is e_type
   //  coord_txyz            Coordinates at which to calculate spectra (local spectra if NULL)
   //  is_print              Whether to print or not content of gr/h/multigr
   //  f_print               File in which to print gr/h/multigr
   // OUTPUT
   //  results               Structure containing all results


   FILE *fout_ref = fOutputLog;
   if (is_print)
      fOutputLog = f_print;

   // Check if something to calculate
   if (combos_etypes_phiff == "")
      TUMessages::Error(stdout, "TURunPropagation", "FillSpectra", "Cannot proceed, no combos_etypes_phiff to extract!");

   if (GetNSolModModels() > 1)
      TUMessages::Error(stdout, "TURunPropagation", "FillSpectra", "Multiple Solar modulation models not implemented yet");


   // Calculate
   Propagate(fIsUseNormList, !is_print);
   //fPropagModels[0]->PrintSolarSystemISFluxes(fOutputLog);

   // Get IS fluxes at location coord_txyz (for all models)
   //   => Local spectra if coord_txyz = NULL
   Int_t n_models = GetNPropagModels();
   TUValsTXYZECr **flux_tot = new TUValsTXYZECr*[n_models];
   TUValsTXYZECr **flux_sec = new TUValsTXYZECr*[n_models];
   TUValsTXYZECr **flux_prim = new TUValsTXYZECr*[n_models];
   for (Int_t n = 0; n < n_models; ++n) {
      // Retrieve generic TUValsTXYZECr (from which any combo/TOA flux can be calculated)
      if (coord_txyz) {
         flux_tot[n] = fPropagModels[n]->OrphanFluxes(coord_txyz, /*tot0_prim1_sec2*/0);
         flux_prim[n] = fPropagModels[n]->OrphanFluxes(coord_txyz, /*tot0_prim1_sec2*/1);
         flux_sec[n] = fPropagModels[n]->OrphanFluxes(coord_txyz, /*tot0_prim1_sec2*/2);
      } else { // if no coordinate, assumes Earth location
         flux_tot[n] = fPropagModels[n]->GetSolarSystemFluxesTot();
         flux_prim[n] = fPropagModels[n]->GetSolarSystemFluxesPrim();
         flux_sec[n] = fPropagModels[n]->GetSolarSystemFluxesSec();
      }
   }


   // FILL RESULTS!
   // Extract model names
   vector<string> models;
   for (Int_t n = 0; n < n_models; ++n)
      models.push_back(fPropagModels[n]->GetModelName());
   // Set queried quantities and resize containers TURunOutputs
   results.Initialise();
   results.SetCombosAndResize(models, (TUAxesCrE *)fPropagModels[0], combos_etypes_phiff, e_power, true, fOutputLog);

   // If fit data, need to extract list of exp.names or modulation nuisance parameters,
   if (fFitData && !fIsCLMode)
      ExtractFromFitData(/*i_solmod*/0, results, is_print, f_print);

   // Loop on list of combos, and add new graph for each combo (for IS fluxes):
   for (Int_t i = 0; i < results.GetNCombos(); ++i) {
      // Extract common E grid for combo
      TUAxis *e_grid =  fPropagModels[0]->OrphanGetECombo(results.GetCombo(i), results.GetEType(i), fOutputLog);

      // Loop on all propagation models (calculate prim & sec only for anti-nuclei)
      for (Int_t n = 0; n < results.GetNModels(); ++n) {
         string combo = results.GetCombo(i);
         gENUM_ETYPE etype = results.GetEType(i);
         TUMisc::UpperCase(combo);

         // Loop on all modulation levels for current combo
         for (Int_t j = 0; j < results.GetNphiFF(i); ++j) {
            // Calculate model
            fSolModModels[0]->SetParVal(0, results.GetphiFF(i, j));
            Double_t *y_prim = flux_prim[n]->OrphanVals(combo, e_grid, etype, e_power, coord_txyz, fSolModModels[0]);
            Double_t *y_sec = flux_sec[n]->OrphanVals(combo, e_grid, etype, e_power, coord_txyz, fSolModModels[0]);
            Double_t *y_tot = flux_tot[n]->OrphanVals(combo, e_grid, etype, e_power, coord_txyz, fSolModModels[0]);
            for (Int_t k = 0; k < e_grid->GetN(); ++k) {
               results.SetTOAComboPrim(i, n, j, k, y_prim[k]);
               results.SetTOAComboSec(i, n, j, k, y_sec[k]);
               results.SetTOAComboTot(i, n, j, k, y_tot[k]);
            }
            delete[] y_prim;
            y_prim = NULL;
            delete[] y_sec;
            y_sec = NULL;
            delete[] y_tot;
            y_tot = NULL;

            // If combo a ratio, also calculates ratios with isotopes in numerator (total only)
            for (Int_t r = 0; r < results.GetNCombosRatios(i); ++r) {
               Double_t *y = flux_tot[n]->OrphanVals(results.GetComboRatio(i, r), e_grid, etype, e_power, coord_txyz, fSolModModels[0]);
               for (Int_t k = 0; k < e_grid->GetN(); ++k)
                  results.SetTOAComboRatio(i, r, n, j, k, y[k]);
               delete[] y;
               y = NULL;
            }


            // Save in ASCII file
            if (fIsSaveInFiles) {
               //string uid = UID(list_combos[i], list_etypes[i], e_power, fPropagModels[n], fSolModModels[0]) + "_" + leg_title;
               string uid = UID(combo, etype, e_power, results.GetModel(n), fSolModModels[0]->UID());
               TUMisc:: RemoveSpecialChars(uid);
               //// Specific case when nuisance parameter in order not to have
               //// experiment name appearing in the 'model' calculated flux!
               //string solmod_id = fSolModModels[0]->UID();
               //if (fFitData && fFitData->ParsData_GetN() >= 0)
               //   solmod_id = fSolModModels[0]->GetModelName() + "_"
               //               + fSolModModels[0]->GetFreePars()->GetParEntry(0)->ParNameValUnit(0, fSolModModels[0]->ParsModExps_GetBase()->GetParEntry(0)->GetName());
               string file_save = fOutputDir + "/local_fluxes_" + uid;
               if (GetIndexSample() < 0)
                  file_save += ".out";
               else
                  file_save += (string)Form("_%0*d", TUMisc::Number2Digits(GetNSamples()), GetIndexSample() + 1) + ".out";
               FILE *fp_save = fopen(file_save.c_str(), "w");
               TUMisc::PrintUSINEHeader(fp_save, !is_print);
               fprintf(fp_save, "# %s\n", fPropagModels[n]->GetModelName().c_str());
               flux_tot[n]->PrintVals(fp_save, combo, e_grid, etype, e_power, coord_txyz,  fSolModModels[0]);
               fclose(fp_save);
               cout << "  => Saved in " << file_save << endl;
            } else
               flux_tot[n]->PrintVals(fOutputLog, combo, e_grid, etype, e_power, coord_txyz,  fSolModModels[0]);
         }
      }
      delete e_grid;
      e_grid = NULL;
   }

   /////////////////////////////////////////////////////////////////////
   // 1. Primary/secondary fraction w.r.t. propag. (local IS fluxes)  //
   // 2. Isotopic fraction w.r.t. elemental contribution              //
   /////////////////////////////////////////////////////////////////////
   for (Int_t n = 0; n < results.GetNModels(); ++n) {
      TUCoordTXYZ *earth = fPropagModels[n]->GetSolarSystemCoords();
      // Loop on all CRs and Z (from all combos)
      for (Int_t i = 0; i < results.GetNIsotAndZ(); ++i) {
         vector<Double_t> frac;
         if (fPropagModels[0]->IsElementAndInList(results.GetIsotAndZ(i))) {
            Int_t z_cr = fPropagModels[0]->ElementNameToZ(results.GetIsotAndZ(i));
            fPropagModels[n]->FillFractionElement(frac, z_cr, earth, 1);
         } else {
            Int_t j_cr = fPropagModels[0]->Index(results.GetIsotAndZ(i));
            fPropagModels[n]->FillFraction(frac, j_cr, earth, 1);
         }
         // Set result
         for (Int_t k = 0; k < results.GetNE(); ++k)
            results.SetIsotAndZPrimFrac(i, n, k, frac[k]);
      }
      earth = NULL;
   }

   // 2. Isotopic fraction w.r.t. elemental contribution
   for (Int_t n = 0; n < fNPropagModels; ++n) {
      TUCoordTXYZ *earth = fPropagModels[n]->GetSolarSystemCoords();
      // Loop on all CRs (from all combos)
      for (Int_t i = 0; i < results.GetNIsot(); ++i) {
         vector<Double_t> frac;
         Int_t j_cr = fPropagModels[0]->Index(results.GetIsot(i));
         fPropagModels[n]->FillFraction(frac, j_cr, earth, 0);

         // Set result
         for (Int_t k = 0; k < results.GetNE(); ++k)
            results.SetIsotFrac(i, n, k, frac[k]);
      }
      earth = NULL;
   }

   // Fill model parameters
   fPropagModels[0]->FillModelParameters(results);

   // Reset fOutputLog
   if (is_print) {
      fOutputLog = fout_ref;
      fout_ref = NULL;
   }

   // Reset to null pointers to IS fluxes
   for (Int_t n = 0; n < n_models; ++n) {
      flux_tot[n] = NULL;
      flux_prim[n] = NULL;
      flux_sec[n] = NULL;
   }
   delete[] flux_tot;
   flux_tot  = NULL;
   delete[] flux_prim;
   flux_prim = NULL;
   delete[] flux_sec;
   flux_sec  = NULL;
}

//______________________________________________________________________________
Int_t TURunPropagation::GetNdofForXS() const
{
   //--- Calculates number of degrees of freedom for XS nuisance parameters.
   //    If NSS nuisance parameters (norm, shift, slope), each degree of
   //    freedom is associated to constraint, so ndof = 0. For LC case,
   //    we only have one constraint, so that the number of dof is
   //       n_lc_params - n_constraints

   Int_t n_lc = 0;
   // Count number of LC parameters
   for (Int_t l = 0; l < fFitPars->GetNPars(); ++l) {
      if (fFitPars->GetParEntry(l)->GetFitType() != kNUISANCE
            && fFitPars->GetParEntry(l)->GetFitType() != kFIT)
         continue;

      if (fFitPars->GetParEntry(l)->GetFitName().find("LCInel") != string::npos
            || fFitPars->GetParEntry(l)->GetFitName().find("LCProd") != string::npos)
         n_lc += 1;
   }
   // Subtract a constraint per inelastic XS
   n_lc -= fPropagModels[0]->TUXSections::ParsXS_LinComb_GetNReac(true);
   // Subtract a constraint per production XS
   n_lc -= fPropagModels[0]->TUXSections::ParsXS_LinComb_GetNReac(false);

   return n_lc;
}

//______________________________________________________________________________
void TURunPropagation::Initialise(Bool_t is_delete)
{
   //--- Initialises class members.
   //  is_delete         Whether to delete or only initialise

   if (is_delete) {
      if (fDisplayedData) delete fDisplayedData;
      if (fFitCovMatrix) delete fFitCovMatrix;
      if (fFitData) delete fFitData;
      if (fFitPars) delete fFitPars;
      if (fHessianMatrix) delete fHessianMatrix;
      if (fMinimizer) delete fMinimizer;
      if (fPropagModels) {
         for (Int_t i = 0; i < fNPropagModels; ++i) {
            if (fPropagModels[i]) delete fPropagModels[i];
            fPropagModels[i] = NULL;
         }
         delete[] fPropagModels;
      }
      if (fSolModModels) {
         for (Int_t i = 0; i < fNSolModModels; ++i) {
            if (fSolModModels[i]) delete fSolModModels[i];
            fSolModModels[i] = NULL;
         }
         delete[] fSolModModels;
      }
      ClearContours();
      ClearScansOrProfiles(true);
      ClearScansOrProfiles(false);
   }

   fChi2Min = 0.;
   fFitCovMatrix = NULL;
   fDisplayedData = NULL;
   fDisplayedErrType = kERRTOT;
   fDiplayedFluxEPower = 0.;
   fFitData = NULL;
   fFitDataErrType = kERRTOT;
   fFitERange = "";
   fFitPars = NULL;
   fHessianMatrix = NULL;
   fIndexSample = -1;
   fInitFileName = "";
   fIsCLMode = false;
   fIsMINOS = false;
   fIsPrintBestFit = true;
   fIsPrintCovMatrix = true;
   fIsPrintHessianMatrix = false;
   fIsUseNormList = true;
   fIsUseBinRange = false;
   fMinimizer = NULL;
   fMinimizerAlgo = "";
   fMinimizerName = "";
   fMinimizerNMaxCall = 0;
   fMinimizerStrategy = 1;
   fMinuitPrecision = 1.;
   fMinuitPrintLevel = 1;
   fMinuitTolerance = 1.;
   fNExtraInBinRange = 0;
   fNPropagModels = 0;
   fNSamples = 0;
   fNSolModModels = 0;
   fPropagModels = NULL;
   fSolModModels = NULL;
}

//______________________________________________________________________________
void TURunPropagation::LoopDisplaySpectra(TApplication *app, Bool_t is_sep_isot, TUCoordTXYZ *coord_txyz, Bool_t is_print, FILE *f_print)
{
   //--- Loops to calculate/display spectra for any combo/modul at coord_txyz (if NULL, local spectra).
   //  app                   Root application
   //  is_sep_isot           If true, displays/prints separate contribution of isotopes (for elements)
   //  coord_txyz            Coordinates at which to calculate spectra (local spectra if NULL)
   //  is_print              Whether to print or not content of gr/h/multigr
   //  f_print               File in which to print gr/h/multigr


   string const &combos_etypes_phiff = "";
   Double_t e_power = 0.;

   FILE *fout_ref = fOutputLog;
   if (is_print)
      fOutputLog = f_print;

   Bool_t is_tui = true;

   // Print infos
   if (coord_txyz)
      TUMessages::Separator(fOutputLog, "Local IS and TOA spectra");
   else
      TUMessages::Separator(fOutputLog, Form("IS spectra at %s", coord_txyz->FormVals().c_str()));
   string indent = TUMessages::Indent(true);
   PrintListOfModels(fOutputLog);
   fprintf(fOutputLog, "%s  [only force-field modulation is available for the moment]\n", indent.c_str());
   TUMessages::Indent(false);


   // Loop on quantities/modulation levels
   do {


      string combos_etypes_phiff = "";
      Double_t e_power = fPropagModels[0]->TUI_SelectCombosEAxisphiFF(combos_etypes_phiff, true);

      // Test if quit
      if (combos_etypes_phiff == "")
         break;

      // Do calculation/plot
      TURunOutputs results;
      FillSpectra(results, combos_etypes_phiff, e_power, coord_txyz, is_print, f_print);
      PlotSpectra(app, results, is_sep_isot, is_print, f_print);
   } while (1);

   // Reset fOutputLog
   if (is_print) {
      fOutputLog = fout_ref;
      fout_ref = NULL;
   }
}

//______________________________________________________________________________
void TURunPropagation::MinimiseTOAFluxes(TUInitParList *init_pars, Bool_t is_verbose, Bool_t is_logandstdout)
{
   //--- Creates a minimiser to find the best parameters to match TOA fluxes (parameters are
   //    free parameters of the propagation. Returns the best chi2 value found (store best
   //    fitted parameters and errors in fFitPars and fFitParsSolMod.
   //  init_pars         USINE initialisation parameters
   //  is_verbose        Verbose on or off
   //  is_logandstdout   Whether chatter printed 'on screen and in fOutputLog'
   //
   //    N.B.: the minimiser is defined by its name, its algorithm (optional),
   //          and several other parameters loaded from the par file. Possible
   //          combinations for {minimiser, algorithm} are:
   //          Name               Name algorithm
   //      Minuit /Minuit2        Migrad, Simplex,Combined,Scan  (default is Migrad)
   //      Minuit2                Fumili2
   //      Fumili                    -
   //      GSLMultiMin            ConjugateFR, ConjugatePR, BFGS,
   //                             BFGS2, SteepestDescent
   //      GSLMultiFit               -
   //      GSLSimAn                  -
   //      Genetic                   -

   string indent = TUMessages::Indent(true);
   if (is_verbose)
      PrintLog(Form("%s[TURunPropagation::MinimiseTOAFluxes]", indent.c_str()), is_logandstdout);

   ////////////////////////////
   ////// READ MINIMISER //////
   ////////////////////////////
   //  MINIMISER (fMinimizerAlgo, fMinimizerName, fNMaxCall...)
   const Int_t gg = 3;
   string gr_sub_name[gg] = {"UsineFit", "Config", ""};
   gr_sub_name[2] = "Minimiser";
   if (is_verbose)
      PrintLog(Form("%s### Set ROOT minimiser properties", indent.c_str()), is_logandstdout);
   fMinimizerName = init_pars->GetParEntry(init_pars->IndexPar(gr_sub_name)).GetVal();
   gr_sub_name[2] = "Algorithm";
   string tmp_algo = init_pars->GetParEntry(init_pars->IndexPar(gr_sub_name)).GetVal();
   fMinimizerAlgo = "";
   if (tmp_algo != "-") fMinimizerAlgo = tmp_algo;
   gr_sub_name[2] = "NMaxCall";
   fMinimizerNMaxCall = atoi(init_pars->GetParEntry(init_pars->IndexPar(gr_sub_name)).GetVal().c_str());
   gr_sub_name[2] = "Strategy";
   fMinimizerStrategy = atoi(init_pars->GetParEntry(init_pars->IndexPar(gr_sub_name)).GetVal().c_str());
   gr_sub_name[2] = "Tolerance";
   fMinuitTolerance = atof(init_pars->GetParEntry(init_pars->IndexPar(gr_sub_name)).GetVal().c_str());
   gr_sub_name[2] = "Precision";
   fMinuitPrecision = atof(init_pars->GetParEntry(init_pars->IndexPar(gr_sub_name)).GetVal().c_str());
   gr_sub_name[2] = "PrintLevel";
   fMinuitPrintLevel = atoi(init_pars->GetParEntry(init_pars->IndexPar(gr_sub_name)).GetVal().c_str());
   if (is_verbose) {
      PrintLog(Form("%s   - Minimiser=%s, algo=%s  (print level=%d)", indent.c_str(), fMinimizerName.c_str(), fMinimizerAlgo.c_str(), fMinuitPrintLevel), is_logandstdout);
      PrintLog(Form("%s   - NMaxCall=%d, strategy=%d, tolerance=%.3le precision=%.3le", indent.c_str(), fMinimizerNMaxCall, fMinimizerStrategy, fMinuitTolerance, fMinuitPrecision), is_logandstdout);
   }

   // MINOS on/off (check: if Not minuit selected, cannot use MINOS)
   gr_sub_name[2] = "IsMINOS";
   fIsMINOS = TUMisc::String2Bool(init_pars->GetParEntry(init_pars->IndexPar(gr_sub_name)).GetVal().c_str());
   if (fIsMINOS) {
      string tmp_min = fMinimizerName;
      TUMisc::UpperCase(tmp_min);
      if (tmp_min.find("MINUIT") != string::npos)
         PrintLog(Form("%s     -> MINOS is on", indent.c_str()), is_logandstdout);
      else {
         fIsMINOS = false;
         PrintLog(Form("%s     -> to enable MINOS, you need to use Minuit Minimiser (not %s selected here)", indent.c_str(), fMinimizerName.c_str()), is_logandstdout);
      }
   }

   ///////////////////////////
   ////// SET MINIMISER //////
   ///////////////////////////

   // Clear
   if (fMinimizer) {
      delete fMinimizer;
      fMinimizer = NULL;
   }

   fMinimizer = ROOT::Math::Factory::CreateMinimizer(fMinimizerName, fMinimizerAlgo);
   if (is_verbose)
      PrintLog(Form("%s   Create minimiser: %s (algo=%s)",
                    indent.c_str(), fMinimizerName.c_str(), fMinimizerAlgo.c_str()), is_logandstdout);

   // Set-up fit parameters and minimise
   if (fFitPars->GetNPars() == 0) {
      PrintLog(Form("%s  => No free parameters, nothing to do!", indent.c_str()), is_logandstdout);
      if (is_verbose)
         PrintLog(Form("%s[TURunPropagation::MinimiseTOAFluxes] <DONE>\n\n", indent.c_str()), is_logandstdout);
      return;
   }

   if (is_verbose)
      PrintLog(Form("%s   [Chi2 minimisation]", indent.c_str()), is_logandstdout);

   // Create function wrapper for minimiser (IMultiGenFunction type):
   ROOT::Math::Functor f(this, &TURunPropagation::Chi2_TOAFluxes, fFitPars->GetNPars());

   // Set minimiser to wrapper
   fMinimizer->SetFunction(f);

   // Set minimiser parameters (tolerance , etc)
   fMinimizer->SetMaxFunctionCalls(fMinimizerNMaxCall); // for Minuit/Minuit2
   fMinimizer->SetMaxIterations(fMinimizerNMaxCall);  // for GSL
   fMinimizer->SetStrategy(fMinimizerStrategy);
   fMinimizer->SetTolerance(fMinuitTolerance);
   fMinimizer->SetPrecision(fMinuitPrecision);
   fMinimizer->SetPrintLevel(fMinuitPrintLevel);

   // Set fit variables
   for (Int_t i = 0; i < fFitPars->GetNPars(); ++i) {
      TUFreeParEntry *par = fFitPars->GetParEntry(i);
      if (par->GetFitType() == kFIXED)
         // Fixed variable
         fMinimizer->SetFixedVariable(i, par->GetFitName(), par->GetFitInit());
      else {
         // Set (un-)limited variables
         if (par->IsLowerLimitedPar() && par->IsUpperLimitedPar())
            fMinimizer->SetLimitedVariable(i, par->GetFitName(), par->GetFitInit(), par->GetFitInitSigma(), par->GetFitInitMin(), par->GetFitInitMax());
         else if (par->IsLowerLimitedPar())
            fMinimizer->SetLowerLimitedVariable(i, par->GetFitName(), par->GetFitInit(), par->GetFitInitSigma(), par->GetFitInitMin());
         else if (par->IsUpperLimitedPar())
            fMinimizer->SetUpperLimitedVariable(i, par->GetFitName(), par->GetFitInit(), par->GetFitInitSigma(), par->GetFitInitMax());
         else
            fMinimizer->SetVariable(i, par->GetFitName(), par->GetFitInit(), par->GetFitInitSigma());
      }
      par = NULL;
   }

   fFitPars->PrintFitInit(fOutputLog);
   if (is_verbose && fOutputLog != stdout && is_logandstdout)
      fFitPars->PrintFitInit(stdout);

   // Minimisation of fFitPars on fFitData using TURunPropagation::Chi2_TOAFluxes
   clock_t start_time = clock();
   PrintLog(Form("\n >>> START MINIMISER <<<\n"), is_logandstdout);
   fMinimizer->Minimize();
   //PrintLog(Form(" >>> Minimized in %le s", Double_t(clock() - start_time) / (Double_t)CLOCKS_PER_SEC), is_logandstdout);

   // MINOS Errors
   if (fIsMINOS) {
      PrintLog(Form("\n >>> START MINOS <<<\n"), is_logandstdout);
      for (Int_t i = 0; i < fFitPars->GetNPars(); ++i) {
         PrintLog(Form("    - for parameter %s", fFitPars->GetParEntry(i)->GetName().c_str()), is_logandstdout);
         Double_t err_lo, err_up;
         fMinimizer->GetMinosError(i, err_lo, err_up);
         fFitPars->GetParEntry(i)->SetFitValErrAsymm(err_lo, err_up);
      }
   }

   // Copy best-fit parameters and errors
   const Double_t *xi = fMinimizer->X();
   const Double_t *err = fMinimizer->Errors();
   fChi2Min = fMinimizer->MinValue();
   // Check that minimum is minimum!
   //cout << "chi2min=" << fChi2Min << " vs " << Chi2_TOAFluxes(fMinimizer->X()) << endl;

   for (Int_t i = 0; i < fFitPars->GetNPars(); ++i) {
      TUFreeParEntry *par = fFitPars->GetParEntry(i);
      par->SetFitValBest(xi[i]);
      par->SetFitValErr(err[i]);
      par->CopyFitValBestInVal();
      // For display purpose later, we copy phi_best
      // for the corresponding data in fFitData
      Int_t i_exp = -1, i_par = -1;
      fSolModModels[0]->ParsModExps_Indices(par->GetName(), i_exp, i_par);
      if (i_exp >= 0) {
         // Copy for all data for this exp the modulation parameter phi
         for (Int_t e = 0; e < gN_ETYPE; ++e) {
            gENUM_ETYPE e_type = TUEnum::gENUM_ETYPE_LIST[e];
            for (Int_t kk = 0; kk < fFitData->GetNData(i_exp, e_type); ++kk) {
               Int_t i_data = fFitData->IndexData(i_exp, e_type);
               TUDataEntry *crdata = fFitData->GetEntry(i_data);
               crdata->SetExpphi(par->GetVal(false));
               crdata = NULL;
            }
         }
      }
      par = NULL;
   }

   // Print convergence status
   string status_message = "";
   if (fMinimizer->Status() == 0) status_message = "OK";
   else if (fMinimizer->Status() == 1) status_message = "Covariance was made pos defined";
   else if (fMinimizer->Status() == 2) status_message = "Hesse is invalid";
   else if (fMinimizer->Status() == 3) status_message = "Edm is above max";
   else if (fMinimizer->Status() == 4) status_message = "Reached call limit";
   else if (fMinimizer->Status() == 5) status_message = "Any other failure";
   if (fIsMINOS) {
      // In case of Minos failed the status error is updated as following
      // status += 10 * minosStatus where the minos status is...
      Int_t status = fMinimizer->Status() / 10;
      if (status == 0) status_message += " + [MINOS] OK";
      else if (status == 1) status_message += " + [MINOS] maximum number of function calls exceeded when running for lower error";
      else if (status == 2) status_message += " + [MINOS] maximum number of function calls exceeded when running for upper error";
      else if (status == 3) status_message += " + [MINOS] new minimum found when running for lower error";
      else if (status == 4) status_message += " + [MINOS] new minimum found when running for upper error";
      else if (status == 5) status_message += " + [MINOS] any other failure";
   }
   status_message = Form("# Minimization status: %d  ->  %s", fMinimizer->Status(), status_message.c_str());
   PrintLog(status_message, is_logandstdout);
   string covmatrix_message = "";
   if (fMinimizer->CovMatrixStatus() == -1) covmatrix_message = "not available (inversion failed or Hesse failed)";
   else if (fMinimizer->CovMatrixStatus() == 0) covmatrix_message = "available but not positive defined";
   else if (fMinimizer->CovMatrixStatus() == 1) covmatrix_message = "covariance only approximate";
   else if (fMinimizer->CovMatrixStatus() == 2) covmatrix_message = "full matrix but forced pos def";
   else if (fMinimizer->CovMatrixStatus() == 3) covmatrix_message = "full accurate matrix";
   covmatrix_message = Form("# Covariance matrix status: %d  ->  %s", fMinimizer->CovMatrixStatus(), covmatrix_message.c_str());
   PrintLog(covmatrix_message, is_logandstdout);

   PrintLog("\n >>> RESULTS <<<\n", is_logandstdout);

   // Print best-chi2 found (and save in file)
   if (!fIsCLMode) {
      PrintFitResult(stdout, false);
      fFitPars->PrintFitValsAndInit(stdout);
   }
   if (fOutputLog != stdout) {
      PrintFitResult(fOutputLog, false);
      fFitPars->PrintFitValsAndInit(fOutputLog);
   }
   if (fOutputDir != "" && fOutputDir != "usine.tests" && fIsPrintBestFit) {
      string f_chi2 = fOutputDir + "/fit_result.out";
      FILE *fp_chi2 = fopen(f_chi2.c_str(), "w");
      PrintFitResult(fp_chi2, true);
      fFitPars->PrintFitValsAndInit(fp_chi2);
      fprintf(fp_chi2, "\n# CONVERGENCE STATUS\n");
      fprintf(fp_chi2, "%s\n", status_message.c_str());
      fprintf(fp_chi2, "%s\n", covmatrix_message.c_str());
      fclose(fp_chi2);
      cout << "  => Saved in " << f_chi2 << endl << endl;
   }

   // Print covariance matrix of fit parameters (if selected)
   PrintLog("", is_logandstdout);
   if (fIsPrintCovMatrix) {
      vector<Int_t> indices = fFitPars->IndicesPars(kFIT);
      vector<Int_t> tmp = fFitPars->IndicesPars(kNUISANCE);
      indices.insert(indices.end(), tmp.begin(), tmp.end());
      if (fFitCovMatrix) delete fFitCovMatrix;
      fFitCovMatrix = new Double_t[fFitPars->GetNPars()*fFitPars->GetNPars()];
      PrintLog("# Covariance matrix for fit parameters", is_logandstdout);
      if (fMinimizer->GetCovMatrix(fFitCovMatrix)) {
         if (fOutputLog != stdout)
            PrintFitCovMatrix(fOutputLog, indices, fFitCovMatrix, true);
         if (fOutputDir != "" && fOutputDir != "usine.tests") {
            string f_cov = fOutputDir + "/fit_covmatrix.out";
            FILE *fp_cov = fopen(f_cov.c_str(), "w");
            TUMisc::PrintUSINEHeader(fp_cov, true);
            PrintFitCovMatrix(fp_cov, indices, fFitCovMatrix, true);
            fclose(fp_cov);
            cout << "  => Saved in " << f_cov << endl << endl;;
         }
      } else
         PrintLog("  => Failed", is_logandstdout);
   }


   // Print Hessian matrix of fit parameters (if selected)
   if (fIsPrintHessianMatrix) {
      vector<Int_t> indices = fFitPars->IndicesPars(kFIT);
      vector<Int_t> tmp = fFitPars->IndicesPars(kNUISANCE);
      indices.insert(indices.end(), tmp.begin(), tmp.end());
      if (fHessianMatrix) delete fHessianMatrix;
      fHessianMatrix = new Double_t[fFitPars->GetNPars()*fFitPars->GetNPars()];
      PrintLog("# Hessian matrix for fit parameters", is_logandstdout);
      if (fMinimizer->GetHessianMatrix(fHessianMatrix)) {
         if (fOutputLog != stdout)
            PrintFitCovMatrix(fOutputLog, indices, fHessianMatrix, true);
         if (fOutputDir != "" && fOutputDir != "usine.tests") {
            string f_hessian = fOutputDir + "/fit_hessianmatrix.out";
            FILE *fp_hessian = fopen(f_hessian.c_str(), "w");
            TUMisc::PrintUSINEHeader(fp_hessian, true);
            PrintFitCovMatrix(fp_hessian, indices, fHessianMatrix, true);
            fclose(fp_hessian);
            cout << "  => Saved in " << f_hessian << endl << endl;
         }
      } else
         PrintLog("  => Failed", is_logandstdout);
   }


   // To performs scans, profiles, and contours, it's best to start from best-fit
   // N.B.: do not update init.par of nuisance parameter, because it changes the fit!
   for (Int_t i = 0; i < fFitPars->GetNPars(); ++i) {
      fFitPars->GetParEntry(i)->CopyFitValBestInVal();
      if (fFitPars->GetParEntry(i)->GetFitType() == kFIT)
         fFitPars->GetParEntry(i)->CopyValInFitInit();
   }


   // Perform scans
   if (GetNScans() > 0) {
      PrintLog("# Perform scan for:", is_logandstdout);
      for (Int_t i_scan = 0; i_scan < GetNScans(); ++i_scan) {
         string scan_par = fFitPars->GetParEntry(fScans[i_scan].i_par)->GetName();
         PrintLog(Form("   - %s", scan_par.c_str()), is_logandstdout);
         FillGraphsScansOrProfiles(i_scan, true);
         // Store file
         if (fOutputLog != stdout)
            TUMisc::Print(fOutputLog, fScans[i_scan].gr, 0);
         if (fOutputDir != "" && fOutputDir != "usine.tests") {
            string f_scan = fOutputDir + "/fit_parameterscan_" + scan_par + ".out";
            FILE *fp_scan = fopen(f_scan.c_str(), "w");
            TUMisc::PrintUSINEHeader(fp_scan, true);
            TUMisc::Print(fp_scan, fScans[i_scan].gr, 0);
            fclose(fp_scan);
            cout << "  => Saved in " << f_scan << endl;
         }
      }
   }


   // Extract profiles likelihood
   if (GetNProfiles() > 0) {
      PrintLog("# Profile likelihood for:", is_logandstdout);
      for (Int_t i_prof = 0; i_prof < GetNProfiles(); ++i_prof) {
         string prof_par = fFitPars->GetParEntry(fProfiles[i_prof].i_par)->GetName();
         PrintLog(Form("   - %s", prof_par.c_str()), is_logandstdout);
         // No chatter for minimizer
         //fMinimizer->SetPrintLevel(0);
         FillGraphsScansOrProfiles(i_prof, false);
         // Reset to user-chosen chatter
         //fMinimizer->SetPrintLevel(fMinuitPrintLevel);
         // Store file
         if (fOutputLog != stdout)
            TUMisc::Print(fOutputLog, fProfiles[i_prof].gr, 0);
         if (fOutputDir != "" && fOutputDir != "usine.tests") {
            string f_prof = fOutputDir + "/fit_profilelikelihood_" + prof_par + ".out";
            FILE *fp_prof = fopen(f_prof.c_str(), "w");
            TUMisc::PrintUSINEHeader(fp_prof, true);
            TUMisc::Print(fp_prof, fProfiles[i_prof].gr, 0);
            fclose(fp_prof);
            cout << "  => Saved in " << f_prof << endl;
         }
      }
   }


   // Extract contours
   if (GetNContours() > 0) {
      PrintLog("# Extract contours for:", is_logandstdout);
      for (Int_t i_cont = 0; i_cont < GetNContours(); ++i_cont) {
         string cont_par1 = fFitPars->GetParEntry(fContours[i_cont].i_par1)->GetName();
         string cont_par2 = fFitPars->GetParEntry(fContours[i_cont].i_par2)->GetName();
         PrintLog(Form("   - %s vs %s (%d pts)", cont_par1.c_str(), cont_par2.c_str(), fContours[i_cont].n_pts), is_logandstdout);
         if (FillGraphsContours(i_cont)) {
            TUMisc::Print(stdout, fContours[i_cont].mg, 0);
            // Store file
            if (fOutputLog != stdout)
               TUMisc::Print(fOutputLog, fContours[i_cont].mg, 0);
            if (fOutputDir != "" && fOutputDir != "usine.tests") {
               string f_cont = fOutputDir + "/fit_contours_" + cont_par1 + "_vs_" + cont_par2 + ".out";
               FILE *fp_cont = fopen(f_cont.c_str(), "w");
               TUMisc::PrintUSINEHeader(fp_cont, true);
               TUMisc::Print(fp_cont, fContours[i_cont].mg, 0);
               fclose(fp_cont);
               cout << "  => Saved in " << f_cont << endl;
            }
         } else
            cout << "  => Unable to extract contour..." << endl;
      }
   }


   if (is_verbose)
      PrintLog(Form("%s[TURunPropagation::MinimiseTOAFluxes] <DONE>\n\n", indent.c_str()), is_logandstdout);
   TUMessages::Indent(false);
}

//______________________________________________________________________________
Bool_t TURunPropagation::ModelBiasAtFitData(Int_t i_qty, Int_t i_exp, gENUM_ETYPE etype, vector<Double_t> &x_bias, vector<Double_t> &y_bias)
{
   //--- Calculates bias to apply to model when comparing to specific fit data (if covariance). Returns false is no bias.
   // INPUTS
   //  i_qty                 Index of quantity in fFitData
   //  i_exp                 Index of experiment in fFitData
   //  e_type                energy type of data in fFitData
   // OUTPUT
   //  x_bias                Position x at which bias is calculated (corresponds to E of data)
   //  y_bias                Bias to apply to compare the fit data data to model (at x positions)


   // Retrieve index and number of data that may be biased
   Int_t i_data = fFitData->IndexData(i_exp, i_qty, etype);
   Int_t n_data = fFitData->GetNData(i_exp, i_qty, etype);

   // Create bias (initialised with 1, i.e. no bias)
   x_bias.clear();
   y_bias.clear();

   //--- Cases for which bias is 1
   // a) Not asking for covariance or no covariance data
   if (fFitDataErrType != kERRCOV || fFitData->GetEntry(i_data)->GetNCovTypes() == 0)
      return false;
   // b) Nuisance parameters not used or not in fit
   Bool_t is_nuispar4fit = false;
   for (Int_t tt = 0; tt < fFitData->ParsData_GetN(); ++tt) {
      if (fFitData->ParsData_IsNuisParsInFit(tt) && (fFitData->ParsData_IndexExp(tt) == i_exp) && (fFitData->ParsData_IndexQty(tt) == i_qty)) {
         is_nuispar4fit = true;
         break;
      }
   }
   if (!is_nuispar4fit)
      return false;

   // --- If bias, evaluate it
   for (Int_t g = 0; g < n_data; ++g) {
      x_bias.push_back(fFitData->GetEntry(i_data + g)->GetEMean());
      y_bias.push_back(fFitData->ParsData_ModelBias(i_data + g));
      //cout << "x_bias,y_bias (data): " << g << " " << x_bias[g] << " , " << y_bias[g] << endl;
   }
   return true;
}

//______________________________________________________________________________
TGraphAsymmErrors *TURunPropagation::OrphanResiduals(vector<Double_t> const &e_model, vector<Double_t> &y_model,
      TGraphAsymmErrors *gr_data, Int_t switch_res)
{
   //--- Prints and displays local (Solar system) fluxes for any quantity/modul level.
   //  e_model               [NE] E grid values
   //  y_model               [NE] Model values on egrid
   //  gr_data               [NData] TGraphAsymmErrors for data
   //  switch_res            Quantity to return:
   //                          - 0 -> (data-model)/model       (residual)
   //                          - 1 -> (data-model)/sigma_data  (score)

   // Start with a copy of gr_data
   TGraphAsymmErrors *gr_res = new TGraphAsymmErrors(*gr_data);
   string name[2] = {"residual", "score"};
   gr_res->SetName(name[switch_res].c_str());

   // Interpolate model on data grid
   Double_t *y_model_interp = new Double_t[gr_data->GetN()];
   TUMath::Interpolate(&e_model[0], (Int_t)e_model.size(), &y_model[0], gr_data->GetX(), gr_data->GetN(), y_model_interp, kLOGLOG);

   if (switch_res == 0) {
      // (data-model)/model
      gr_res->GetYaxis()->SetTitle("residual #equiv ( data - model ) / model [\%]");
      for (Int_t g = 0; g < gr_res->GetN(); ++g) {
         Double_t x, y;
         gr_res->GetPoint(g, x, y);
         // Residual
         gr_res->SetPoint(g, x, (y - y_model_interp[g]) / y_model_interp[g] * 100.);
         // Rescale errors
         gr_res->SetPointEYhigh(g, gr_res->GetErrorYhigh(g) / y * 100.);
         gr_res->SetPointEYlow(g, gr_res->GetErrorYlow(g) / y * 100.);
      }
   } else if (switch_res == 1) {
      // (data-model)/sigma_data
      gr_res->GetYaxis()->SetTitle("score #equiv ( data - model ) / #sigma_{data}");
      for (Int_t g = 0; g < gr_res->GetN(); ++g) {
         Double_t x, y;
         gr_res->GetPoint(g, x, y);
         // Residual
         if (y_model_interp[g] > y)
            gr_res->SetPoint(g, x, (y - y_model_interp[g]) / gr_res->GetErrorYhigh(g));
         else
            gr_res->SetPoint(g, x, (y - y_model_interp[g]) / gr_res->GetErrorYlow(g));
         // Rescale errors
         gr_res->SetPointEYhigh(g, 0.);
         gr_res->SetPointEYlow(g, 0.);
      }
   } else
      TUMessages::Error(fOutputLog, "TURunPropagation", "OrphanResiduals", Form("switch_res=%d not defined", switch_res));

   delete[] y_model_interp;
   y_model_interp = NULL;
   return gr_res;
}

//______________________________________________________________________________
void TURunPropagation::PlotSpectra(TApplication *app, TURunOutputs const &results, Bool_t is_sep_isot, Bool_t is_print, FILE *f_print)
{
   //--- Plots and saves spectra-related quantities in fResults (fluxes, ratios, primary and isotopic fractions...).
   //  app              Root application
   //  results          Structure containing all results to plot
   //  is_sep_isot      If true, displays/prints separate contribution of isotopes (for elements)
   //  is_print         Whether to print or not content of gr/h/multigr
   //  f_print          File in which to print gr/h/multigr

   // Create Root file
   string plot_root = fOutputDir + "/results.root";
   TFile *root_file = NULL;
   if (fIsSaveInFiles)
      root_file = new TFile(plot_root.c_str(), "recreate");
   // Set Plot style
   TUMisc::RootSetStylePlots();
   TText *usine_txt = TUMisc::OrphanUSINEAdOnPlots();


   // Canvas, multigraph and legend
   Int_t n_combos = results.GetNCombos();
   if (n_combos == 0)
      TUMessages::Error(stdout, "TURunPropagation", "PlotSpectra", "No combo found");
   TCanvas **cvs = new TCanvas*[n_combos];
   TMultiGraph **mg = new TMultiGraph*[n_combos];
   TLegend **leg = new TLegend*[n_combos];
   TLegend **leg_data = new TLegend*[n_combos];
   // and, if model was fitted, residuals (w.r.t. model or 1sigma data errors)
   const Int_t n_res = 2;
   string res[n_res] = {"residual", "score"};
   TCanvas **cvs_res[n_res] = {NULL, NULL};
   TMultiGraph **mg_res[n_res] = {NULL, NULL};
   TLegend **leg_res[n_res] = {NULL, NULL};
   if (fFitData && !fIsCLMode) {
      for (Int_t i_res = 0; i_res < n_res; ++i_res) {
         cvs_res[i_res] = new TCanvas*[n_combos];
         mg_res[i_res] = new TMultiGraph*[n_combos];
         leg_res[i_res] = new TLegend*[n_combos];
      }
   }
   // Model parameters
   TCanvas *cvs_model = NULL;


   // Retrieve name of all experiences (to remove from solar modulation name if needed)
   vector<string> names_all_fit_exp;
   if (fFitData && !fIsCLMode) {
      for (Int_t i = 0; i < n_combos; ++i) {
         for (Int_t j = 0; j < results.GetNphiFF(i); ++j) {
            string name_exp = fFitData->GetExp(results.GetIndexExp4FitData(i, j));
            TUMisc::RemoveSpecialChars(name_exp);
            names_all_fit_exp.push_back(name_exp);
         }
      }
   }

   // Loop on combos to display
   for (Int_t i = 0; i < n_combos; ++i) {
      Int_t n_x = results.GetNE();
      gENUM_ETYPE etype = results.GetEType(i);
      string combo = results.GetCombo(i);
      string combo_uc = results.GetCombo(i);
      TUMisc::UpperCase(combo_uc);
      Double_t e_power = results.GetEPower();

      // Get x and y titles for combo
      string x_title = "", y_title = "";
      TUEnum::XYTitles(x_title, y_title, combo, etype, e_power);

      ////////////////////////////////
      //// CANVAS (DATA & MODELS) ////
      ////////////////////////////////
      string name = Form("local_fluxes_%s_%s", combo.c_str(), TUEnum::Enum2Name(etype).c_str());
      TUMisc::RemoveSpecialChars(name);
      cvs[i] = new TCanvas(name.c_str(), (combo + " (local fluxes)").c_str());
      cvs[i]->SetWindowPosition(0, i * 50);
      cvs[i]->SetLogx();
      cvs[i]->cd();
      leg_data[i] = new TLegend(0.5, 0.5, 0.95, 0.93);
      leg[i] = new TLegend(0.16, 0.7, 0.87, 0.93);
      if (fFitData && !fIsCLMode) {
         TUEnum::Enum2Name(fFitDataErrType).c_str(), TUEnum::Enum2Name(fDisplayedErrType).c_str(),
                leg[i]->SetHeader(Form("#splitline{#sigma_{data}: k%s (fit), k%s (plot)}{%s}",
                                       TUEnum::Enum2Name(fFitDataErrType).c_str(),
                                       TUEnum::Enum2Name(fDisplayedErrType).c_str(),
                                       fFitERange.c_str()));
      } else {
         if (GetNSamples() > 0)
            leg[i]->SetHeader(Form("#splitline{#sigma_{data}: k%s}{CLs from %d samples}",
                                   TUEnum::Enum2Name(fFitDataErrType).c_str(), GetNSamples()));
         else
            leg[i]->SetHeader(Form("#sigma_{data}: k%s", TUEnum::Enum2Name(fFitDataErrType).c_str()));
      }
      leg[i]->SetTextSize(0.04);

      ///////////////////////////////////
      //// CANVAS (RESIDUALS IF FIT) ////
      ///////////////////////////////////
      if (fFitData && !fIsCLMode) {
         // Allocate arrays of canvas, mg, and leg (one per combo)
         string res[n_res] = {"residual", "score"};
         for (Int_t i_res = 0; i_res < n_res; ++i_res) {
            // Allocate for combo
            string name_res = name + "_" + res[i_res];
            cvs_res[i_res][i] = new TCanvas(name_res.c_str(), (combo + " (" + res[i_res] + ")").c_str(), 500, 300);
            cvs_res[i_res][i]->SetWindowPosition(i_res * 250, 400 + i * 50);
            cvs_res[i_res][i]->SetLogx();
            //cvs_res[i_res][i]->cd();
            leg_res[i_res][i] = new TLegend(0.14, 0.75, 0.9, 0.93);
            leg_res[i_res][i]->SetTextSize(0.045);
            leg_res[i_res][i]->SetHeader(("#sigma_{data}: k" + TUEnum::Enum2Name(fFitDataErrType)
                                          + " (fit), k" + TUEnum::Enum2Name(fDisplayedErrType) + " (plot)").c_str());

            mg_res[i_res][i] = new TMultiGraph();
            mg_res[i_res][i]->SetName(("mgres_" + name_res).c_str());
         }
      }


      ////////////////////////
      //// GRAPH EXP DATA ////
      ////////////////////////
      TMultiGraph *mg_tmp =
         fDisplayedData->OrphanGetMultiGraph(combo, "ALL", fDisplayedErrType, etype, e_power, false, leg_data[i], fOutputLog);
      if (mg_tmp)
         mg[i] = mg_tmp;
      else {
         mg[i] = new TMultiGraph();
         string mg_titles = ";" + x_title + ";" + y_title;
         mg[i]->SetTitle(mg_titles.c_str());
      }
      mg[i]->SetName(("mg_" + name).c_str());
      mg[i]->Draw("A");
      usine_txt->Draw();
      if (is_print)
         TUMisc::Print(f_print, mg[i], 2);

      Bool_t ylogscale = false;


      // Loop on all propagation models
      Int_t n_models = results.GetNModels();
      for (Int_t n = 0; n < n_models; ++n) {

         Int_t colour = n;
         if (fIsCLMode)
            colour = (n + 1) / 2 + 1;

         if (!fIsCLMode || n % 2 == 0)
            leg[i]->AddEntry((TObject *)0, results.GetModel(n).c_str(), "");

         // Loop on all modulation levels for current combo
         for (Int_t j = 0; j < results.GetNphiFF(i); ++j) {

            // Update modulation value (for print purpose)
            fSolModModels[0]->SetParVal(0, results.GetphiFF(i, j));

            // Extract 'id_base'
            string id_base = "model_" + UID(combo, etype, e_power, results.GetModel(n), fSolModModels[0]->UID());
            TUMisc::RemoveSpecialChars(id_base);

            // If fFitData, update 'id_base' and 'colour'
            if (fFitData && !fIsCLMode) {
               // if 'id_base' contains one of the name of experiments (happens
               // if Solar modulation as nuisance), remove it for the model name.
               for (Int_t jjj = 0; jjj < (Int_t)names_all_fit_exp.size(); ++jjj) {
                  size_t found = id_base.find(names_all_fit_exp[jjj]);
                  if (found != string::npos)
                     id_base.erase(found, names_all_fit_exp[jjj].size());
               }

               // Use colour same as in displayed experiments for ease of visualisation
               string qty_exps_etype = combo + ":ALL:" + TUEnum::Enum2Name(etype);
               TUDataSet *subset = fDisplayedData->OrphanFormSubSet(qty_exps_etype, fOutputLog);
               colour = subset->IndexInExps(fFitData->GetExp(results.GetIndexExp4FitData(i, j)));
            }
            ///////////////////////////////////
            //// ADD MODEL: TOT, PRIM, SEC ////
            ///////////////////////////////////
            ylogscale = TUMisc::IsYaxisLog(combo);

            // Check whether prim and sec must be plotted along
            const Int_t n_contribs = 3;
            string name_contrib[n_contribs] = {" (prim)", " (sec)", ""};

            // Form legend for graph
            string phi = "#phi";
            if (fFitData && !fIsCLMode && results.GetIsExpSolModNuis(i, j)) phi = "#phi^{nuisance}";
            string leg_model = "IS";
            if (results.GetphiFF(i, j) > 1.e-3) {
               string phi_text = Form("%.3f GV", results.GetphiFF(i, j));
               if (fSolModModels[0]->GetModelName() == "SolMod0DFF") leg_model = phi + "_{FF}= " + (string)phi_text;
               else leg_model = phi + "_{" + fSolModModels[0]->GetModelName() + "}= " + phi_text;
            }

            // If antimatter and several conditions, do not show prim/sep separately
            Int_t i_start = 0;
            if (!((combo_uc == "1H-BAR" || combo_uc == "2H-BAR" || combo_uc == "H-BAR")
                  && !fIsCLMode && fPropagModels[n]->GetNSrcDM() > 0))
               i_start = 2;

            for (Int_t i_contrib = i_start; i_contrib < n_contribs; ++i_contrib) {
               Int_t line_style = (n + 1) % 10;
               Int_t line_width = (n + 2);
               if (fIsCLMode) {
                  line_width = (n + 1) / 2 + 2;
                  line_style = (n + 1) / 2 + 1;
               }
               Int_t line_colour = TUMisc::RootColor(colour);
               TGraph *gr_contrib = new TGraph(n_x);
               for (Int_t k = 0; k < n_x; ++k)
                  gr_contrib->SetPoint(k, results.GetEAxisVal(i, k), results.GetTOACombo(i_contrib, i, n, j, k));
               if (i_contrib == 0)
                  line_style = (j + 1) % 10;
               else if (i_contrib == 2 && fIsCLMode)
                  line_colour = TUMisc::RootColor(i);

               // Set graph properties, save, add in mg
               TUEnum::XYTitles(x_title, y_title, combo, etype, e_power);
               string gr_name = id_base + "_" + name_contrib[i_contrib];
               TUMisc::RemoveSpecialChars(gr_name);
               gr_contrib->SetName(gr_name.c_str());
               gr_contrib->SetTitle("");
               gr_contrib->SetLineColor(line_colour);
               gr_contrib->SetLineStyle(line_style);
               gr_contrib->SetLineWidth(line_width);
               gr_contrib->GetXaxis()->SetTitle(x_title.c_str());
               gr_contrib->GetXaxis()->CenterTitle();
               gr_contrib->GetYaxis()->SetTitle(y_title.c_str());
               gr_contrib->GetYaxis()->CenterTitle();
               if (fIsSaveInFiles) {
                  root_file->cd();
                  gr_contrib->Write();
               }
               mg[i]->Add(gr_contrib, "L");
               // To only draw even sigma
               if (!fIsCLMode || n % 2 == 0) {
                  TLegendEntry *entry = leg[i]->AddEntry(gr_contrib, (leg_model + name_contrib[i_contrib]).c_str(), "L");
                  entry->SetTextColor(line_colour);
                  entry = NULL;
               }
               gr_contrib = NULL;
            }


            ///////////////////////////////////////////////
            //// ADD MODEL: COMBO BROKEN-DOWN INTO CRS ////
            ///////////////////////////////////////////////
            if (is_sep_isot) {
               for (Int_t r = 0; r < results.GetNCombosRatios(i); ++r) {
                  TGraph *gr = new TGraph(n_x);
                  for (Int_t k = 0; k < n_x; ++k)
                     gr->SetPoint(k, results.GetEAxisVal(i, k), results.GetTOAComboRatio(i, r, n, j, k));
                  string id_ratio = UID(results.GetComboRatio(i, r), etype, e_power, results.GetModel(n), fSolModModels[0]->UID());
                  TUMisc::RemoveSpecialChars(id_ratio);
                  gr->SetName(id_ratio.c_str());
                  gr->SetTitle("");
                  gr->SetLineColor(TUMisc::RootColor(n));
                  gr->SetLineStyle((r + 2) % 10);
                  gr->SetLineWidth(1);
                  string x_tit = "", y_tit = "";
                  TUEnum::XYTitles(x_title, y_title, results.GetComboRatio(i, r), etype, e_power);
                  gr->GetXaxis()->SetTitle(x_title.c_str());
                  gr->GetXaxis()->CenterTitle();
                  gr->GetYaxis()->SetTitle(y_title.c_str());
                  gr->GetYaxis()->CenterTitle();
                  if (fIsSaveInFiles) {
                     root_file->cd();
                     gr->Write();
                  }
                  mg[i]->Add(gr, "L");
                  // To only draw even sigma
                  if (fIsCLMode && n % 2 != 0) {
                     TLegendEntry *entry = leg[i]->AddEntry(gr, results.GetComboRatio(i, r).c_str(), "L");
                     entry->SetTextColor(TUMisc::RootColor(n));
                     entry = NULL;
                  }
                  gr = NULL;
               }
            }

            ////////////////////////////////////////
            //// ADD BIASED-MODEL (FIT-RELATED) ////
            ////////////////////////////////////////
            vector<Double_t> modelbiased;
            vector<Double_t> eaxis_vals;
            for (Int_t k = 0; k < n_x; ++k)
               eaxis_vals.push_back(results.GetEAxisVal(i, k));

            Bool_t is_bias = false;
            if (fFitData && !fIsCLMode) {
               // Check whether data were biased or not: if bias, retrieve it!
               //   => depends whether nuisance parameters were used or not on the data
               vector<Double_t> x_bias, y_bias;
               Int_t i_qty = fFitData->IndexInQties(combo);
               Int_t i_exp = results.GetIndexExp4FitData(i, j);
               is_bias = ModelBiasAtFitData(i_qty, i_exp, etype, x_bias, y_bias);

               // Calculate biased model and add/save
               if (is_bias) {
                  for (Int_t k = 0; k < n_x; ++k)
                     modelbiased.push_back(results.GetTOAComboTot(i, n, j, k));
                  TUMath::Bias(eaxis_vals, modelbiased, x_bias, y_bias, kLOGLIN);
                  TGraph *gr_modelbiased = new TGraph(n_x, &eaxis_vals[0], &modelbiased[0]);
                  gr_modelbiased->SetName((id_base + "_bias").c_str());
                  gr_modelbiased->SetLineColor(TUMisc::RootColor(colour));
                  gr_modelbiased->SetLineWidth(1);
                  gr_modelbiased->SetLineStyle(1);
                  gr_modelbiased->GetXaxis()->SetTitle(x_title.c_str());
                  gr_modelbiased->GetXaxis()->CenterTitle();
                  gr_modelbiased->GetYaxis()->SetTitle(y_title.c_str());
                  gr_modelbiased->GetYaxis()->CenterTitle();
                  mg[i]->Add(gr_modelbiased, "L");
                  TLegendEntry *entry = leg[i]->AddEntry(gr_modelbiased, (leg_model + (string)" (#times nuis. bias)").c_str(), "L");
                  entry->SetTextColor(TUMisc::RootColor(colour));
                  entry = NULL;
                  gr_modelbiased = NULL;
               }
            }


            ////////////////////////////////////
            //// FIT-RELATED: ADD RESIDUALS ////
            ////////////////////////////////////
            if (fFitData && !fIsCLMode) {
               // Retrieve data w.r.t. which calculate residuals
               Int_t i_qty = fFitData->IndexInQties(combo);
               Int_t i_exp = results.GetIndexExp4FitData(i, j);
               Int_t i_data = fFitData->IndexData(i_exp, i_qty, etype);
               string err = TUEnum::Enum2Name(fDisplayedErrType);
               TGraphAsymmErrors *gr_data = fFitData->OrphanGetGraph(i_data, fDisplayedErrType, etype, 0.);
               string exp_name_short = fFitData->GetExp(i_exp).substr(0, 28);

               // Calculation must-be done w.r.t. 'flat' data (not multiplied by E^e_power)
               vector<Double_t> model_original;
               if (is_bias) {
                  model_original = modelbiased;
                  exp_name_short += ": model=model #times bias";
               } else {
                  for (Int_t k = 0; k < n_x; ++k)
                     model_original.push_back(results.GetTOAComboTot(i, n, j, k));
               }
               // Return to 'original'
               if (!TUMisc::IsRatio(combo) && combo != "<LnA>") {
                  Double_t minus_e_power = -e_power;
                  TUMath::PowerRescale(&eaxis_vals[0], &model_original[0], n_x, minus_e_power);
               }

               // Fill for various types of residuals asked for
               for (Int_t i_res = 0; i_res < n_res; ++i_res) {
                  TGraphAsymmErrors *gr_res = OrphanResiduals(eaxis_vals, model_original, gr_data, i_res);
                  string res_name = (string)gr_res->GetName() + "_" + combo + "_" + fFitData->GetExp(i_exp) + "_"
                                    + err + "_" + TUEnum::Enum2Name(etype);
                  TUMisc::RemoveSpecialChars(res_name);
                  gr_res->SetName(res_name.c_str());
                  gr_res->SetMarkerColor(TUMisc::RootColor(colour));
                  gr_res->SetLineColor(TUMisc::RootColor(colour));
                  gr_res->SetMarkerStyle(TUMisc::RootMarker(colour));
                  gr_res->SetMarkerSize(1.2);
                  gr_res->SetDrawOption("P");
                  gr_res->SetFillColor(0);
                  gr_res->SetFillStyle(0);
                  gr_res->GetXaxis()->SetTitle(x_title.c_str());
                  mg_res[i_res][i]->Add(gr_res, "P");
                  string mg_title = ";" + x_title + "; " + gr_res->GetYaxis()->GetTitle();
                  mg_res[i_res][i]->SetTitle(mg_title.c_str());
                  TLegendEntry *entry = leg_res[i_res][i]->AddEntry(gr_res, exp_name_short.c_str(), "P");
                  entry->SetTextColor(TUMisc::RootColor(colour));
                  entry = NULL;
               }
               // Free memory for data
               delete gr_data;
               gr_data = NULL;
            }

         }
      }
      // mg in first cvs (spectrum)
      cvs[i]->cd();
      cvs[i]->SetLogy(ylogscale);
      mg[i]->Draw("A");
      mg[i]->GetXaxis()->CenterTitle();
      mg[i]->GetYaxis()->CenterTitle();
      usine_txt->Draw();
      if (is_print)
         TUMisc::Print(f_print, mg[i], 0);
      leg_data[i]->Draw();
      leg[i]->Draw();
      cvs[i]->Modified();
      cvs[i]->Update();
      if (fIsSaveInFiles) {
         root_file->cd();
         cvs[i]->Write();
         TUMisc::RootSave(cvs[i], fOutputDir, cvs[i]->GetName(), "CP");
      }


      // And biased model and residuals...
      if (fFitData && !fIsCLMode) {
         for (Int_t i_res = 0; i_res < n_res; ++i_res) {
            cvs_res[i_res][i]->cd();
            cvs_res[i_res][i]->SetLogy(0);
            mg_res[i_res][i]->Draw("A");
            mg_res[i_res][i]->GetXaxis()->CenterTitle();
            mg_res[i_res][i]->GetYaxis()->CenterTitle();
            if (i_res == 1) {
               // Draw 1 sigma line
               TLine line;
               line.SetLineColor(kGray);
               line.SetLineWidth(2);
               line.SetLineStyle(2);
               Double_t xmin = mg_res[i_res][i]->GetXaxis()->GetXmin();
               Double_t xmax = mg_res[i_res][i]->GetXaxis()->GetXmax();
               line.DrawLine(xmin, -1., xmax, -1.);
               line.DrawLine(xmin, 1., xmax, 1.);
               TLatex tl;
               tl.SetTextAlign(12);
               tl.SetTextColor(kGray);
               tl.SetTextSize(0.08);
               tl.DrawLatex(xmin * 1.3, 0., "1#sigma");
            }
            usine_txt->Draw();
            if (is_print)
               TUMisc::Print(f_print, mg_res[i_res][i], 0);
            leg_res[i_res][i]->Draw();
            cvs_res[i_res][i]->Modified();
            cvs_res[i_res][i]->Update();
            if (fIsSaveInFiles) {
               root_file->cd();
               cvs_res[i_res][i]->Write();
               TUMisc::RootSave(cvs_res[i_res][i], fOutputDir, cvs_res[i_res][i]->GetName(), "CP");
            }
         }
      }
   }


   ///////////////////////////////////
   // Scans, profiles, and contours //
   ///////////////////////////////////
   TCanvas **cvs_scan = NULL;
   TCanvas **cvs_prof = NULL;
   TCanvas **cvs_cont = NULL;
   if (fFitData && !fIsCLMode) {
      // void PlotContoursScansOrProfiles(TCanvas **cvs, Int_t scan0_prof1_cont2, TText *usine_txt, Bool_t is_print, FILE * f_print, TFile *root_file);
      if (GetNScans() > 0) {
         cvs_scan = new TCanvas*[GetNScans()];
         PlotContoursScansOrProfiles(cvs_scan, 0, usine_txt, is_print, f_print, root_file);
      }
      if (GetNProfiles() > 0) {
         cvs_prof = new TCanvas*[GetNProfiles()];
         PlotContoursScansOrProfiles(cvs_prof, 1, usine_txt, is_print, f_print, root_file);
      }
      if (GetNContours() > 0) {
         cvs_cont = new TCanvas*[GetNContours()];
         PlotContoursScansOrProfiles(cvs_cont, 2, usine_txt, is_print, f_print, root_file);
      }
   }



   /////////////////////////////////////////////////////////////////////
   // 1. Primary/secondary fraction w.r.t. propag. (local IS fluxes)  //
   // 2. Isotopic fraction w.r.t. elemental contribution              //
   /////////////////////////////////////////////////////////////////////
   string name_eaxis = TUEnum::Enum2NameUnit(results.GetEType(0));
   // Draw
   TCanvas *c_frac_primsec = new TCanvas();
   TMultiGraph *mg_frac_primsec = new TMultiGraph();
   TLegend *leg_frac_primsec = new TLegend();
   PlotSpectraISFractions(results, /*is_prim_or_isot*/true,
                          c_frac_primsec, mg_frac_primsec, leg_frac_primsec,
                          usine_txt, is_print, f_print, root_file);

   TCanvas *c_frac_isot = new TCanvas();
   TMultiGraph *mg_frac_isot = new TMultiGraph();
   TLegend *leg_frac_isot = new TLegend();
   PlotSpectraISFractions(results, /*is_prim_or_isot*/false,
                          c_frac_isot, mg_frac_isot, leg_frac_isot,
                          usine_txt, is_print, f_print, root_file);


   ///////////////////////////
   // PLOT MODEL PARAMETERS //
   ///////////////////////////
   if (!is_print) {
      string f_model = fOutputDir + "/model_vals_pars.out";
      FILE *fp_model = fopen(f_model.c_str(), "w");
      cvs_model = fPropagModels[0]->OrphanModelParameters(results, true, fp_model);
      fclose(fp_model);
      cout << "  => Saved in " << f_model << endl;
   } else
      cvs_model = fPropagModels[0]->OrphanModelParameters(results, true, f_print, false);


   //////////////////////////////////
   // We are done! Close ROOT file //
   //////////////////////////////////
   if (fIsSaveInFiles)
      root_file->Close();


   ///////////////////////////////////////////////////////////////////
   // Enter event loop, one can now interact with the objects in
   // the canvas. Select "Exit ROOT" from cvs "File" menu to exit
   // the event loop and execute the next statements.
   ///////////////////////////////////////////////////////////////////
   if (!gROOT->IsBatch())
      app->Run(kTRUE);


   /////////////////
   // Free memory //
   /////////////////
   // -> canvas
   if (cvs_model) delete cvs_model;
   cvs_model = NULL;
   // -> graphs
   if (mg) {
      for (Int_t c = 0; c < n_combos; ++c) {
         if (mg[c]) delete mg[c];
         mg[c] = NULL;
         if (leg_data[c]) delete leg_data[c];
         leg_data[c] = NULL;
         if (leg[c]) delete leg[c];
         leg[c] = NULL;
         if (cvs[c]) delete cvs[c];
         cvs[c] = NULL;
      }
      delete[] mg;
      mg = NULL;
      delete[] leg_data;
      leg_data = NULL;
      delete[] leg;
      leg = NULL;
      delete[] cvs;
      cvs = NULL;
   }
   // -> fit-specific
   if (fFitData && !fIsCLMode) {
      // Residuals: do not delete mg_res[n_res],
      // it was not created with new!
      for (Int_t i_res = 0; i_res < n_res; ++i_res) {
         if (mg_res[i_res]) {
            for (Int_t c = 0; c < n_combos; ++c) {
               if (mg_res[i_res][c]) delete mg_res[i_res][c];
               mg_res[i_res][c] = NULL;
               if (leg_res[i_res][c]) delete leg_res[i_res][c];
               leg_res[i_res][c] = NULL;
               if (cvs_res[i_res][c]) delete cvs_res[i_res][c];
               cvs_res[i_res][c] = NULL;
            }
            mg_res[i_res] = NULL;
            leg_res[i_res] = NULL;
            cvs_res[i_res] = NULL;
         }
      }
      // Scans
      if (cvs_scan) {
         for (Int_t c = 0; c < GetNScans(); ++c) {
            if (cvs_scan[c]) delete cvs_scan[c];
            cvs_scan[c] = NULL;
         }
         if (cvs_scan) delete[] cvs_scan;
         cvs_scan = NULL;
      }
      // Contours
      if (cvs_cont) {
         for (Int_t c = 0; c < GetNContours(); ++c) {
            if (cvs_cont[c]) delete cvs_cont[c];
            cvs_cont[c] = NULL;
         }
         if (cvs_cont) delete[] cvs_cont;
         cvs_cont = NULL;
      }
   }
   //  -> fractions
   if (mg_frac_primsec) delete mg_frac_primsec;
   mg_frac_primsec = NULL;
   if (leg_frac_primsec) delete leg_frac_primsec;
   leg_frac_primsec = NULL;
   if (c_frac_primsec) delete c_frac_primsec;
   c_frac_primsec = NULL;
   // Isotopic fractions
   if (mg_frac_isot) delete mg_frac_isot;
   mg_frac_isot = NULL;
   if (leg_frac_isot) delete leg_frac_isot;
   leg_frac_isot = NULL;
   if (c_frac_isot) delete c_frac_isot;
   c_frac_isot = NULL;

   // Free memory
   delete usine_txt;

}

//______________________________________________________________________________
void TURunPropagation::PlotSpectraISFractions(TURunOutputs const &results, Bool_t is_prim_or_isot,
      TCanvas *c_frac, TMultiGraph *mg_frac, TLegend *leg_frac,
      TText *usine_txt, Bool_t is_print, FILE *f_print, TFile *root_file)
{
   //--- Draws and displays fractions for obtained values/lists/models.
   // INPUTS:
   //  results               Structure of quantities to display
   //  is_prim_or_isot       Whether frac_vals is sec/prim fraction (true) or isotopic fraction (false)
   //  usine_txt             USINE ad
   //  is_print              Whether to print or not in file f_print
   //  f_print               File in which t print outputs
   //  root_file             File in which to print
   // OUTPUTS:
   //  c_frac                Root canvas
   //  mg_frac               Root multigraph
   //  leg_frac              Root legend


   // Canvas, multigraph, and legend must be created (new) before calling function
   if (!c_frac || !mg_frac || !leg_frac)
      TUMessages::Error(f_print, "TURunPropagation", "PlotSpectraISFractions",
                        "c_frac, mg_frac, and leg_frac must be created (new) before calling this function!");

   string c_name = "local_fluxes";
   string c_title = "(local IS fluxes)";
   string axis1_title = "";
   string axis2_title = "";
   if (is_prim_or_isot) {
      c_name += "_primarycontent";
      c_title = "Primary/secondary content " + c_title;
      axis1_title = "Primary fraction  f  [%]";
      axis2_title = "Secondary fraction  (1-f)  [%]";
   } else {
      c_name += "_isotopicfraction";
      c_title = "Isotopic fraction of elements " + c_title;
      axis1_title = "Isotopic fraction  f  [%]";
      axis2_title = "Other isotopes  (1-f)  [%]";
   }
   TUMisc::RemoveSpecialChars(c_name);

   // Set Canvas
   c_frac->SetName(c_name.c_str());
   c_frac->SetTitle(c_title.c_str());
   c_frac->SetWindowPosition(c_frac->GetWw(), 0 + (Int_t)(!is_prim_or_isot) * 100);
   //c_frac->SetGridx();
   //c_frac]->SetGridy();
   c_frac->SetLogx(1);
   c_frac->SetLogy(0);
   c_frac->SetRightMargin(0.11);
   c_frac->SetTickx(1);
   c_frac->SetTicky(1);

   // Set mg and leg
   mg_frac->SetTitle("");
   string mg_name = "mg_" + c_name;
   mg_frac->SetName(mg_name.c_str());
   //
   leg_frac->SetX1(0.60);
   leg_frac->SetY1(0.65);
   leg_frac->SetX2(0.92);
   leg_frac->SetY2(0.92);

   Int_t n_models = results.GetNModels();
   for (Int_t n = 0; n < n_models; ++n) {
      // To only draw even sigma
      if (!fIsCLMode || n % 2 == 0)
         leg_frac->AddEntry((TObject *)0, results.GetModel(n).c_str(), "");
      Int_t n_qties = 0;
      if (is_prim_or_isot) n_qties = results.GetNIsotAndZ();
      else n_qties = results.GetNIsot();

      for (Int_t i = 0; i < n_qties; ++i) {

         // Get family combo
         gENUM_CRFAMILY family_combo;
         string combo = "";
         if (is_prim_or_isot) combo = results.GetIsotAndZ(i);
         else combo = results.GetIsot(i);

         if (!fPropagModels[0]->IsComboPossibleForEGrids(combo, family_combo))
            break;

         TUAxis *e_grid =  fPropagModels[0]->GetEFamily(family_combo);
         Int_t z_i = fPropagModels[0]->NameToZ(combo);
         string gr_name = "";
         TGraph *gr_frac = new TGraph(e_grid->GetN());
         if (is_prim_or_isot) {
            gr_name = "primaryfracIS_" +  combo + "_" + results.GetModel(n);
            for (Int_t k = 0; k < e_grid->GetN(); ++k)
               gr_frac->SetPoint(k, e_grid->GetVal(k), results.GetIsotAndZPrimFrac(i, n, k));
         } else {
            for (Int_t k = 0; k < e_grid->GetN(); ++k)
               gr_frac->SetPoint(k, e_grid->GetVal(k), results.GetIsotFrac(i, n, k));
            gr_name = "isotopicfracIS_" +  combo + "_" + results.GetModel(n);
         }
         TUMisc::RemoveSpecialChars(gr_name);
         gr_frac->SetName(gr_name.c_str());
         gr_frac->SetTitle("");
         gr_frac->SetLineColor(abs(TUMisc::RootColor(z_i)));
         gr_frac->SetLineStyle((i + 1) % 10);
         gr_frac->SetLineWidth(2 * n + 2);
         if (fIsCLMode)
            gr_frac->SetLineWidth(2 * (n + 1) / 2);
         gr_frac->GetXaxis()->SetTitle(results.GetEAxisNameUnit(0).c_str());
         gr_frac->GetXaxis()->CenterTitle();
         gr_frac->GetYaxis()->SetTitle(axis1_title.c_str());
         gr_frac->GetYaxis()->CenterTitle();
         //cout << gr_name << endl;
         //gr_frac->Print();
         if (fIsSaveInFiles) {
            root_file->cd();
            gr_frac->Write();
         }
         mg_frac->Add(gr_frac, "L");
         string leg_text = combo.c_str();

         if (!is_prim_or_isot)
            leg_text = leg_text + "/"
                       + fPropagModels[0]->ZToElementName(fPropagModels[0]->NameToZ(combo));

         leg_text = TUMisc::FormatCRQty(leg_text.c_str(), 2); // switch_format0=usine-name,1=text,2=root-plot
         // To only draw even sigma
         if (!fIsCLMode || n % 2 == 0) {
            TLegendEntry *entry = leg_frac->AddEntry(gr_frac, leg_text.c_str(), "L");
            entry->SetTextColor(abs(TUMisc::RootColor(z_i)));
            entry = NULL;
         }
         gr_frac = NULL;
         e_grid = NULL;
      }
   }

   c_frac->cd();
   mg_frac->Draw("A");
   usine_txt->Draw();
   mg_frac->GetXaxis()->SetTitle(results.GetEAxisNameUnit(0).c_str());
   mg_frac->GetXaxis()->CenterTitle();
   mg_frac->GetYaxis()->SetTitle(axis1_title.c_str());
   mg_frac->GetYaxis()->CenterTitle();
   mg_frac->SetMinimum(0.);
   mg_frac->SetMaximum(100.);
   Double_t eknmax = mg_frac->GetXaxis()->GetXmax();
   TGaxis *axis2 = new TGaxis(eknmax, 100., (1. + 1.e-5)*eknmax, 0., 0., 100., 510, "-L");
   axis2->SetName("axis2");
   axis2->CenterTitle();
   axis2->SetTitle(axis2_title.c_str());
   axis2->SetLabelOffset(0.01);
   axis2->SetLabelFont(132);
   axis2->SetLabelSize(0.055);
   axis2->SetTitleOffset(1.1);
   axis2->SetTitleFont(132);
   axis2->SetTitleSize(0.055);
   axis2->SetNdivisions(506);
   if (is_prim_or_isot)
      axis2->Draw("ASAME");
   if (is_print)
      TUMisc::Print(f_print, mg_frac, 0);
   leg_frac->Draw();
   c_frac->Modified();
   c_frac->Update();
   if (fIsSaveInFiles) {
      root_file->cd();
      c_frac->Write();
      TUMisc::RootSave(c_frac, fOutputDir, c_frac->GetName(), "CP");
   }
}

//______________________________________________________________________________
void TURunPropagation::PlotContoursScansOrProfiles(TCanvas **cvs, Int_t scan0_prof1_cont2,
      TText *usine_txt, Bool_t is_print,
      FILE *f_print, TFile *root_file)
{
   //--- Draws and displays free parameter profile (from minimisation).
   // INPUTS:
   //  scan0_prof1_cont2     Different options to draw (scan, contour, or profile)
   //  usine_txt             USINE ad
   //  is_print              Whether to print or not in file f_print
   //  f_print               File in which t print outputs
   //  root_file             File in which to print
   // OUTPUTS:
   //  cvs                   [NProfiles] Root canvas

   // Canvas
   if (!cvs)
      TUMessages::Error(f_print, "TURunPropagation", "PlotSpectraISFractions", "cvs");

   Int_t n_drawn = 0;
   string name = "";
   switch (scan0_prof1_cont2)  {
      case 0:
         name = "scan";
         n_drawn = GetNScans();
         break;
      case 1:
         name = "profile";
         n_drawn = GetNProfiles();
         break;
      case 2:
         name = "contour";
         n_drawn = GetNContours();
         break;
      default:
         string message = Form("scan0_prof1_cont2=%d is not a valid option (0, 1, or 2)", scan0_prof1_cont2);
         TUMessages::Error(GetOutputLog(), "TURunPropagation", "PlotContoursScansOrProfiles", message);
   }

   // Basic check
   if (n_drawn == 0) {
      cvs = NULL;
      string message = "No " + name + " available to plot";
      TUMessages::Warning(GetOutputLog(), "TURunPropagation", "PlotContoursScansOrProfiles", message);
      return;
   }

   // Loop on scans to perform
   for (Int_t i_drawn = 0; i_drawn < n_drawn; ++i_drawn) {
      // If failed to extract contour (= no multigraph)
      if (scan0_prof1_cont2 == 2 && !GetContours(i_drawn))
         continue;

      switch (scan0_prof1_cont2)  {
         case 0: {// Scans
               string cvs_title = Form("Scan %s (%d pts)", fFitPars->GetParEntry(fScans[i_drawn].i_par)->GetFitName().c_str(), fScans[i_drawn].n_pts);
               string cvs_name = Form("scan_%s_%dpts", fFitPars->GetParEntry(fScans[i_drawn].i_par)->GetFitName().c_str(), fScans[i_drawn].n_pts);
               TUMisc::RemoveSpecialChars(cvs_name);
               cvs[i_drawn] = new TCanvas(cvs_name.c_str(), cvs_title.c_str(), 300, 300);
               cvs[i_drawn]->SetLogx(0);
               cvs[i_drawn]->SetLogy(0);
               cvs[i_drawn]->cd();
               cvs[i_drawn]->SetWindowPosition(1000, i_drawn * 75);
               GetScan(i_drawn)->SetLineWidth(2);
               GetScan(i_drawn)->SetMarkerStyle(kPlus);
               GetScan(i_drawn)->Draw("ALP");
               usine_txt->Draw();
               if (is_print)
                  TUMisc::Print(f_print, GetScan(i_drawn), 0);
               break;
            }
         case 1: {// Profiles
               string cvs_title = Form("Profile %s (%d pts)", fFitPars->GetParEntry(fProfiles[i_drawn].i_par)->GetFitName().c_str(), fProfiles[i_drawn].n_pts);
               string cvs_name = Form("profile_%s_%dpts", fFitPars->GetParEntry(fProfiles[i_drawn].i_par)->GetFitName().c_str(), fProfiles[i_drawn].n_pts);
               TUMisc::RemoveSpecialChars(cvs_name);
               cvs[i_drawn] = new TCanvas(cvs_name.c_str(), cvs_title.c_str(), 300, 300);
               cvs[i_drawn]->SetWindowPosition(1150, i_drawn * 75);
               cvs[i_drawn]->SetLogx(0);
               cvs[i_drawn]->SetLogy(0);
               cvs[i_drawn]->cd();
               GetProfile(i_drawn)->SetLineWidth(2);
               GetProfile(i_drawn)->SetMarkerStyle(kPlus);
               GetProfile(i_drawn)->Draw("ALP");
               Double_t x_min = GetProfile(i_drawn)->GetXaxis()->GetXmin();
               Double_t x_max = GetProfile(i_drawn)->GetXaxis()->GetXmax();
               Double_t xrange = x_max - x_min;
               Double_t yrange = GetProfile(i_drawn)->GetYaxis()->GetXmax() - GetProfile(i_drawn)->GetYaxis()->GetXmin();
               TLatex tl;
               tl.SetTextAlign(12);
               tl.SetTextColor(kGray);
               tl.SetTextSize(0.045);
               TLine l_chi2;
               l_chi2.SetLineColor(kGray);
               l_chi2.SetLineWidth(1);
               for (Int_t i = 0; i < 4; ++i) {
                  Double_t y = fChi2Min + Double_t(i * i);
                  l_chi2.SetLineStyle(i + 1);
                  l_chi2.DrawLine(x_min, y, x_max, y);
                  if (i == 0)
                     tl.DrawLatex(x_max - 0.1 * xrange, y + 0.04 * yrange, "#chi^{2}_{min}");
                  else
                     tl.DrawLatex(x_max - 0.1 * xrange, y + 0.04 * yrange, Form("%d#sigma", i));
               }
               usine_txt->Draw();
               if (is_print)
                  TUMisc::Print(f_print, GetProfile(i_drawn), 0);
               break;
            }
         case 2: {// Contours
               string cvs_title =  Form("Contours %s vs %s (%d pts) CLs={%s}",
                                        fFitPars->GetParEntry(fContours[i_drawn].i_par1)->GetFitName().c_str(),
                                        fFitPars->GetParEntry(fContours[i_drawn].i_par2)->GetFitName().c_str(),
                                        fContours[i_drawn].n_pts, TUMisc::List2String(fContours[i_drawn].cls, ',', 1).c_str());
               string cvs_name =  Form("contours_%s_%s_%dpts_CLs%s",
                                       fFitPars->GetParEntry(fContours[i_drawn].i_par1)->GetFitName().c_str(),
                                       fFitPars->GetParEntry(fContours[i_drawn].i_par2)->GetFitName().c_str(),
                                       fContours[i_drawn].n_pts, TUMisc::List2String(fContours[i_drawn].cls, '_', 1).c_str());
               TUMisc::RemoveSpecialChars(cvs_name);
               cvs[i_drawn] = new TCanvas(cvs_name.c_str(), cvs_title.c_str(), 300, 300);
               cvs[i_drawn]->SetWindowPosition(1300, i_drawn * 75);
               cvs[i_drawn]->SetLogx(0);
               cvs[i_drawn]->SetLogy(0);
               cvs[i_drawn]->cd();
               GetContours(i_drawn)->Draw("A");
               GetContours(i_drawn)->GetXaxis()->CenterTitle();
               GetContours(i_drawn)->GetYaxis()->CenterTitle();
               fContours[i_drawn].leg->Draw();
               usine_txt->Draw();
               if (is_print)
                  TUMisc::Print(f_print, GetContours(i_drawn), 0);
               break;
            }
         default:
            break;
      }

      cvs[i_drawn]->Modified();
      cvs[i_drawn]->Update();
      if (fIsSaveInFiles) {
         root_file->cd();
         cvs[i_drawn]->Write();
         TUMisc::RootSave(cvs[i_drawn], fOutputDir, cvs[i_drawn]->GetName(), "CP");
      }
   }
}

//______________________________________________________________________________
void TURunPropagation::Plots_AntinucExtra(TApplication *app, Int_t switch_plot, Bool_t is_noina,
      string const &option, gENUM_ETYPE e_type, Bool_t is_test, FILE *f_test)
{
   //--- Plots for checks related to species produced via a differential X-section
   //    (e.g., 1H-BAR, 2H-BAR). The total secondary and tertiary contributions (if
   //    exist) are split in various reactions and energy ranges.
   //
   //  app                 ROOT application
   //  switch_plot         Switch for quantity to calculate & plot
   //                         0. Separate secondary source term (before propagation)
   //                         1. Separate secondary contributions to total production
   //                         2. Separate secondaries from different parent energy ranges
   //                         3. Separate tertiaries contributions parent energy ranges
   //  is_noina            If switch_plot=4, set to 0 sigTotINA is is_noina=true
   //  option              Option name in usine (to print)
   //  e_type              E-type (kEKN, kR, kEK, or kETOT) in which to display result
   //  is_test             Whether run in test mode
   //  f_test              File to output result of test


   FILE *fout_ref = fOutputLog;
   if (is_test)
      fOutputLog = f_test;

   if (switch_plot == 0)
      TUMessages::Separator(f_test,
                            option + ". Separate source terms (before propag) for secondary diff. prod. (all reactions)");
   else if (switch_plot == 1)
      TUMessages::Separator(f_test,
                            option + ". Separate contribs (after propag) for secondary diff. prod. species (all reactions)");
   else if (switch_plot == 2)
      TUMessages::Separator(f_test,
                            option + ".Separate secondary contribs (differential production) per parent energy range");
   else if (switch_plot == 3) {
      TUMessages::Separator(f_test,
                            option + ". Check tertiary contributions (non-annihilating rescattering) for antinuclei");
      if (is_noina)
         fprintf(f_test, "   N.B.: sigINAtot is set to zero in order to always have a positive tertiary contribution\n");
      else
         fprintf(f_test, "   N.B.: as sigINAtot is non-zero, at high energy the tertiary contribution is negative\n");
   }

   // Check at least one model (warning if more than one model): only calculate for model 0
   if (fNPropagModels == 0) {
      TUMessages::Warning(fOutputLog, "TURunPropagation", "Plots_AntinucExtra",
                          "No propagation model loaded");
      // Reset reference output file
      fOutputLog = fout_ref;
      fout_ref = NULL;
      return;
   } else if (fNPropagModels > 1) {
      string message = "Display result for first propagation model only ("
                       + fPropagModels[0]->GetModelName() + ")";
      TUMessages::Warning(fOutputLog, "TURunPropagation", "Plots_AntinucExtra", message);
   }

   // Extract list of differential cross-sections for all families
   Int_t n_diffprod = 0;
   vector<Int_t> diffprod_crs[gN_CRFAMILY];
   vector<vector<Int_t> > diffprod_parents[gN_CRFAMILY];
   vector<gENUM_CRFAMILY> diffprod_family;
   for (Int_t i = 0; i < gN_CRFAMILY; ++i) {
      fPropagModels[0]->ExtractDiffProdSpecies(diffprod_crs[i],
            diffprod_parents[i], TUEnum::gENUM_CRFAMILY_LIST[i]);
      if (diffprod_crs[i].size() != 0)
         diffprod_family.push_back(TUEnum::gENUM_CRFAMILY_LIST[i]);
      n_diffprod += diffprod_crs[i].size();
   }
   // If no differential production, nothing to do
   if (diffprod_family.size() == 0) {
      string message  = "No production by differential cross-sections: nothing to display";
      TUMessages::Warning(fOutputLog, "TURunPropagation", "Plots_AntinucExtra", message);
      // Reset reference output file
      fOutputLog = fout_ref;
      fout_ref = NULL;
      return;
   }

   // Canvas and graphs
   TCanvas **c_check = new TCanvas*[n_diffprod];
   TMultiGraph **mg = new TMultiGraph*[n_diffprod];
   TLegend **leg = new TLegend*[n_diffprod];
   TCanvas **c_check_frac = new TCanvas*[n_diffprod];
   TMultiGraph **mg_frac = new TMultiGraph*[n_diffprod];
   TLegend **leg_frac = new TLegend*[n_diffprod];
   for (Int_t i = 0; i < n_diffprod; ++i) {
      c_check[i] = NULL;
      mg[i] = NULL;
      leg[i] = NULL;
      c_check_frac[i] = NULL;
      mg_frac[i] = NULL;
      leg_frac[i] = NULL;
   }

   Int_t ne = fPropagModels[0]->GetNE();
   string model = fPropagModels[0]->GetModelName();
   string name_etype = TUEnum::Enum2Name(e_type);

   // Local ISM medium
   TUMediumTXYZ *ism = fPropagModels[0]->GetISM();
   ism->FillMediumEntry(fPropagModels[0]->GetSolarSystemCoords());
   TUMediumEntry *local_ism = ism->GetMediumEntry();
   //local_ism->Print();
   ism = NULL;

   // Calculation dependent quantities
   TUXSections *xsec = NULL;
   if (switch_plot == 1 || switch_plot == 2 || switch_plot == 3)
      xsec = new TUXSections(*fPropagModels[0]);


   string indent = TUMessages::Indent(true);
   TUMisc::RootSetStylePlots();
   TText *usine_txt = TUMisc::OrphanUSINEAdOnPlots();

   Propagate(fIsUseNormList, true);

   Int_t i_diffprod = -1;
   // Loop on family for which differential production exists
   for (Int_t f = 0; f < (Int_t)diffprod_family.size(); ++f) {
      gENUM_CRFAMILY i_family = diffprod_family[f];
      Int_t n_crnuc = diffprod_crs[i_family].size();

      // If option 3 selected, applied only to anti-nuclei
      if (switch_plot == 3 && i_family != kANTINUC) {
         i_diffprod += n_crnuc;
         continue;
      }

      // 'CR-family'-related quantities
      string family = TUEnum::Enum2Name(i_family);
      TUAxis *e_family = fPropagModels[0]->GetEFamily(i_family);

      // Loop on fragments for this family
      for (Int_t i = 0; i < n_crnuc; ++i) {
         i_diffprod += 1;
         Int_t j_cr = diffprod_crs[i_family][i];
         gENUM_ETYPE e_type_native = fPropagModels[0]->TUAxesCrE::GetEType(j_cr);

         // conversion factors for CR x and y axis into correct e_type
         vector<Double_t> xvals_etype(e_family->GetN()), yscale_etype(e_family->GetN());
         for (Int_t k = 0; k < e_family->GetN(); ++k) {
            xvals_etype[k] = fPropagModels[0]->GetE(j_cr, k, e_type);
            yscale_etype[k] = TUPhysics::dNdEin_to_dNdEout(e_family->GetVal(k), e_type_native, e_type,
                              fPropagModels[0]->GetCREntry(j_cr).GetA(), fPropagModels[0]->GetCREntry(j_cr).GetZ(),
                              fPropagModels[0]->GetCREntry(j_cr).GetmGeV());
         }

         string name_cr = fPropagModels[0]->GetCREntry(j_cr).GetName();
         string name_cr_trim = name_cr;
         TUMisc::RemoveSpecialChars(name_cr_trim);
         fprintf(f_test, "%s   ... process %s [%s]\n", indent.c_str(), name_cr.c_str(), family.c_str());
         string name_cr_root = fPropagModels[0]->GetNameROOT(j_cr);
         // Create canvas for each fragment
         string name = "", title = "";
         if (switch_plot == 0) {
            name = Form("check_source_terms_%s", name_cr_trim.c_str());
            title = Form("Source terms (before propag) for %s [%s]", name_cr_root.c_str(), model.c_str());
         } else if (switch_plot == 1) {
            name = Form("check_secondary_contribs_per_reaction_%s", name_cr_trim.c_str());
            title = Form("Secondaries per reaction for %s [%s]", name_cr_root.c_str(), model.c_str());
         } else if (switch_plot == 2) {
            name = Form("check_secondary_contribs_per_erange_%s", name_cr_trim.c_str());
            title = Form("Secondaries per %s range for %s [%s]", name_etype.c_str(), name_cr_root.c_str(), model.c_str());
         } else if (switch_plot == 3) {
            name = Form("check_tertiary_contribution_per_erange_%s", name_cr_trim.c_str());
            title = Form("Tertiaries per %s range for %s [%s]", name_etype.c_str(), name_cr_root.c_str(), model.c_str());
         }
         c_check[i_diffprod] = new TCanvas(name.c_str(), title.c_str());
         c_check[i_diffprod]->SetWindowPosition(0, i_diffprod * 100);
         //c_check[i_diffprod]->SetGridx();
         //c_check[i_diffprod]->SetGridy();
         c_check[i_diffprod]->SetLogx(1);
         c_check[i_diffprod]->SetLogy(1);
         c_check_frac[i_diffprod] = new TCanvas((name + "_frac").c_str(), title.c_str());
         c_check_frac[i_diffprod]->SetWindowPosition(c_check_frac[i_diffprod]->GetWw(), i_diffprod * 100);
         //c_check_frac[i_diffprod]->SetGridx();
         //c_check_frac[i_diffprod]->SetGridy();
         c_check_frac[i_diffprod]->SetLogx(1);
         c_check_frac[i_diffprod]->SetLogy(0);
         // Allocate and fill multigraphs
         string mg_name = "mg_" + name;
         string mg_title = "mg_" + title;
         //cout << mg_name << " " << mg_title << endl;
         mg[i_diffprod] = new TMultiGraph(mg_name.c_str(), mg_title.c_str());
         // set titles (and axes names)
         string x_title = TUEnum::Enum2NameUnit(e_type);
         vector<string> tmp;
         TUMisc::String2List(x_title, "[]", tmp);
         string y_title;
         if (switch_plot == 0)
            y_title = "Source spectra  [m^{-3} s^{-1} (" + tmp[1] + ")^{-1}]";
         else if (switch_plot == 1 || switch_plot == 2 || switch_plot == 3)
            y_title = "Flux  [m^{-2} s^{-1} sr^{-1} (" + tmp[1] + ")^{-1}]";
         string mg_titles = ";" + x_title + ";" + y_title;
         mg[i_diffprod]->SetTitle(mg_titles.c_str());
         leg[i_diffprod] = new TLegend(0.4, 0.6, 0.94, 0.95);
         mg_name = "mg_frac_" + (string)name;
         mg_title = "mg_frac_" + (string)title;
         //cout << mg_name << " " << mg_title << endl;
         mg_frac[i_diffprod] = new TMultiGraph(mg_name.c_str(), mg_title.c_str());
         mg_titles = ";" + x_title + ";" + "Fraction of total [%]";
         mg_frac[i_diffprod]->SetTitle(mg_titles.c_str());
         leg_frac[i_diffprod] = new TLegend(0.4, 0.6, 0.94, 0.95);

         if (switch_plot == 2 || switch_plot == 3) {
            //------------------------//
            // E-range contrib. calc. //
            //------------------------//
            Int_t j_tertiary = fPropagModels[0]->MapCRIndex2INAIndex(j_cr);
            const Int_t n_bool = 2;
            string tert[n_bool] = {"[no tertiary]", "[with tertiaries]"};
            Bool_t is_tert[n_bool] = {false, true};
            Bool_t is_tert_ref = fPropagModels[0]->IsTertiaries();

            // Set n energy ranges on which tertiary contribution is calculated
            // -> [0 - ekn1], [ekn1 - ekn2] ... [eknlast - infinity]
            string name_switch = "";
            vector<Double_t> ekn_vals;
            ekn_vals.push_back(0.);
            if (switch_plot == 2) {
               tert[0] = "[total secondaries]";
               name_switch = "fluxISsecondaries";
               const Int_t n_tmp = 7;
               Double_t ekn_tmp[n_tmp] = {10., 3.16e1, 1.e2, 3.16e2, 1.e3, 3.16e3, 1.e4};
               // We use kNUC for parents of secondary production,
               // because they are the most likely contributors
               TUAxis *ekn_nuclei = fPropagModels[0]->GetEFamily(kNUC);
               Double_t ekn_max = ekn_nuclei->GetVal(ne - 1);
               for (Int_t k = 0; k < n_tmp; ++k) {
                  Int_t k_test = ekn_nuclei->IndexClosest(ekn_tmp[k]);
                  if (k_test >= 0 && ekn_nuclei->GetVal(k_test) < ekn_max)
                     ekn_vals.push_back(ekn_nuclei->GetVal(k_test));
               }
               if (ekn_tmp[n_tmp - 1] < ekn_max)
                  ekn_vals.push_back(ekn_max);
               ekn_nuclei = NULL;
            } else if (switch_plot == 3) {
               fPropagModels[0]->MapCRIndex2INAIndex(j_cr);
               name_switch = "fluxIStertiaries";
               const Int_t n_tmp = 4;
               Double_t ekn_tmp[n_tmp] = {2., 5., 10., 20.};
               Double_t ekn_max = e_family->GetVal(ne - 1);
               for (Int_t k = 0; k < n_tmp; ++k) {
                  Int_t k_test = e_family->IndexClosest(ekn_tmp[k]);
                  if (k_test >= 0 && e_family->GetVal(k_test) < ekn_max)
                     ekn_vals.push_back(e_family->GetVal(k_test));
               }
               if (ekn_tmp[n_tmp - 1] < ekn_max)
                  ekn_vals.push_back(ekn_max);
            }

            // Get flux w/wo tertiaries
            vector<Double_t> flux_ref[n_bool];
            for (Int_t is = 0; is < 2; ++is) {
               // For secondaries, do not need to loop twice
               if (switch_plot == 2 && is != 0)
                  continue;
               else
                  fprintf(f_test, "%s     %s\n", indent.c_str(), tert[is].c_str());


               // Set tertiaries off/on
               if (switch_plot == 3)
                  fPropagModels[0]->SetIsTertiaries(is_tert[is]);

               // Do propagation
               fPropagModels[0]->SetPropagated(false);
               Propagate(false, false);

               // Flux (keep first calculation as reference)
               fPropagModels[0]->FillFlux(flux_ref[is], j_cr, fPropagModels[0]->GetSolarSystemCoords(), 0);

               // Add in plots
               TGraph *gr = NULL;
               if (e_type == e_type_native)
                  gr = new TGraph(ne, e_family->GetVals(), &flux_ref[is][0]);
               else {
                  // convert for CR x and y axis into correct e_type
                  vector<Double_t> y4etype;
                  for (Int_t k = 0; k < e_family->GetN(); ++k)
                     y4etype.push_back(flux_ref[is][k]*yscale_etype[k]);
                  gr = new TGraph(ne, &xvals_etype[0], &y4etype[0]);
               }
               string gr_name = name_switch + "_" + model + "_" + name_etype + "_" + name_cr;
               TUMisc::RemoveSpecialChars(gr_name);
               gr->SetName(gr_name.c_str());
               gr->SetLineColor(kBlack);
               gr->SetLineStyle(2 - (Int_t)fPropagModels[0]->IsTertiaries());
               gr->SetLineWidth(2);
               mg[i_diffprod]->Add(gr, "L");
               string leg_tert = fPropagModels[0]->GetNameROOT(j_cr);
               TLegendEntry *entry = leg[i_diffprod]->AddEntry(gr, (leg_tert + " " + tert[is]).c_str(), "L");
               entry->SetTextColor(kBlack);
               entry = NULL;
               // save in file
               if (!is_test) {
                  string gr_filename = GetOutputDir() + "/" + gr_name + ".out";
                  printf(" ... save in %s\n", gr_filename.c_str());
                  FILE *fp = fopen(gr_filename.c_str(), "w");
                  TUMisc::PrintUSINEHeader(fp, true);
                  TUMisc::Print(fp, gr, 0);
                  fclose(fp);
               }

               // evaluate for three energy intervals if:
               //    - switch_plot=3 and both w/o tertiary calculation done
               //    - switch_plot=2
               if ((is == 1 && switch_plot == 3) || switch_plot == 2) {

                  // If tertiaries, stores relative difference between w/wo tertiaries
                  if (switch_plot == 3) {
                     // Tertiaries alone
                     vector<Double_t> diff_tert;
                     for (Int_t k = 0; k < ne; ++k)
                        diff_tert.push_back(flux_ref[1][k] - flux_ref[0][k]);
                     gr = new TGraph(ne, &xvals_etype[0], &diff_tert[0]);
                     gr_name = name_switch + "_" + model + "_" + name_etype + "_" + name_cr + "_tertiary_only";
                     TUMisc::RemoveSpecialChars(gr_name);
                     gr->SetName(gr_name.c_str());
                     gr->SetLineColor(kGray + 1);
                     gr->SetLineStyle(1);
                     gr->SetLineWidth(2);
                     mg[i_diffprod]->Add(gr, "L");
                     entry = leg[i_diffprod]->AddEntry(gr, "Tertiary only [with - without]", "L");
                     entry->SetTextColor(kGray + 1);
                     // save in file
                     if (!is_test) {
                        string gr_filename = gr_name + ".out";
                        printf(" ... save in %s\n", gr_filename.c_str());
                        FILE *fp = fopen(gr_filename.c_str(), "w");
                        TUMisc::PrintUSINEHeader(fp, true);
                        TUMisc::Print(fp, gr, 0);
                        fclose(fp);
                     }

                     // Relative difference w/wo tertiaries
                     vector<Double_t> rel_diff_tert;
                     for (Int_t k = 0; k < ne; ++k) {
                        if (fabs(flux_ref[0][k]) > 1.e-40)
                           rel_diff_tert.push_back((flux_ref[1][k] - flux_ref[0][k]) / flux_ref[0][k] * 100.);
                        else
                           rel_diff_tert.push_back(0.);
                     }
                     gr = new TGraph(ne, &xvals_etype[0], &rel_diff_tert[0]);
                     gr_name = name_switch + "_" + model + "_" + name_etype + "_" + name_cr + "_frac";
                     TUMisc::RemoveSpecialChars(gr_name);
                     gr->SetName(gr_name.c_str());
                     gr->SetLineColor(TUMisc::RootColor(0));
                     gr->SetLineStyle(1);
                     gr->SetLineWidth(2);
                     mg_frac[i_diffprod]->Add(gr, "L");
                     entry = leg_frac[i_diffprod]->AddEntry(gr, "(y_{w}-y_{w/o})/y_{w/o} [%]", "L");
                     entry->SetTextColor(TUMisc::RootColor(0));
                     // Save in file
                     if (!is_test) {
                        string gr_filename = GetOutputDir() + "/" + gr_name + ".out";
                        printf(" ... save in %s\n", gr_filename.c_str());
                        FILE *fp = fopen(gr_filename.c_str(), "w");
                        TUMisc::PrintUSINEHeader(fp, true);
                        TUMisc::Print(fp, gr, 0);
                        fclose(fp);
                     }
                  }

                  // We calculate the secondary (resp. tertiary ) contributions,
                  // switching-off secondary production (resp. energy redistributions)
                  // for each interval in turn
                  vector<vector<Double_t> >fluxes_range(ekn_vals.size() - 1, vector<Double_t>(ne, 0.));
                  vector<Double_t> flux_sum(ne, 0.);
                  vector<string> range_leg_save, range_name_save;
                  for (Int_t ii = 0; ii < (Int_t)ekn_vals.size() - 1; ++ii) {
                     // Range
                     string range_name = "", range_leg = "";
                     if (switch_plot == 2) {
                        range_name = Form("sec_from_%.2f_to_%.2f_GeVn", ekn_vals[ii], ekn_vals[ii + 1]);
                        range_leg = Form("From parents in [%.2f - %.2f] GeV/n", ekn_vals[ii], ekn_vals[ii + 1]);
                     } else if (switch_plot == 3) {
                        if (is_noina) {
                           range_name = Form("tert_from_%.2f_to_%.2f_GeVn_sigtotinazero", ekn_vals[ii], ekn_vals[ii + 1]);
                           range_leg = Form("Tert. from [%.2f - %.2f] GeV/n, #sigma^{INA}_{tot}=0", ekn_vals[ii], ekn_vals[ii + 1]);
                        } else {
                           range_name = Form("tert_from_%.2f_to_%.2f_GeVn", ekn_vals[ii], ekn_vals[ii + 1]);
                           range_leg = Form("Tert. from [%.2f - %.2f] GeV/n", ekn_vals[ii], ekn_vals[ii + 1]);
                        }
                     }
                     range_name_save.push_back(range_name);
                     range_leg_save.push_back(range_leg);

                     // Reset X-sections
                     fPropagModels[0]->TUXSections::Copy(*xsec);

                     if (switch_plot == 2) {
                        Int_t g_max = -1;
                        if (fPropagModels[0]->IsGhostsLoaded())
                           g_max = fPropagModels[0]->GetCREntry(j_cr).GetNGhosts() - 1;

                        // Set secondary production only for parents in E range
                        for (Int_t j = 0; j < (Int_t)diffprod_parents[i_family][i].size(); ++j) {
                           Int_t j_parent = diffprod_parents[i_family][i][j];
                           Int_t j_parent_in_loc =  fPropagModels[0]->IndexInCRList_Parent(j_cr, j_parent);
                           for (Int_t t = 0; t < fPropagModels[0]->GetNTargets(); ++t) {
                              //cout << fPropagModels[0]->GetName(j_cr,j_parent)
                              //     //<< " (" << fPropagModels[0]->GetCREntry(j_parent_in_loc).GetName() << ")"
                              //     << " + " << fPropagModels[0]->GetNameTarget(t)
                              //     << " -> " << fPropagModels[0]->GetCREntry(j_cr).GetName() << endl;
                              for (Int_t k_in = 0; k_in < ne; ++k_in) {
                                 Double_t ekn_tmp = fPropagModels[0]->TUAxesCrE::GetEkn(j_parent_in_loc, k_in);
                                 if (ekn_tmp >= ekn_vals[ii] && ekn_tmp <= ekn_vals[ii + 1])
                                    continue;
                                 for (Int_t g_ghost = g_max; g_ghost >= -1; --g_ghost) {
                                    for (Int_t k_out = 0; k_out < ne; ++k_out)
                                       fPropagModels[0]->SetProdDSigDEk(j_parent, t, j_cr, k_in, k_out, 0., g_ghost);
                                 }
                              }
                           }
                        }
                     } else if (switch_plot == 3) {
                        // Set INA non-zero only in the desired interval
                        for (Int_t t = 0; t < fPropagModels[0]->GetNTargets(); ++t) {
                           for (Int_t k_out = 0; k_out < ne; ++k_out) {
                              Double_t ekn_out = e_family->GetVal(k_out);
                              // Reset tot.inel. to zero if necessary
                              if (is_noina)
                                 fPropagModels[0]->SetTotINA(j_tertiary, t, k_out, 0.);
                              else {
                                 if (ekn_out < ekn_vals[ii] || ekn_out > ekn_vals[ii + 1]) {
                                    // Total inelastic = annihil. + non-annihil.
                                    Double_t tot_ia = fPropagModels[0]->GetTotIA(j_cr, t, k_out)
                                                      + fPropagModels[0]->GetTotINA(j_tertiary, t, k_out);
                                    fPropagModels[0]->SetTotINA(j_tertiary, t, k_out, 0.);
                                    fPropagModels[0]->SetTotIA(j_cr, t, k_out, tot_ia);
                                 }
                              }
                              // Set to zero DSigDEkINA
                              for (Int_t k_in = 0; k_in < ne; ++k_in) {
                                 Double_t ekn_in = e_family->GetVal(k_in);
                                 if (ekn_in < ekn_vals[ii] || ekn_in > ekn_vals[ii + 1])
                                    fPropagModels[0]->SetDSigDEkINA(j_tertiary, t, k_in, k_out, 0.);
                              }
                           }
                        }
                     }
                     // Update ISM0D (to local value)
                     fPropagModels[0]->FillInterRate_ISM0D(local_ism, true);

                     // Do propagation
                     fprintf(f_test, "%s      - Contrib. from E-range = [%le,%le]\n",
                             indent.c_str(), ekn_vals[ii], ekn_vals[ii + 1]);

                     fPropagModels[0]->SetPropagated(false);
                     Propagate(false, false);

                     // Get flux
                     fPropagModels[0]->FillFlux(fluxes_range[ii], j_cr, fPropagModels[0]->GetSolarSystemCoords(), 2);
                     for (Int_t k = 0; k < ne; ++k) {
                        // Tertiary contribution is current calculation - flux no tertiaries
                        if (switch_plot == 3)
                           fluxes_range[ii][k] = fluxes_range[ii][k] - flux_ref[0][k];
                        flux_sum[k] += fluxes_range[ii][k];
                     }

                     if (e_type == e_type_native)
                        gr = new TGraph(ne, e_family->GetVals(), &fluxes_range[ii][0]);
                     else {
                        vector<Double_t> y4etype;
                        for (Int_t k = 0; k < ne; ++k)
                           y4etype.push_back(fluxes_range[ii][k]*yscale_etype[k]);
                        gr = new TGraph(ne, &xvals_etype[0], &y4etype[0]);
                     }
                     gr_name = name_switch + "_" + model + "_" + name_etype + "_" + name_cr + "_" + range_name;
                     TUMisc::RemoveSpecialChars(gr_name);
                     gr->SetName(gr_name.c_str());
                     gr->SetLineColor(TUMisc::RootColor(ii + 1));
                     gr->SetLineStyle(ii + 3);
                     gr->SetLineWidth(3);
                     //cout << gr_name << endl;
                     //gr->Print();
                     mg[i_diffprod]->Add(gr, "L");
                     entry = leg[i_diffprod]->AddEntry(gr, range_leg.c_str(), "L");
                     entry->SetTextColor(TUMisc::RootColor(ii + 1));
                     entry = NULL;
                     // Save in file
                     if (!is_test) {
                        string gr_filename = GetOutputDir() + "/" + gr_name + ".out";
                        printf(" ... save in %s\n", gr_filename.c_str());
                        FILE *fp = fopen(gr_filename.c_str(), "w");
                        TUMisc::PrintUSINEHeader(fp, true);
                        TUMisc::Print(fp, gr, 0);
                        fclose(fp);
                     }
                  }

                  // Fill multigraph for fraction of total
                  entry = leg_frac[i_diffprod]->AddEntry((TObject *)0, "y_{in range}/y_{tot} [%]", "");
                  entry->SetTextColor(kGray + 1);
                  for (Int_t ii = 0; ii < (Int_t)ekn_vals.size() - 1; ++ii) {
                     vector<Double_t> rel_diff;
                     for (Int_t k = 0; k < ne; ++k) {
                        if ((switch_plot == 3 && fabs(flux_sum[k]) > 1.e-40 && flux_sum[k] > 0
                              && fluxes_range[ii][k] > 0 && flux_sum[k] > fluxes_range[ii][k])
                              || switch_plot == 2)
                           rel_diff.push_back(fluxes_range[ii][k] / flux_sum[k] * 100.);
                        else
                           rel_diff.push_back(0.);
                     }
                     gr = new TGraph(ne, &xvals_etype[0], &rel_diff[0]);
                     gr_name = name_switch + "_" + model + "_" + name_etype + "_" + name_cr + "_" + range_name_save[ii] + "_frac";
                     TUMisc::RemoveSpecialChars(gr_name);
                     gr->SetName(gr_name.c_str());
                     gr->SetLineColor(TUMisc::RootColor(ii + 1));
                     gr->SetLineStyle(ii + 3);
                     gr->SetLineWidth(3);
                     mg_frac[i_diffprod]->Add(gr, "L");
                     entry = leg_frac[i_diffprod]->AddEntry(gr, range_leg_save[ii].c_str(), "L");
                     entry->SetTextColor(TUMisc::RootColor(ii + 1));
                     entry = NULL;
                     // Save in file
                     if (!is_test) {
                        string gr_filename = GetOutputDir() + "/" + gr_name + ".out";
                        printf(" ... save in %s\n", gr_filename.c_str());
                        FILE *fp = fopen(gr_filename.c_str(), "w");
                        TUMisc::PrintUSINEHeader(fp, true);
                        TUMisc::Print(fp, gr, 0);
                        fclose(fp);
                     }
                  }
                  // Add total tertiary in plot
                  if (switch_plot == 3) {
                     if (e_type == e_type_native)
                        gr = new TGraph(ne, e_family->GetVals(), &flux_sum[0]);
                     else {
                        vector<Double_t> y4etype;
                        for (Int_t k = 0; k < ne; ++k)
                           y4etype.push_back(flux_sum[k]*yscale_etype[k]);
                        gr = new TGraph(ne, &xvals_etype[0], &y4etype[0]);
                     }
                     gr_name = "fluxIStertiaries_" + model + "_" + name_etype + "_" + name_cr + "_all";
                     TUMisc::RemoveSpecialChars(gr_name);
                     gr->SetName(gr_name.c_str());
                     gr->SetLineColor(kGray + 1);
                     gr->SetLineStyle(1);
                     gr->SetLineWidth(1);
                     mg[i_diffprod]->Add(gr, "L");
                     entry = leg[i_diffprod]->AddEntry(gr, "#sum (over ranges)", "L");
                     entry->SetTextColor(kGray + 1);
                     entry = NULL;
                     // Save in file
                     if (!is_test) {
                        string gr_filename = GetOutputDir() + "/" + gr_name + ".out";
                        printf(" ... save in %s\n", gr_filename.c_str());
                        FILE *fp = fopen(gr_filename.c_str(), "w");
                        TUMisc::PrintUSINEHeader(fp, true);
                        TUMisc::Print(fp, gr, 0);
                        fclose(fp);
                     }
                  }
               }
            }

            // Reset Xsec to initial value
            fPropagModels[0]->TUXSections::Copy(*xsec);
            if (switch_plot == 3)
               fPropagModels[0]->SetIsTertiaries(is_tert_ref);
         } else if (switch_plot == 0 || switch_plot == 1) {
            //-------------------------//
            // Src, Sec contrib. calc. //
            //-------------------------//

            // Loop on parents for this fragment
            vector<vector<vector<Double_t> > > res_par_targ;
            vector<Double_t> res_tot;
            res_tot.assign(ne, 0.);
            Int_t n_parents = diffprod_parents[i_family][i].size();
            for (Int_t j = 0; j < n_parents; ++j) {
               Int_t j_parent = diffprod_parents[i_family][i][j];
               Int_t j_parent_in_loc =  fPropagModels[0]->IndexInCRList_Parent(j_cr, j_parent);
               string name_parent = fPropagModels[0]->GetCREntry(j_parent_in_loc).GetName();

               // Get local flux [m2 s sr GeV/n]^-1 for parent (all models)
               // and convert it to dN/dE=4pi/v*flux [m3 GeV/n]^-1
               vector<Double_t> dnde_parent;
               if (switch_plot == 0) {
                  fPropagModels[0]->FillFlux(dnde_parent, j_parent_in_loc, fPropagModels[0]->GetSolarSystemCoords(), 0);
                  for (Int_t k = 0; k < ne; ++k)
                     dnde_parent[k] *= 4.*TUMath::Pi() / (fPropagModels[0]->GetBeta(j_parent_in_loc, k) * TUPhysics::C_mpers());
               }

               // Loop on targets
               vector<vector<Double_t> > res_targ;
               for (Int_t t = 0; t < fPropagModels[0]->GetNTargets(); ++t) {
                  vector<Double_t> result(ne, 0.);

                  // Set to zero all ISM elements, but for current target
                  // (mostly used for source term calculation)
                  TUMediumEntry medium;
                  medium.Copy(*local_ism);
                  string target = fPropagModels[0]->GetNameTarget(t);
                  string leg_target = "";
                  for (Int_t m = 0; m < medium.GetNMediumElements(); ++m) {
                     if (medium.GetNameMediumElement(m) != target)
                        medium.SetDensity(m, 0.);
                     else
                        leg_target = Form("%s (%.2f cm^{-3})", target.c_str(), medium.GetDensity(m));
                  }
                  fprintf(f_test, "%s      - Contrib. from %s + %s\n",
                          indent.c_str(), name_parent.c_str(), target.c_str());

                  vector<Double_t> res_tmp;
                  //-------------------------//
                  // Source term calculation //
                  //-------------------------//
                  if (switch_plot == 0) {
                     fPropagModels[0]->SecondaryProduction(&dnde_parent[0], j_parent, j_cr, &medium, &result[0], fPropagModels[0]->GetEpsIntegr(), false/*is_ism0D*/, f_test);
                     // N.B.: dnde_parent = [m3 (GeV/n)]^-1
                     //       qterm = [m3 (GeV/n) Myr]^-1 => converted into qterm = [m3 (GeV/n) s]^-1
                     for (Int_t k = 0; k < ne; ++k) {
                        result[k] /= (TUPhysics::Convert_yr_to_s() * 1.e-6);
                        res_tmp.push_back(result[k]);
                        res_tot[k] += result[k];
                     }
                  } else if (switch_plot == 1) {
                     //-------------------------//
                     // Separate contrib. calc. //
                     //-------------------------//
                     // Reset Xsec, and set all other cross-sections (for other parents and targets to zero)
                     Int_t g_max = -1;
                     if (fPropagModels[0]->IsGhostsLoaded())
                        g_max = fPropagModels[0]->GetCREntry(j_cr).GetNGhosts() - 1;
                     fPropagModels[0]->TUXSections::Copy(*xsec);
                     for (Int_t jj = 0; jj < (Int_t)diffprod_parents[i_family][i].size(); ++jj) {
                        Int_t j_parent_test = diffprod_parents[i_family][i][jj];
                        for (Int_t tt = 0; tt < fPropagModels[0]->GetNTargets(); ++tt) {
                           if (tt != t || jj != j) {
                              for (Int_t g_ghost = g_max; g_ghost >= -1; --g_ghost) {
                                 for (Int_t k_in = ne - 1; k_in >= 0; --k_in) {
                                    for (Int_t k_out = ne - 1; k_out >= 0; --k_out)
                                       fPropagModels[0]->SetProdDSigDEk(j_parent_test, tt, j_cr, k_in, k_out, 0., g_ghost);
                                 }
                              }
                           }
                        }
                     }
                     // Update ISM0D (to local value)
                     fPropagModels[0]->FillInterRate_ISM0D(local_ism, true);

                     // Do propagation
                     fPropagModels[0]->SetPropagated(false);
                     Propagate(false, false);

                     // Get fluxes
                     fPropagModels[0]->FillFlux(result, j_cr, fPropagModels[0]->GetSolarSystemCoords(), 2);
                     for (Int_t k = 0; k < ne; ++k) {
                        res_tmp.push_back(result[k]);
                        res_tot[k] += result[k];
                     }

                     // If last time, reset Xsec to initial value
                     if (j == (Int_t)diffprod_parents[i_family][i].size() - 1 && t == fPropagModels[0]->GetNTargets() - 1)
                        fPropagModels[0]->TUXSections::Copy(*xsec);
                  }
                  res_targ.push_back(res_tmp);

                  // Fill graph and legend
                  string contrib_leg = fPropagModels[0]->GetNameROOT(j_parent_in_loc) + "+" + (string)leg_target
                                       + " #rightarrow " + fPropagModels[0]->GetNameROOT(j_cr);
                  TGraph *gr = NULL;
                  if (e_type == e_type_native)
                     gr = new TGraph(ne, e_family->GetVals(), &result[0]);
                  else {
                     vector<Double_t> y4etype;
                     for (Int_t k = 0; k < ne; ++k)
                        y4etype.push_back(result[k]*yscale_etype[k]);
                     gr = new TGraph(ne, &xvals_etype[0], &y4etype[0]);
                  }
                  string gr_name = "qsrc_" + model + "_" + name_cr + "_from_" + name_parent + "_on_" + target;
                  if (switch_plot == 1)
                     gr_name = "fluxISsecondaries_" + model + "_" + name_cr + "_from_" + name_parent + "_on_" + target;
                  gr_name += "_" + name_etype;
                  TUMisc::RemoveSpecialChars(gr_name);
                  gr->SetName(gr_name.c_str());
                  gr->SetLineColor(TUMisc::RootColor(n_parents - 1 - j + 1));
                  gr->SetLineStyle((j + 1) % 10);
                  gr->SetLineWidth(2 + 2 * (fPropagModels[0]->GetNTargets() - 1 - t));
                  mg[i_diffprod]->Add(gr, "L");
                  TLegendEntry *entry = leg[i_diffprod]->AddEntry(gr, contrib_leg.c_str(), "L");
                  entry->SetTextColor(TUMisc::RootColor(n_parents - 1 - j + 1));
                  entry = NULL;
                  // Save in file
                  if (!is_test) {
                     string gr_filename = GetOutputDir() + "/" + gr_name + ".out";
                     printf(" ... save in %s\n", gr_filename.c_str());
                     FILE *fp = fopen(gr_filename.c_str(), "w");
                     TUMisc::PrintUSINEHeader(fp, true);
                     TUMisc::Print(fp, gr, 0);
                     fclose(fp);
                  }
               }
               res_par_targ.push_back(res_targ);
            }

            // Fill graph for relative difference
            for (Int_t j = 0; j < n_parents; ++j) {
               Int_t j_parent = diffprod_parents[i_family][i][j];
               Int_t j_parent_in_loc =  fPropagModels[0]->IndexInCRList_Parent(j_cr, j_parent);
               string name_parent = fPropagModels[0]->GetCREntry(j_parent_in_loc).GetName();

               // Loop on targets
               for (Int_t t = 0; t < fPropagModels[0]->GetNTargets(); ++t) {
                  string target = fPropagModels[0]->GetNameTarget(t);
                  for (Int_t k = 0; k < ne; ++k) {
                     if (res_tot[k] > 1.e-30)
                        res_par_targ[j][t][k] = res_par_targ[j][t][k] / res_tot[k] * 100.;
                     else
                        res_par_targ[j][t][k] = 0.;
                  }
                  // Fill graph and legend
                  string contrib_leg = fPropagModels[0]->GetNameROOT(j_parent_in_loc) + "+" + target
                                       + " #rightarrow " + fPropagModels[0]->GetNameROOT(j_cr);
                  TGraph *gr = new TGraph(ne, &xvals_etype[0], &res_par_targ[j][t][0]);
                  string gr_name = "qsrc_" + model + "_" + name_cr + "_from_" + name_parent + "_on_" + target + "_frac";
                  if (switch_plot == 1)
                     gr_name = "fluxISsecondaries_" + model + "_" + name_cr + "_from_" + name_parent + "_on_" + target + "_frac";
                  gr_name += "_" + name_etype;
                  TUMisc::RemoveSpecialChars(gr_name);
                  gr->SetName(gr_name.c_str());
                  gr->SetLineColor(TUMisc::RootColor(n_parents - 1 - j + 1));
                  gr->SetLineStyle((j + 1) % 10);
                  gr->SetLineWidth(2 + 2 * (fPropagModels[0]->GetNTargets() - 1 - t));
                  mg_frac[i_diffprod]->Add(gr, "L");
                  TLegendEntry *entry = leg_frac[i_diffprod]->AddEntry(gr, contrib_leg.c_str(), "L");
                  entry->SetTextColor(TUMisc::RootColor(n_parents - 1 - j + 1));
                  entry = NULL;
                  // Save in file
                  if (!is_test) {
                     string gr_filename = GetOutputDir() + "/" + gr_name + ".out";
                     printf(" ... save in %s\n", gr_filename.c_str());
                     FILE *fp = fopen(gr_filename.c_str(), "w");
                     TUMisc::PrintUSINEHeader(fp, true);
                     TUMisc::Print(fp, gr, 0);
                     fclose(fp);
                  }
               }
            }
         }
         // Display
         c_check[i_diffprod]->cd();
         mg[i_diffprod]->Draw("AL");
         mg[i_diffprod]->GetXaxis()->CenterTitle();
         mg[i_diffprod]->GetYaxis()->CenterTitle();
         if (is_test)
            TUMisc::Print(f_test, mg[i_diffprod], 0);
         usine_txt->Draw();
         leg[i_diffprod]->Draw();
         c_check[i_diffprod]->Modified();
         c_check[i_diffprod]->Update();

         // Display
         c_check_frac[i_diffprod]->cd();
         mg_frac[i_diffprod]->Draw("AL");
         mg_frac[i_diffprod]->GetXaxis()->CenterTitle();
         mg_frac[i_diffprod]->GetYaxis()->CenterTitle();
         if (is_test)
            TUMisc::Print(f_test, mg_frac[i_diffprod], 0);
         usine_txt->Draw();
         leg_frac[i_diffprod]->Draw();
         c_check_frac[i_diffprod]->Modified();
         c_check_frac[i_diffprod]->Update();
      }
      e_family = NULL;
   }

   // Enter event loop, one can now interact with the objects in
   // the canvas. Select "Exit ROOT" from Canvas "File" menu to exit
   // the event loop and execute the next statements.
   if (!gROOT->IsBatch())
      app->Run(kTRUE);


   // Free memory
   delete usine_txt;
   local_ism = NULL;
   if (xsec)
      delete xsec;
   for (Int_t i = 0; i < n_diffprod; ++i) {
      if (c_check[i]) delete c_check[i];
      c_check[i] = NULL;
      if (mg[i]) delete mg[i];
      mg[i] = NULL;
      if (leg[i]) delete leg[i];
      leg[i] = NULL;
      if (c_check_frac[i]) delete c_check_frac[i];
      c_check_frac[i] = NULL;
      if (mg_frac[i]) delete mg_frac[i];
      mg_frac[i] = NULL;
      if (leg_frac[i]) delete leg_frac[i];
      leg_frac[i] = NULL;
   }
   delete[] c_check;
   delete[] mg;
   delete[] leg;
   delete[] c_check_frac;
   delete[] mg_frac;
   delete[] leg_frac;

   // Reset reference output file
   fOutputLog = fout_ref;
   fout_ref = NULL;

   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TURunPropagation::Plots_DecayOnOff(TApplication *app, Bool_t is_beta_or_ec,
                                        Bool_t is_test_exactmatch, string const &option,
                                        gENUM_ETYPE e_type, Bool_t is_test, FILE *f_test)
{
   //--- Prints and plots for checks related to unstable nuclei (beta and EC-decay).
   //    The spirit of the test is that all species that decay must re-appear in
   //    equal amount as daughters. Note however that three conditions must be
   //    enforced to ensure a perfect match: (i) switch off all E redistributions;
   //    (ii) set (A,Z,m) of daughter equals to (A,Z,m) of its parent (to ensure
   //    same rigidity in propagation); (iii) set destruction cross-section for
   //    parent and daughter to same value; (iv) no local under-density. If a
   //    perfect match is then not achieved, the coding of the model is wrong!
   //
   //  app                 ROOT application
   //  is_beta_or_ec       Whether to test beta-decay or EC-decay
   //  is_test_exactmatch  If true, enforce the three conditions as stated above
   //  option              Option name in usine (to print)
   //  e_type              E-type (kEKN, kR, kEK, or kETOT) in which to display result
   //  is_test             Whether run in test mode
   //  f_test              File to output result of test

   FILE *fout_ref = fOutputLog;
   if (is_test)
      fOutputLog = f_test;

   const Int_t n = 2;
   string decay[n] = {"EC-decay", "BETA-decay"};
   string decay_root[n] = {"Unstable EC", "Unstable #beta"};
   string match[n] = {"", "[match]"};

   string separator = option + ". Check appearance/disappearance of " + decay[is_beta_or_ec] + "species";
   TUMessages::Separator(f_test, separator);

   Double_t rhole_ref = 0.;
   if (is_test_exactmatch) {
      fprintf(f_test, "   N.B.: perfect match between disappeared/appeared fluxes is expected\n");
      fprintf(f_test, "         because the following conditions are enforced:\n");

      if (fPropagModels[0]->GetModel() == kMODEL1DKISOVC || fPropagModels[0]->GetModel() == kMODEL2DKISOVC) {
         Int_t i_par = fPropagModels[0]->IndexPar("rhole");
         rhole_ref = fPropagModels[0]->GetParEntry(i_par)->GetVal(false);
         fPropagModels[0]->GetParEntry(i_par)->SetVal(0., false);
      }
   } else {
      fprintf(f_test, "   N.B.: perfect match between disappeared/appeared fluxes not expected\n");
      fprintf(f_test, "         because the following conditions are not met:\n");
   }
   fprintf(f_test, "        (i) all energy redistributions switched off;\n");
   fprintf(f_test, "        (ii) (A,Z,m) of daughter equals (A,Z,m) of its parent (ensure same R in propagation);\n");
   fprintf(f_test, "        (iii) destruction cross-section switched-off (simplest way to have sig_inel(parent=daughter)).\n");
   fprintf(f_test, "        (iv) local under-dense set to 0 if exists and non-null.\n");
   fprintf(f_test, "\n");

   // Check at least one model (warning if more than one model): only calculate for model 0
   if (fNPropagModels == 0) {
      TUMessages::Warning(fOutputLog, "TURunPropagation", "Plots_DecayOnOff", "No propagation model loaded");
      if (is_test) {
         fOutputLog = fout_ref;
         fout_ref = NULL;
      }
      return;
   } else if (fNPropagModels > 1) {
      string message = "Display result for first propagation model only ("
                       + fPropagModels[0]->GetModelName() + ")";
      TUMessages::Warning(fOutputLog, "TURunPropagation", "Plots_DecayOnOff", message);
   }


   // Check at least one unstable
   Int_t n_rad = fPropagModels[0]->GetNUnstables(is_beta_or_ec);
   if (n_rad == 0) {
      TUMessages::Warning(fOutputLog, "TURunPropagation", "Plots_DecayOnOff",
                          "No radioactive species in CR list, nothing to do");
      if (is_test) {
         fOutputLog = fout_ref;
         fout_ref = NULL;
      }
      return;
   }

   string indent = TUMessages::Indent(true);
   Int_t ne = fPropagModels[0]->GetNE();

   // Keep reference propagation mode and enable decay
   TUPropagSwitches *ref_propag = fPropagModels[0]->TUPropagSwitches::Clone();
   fPropagModels[0]->SetIsDecayBETA(true);
   fPropagModels[0]->SetIsDecayFedBETA(true);
   fPropagModels[0]->SetIsDecayEC(true);
   fPropagModels[0]->SetIsDecayFedEC(true);


   // Extract parent/daughter names, but also associated elements
   // and isotopes names.
   // N.B.: quantities related to the parent (resp. daughter)
   // are indexed [0] (resp. [1])
   vector<string> unstable_names[n];
   vector<string> unstable_names_root[n];
   vector<string> elements[n];
   vector<vector<Int_t> > isotopes[n];
   vector<vector<string> > isotopes_names[n];
   vector<vector<string> > isotopes_names_root[n];
   vector<string> isotopes_list_root[n];

   // Save reference properties of parent/daughter CR entries
   // and create addition entries for the associated "STABLE" set to stable)
   vector<Int_t> j_rad[n];
   vector<TUCREntry> parents;
   vector<TUCREntry> daughters, daughters_match;
   // Loop on unstables
   //fPropagModels[0]->PrintParents();
   for (Int_t p = 0; p < n_rad; ++p) {
      // 1. Save reference CR unstables and daughters
      j_rad[0].push_back(fPropagModels[0]->IndexDecayParent(p, is_beta_or_ec));
      TUCREntry tmp = fPropagModels[0]->GetCREntry(j_rad[0][p]);
      parents.push_back(tmp);
      j_rad[1].push_back(fPropagModels[0]->IndexDecayDaughter(p, is_beta_or_ec));
      if (j_rad[1][p] < 0) {
         string name_daughter_or_prog = "";
         if (is_beta_or_ec)
            name_daughter_or_prog = tmp.GetBETACRFriend();
         else
            name_daughter_or_prog = tmp.GetECCRFriend();
         string message = "Please change lighter CR in list to include " + name_daughter_or_prog;
         TUMessages::Error(fOutputLog, "TURunPropagation", "Plots_DecayOnOff", message);
      }
      daughters.push_back(fPropagModels[0]->GetCREntry(j_rad[1][p]));
      daughters_match.push_back(fPropagModels[0]->GetCREntry(j_rad[1][p]));

      Int_t j_parent = fPropagModels[0]->IndexInParentList(j_rad[1][p], fPropagModels[0]->GetCREntry(j_rad[1][p]).GetName());
      if (j_parent >= 0)
         fprintf(fOutputLog, " ***** There is secondary production for %s -> %s\n",
                 fPropagModels[0]->GetCREntry(j_rad[0][p]).GetName().c_str(),
                 fPropagModels[0]->GetCREntry(j_rad[1][p]).GetName().c_str());


      // 2. Extract names (must be done before changing A and Z of a nucleus).
      // Loop on parent/daughter
      for (Int_t i = 0; i < n; ++i) {
         vector<Int_t> indices;
         vector<string> names;
         unstable_names[i].push_back(fPropagModels[0]->GetCREntry(j_rad[i][p]).GetName());
         unstable_names_root[i].push_back(fPropagModels[0]->GetNameROOT(j_rad[i][p]));
         string elem = TUMisc::FormatCRQty(fPropagModels[0]->CRToElementName(j_rad[i][p]), 2);
         elements[i].push_back(elem);
         fPropagModels[0]->ElementNameToCRIndices(elements[i][p], indices, !is_test);
         isotopes[i].push_back(indices);
         fPropagModels[0]->ElementNameToCRNames(elements[i][p], names, false);
         isotopes_names[i].push_back(names);
         names.clear();
         string list = "";
         for (Int_t m = 0; m < (Int_t)indices.size(); ++m) {
            names.push_back(fPropagModels[0]->GetNameROOT(indices[m]));
            string a_tmp = Form("%d", fPropagModels[0]->GetCREntry(indices[m]).GetA());
            if (m == 0)
               list = "^{" + a_tmp;
            else
               list = list + "+" + a_tmp;
         }
         list = list + "}" + elem;
         isotopes_names_root[i].push_back(names);
         isotopes_list_root[i].push_back(list);
      }


      // 3. If need to check exact match, set daughters_match to parent's (A,Z,m)
      if (is_test_exactmatch)
         daughters_match[p].SetEntry(fPropagModels[0]->GetCREntry(j_rad[0][p]).GetA(),
                                     fPropagModels[0]->GetCREntry(j_rad[0][p]).GetZ(),
                                     fPropagModels[0]->GetCREntry(j_rad[0][p]).Getmamu()*TUPhysics::Convert_amu_to_GeV(),
                                     fPropagModels[0]->GetCREntry(j_rad[1][p]).GetName());
   }


   // If check is asked for exact match:
   //   - switch-off energy redistribution
   //   - set parent and daughter nuclei to the same A, Z, m
   //     (ensure same propagation history)
   //   - Switch-off secondary production of parent->daughter
   //     (both BETA and EC)
   // As a result, decayed parents should exactly match the number
   // of appeared daughters
   if (is_test_exactmatch) {
      // Switch-off energy redistribution
      fPropagModels[0]->SetIsELossCoulomb(false);
      fPropagModels[0]->SetIsELossIon(false);
      fPropagModels[0]->SetIsELossAdiabatic(false);
      fPropagModels[0]->SetIsEReacceleration(false);
      fPropagModels[0]->SetIsDestruction(false);

      // Normalisation to source must be made before
      // any change of CR properties
      Propagate(fIsUseNormList, false);
      for (Int_t p = 0; p < n_rad; ++p)
         fPropagModels[0]->UpdateCREntry(j_rad[1][p], daughters_match[p]);

      // N.B.: as we change the particle properties, their beta,
      // gamma and rigidity have to be recalculated!!!!!!
      fPropagModels[0]->AllocateAndFillENamesAndEVars();

      // Save reference production Unstable->Daughter (both BETA and EC)
      fPropagModels[0]->GetTransport()->GetFreePars()->SetParListStatus(true);
      fPropagModels[0]->InitialiseForCalculation(/*is_init_or_update*/true,  /*is_force_update*/false, /*is_verbose*/false, f_test);
   } else
      Propagate(fIsUseNormList, false);

   fprintf(f_test, "\n");
   fPropagModels[0]->PrintUnstables(f_test, is_beta_or_ec);
   fprintf(f_test, "\n");
   fPropagModels[0]->PrintPropagSwitches(f_test);

   // Storage variables for fluxes (for unstable): we plot below
   // the flux for the unstable and its daughter, but also for the
   // corresponding element (and associated isotopes).
   vector<vector<Double_t> > flux_ref_rads[n], flux_decayoff_rads[n];
   vector<vector<Double_t> > flux_ref_elements[n], flux_decayoff_elements[n];
   vector<vector<vector<Double_t> > > flux_ref_isotopes[n], flux_decayoff_isotopes[n];
   vector<Double_t> v_zero(ne, 0.);


   //---------------------------------------------------
   // Calculate reference fluxes for unstable [DECAY=ON]
   //---------------------------------------------------
   fprintf(fOutputLog, "\n");
   fprintf(fOutputLog, "%s   ... process reference fluxes [DECAY = ON]\n", indent.c_str());
   fPropagModels[0]->SetPropagated(false);
   Propagate(false, false);

   TUCoordTXYZ *coord_txyz = fPropagModels[0]->GetSolarSystemCoords();
   for (Int_t p = 0; p < n_rad; ++p) {
      // Loop on parent/daughter
      for (Int_t i = 0; i < n; ++i) {
         // Initialise all to zero (including isotopes)
         flux_ref_rads[i].push_back(v_zero);
         flux_decayoff_rads[i].push_back(v_zero);
         flux_ref_elements[i].push_back(v_zero);
         flux_decayoff_elements[i].push_back(v_zero);

         vector<vector<Double_t> > vv_zero;
         for (Int_t ii = 0; ii < (Int_t)isotopes[i][p].size(); ++ii)
            vv_zero.push_back(v_zero);
         flux_ref_isotopes[i].push_back(vv_zero);
         flux_decayoff_isotopes[i].push_back(vv_zero);

         // Fill parent/daughter
         fPropagModels[0]->FillFlux(flux_ref_rads[i][p], j_rad[i][p], coord_txyz, 0);

         // Fill isotopes
         for (Int_t ii = 0; ii < (Int_t)isotopes[i][p].size(); ++ii) {
            fPropagModels[0]->FillFlux(flux_ref_isotopes[i][p][ii], isotopes[i][p][ii], coord_txyz, 0);
            for (Int_t k = 0; k < ne; ++k)
               flux_ref_elements[i][p][k] += flux_ref_isotopes[i][p][ii][k];
         }
      }
   }
   fprintf(f_test, "\n");

   Int_t n_desired = 20;
   Int_t k_step = max(1, ne / n_desired);
   fPropagModels[0]->SetPropagated(false);
   Propagate(false, false);

   //------------------------------------------
   // Calculate fluxes for unstable [DECAY=OFF]
   //------------------------------------------
   // N.B.: for the sake of readability, we reduce the number
   // of points in the plot.
   TUAxis *e_family = fPropagModels[0]->GetEFamily(kNUC);

   for (Int_t p = n_rad - 1; p >= 0 ; --p) {
      // Set to STABLE (also sets half-life to -1)
      fprintf(f_test, "%s   ... process unstable %s  [DECAY = OFF]",
              indent.c_str(), fPropagModels[0]->GetCREntry(j_rad[0][p]).GetName().c_str());
      indent = TUMessages::Indent(true);
      fPropagModels[0]->UpdateType(j_rad[0][p], "STABLE");
      fPropagModels[0]->UpdateType(j_rad[1][p], "STABLE");
      fPropagModels[0]->UpdateIndicesOfUnstables();
      //cout << endl;
      //fPropagModels[0]->PrintUnstables(f_test, is_beta_or_ec);
      //cout << indent << "   with CR under scrutiny set to: "
      //     << fPropagModels[0]->GetCREntry(j_rad[0][p]).GetName() << endl;


      // Propagation for new set-up
      // N.B.: changed CR properties mean that some coefficient
      // in the transport equation must be changed. There is no
      // 'clean' way to do it as in normal use, these properties
      // are not meant to be changed. To propagate these changes
      // in the calculation, transport status must be set to false.
      fPropagModels[0]->InitialiseForCalculation(/*is_init_or_update*/false,  /*is_force_update*/true, /*is_verbose*/false, f_test);
      fPropagModels[0]->SetPropagated(false);
      Propagate(false, false);

      fprintf(f_test, "Flux IS: %s -> %s\n", unstable_names[0][p].c_str(), unstable_names[1][p].c_str());
      fprintf(f_test, "Ekn [GeV/n]     (%s)        (%s)no_decay   (%s)        (%s)not_fed     %s(stable-decayed)  %s(fed-notfed)\n",
              unstable_names[0][p].c_str(), unstable_names[0][p].c_str(),
              unstable_names[1][p].c_str(), unstable_names[1][p].c_str(),
              unstable_names[0][p].c_str(), unstable_names[1][p].c_str());

      // Loop on parent/daughter
      for (Int_t i = 0; i < n; ++i) {
         // Fill parent/daughter
         fPropagModels[0]->FillFlux(flux_decayoff_rads[i][p], j_rad[i][p], coord_txyz, 0);

         // Fill isotopes (and element as sum of isotopes
         for (Int_t ii = 0; ii < (Int_t)isotopes[i][p].size(); ++ii) {
            fPropagModels[0]->FillFlux(flux_decayoff_isotopes[i][p][ii], isotopes[i][p][ii], coord_txyz, 0);
            for (Int_t k = 0; k < ne; ++k)
               flux_decayoff_elements[i][p][k] += flux_decayoff_isotopes[i][p][ii][k];
         }
      }

      for (Int_t k = 0; k < ne; k += k_step)
         fprintf(f_test, "%le    %le %le      %le %le      %le  %le\n", e_family->GetVal(k),
                 flux_ref_rads[0][p][k], flux_decayoff_rads[0][p][k],
                 flux_ref_rads[1][p][k], flux_decayoff_rads[1][p][k],
                 flux_ref_rads[0][p][k] - flux_decayoff_rads[0][p][k],
                 flux_ref_rads[1][p][k] - flux_decayoff_rads[1][p][k]);


      // Reset to reference
      fPropagModels[0]->UpdateCREntry(j_rad[0][p], parents[p]);
      fPropagModels[0]->UpdateCREntry(j_rad[1][p], daughters[p]);
      fPropagModels[0]->UpdateIndicesOfUnstables();
      //fPropagModels[0]->PrintUnstables(f_test, is_beta_or_ec);
      //cout << endl;
      indent = TUMessages::Indent(false);
   }


   // Reset to initial CR properties
   for (Int_t p = 0; p < n_rad; ++p) {
      fPropagModels[0]->UpdateCREntry(j_rad[0][p], parents[p]);
      fPropagModels[0]->UpdateCREntry(j_rad[1][p], daughters[p]);
   }


   // Reset to initial propagation properties
   if (ref_propag) {
      fPropagModels[0]->TUPropagSwitches::Copy(*ref_propag);
      delete ref_propag;
      ref_propag = NULL;
   }

   // Refill-calculate all properties/propagation
   if (is_test_exactmatch) {
      if (fPropagModels[0]->GetModel() == kMODEL1DKISOVC || fPropagModels[0]->GetModel() == kMODEL2DKISOVC) {
         Int_t i_par = fPropagModels[0]->IndexPar("rhole");
         fPropagModels[0]->GetParEntry(i_par)->SetVal(rhole_ref, false);
      }
      fPropagModels[0]->AllocateAndFillENamesAndEVars();
      fPropagModels[0]->GetTransport()->GetFreePars()->SetParListStatus(true);
      fPropagModels[0]->InitialiseForCalculation(/*is_init_or_update*/false,  /*is_force_update*/true, /*is_verbose*/false, f_test);
   }
   fPropagModels[0]->UpdateIndicesOfUnstables();
   fPropagModels[0]->PrintUnstables(f_test, is_beta_or_ec);
   fPropagModels[0]->SetPropagated(false);


   // Plot/display results, using several plots:
   // -----------------------------------------
   //   1. Isotopes: ratio (stable-ref)_parent / (stable-ref)_daughter
   //   2. Isotopes and elements: ratio (stable/ref)_parent and (stable/ref)_daughter
   //   3. Element: ratio ref_parent/ref_daughter and stable_parent/stable_daughter
   //   4. Isotopic ratios of parent and daughter (w and w/o decay)
   TUMisc::RootSetStylePlots();
   TText *usine_txt = TUMisc::OrphanUSINEAdOnPlots();

   string model = fPropagModels[0]->GetModelName();

   // 1. Isotopes: ratio (stable-ref)_parent / (stable-ref)_daughter
   // Canvas and graphs
   string name = Form("check_unstable_balance_%s_%s_%s",
                      decay[is_beta_or_ec].c_str(), model.c_str(),
                      match[is_test_exactmatch].c_str());
   string title = Form("Check %s balance: %s %s",
                       decay_root[is_beta_or_ec].c_str(), model.c_str(),
                       match[is_test_exactmatch].c_str());
   TCanvas *c1 = new TCanvas(name.c_str(), title.c_str());
   c1->SetWindowPosition(0, 0);
   //c1->SetGridx();
   //c1->SetGridy();
   c1->SetLogx(1);
   c1->SetLogy(0);
   string mg_name = "mg_" + name;
   string x_title = TUEnum::Enum2NameUnit(kEKN);
   string y_title = "(#phi - #phi_{w/o decay})_{parent} / (#phi - #phi_{w/o decay})_{daughter}";
   string mg_titles = ";" + x_title + ";" + y_title;
   TMultiGraph *mg1 = new TMultiGraph(mg_name.c_str(), mg_name.c_str());
   mg1->SetTitle(mg_titles.c_str());
   TLegend *leg1 = new TLegend(0.4, 0.6, 0.94, 0.95);
   leg1->SetHeader(decay_root[is_beta_or_ec].c_str());
   // Loop on unstables
   for (Int_t p = 0; p < n_rad; ++p) {
      // Add graph only if radioactive has its daughter in CR list
      // (otherwise cannot perform the calculation)

      TGraph *gr = new TGraph();
      for (Int_t k = 0; k < ne; k += k_step) {

         Double_t y = fabs(flux_decayoff_rads[0][p][k] - flux_ref_rads[0][p][k])
                      / fabs(flux_decayoff_rads[1][p][k] - flux_ref_rads[1][p][k]);
         gr->SetPoint(k / k_step, e_family->GetVal(k), y);
      }
      string gr_name = "gr_balance_" + decay[is_beta_or_ec]
                       + "_" +  model + "_" + match[is_test_exactmatch]
                       + "_" + unstable_names[0][p] + "_" + unstable_names[1][p];
      TUMisc::RemoveSpecialChars(gr_name);
      gr->SetName(gr_name.c_str());
      gr->SetLineColor(TUMisc::RootColor(p));
      gr->SetLineStyle(p % 9 + 1);
      gr->SetLineWidth(2);
      mg1->Add(gr, "L");
      Double_t hl_myr = 0.;
      if (is_beta_or_ec)
         hl_myr = fPropagModels[0]->GetCREntry(j_rad[0][p]).GetBETAHalfLife();
      else
         hl_myr = fPropagModels[0]->GetCREntry(j_rad[0][p]).GetECHalfLife();
      string text_leg = Form("%s [t_{1/2}=%g Myr] #rightarrow %s",
                             unstable_names_root[0][p].c_str(), hl_myr, unstable_names_root[1][p].c_str());
      TLegendEntry *entry = leg1->AddEntry(gr, text_leg.c_str(), "L");
      entry->SetTextColor(TUMisc::RootColor(p));
      entry = NULL;
   }

   // Display
   c1->cd();
   mg1->Draw("AL");
   if (is_test)
      TUMisc::Print(f_test, mg1, 0);
   usine_txt->Draw();
   leg1->Draw();
   c1->Modified();
   c1->Update();



   // 2. Isotopes and elements: ratio (stable/ref)_parent and (stable/ref)_daughter
   // Canvas and graphs
   TCanvas **c2 = new TCanvas*[n_rad];
   TMultiGraph **mg2 = new TMultiGraph*[n_rad];
   TLegend **leg2 = new TLegend*[n_rad];
   // Loop on unstables
   for (Int_t p = 0; p < n_rad; ++p) {
      name = Form("check_unstable_disappearance_%s_%s_%s_%s_%s",
                  decay[is_beta_or_ec].c_str(),
                  unstable_names[0][p].c_str(), unstable_names[1][p].c_str(),
                  model.c_str(), match[is_test_exactmatch].c_str());
      title = Form("Fraction (dis-)appeared of (%s-)%s %s: %s [%s]",
                   unstable_names[0][p].c_str(), unstable_names[1][p].c_str(),
                   decay_root[is_beta_or_ec].c_str(), model.c_str(),
                   match[is_test_exactmatch].c_str());
      c2[p] = new TCanvas(name.c_str(), title.c_str());
      c2[p]->SetWindowPosition(c2[p]->GetWw(), p * 100);
      //c2[p]->SetGridx();
      //c2[p]->SetGridy();
      c2[p]->SetLogx(1);
      c2[p]->SetLogy(0);
      mg_name = "mg_" + name;
      y_title = "(#phi - #phi_{no~decay}) / #phi_{no~decay} #times 100  [%]";
      mg_titles = ";" + x_title + ";" + y_title;
      mg2[p] = new TMultiGraph(mg_name.c_str(), mg_name.c_str());
      mg2[p]->SetTitle(mg_titles.c_str());
      leg2[p] = new TLegend(0.65, 0.5, 0.97, 0.95);
      //string header = decay_root[is_beta_or_ec] + ": "
      // + unstable_names_root[0][p] + " #rightarrow "
      // + unstable_names_root[1][p];
      //leg2[p]->SetHeader(header.c_str());

      // Add graph only if radioactive has its daughter in CR list
      // (otherwise cannot perform the calculation)
      for (Int_t i = n - 1; i >= 0; --i) {
         TGraph *gr1 = new TGraph();
         TGraph *gr2 = new TGraph();
         for (Int_t k = 0; k < ne; k += k_step) {
            Double_t y1 = 100.*(flux_ref_rads[i][p][k] - flux_decayoff_rads[i][p][k])
                          / flux_decayoff_rads[i][p][k];
            Double_t y2 = 100.*(flux_ref_elements[i][p][k] - flux_decayoff_elements[i][p][k])
                          / flux_decayoff_elements[i][p][k];
            gr1->SetPoint(k / k_step, e_family->GetVal(k), y1);
            gr2->SetPoint(k / k_step, e_family->GetVal(k), y2);
         }
         string gr1_name = "gr_disappearance_" + decay[is_beta_or_ec]
                           + "_" +  model + "_" + match[is_test_exactmatch]
                           + "_" + unstable_names[i][p];
         string gr2_name = "gr_disappearance_" + decay[is_beta_or_ec]
                           + "_" +  model + "_" + match[is_test_exactmatch]
                           + "_" + elements[i][p];


         TLegendEntry *entry = NULL;
         string opt = "LP";
         if (i == 0) {
            entry = leg2[p]->AddEntry((TObject *)0, "[disappearance]", "");
            entry->SetTextColor(TUMisc::RootColor(p));
            opt = "L";
         } else {
            entry = leg2[p]->AddEntry((TObject *)0, "[appearance]", "");
            entry->SetTextColor(TUMisc::RootColor(p));
         }

         TUMisc::RemoveSpecialChars(gr1_name);
         gr1->SetName(gr1_name.c_str());
         gr1->SetLineColor(TUMisc::RootColor(p));
         gr1->SetLineStyle(1);
         gr1->SetLineWidth(2);
         gr1->SetMarkerColor(TUMisc::RootColor(p));
         gr1->SetMarkerStyle(TUMisc::RootMarker(p));
         gr1->SetMarkerSize(1.5);
         mg2[p]->Add(gr1, opt.c_str());

         TUMisc::RemoveSpecialChars(gr2_name);
         gr2->SetName(gr2_name.c_str());
         gr2->SetLineColor(TUMisc::RootColor(p));
         gr2->SetLineStyle(7);
         gr2->SetLineWidth(2);
         gr2->SetMarkerColor(TUMisc::RootColor(p));
         gr2->SetMarkerStyle(TUMisc::RootMarker(p));
         gr2->SetMarkerSize(1.5);
         mg2[p]->Add(gr2, opt.c_str());

         if (i == 1) {
            entry = leg2[p]->AddEntry(gr1, unstable_names_root[i][p].c_str(), opt.c_str());
            entry->SetTextColor(TUMisc::RootColor(p));

            entry = leg2[p]->AddEntry(gr2, (elements[i][p] + " = "
                                            + isotopes_list_root[i][p]).c_str(), opt.c_str());
            entry->SetTextColor(TUMisc::RootColor(p));
         } else {
            entry = leg2[p]->AddEntry(gr2, (elements[i][p] + " = "
                                            + isotopes_list_root[i][p]).c_str(), opt.c_str());
            entry->SetTextColor(TUMisc::RootColor(p));

            entry = leg2[p]->AddEntry(gr1, unstable_names_root[i][p].c_str(), opt.c_str());
            entry->SetTextColor(TUMisc::RootColor(p));
         }
      }
      // Display
      c2[p]->cd();
      mg2[p]->Draw("AL");
      if (is_test)
         TUMisc::Print(f_test, mg2[p], 0);
      usine_txt->Draw();
      leg2[p]->Draw();
      c2[p]->Modified();
      c2[p]->Update();
   }


   // 3. Element: ratio ref_parent/ref_daughter and stable_parent/stable_daughter
   // Canvas and graphs
   TCanvas **c3 = new TCanvas*[n_rad];
   TMultiGraph **mg3 = new TMultiGraph*[n_rad];
   TLegend **leg3 = new TLegend*[n_rad];
   // Loop on unstables
   for (Int_t p = 0; p < n_rad; ++p) {
      name = Form("check_unstable_maximpact_%s_%s_%s_%s_%s",
                  decay[is_beta_or_ec].c_str(), unstable_names[0][p].c_str(),
                  unstable_names[1][p].c_str(), model.c_str(),
                  match[is_test_exactmatch].c_str());
      title = Form("%s->%s max. impact (%s): %s [%s]",
                   unstable_names[0][p].c_str(), unstable_names[1][p].c_str(),
                   decay_root[is_beta_or_ec].c_str(), model.c_str(),
                   match[is_test_exactmatch].c_str());
      c3[p] = new TCanvas(name.c_str(), title.c_str());
      c3[p]->SetWindowPosition(2 * c3[p]->GetWw(), p * 100);
      //c3[p]->SetGridx();
      //c3[p]->SetGridy();
      c3[p]->SetLogx(1);
      c3[p]->SetLogy(1);
      mg_name = "mg_" + name;
      y_title = "#phi (parent)  /  #phi (daughter)";
      mg_titles = ";" + x_title + ";" + y_title;
      mg3[p] = new TMultiGraph(mg_name.c_str(), mg_name.c_str());
      mg3[p]->SetTitle(mg_titles.c_str());
      leg3[p] = new TLegend(0.6, 0.5, 0.97, 0.95);


      // Graph elements
      TGraph *gr1a = new TGraph();
      TGraph *gr1b = new TGraph();
      TGraph *gr2a = new TGraph();
      TGraph *gr2b = new TGraph();

      for (Int_t k = 0; k < ne; k += k_step) {
         Double_t y1a = flux_ref_elements[0][p][k] / flux_ref_elements[1][p][k];
         Double_t y1b = flux_decayoff_elements[0][p][k] / flux_decayoff_elements[1][p][k];
         Double_t y2a = flux_ref_rads[0][p][k] / flux_ref_rads[1][p][k];
         Double_t y2b = flux_decayoff_rads[0][p][k] / flux_decayoff_rads[1][p][k];
         gr1a->SetPoint(k / k_step, e_family->GetVal(k), y1a);
         gr1b->SetPoint(k / k_step, e_family->GetVal(k), y1b);
         gr2a->SetPoint(k / k_step, e_family->GetVal(k), y2a);
         gr2b->SetPoint(k / k_step, e_family->GetVal(k), y2b);
      }
      string gr1a_name = "gr_maximpact_" + decay[is_beta_or_ec]
                         + "_" +  model + "_" + match[is_test_exactmatch]
                         + "_" + unstable_names[0][p] + "_" + unstable_names[1][p];
      string gr1b_name = gr1a_name + "_nodecay";
      string gr2a_name = "gr_maximpact_" + decay[is_beta_or_ec]
                         + "_" +  model + "_" + match[is_test_exactmatch]
                         + "_" + elements[0][p] + "_" + elements[1][p];
      string gr2b_name = gr2a_name + "_nodecay";

      string leg1a_name = unstable_names_root[0][p]
                          + " / " + unstable_names_root[1][p];
      string leg2a_name = isotopes_list_root[0][p]
                          + " / " + isotopes_list_root[1][p];


      TLegendEntry *entry = leg3[p]->AddEntry((TObject *)0, "[With decay]", "");
      entry->SetTextColor(TUMisc::RootColor(p));

      TUMisc::RemoveSpecialChars(gr1a_name);
      gr1a->SetName(gr1a_name.c_str());
      gr1a->SetLineColor(TUMisc::RootColor(p));
      gr1a->SetLineStyle(1);
      gr1a->SetLineWidth(2);
      gr1a->SetMarkerColor(TUMisc::RootColor(p));
      gr1a->SetMarkerStyle(TUMisc::RootMarker(p));
      gr1a->SetMarkerSize(1.5);
      mg3[p]->Add(gr1a, "L");
      entry = leg3[p]->AddEntry(gr1a, leg1a_name.c_str(), "L");
      entry->SetTextColor(TUMisc::RootColor(p));

      TUMisc::RemoveSpecialChars(gr2a_name);
      gr2a->SetName(gr2a_name.c_str());
      gr2a->SetLineColor(TUMisc::RootColor(p));
      gr2a->SetLineStyle(1);
      gr2a->SetLineWidth(2);
      gr2a->SetMarkerColor(TUMisc::RootColor(p));
      gr2a->SetMarkerStyle(TUMisc::RootMarker(p));
      gr2a->SetMarkerSize(1.5);
      mg3[p]->Add(gr2a, "PL");
      entry = leg3[p]->AddEntry(gr2a, leg2a_name.c_str(), "PL");
      entry->SetTextColor(TUMisc::RootColor(p));

      entry = leg3[p]->AddEntry((TObject *)0, "[No decay]", "");
      entry->SetTextColor(TUMisc::RootColor(p));

      TUMisc::RemoveSpecialChars(gr1b_name);
      gr1b->SetName(gr1b_name.c_str());
      gr1b->SetLineColor(TUMisc::RootColor(p));
      gr1b->SetLineStyle(3);
      gr1b->SetLineWidth(2);
      gr1b->SetMarkerColor(TUMisc::RootColor(p));
      gr1b->SetMarkerStyle(TUMisc::RootMarker(p));
      gr1b->SetMarkerSize(1.5);
      mg3[p]->Add(gr1b, "L");
      entry = leg3[p]->AddEntry(gr1b, leg1a_name.c_str(), "L");
      entry->SetTextColor(TUMisc::RootColor(p));

      TUMisc::RemoveSpecialChars(gr2b_name);
      gr2b->SetName(gr2b_name.c_str());
      gr2b->SetLineColor(TUMisc::RootColor(p));
      gr2b->SetLineStyle(3);
      gr2b->SetLineWidth(2);
      gr2b->SetMarkerColor(TUMisc::RootColor(p));
      gr2b->SetMarkerStyle(TUMisc::RootMarker(p));
      gr2b->SetMarkerSize(1.5);
      mg3[p]->Add(gr2b, "PL");
      entry = leg3[p]->AddEntry(gr2b, leg2a_name.c_str(), "PL");
      entry->SetTextColor(TUMisc::RootColor(p));

      // Display
      c3[p]->cd();
      mg3[p]->Draw("AL");
      if (is_test)
         TUMisc::Print(f_test, mg3[p], 0);
      usine_txt->Draw();
      leg3[p]->Draw();
      c3[p]->Modified();
      c3[p]->Update();
   }



   // 4. Isotopic ratios of parent and daughter (w and w/o decay)
   // Canvas and graphs
   TCanvas **c4 = new TCanvas*[n_rad];
   TMultiGraph **mg4 = new TMultiGraph*[n_rad];
   TLegend **leg4 = new TLegend*[n_rad];
   // Loop on unstables
   for (Int_t p = 0; p < n_rad; ++p) {
      name = Form("check_unstable_isotopicratios_%s_%s_%s_%s_%s",
                  decay[is_beta_or_ec].c_str(), unstable_names[0][p].c_str(),
                  unstable_names[1][p].c_str(), model.c_str(),
                  match[is_test_exactmatch].c_str());
      title = Form("Isotopic ratio w/o %s (%s->%s): %s [%s]",
                   decay_root[is_beta_or_ec].c_str(), unstable_names[0][p].c_str(),
                   unstable_names[1][p].c_str(), model.c_str(),
                   match[is_test_exactmatch].c_str());
      c4[p] = new TCanvas(name.c_str(), title.c_str());
      c4[p]->SetWindowPosition(3 * c4[p]->GetWw(), p * 100);
      //c4[p]->SetGridx();
      //c4[p]->SetGridy();
      c4[p]->SetLogx(1);
      c4[p]->SetLogy(0);
      mg_name = "mg_" + name;
      y_title = "Isotopic fraction";
      mg_titles = ";" + x_title + ";" + y_title;
      mg4[p] = new TMultiGraph(mg_name.c_str(), mg_name.c_str());
      mg4[p]->SetTitle(mg_titles.c_str());
      leg4[p] = new TLegend(0.6, 0.2, 0.97, 0.95);


      // Loop on parent/daughter
      for (Int_t i = 0; i < n; ++i) {
         TLegendEntry *entry = NULL;

         if (i == 0) {
            entry = leg4[p]->AddEntry((TObject *)0, ("["
                                      + decay_root[is_beta_or_ec] + " "
                                      + unstable_names_root[0][p] + "]").c_str(), "");
            entry->SetTextColor(TUMisc::RootColor(p));
         } else {
            entry = leg4[p]->AddEntry((TObject *)0, ("[Daughter "
                                      + unstable_names_root[1][p] + "]").c_str(), "");
            entry->SetTextColor(TUMisc::RootColor(p));
         }

         string opt = "L";
         if (i == 1)
            opt = "LP";

         // Loop on isotopes
         for (Int_t ii = 0; ii < (Int_t)isotopes[i][p].size(); ++ii) {
            TGraph *gr1 = new TGraph();
            TGraph *gr2 = new TGraph();
            for (Int_t k = 0; k < ne; k += k_step) {
               Double_t y1 = flux_decayoff_isotopes[i][p][ii][k]
                             / flux_decayoff_elements[i][p][k];
               Double_t y2 = flux_ref_isotopes[i][p][ii][k]
                             / flux_ref_elements[i][p][k];
               gr1->SetPoint(k / k_step, e_family->GetVal(k), y1);
               gr2->SetPoint(k / k_step, e_family->GetVal(k), y2);
            }

            string gr1_name = "gr_isotopicratio_" + decay[is_beta_or_ec]
                              + "_" +  model + "_" + match[is_test_exactmatch]
                              + "_" + isotopes_names[i][p][ii] + "_" + elements[i][p];
            string gr2_name = gr1_name + "_nodecay";
            string leg1_name = isotopes_names_root[i][p][ii]
                               + " / " + elements[i][p];
            string leg2_name = leg1_name + " (no decay)";
            leg1_name = leg1_name + " (with decay)";

            TUMisc::RemoveSpecialChars(gr1_name);
            gr1->SetName(gr1_name.c_str());
            gr1->SetLineColor(TUMisc::RootColor(ii));
            gr1->SetLineStyle(ii + 1);
            gr1->SetLineWidth(3);
            gr1->SetMarkerColor(TUMisc::RootColor(ii));
            gr1->SetMarkerStyle(TUMisc::RootMarker(p));
            gr1->SetMarkerSize(1.5);
            mg4[p]->Add(gr1, opt.c_str());
            entry = leg4[p]->AddEntry(gr1, leg1_name.c_str(), opt.c_str());
            entry->SetTextColor(TUMisc::RootColor(ii));

            TUMisc::RemoveSpecialChars(gr2_name);
            gr2->SetName(gr2_name.c_str());
            gr2->SetLineColor(TUMisc::RootColor(ii));
            gr2->SetLineStyle(ii + 1);
            gr2->SetLineWidth(1);
            gr2->SetMarkerColor(TUMisc::RootColor(ii));
            gr2->SetMarkerStyle(TUMisc::RootMarker(p));
            gr2->SetMarkerSize(1.5);
            mg4[p]->Add(gr2, opt.c_str());
            entry = leg4[p]->AddEntry(gr2, leg2_name.c_str(), opt.c_str());
            entry->SetTextColor(TUMisc::RootColor(ii));
         }
      }

      // Display
      c4[p]->cd();
      mg4[p]->Draw("AL");
      if (is_test)
         TUMisc::Print(f_test, mg4[p], 0);
      usine_txt->Draw();
      leg4[p]->Draw();
      c4[p]->Modified();
      c4[p]->Update();
   }


   // Enter event loop, one can now interact with the objects in
   // the canvas. Select "Exit ROOT" from Canvas "File" menu to exit
   // the event loop and execute the next statements.
   if (!gROOT->IsBatch())
      app->Run(kTRUE);

   delete usine_txt;
   e_family = NULL;

   // Free memory
   // Canvas c1
   delete leg1;
   leg1 = NULL;
   if (mg1) delete mg1;
   mg1 = NULL;
   delete c1;
   c1 = NULL;

   // Canvas c2
   for (Int_t p = 0; p < n_rad; ++p) {
      if (leg2[p]) delete leg2[p];
      leg2[p] = NULL;
      if (mg2[p]) delete mg2[p];
      mg2[p] = NULL;
      if (c2[p]) delete c2[p];
      c2[p] = NULL;
   }
   if (leg2) delete[] leg2;
   leg2 = NULL;
   if (mg2) delete[] mg2;
   mg2 = NULL;
   if (c2) delete[] c2;
   c2 = NULL;

   // Canvas c3
   for (Int_t p = 0; p < n_rad; ++p) {
      if (leg3[p]) delete leg3[p];
      leg3[p] = NULL;
      if (mg3[p]) delete mg3[p];
      mg3[p] = NULL;
      if (c3[p]) delete c3[p];
      c3[p] = NULL;
   }
   if (leg3) delete[] leg3;
   leg3 = NULL;
   if (mg3) delete[] mg3;
   mg3 = NULL;
   if (c3) delete[] c3;
   c3 = NULL;

   // Canvas c4
   for (Int_t p = 0; p < n_rad; ++p) {
      if (leg4[p]) delete leg4[p];
      leg4[p] = NULL;
      if (mg4[p]) delete mg4[p];
      mg4[p] = NULL;
      if (c4[p]) delete c4[p];
      c4[p] = NULL;
   }
   if (leg4) delete[] leg4;
   leg4 = NULL;
   if (mg4) delete[] mg4;
   mg4 = NULL;
   if (c4) delete[] c4;
   c4 = NULL;

   TUMessages::Indent(false);

   if (is_test) {
      fOutputLog = fout_ref;
      fout_ref = NULL;
   }
}

//______________________________________________________________________________
void TURunPropagation::Plots_GCRvsSSAbund(TApplication *app, Double_t ekn, Bool_t is_print, FILE *f_print)
{
   //--- Prints and displays comparison of GCR vs SS abundances.
   //  app              Root application
   //  ekn              Energy at which to show comparison
   //  is_print         Whether to print or not content of gr/h/multigr
   //  f_print          File in which to print gr/h/multigr

   // Print infos
   TUMessages::Separator(f_print, "GCR vs SS abundances");
   //PrintListOfModels(f);

   // Set Plot style
   TUMisc::RootSetStylePlots();
   TText *usine_txt = TUMisc::OrphanUSINEAdOnPlots();

   if (ekn < 0) {
      string indent = TUMessages::Indent(true);
      cout << indent << "   ### Select Ekn [GeV/n]: ";
      cin >> ekn;
      TUMessages::Indent(false);
   }

   // Create Root file
   string plot_root = fOutputDir + "/local_gcr_vs_ss.root";
   TFile *root_file = NULL;
   if (fIsSaveInFiles)
      root_file = new TFile(plot_root.c_str(), "recreate");

   Propagate(fIsUseNormList, false);
   //fPropagModels[0]->PrintSolarSystemISFluxes(f);

   // Gather source spectra, primary content for
   // elements and isotopes for all models
   const Int_t n_more = 2;
   string title_more[n_more] = {
      "Source abundances pattern (steady-state models)",
      "Comparison of SS/GCR/src abundances"
   };
   string cname_more[n_more] = {
      "FIP_vs_Volatility",
      "CR_vs_SS_abundances"
   };

   // Canvas
   TCanvas **c_vs_ss = new TCanvas*[n_more];
   for (Int_t i = 0; i < n_more; ++i) {
      c_vs_ss[i] = new TCanvas(cname_more[i].c_str(), title_more[i].c_str(), 600, 600);
      c_vs_ss[i]->SetWindowPosition(i * c_vs_ss[i]->GetWw(), 0);
      c_vs_ss[i]->Divide(1, 2);
   }

   // Canvas 1: GCR/SS abundances vs FIP and Volatility
   TMultiGraph *mg_fip = new TMultiGraph();
   TMultiGraph *mg_tc = new TMultiGraph();
   TLegend *leg_fip = new TLegend(0.15, 0.15, 0.5, 0.93);
   TLegend *leg_tc = new TLegend(0.15, 0.15, 0.5, 0.93);
   string norm_element = "Si";
   for (Int_t j = 0; j < fPropagModels[0]->GetNSrcAstro(); ++j) {
      fPropagModels[0]->GetSrcAstro(j)->AddPlotRatioSrcToSSAbund(mg_fip, leg_fip,
            norm_element, fPropagModels[0]->GetModelName(), true);
      fPropagModels[0]->GetSrcAstro(j)->AddPlotRatioSrcToSSAbund(mg_tc, leg_tc,
            norm_element, fPropagModels[0]->GetModelName(), false);
   }
   //
   c_vs_ss[0]->cd(1);
   gPad->SetLogx(1);
   gPad->SetLogy(1);
   mg_fip->Draw("A");
   usine_txt->Draw();
   if (is_print)
      TUMisc::Print(f_print, mg_fip, 0);
   leg_fip->Draw();
   if (fIsSaveInFiles) {
      root_file->cd();
      mg_fip->Write();
   }
   //
   c_vs_ss[0]->cd(2);
   gPad->SetLogx(0);
   gPad->SetLogy(1);
   mg_tc->Draw("A");
   usine_txt->Draw();
   if (is_print)
      TUMisc::Print(f_print, mg_tc, 0);
   leg_tc->Draw();
   if (fIsSaveInFiles) {
      root_file->cd();
      mg_tc->Write();
   }


   // Canvas 2: GCR/SS abundances vs CR and element
   TLegend *leg_abund = new TLegend(0.75, 0.75, 0.97, 0.93);
   leg_abund->AddEntry((TObject *)0, fPropagModels[0]->GetModelName().c_str(), "");
   string scale_element = "O";
   Double_t norm = 100.;
   Double_t bar_skip = 0.05;
   Double_t bar_width = 0.5 - 2.*bar_skip;
   Double_t ekn_found = ekn;

   // Histo: 2 series (for isotopes and elements)
   //  - GCR sources (as many as model sources)
   //  - GCR prim and tot propagated fluxes
   //  - SS abundances
   const Int_t n_h = 3 + fPropagModels[0]->GetNSrcAstro();
   const Int_t n_htot = 2 * n_h;
   TH1D **h_abund = new TH1D*[n_htot];
   for (Int_t is_cr = 0; is_cr < 2; ++is_cr) {
      c_vs_ss[1]->cd(is_cr + 1);

      for (Int_t i = 0; i < n_h; ++i) {
         Int_t i_h = is_cr * n_h + i;

         // Default attributes
         Int_t color = kBlack;
         Int_t markerstyle = 34;
         Int_t fill_style = 1;
         Double_t bwidth = 1.;
         Double_t boff = 0.;
         string leg_txt = "";
         string opt_plot = "";
         string leg_plot = "";

         // Create histos
         if (i == 0) {
            // SS abundances
            h_abund[i_h] = fPropagModels[0]->GetSrcAstro(0)->OrphanSrcAbundances((Bool_t)is_cr,
                           false, fPropagModels[0]->GetModelName(), scale_element, norm);
            color = kOrange;
            leg_txt = "SS";
            opt_plot = "][";
            leg_plot = "PL";
         } else if (i == 1) {
            // GCR total flux
            h_abund[i_h] = fPropagModels[0]->OrphanGCRLocalAbundances((Bool_t)is_cr,
                           ekn_found, 0, scale_element, norm);
            color = kGray + 1;
            fill_style = 3002;
            bwidth = bar_width - 0.1;
            boff = bar_skip + 0.05;
            leg_txt = Form("GCR @ %.1le GeV/n [Tot]", ekn_found);
            opt_plot = "bar,SAME";
            leg_plot = "F";
         } else if (i == 2) {
            // GCR primary flux
            h_abund[i_h] = fPropagModels[0]->OrphanGCRLocalAbundances((Bool_t)is_cr,
                           ekn_found, 1, scale_element, norm);
            color = kGray + 2;
            fill_style = 3001;
            bwidth = bar_width;
            boff = bar_skip;
            leg_txt = Form("GCR @ %.1le GeV/n [Prim]", ekn_found);
            opt_plot = "bar,SAME";
            leg_plot = "F";
         } else {
            h_abund[i_h] = fPropagModels[0]->GetSrcAstro(i - 3)->OrphanSrcAbundances((Bool_t)is_cr, true,
                           fPropagModels[0]->GetModelName(), scale_element, norm);
            color = kRed;
            fill_style = 3344;
            bwidth = bar_width - 0.2;
            boff = bar_skip + 0.5;
            leg_txt = Form("Src norm. - %s", fPropagModels[0]->GetSrcAstro(i - 3)->GetSrcName().c_str());
            opt_plot = "bar,SAME";
            leg_plot = "F";
         }

         if (is_print)
            TUMisc::PrintHisto(f_print, h_abund[i_h], 0);
         h_abund[i_h]->SetFillStyle(fill_style);
         h_abund[i_h]->SetFillColor(color);
         h_abund[i_h]->SetBarWidth(bwidth);
         h_abund[i_h]->SetBarOffset(bar_skip);
         h_abund[i_h]->SetMarkerColor(color);
         h_abund[i_h]->SetMarkerStyle(markerstyle);
         h_abund[i_h]->SetLineColor(color);
         h_abund[i_h]->SetLineStyle(1);
         h_abund[i_h]->SetLineWidth(1);
         h_abund[i_h]->Draw(opt_plot.c_str());
         usine_txt->Draw();
         if (!is_cr) {
            TLegendEntry *entry = leg_abund->AddEntry(h_abund[i_h], leg_txt.c_str(), leg_plot.c_str());
            entry->SetTextColor(color);
         }
         if (fIsSaveInFiles) {
            root_file->cd();
            h_abund[i_h]->Write();
         }
      }
      gPad->SetLogx(0);
      gPad->SetLogy(1);
      leg_abund->Draw();
   }

   // Update all
   for (Int_t i = 0; i < n_more; ++i) {
      c_vs_ss[i]->Modified();
      c_vs_ss[i]->Update();

      if (fIsSaveInFiles) {
         c_vs_ss[i]->Write();
         TUMisc::RootSave(c_vs_ss[i], fOutputDir, c_vs_ss[i]->GetName(), "CP");
      }
   }

   //if (fIsSaveInFiles) {
   //   root_file->Close();
   //   delete root_file;
   //}

   /* terminate */
   // Enter event loop, one can now interact with the objects in
   // the canvas. Select "Exit ROOT" from cvs "File" menu to exit
   // the event loop and execute the next statements.
   if (!gROOT->IsBatch())
      app->Run(kTRUE);

   // Free memory
   //////////////
   // canvas 1 //
   //////////////
   delete mg_fip;
   delete mg_tc;
   delete leg_fip;
   delete leg_tc;
   //////////////
   // canvas 2 //
   //////////////
   if (h_abund) {
      for (Int_t i = 0; i < n_htot; ++i) {
         if (h_abund[i]) delete h_abund[i];
         h_abund[i] = NULL;
      }
      delete[] h_abund;
   }
   delete leg_abund;
   // Delete canvas
   delete usine_txt;
   for (Int_t i = 0; i < n_more; ++i) {
      delete c_vs_ss[i];
      c_vs_ss[i] = NULL;
   }
   delete[] c_vs_ss;
}

//______________________________________________________________________________
void TURunPropagation::Plots_ImpactBoundaryConditions(TApplication *app,
      Bool_t is_normdata4allconfigs, string const &option, Bool_t is_test, FILE *f_test)
{
   //--- Compares results for all available boundary contitions.
   //  app                     Root application (only one per session)
   //  is_normdata4allconfigs  If true, renorm to data for all configs, otherwise keep same norm for all
   //  option                  Option name in usine (to print)
   //  is_test             Whether run in test mode
   //  f_test              File to output result of test


   FILE *fout_ref = fOutputLog;
   if (is_test) {
      fout_ref = fOutputLog;
      fOutputLog = f_test;
   }

   string indent = TUMessages::Indent(true);
   TUMessages::Indent(false);

   TUMessages::Separator(f_test, option + ". Flux comparisons (On-Off propagation effects)");

   // Check at least one model (warning if more than one model): only calculate for model 0
   if (fNPropagModels == 0) {
      TUMessages::Warning(fOutputLog, "TURunPropagation", "Plots_ImpactBoundaryConditions",
                          "No propagation model loaded");
      if (is_test) {
         fOutputLog = fout_ref;
         fout_ref = NULL;
      }
      return;
   } else if (fNPropagModels > 1) {
      string message = "Display result for first propagation model only ("
                       + fPropagModels[0]->GetModelName() + ")";
      TUMessages::Warning(fOutputLog, "TURunPropagation", "Plots_ImpactBoundaryConditions", message);
   }


   // Keep reference propagation mode for first model
   gENUM_BCTYPE ref_bc_le_antinuc = fPropagModels[0]->GetBCTypeAntinucLE();
   gENUM_BCTYPE ref_bc_he_antinuc = fPropagModels[0]->GetBCTypeAntinucHE();
   gENUM_BCTYPE ref_bc_le_lepton = fPropagModels[0]->GetBCTypeLeptonLE();
   gENUM_BCTYPE ref_bc_he_lepton = fPropagModels[0]->GetBCTypeLeptonHE();
   gENUM_BCTYPE ref_bc_le_nuc = fPropagModels[0]->GetBCTypeNucLE();
   gENUM_BCTYPE ref_bc_he_nuc = fPropagModels[0]->GetBCTypeNucHE();

   // Create lists on which to loop
   const Int_t n_bc_le = 3;
   gENUM_BCTYPE bc_le[n_bc_le] = {kD2NDLNEKN2_ZERO, kNOCURRENT, kDFDP_ZERO};
   //gENUM_BCTYPE bc_le[n_bc] = {kNOCHANGE, kD2NDLNEKN2_ZERO, kNOCURRENT, kDFDP_ZERO};
   const Int_t n_bc_he = 1;
   gENUM_BCTYPE bc_he[n_bc_he] = {kNOCHANGE};
   //gENUM_BCTYPE bc_he[n_bc_he] = {kNOCHANGE, kD2NDLNEKN2_ZERO, kNOCURRENT, kDFDP_ZERO};

   // Create pairs of LE,HE conditionlist for each type, skipping Loop on all BC in list, skipping the ref one
   vector<pair<gENUM_BCTYPE, gENUM_BCTYPE> > bc_antinuc;
   vector<pair<gENUM_BCTYPE, gENUM_BCTYPE> > bc_lepton;
   vector<pair<gENUM_BCTYPE, gENUM_BCTYPE> > bc_nuc;
   // First BC is ref BC!
   pair <gENUM_BCTYPE, gENUM_BCTYPE> tmp;
   tmp.first = ref_bc_le_antinuc;
   tmp.second = ref_bc_he_antinuc;
   bc_antinuc.push_back(tmp);
   tmp.first = ref_bc_le_lepton;
   tmp.second = ref_bc_he_lepton;
   bc_lepton.push_back(tmp);
   tmp.first = ref_bc_le_nuc;
   tmp.second = ref_bc_he_nuc;
   bc_nuc.push_back(tmp);
   // Add all other configs
   for (Int_t k_le = 0; k_le < n_bc_le; ++k_le) {
      for (Int_t k_he = 0; k_he < n_bc_he; ++k_he) {
         tmp.first = bc_le[k_le];
         tmp.second = bc_he[k_he];
         // Add config for antinuc (if not ref)
         if (bc_le[k_le] != ref_bc_le_antinuc || bc_he[k_he] != ref_bc_he_antinuc)
            bc_antinuc.push_back(tmp);
         // Add config for lepton (if not ref)
         if (bc_le[k_le] != ref_bc_le_lepton || bc_he[k_he] != ref_bc_he_lepton)
            bc_lepton.push_back(tmp);
         // Add config for nuc
         if (bc_le[k_le] != ref_bc_le_nuc || bc_he[k_he] != ref_bc_he_nuc)
            bc_nuc.push_back(tmp);
      }
   }

   // Calculate reference flux (to get proper normalisation),
   // and then for all configurations
   Propagate(fIsUseNormList, false);

   const Int_t n_configs = bc_nuc.size();
   TUValsTXYZECr **local_fluxes = new TUValsTXYZECr*[n_configs];
   for (Int_t c = 0; c < n_configs; ++c) {
      fprintf(f_test, "%s... process config %d/%d\n", indent.c_str(), c + 1, n_configs);

      // Update boundary conditions for configuration
      fPropagModels[0]->SetBCTypeAntinucLE(bc_antinuc[c].first);
      fPropagModels[0]->SetBCTypeAntinucHE(bc_antinuc[c].second);
      fPropagModels[0]->SetBCTypeLeptonLE(bc_lepton[c].first);
      fPropagModels[0]->SetBCTypeLeptonHE(bc_lepton[c].second);
      fPropagModels[0]->SetBCTypeNucLE(bc_nuc[c].first);
      fPropagModels[0]->SetBCTypeNucHE(bc_nuc[c].second);
      fPropagModels[0]->PrintBoundaryConditions(f_test);

      fPropagModels[0]->InitialiseForCalculation(/*is_init_or_update*/false,  /*is_force_update*/true, /*is_verbose*/false, f_test);
      fPropagModels[0]->SetPropagated(false);
      // N.B.: we normalise to CR data for all configs or not...
      Propagate(is_normdata4allconfigs, false);
      local_fluxes[c] = fPropagModels[0]->OrphanFluxes(fPropagModels[0]->GetSolarSystemCoords(), 0);
   }

   // Set Plot style
   TUMisc::RootSetStylePlots();
   TText *usine_txt = TUMisc::OrphanUSINEAdOnPlots();

   // Loop on qties and displays
   do {
      // Get configs to plot
      vector<string> list_combos;
      vector<gENUM_ETYPE> list_etypes;
      vector<vector<Double_t> > list_phi;
      Double_t e_power = 0.;
      if (!is_test)
         e_power = fPropagModels[0]->TUI_SelectCombosEAxisphiFF(list_combos, list_etypes, list_phi, true);
      else {
         list_combos.push_back("B/C");
         list_etypes.push_back(kEKN);
         list_combos.push_back("O");
         list_etypes.push_back(kEKN);
         vector<Double_t> tmp_phi(1, 0.);
         list_phi.push_back(tmp_phi);
         list_phi.push_back(tmp_phi);
         e_power = 2.8;
      }

      // Whether to quit
      string dummy_quit = list_combos[0];
      TUMisc::UpperCase(dummy_quit);
      if (dummy_quit == "Q" || dummy_quit == "0")
         break;


      // Canvas, multigraph and legend (2x to show absolute and relative comparison)
      Int_t n_combos = list_combos.size();
      TCanvas **cvs = new TCanvas*[2 * n_combos];
      TMultiGraph **mg = new TMultiGraph*[2 * n_combos];
      TLegend **leg = new TLegend*[2 * n_combos];

      // Loop on list of combos, and add new graph for each combo (for IS fluxes):
      for (Int_t i = 0; i < n_combos; ++i) {
         // Extract if lepton, nuc, and antinuc in combo
         vector<Int_t> cr_indices;
         Int_t n_denom = -1;
         fPropagModels[0]->ComboToCRIndices(list_combos[i], cr_indices, n_denom, false);
         Bool_t is_nuc = false;
         Bool_t is_antinuc = false;
         Bool_t is_lepton = false;
         for (Int_t ll = 0; ll < (Int_t)cr_indices.size(); ++ll) {
            if (fPropagModels[0]->GetCREntry(cr_indices[ll]).GetName().find("BAR") != std::string::npos)
               is_antinuc = true;
            else if (fPropagModels[0]->GetCREntry(cr_indices[ll]).GetName().find("ELECTRON") != std::string::npos
                     || fPropagModels[0]->GetCREntry(cr_indices[ll]).GetName().find("POSITRON") != std::string::npos)
               is_lepton = true;
            else if (fPropagModels[0]->GetCREntry(cr_indices[ll]).GetZ() > 0)
               is_nuc = true;
         }
         // Extract combo common E grid
         TUAxis *e_grid =  fPropagModels[0]->OrphanGetECombo(list_combos[i], list_etypes[i], f_test);
         // set titles (and axes names)
         string x_title = "", y_title = "";
         TUEnum::XYTitles(x_title, y_title, list_combos[i], list_etypes[i], e_power);

         for (Int_t tt = 0; tt < 2; ++tt) {
            string name = list_combos[i] + "_" + TUEnum::Enum2Name(list_etypes[i]) + "_" + fPropagModels[0]->GetModelName();
            TUMisc::RemoveSpecialChars(name);
            if (tt == 0) name = "check_boundaryconditions_" + name;
            else name = "check_boundaryconditions_rel_" + name;
            cvs[tt * n_combos + i] = new TCanvas(name.c_str(), list_combos[i].c_str());
            cvs[tt * n_combos + i]->SetWindowPosition(tt * 200, i * 50);
            cvs[tt * n_combos + i]->SetLogx();
            cvs[tt * n_combos + i]->cd();
            leg[tt * n_combos + i] = new TLegend(0.4, 0.5, 0.95, 0.95);
            mg[tt * n_combos + i] = new TMultiGraph();
            if (tt == 1)
               y_title = TUMisc::FormatCRQty(list_combos[i], 2) + "_{config} / "
                         + TUMisc::FormatCRQty(list_combos[i], 2) + "_{ref}";
            string mg_titles = ";" + x_title + ";" + y_title;
            mg[tt * n_combos + i]->SetTitle(mg_titles.c_str());
            Bool_t ylogscale = true;

            // Loop on all modulation levels
            leg[tt * n_combos + i]->SetHeader((fPropagModels[0]->GetModelName()
                                               + " (" + fSolModModels[0]->GetModelName() + ")").c_str());
            string ref_antinuc = "Antinuc[LE=" + TUEnum::Enum2Name(ref_bc_le_antinuc) + ",HE=" + TUEnum::Enum2Name(ref_bc_he_antinuc) + "]";
            string ref_lepton = "Lepton[LE=" + TUEnum::Enum2Name(ref_bc_le_lepton) + ",HE=" + TUEnum::Enum2Name(ref_bc_he_lepton) + "]";
            string ref_nuc = "Nuc[LE=" + TUEnum::Enum2Name(ref_bc_le_nuc) + ",HE=" + TUEnum::Enum2Name(ref_bc_he_nuc) + "]";
            leg[tt * n_combos + i]->AddEntry((TObject *)0, "Reference config:", "");
            if (is_antinuc) leg[tt * n_combos + i]->AddEntry((TObject *)0, ref_antinuc.c_str(), "");
            if (is_lepton) leg[tt * n_combos + i]->AddEntry((TObject *)0, ref_lepton.c_str(), "");
            if (is_nuc) leg[tt * n_combos + i]->AddEntry((TObject *)0, ref_nuc.c_str(), "");
            for (Int_t j = 0; j < (Int_t)list_phi[i].size(); ++j) {
               string phi_text = Form("%.3f GV", list_phi[i][j]);
               string phi_root = Form("%dMV", Int_t(list_phi[i][j] * 1.e3));

               // Update modulation value
               fSolModModels[0]->SetParVal(0, list_phi[i][j]);
               leg[tt * n_combos + i]->AddEntry((TObject *)0, ("#phi= " + phi_text).c_str(), "");

               // Loop on all configurations
               for (Int_t c = 0; c < n_configs; ++c) {
                  // For second series of graph (rel to ref), skip ref in c=0
                  if (tt == 1 && c == 0)
                     continue;
                  TUCoordTXYZ *dummy = NULL;
                  string leg_title = "";
                  if (is_antinuc)
                     leg_title += "Antinuc[LE=" + TUEnum::Enum2Name(bc_antinuc[c].first) + ",HE=" + TUEnum::Enum2Name(bc_antinuc[c].second) + "]  ";
                  if (is_lepton)
                     leg_title += "Lepton[LE=" + TUEnum::Enum2Name(bc_lepton[c].first) + ",HE=" + TUEnum::Enum2Name(bc_lepton[c].second) + "]  ";
                  if (is_nuc)
                     leg_title += "Nuc[LE=" + TUEnum::Enum2Name(bc_nuc[c].first) + ",HE=" + TUEnum::Enum2Name(bc_nuc[c].second) + "]  ";

                  // Fill first graph
                  Double_t *y = local_fluxes[c]->OrphanVals(list_combos[i], e_grid, list_etypes[i], e_power, dummy, fSolModModels[0]);
                  TGraph *gr = new TGraph(e_grid->GetN(), e_grid->GetVals(), y);
                  delete[] y;
                  y = NULL;
                  string gr_name = UID(list_combos[i], list_etypes[i], e_power, fPropagModels[0]->GetModelName(), fSolModModels[0]->UID()) + "_" + leg_title;
                  TUMisc::RemoveSpecialChars(gr_name);
                  gr->SetName(gr_name.c_str());
                  // If relative values, divide by ref
                  if (tt == 1) {
                     Double_t *y_ref = local_fluxes[0]->OrphanVals(list_combos[i], e_grid, list_etypes[i], e_power, dummy, fSolModModels[0]);
                     gr_name = "rel_" + (string)gr->GetName();
                     for (Int_t i_pts = 0; i_pts < gr->GetN(); ++i_pts) {
                        Double_t x, y;
                        gr->GetPoint(i_pts, x, y);
                        gr->SetPoint(i_pts, x, y / y_ref[i_pts]);
                     }
                     delete y_ref;
                     y_ref = NULL;
                  }
                  gr->SetLineColor(TUMisc::RootColor(c));
                  gr->SetLineStyle((c + 1) % 10);
                  gr->SetLineWidth(2 + 2 * j);
                  mg[tt * n_combos + i]->Add(gr, "L");
                  ylogscale = TUMisc::IsYaxisLog(list_combos[i]);
                  // Add legend
                  TLegendEntry *entry = leg[tt * n_combos + i]->AddEntry(gr, leg_title.c_str(), "L");
                  entry->SetTextColor(TUMisc::RootColor(c));

                  if (tt == 0) {
                     fprintf(f_test, " ###### %s\n", leg_title.c_str());
                     local_fluxes[c]->PrintVals(f_test, list_combos[i], e_grid, list_etypes[i], e_power, dummy,  fSolModModels[0]);
                  } else
                     ylogscale = false;
                  gr = NULL;
               }
            }
            // mg in first cvs (spectrum)
            cvs[tt * n_combos + i]->SetLogy(ylogscale);
            mg[tt * n_combos + i]->Draw("A");
            if (is_test)
               TUMisc::Print(f_test, mg[tt * n_combos + i], 0);
            else {
               string f_save = "comparison_BoundCond_" + list_combos[i] + "_" + TUEnum::Enum2Name(list_etypes[i]);
               if (tt == 1)
                  f_save += "_rel";
               TUMisc::RemoveSpecialChars(f_save);
               f_save = fOutputDir + "/" + f_save + ".out";
               FILE *fp_save = fopen(f_save.c_str(), "w");
               TUMisc::PrintUSINEHeader(fp_save, true);
               TUMisc::Print(fp_save, mg[tt * n_combos + i], 0);
               fclose(fp_save);
               printf("   ... result saved in %s\n", f_save.c_str());
            }

            usine_txt->Draw();
            mg[tt * n_combos + i]->GetXaxis()->CenterTitle();
            mg[tt * n_combos + i]->GetYaxis()->CenterTitle();
            leg[tt * n_combos + i]->Draw();
            cvs[tt * n_combos + i]->Modified();
            cvs[tt * n_combos + i]->Update();
         }
         delete e_grid;
         e_grid = NULL;
      }

      /* terminate */
      // Enter event loop, one can now interact with the objects in
      // the canvas. Select "Exit ROOT" from cvs "File" menu to exit
      // the event loop and execute the next statements.
      if (!gROOT->IsBatch())
         app->Run(kTRUE);


      // Free memory
      for (Int_t i = 0; i < 2 * n_combos; ++i) {
         if (mg[i]) delete mg[i];
         mg[i] = NULL;
         if (leg[i]) delete leg[i];
         leg[i] = NULL;
         if (cvs[i]) delete cvs[i];
         cvs[i] = NULL;
      }
      if (mg) delete[] mg;
      mg = NULL;
      if (leg) delete[] leg;
      leg = NULL;
      if (cvs) delete[] cvs;
      cvs = NULL;

      if (is_test)
         break;
   } while (1);
   delete usine_txt;

   // Free memory of fluxes
   for (Int_t c = 0; c < n_configs; ++c) {
      if (local_fluxes[c])
         delete local_fluxes[c];
      local_fluxes[c] = NULL;
   }
   if (local_fluxes)
      delete[] local_fluxes;
   local_fluxes = NULL;

   // Reset to initial config
   fPropagModels[0]->SetBCTypeAntinucLE(ref_bc_le_antinuc);
   fPropagModels[0]->SetBCTypeAntinucHE(ref_bc_he_antinuc);
   fPropagModels[0]->SetBCTypeLeptonLE(ref_bc_le_lepton);
   fPropagModels[0]->SetBCTypeLeptonHE(ref_bc_he_lepton);
   fPropagModels[0]->SetBCTypeNucLE(ref_bc_le_nuc);
   fPropagModels[0]->SetBCTypeNucHE(ref_bc_he_nuc);
   fPropagModels[0]->InitialiseForCalculation(/*is_init_or_update*/true,  /*is_force_update*/false, /*is_verbose*/false, f_test);
   fPropagModels[0]->SetPropagated(false);

   if (is_test) {
      fOutputLog = fout_ref;
      fout_ref = NULL;
   }
}

//______________________________________________________________________________
void TURunPropagation::Plots_ImpactPropagSwitches(TApplication *app,
      Bool_t is_normdata4allconfigs, string const &option, Bool_t is_test, FILE *f_test)
{
   //--- Compares propagated fluxes switching on or off different processes
   //    (energy losses, radioactive decay, inelastic interaction, etc.).
   //  app                     Root application (only one per session)
   //  is_normdata4allconfigs  If true, renorm to data for all configs, otherwise keep same norm for all
   //  option                  Option name in usine (to print)
   //  is_test             Whether run in test mode
   //  f_test              File to output result of test

   FILE *fout_ref = NULL;
   if (is_test) {
      fout_ref = fOutputLog;
      fOutputLog = f_test;
   }

   string indent = TUMessages::Indent(true);
   TUMessages::Indent(false);
   TUMessages::Separator(f_test, option + ". Flux comparisons");

   if (is_normdata4allconfigs)
      fprintf(f_test, "    N.B.: each configuration is normalised to data, i.e. match elemental flux data at a given energy\n");
   else
      fprintf(f_test, "    N.B.: only reference configuration normalised to data: other ones use same source abundances as ref.\n");

   // Check at least one model (warning if more than one model): only calculate for model 0
   if (fNPropagModels == 0) {
      TUMessages::Warning(fOutputLog, "TURunPropagation", "Plots_ImpactPropagSwitches",
                          "No propagation model loaded");
      if (is_test) {
         fOutputLog = fout_ref;
         fout_ref = NULL;
      }
      return;
   } else if (fNPropagModels > 1) {
      string message = "Display result for first propagation model only ("
                       + fPropagModels[0]->GetModelName() + ")";
      TUMessages::Warning(fOutputLog, "TURunPropagation", "Plots_ImpactPropagSwitches", message);
   }


   // Keep reference propagation mode for first model
   TUPropagSwitches *ref_propag = fPropagModels[0]->TUPropagSwitches::Clone();
   TUPropagSwitches switch_comparison;
   switch_comparison.SetAllSwitches(false);


   string user_choice;
   do {
      switch_comparison.PrintPropagSwitches(f_test, true /*is_comparison*/);

      if (!is_test) {
         cout << ">> Loop to switch on/off one at a time (e.g. '2') then '0' (calculate for all ON switches), or 'Q' (quit): ";
         cin >> user_choice;
         Int_t index = atoi(user_choice.c_str()) - 1;
         switch_comparison.SetSwitch(index, !switch_comparison.IsOn(index));
         TUMisc::UpperCase(user_choice);
      } else {
         switch_comparison.SetSwitch(0, !switch_comparison.IsOn(0));
         switch_comparison.SetSwitch(1, !switch_comparison.IsOn(1));
         switch_comparison.SetSwitch(3, !switch_comparison.IsOn(3));
         user_choice = "0";
      }
   } while (user_choice != "0" && user_choice != "Q");

   if (user_choice == "Q") return;

   PrintListOfModels(fOutputLog);
   fprintf(f_test, "      [only force-field modulation is available for the moment]\n");


   // Calculate reference flux (to get proper normalisation),
   // and then for all configurations
   Propagate(fIsUseNormList, false);
   vector<TUPropagSwitches> list_configs;
   ref_propag->ExtractConfigurations(switch_comparison, list_configs);
   Int_t n_configs = list_configs.size();
   TUValsTXYZECr **local_fluxes = new TUValsTXYZECr*[n_configs];
   for (Int_t c = 0; c < n_configs; ++c) {
      fprintf(f_test, "%s... process %s\n", indent.c_str(), list_configs[c].ExtractNameSwitches(2).c_str());
      fPropagModels[0]->TUPropagSwitches::Copy(list_configs[c]);
      fPropagModels[0]->InitialiseForCalculation(/*is_init_or_update*/false,  /*is_force_update*/true, /*is_verbose*/false, f_test);
      fPropagModels[0]->SetPropagated(false);
      // N.B.: we normalise to CR data for all configs or not...
      Propagate(is_normdata4allconfigs, false);
      local_fluxes[c] = fPropagModels[0]->OrphanFluxes(fPropagModels[0]->GetSolarSystemCoords(), 0);
   }

   // Set Plot style
   TUMisc::RootSetStylePlots();
   TText *usine_txt = TUMisc::OrphanUSINEAdOnPlots();

   // Loop on qties and displays
   do {
      // Get configs to plot
      vector<string> list_combos;
      vector<gENUM_ETYPE> list_etypes;
      vector<vector<Double_t> > list_phi;
      Double_t e_power = 0.;
      if (!is_test)
         e_power = fPropagModels[0]->TUI_SelectCombosEAxisphiFF(list_combos, list_etypes, list_phi, true);
      else {
         list_combos.push_back("B/C");
         list_etypes.push_back(kEKN);
         list_combos.push_back("O");
         list_etypes.push_back(kR);
         vector<Double_t> tmp_phi(1, 0.);
         list_phi.push_back(tmp_phi);
         list_phi.push_back(tmp_phi);
         e_power = 2.8;
      }

      // Whether to quit
      string dummy_quit = list_combos[0];
      TUMisc::UpperCase(dummy_quit);
      if (dummy_quit == "Q" || dummy_quit == "0")
         break;


      // Canvas, multigraph and legend
      Int_t n_combos = list_combos.size();
      TCanvas **cvs = new TCanvas*[n_combos];
      TMultiGraph **mg = new TMultiGraph*[n_combos];
      TLegend **leg = new TLegend*[n_combos];

      // Loop on list of combos, and add new graph for each combo (for IS fluxes):
      for (Int_t i = 0; i < n_combos; ++i) {
         // Extract combo common E grid
         TUAxis *e_grid =  fPropagModels[0]->OrphanGetECombo(list_combos[i], list_etypes[i], f_test);

         string name = "check_onoff_propag_" + list_combos[i] + "_"
                       + TUEnum::Enum2Name(list_etypes[i]) + "_" + fPropagModels[0]->GetModelName();
         TUMisc::RemoveSpecialChars(name);
         cvs[i] = new TCanvas(name.c_str(), list_combos[i].c_str());
         cvs[i]->SetWindowPosition(0, i * 50);
         cvs[i]->SetLogx();
         cvs[i]->cd();
         leg[i] = new TLegend(0.4, 0.5, 0.95, 0.95);
         mg[i] = new TMultiGraph();
         // set titles (and axes names)
         string x_title = "", y_title = "";
         TUEnum::XYTitles(x_title, y_title, list_combos[i], list_etypes[i], e_power);
         string mg_titles = ";" + x_title + ";" + y_title;
         mg[i]->SetTitle(mg_titles.c_str());
         Bool_t ylogscale;

         // Loop on all modulation levels
         leg[i]->SetHeader((fPropagModels[0]->GetModelName()
                            + " (" + fSolModModels[0]->GetModelName() + ")").c_str());
         //leg[i]->AddEntry((TObject *)0, ("Reference config = "
         //                                + (string)ref_propag->ExtractNameSwitches(1, true)).c_str(), "");
         for (Int_t j = 0; j < (Int_t)list_phi[i].size(); ++j) {
            string phi_text = Form("%.3f GV", list_phi[i][j]);
            string phi_root = Form("%dMV", Int_t(list_phi[i][j] * 1.e3));

            // Update modulation value
            fSolModModels[0]->SetParVal(0, list_phi[i][j]);
            leg[i]->AddEntry((TObject *)0, ("#phi= " + phi_text).c_str(), "");

            // Loop on all configurations
            for (Int_t c = 0; c < (Int_t)list_configs.size(); ++c) {
               TUCoordTXYZ *dummy = NULL;
               string leg_title = list_combos[i] + " "
                                  + list_configs[c].ExtractNameSwitches(1, ref_propag);
               Double_t *y = local_fluxes[c]->OrphanVals(list_combos[i], e_grid, list_etypes[i], e_power, dummy, fSolModModels[0]);
               TGraph *gr = new TGraph(e_grid->GetN(), e_grid->GetVals(), y);
               delete[] y;
               y = NULL;
               string gr_name = UID(list_combos[i], list_etypes[i], e_power, fPropagModels[0]->GetModelName(), fSolModModels[0]->UID()) + "_" + list_configs[c].ExtractNameSwitches(2);
               TUMisc::RemoveSpecialChars(gr_name);
               gr->SetName(gr_name.c_str());
               gr->SetLineColor(TUMisc::RootColor(c));
               gr->SetLineStyle((c + 1) % 10);
               gr->SetLineWidth(2 + j);
               mg[i]->Add(gr, "L");
               ylogscale = TUMisc::IsYaxisLog(list_combos[i]);
               fprintf(f_test, "%s\n", leg_title.c_str());
               // Add legend
               TLegendEntry *entry = leg[i]->AddEntry(gr, leg_title.c_str(), "L");
               entry->SetTextColor(TUMisc::RootColor(c));
               local_fluxes[c]->PrintVals(f_test, list_combos[i], e_grid, list_etypes[i], e_power, dummy,  fSolModModels[0]);
               gr = NULL;
            }
         }

         delete e_grid;
         e_grid = NULL;

         // mg in first cvs (spectrum)
         cvs[i]->SetLogy(ylogscale);
         mg[i]->Draw("A");
         if (is_test)
            TUMisc::Print(f_test, mg[i], 0);
         else {
            string f_save = "comparison_switches_" + list_combos[i] + "_" + TUEnum::Enum2Name(list_etypes[i]);
            TUMisc::RemoveSpecialChars(f_save);
            f_save = fOutputDir + "/" + f_save + ".out";
            FILE *fp_save = fopen(f_save.c_str(), "w");
            TUMisc::PrintUSINEHeader(fp_save, true);
            TUMisc::Print(fp_save, mg[i], 0);
            fclose(fp_save);
            printf("   ... result saved in %s\n", f_save.c_str());
         }

         usine_txt->Draw();
         mg[i]->GetXaxis()->CenterTitle();
         mg[i]->GetYaxis()->CenterTitle();
         leg[i]->Draw();
         cvs[i]->Modified();
         cvs[i]->Update();
      }

      /* terminate */
      // Enter event loop, one can now interact with the objects in
      // the canvas. Select "Exit ROOT" from cvs "File" menu to exit
      // the event loop and execute the next statements.
      if (!gROOT->IsBatch())
         app->Run(kTRUE);


      // Free memory
      for (Int_t i = 0; i < n_combos; ++i) {
         if (mg[i]) delete mg[i];
         mg[i] = NULL;
         if (leg[i]) delete leg[i];
         leg[i] = NULL;
         if (cvs[i]) delete cvs[i];
         cvs[i] = NULL;
      }
      if (mg) delete[] mg;
      mg = NULL;
      if (leg) delete[] leg;
      leg = NULL;
      if (cvs) delete[] cvs;
      cvs = NULL;

      if (is_test)
         break;
   } while (1);
   delete usine_txt;

   // Free memory of fluxes
   for (Int_t c = 0; c < n_configs; ++c) {
      if (local_fluxes[c])
         delete local_fluxes[c];
      local_fluxes[c] = NULL;
   }
   if (local_fluxes)
      delete[] local_fluxes;
   local_fluxes = NULL;

   // Reset to initial config
   fPropagModels[0]->TUPropagSwitches::Copy(*ref_propag);
   fPropagModels[0]->InitialiseForCalculation(/*is_init_or_update*/true,  /*is_force_update*/false, /*is_verbose*/false, f_test);
   fPropagModels[0]->SetPropagated(false);
   delete ref_propag;
   ref_propag = NULL;

   if (is_test) {
      fOutputLog = fout_ref;
      fout_ref = NULL;
   }
}

//______________________________________________________________________________
void TURunPropagation::Plots_ImpactXSOnCombos(TApplication *app, Int_t switch_xs,
      Bool_t is_normdata4allconfigs, string const &option, Bool_t is_test, FILE *f_test)
{
   //--- Compares results for cross-section selections.
   //  app                     Root application (only one per session)
   //  switch_xs               X-sections to consider
   //                             0 => [NUC] Inelastic cross-sections
   //                             1 => [NUC] Production cross-sections
   //                             2 => [ANTINUC] Inelastic cross-sections
   //                             3 => [ANTINUC] Production cross-sections
   //                             4 => [ANTINUC] Tertiary: inelastic non-ann cross-sections
   //                             5 => [ANTINUC] Tertiary: redistribution cross-sections
   //                             6 => [LEPTON] Production cross-sections
   //  is_normdata4allconfigs  If true, renorm to data for all configs, otherwise keep same norm for all
   //  option                  Option name in usine (to print)
   //  is_test             Whether run in test mode
   //  f_test              File to output result of test

   FILE *fout_ref = NULL;
   if (is_test) {
      fout_ref = fOutputLog;
      fOutputLog = f_test;
   }

   string indent = TUMessages::Indent(true);
   TUMessages::Indent(false);

   TUMessages::Separator(f_test, option + ". Flux comparisons (varying cross-sections)");

   // Get selection X-section files
   string dir[] = {"$USINE/inputs/XS_NUCLEI/", "$USINE/inputs/XS_NUCLEI/",
                   "$USINE/inputs/XS_ANTINUC/", "$USINE/inputs/XS_ANTINUC/", "$USINE/inputs/XS_ANTINUC/", "$USINE/inputs/XS_ANTINUC/"
                  };
   string base[] = {"sigInel*", "sigProd*", "sigInelANN*", "dSdEProd*", "sigInelNONANN*", "dSdENAR*"};
   string exclude[] = {"", "", "", "tertiary", "", "", ""};
   string ext[] = {"inelXSnuc", "prodXSnuc", "inelXSantinuc", "prodXSantinuc", "nonannXSantinuc", "narXSantinuc"};
   string pattern = dir[switch_xs] + base[switch_xs];
   vector<string> xs_files = TUMisc::ExtractFromDir(pattern, /*is_print*/ true, f_test, /*is_selection*/ true, exclude[switch_xs], is_test);
   vector<string> ref_files;
   if (switch_xs == 0 || switch_xs == 2) {
      for (Int_t i = 0; i < fPropagModels[0]->GetNFilesXTotIA(); ++i)
         ref_files.push_back(fPropagModels[0]->GetFileXTotIA(i));
   } else if (switch_xs == 1  || switch_xs == 3  || switch_xs == 6) {
      for (Int_t i = 0; i < fPropagModels[0]->GetNFilesXProd(); ++i)
         ref_files.push_back(fPropagModels[0]->GetFileXProd(i));
   } else if (switch_xs == 4) {
      for (Int_t i = 0; i < fPropagModels[0]->GetNFilesXTotINA(); ++i)
         ref_files.push_back(fPropagModels[0]->GetFileXTotINA(i));
   } else if (switch_xs == 5) {
      for (Int_t i = 0; i < fPropagModels[0]->GetNFilesXDiffINA(); ++i)
         ref_files.push_back(fPropagModels[0]->GetFileXDiffINA(i));
   }


   // In TUXSections, always compare file to load to list already loaded (to avoid double-loading)
   // For comparison, we always want to enfore loading, even if was in list before
   fPropagModels[0]->TUXSections::ClearFilesXDiffINA();
   fPropagModels[0]->TUXSections::ClearFilesXProd();
   fPropagModels[0]->TUXSections::ClearFilesXTotIA();
   fPropagModels[0]->TUXSections::ClearFilesXTotINA();


   // Local ISM medium
   TUMediumTXYZ *ism = fPropagModels[0]->GetISM();
   ism->FillMediumEntry(fPropagModels[0]->GetSolarSystemCoords());
   TUMediumEntry *local_ism = ism->GetMediumEntry();
   //local_ism->Print();
   ism = NULL;


   // Calculate reference flux (to get proper normalisation),
   // and then for all configurations
   const Int_t n_configs = xs_files.size();
   TUValsTXYZECr **local_fluxes = new TUValsTXYZECr*[n_configs];
   fPropagModels[0]->SetPropagated(false);
   Propagate(fIsUseNormList, false);
   for (Int_t c = 0; c < n_configs; ++c) {
      string xs_current_file = xs_files[c];
      TUMisc::SubstituteEnvInPath(xs_current_file, "$USINE");
      fprintf(f_test, "%s... process %d/%d using %s\n", indent.c_str(), c + 1, n_configs, xs_current_file.c_str());
      if (switch_xs == 0 || switch_xs == 2)
         fPropagModels[0]->TUXSections::LoadOrUpdateTotInel(xs_files[c], /*is_ia_or_ina*/ true, /*is_verbose*/ false, fOutputLog);
      else if (switch_xs == 1 || switch_xs == 3 || switch_xs == 6)
         fPropagModels[0]->TUXSections::LoadOrUpdateProd(xs_files[c], /*is_verbose*/ false, fOutputLog);
      else if (switch_xs == 4)
         fPropagModels[0]->TUXSections::LoadOrUpdateTotInel(xs_files[c], /*is_ia_or_ina*/ false, /*is_verbose*/ false, fOutputLog);
      else if (switch_xs == 5)
         fPropagModels[0]->TUXSections::LoadOrUpdateDSigDEkINA(xs_files[c], /*is_verbose*/ false, fOutputLog);

      // Update ISM0D (to local value)
      fPropagModels[0]->FillInterRate_ISM0D(local_ism, true);
      fPropagModels[0]->InitialiseForCalculation(/*is_init_or_update*/false,  /*is_force_update*/true, /*is_verbose*/false, f_test);
      fPropagModels[0]->SetPropagated(false);
      // N.B.: we normalise to CR data for all configs or not...
      Propagate(is_normdata4allconfigs, false);
      local_fluxes[c] = fPropagModels[0]->OrphanFluxes(fPropagModels[0]->GetSolarSystemCoords(), 0);
   }

   // Set Plot style
   TUMisc::RootSetStylePlots();
   TText *usine_txt = TUMisc::OrphanUSINEAdOnPlots();

   // Loop on qties and displays
   do {
      // Get configs to plot
      vector<string> list_combos;
      vector<gENUM_ETYPE> list_etypes;
      vector<vector<Double_t> > list_phi;
      Double_t e_power = 0.;
      if (switch_xs == 0 || switch_xs == 1) {
         // NUCLEI
         if (!is_test)
            e_power = fPropagModels[0]->TUI_SelectCombosEAxisphiFF(list_combos, list_etypes, list_phi, true);
         else {
            list_combos.push_back("B/C");
            list_etypes.push_back(kEKN);
            list_combos.push_back("O");
            list_etypes.push_back(kR);
            vector<Double_t> tmp_phi(1, 0.);
            list_phi.push_back(tmp_phi);
            list_phi.push_back(tmp_phi);
            e_power = 2.8;
         }
         // Whether to quit
         string dummy_quit = list_combos[0];
         TUMisc::UpperCase(dummy_quit);
         if (dummy_quit == "Q" || dummy_quit == "0")
            break;
      } else if (switch_xs >= 2 && switch_xs <= 5) {
         // ANTINUCLEI
         if (fPropagModels[0]->TUAxesCrE::Index("1H-BAR", false) >= 0) {
            list_combos.push_back("1H-BAR");
            list_etypes.push_back(kEKN);
         }
         if (fPropagModels[0]->TUAxesCrE::Index("2H-BAR", false) >= 0) {
            list_combos.push_back("2H-BAR");
            list_etypes.push_back(kEKN);
         }
         if (!is_test) {
            vector<Double_t> tmp_phi;
            TUI_SelectListOfPhi(tmp_phi);
            list_phi.push_back(tmp_phi);
            list_phi.push_back(tmp_phi);
         } else {
            vector<Double_t> tmp_phi(1, 0.);
            list_phi.push_back(tmp_phi);
            list_phi.push_back(tmp_phi);
         }
      } else if (switch_xs == 6) {
         // LEPTONS
         if (fPropagModels[0]->TUAxesCrE::Index("ELECTRON", false) >= 0) {
            list_combos.push_back("ELECTRON");
            list_etypes.push_back(kEK);
         }
         if (fPropagModels[0]->TUAxesCrE::Index("POSITRON", false) >= 0) {
            list_combos.push_back("POSITRON");
            list_etypes.push_back(kEK);
         }
         if (!is_test) {
            vector<Double_t> tmp_phi;
            TUI_SelectListOfPhi(tmp_phi);
            list_phi.push_back(tmp_phi);
            list_phi.push_back(tmp_phi);
         } else {
            vector<Double_t> tmp_phi(1, 0.);
            list_phi.push_back(tmp_phi);
            list_phi.push_back(tmp_phi);
         }
      }

      // Canvas, multigraph and legend (2x to show absolute and relative comparison)
      Int_t n_combos = list_combos.size();
      TCanvas **cvs = new TCanvas*[2 * n_combos];
      TMultiGraph **mg = new TMultiGraph*[2 * n_combos];
      TLegend **leg = new TLegend*[2 * n_combos];

      for (Int_t tt = 0; tt < 2; ++tt) {
         // Loop on list of combos, and add new graph for each combo (for IS fluxes):
         for (Int_t i = 0; i < n_combos; ++i) {
            // Extract combo common E grid
            TUAxis *e_grid =  fPropagModels[0]->OrphanGetECombo(list_combos[i], list_etypes[i], f_test);
            string name = "";
            if (tt == 0)
               name = "check_" + ext[switch_xs] + "_" + list_combos[i] + "_" + TUEnum::Enum2Name(list_etypes[i]) + "_" + fPropagModels[0]->GetModelName();
            else if (tt == 1)
               name = "check_" + ext[switch_xs] + "_rel_" + list_combos[i] + "_" + TUEnum::Enum2Name(list_etypes[i]) + "_" + fPropagModels[0]->GetModelName();
            TUMisc::RemoveSpecialChars(name);
            cvs[tt * n_combos + i] = new TCanvas(name.c_str(), list_combos[i].c_str());
            cvs[tt * n_combos + i]->SetWindowPosition(tt * 200, i * 50);
            cvs[tt * n_combos + i]->SetLogx();
            cvs[tt * n_combos + i]->cd();
            leg[tt * n_combos + i] = new TLegend(0.4, 0.5, 0.95, 0.95);
            mg[tt * n_combos + i] = new TMultiGraph();
            // set titles (and axes names)
            string x_title = "", y_title = "";
            TUEnum::XYTitles(x_title, y_title, list_combos[i], list_etypes[i], e_power);
            if (tt == 1)
               y_title = TUMisc::FormatCRQty(list_combos[i], 2) + "_{config} / "
                         + TUMisc::FormatCRQty(list_combos[i], 2) + "_{ref}";
            string mg_titles = ";" + x_title + ";" + y_title;
            mg[tt * n_combos + i]->SetTitle(mg_titles.c_str());
            Bool_t ylogscale;

            // PATH to files
            string file_dir = TUMisc::GetPath(dir[switch_xs]);
            string ref_file = xs_files[0];
            ref_file.erase(ref_file.find(file_dir), file_dir.length());
            // Loop on all modulation levels
            leg[tt * n_combos + i]->SetHeader((fPropagModels[0]->GetModelName() + " (" + fSolModModels[0]->GetModelName() + ")").c_str());
            if (tt == 1)
               leg[tt * n_combos + i]->AddEntry((TObject *)0, ("Ref = " + ref_file).c_str(), "");
            for (Int_t j = 0; j < (Int_t)list_phi[i].size(); ++j) {
               string phi_text = Form("%.3f GV", list_phi[i][j]);
               string phi_root = Form("%dMV", Int_t(list_phi[i][j] * 1.e3));

               // Update modulation value
               fSolModModels[0]->SetParVal(0, list_phi[i][j]);
               leg[tt * n_combos + i]->AddEntry((TObject *)0, ("#phi= " + phi_text).c_str(), "");

               // Loop on all configurations
               for (Int_t c = 0; c < n_configs; ++c) {
                  // For second series of graph (rel to ref), kip ref in c=0
                  if (tt == 1 && c == 0)
                     continue;
                  // If ANTINUC
                  if (switch_xs >= 2 && switch_xs <= 5) {
                     // If 1H-BAR, only compare relevant XS
                     if (list_combos[i] == "1H-BAR" && xs_files[c].find("pbar") == string::npos)
                        continue;
                     // If 2H-BAR, only compare relevant XS
                     if (list_combos[i] == "2H-BAR" && xs_files[c].find("dbar") == string::npos)
                        continue;
                  } else if (switch_xs == 6) {
                     // If ELECTRON, only compare relevant XS
                     if (list_combos[i] == "ELECTRON" && xs_files[c].find("elect") != string::npos)
                        continue;
                     // If POSITRON, only compare relevant XS
                     if (list_combos[i] == "POSITRON" && xs_files[c].find("posit") != string::npos)
                        continue;
                  }
                  TUCoordTXYZ *dummy = NULL;
                  string leg_title = xs_files[c];
                  // Strip full path to keep only name
                  leg_title.erase(leg_title.find(file_dir), file_dir.length());
                  // Also strip extension
                  if (leg_title.find(".dat") || leg_title.find(".txt"))
                     leg_title.erase(leg_title.end() - 4, leg_title.end());

                  // Fill first graph
                  Double_t *y = local_fluxes[c]->OrphanVals(list_combos[i], e_grid, list_etypes[i], e_power, dummy, fSolModModels[0]);
                  TGraph *gr = new TGraph(e_grid->GetN(), e_grid->GetVals(), y);
                  delete[] y;
                  y = NULL;
                  string gr_name = UID(list_combos[i], list_etypes[i], e_power, fPropagModels[0]->GetModelName(), fSolModModels[0]->UID()) + "_" + leg_title;
                  TUMisc::RemoveSpecialChars(gr_name);
                  gr->SetName(gr_name.c_str());
                  // If relative values, divide by ref
                  if (tt == 1) {
                     Double_t *y_ref = local_fluxes[0]->OrphanVals(list_combos[i], e_grid, list_etypes[i], e_power, dummy, fSolModModels[0]);
                     gr_name = "rel_" + (string)gr->GetName();
                     for (Int_t i_pts = 0; i_pts < gr->GetN(); ++i_pts) {
                        Double_t x, y;
                        gr->GetPoint(i_pts, x, y);
                        gr->SetPoint(i_pts, x, y / y_ref[i_pts]);
                     }
                     delete y_ref;
                     y_ref = NULL;
                  }
                  gr->SetLineColor(TUMisc::RootColor(c));
                  gr->SetLineStyle((c + 1) % 10);
                  gr->SetLineWidth(2 + 2 * j);
                  mg[tt * n_combos + i]->Add(gr, "L");
                  ylogscale = TUMisc::IsYaxisLog(list_combos[i]);
                  // Add legend
                  TLegendEntry *entry = leg[tt * n_combos + i]->AddEntry(gr, leg_title.c_str(), "L");
                  entry->SetTextColor(TUMisc::RootColor(c));
                  if (tt == 0 && is_test) {
                     fprintf(f_test, " ###### %s\n", leg_title.c_str());
                     local_fluxes[c]->PrintVals(f_test, list_combos[i], e_grid, list_etypes[i], e_power, dummy,  fSolModModels[0]);
                  } else
                     ylogscale = false;
                  gr = NULL;
               }
            }
            delete e_grid;
            e_grid = NULL;
            if (is_test)
               TUMisc::Print(f_test, mg[tt * n_combos + i], 0);
            else {
               string f_save = "comparison_xs_" + list_combos[i] + "_" + TUEnum::Enum2Name(list_etypes[i]);
               if (tt == 1)
                  f_save += "_rel";
               TUMisc::RemoveSpecialChars(f_save);
               f_save = fOutputDir + "/" + f_save + ".out";
               FILE *fp_save = fopen(f_save.c_str(), "w");
               TUMisc::PrintUSINEHeader(fp_save, true);
               TUMisc::Print(fp_save, mg[tt * n_combos + i], 0);
               fclose(fp_save);
               printf("   ... result saved in %s\n", f_save.c_str());
            }
            // mg in first cvs (spectrum)
            cvs[tt * n_combos + i]->SetLogy(ylogscale);
            mg[tt * n_combos + i]->Draw("A");
            if (mg[tt * n_combos + i]->GetXaxis()) mg[tt * n_combos + i]->GetXaxis()->CenterTitle();
            if (mg[tt * n_combos + i]->GetYaxis()) mg[tt * n_combos + i]->GetYaxis()->CenterTitle();
            usine_txt->Draw();
            leg[tt * n_combos + i]->Draw();
            cvs[tt * n_combos + i]->Modified();
            cvs[tt * n_combos + i]->Update();
         }
      }

      /* terminate */
      // Enter event loop, one can now interact with the objects in
      // the canvas. Select "Exit ROOT" from cvs "File" menu to exit
      // the event loop and execute the next statements.
      if (!gROOT->IsBatch())
         app->Run(kTRUE);


      // Free memory
      for (Int_t i = 0; i < 2 * n_combos; ++i) {
         if (mg[i]) delete mg[i];
         mg[i] = NULL;
         if (leg[i]) delete leg[i];
         leg[i] = NULL;
         if (cvs[i]) delete cvs[i];
         cvs[i] = NULL;
      }
      if (mg) delete[] mg;
      mg = NULL;
      if (leg) delete[] leg;
      leg = NULL;
      if (cvs) delete[] cvs;
      cvs = NULL;
      if (switch_xs > 1)
         break;

      if (is_test)
         break;
   } while (1);
   delete usine_txt;

   // Free memory of fluxes
   for (Int_t c = 0; c < n_configs; ++c) {
      if (local_fluxes[c])
         delete local_fluxes[c];
      local_fluxes[c] = NULL;
   }
   if (local_fluxes)
      delete[] local_fluxes;
   local_fluxes = NULL;

   // Reset to initial config
   if (switch_xs == 0  || switch_xs == 2) {
      fPropagModels[0]->ClearFilesXTotIA();
      for (Int_t i = 0; i < (Int_t)ref_files.size(); ++i)
         fPropagModels[0]->TUXSections::LoadOrUpdateTotInel(TUMisc::GetPath(ref_files[i]), /*is_ia_or_ina*/ true, /*is_verbose*/ false, fOutputLog);
   } else if (switch_xs == 1  || switch_xs == 3  || switch_xs == 6) {
      fPropagModels[0]->ClearFilesXProd();
      for (Int_t i = 0; i < (Int_t)ref_files.size(); ++i)
         fPropagModels[0]->TUXSections::LoadOrUpdateProd(TUMisc::GetPath(ref_files[i]), /*is_verbose*/ false, fOutputLog);
   } else if (switch_xs == 4) {
      for (Int_t i = 0; i < (Int_t)ref_files.size(); ++i)
         fPropagModels[0]->TUXSections::LoadOrUpdateTotInel(TUMisc::GetPath(ref_files[i]), /*is_ia_or_ina*/ false, /*is_verbose*/ false, fOutputLog);
   } else if (switch_xs == 5) {
      for (Int_t i = 0; i < (Int_t)ref_files.size(); ++i)
         fPropagModels[0]->TUXSections::LoadOrUpdateDSigDEkINA(TUMisc::GetPath(ref_files[i]), /*is_verbose*/ false, fOutputLog);
   }

   fPropagModels[0]->InitialiseForCalculation(/*is_init_or_update*/true,  /*is_force_update*/false, /*is_verbose*/false, f_test);
   fPropagModels[0]->SetPropagated(false);

   if (is_test) {
      fOutputLog = fout_ref;
      fout_ref = NULL;
   }
}

//______________________________________________________________________________
void TURunPropagation::Plots_XSProdRanking(TApplication *app, string option, gENUM_ETYPE e_type,
      Bool_t is_test, FILE *f_test)
{
   //--- Plots fractional contributions of parents (1 step and multi-step production).
   //    N.B.: based on nuclear XS production files in $USINE/inputs/XS_NUC/sigProd*.
   //    If several files selected, calculate mean and dispersion for various files.
   //       - D0. Relative contributions (primary, secondary, radioactive) in isotopes and elements,
   //       - D1. Ranking (propag.-weighted) of multi-step channels [D1+ to force use of hard-coded propag. params]
   //       - D2a. Ranking (propag.-weighted) of individual XS [D2a+ to force use of hard-coded propag. params]
   //       - D2b. Ranking (propag.-weighted) of individual ghost-separated XS [D2b+ to force use of hard-coded propag. params]
   //
   //  app                 ROOT application
   //  option              To recall/print number used for option
   //  e_type              E-type (kEKN, kR, kEK, or kETOT) in which to display result
   //  is_test             Whether run in test mode
   //  f_test              File to output result of test


   TUMessages::Separator(f_test, option + ". Fractional secondary contribs for CRs (LIS)");
   string indent = TUMessages::Indent(true);
   FILE *fout_ref = fOutputLog;
   if (is_test)
      fOutputLog = f_test;

   // Options
   Bool_t is_ghost_separately = false, is_hardcoded_params = false;
   if (option.find("D2B") != string::npos)
      is_ghost_separately = true;

   if (option.find("+") != string::npos) {
      is_hardcoded_params = true;
      option = option.substr(0, option.size() - 1);
   }

   // Checks
   if (fNPropagModels == 0) { // at most one model
      TUMessages::Warning(fOutputLog, "TURunPropagation", "Plots_XSProdRanking", "No propagation model loaded");
      fOutputLog = fout_ref;
      fout_ref = NULL;
      if (is_test) {
         fOutputLog = fout_ref;
         fout_ref = NULL;
      }
      return;
   } else if (fNPropagModels > 1) {
      string message = "Display result for first propagation model only ("
                       + fPropagModels[0]->GetModelName() + ")";
      TUMessages::Warning(fOutputLog, "TURunPropagation", "Plots_XSProdRanking", message);
   }

   ////////////////////////////////
   // USER SELECTION OF XS FILES //
   ////////////////////////////////
   string dir = "$USINE/inputs/XS_NUCLEI/";
   if (is_ghost_separately)
      dir += "GHOST_SEPARATELY/";
   string base = "sigProd*";
   string pattern = dir + base;
   vector<string> xs_files, ref_files;
   xs_files = TUMisc::ExtractFromDir(pattern, /*is_print*/ true, fOutputLog, /*is_selection*/ true, /*exclude_pattern*/"", is_test);
   if (xs_files.size() > 0) {
      for (Int_t i = 0; i < fPropagModels[0]->GetNFilesXProd(); ++i)
         ref_files.push_back(fPropagModels[0]->GetFileXProd(i));
   }
   const Int_t n_xsfiles = max(1, (Int_t)xs_files.size());

   /////////////////////////
   // USER SELECTION OF Z //
   /////////////////////////
   Int_t z2plot = 0;
   vector<Int_t> icrs2plot;
   string name_z = "";
   if (option != "D0") {
      fprintf(f_test, "\n\n");
      do {
         if (!is_test) {
            cout << indent << ">>> Contribution for isotopes/element of charge Z (|Z|>=1): ";
            cin >> z2plot;
         } else
            z2plot = 12;
         name_z = fPropagModels[0]->ZToElementName(z2plot);
         fPropagModels[0]->ElementNameToCRIndices(name_z, icrs2plot, !is_test);
         if (icrs2plot.size() == 0) {
            cout << indent << "   ---> No CR found for Z=" << z2plot << ", available Z in ["
                 << fPropagModels[0]->ExtractZMin() << "-" << fPropagModels[0]->ExtractZMax() << "]" << endl;
         }
      } while (icrs2plot.size() == 0);
   } else
      icrs2plot.push_back(0);   // Dummy (set here not to crash later)
   // Fill indices in storage vectors (first is for Z, rest for isotopes)
   const Int_t ncrs2plot = icrs2plot.size();
   Int_t nghosts_max = 0;
   if (fPropagModels[0]->IsGhostsLoaded()) {
      for (Int_t i = 0; i < ncrs2plot; ++i) {
         if (fPropagModels[0]->GetCREntry(icrs2plot[i]).IsChargedCR())
            nghosts_max = max(nghosts_max, fPropagModels[0]->GetCREntry(icrs2plot[i]).GetNGhosts());
      }
   }
   ////////////////////////////////
   // USER SELECTION OF ENERGIES //
   ////////////////////////////////
   vector<Double_t> ekn2plot;
   vector<Int_t> k2plot;
   string user_e = "";
   do {
      if (!is_test) {
         cout << indent << ">>> Select comma-separated list of energies (type 'D' for 1,10,100 GeV/n): ";
         cin >> user_e;
      } else
         user_e = "D";
      if (user_e == "D") {
         ekn2plot.push_back(1.);
         ekn2plot.push_back(10.);
         ekn2plot.push_back(100.);
         user_e = "1,10,100";
         break;
      } else {
         TUMisc::String2List(user_e, ",", ekn2plot);
         break;
      }
   } while (1);
   Int_t n_ekn2plot = ekn2plot.size();
   fprintf(f_test, "\n");
   // Find indices in EKN (we take any nucleus)
   for (Int_t ee = 0; ee < n_ekn2plot; ++ee)
      k2plot.push_back(fPropagModels[0]->IndexClosest(icrs2plot[0], ekn2plot[ee]));


   ////////////////////////////////////////////////
   // Model information and reference config/res //
   ////////////////////////////////////////////////
   string model = fPropagModels[0]->GetModelName();
   Double_t eps_normdata = fPropagModels[0]->GetEpsNormData();
   // X-sec (reference)
   TUXSections *ref_xs = new TUXSections(*fPropagModels[0]);
   // Local ISM medium
   TUMediumTXYZ *ism = fPropagModels[0]->GetISM();
   ism->FillMediumEntry(fPropagModels[0]->GetSolarSystemCoords());
   TUMediumEntry *local_ism = ism->GetMediumEntry();
   //local_ism->Print();
   ism = NULL;
   TUCoordTXYZ *sun_coord = fPropagModels[0]->GetSolarSystemCoords();
   // Keep reference propagation mode
   TUPropagSwitches *ref_propag = fPropagModels[0]->TUPropagSwitches::Clone();

   // Find list of CRs and elements in configuration
   Int_t jcr_min = 0;
   Int_t jcr_max = fPropagModels[0]->GetNCRs() - 1;
   for (Int_t j_cr = 0; j_cr < fPropagModels[0]->GetNCRs(); ++j_cr) {
      if (fPropagModels[0]->GetCREntry(j_cr).Getmamu() > 1 && fPropagModels[0]->GetCREntry(j_cr).GetZ() > 0) {
         jcr_min = j_cr;
         break;
      }
   }
   Int_t z_min = fPropagModels[0]->ExtractZMin();
   Int_t z_max = fPropagModels[0]->ExtractZMax();
   //Int_t z_min = fPropagModels[0]->GetCREntry(jcr_min).GetZ();
   //Int_t z_max = fPropagModels[0]->GetCREntry(jcr_max).GetZ();

   const Int_t ntot_nuc = fPropagModels[0]->GetNCRs() - jcr_min;
   const Int_t ne = fPropagModels[0]->GetNE();
   vector<Int_t> list_z = fPropagModels[0]->ExtractAllZ(false, z_min, z_max);
   const Int_t ntot_z = list_z.size();
   fprintf(f_test, "%s [%d nuclei in list from %s (Z=%d) to %s (Z=%d)]\n\n", indent.c_str(), ntot_nuc,
           fPropagModels[0]->GetCREntry(jcr_min).GetName().c_str(), z_min, fPropagModels[0]->GetCREntry(jcr_max).GetName().c_str(), z_max);


   //////////////////////////////
   // VECTORS TO STORE RESULTS //
   //////////////////////////////
   // *) Arrays for fractional contributions per process
   //    (primary/sec/rad...) for all CRs and Z (in CR list)
   const Int_t n_types = 7; // [0]=all, [1]=sec_frag, [2]=prim, [3]=sec_1step, [4]=sec_2step, [5]=sec_sup2step, [6]=rad
   const Int_t n_stats = 4; // [0]=mean, [1]=std_dev, [2]=min, [3]=max
   // Allocate/initialise using vectors (static arrays have less memory)
   //    prod_cr[n_xsfiles][ntot_nuc][n_types][ne]
   //    prod_z[n_xsfiles][ntot_z][n_types][ne]
   //    fracprod_cr_stat[n_stats][ntot_nuc][n_types][ne]
   //    fracprod_z_stat[n_stats][ntot_z][n_types][ne]
   vector<vector<vector<vector<Double_t> > > >
           prod_cr(n_xsfiles, vector<vector<vector<Double_t> > >(ntot_nuc, vector<vector<Double_t> >(n_types, vector<Double_t>(ne, 0.)))),
           prod_z(n_xsfiles, vector<vector<vector<Double_t> > >(ntot_nuc, vector<vector<Double_t> >(n_types, vector<Double_t>(ne, 0.)))),
           fracprod_cr_stat(n_stats, vector<vector<vector<Double_t> > >(ntot_nuc, vector<vector<Double_t> >(n_types, vector<Double_t>(ne, 0.)))),
           fracprod_z_stat(n_stats, vector<vector<vector<Double_t> > >(ntot_z, vector<vector<Double_t> >(n_types, vector<Double_t>(ne, 0.)))),
           prod_cr_stat(n_stats, vector<vector<vector<Double_t> > >(ntot_nuc, vector<vector<Double_t> >(n_types, vector<Double_t>(ne, 0.)))),
           prod_z_stat(n_stats, vector<vector<vector<Double_t> > >(ntot_z, vector<vector<Double_t> >(n_types, vector<Double_t>(ne, 0.))));
   // Initialise min to 1.e40
   for (Int_t t = 0; t < n_types; ++t) {
      for (Int_t k = 0; k < ne; ++k) {
         for (Int_t i = 0; i < ntot_nuc; ++i) {
            fracprod_cr_stat[2][i][t][k] = 1.e40;
            prod_cr_stat[2][i][t][k] = 1.e40;
         }
         for (Int_t i = 0; i < ntot_z; ++i) {
            fracprod_z_stat[2][i][t][k] = 1.e40;
            prod_z_stat[2][i][t][k] = 1.e40;
         }
      }
   }

   //  **) Arrays to store fractional reactions for selected Z
   //      and its isotopes: size related to number of parents.
   //       - D0. Relative contributions
   //       - D1. Ranking (propag.-weighted) of multi-step
   //       - D2a. Ranking (propag.-weighted) of individual XS
   //       - D2b. Ranking (propag.-weighted) of individual ghost-separated XS
   const Int_t n2plot = 1 + ncrs2plot;
   vector<Int_t> n_reacs(n2plot, 0);
   //Int_t n_targ = fPropagModels[0]->TUXSections::GetNTargets();
   Int_t n_tmp = 0;
   if (option == "D0") // Unused in that case
      n_tmp = 1;
   else {
      Int_t ncrs_supz2plot = jcr_max - icrs2plot[0];
      n_tmp = ((1 + ncrs_supz2plot) * (1 + ncrs_supz2plot) / 2) * ncrs2plot;
      if (option == "D2A")
         n_tmp *= fPropagModels[0]->GetNTargets();
      else if (option == "D2B")
         n_tmp *= fPropagModels[0]->GetNTargets() * (1 + nghosts_max);
   }
   const Int_t nreac2sort = n_tmp;
   // Allocate/initialise using vectors (static arrays have less memory)
   //    reac[n_xsfiles][n2plot][nreac2sort][ne]
   //    xs[n_xsfiles][n2plot][nreac2sort][ne]
   //    fracreac_stat[n_stats][n2plot][nreac2sort][ne]
   //    xs_stat[n_stats][n2plot][nreac2sort][ne]
   vector<vector<vector<vector<Double_t> > > >
        reac(n_xsfiles, vector<vector<vector<Double_t> > >(n2plot, vector<vector<Double_t> >(nreac2sort, vector<Double_t>(ne, 0.)))),
        xs(n_xsfiles, vector<vector<vector<Double_t> > >(n2plot, vector<vector<Double_t> >(nreac2sort, vector<Double_t>(ne, 0.)))),
        fracreac_stat(n_stats, vector<vector<vector<Double_t> > >(n2plot, vector<vector<Double_t> >(nreac2sort, vector<Double_t>(ne, 0.)))),
        reac_stat(n_stats, vector<vector<vector<Double_t> > >(n2plot, vector<vector<Double_t> >(nreac2sort, vector<Double_t>(ne, 0.)))),
        xs_stat(n_stats, vector<vector<vector<Double_t> > >(n2plot, vector<vector<Double_t> >(nreac2sort, vector<Double_t>(ne, 0.))));
   // Initialise min to 1.e40

   for (Int_t r = 0; r < nreac2sort; ++r) {
      for (Int_t k = 0; k < ne; ++k) {
         for (Int_t i = 0; i < n2plot; ++i) {
            fracreac_stat[2][i][r][k] = 1.e40;
            reac_stat[2][i][r][k] = 1.e40;
            xs_stat[2][i][r][k] = 1.e40;
         }
      }
   }
   // For each reaction and CR selected, store name for legend/texte/graph and color/style/marker (for plots)
   //    reac_leg_txt_gr[3][n2plot][nreac2sort]
   //    reac_col_sty_mkr[3][n2plot][nreac2sort]
   //    reac_is1step[n2plot][nreac2sort]
   vector<vector<vector<string> > > reac_leg_txt_gr(3, vector<vector<string> > (n2plot, vector<string>(nreac2sort, "")));
   vector<vector<vector<Int_t> > > reac_col_sty_mkr(3, vector<vector<Int_t> > (n2plot, vector<Int_t>(nreac2sort, 0)));
   vector<vector<Bool_t> >  reac_is1step(n2plot, vector<Bool_t>(nreac2sort, true));

   Bool_t is_extraverbose = false;
   Bool_t is_debug_info = false;
   if (n_xsfiles == 1 && ntot_nuc > 40)
      is_extraverbose = true;

   /////////////////////////////
   // CALCULATE CONTRIBUTIONS //
   /////////////////////////////
   // Normalised quantities (need to be rested for each XS)
   TUNormList *normlist_ref = fPropagModels[0]->TUNormList::Clone();

   for (Int_t i_xsfile = 0; i_xsfile < n_xsfiles; ++i_xsfile) { // Loop on various XS files
      if (n_xsfiles > 1) {
         string xs_current_file = xs_files[i_xsfile];
         TUMisc::SubstituteEnvInPath(xs_current_file, "$USINE");
         fprintf(f_test, "%s... process %d/%d using %s\n", indent.c_str(), i_xsfile + 1, n_xsfiles, xs_current_file.c_str());
      }
      // [EXPERT ONLY] UPDATE propagation for this XS files (if option selected by user)
      if (is_hardcoded_params) {
         fprintf(f_test, "%s     N.B.: use hardcoded propagation parameters!\n", indent.c_str());
         Int_t i_k0 = fPropagModels[0]->GetTransport()->IndexPar("K0");
         Int_t i_delta = fPropagModels[0]->GetTransport()->IndexPar("delta");
         Int_t i_va = fPropagModels[0]->GetTransport()->IndexPar("Va");
         Int_t i_vc = fPropagModels[0]->GetTransport()->IndexPar("Vc");

         if (i_xsfile == 0) {
            //GALP12
            fPropagModels[0]->GetTransport()->GetFreePars()->SetParVal(i_k0, 0.0622);
            fPropagModels[0]->GetTransport()->GetFreePars()->SetParVal(i_delta, 0.543);
            fPropagModels[0]->GetTransport()->GetFreePars()->SetParVal(i_va, 2.);
            fPropagModels[0]->GetTransport()->GetFreePars()->SetParVal(i_vc, 2.864);
         } else if (i_xsfile == 1) {
            //GAL22
            fPropagModels[0]->GetTransport()->GetFreePars()->SetParVal(i_k0, 0.0771);
            fPropagModels[0]->GetTransport()->GetFreePars()->SetParVal(i_delta, 0.517);
            fPropagModels[0]->GetTransport()->GetFreePars()->SetParVal(i_va, 2.35);
            fPropagModels[0]->GetTransport()->GetFreePars()->SetParVal(i_vc, 0.1);
         } else if (i_xsfile == 2) {
            //Soutoul 2001
            fPropagModels[0]->GetTransport()->GetFreePars()->SetParVal(i_k0, 0.02786);
            fPropagModels[0]->GetTransport()->GetFreePars()->SetParVal(i_delta, 0.643);
            fPropagModels[0]->GetTransport()->GetFreePars()->SetParVal(i_va, 2.0);
            fPropagModels[0]->GetTransport()->GetFreePars()->SetParVal(i_vc, 5.5);
         } else if (i_xsfile == 3) {
            //Webber 2003
            fPropagModels[0]->GetTransport()->GetFreePars()->SetParVal(i_k0, 0.0405);
            fPropagModels[0]->GetTransport()->GetFreePars()->SetParVal(i_delta, 0.5906);
            fPropagModels[0]->GetTransport()->GetFreePars()->SetParVal(i_va, 2.);
            fPropagModels[0]->GetTransport()->GetFreePars()->SetParVal(i_vc, 6.429);
         }
         // ....
      }

      //////////////////////////////////////////////
      // Reference flux for current configuration //
      //////////////////////////////////////////////
      // Normalised quantities (need to be reset for each XS)
      fPropagModels[0]->TUNormList::Copy(*normlist_ref);

      // Standard primary, secondary and radioactive mode should be on
      fPropagModels[0]->TUPropagSwitches::SetIsPrimStandard(true);
      fPropagModels[0]->TUPropagSwitches::SetIsDecayBETA(true);
      fPropagModels[0]->TUPropagSwitches::SetIsDecayFedBETA(true);
      fPropagModels[0]->TUPropagSwitches::SetIsDecayEC(true);
      fPropagModels[0]->TUPropagSwitches::SetIsDecayFedEC(true);
      fPropagModels[0]->TUPropagSwitches::SetIsSecondaries(true);
      // To avoid warning prints for antinuclei
      fPropagModels[0]->TUPropagSwitches::SetIsTertiaries(false);
      // X-sec (reference to reset XS for 1- and 2-step calculations)
      fPropagModels[0]->TUXSections::LoadOrUpdateProd(xs_files[i_xsfile], /*is_verbose*/ false, fOutputLog);
      TUXSections *current_xs = new TUXSections(*fPropagModels[0]);
      // Update ISM0D (to local value)
      fPropagModels[0]->FillInterRate_ISM0D(local_ism, true);
      fPropagModels[0]->SetPropagated(false);
      fPropagModels[0]->InitialiseForCalculation(/*is_init_or_update*/true,  /*is_force_update*/true, /*is_verbose*/false, f_test);

      // Reference fluxes
      Propagate(fIsUseNormList, false, 0, -1);

      ///////////////////////////////////////////////////
      // Extract prim., rad., all contribs for all CRs //
      ///////////////////////////////////////////////////
      // Calculate prim, total, total-secondary production
      for (Int_t j_cr = jcr_min; j_cr <= jcr_max; ++j_cr) {
         vector<Double_t> tmp_tot, tmp_prim, tmp_sec;
         fPropagModels[0]->FillFlux(tmp_tot, j_cr, sun_coord, 0/*tot0_prim1_sec2*/);
         fPropagModels[0]->FillFlux(tmp_prim, j_cr, sun_coord, 1/*tot0_prim1_sec2*/);
         fPropagModels[0]->FillFlux(tmp_sec, j_cr, sun_coord, 2/*tot0_prim1_sec2*/);
         // Store results
         Int_t icr = j_cr - jcr_min;
         Int_t iz = TUMisc::IndexInList(list_z, fPropagModels[0]->GetCREntry(j_cr).GetZ());
         for (Int_t k = 0; k < ne; ++k) {
            // Fill prod_cr[n_xsfiles][ntot_nuc][n_types][ne] and
            // prod_z[n_xsfiles][n_z][n_types][ne] for prim, sec_tot and tot
            // type: [0]=all, [1]=sec_frag, [2]=prim, [3]=sec_1step, [4]=sec_2step, [5]=sec_sup2step, [6]=rad
            prod_cr[i_xsfile][icr][0][k] = tmp_tot[k];  // [0] = all
            prod_cr[i_xsfile][icr][2][k] = tmp_prim[k]; // [2] = prim
            prod_cr[i_xsfile][icr][1][k] = tmp_sec[k];  // [1] = sec_frag (=sec-rad)
            prod_cr[i_xsfile][icr][5][k] = tmp_sec[k];  // [5] = sec_sup2step (=sec_frag-(sec_1step+sec_2step))
            // Add for charge all CRs having expected Z
            prod_z[i_xsfile][iz][0][k] += tmp_tot[k];  // [0] = all
            prod_z[i_xsfile][iz][2][k] += tmp_prim[k]; // [2] = prim
            prod_z[i_xsfile][iz][1][k] += tmp_sec[k];  // [1] = sec_frag (=sec-rad)
            prod_z[i_xsfile][iz][5][k] += tmp_sec[k];  // [5] = sec_sup2step (=sec_frag-(sec_1step+sec_2step))

            // DEBUG
            if (is_debug_info && k == k2plot[0])
               cout << fPropagModels[0]->GetCREntry(j_cr).GetName() << " all|prim|sec_tot="
                    << prod_cr[i_xsfile][icr][0][k] << "|" << prod_cr[i_xsfile][icr][2][k]
                    << "|" << prod_cr[i_xsfile][icr][1][k]  << endl;
         }
      }
      //Calculate decay-fed contributions
      for (Int_t j_cr = jcr_min; j_cr <= jcr_max; ++j_cr) {
         // Skip non DECAY-FED species
         if (!fPropagModels[0]->TUAxesCrE::TUCRList::IsDecayFed(j_cr))
            continue;
         else  {
            // Switch-off decay-fed for this nucleus
            string type = fPropagModels[0]->GetCREntry(j_cr).GetType();
            string beta_progenitor = fPropagModels[0]->GetCREntry(j_cr).GetBETACRFriend();
            string ec_progenitor = fPropagModels[0]->GetCREntry(j_cr).GetECCRFriend();
            fPropagModels[0]->UpdateType(j_cr, "STABLE");
            fPropagModels[0]->UpdateIndicesOfUnstables();
            // Propagate
            fPropagModels[0]->TUPropagSwitches::SetIsDecayBETA(true);
            fPropagModels[0]->TUPropagSwitches::SetIsDecayFedBETA(true);
            fPropagModels[0]->TUPropagSwitches::SetIsDecayEC(true);
            fPropagModels[0]->TUPropagSwitches::SetIsDecayFedEC(true);
            fPropagModels[0]->SetPropagated(false);
            fPropagModels[0]->InitialiseForCalculation(/*is_init_or_update*/true,  /*is_force_update*/true, /*is_verbose*/false, f_test);
            // Calculate rad (and update secondary)
            Propagate(false, false);
            vector<Double_t> nodecayfed;
            fPropagModels[0]->FillFlux(nodecayfed, j_cr, sun_coord, 0);
            // Reset CR properties (not stable)
            fPropagModels[0]->UpdateType(j_cr, type);
            fPropagModels[0]->UpdateCRFriend(j_cr, beta_progenitor, true);
            fPropagModels[0]->UpdateCRFriend(j_cr, ec_progenitor, false);
            fPropagModels[0]->UpdateIndicesOfUnstables();
            // Evaluate decay-fed contribution
            Int_t icr = j_cr - jcr_min;
            Int_t iz = TUMisc::IndexInList(list_z, fPropagModels[0]->GetCREntry(j_cr).GetZ());
            for (Int_t k = 0; k < ne; ++k) {
               // Subtraction precision cannot be better than precision
               // used for renormalisation of source abundances
               if (fabs((nodecayfed[k] - prod_cr[i_xsfile][icr][0][k]) / prod_cr[i_xsfile][icr][0][k]) > eps_normdata) {
                  Double_t contrib_rad = prod_cr[i_xsfile][icr][0][k] - nodecayfed[k];
                  // Fill prod_cr[n_xsfiles][ntot_nuc][n_types][ne] and
                  // prod_z[n_xsfiles][n_z][n_types][ne] for rad. contrib.
                  // type: [0]=all, [1]=sec_frag, [2]=prim, [3]=sec_1step, [4]=sec_2step, [5]=sec_sup2step, [6]=rad
                  prod_cr[i_xsfile][icr][6][k] = contrib_rad; // [6] = rad
                  prod_cr[i_xsfile][icr][1][k] -= contrib_rad;// [1] = sec_frag (=sec-rad)
                  prod_cr[i_xsfile][icr][5][k] -= contrib_rad;// [5] = sec_sup2step (=(sec-rad)-(sec_1step+sec_2step))
                  // Add for charge all CRs having expected Z
                  prod_z[i_xsfile][iz][6][k] += contrib_rad; // [6] = rad
                  prod_z[i_xsfile][iz][1][k] -= contrib_rad; // [1] = sec_frag (=sec-rad)
                  prod_z[i_xsfile][iz][5][k] -= contrib_rad; // [5] = sec_sup2step (=(sec-rad)-(sec_1step+sec_2step))
               }
               //
               if (is_debug_info && k == k2plot[0])
                  cout << fPropagModels[0]->GetCREntry(j_cr).GetName() << " all|prim|sec_tot="
                       << prod_cr[i_xsfile][icr][0][k] << "|" << prod_cr[i_xsfile][icr][2][k]
                       << "|" << prod_cr[i_xsfile][icr][1][k]  << endl;
            }
         }
      }
      fPropagModels[0]->SetPropagated(false);
      fPropagModels[0]->InitialiseForCalculation(/*is_init_or_update*/true,  /*is_force_update*/true, /*is_verbose*/false, f_test);
      Propagate(false, false);

      // Reset number of contribs (of Z (and CRs_with_Z) selected to zero
      for (Int_t r = 0; r < n2plot; ++r)
         n_reacs[r] = 0;


      ///////////////////////////////////////////////
      // Calculate 1-step and 2-step contributions //
      ///////////////////////////////////////////////
      if (option == "D0" || option.find("D1") != string::npos) {
         // Loop on progenitors (first one is heavier than jcr_min)
         for (Int_t jP = jcr_min + 1; jP <= jcr_max; ++jP) {
            string name_parent = fPropagModels[0]->TUCRList::GetCREntry(jP).GetName();
            fprintf(f_test, "%s   ... 1-step reactions %s->X\n", indent.c_str(), name_parent.c_str());
            //------------------------
            // 1-step reactions (P->F)
            //------------------------
            // Let us consider the CR indices (0 is the lighter and N-1 the heavier CR)
            //     N
            //     ...
            //     ...
            //     2
            //     0
            // Starting from the lighter CR, 1-step contributions to evaluate are
            //     0: none
            //     1: 1->0
            //     2: 2->0, 2->1
            //     3: 3->0, 3->1, 3-2
            //     ...
            //     N: N->0, N->1, N-2, ... N->N-1
            // This amounts to consider secondary contributions from primary jP only,
            // by setting all XS to zero but those involving jP, and also discarding
            // BETA-Fed and EC-Fed contributions
            fPropagModels[0]->TUPropagSwitches::SetIsDecayBETA(true);
            fPropagModels[0]->TUPropagSwitches::SetIsDecayFedBETA(false);
            fPropagModels[0]->TUPropagSwitches::SetIsDecayEC(true);
            fPropagModels[0]->TUPropagSwitches::SetIsDecayFedEC(false);

            // A) Reset XS + set to zero all reactions not jP as parent + propagate
            fPropagModels[0]->TUXSections::Copy(*current_xs);
            fPropagModels[0]->TUXSections::ResetProdZeroFor1StepReactionNotCRToX(jP);

            // Update ISM0D (to local value)
            fPropagModels[0]->FillInterRate_ISM0D(local_ism, true);
            // Propagate (without normalisation). Also, to
            // optimise calculation, only loop on [jcr_min,jP]
            fPropagModels[0]->SetPropagated(false);
            Propagate(false, false, jcr_min, jP);

            // B) Evaluate 1-step contribution P->F (loop on all fragments)
            for (Int_t ii = 0; ii < fPropagModels[0]->GetNFragments(jP); ++ii) {
               Int_t jF = fPropagModels[0]->IndexInCRList_Fragment(jP, ii);
               string name_frag = fPropagModels[0]->GetCREntry(jF).GetName();
               // Skip calculation if fragment not in list of nuclei
               if (jF < jcr_min || jF > jcr_max)
                  continue;
               //Int_t j_p = fPropagModels[0]->IndexInParentList(jF, name_parent);
               vector<Double_t> flux_parent_sec;
               fPropagModels[0]->FillFlux(flux_parent_sec, jF, sun_coord, 2/*Int_t tot0_prim1_sec2*/);
               // Add 1-step production channel for this reaction (P->F)
               // For fragment jF (in CR list), index in [jcr_min,jcrmax] and in 'list_z'
               //cout << indent << "            [process " << name_parent << "->" << name_frag << "]" << endl;
               Int_t icr = jF - jcr_min;
               Int_t iz = TUMisc::IndexInList(list_z, fPropagModels[0]->GetCREntry(jF).GetZ());
               // Fill prod_cr[n_xsfiles][ntot_nuc][n_types][ne] and prod_z[n_xsfiles][n_z][n_types][ne]
               // type: [0]=all, [1]=sec_frag, [2]=prim, [3]=sec_1step, [4]=sec_2step, [5]=sec_sup2step, [6]=rad
               for (Int_t k = 0; k < ne; ++k) {
                  prod_cr[i_xsfile][icr][3][k] += flux_parent_sec[k]; // [3]=sec_1step
                  prod_cr[i_xsfile][icr][5][k] -= flux_parent_sec[k]; // [5]=sec_sup2step = sec_frag - (sec_1step+sec_2step)
                  // Add for charge all CRs having expected Z
                  prod_z[i_xsfile][iz][3][k] += flux_parent_sec[k];   // [3]=sec_1step
                  prod_z[i_xsfile][iz][5][k] -= flux_parent_sec[k];   // [5]=sec_sup2step = sec_frag - (sec_1step+sec_2step)
                  //
                  if (is_debug_info && k == k2plot[0])
                     cout << fPropagModels[0]->GetCREntry(jF).GetName() << " all|prim|sec_tot="
                          << prod_cr[i_xsfile][icr][0][k] << "|" << prod_cr[i_xsfile][icr][2][k]
                          << "|" << prod_cr[i_xsfile][icr][1][k]  << endl;
               }

               if (option != "D0") {
                  // N.B.: if jF not in user-selection, skip as a contribution!
                  Int_t icr2plot = TUMisc::IndexInList(icrs2plot, jF);
                  // If index not in selection, this is not a contribution, skip
                  if (icr2plot < 0)
                     continue;

                  // This is a contribution (for CR selection)
                  for (Int_t k = ne - 1; k >= 0; --k) {
                     reac[i_xsfile][0][n_reacs[0]][k] = flux_parent_sec[k];  // for Z
                     reac[i_xsfile][1 + icr2plot][n_reacs[1 + icr2plot]][k] = flux_parent_sec[k]; // for CRs
                     // Store XS only (for straight-ahead only)
                     // contrib_xsH[icr2plot][i_contrib][k] = fPropagModels[0]->GetProdSig(j_p, /*tt*/0, jF, k);
                     // Double_t xs[n_xsfiles][1+ncrs2plot][nreac2sort][ne];
                  }
                  if (is_debug_info) {
                     cout << indent << "            [" << name_parent << "->" << name_frag
                          << ": add contrib@" << ekn2plot[0] << " GeV/n (" << flux_parent_sec[k2plot[0]] << ") "
                          << n_reacs[1 + icr2plot] << " for " << name_frag << " and  "
                          << n_reacs[0] << " for " << fPropagModels[0]->ZToElementName(list_z[iz]) << endl;
                  }
                  // Fill name/legends/text, color/style/marker for each reaction (for each CR selected)
                  string text = name_parent + "->" + name_frag;
                  string leg = fPropagModels[0]->GetNameROOT(jP) + "#rightarrow" + fPropagModels[0]->GetNameROOT(jF);
                  string gr_name = "prod1step_frac_" + model + "_" + name_parent + "_to_" + name_frag;
                  TUMisc::RemoveSpecialChars(gr_name);

                  // Name and info on current CR reaction (same for all XS files)
                  if (i_xsfile == 0) {
                     //   reac_leg_txt_gr[3][1+ncrs2plot][nreac2sort]
                     //   reac_col_sty_mkr[3][1+ncrs2plot][nreac2sort]
                     //   reac_is1step[1+ncrs2plot][nreac2sort]
                     Int_t iplot = 1 + icr2plot;
                     Int_t ireac_z = n_reacs[0];
                     Int_t ireac_cr = n_reacs[iplot];

                     // is 1-step?
                     reac_is1step[0][ireac_z] = true;
                     reac_is1step[iplot][ireac_cr] = true;
                     // reac name for leg/txt/gr
                     reac_leg_txt_gr[0][0][ireac_z] = leg;
                     reac_leg_txt_gr[0][iplot][ireac_cr] = leg;
                     reac_leg_txt_gr[1][0][ireac_z] = text;
                     reac_leg_txt_gr[1][iplot][ireac_cr] = text;
                     reac_leg_txt_gr[2][0][ireac_z] = gr_name;
                     reac_leg_txt_gr[2][iplot][ireac_cr] = gr_name;
                     // graph color/style/marker
                     reac_col_sty_mkr[0][0][ireac_z] = TUMisc::RootColor(jP % 10);
                     reac_col_sty_mkr[0][iplot][ireac_cr] = TUMisc::RootColor(jP % 10);
                     reac_col_sty_mkr[1][0][ireac_z] = jP % 7 + 1;
                     reac_col_sty_mkr[1][iplot][ireac_cr] = jP % 7 + 1;
                     reac_col_sty_mkr[2][0][ireac_z] = 0;
                     reac_col_sty_mkr[2][iplot][ireac_cr] = 0;
                  }

                  // Increment n_contrib
                  n_reacs[0] += 1;
                  n_reacs[1 + icr2plot] += 1;
               }
            }
            //--------------------------------
            // 2-steps reactions (P->inter->F)
            //--------------------------------
            // N.B.: at this point, we are within a loop on jP
            // Let us consider the CR indices (0 is the lighter and N-1 the heavier CR)
            //     N
            //     ...
            //     ...
            //     2
            //     0
            // Starting from the lighter CR, 2-step contributions to evaluate are
            //     0: none
            //     1: none
            //     2: 2->1->0
            //     3: 3->1->0
            //        3->2->1, 3->2->0
            //     ...
            //     N: N->1->0
            //        N->2->1, N->2->0
            //        ...
            //        N->N-1->N-2, N->N-1->N-3, ... N->N-1->0
            // In reactions jP->j_inter->jF, this amounts to consider secondary
            // contributions from primary jP in j_inter, itself fragmenting in jF,
            // with the source term for j_inter set to 0 (and as before no decay-fed)

            // Loop on j_inter, which will be fragments of jP
            Bool_t is_printed = false;
            for (Int_t ii = 0; ii < fPropagModels[0]->GetNFragments(jP); ++ii) {
               Int_t j_inter = fPropagModels[0]->IndexInCRList_Fragment(jP, ii);
               if (j_inter <= jcr_min)
                  continue;
               string name_inter = fPropagModels[0]->TUCRList::GetCREntry(j_inter).GetName();
               if (is_extraverbose)
                  fprintf(f_test, "%s        + 2-step reaction %s->%s->X\n",
                          indent.c_str(), name_parent.c_str(), name_inter.c_str());
               else {
                  if (!is_printed) {
                     fprintf(f_test, "%s        + 2-step reaction %s->INTER->X\n",
                             indent.c_str(), name_parent.c_str());
                     is_printed = true;
                  }
               }

               // A) Set to 0 primary source spectra for j_inter
               vector<Double_t> q_src_std, q_src_dm;
               for (Int_t s = 0; s < fPropagModels[0]->GetNSrcAstro(); ++s) {
                  Int_t j_species = fPropagModels[0]->GetSrcAstro(s)->IndexCRInSrcSpecies(j_inter);
                  if (j_species < 0)
                     q_src_std.push_back(0.);
                  else {
                     q_src_std.push_back(fPropagModels[0]->GetSrcAstro(s)->GetNormSrcSpectrum(j_species));
                     fPropagModels[0]->GetSrcAstro(s)->SetNormSrcSpectrum(j_species, 0.);
                  }
               }
               for (Int_t s = 0; s < fPropagModels[0]->GetNSrcDM(); ++s) {
                  Int_t j_species = fPropagModels[0]->GetSrcDM(s)->IndexCRInSrcSpecies(j_inter);
                  if (j_species < 0)
                     q_src_dm.push_back(0.);
                  else {
                     q_src_dm.push_back(fPropagModels[0]->GetSrcDM(s)->GetNormSrcSpectrum(j_species));
                     fPropagModels[0]->GetSrcDM(s)->SetNormSrcSpectrum(j_species, 0.);
                  }
               }

               // B) Reset XS + set to zero all reactions but P->j_inter and j_inter->jF + propagate
               fPropagModels[0]->TUXSections::Copy(*current_xs);
               fPropagModels[0]->TUXSections::ResetProdZeroFor2StepReactionNotCRToInterToX(jP, j_inter);
               // Update ISM0D (to local value)
               fPropagModels[0]->FillInterRate_ISM0D(local_ism, true);
               // Propagate (without normalisation), and to
               // optimise calculation, only loop on [jcr_min,jP]
               fPropagModels[0]->SetPropagated(false);
               Propagate(false, false, jcr_min, jP);


               // C) Evaluate 2-step contribution P->j_inter->F (loop on all fragments)
               for (Int_t jj = 0; jj < fPropagModels[0]->GetNFragments(j_inter); ++jj) {
                  Int_t jF = fPropagModels[0]->IndexInCRList_Fragment(j_inter, jj);
                  string name_frag = fPropagModels[0]->GetCREntry(jF).GetName();
                  // Skip calculation if fragment not in list of nuclei
                  if (jF < jcr_min || jF > jcr_max)
                     continue;
                  //Int_t j_p = fPropagModels[0]->IndexInParentList(jF, name_parent);
                  vector<Double_t> flux_parent_sec;
                  fPropagModels[0]->FillFlux(flux_parent_sec, jF, sun_coord, 2/*Int_t tot0_prim1_sec2*/);
                  // Add 2-step production channel for this reaction (P->j_inter->F)
                  // For fragment jF (in CR list), index in [jcr_min,jcrmax] and in 'list_z'
                  // cout << indent << "            [" << name_parent << "->" << name_inter << "->" << name_frag << "]" << endl;
                  Int_t i_cr = jF - jcr_min;
                  Int_t i_z = TUMisc::IndexInList(list_z, fPropagModels[0]->GetCREntry(jF).GetZ());
                  // Fill prod_cr[n_xsfiles][ntot_nuc][n_types][ne] and prod_z[n_xsfiles][n_z][n_types][ne]
                  // type: [0]=all, [1]=sec_frag, [2]=prim, [3]=sec_1step, [4]=sec_2step, [5]=sec_sup2step, [6]=rad
                  for (Int_t k = 0; k < ne; ++k) {
                     prod_cr[i_xsfile][i_cr][4][k] += flux_parent_sec[k]; // [4]=sec_2step
                     prod_cr[i_xsfile][i_cr][5][k] -= flux_parent_sec[k]; // [5]=sec_sup2step = sec_frag - (sec_1step+sec_2step)
                     // Add for charge all CRs having expected Z
                     prod_z[i_xsfile][i_z][4][k] += flux_parent_sec[k];   // [4]=sec_2step
                     prod_z[i_xsfile][i_z][5][k] -= flux_parent_sec[k];   // [5]=sec_sup2step = sec_frag - (sec_1step+sec_2step)
                  }


                  if (option != "D0") {
                     // N.B.: if jF not in user-selection, skip as a contribution!
                     Int_t icr2plot = TUMisc::IndexInList(icrs2plot, jF);
                     // If index not in selection, this is not a contribution, skip
                     if (icr2plot < 0)
                        continue;

                     // This is a contribution (for CR selection)
                     for (Int_t k = ne - 1; k >= 0; --k) {
                        reac[i_xsfile][0][n_reacs[0]][k] = flux_parent_sec[k];  // for Z
                        reac[i_xsfile][1 + icr2plot][n_reacs[1 + icr2plot]][k] = flux_parent_sec[k]; // for CRs
                        // Store XS only (for straight-ahead only)
                        // contrib_xsH[icr2plot][i_contrib][k] = fPropagModels[0]->GetProdSig(j_p, /*tt*/0, jF, k);
                        // Double_t xs[n_xsfiles][1+ncrs2plot][nreac2sort][ne];
                     }
                     if (is_debug_info) {
                        cout << indent << "            [" << name_parent << "->" << name_inter << "->" << name_frag
                             << ": add contrib@" << ekn2plot[0] << " GeV/n (" << flux_parent_sec[k2plot[0]] << ") "
                             << n_reacs[1 + icr2plot] << " for " << name_frag << " and "
                             << n_reacs[0] << " for " << fPropagModels[0]->ZToElementName(list_z[i_z]) << endl;
                     }

                     // Fill name/legends/text, color/style/marker for each reaction (for each CR selected)
                     string text = name_parent + "->" + name_inter + "->" + name_frag;
                     string leg = fPropagModels[0]->GetNameROOT(jP)
                                  + "#rightarrow" + fPropagModels[0]->GetNameROOT(j_inter)
                                  + "#rightarrow" + fPropagModels[0]->GetNameROOT(jF);
                     string gr_name = "prod2step_frac_" + model + "_" + name_parent + "_to_" + name_inter + "_to_" + name_frag;
                     TUMisc::RemoveSpecialChars(gr_name);

                     // Name and info on current CR reaction (same for all XS files)
                     if (i_xsfile == 0) {
                        //   reac_leg_txt_gr[3][1+ncrs2plot][nreac2sort]
                        //   reac_col_sty_mkr[3][1+ncrs2plot][nreac2sort]
                        //   reac_is1step[1+ncrs2plot][nreac2sort]
                        Int_t iplot = 1 + icr2plot;
                        Int_t ireac_z = n_reacs[0];
                        Int_t ireac_cr = n_reacs[iplot];
                        // is 1-step?
                        reac_is1step[0][ireac_z] = false;
                        reac_is1step[iplot][ireac_cr] = false;
                        // reac name for leg/txt/gr
                        reac_leg_txt_gr[0][0][ireac_z] = leg;
                        reac_leg_txt_gr[0][iplot][ireac_cr] = leg;
                        reac_leg_txt_gr[1][0][ireac_z] = text;
                        reac_leg_txt_gr[1][iplot][ireac_cr] = text;
                        reac_leg_txt_gr[2][0][ireac_z] = gr_name;
                        reac_leg_txt_gr[2][iplot][ireac_cr] = gr_name;
                        // graph color/style/marker
                        reac_col_sty_mkr[0][0][ireac_z] = TUMisc::RootColor(jP % 10);
                        reac_col_sty_mkr[0][iplot][ireac_cr] = TUMisc::RootColor(jP % 10);
                        reac_col_sty_mkr[1][0][ireac_z] = (j_inter - jF) % 7 + 1;
                        reac_col_sty_mkr[1][iplot][ireac_cr] = (j_inter - jF) % 7 + 1;
                        reac_col_sty_mkr[2][0][ireac_z] = 0;
                        reac_col_sty_mkr[2][iplot][ireac_cr] = 0;
                     }

                     // Increment n_contrib
                     n_reacs[0] += 1;
                     n_reacs[1 + icr2plot] += 1;
                  }
               }

               // Calculation (2-step) done: reset source for intermediary nucleus
               for (Int_t s = 0; s < fPropagModels[0]->GetNSrcAstro(); ++s) {
                  Int_t j_species = fPropagModels[0]->GetSrcAstro(s)->IndexCRInSrcSpecies(j_inter);
                  if (j_species < 0)
                     continue;
                  fPropagModels[0]->GetSrcAstro(s)->SetNormSrcSpectrum(j_species, q_src_std[s]);
               }
               for (Int_t s = 0; s < fPropagModels[0]->GetNSrcDM(); ++s) {
                  Int_t j_species = fPropagModels[0]->GetSrcDM(s)->IndexCRInSrcSpecies(j_inter);
                  if (j_species < 0)
                     continue;
                  fPropagModels[0]->GetSrcDM(s)->SetNormSrcSpectrum(j_species, q_src_dm[s]);
               }
            }
         }
      } else {
         // Option D2: loop on all possible reactions to see their impact on production

         // We need to remove from the normalisation the qty sought
         vector<Int_t> z_not2norm(1, z2plot);
         vector<vector<string> > crs_not2norm;
         vector<string> tmp_crs;
         fPropagModels[0]->ElementNameToCRNames(name_z, tmp_crs, false);
         crs_not2norm.push_back(tmp_crs);
         fPropagModels[0]->ElementNameToCRIndices(name_z, icrs2plot, !is_test);
         fPropagModels[0]->UpdateAndFillNotToNorm(z_not2norm, crs_not2norm, false, fOutputLog);

         // Loop on all fragments
         for (Int_t jF = jcr_min; jF <= jcr_max; ++jF) {
            // Skip if lighter than CR to evaluate
            if (jF < icrs2plot[0])
               continue;
            string name_frag = fPropagModels[0]->TUCRList::GetCREntry(jF).GetName();
            fprintf(f_test, "%s   ... reaction X-> %s\n", indent.c_str(), name_frag.c_str());
            Int_t g_max = -1;
            if (fPropagModels[0]->IsGhostsLoaded())
               g_max = fPropagModels[0]->GetCREntry(jF).GetNGhosts() - 1;
            for (Int_t jp = fPropagModels[0]->TUCRList::GetNParents(jF) - 1; jp >= 0; --jp) {
               Int_t jP = fPropagModels[0]->IndexInCRList_Parent(jF, jp);
               string name_parent = fPropagModels[0]->GetCREntry(jP).GetName();
               // Switch-off a single reaction at a time to evaluate its contribution
               fPropagModels[0]->TUPropagSwitches::SetIsDecayBETA(true);
               fPropagModels[0]->TUPropagSwitches::SetIsDecayFedBETA(true);
               fPropagModels[0]->TUPropagSwitches::SetIsDecayEC(true);
               fPropagModels[0]->TUPropagSwitches::SetIsDecayFedEC(true);

               for (Int_t t_targ = 0; t_targ < fPropagModels[0]->GetNTargets(); ++t_targ) {
                  string name_ghost = "";
                  for (Int_t g = g_max; g >= -1; --g) {
                     if (g >= 0)
                        name_ghost = fPropagModels[0]->GetCREntry(jF).GetGhost(g).GetName();
                     // A) Reset XS + set to zero reactions jp->jF (all targets and possible ghosts)
                     fPropagModels[0]->TUXSections::Copy(*current_xs);

                     for (Int_t k = 0; k < fPropagModels[0]->GetNE(); ++k)
                        fPropagModels[0]->SetProdSig(jp, t_targ, jF, k, 0., g);

                     // Update ISM0D (to local value)
                     fPropagModels[0]->FillInterRate_ISM0D(local_ism, true);
                     // --> Propagate with normalisation, because the XS contribution
                     // must be calculated in a 'standard run' configuration.
                     // --> All nuclei required to be propagated for correct
                     // calculation of impact on selected CR flux.
                     fPropagModels[0]->SetPropagated(false);
                     Propagate(true, false, jcr_min, -1);

                     // B) Store contribution
                     for (Int_t i = 0; i < ncrs2plot; ++i) {
                        vector<Double_t> tmp_tot;
                        fPropagModels[0]->FillFlux(tmp_tot, icrs2plot[i], sun_coord, 0/*Int_t tot0_prim1_sec2*/);
                        // Add channel for isotopes and element by comparing to
                        //    prod_cr[i_xsfile][icr][ntypes][k]
                        // type: [0]=all, [1]=sec_frag, [2]=prim, [3]=sec_1step, [4]=sec_2step, [5]=sec_sup2step, [6]=rad
                        //cout << indent << "            [process " << name_parent << "->" << name_frag << "]" << endl;
                        Int_t icr = icrs2plot[i] - jcr_min;
                        //printf("%.8g %.8g  %.8g\n", tmp_tot[k2plot[0]], prod_cr[i_xsfile][icr][0][k2plot[0]],
                        //       prod_cr[i_xsfile][icr][0][k2plot[0]]- tmp_tot[k2plot[0]]);
                        for (Int_t k = ne - 1; k >= 0; --k) {
                           Double_t contrib = 0.;
                           // Subtraction precision cannot be better than precision
                           // used for renormalisation of source abundances
                           if (prod_cr[i_xsfile][icr][0][k] > 1.e-40 && tmp_tot[k] > 1.e-40
                                 && fabs((prod_cr[i_xsfile][icr][0][k] - tmp_tot[k]) / prod_cr[i_xsfile][icr][0][k] > eps_normdata))
                              contrib = prod_cr[i_xsfile][icr][0][k] - tmp_tot[k];

                           // Also discard too small XS
                           if (xs[i_xsfile][0][n_reacs[0]][ne - 1] < 1.e-40)
                              contrib = 0.;
                           reac[i_xsfile][0][n_reacs[0]][k] += contrib;  // for Z
                           reac[i_xsfile][1 + i][n_reacs[1 + i]][k] = contrib; // for CRs
                           // Store XS (for straight-ahead only)
                           xs[i_xsfile][0][n_reacs[0]][k] = current_xs->GetProdSig(jp, t_targ, jF, k, g);
                           xs[i_xsfile][1 + i][n_reacs[1 + i]][k] = current_xs->GetProdSig(jp, t_targ, jF, k, g);
                           // Double_t xs[n_xsfiles][1+ncrs2plot][nreac2sort][ne];
                           if (is_debug_info && k == k2plot[0]) {
                              cout << indent << "            [" << name_parent << "+"
                                   << fPropagModels[0]->GetNameTarget(t_targ) << "->(" << name_ghost
                                   << ")" << name_frag << ": add contrib@" << ekn2plot[0] << " GeV/n ("
                                   << contrib << ", sigma=" << xs[i_xsfile][0][n_reacs[0]][k] << "[mb]) "
                                   << n_reacs[1 + i] << " for " << fPropagModels[0]->GetCREntry(icrs2plot[i]).GetName() << " and  "
                                   << n_reacs[0] << " for " << fPropagModels[0]->ZToElementName(z2plot) << endl;
                           }
                        }
                        // Fill name/legends/text, color/style/marker for each reaction (for each CR selected)
                        string text = "sigma(" + name_parent + "+" + fPropagModels[0]->GetNameTarget(t_targ) + "->" + name_frag + ")";
                        string leg = fPropagModels[0]->GetNameROOT(jP) + "+" + fPropagModels[0]->GetNameTarget(t_targ) + "#rightarrow" + fPropagModels[0]->GetNameROOT(jF);
                        string gr_name = "prod1step_frac_" + model + "_" + name_parent + "_on_" + fPropagModels[0]->GetNameTarget(t_targ) + "_to_" + name_frag;
                        if (g >= 0) {
                           text = "sigma(" + name_parent + "+" + fPropagModels[0]->GetNameTarget(t_targ) + "->(" + name_ghost + ")" + name_frag + ")";
                           leg = fPropagModels[0]->GetNameROOT(jP) + "+" + fPropagModels[0]->GetNameTarget(t_targ) + "#rightarrow(" + name_ghost + ")" + fPropagModels[0]->GetNameROOT(jF);
                           gr_name = "prod1step_frac_" + model + "_" + name_parent + "_on_" + fPropagModels[0]->GetNameTarget(t_targ) + "_to_" + name_ghost + "_" + name_frag;
                        }
                        TUMisc::RemoveSpecialChars(gr_name);

                        // Name and info on current CR reaction (same for all XS files)
                        if (i_xsfile == 0) {
                           //   reac_leg_txt_gr[3][1+ncrs2plot][nreac2sort]
                           //   reac_col_sty_mkr[3][1+ncrs2plot][nreac2sort]
                           //   reac_is1step[1+ncrs2plot][nreac2sort]
                           Int_t iplot = 1 + i;
                           Int_t ireac_z = n_reacs[0];
                           Int_t ireac_cr = n_reacs[iplot];
                           // reac name for leg/txt/gr
                           reac_leg_txt_gr[0][0][ireac_z] = leg;
                           reac_leg_txt_gr[0][iplot][ireac_cr] = leg;
                           reac_leg_txt_gr[1][0][ireac_z] = text;
                           reac_leg_txt_gr[1][iplot][ireac_cr] = text;
                           reac_leg_txt_gr[2][0][ireac_z] = gr_name;
                           reac_leg_txt_gr[2][iplot][ireac_cr] = gr_name;
                           // graph color/style/marker
                           reac_col_sty_mkr[0][0][ireac_z] = TUMisc::RootColor(jP % 10);
                           reac_col_sty_mkr[0][iplot][ireac_cr] = TUMisc::RootColor(jP % 10);
                           reac_col_sty_mkr[1][0][ireac_z] = jF % 7 + 1;
                           reac_col_sty_mkr[1][iplot][ireac_cr] = jF % 7 + 1;
                           reac_col_sty_mkr[2][0][ireac_z] = 0;
                           reac_col_sty_mkr[2][iplot][ireac_cr] = 0;
                        }
                        // Increment n_contrib
                        n_reacs[1 + i] += 1;
                     }
                     n_reacs[0] += 1;
                  }
               }
            }
         }
      }

      // ... and delete
      if (current_xs)
         delete current_xs;
      current_xs = NULL;

      // Calculate stat quantities:
      // --------------------------
      //    fracprod_cr_stat[n_stats][ntot_nuc][n_types][ne]
      //    fracprod_z_stat[n_stats][ntot_z][n_types][ne]
      // from
      //    prod_cr[n_xsfiles][ntot_nuc][n_types][ne]
      //    prod_z[n_xsfiles][ntot_z][n_types][ne]
      // n_types: [0]=all, [1]=sec_frag, [2]=prim, [3]=sec_1step, [4]=sec_2step, [5]=sec_sup2step, [6]=rad
      // n_stats: [0]=mean, [1]=std_dev, [2]=min, [3]=max
      for (Int_t t = 0; t < n_types; ++t) {
         for (Int_t k = 0; k < ne; ++k) {
            for (Int_t j_cr = jcr_min; j_cr <= jcr_max; ++j_cr) {
               Int_t i_cr = j_cr - jcr_min;
               Double_t frac_wrt_tot = 0.;
               // If total null, fraction is 100% only for secondary
               if (prod_cr[i_xsfile][i_cr][0][k] > 1.e-40)
                  frac_wrt_tot = 100.* prod_cr[i_xsfile][i_cr][t][k] / prod_cr[i_xsfile][i_cr][0][k];
               else if (t == 1)
                  frac_wrt_tot = 100.;
               // N.B.: special case for t=0, do not calculate fraction, just total!
               if (t == 0) frac_wrt_tot = prod_cr[i_xsfile][i_cr][t][k];
               fracprod_cr_stat[0][i_cr][t][k] += frac_wrt_tot;
               fracprod_cr_stat[1][i_cr][t][k] += frac_wrt_tot * frac_wrt_tot;
               fracprod_cr_stat[2][i_cr][t][k] = min(frac_wrt_tot, fracprod_cr_stat[2][i_cr][t][k]);
               fracprod_cr_stat[3][i_cr][t][k] = max(frac_wrt_tot, fracprod_cr_stat[3][i_cr][t][k]);
               prod_cr_stat[0][i_cr][t][k] += prod_cr[i_xsfile][i_cr][t][k];
               prod_cr_stat[1][i_cr][t][k] += prod_cr[i_xsfile][i_cr][t][k] * prod_cr[i_xsfile][i_cr][t][k];
               prod_cr_stat[2][i_cr][t][k] = min(prod_cr[i_xsfile][i_cr][t][k], prod_cr_stat[2][i_cr][t][k]);
               prod_cr_stat[3][i_cr][t][k] = max(prod_cr[i_xsfile][i_cr][t][k], prod_cr_stat[3][i_cr][t][k]);
            }
            for (Int_t i = 0; i < ntot_z; ++i) {
               Double_t frac_wrt_tot = 0.;
               // If total null, fraction is 100% only for secondary
               if (prod_z[i_xsfile][i][0][k] > 1.e-40)
                  frac_wrt_tot = 100.*prod_z[i_xsfile][i][t][k] / prod_z[i_xsfile][i][0][k];
               else if (t == 1)
                  frac_wrt_tot = 100.;
               // N.B.: special case for t=0, do not calculate fraction, just total!
               if (t == 0) frac_wrt_tot = prod_z[i_xsfile][i][t][k];
               fracprod_z_stat[0][i][t][k] += frac_wrt_tot;
               fracprod_z_stat[1][i][t][k] += frac_wrt_tot * frac_wrt_tot;
               fracprod_z_stat[2][i][t][k] = min(frac_wrt_tot, fracprod_z_stat[2][i][t][k]);
               fracprod_z_stat[3][i][t][k] = max(frac_wrt_tot, fracprod_z_stat[3][i][t][k]);
               prod_z_stat[0][i][t][k] += prod_z[i_xsfile][i][t][k];
               prod_z_stat[1][i][t][k] += prod_z[i_xsfile][i][t][k] * prod_z[i_xsfile][i][t][k];
               prod_z_stat[2][i][t][k] = min(prod_z[i_xsfile][i][t][k], prod_z_stat[2][i][t][k]);
               prod_z_stat[3][i][t][k] = max(prod_z[i_xsfile][i][t][k], prod_z_stat[3][i][t][k]);
            }
         }
      }
      // and
      //    fracreac_stat[n_stats][1+ncrs2plot][nreac2sort][ne]
      //    xs_stat[n_stats][1+ncrs2plot][nreac2sort][ne]
      // from
      //    reac[n_xsfiles][1+ncrs2plot][nreac2sort][ne]
      //    xs[n_xsfiles][1+ncrs2plot][nreac2sort][ne];
      // n_stats: [0]=mean, [1]=std_dev, [2]=min, [3]=max
      Int_t i_z = TUMisc::IndexInList(list_z, z2plot);
      for (Int_t k = 0; k < ne; ++k) {
         for (Int_t r = 0; r < n_reacs[0]; ++r) {
            Double_t frac_wrt_sectot = 0.;
            if (prod_z[i_xsfile][i_z][1][k] > 1.e-40)
               frac_wrt_sectot = 100.*reac[i_xsfile][0][r][k] / prod_z[i_xsfile][i_z][1][k]; //[1]=sec_frag
            fracreac_stat[0][0][r][k] += frac_wrt_sectot;
            fracreac_stat[1][0][r][k] += frac_wrt_sectot * frac_wrt_sectot;
            fracreac_stat[2][0][r][k] = min(frac_wrt_sectot, fabs(fracreac_stat[2][0][r][k]));
            fracreac_stat[3][0][r][k] = max(frac_wrt_sectot, fabs(fracreac_stat[3][0][r][k]));
            xs_stat[0][0][r][k] += xs[i_xsfile][0][r][k];
            xs_stat[1][0][r][k] += xs[i_xsfile][0][r][k] * xs[i_xsfile][0][r][k];
            xs_stat[2][0][r][k] = min(xs[i_xsfile][0][r][k], xs_stat[2][0][r][k]);
            xs_stat[3][0][r][k] = max(xs[i_xsfile][0][r][k], xs_stat[3][0][r][k]);
         }
      }
      for (Int_t i = 0; i < ncrs2plot; ++i) {
         Int_t i_cr = icrs2plot[i] - jcr_min;
         for (Int_t k = 0; k < ne; ++k) {
            for (Int_t r = 0; r < n_reacs[1 + i]; ++r) {
               Double_t frac_wrt_sectot = 0.;
               if (prod_cr[i_xsfile][i_cr][1][k] > 1.e-40)
                  frac_wrt_sectot = 100.*reac[i_xsfile][1 + i][r][k] / prod_cr[i_xsfile][i_cr][1][k]; //[1]=sec_frag
               fracreac_stat[0][1 + i][r][k] += frac_wrt_sectot;
               fracreac_stat[1][1 + i][r][k] += frac_wrt_sectot * frac_wrt_sectot;
               fracreac_stat[2][1 + i][r][k] = min(frac_wrt_sectot, fabs(fracreac_stat[2][1 + i][r][k]));
               fracreac_stat[3][1 + i][r][k] = max(frac_wrt_sectot, fabs(fracreac_stat[3][1 + i][r][k]));
               xs_stat[0][1 + i][r][k] += xs[i_xsfile][1 + i][r][k];
               xs_stat[1][1 + i][r][k] += xs[i_xsfile][1 + i][r][k] * xs[i_xsfile][1 + i][r][k];
               xs_stat[2][1 + i][r][k] = min(xs[i_xsfile][1 + i][r][k], xs_stat[2][1 + i][r][k]);
               xs_stat[3][1 + i][r][k] = max(xs[i_xsfile][1 + i][r][k], xs_stat[3][1 + i][r][k]);
               //
               if (is_debug_info && k == k2plot[0])
                  cout << fPropagModels[0]->GetCREntry(icrs2plot[i]).GetName() << "  frac=" << fracreac_stat[0][1 + i][r][k]
                       << "  reac=" << reac[i_xsfile][1 + i][r][k] << endl;
            }
         }
      }
   }

   ////////////////////////////////////////////////
   // Calculate mean/std for all stat quantities //
   ////////////////////////////////////////////////
   // n_types: [0]=all, [1]=sec_frag, [2]=prim, [3]=sec_1step, [4]=sec_2step, [5]=sec_sup2step, [6]=rad
   // n_stats = [0]=mean, [1]=std_dev, [2]=min, [3]=max
   for (Int_t t = 0; t < n_types; ++t) {
      for (Int_t k = 0; k < ne; ++k) {
         for (Int_t j_cr = jcr_min; j_cr <= jcr_max; ++j_cr) {
            Int_t i_cr = j_cr - jcr_min;
            // With ex=sum_n(x_i) and ex2=sum_n(x_i*x_i)
            // var = (ex2 - (ex*ex)/n) / (n-1)
            // mean = ex/n
            Double_t ex = fracprod_cr_stat[0][i_cr][t][k];
            Double_t ex2 = fracprod_cr_stat[1][i_cr][t][k];
            fracprod_cr_stat[0][i_cr][t][k] = ex / (Double_t)n_xsfiles;
            // Subtraction precision cannot be better than precision
            // used for renormalisation of source abundances
            Double_t tmp = ex2 - (ex * ex) / (Double_t)n_xsfiles;
            if (ex2 > 1.e-40 && fabs(tmp) > 1.e-40 && fabs(tmp / ex2) > eps_normdata)
               fracprod_cr_stat[1][i_cr][t][k] = sqrt(tmp / (Double_t)n_xsfiles);
            else
               fracprod_cr_stat[1][i_cr][t][k] = 0.;

            // For flux
            ex = prod_cr_stat[0][i_cr][t][k];
            ex2 = prod_cr_stat[1][i_cr][t][k];
            prod_cr_stat[0][i_cr][t][k] = ex / (Double_t)n_xsfiles;
            // Subtraction precision cannot be better than precision
            // used for renormalisation of source abundances
            tmp = ex2 - (ex * ex) / (Double_t)n_xsfiles;
            if (ex2 > 1.e-40 && fabs(tmp) > 1.e-40 && fabs(tmp / ex2) > eps_normdata)
               prod_cr_stat[1][i_cr][t][k] = sqrt(tmp / (Double_t)n_xsfiles);
            else
               prod_cr_stat[1][i_cr][t][k] = 0.;

         }
         for (Int_t i_z = 0; i_z < ntot_z; ++i_z) {
            // With ex=sum_n(x_i) and ex2=sum_n(x_i*x_i)
            // var = (ex2 - (ex*ex)/n) / (n-1)
            // mean = ex/n
            Double_t ex = fracprod_z_stat[0][i_z][t][k];
            Double_t ex2 = fracprod_z_stat[1][i_z][t][k];
            fracprod_z_stat[0][i_z][t][k] = ex / (Double_t)n_xsfiles;
            // Subtraction precision cannot be better than precision
            // used for renormalisation of source abundances
            Double_t tmp = ex2 - (ex * ex) / (Double_t)n_xsfiles;
            if (ex2 > 1.e-40 && fabs(tmp) > 1.e-40 && fabs(tmp / ex2) > eps_normdata)
               fracprod_z_stat[1][i_z][t][k] = sqrt(tmp / (Double_t)n_xsfiles);
            else
               fracprod_z_stat[1][i_z][t][k] = 0.;

            // For flux
            ex = prod_z_stat[0][i_z][t][k];
            ex2 = prod_z_stat[1][i_z][t][k];
            prod_z_stat[0][i_z][t][k] = ex / (Double_t)n_xsfiles;
            // Subtraction precision cannot be better than precision
            // used for renormalisation of source abundances
            tmp = ex2 - (ex * ex) / (Double_t)n_xsfiles;
            if (ex2 > 1.e-40 && fabs(tmp) > 1.e-40 && fabs(tmp / ex2) > eps_normdata)
               prod_z_stat[1][i_z][t][k] = sqrt(tmp / (Double_t)n_xsfiles);
            else
               prod_z_stat[1][i_z][t][k] = 0.;
         }
      }
   }
   for (Int_t p = 0; p < n2plot; ++p) {
      for (Int_t r = 0; r < n_reacs[p]; ++r) {
         for (Int_t k = 0; k < ne; ++k) {
            // With ex=sum_n(x_i) and ex2=sum_n(x_i*x_i)
            // var = (ex2 - (ex*ex)/n) / (n-1)
            // mean = ex/n
            Double_t ex = fracreac_stat[0][p][r][k];
            Double_t ex2 = fracreac_stat[1][p][r][k];
            fracreac_stat[0][p][r][k] = ex / (Double_t)n_xsfiles;
            // Subtraction precision cannot be better than precision
            // used for renormalisation of source abundances
            Double_t tmp = ex2 - (ex * ex) / (Double_t)n_xsfiles;
            if (ex2 > 1.e-40 && fabs(tmp) > 1.e-40 && fabs(tmp / ex2) > eps_normdata)
               fracreac_stat[1][p][r][k] = sqrt(tmp / (Double_t)n_xsfiles);
            else
               fracreac_stat[1][p][r][k] = 0.;

            // For XS
            ex = xs_stat[0][p][r][k];
            ex2 = xs_stat[1][p][r][k];
            xs_stat[0][p][r][k] = ex / (Double_t)n_xsfiles;
            // Subtraction precision cannot be better than precision
            // used for renormalisation of source abundances
            tmp = ex2 - (ex * ex) / (Double_t)n_xsfiles;
            if (ex2 > 1.e-40 && tmp > 1.e-40 && (tmp / ex2) > eps_normdata)
               xs_stat[1][p][r][k] = sqrt(tmp / (Double_t)n_xsfiles);
            else
               xs_stat[1][p][r][k] = 0.;
         }
      }
   }

   // Reset to initial propagation properties
   fPropagModels[0]->TUNormList::Copy(*normlist_ref);
   fPropagModels[0]->TUXSections::Copy(*ref_xs);
   fPropagModels[0]->FillInterRate_ISM0D(local_ism, true);
   fPropagModels[0]->TUPropagSwitches::Copy(*ref_propag);
   delete ref_propag;
   ref_propag = NULL;
   fPropagModels[0]->ClearFilesXProd();
   for (Int_t i = 0; i < (Int_t)ref_files.size(); ++i)
      fPropagModels[0]->TUXSections::LoadOrUpdateProd(TUMisc::GetPath(ref_files[i]), /*is_verbose*/ false, fOutputLog);
   fPropagModels[0]->InitialiseForCalculation(/*is_init_or_update*/true,  /*is_force_update*/false, /*is_verbose*/false, f_test);
   fPropagModels[0]->SetPropagated(false);
   delete ref_xs;
   ref_xs = NULL;
   delete normlist_ref;
   normlist_ref = NULL;
   local_ism = NULL;
   TUMessages::Indent(false);

   //////////////////////////////////////////
   //  PRINT/PLOT ALL CR/ELEMENT FRACTIONS //
   //////////////////////////////////////////
   TUMisc::RootSetStylePlots();
   TText *usine_txt = TUMisc::OrphanUSINEAdOnPlots();

   if (option == "D0") {
      const Int_t n_fractype = 5;
      string type_leg[n_fractype] = {"Primary", "Secondary (1-step)", "Secondary (2-step)", "Secondary (> 2-step)", "Decay-fed"};
      string type_name[n_fractype] = {"prim", "sec_1step", "sec_2step", "sec_sup2step", "rad"};

      // PRINT
      // -----
      const Int_t n_save = 2;
      Int_t ntot[n_save] = {ntot_nuc, ntot_z};
      string prefix[n_save] = {"/fraccontrib_nuc_", "/fraccontrib_z_"};
      for (Int_t f = 0; f < n_save; ++f) {
         string file_save = fOutputDir + prefix[f] + fPropagModels[0]->GetModelName() + "_IS.out";
         FILE *fp_save = NULL;
         if (is_test)
            fp_save = f_test;
         else
            fp_save = fopen(file_save.c_str(), "w");
         TUMisc::PrintUSINEHeader(fp_save, !is_test);
         fprintf(fp_save, "# %s\n", fPropagModels[0]->GetModelName().c_str());

         // Header
         fprintf(fp_save, "#\n");
         fprintf(fp_save, "# Format:\n");
         fprintf(fp_save, "# ------\n");
         fprintf(fp_save, "# Name   ");
         for (Int_t tt = 0; tt < n_fractype; ++tt)
            fprintf(fp_save, "%-22s", ("frac(" + type_name[tt] + ")@").c_str());
         fprintf(fp_save, "\n");
         fprintf(fp_save, "#        ");
         for (Int_t tt = 0; tt < n_fractype; ++tt)
            fprintf(fp_save, "%-22s", (user_e + " [GeV/n]").c_str());
         if (n_xsfiles > 1)
            fprintf(fp_save, "\n# N.B.: use %d XS files to provide for each energy  mean(std_dev|min|max)", n_xsfiles);
         fprintf(fp_save, "\n\n");

         for (Int_t i = 0; i < ntot[f]; ++i) {
            if (f == 0)
               fprintf(fp_save, "%-9s", fPropagModels[0]->GetCREntry(i + jcr_min).GetName().c_str());
            else
               fprintf(fp_save, "%-9s", fPropagModels[0]->TUAxesCrE::ZToElementName(list_z[i]).c_str());
            for (Int_t tt = 0; tt < n_fractype; ++tt) {
               for (Int_t ee = 0; ee < n_ekn2plot; ++ee) {
                  Int_t k = k2plot[ee];
                  // N.B.: fracprod_cr_stat[n_stats][ntot_nuc][n_fractypes][ne] is based on
                  //    n_fractypes: [0]=all, [1]=sec_frag, [2]=prim, [3]=sec_1step, [4]=sec_2step, [5]=sec_sup2step, [6]=rad
                  //    n_stats: [0]=mean, [1]=std_dev, [2]=min, [3]=max
                  // whereas we fill 0="prim", 1="sec_1step", 2="sec_2step", 3="sec_sup2step", 4="rad"};
                  Double_t val = 0.;
                  if (f == 0) val = fracprod_cr_stat[0][i][tt + 2][k];
                  else val = fracprod_z_stat[0][i][tt + 2][k];
                  if (n_xsfiles == 1) {
                     if (!std::isnan(fabs(val)))
                        fprintf(fp_save, "%le ", val);
                     else
                        fprintf(fp_save, "      -      ");
                  } else {
                     if (!std::isnan(fabs(val))) {
                        fprintf(fp_save, "%le(%.2le|%.2le|%.2le)", val, fracprod_cr_stat[1][i][tt + 2][k],
                                fracprod_cr_stat[2][i][tt + 2][k], fracprod_cr_stat[3][i][tt + 2][k]);
                     } else
                        fprintf(fp_save, "            -             ");
                  }
               }
               fprintf(fp_save, "    ");
            }
            fprintf(fp_save, "\n");
         }
         if (!is_test) {
            fclose(fp_save);
            cout << "  => Saved in " << file_save << endl;
         }
      }

      // PLOT
      // ----
      TH1D ***hfrac_gcr_cr = new TH1D **[n_ekn2plot];
      TH1D ***hfrac_gcr_z = new TH1D **[n_ekn2plot];
      TH1D ***h_gcr_cr = new TH1D **[n_ekn2plot];
      TH1D ***h_gcr_z = new TH1D **[n_ekn2plot];
      for (Int_t ee = 0; ee < n_ekn2plot; ++ee) {
         hfrac_gcr_cr[ee] = new TH1D*[n_fractype];
         hfrac_gcr_z[ee] = new TH1D*[n_fractype];
         h_gcr_cr[ee] = new TH1D*[n_fractype];
         h_gcr_z[ee] = new TH1D*[n_fractype];
         for (Int_t ii = 0; ii < n_fractype; ++ii) {
            hfrac_gcr_cr[ee][ii] = NULL;
            hfrac_gcr_z[ee][ii] = NULL;
            h_gcr_cr[ee][ii] = NULL;
            h_gcr_z[ee][ii] = NULL;
         }
      }
      Double_t bar_skip = 0.2;
      Double_t bar_width = (1. - 2 * bar_skip) / (Double_t)(n_ekn2plot);
      string text_energies;
      for (Int_t ee = 0; ee < n_ekn2plot; ++ee)
         text_energies = text_energies + (string)Form("%.2f ", ekn2plot[ee]);
      string c_name = Form("frac_contrib_all_%s", model.c_str());
      string c_title = Form("Flux and fractional contributions at %s GeV/n", text_energies.c_str());
      TCanvas *c_all = new TCanvas(c_name.c_str(), c_title.c_str(), 1000, 900);
      c_all->SetWindowPosition(2 * c_all->GetWw(), 0);
      c_all->Divide(2, 3);
      TLegend *leg_all = new TLegend(0.2, 0.1, 0.9, 0.8);
      string leg_header = Form("#splitline{Contribution per process}{at %d energies: %s GeV/n}",
                               n_ekn2plot, text_energies.c_str());
      leg_all->SetHeader(leg_header.c_str());

      // Loop on energies
      for (Int_t ee = 0; ee < n_ekn2plot; ++ee) {
         // Loop on types (prim, sec...)
         vector<Double_t> tot_cr(ntot_nuc, 100.);
         vector<Double_t> tot_z(ntot_z, 100.);
         for (Int_t tt = 0; tt < n_fractype; ++tt) {
            // Fill isotopic contribs.
            // -----------------------
            string hfrac_name_cr = Form("histo_contribfrac_all_crs_%s_%s_e%d", model.c_str(), type_name[tt].c_str(), ee);
            hfrac_gcr_cr[ee][tt] = fPropagModels[0]->TUAxesCrE::OrphanEmptyHistoOfCRs(hfrac_name_cr.c_str(), "", fPropagModels[0]->GetCREntry(jcr_min).GetName(), fPropagModels[0]->GetCREntry(jcr_max).GetName());
            hfrac_gcr_cr[ee][tt]->SetName(hfrac_name_cr.c_str());
            hfrac_gcr_cr[ee][tt]->GetYaxis()->SetTitle("Fractional contribution [%]");
            string h_name_cr = Form("histo_contrib_all_crs_%s_%s_e%d", model.c_str(), type_name[tt].c_str(), ee);
            h_gcr_cr[ee][tt] = fPropagModels[0]->TUAxesCrE::OrphanEmptyHistoOfCRs(h_name_cr.c_str(), "", fPropagModels[0]->GetCREntry(jcr_min).GetName(), fPropagModels[0]->GetCREntry(jcr_max).GetName());
            h_gcr_cr[ee][tt]->SetName(h_name_cr.c_str());
            h_gcr_cr[ee][tt]->GetYaxis()->SetTitle("Flux [(GeV/n)^{-1}  m^{-2} s^{-1} sr^{-1}]");
            // N.B.: fracprod_cr_stat[n_stats][ntot_nuc][n_fractypes][ne] is based on
            //    n_fractypes: [0]=all, [1]=sec_frag, [2]=prim, [3]=sec_1step, [4]=sec_2step, [5]=sec_sup2step, [6]=rad
            //    n_stats: [0]=mean, [1]=std_dev, [2]=min, [3]=max
            // whereas we fill 0="prim", 1="sec_1step", 2="sec_2step", 3="sec_sup2step", 4="rad"};
            for (Int_t i = 0; i < h_gcr_cr[ee][tt]->GetNbinsX(); ++i) {
               Int_t k = fPropagModels[0]->IndexClosest(jcr_min, ekn2plot[ee]);
               Double_t val = fracprod_cr_stat[0][i][tt + 2][k];
               if (val > eps_normdata) {
                  hfrac_gcr_cr[ee][tt]->SetBinContent(i + 1, tot_cr[i]);
                  h_gcr_cr[ee][tt]->SetBinContent(i + 1, prod_cr_stat[0][i][tt + 2][k]);
                  //cout << val << " " << fracprod_cr_stat[1][i][tt + 2][k] << endl;
                  //if (n_xsfiles > 1)
                  //   h_gcr_cr[ee][tt]->SetBinError(i + 1, fracprod_cr_stat[1][i][tt + 2][k]);
               }
               tot_cr[i] -= val;
            }
            c_all->cd(1);
            hfrac_gcr_cr[ee][tt]->SetLineWidth(2);
            hfrac_gcr_cr[ee][tt]->SetLineColor(TUMisc::RootColor(tt));
            hfrac_gcr_cr[ee][tt]->SetFillStyle(1001);
            hfrac_gcr_cr[ee][tt]->SetFillColor(TUMisc::RootColor(tt));
            hfrac_gcr_cr[ee][tt]->SetBarWidth(bar_width);
            hfrac_gcr_cr[ee][tt]->SetBarOffset(bar_skip + ee * bar_width);
            hfrac_gcr_cr[ee][tt]->Draw("bar,PSAME");
            usine_txt->Draw();
            c_all->Modified();
            c_all->Update();

            c_all->cd(2);
            h_gcr_cr[ee][tt]->SetLineWidth(2);
            h_gcr_cr[ee][tt]->SetLineColor(TUMisc::RootColor(tt));
            h_gcr_cr[ee][tt]->SetFillStyle(1001);
            h_gcr_cr[ee][tt]->SetFillColor(TUMisc::RootColor(tt));
            h_gcr_cr[ee][tt]->SetBarWidth(bar_width);
            h_gcr_cr[ee][tt]->SetBarOffset(bar_skip + ee * bar_width);
            h_gcr_cr[ee][tt]->Draw("bar,PSAME");
            usine_txt->Draw();
            c_all->Modified();
            c_all->Update();

            // Legend
            if (ee == 0) {
               TLegendEntry *entry = leg_all->AddEntry(h_gcr_cr[ee][tt], type_leg[tt].c_str(), "F");
               entry->SetTextColor(TUMisc::RootColor(tt));
            }
            // Fill elemental contributions
            // -----------------------------
            string hfrac_name_elem = Form("histo_contribfrac_all_elem_%s_%s_e%d", model.c_str(), type_name[tt].c_str(), ee);
            hfrac_gcr_z[ee][tt] = fPropagModels[0]->TUAxesCrE::OrphanEmptyHistoOfElements(hfrac_name_elem.c_str(), "",
                                  fPropagModels[0]->TUAxesCrE::ZToElementName(z_min), fPropagModels[0]->TUAxesCrE::ZToElementName(z_max));
            hfrac_gcr_z[ee][tt]->SetName(hfrac_name_elem.c_str());
            hfrac_gcr_z[ee][tt]->GetYaxis()->SetTitle("Fractional contribution [%]");
            string h_name_elem = Form("histo_contrib_all_elem_%s_%s_e%d", model.c_str(), type_name[tt].c_str(), ee);
            h_gcr_z[ee][tt] = fPropagModels[0]->TUAxesCrE::OrphanEmptyHistoOfElements(h_name_elem.c_str(), "",
                              fPropagModels[0]->TUAxesCrE::ZToElementName(z_min), fPropagModels[0]->TUAxesCrE::ZToElementName(z_max));
            h_gcr_z[ee][tt]->SetName(h_name_elem.c_str());
            h_gcr_z[ee][tt]->GetYaxis()->SetTitle("Flux [(GeV/n)^{-1}  m^{-2} s^{-1} sr^{-1}]");
            for (Int_t i = 0; i < h_gcr_z[ee][tt]->GetNbinsX(); ++i) {
               Int_t k = fPropagModels[0]->IndexClosest(jcr_min, ekn2plot[ee]);
               Double_t val = fracprod_z_stat[0][i][tt + 2][k];
               if (val > eps_normdata) {
                  hfrac_gcr_z[ee][tt]->SetBinContent(i + 1, tot_z[i]);
                  h_gcr_z[ee][tt]->SetBinContent(i + 1, prod_z_stat[0][i][tt + 2][k]);
               }
               tot_z[i] -= val;
            }
            c_all->cd(3);
            hfrac_gcr_z[ee][tt]->SetLineWidth(2);
            hfrac_gcr_z[ee][tt]->SetLineColor(TUMisc::RootColor(tt));
            hfrac_gcr_z[ee][tt]->SetFillStyle(1001);
            hfrac_gcr_z[ee][tt]->SetFillColor(TUMisc::RootColor(tt));
            hfrac_gcr_z[ee][tt]->SetBarWidth(bar_width);
            hfrac_gcr_z[ee][tt]->SetBarOffset(bar_skip + ee * bar_width);
            hfrac_gcr_z[ee][tt]->Draw("bar,PSAME");
            usine_txt->Draw();
            c_all->Modified();
            c_all->Update();

            c_all->cd(4);
            h_gcr_z[ee][tt]->SetLineWidth(2);
            h_gcr_z[ee][tt]->SetLineColor(TUMisc::RootColor(tt));
            h_gcr_z[ee][tt]->SetFillStyle(1001);
            h_gcr_z[ee][tt]->SetFillColor(TUMisc::RootColor(tt));
            h_gcr_z[ee][tt]->SetBarWidth(bar_width);
            h_gcr_z[ee][tt]->SetBarOffset(bar_skip + ee * bar_width);
            h_gcr_z[ee][tt]->Draw("bar,PSAME");
            usine_txt->Draw();
            c_all->Modified();
            c_all->Update();
         }
      }
      for (Int_t iii = 1; iii < 5; ++iii) {
         c_all->cd(iii);
         gPad->SetLogx(0);
         if (iii == 1 || iii == 3)
            gPad->SetLogy(0);
         else
            gPad->SetLogy(1);
      }
      c_all->cd(5);
      leg_all->Draw();
      c_all->cd(6);
      leg_all->Draw();
      c_all->Modified();
      c_all->Update();


      // Enter event loop, one can now interact with the objects in
      // the canvas. Select "Exit ROOT" from Canvas "File" menu to exit
      // the event loop and execute the next statements.
      if (!gROOT->IsBatch())
         app->Run(kTRUE);

      // Free memory of CRs and elements
      for (Int_t ee = 0; ee < n_ekn2plot; ++ee) {
         for (Int_t tt = 0; tt < n_fractype; ++tt) {
            if (hfrac_gcr_cr[ee][tt]) delete hfrac_gcr_cr[ee][tt];
            hfrac_gcr_cr[ee][tt] = NULL;
            if (hfrac_gcr_z[ee][tt]) delete hfrac_gcr_z[ee][tt];
            hfrac_gcr_z[ee][tt] = NULL;
            if (h_gcr_cr[ee][tt]) delete h_gcr_cr[ee][tt];
            h_gcr_cr[ee][tt] = NULL;
            if (h_gcr_z[ee][tt]) delete h_gcr_z[ee][tt];
            h_gcr_z[ee][tt] = NULL;
         }
         delete[]  hfrac_gcr_cr[ee];
         hfrac_gcr_cr[ee] = NULL;
         delete[]  hfrac_gcr_z[ee];
         hfrac_gcr_z[ee] = NULL;
         delete[]  h_gcr_cr[ee];
         h_gcr_cr[ee] = NULL;
         delete[]  h_gcr_z[ee];
         h_gcr_z[ee] = NULL;
      }
      if (hfrac_gcr_cr) delete[] hfrac_gcr_cr;
      hfrac_gcr_cr = NULL;
      if (hfrac_gcr_z) delete[] hfrac_gcr_z;
      hfrac_gcr_z = NULL;
      if (h_gcr_cr) delete[] h_gcr_cr;
      h_gcr_cr = NULL;
      if (h_gcr_z) delete[] h_gcr_z;
      h_gcr_z = NULL;
      delete c_all;
      c_all = NULL;
      delete leg_all;
      leg_all = NULL;
   }


   /////////////////////////////////////////////
   //  PRINT/PLOT SELECTED REACTIONS (SORTED) //
   /////////////////////////////////////////////
   if (option != "D0") {
      // Deal with low-level contributions and plot (contributions in range [0-1%])
      const Int_t n_ranges = 3;
      Double_t ranges[n_ranges + 1] = {0., 0.01, 0.1, 1.};
      Double_t fracmax_plot = ranges[n_ranges];
      // Create range of contributions, and all falling in this range (total and number of reactions).
      vector<vector<vector<Int_t> > > n_in_range(n2plot, vector<vector<Int_t> >(n_ekn2plot, vector<Int_t>(n_ranges, 0)));
      vector<vector<vector<Double_t> > > sum_range(n2plot, vector<vector<Double_t> >(n_ekn2plot, vector<Double_t>(n_ranges, 0.)));
      vector<vector<vector<Bool_t> > > is_in_ranges(n2plot, vector<vector<Bool_t> >(nreac2sort, vector<Bool_t>(n_ekn2plot, false)));
      // Create vector of pair to enable sorting according to relative contribution
      vector<vector<Int_t> > ireac_sorted(n2plot, vector<Int_t>(nreac2sort, 0));

      // SORT CONTRIBUTIONS (based on lowest selected E)
      // ------------------
      for (Int_t i2plot = 0; i2plot < n2plot; ++i2plot) {
         // Number of contribs
         Int_t n_reac = n_reacs[i2plot];
         if (is_debug_info) {
            cout << indent << "Check @ Ekn=" << ekn2plot[0] << " [GeV/n]" << endl;
            cout << indent << "  " << i2plot << " [BEFORE SORTING]" << endl;
            for (Int_t r = n_reac - 1; r >= 0; --r)
               cout << indent << fracreac_stat[0][i2plot][r][k2plot[0]] << " " << r << endl;
         }
         // Copy in temporary vector
         vector<pair<Double_t, Int_t> > tmp_sort;
         for (Int_t r = n_reac - 1; r >= 0; --r) {
            pair<Double_t, Int_t>  tmp_pair;
            tmp_pair.first = fabs(fracreac_stat[0][i2plot][r][k2plot[0]]);
            tmp_pair.second = r;
            tmp_sort.push_back(tmp_pair);
         }
         // Sort
         std::sort(tmp_sort.begin(), tmp_sort.end());
         // Copy in contribs_frac_sorted
         for (Int_t r = n_reac - 1; r >= 0; --r)
            ireac_sorted[i2plot][r] = tmp_sort[r].second;
         // Check
         if (is_debug_info) {
            cout << indent << "  " << i2plot << " [AFTER SORTING]" << endl;
            for (Int_t r = n_reac - 1; r >= 0; --r) {
               cout << indent << fracreac_stat[0][i2plot][ireac_sorted[i2plot][r]][k2plot[0]] << " " << ireac_sorted[i2plot][r] << endl;
            }
         }
         // ASSIGN CONTRIBUTION RANGE
         // -------------------------
         vector<Double_t> check_sum(n_ekn2plot, 0.);
         for (Int_t r = 0; r < n_reac; ++r) {
            // Loop on all selected energies
            for (Int_t ee = 0; ee < n_ekn2plot; ++ee) {
               Int_t k = k2plot[ee];
               // Get fraction for contribution from
               //    fracreac_stat[n_stats][1+ncrs2plot][nreac2sort][ne]
               Double_t frac = fracreac_stat[0][i2plot][r][k];
               check_sum[ee] += fracreac_stat[0][i2plot][r][k];
               // If falls as small contribution (in range), add it
               if (frac <= ranges[n_ranges]) {
                  // Update is_in_range
                  is_in_ranges[i2plot][r][ee] = true;
                  // Add
                  for (Int_t rr = 0; rr < n_ranges; ++rr) {
                     if (frac > ranges[rr] && frac < ranges[rr + 1]) {
                        sum_range[i2plot][ee][rr] +=  frac;
                        n_in_range[i2plot][ee][rr] += 1;
                     }
                  }
               }
            }
         }
         // PRINT SORTED (for selected energies)
         // ------------
         string name_frag = "";
         if (i2plot == 0) name_frag = fPropagModels[0]->ZToElementName(z2plot);
         else name_frag = fPropagModels[0]->GetCREntry(icrs2plot[i2plot - 1]).GetName();

         string prefix = "";
         if (option.find("D1") != string::npos)
            prefix = "/sortedchannelsfor_";
         else if (option.find("D2A") != string::npos)
            prefix = "/sortedxsfor_";
         else if (option.find("D2B") != string::npos)
            prefix = "/sortedxsghostfor_";
         string file_save = fOutputDir + prefix + name_frag + "_" + fPropagModels[0]->GetModelName() + "_IS.out";
         FILE *fp_save = NULL;
         if (is_test)
            fp_save = f_test;
         else fp_save = fopen(file_save.c_str(), "w");
         // Header
         TUMisc::PrintUSINEHeader(fp_save, !is_test);
         fprintf(fp_save, "# %s\n", fPropagModels[0]->GetModelName().c_str());
         fprintf(fp_save, "#\n");
         fprintf(fp_save, "# Format:\n");
         fprintf(fp_save, "# ------\n");
         // We use fracprod_cr_stat[n_stats][ntot_nuc][n_types][ne] and fracprod_z_stat[n_stats][ntot_z][n_types][ne];
         // n_types: [0]=all, [1]=sec_frag, [2]=prim, [3]=sec_1step, [4]=sec_2step, [5]=sec_sup2step, [6]=rad
         fprintf(fp_save, "#  Contribution fractions for %s  @  %s\n", name_frag.c_str(), (user_e + " [GeV/n]").c_str());
         for (Int_t t_type = 1; t_type < 7; ++t_type) {
            if (t_type == 0) fprintf(fp_save, "#    flux-tot[] =          ");
            if (t_type == 1) fprintf(fp_save, "#    sec-prod =           ");
            else if (t_type == 2) fprintf(fp_save, "#        prim =           ");
            else if (t_type == 6) fprintf(fp_save, "#         rad =           ");
            else continue;
            for (Int_t ee = 0; ee < n_ekn2plot; ++ee) {
               if (i2plot == 0) {
                  Int_t iz = TUMisc::IndexInList(list_z, z2plot);
                  fprintf(fp_save, "   %le", fracprod_z_stat[0][iz][t_type][k2plot[ee]]);
               } else {
                  Int_t icr = icrs2plot[i2plot - 1] - jcr_min;
                  fprintf(fp_save, "   %le", fracprod_cr_stat[0][icr][t_type][k2plot[ee]]);
               }
            }
            fprintf(fp_save, "\n");
         }
         // Print flux for element or isotope (depending on file in which to write)
         fprintf(fp_save, "#  Flux [(GeV/n)/cm^2/s/str] and isotope/element fraction  @  %s\n", (user_e + " [GeV/n]").c_str());
         fprintf(fp_save, "#        flux =           ");
         for (Int_t ee = 0; ee < n_ekn2plot; ++ee) {
            if (i2plot == 0)
               fprintf(fp_save, "   %le", fracprod_z_stat[0][TUMisc::IndexInList(list_z, z2plot)][0][k2plot[ee]]);
            else
               fprintf(fp_save, "   %le", fracprod_cr_stat[0][icrs2plot[i2plot - 1] - jcr_min][0][k2plot[ee]]);
            fprintf(fp_save, "\n");
         }
         // Print isotopic/elemental flux fraction
         for (Int_t i_isot =  1; i_isot < n2plot; ++i_isot) {
            Int_t iz = TUMisc::IndexInList(list_z, z2plot);
            Int_t icr = icrs2plot[i_isot - 1] - jcr_min;
            fprintf(fp_save, "#     isot/elem (%s/%s) =",
                    fPropagModels[0]->GetCREntry(icrs2plot[i_isot - 1]).GetName().c_str(),
                    fPropagModels[0]->ZToElementName(z2plot).c_str());
            for (Int_t ee = 0; ee < n_ekn2plot; ++ee)
               fprintf(fp_save, "   %le", fracprod_cr_stat[0][icr][0][k2plot[ee]] / fracprod_z_stat[0][iz][0][k2plot[ee]]);
            fprintf(fp_save, "\n");
         }

         if (option == "D1")
            fprintf(fp_save, "#\n# Sorted 1&2-step reactions        Fraction of tot.sec.prod.        Cumulative (at same energies)\n");
         else
            fprintf(fp_save, "#\n# Sorted XS                Involved in %% of tot.sec.prod.          XS value at same energies [mb]\n");
         if (n_xsfiles > 1)
            fprintf(fp_save, "# N.B.: use %d XS files to provide for each energy  mean(std_dev|min|max)\n", n_xsfiles);
         fprintf(fp_save, "\n");
         // Print fractions (sorted)
         vector<Double_t> cumul(n_ekn2plot, 0.);
         for (Int_t r = n_reac - 1; r >= 0; --r) {
            Int_t ir_sorted = ireac_sorted[i2plot][r];
            // Print only if non-null contribution
            if (fracreac_stat[0][i2plot][ir_sorted][k2plot[0]] > 1.e-40) {
               fprintf(fp_save, "%-25s", reac_leg_txt_gr[1][i2plot][ir_sorted].c_str());
               for (Int_t ee = 0; ee < n_ekn2plot; ++ee) {
                  if (n_xsfiles == 1)
                     fprintf(fp_save, "   %le", fracreac_stat[0][i2plot][ir_sorted][k2plot[ee]]);
                  else
                     fprintf(fp_save, " %le(%le|%le|%le)",
                             fracreac_stat[0][i2plot][ir_sorted][k2plot[ee]],
                             fracreac_stat[1][i2plot][ir_sorted][k2plot[ee]],
                             fracreac_stat[2][i2plot][ir_sorted][k2plot[ee]],
                             fracreac_stat[3][i2plot][ir_sorted][k2plot[ee]]);
               }
               fprintf(fp_save, "      ");
               // Cumulative
               if (option.find("D1") != string::npos) {
                  for (Int_t ee = 0; ee < n_ekn2plot; ++ee) {
                     cumul[ee] += fracreac_stat[0][i2plot][ir_sorted][k2plot[ee]];
                     fprintf(fp_save, "   %le", cumul[ee]);
                  }
               } else {
                  for (Int_t ee = 0; ee < n_ekn2plot; ++ee) {
                     if (n_xsfiles == 1)
                        fprintf(fp_save, "   %le", xs_stat[0][i2plot][ir_sorted][k2plot[ee]]);
                     else
                        fprintf(fp_save, "%le(%le|%le|%le)",
                                xs_stat[0][i2plot][ir_sorted][k2plot[ee]],
                                xs_stat[1][i2plot][ir_sorted][k2plot[ee]],
                                xs_stat[2][i2plot][ir_sorted][k2plot[ee]],
                                xs_stat[3][i2plot][ir_sorted][k2plot[ee]]);
                  }
               }
               fprintf(fp_save, "\n");
            }
         }
         // Check sum (contains only 1 and 2 step prod)
         if (option.find("D1") != string::npos) {
            fprintf(fp_save, "\n# Sec. prod.:\n");
            fprintf(fp_save, "#     => Total  =          ");
            for (Int_t ee = 0; ee < n_ekn2plot; ++ee) {
               Double_t check = check_sum[ee];
               if (option.find("D1") != string::npos) {
                  if (i2plot == 0) {
                     Int_t iz = TUMisc::IndexInList(list_z, z2plot);
                     check += 100.*fracprod_z_stat[0][iz][5][k2plot[ee]] / fracprod_z_stat[0][iz][1][k2plot[ee]];
                  } else {
                     Int_t icr = icrs2plot[i2plot - 1] - jcr_min;
                     check +=  100.*fracprod_cr_stat[0][icr][5][k2plot[ee]] / fracprod_cr_stat[0][icr][1][k2plot[ee]];
                  }
               }
               fprintf(fp_save, "   %.3le", check);
            }
            fprintf(fp_save, "\n");
            fprintf(fp_save, "#  with \n");
            // We use fracprod_cr_stat[n_stats][ntot_nuc][n_types][ne] and fracprod_z_stat[n_stats][ntot_z][n_types][ne];
            // n_types: [0]=all, [1]=sec_frag, [2]=prim, [3]=sec_1step, [4]=sec_2step, [5]=sec_sup2step, [6]=rad
            for (Int_t t_type = 3; t_type < 6; ++t_type) {
               if (t_type == 3) fprintf(fp_save, "#        1-step =          ");
               else if (t_type == 4) fprintf(fp_save, "#        2-step =          ");
               else if (t_type == 5) fprintf(fp_save, "#       >2-step =          ");
               for (Int_t ee = 0; ee < n_ekn2plot; ++ee) {
                  if (i2plot == 0) {
                     Int_t iz = TUMisc::IndexInList(list_z, z2plot);
                     fprintf(fp_save, "   %.3le", 100.*fracprod_z_stat[0][iz][t_type][k2plot[ee]] / fracprod_z_stat[0][iz][1][k2plot[ee]]);
                  } else {
                     Int_t icr = icrs2plot[i2plot - 1] - jcr_min;
                     fprintf(fp_save, "   %.3le", 100.*fracprod_cr_stat[0][icr][t_type][k2plot[ee]] / fracprod_cr_stat[0][icr][1][k2plot[ee]]);
                  }
               }
               fprintf(fp_save, "\n");
            }
            // Add contributions in range
            fprintf(fp_save, "#\n# N.B.: number and sum for small individual contributions (1- and 2- step only):\n");
            for (Int_t r = n_ranges - 1; r >= 0; --r) {
               fprintf(fp_save, "#   in [%.3f,%.3f]        ", ranges[r], ranges[r + 1]);
               for (Int_t ee = 0; ee < n_ekn2plot; ++ee)
                  fprintf(fp_save, "%4d=>%5.2f   ", n_in_range[i2plot][ee][r], sum_range[i2plot][ee][r]);
               fprintf(fp_save, "\n");
            }
         }
         if (!is_test) {
            fclose(fp_save);
            cout << "  => Saved in " << file_save << endl;
         }
      }


      //////////////////////////
      //         PLOTS        //
      // Multi-step fractions //
      //////////////////////////

      // Canvas a multigraph
      TCanvas **c_check_frac = new TCanvas*[n2plot];
      TMultiGraph **mg_frac = new TMultiGraph*[n2plot];
      TLegend ***leg_frac = new TLegend **[n_ekn2plot];
      for (Int_t ee = 0; ee < n_ekn2plot; ++ee)
         leg_frac[ee] = new TLegend*[n2plot];
      for (Int_t i = 0; i < n2plot; ++i) {
         c_check_frac[i] = NULL;
         mg_frac[i] = NULL;
         for (Int_t ee = 0; ee < n_ekn2plot; ++ee)
            leg_frac[ee][i] = NULL;
      }

      // 1) Loop to plot per CR [flux-weighted or pure Xprod]
      //--------------------------
      string ext = "reactions";
      string y_title = "Secondary fractional contributions [%]";

      if (option.find("D2") != string::npos) {
         ext = "xs";
         y_title = "XS [mb]";
      }
      for (Int_t i2plot = 0; i2plot < n2plot; ++i2plot) {
         //cout << "i_cr=" << i2plot << "/" << ncrs2plot << endl;

         // Allocate and initialise canvas and multigraph only for CRs to plot
         // Create canvas for each fragment
         string name_frag = "";
         if (i2plot == 0)
            name_frag = fPropagModels[0]->ZToElementName(z2plot);
         else
            name_frag = fPropagModels[0]->GetCREntry(icrs2plot[i2plot - 1]).GetName();
         TUMisc::RemoveSpecialChars(name_frag);

         // Name of canvas
         string name = Form("check_contrib1step_to_%s_%s", name_frag.c_str(), ext.c_str());
         string title = "";
         if (option.find("D1") != string::npos)
            title = Form("%s: Ordered 1- and 2-step reactions >%.1f%% [%s]", name_frag.c_str(), fracmax_plot, model.c_str());
         else if (option.find("D2A") != string::npos)
            title = Form("%s: Ordered XS yielding >%.1f%% contrib. [%s]", name_frag.c_str(), fracmax_plot, model.c_str());
         else if (option.find("D2B") != string::npos)
            title = Form("%s: Ordered XS yielding >%.1f%% contrib. (with ghosts) [%s]", name_frag.c_str(), fracmax_plot, model.c_str());
         c_check_frac[i2plot] = new TCanvas(((string)name + "_frac").c_str(), title.c_str());
         c_check_frac[i2plot]->SetWindowPosition(0, i2plot * 100);
         //c_check_frac[i2plot]->SetGridx();
         //c_check_frac[i2plot]->SetGridy();
         c_check_frac[i2plot]->SetLogx(1);
         c_check_frac[i2plot]->SetLogy(0);
         // Allocate and fill multigraphs
         string mg_name = "mg_frac_" + (string)name;
         string mg_title = "mg_frac_" + (string)title;
         //cout << mg_name << " " << mg_title << endl;
         mg_frac[i2plot] = new TMultiGraph(mg_name.c_str(), mg_title.c_str());
         // set titles (and axes names)
         string x_title = TUEnum::Enum2NameUnit(kEKN);
         vector<string> tmp;
         TUMisc::String2List(x_title, "[]", tmp);
         string mg_titles = ";" + x_title + ";" + y_title;
         mg_frac[i2plot] = new TMultiGraph(mg_name.c_str(), mg_title.c_str());
         mg_frac[i2plot]->SetTitle(mg_titles.c_str());

         // Plot according to sort
         TUAxis *e_axis = fPropagModels[0]->GetEFamily(kNUC);

         for (Int_t ee = 0; ee < n_ekn2plot; ++ee) {
            // Loop on all graph
            Int_t k = k2plot[ee];
            string header = "";
            Double_t val_prim, val_rad, val_sec, val_1step, val_2step, val_sup2;
            // fracprod_cr_stat[n_stats][ntot_nuc][n_types][ne]
            // fracprod_z_stat[n_stats][ntot_z][n_types][ne]
            //    n_types: [0]=all, [1]=sec_frag, [2]=prim, [3]=sec_1step, [4]=sec_2step, [5]=sec_sup2step, [6]=rad
            //    n_stats: [0]=mean, [1]=std_dev, [2]=min, [3]=max
            if (i2plot == 0) {
               Int_t iz = TUMisc::IndexInList(list_z, z2plot);
               val_sec   = fracprod_z_stat[0][iz][1][k];
               val_prim  = fracprod_z_stat[0][iz][2][k];
               val_1step = fracprod_z_stat[0][iz][3][k];
               val_2step = fracprod_z_stat[0][iz][4][k];
               val_sup2  = fracprod_z_stat[0][iz][5][k];
               val_rad   = fracprod_z_stat[0][iz][6][k];
            } else {
               Int_t icr = icrs2plot[i2plot - 1] - jcr_min;
               val_sec   = fracprod_cr_stat[0][icr][1][k];
               val_prim  = fracprod_cr_stat[0][icr][2][k];
               val_1step = fracprod_cr_stat[0][icr][3][k];
               val_2step = fracprod_cr_stat[0][icr][4][k];
               val_sup2  = fracprod_cr_stat[0][icr][5][k];
               val_rad   = fracprod_cr_stat[0][icr][6][k];
            }
            if (option.find("D1") != string::npos) {
               header = Form("%.1f GeV/n: %.1f%%(prim)+%.1f%%(sec)",
                             ekn2plot[ee], val_prim, val_sec);
               if (val_rad > 1.e-3)
                  header = header + Form("+%.1f%%(rad)", val_rad);
               header = "#splitline{" + header
                        + Form("}{[Sec: %.1f%%(1-step)+%.1f%%(2-step)+%.1f%%(>2-step)]}", val_1step, val_2step, val_sup2);
            } else
               header = "#sigma [mb] (contrib.sec[%])";

            Double_t ymin_leg = 1. - min(15., Double_t(n_reacs[i2plot] + n_ranges)) * 0.7 / 15.;
            Double_t xx_step = 0.9 / (Double_t)(n_ranges + 1);
            Double_t xx_width = 0.7 / (Double_t)n_ranges;
            Double_t xx_pos = 0.15 + ee * xx_step;
            leg_frac[ee][i2plot] = new TLegend(xx_pos, ymin_leg, xx_pos + xx_width, 0.95);
            leg_frac[ee][i2plot]->SetHeader(header.c_str());
            TLegendEntry *entry = NULL;

            // Loop on reactions
            for (Int_t r = n_reacs[i2plot] - 1 ; r >= 0; --r) {
               // Get index w.r.t. to sorted values (at plotted energy)
               Int_t ir_sorted = ireac_sorted[i2plot][r];
               // Plot only if contribution > fracmax_plot%
               if (fracreac_stat[0][i2plot][ir_sorted][k2plot[0]] < fracmax_plot)
                  continue;
               TGraph *gr = NULL;
               string opt = "LP";
               // Fill TGraph
               // fracreac_stat[n_stats][1+ncrs2plot][nreac2sort][ne]
               if (option.find("D1") != string::npos)
                  gr = new TGraph(ne, e_axis->GetVals(), &fracreac_stat[0][i2plot][ir_sorted][0]);
               else
                  gr = new TGraph(ne, e_axis->GetVals(), &xs_stat[0][i2plot][ir_sorted][0]);
               // If 1-step reaction
               if ((option.find("D1") != string::npos && reac_is1step[i2plot][ir_sorted]) ||
                     reac_leg_txt_gr[1][i2plot][ir_sorted].find("+H->") != string::npos) {
                  gr->SetLineWidth(4);
                  opt = "L";
               } else {
                  gr->SetLineWidth(2);
                  gr->SetMarkerColor(reac_col_sty_mkr[0][i2plot][ir_sorted]);
                  gr->SetMarkerStyle(reac_col_sty_mkr[2][i2plot][ir_sorted]);
                  gr->SetMarkerSize(0.9);
               }
               gr->SetName(Form("%s_%s_%d", reac_leg_txt_gr[2][i2plot][ir_sorted].c_str(), ext.c_str(), ee));
               gr->SetLineColor(reac_col_sty_mkr[0][i2plot][ir_sorted]);
               gr->SetLineStyle(reac_col_sty_mkr[1][i2plot][ir_sorted]);
               string full_leg = "";
               if (option.find("D1") != string::npos)
                  full_leg = Form("%s (%3.2f%%)", reac_leg_txt_gr[0][i2plot][ir_sorted].c_str(), fracreac_stat[0][i2plot][ir_sorted][k]);
               else
                  full_leg = Form("%s=%.1f (%3.2f%%)", reac_leg_txt_gr[0][i2plot][ir_sorted].c_str(), xs_stat[0][i2plot][ir_sorted][k], fracreac_stat[0][i2plot][ir_sorted][k]);
               if (ee == 0)
                  mg_frac[i2plot]->Add(gr, opt.c_str());
               // Add in legend
               entry = leg_frac[ee][i2plot]->AddEntry(gr, full_leg.c_str(), opt.c_str());
               entry->SetTextColor(reac_col_sty_mkr[0][i2plot][ir_sorted]);
               entry = NULL;
            }
            // Add contributions from ranges in legend
            if (option.find("D1") != string::npos) {
               for (Int_t r = n_ranges - 1; r >= 0; --r) {
                  string range_leg = Form("%d #in [%.3f%%,%.3f%%] = %.2f %%",
                                          n_in_range[i2plot][ee][r], ranges[r], ranges[r + 1], sum_range[i2plot][ee][r]);
                  entry = leg_frac[ee][i2plot]->AddEntry("NULL", range_leg.c_str(), "");
                  entry->SetTextColor(kGray + 2);
                  entry->SetTextFont(12);
                  entry = NULL;
               }
            }
         }
         e_axis = NULL;

         // Display contrib_sort fractions
         c_check_frac[i2plot]->cd();
         mg_frac[i2plot]->Draw("APL");
         mg_frac[i2plot]->SetMinimum(0.);
         mg_frac[i2plot]->SetMaximum(100.);
         usine_txt->Draw();
         for (Int_t ee = 0; ee < n_ekn2plot; ++ee)
            leg_frac[ee][i2plot]->Draw();
         c_check_frac[i2plot]->Modified();
         c_check_frac[i2plot]->Update();
      }
      // Enter event loop, one can now interact with the objects in
      // the canvas. Select "Exit ROOT" from Canvas "File" menu to exit
      // the event loop and execute the next statements.
      if (!gROOT->IsBatch())
         app->Run(kTRUE);

      // Free memory
      for (Int_t i2plot = 0; i2plot < n2plot; ++i2plot) {
         if (c_check_frac[i2plot]) delete c_check_frac[i2plot];
         c_check_frac[i2plot] = NULL;
         if (mg_frac[i2plot]) delete mg_frac[i2plot];
         mg_frac[i2plot] = NULL;
         for (Int_t ee = 0; ee < n_ekn2plot; ++ee) {
            if (leg_frac[ee][i2plot]) delete leg_frac[ee][i2plot];
            leg_frac[ee][i2plot] = NULL;
         }
      }
      if (c_check_frac) delete[] c_check_frac;
      if (mg_frac) delete[] mg_frac;
      for (Int_t ee = 0; ee < n_ekn2plot; ++ee) {
         if (leg_frac[ee]) delete[] leg_frac[ee];
         leg_frac[ee] = NULL;
      }
      delete[] leg_frac;
      leg_frac = NULL;
   }

   delete usine_txt;

   // Reset reference output file
   fOutputLog = fout_ref;
   fout_ref = NULL;
}

//______________________________________________________________________________
void TURunPropagation::PrintFitCovMatrix(FILE *f, vector<Int_t> const &indices_infitpars, Double_t *fitcovmatrix, Bool_t is_allfit) const
{
   //--- Prints in file f the indices 'indices_fit' of 'fitcovmatrix'.
   //  f                  File in which to print
   //  indices_infitpars  Indices of fitcovmatrix parameters
   //  fitcovmatrix       Covariance matrix of fit parameters [fFitPars->GetNPars()*fFitPars->GetNPars()]
   //  is_allfit          Whether fitcovmatrix contains all parameters in fFitPars
   //                     (e.g. for covariance or hessian) or size=indices_infitpars.size (e.g. for contours)

   // Print column content
   fprintf(f, "               ");
   for (Int_t i = 0; i < (Int_t)indices_infitpars.size(); ++i)
      fprintf(f, "%-14s ", fFitPars->GetParEntry(indices_infitpars[i])->GetFitName().c_str());
   fprintf(f, "\n");

   // Print values (first column is line content)
   for (Int_t i = 0; i < (Int_t)indices_infitpars.size(); ++i) {
      fprintf(f, "%-14s ", fFitPars->GetParEntry(indices_infitpars[i])->GetFitName().c_str());

      for (Int_t j = 0; j < (Int_t)indices_infitpars.size(); ++j) {
         if (is_allfit)
            fprintf(f, "%+le  ", fitcovmatrix[indices_infitpars[i] *fFitPars->GetNPars() + indices_infitpars[j]]);
         else
            fprintf(f, "%+le  ", fitcovmatrix[i * fFitPars->GetNPars() + j]);
      }
      fprintf(f, "\n");
   }
}

//______________________________________________________________________________
void TURunPropagation::PrintFitResult(FILE *f, Bool_t is_date) const
{
   //--- Print in file f the result of the minimisation procedure ()
   //  f                 File in which to print
   //  is_data           Whether to print data or note
   if (f != stdout && is_date) {
      TUMisc::PrintUSINEHeader(f, true);
      fprintf(f, "\n");
   }
   fprintf(f, "# ANALYSIS RESULT\n");
   fprintf(f, "# NFit      = %-5d\n", fFitPars->GetNPars(kFIT));
   fprintf(f, "# NFixed    = %-5d\n", fFitPars->GetNPars(kFIXED));
   fprintf(f, "# NNuisance = %-5d\n", fFitPars->GetNPars(kNUISANCE));
   fprintf(f, "# NData     = %-5d\n", fFitData->GetNData());
   if (GetNdofForXS() != 0) {
      fprintf(f, "# NpXS      = %-5d  [= #Nuisance(LC_XS) - #LCreactions]\n", GetNdofForXS());
      fprintf(f, "#   => dof = NData-NFit-NpXS = %-5d\n", GetNdof());
   } else
      fprintf(f, "#   => dof = NData-NFit = %-5d\n", GetNdof());
   if (fFitPars->GetNPars(kNUISANCE) == 0)
      fprintf(f, "chi2_min     = %le\n", fChi2Min);
   else
      fprintf(f, "chi2_min     = %le    (includes chi2_nuis=%.2le)\n", fChi2Min, Chi2FromNuisance());
   fprintf(f, "chi2_min/dof = %le\n", fChi2Min / Double_t(GetNdof()));
   fprintf(f, "\n");
}

//______________________________________________________________________________
void TURunPropagation::PrintListOfModels(FILE *f) const
{
   //--- Prints in file f the list of propagation models used.
   //  f                 File in which to print

   TUMessages::Separator(f, "List of models");

   string indent = TUMessages::Indent(true);

   fprintf(f, "%s   Propagation models:\n", indent.c_str());
   for (Int_t n = 0; n < fNPropagModels; ++n)
      fprintf(f, "%s      - %s\n", indent.c_str(),
              fPropagModels[n]->GetModelName().c_str());
   fprintf(f, "\n");

   fprintf(f, "%s   Solar modulation models:\n", indent.c_str());
   for (Int_t n = 0; n < GetNSolModModels(); ++n) {
      fprintf(f, "%s      - %s\n", indent.c_str(),
              fSolModModels[n]->GetModelName().c_str());
   }
   fprintf(f, "\n");
   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TURunPropagation::PrintListOfModelParameters(FILE *f) const
{
   //--- Prints in file f model free parameters (propagation and modulation models).
   //  f                 File in which to print

   TUMessages::Separator(f, "List of model parameters");

   string indent = TUMessages::Indent(true);

   for (Int_t n = 0; n < fNPropagModels; ++n) {
      fprintf(f, "%s      - %s\n", indent.c_str(),
              fPropagModels[n]->GetModelName().c_str());
      fPropagModels[n]->PrintPars(f);
      fprintf(f, "\n");
   }

   for (Int_t n = 0; n < GetNSolModModels(); ++n) {
      fprintf(f, "%s      - %s\n", indent.c_str(),
              fSolModModels[n]->GetModelName().c_str());
      fSolModModels[n]->PrintPars(f);
   }
   fprintf(f, "\n");
   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TURunPropagation::Propagate(Bool_t is_norm_to_data, Bool_t is_verbose, Int_t jcr_start, Int_t jcr_stop)
{
   //--- Propagates all models in list (normalise to data if required).
   //  is_norm_to_data   Whether to normalise fluxes to user-norm selection (TUNormList)
   //  is_verbose        Chatter is on or off
   //  jcr_start         Index of CR for first to calculate
   //  jcr_stop          Index of CR for last to calculate

   if (fNPropagModels == 0)
      TUMessages::Error(fOutputLog, "TURunPropagation", "Propagate", "No propagation model loaded");


   // Check indices to run
   if (jcr_start < 0)
      jcr_start = 0;
   if (jcr_stop > fPropagModels[0]->GetNCRs() - 1 || jcr_stop == -1)
      jcr_stop = fPropagModels[0]->GetNCRs() - 1;
   if (jcr_start > jcr_stop)
      TUMessages::Warning(fOutputLog, "TURunPropagation", "Propagate", "jcr_start>jcr_stop, nothing to do");


   if (is_verbose) {
      fprintf(fOutputLog, "\n");
      if (is_norm_to_data)
         TUMessages::Separator(fOutputLog, "Propagation in progress...");
      else
         TUMessages::Separator(fOutputLog, "Propagation in progress (no further normalisation to data)...");

      fprintf(fOutputLog, "\n");
      fprintf(fOutputLog, "    -> From initialisation file: %s\n", fInitFileName.c_str());
      PrintListOfModels(fOutputLog);
      PrintPropagationMode(fOutputLog);
   }

   for (Int_t n = 0; n < fNPropagModels; ++n) {
      string model = fPropagModels[n]->GetModelName();
      if (is_verbose) {
         if (fPropagModels[n]->IsPropagated())
            fprintf(fOutputLog, "\n   -> Model %s already propagated, nothing to do\n", model.c_str());
         else
            fprintf(fOutputLog, "\n");

         fprintf(fOutputLog, "........... processing %s  ..........\n", model.c_str());
      }
      fPropagModels[n]->Propagate(jcr_start, jcr_stop, is_norm_to_data, is_verbose, fOutputLog);
   }
   if (is_verbose) {
      fprintf(fOutputLog, "........... end processing ..........\n");
   }
}

//______________________________________________________________________________
void TURunPropagation::SetClass(string const &usine_initfile, Bool_t is_verbose,
                                string const &output_dir, FILE *f_log)
{
   //--- Sets class from USINE initialisation file.
   //  usine_initfile    USINE initialisation file
   //  is_verbose        Verbose or not when class is set
   //  output_dir        Directory for outputs
   //  f_log             Log for USINE

   TUInitParList *init_pars = new TUInitParList();
   init_pars->SetClass(usine_initfile, is_verbose, f_log);
   SetClass(init_pars, is_verbose, output_dir, f_log);
   delete init_pars;
   init_pars = NULL;
}

//______________________________________________________________________________
void TURunPropagation::SetClass(TUInitParList *init_pars, Bool_t is_verbose,
                                string const &output_dir, FILE *f_log)
{
   //--- Sets class from init_pars.
   //  init_pars         USINE initialisation parameters
   //  models            Vector of propagation model enum index (see TUEnum.h)
   //  is_verbose        Verbose or not when class is set
   //  output_dir        Directory for outputs
   //  f_log             Log for USINE

   string indent = TUMessages::Indent(true);
   // Outputs
   fOutputLog = f_log;
   fOutputDir =  TUMisc::CreateDir(output_dir, "TURunPropagation", "SetClass");
   if (is_verbose)
      fprintf(f_log, "%s### Output directory (files and plots) is %s\n", indent.c_str(), fOutputDir.c_str());


   if (is_verbose) {
      TUMessages::Separator(f_log, "INITIALISATION USINE", 1);
      fprintf(f_log, "%s[TURunPropagation::SetClass]\n", indent.c_str());
   }

   // Initialise class
   Initialise(true);

   // Store initialisation file name
   fInitFileName = init_pars->GetFileNames();


   const Int_t gg = 3;
   string gr_sub_name[gg] = {"UsineRun", "Models", "Propagation"};

   // Set propagation models (from init file)
   if (is_verbose)
      fprintf(f_log, "%s### Set propagation models\n", indent.c_str());
   Int_t ipar_propag = init_pars->IndexPar(gr_sub_name);

   // Allocate and loop on list of propagation models in init. file
   fNPropagModels = init_pars->GetParEntry(ipar_propag).GetNVals();
   if (fNPropagModels == 0)
      TUMessages::Error(f_log, "TURunPropagation", "SetClass", "No propagation model selected, nothing to do!");
   fPropagModels = new TUModelBase*[fNPropagModels];
   for (Int_t i = 0; i < fNPropagModels; ++i) {
      //Check that model name exists (aborts if not)
      string name = init_pars->GetParEntry(ipar_propag).GetVal();
      gENUM_PROPAG_MODEL model = kBASE;
      TUEnum::Name2Enum(name, model);

      if (is_verbose)
         fprintf(f_log, "%s    - Add %s\n", indent.c_str(), name.c_str());
      // Allocate appropriate model depending on name
      if (model == kMODEL0DLEAKYBOX)
         fPropagModels[i] = new TUModel0DLeakyBox();
      else if (model == kMODEL1DKISOVC)
         fPropagModels[i] = new TUModel1DKisoVc();
      else if (model == kMODEL2DKISOVC)
         fPropagModels[i] = new TUModel2DKisoVc();
      else {
         string message = "Either model " + name + " does not exist or you forgot"
                          + " to create new TUModelYourModel() in this function!" ;
         TUMessages::Error(f_log, "TURunPropagation", "SetClass", message);
      }

      fPropagModels[i]->SetClass(init_pars, name, is_verbose, f_log);
      if (is_verbose)
         fprintf(f_log, "%s    => %s loaded\n", indent.c_str(), fPropagModels[i]->GetModelName().c_str());
   }

   // Set Solar modulation models (from init. file)
   if (is_verbose)
      fprintf(f_log, "%s### Set Solar modulation models\n", indent.c_str());
   gr_sub_name[2] = "SolarModulation";
   Int_t ipar_solmod = init_pars->IndexPar(gr_sub_name);

   // Allocate and loop on list of propagation models in init. file
   fNSolModModels = init_pars->GetParEntry(ipar_solmod).GetNVals();
   fSolModModels = new TUSolModVirtual*[fNSolModModels];
   for (Int_t i = 0; i < fNSolModModels; ++i) {
      //Check that model name exists (aborts if not)
      string name = init_pars->GetParEntry(ipar_solmod).GetVal(i);
      gENUM_SOLMOD_MODEL model = kIS;
      TUEnum::Name2Enum(name, model);
      if (is_verbose)
         fprintf(f_log, "%s    - Add %s\n", indent.c_str(), name.c_str());

      // Allocate appropriate model depending on name
      if (model == kSOLMOD0DFF)
         fSolModModels[i] = new TUSolMod0DFF(init_pars, is_verbose, f_log);
      else if (model == kSOLMOD1DSPHER)
         continue;
      //   fSolModModels[i] = new TUSolMod1DSpher(init_pars, fPropagModels[0], is_verbose, f_log);

      if (is_verbose)
         fprintf(f_log, "%s    => %s loaded\n", indent.c_str(), fSolModModels[i]->GetModelName().c_str());
   }


   /////////////////////
   // OTHER Run Setup //
   /////////////////////
   // Set calculation precision (fEps)
   if (is_verbose) {
      fprintf(f_log, "%s### Other calculation setup\n", indent.c_str());
      fprintf(f_log, "%s   - Set calculation precision\n", indent.c_str());
   }
   // Set boundary conditions (same for all models)
   fPropagModels[0]->SetBCType(init_pars, is_verbose, f_log);
   for (Int_t i = 1; i < GetNPropagModels(); ++i) {
      fPropagModels[i]->SetBCTypeAntinucLE(fPropagModels[0]->GetBCTypeAntinucLE());
      fPropagModels[i]->SetBCTypeAntinucHE(fPropagModels[0]->GetBCTypeAntinucHE());
      fPropagModels[i]->SetBCTypeLeptonLE(fPropagModels[0]->GetBCTypeLeptonLE());
      fPropagModels[i]->SetBCTypeLeptonHE(fPropagModels[0]->GetBCTypeLeptonHE());
      fPropagModels[i]->SetBCTypeNucLE(fPropagModels[0]->GetBCTypeNucLE());
      fPropagModels[i]->SetBCTypeNucHE(fPropagModels[0]->GetBCTypeNucHE());
   }

   gr_sub_name[1] = "Calculation";
   gr_sub_name[2] = "EPS_INTEGR";
   Double_t eps_integr =  atof(init_pars->GetParEntry(init_pars->IndexPar(gr_sub_name)).GetVal().c_str());
   gr_sub_name[2] = "EPS_ITERCONV";
   Double_t eps_iterconv =  atof(init_pars->GetParEntry(init_pars->IndexPar(gr_sub_name)).GetVal().c_str());
   gr_sub_name[2] = "EPS_NORMDATA";
   Double_t eps_normdata =  atof(init_pars->GetParEntry(init_pars->IndexPar(gr_sub_name)).GetVal().c_str());
   if (is_verbose) {
      fprintf(f_log, "%s       => eps_integr   = %le\n", indent.c_str(), eps_integr);
      fprintf(f_log, "%s       => eps_iterconv = %le\n", indent.c_str(), eps_iterconv);
      fprintf(f_log, "%s       => eps_normdata = %le\n", indent.c_str(), eps_normdata);
   }
   for (Int_t i = 0; i < GetNPropagModels(); ++i) {
      fPropagModels[i]->SetEpsIntegr(eps_integr);
      fPropagModels[i]->SetEpsIterConv(eps_iterconv);
      fPropagModels[i]->SetEpsNormData(eps_normdata);
   }

   // Whether to norm or not source spectrum abundance to data
   if (is_verbose)
      fprintf(f_log, "%s   - Normalise source abundances to data in run?\n", indent.c_str());
   gr_sub_name[2] = "IsUseNormList";
   fIsUseNormList = TUMisc::String2Bool(init_pars->GetParEntry(init_pars->IndexPar(gr_sub_name)).GetVal().c_str());
   if (is_verbose) {
      if (fIsUseNormList) {
         fprintf(f_log, "%s      => Yes, leading to:\n", indent.c_str());
         fPropagModels[0]->TUNormList::PrintNormData(f_log);
      } else
         fprintf(f_log, "%s      => No normalisation\n", indent.c_str());
   }



   ///////////////////////
   // Displayed CR data //
   ///////////////////////
   // Set calculation precision (fEps)
   if (fPropagModels[0]->TUDataSet::GetNData() == 0) {
      string message = "No CR data to select from, check in initialisation file!";
      TUMessages::Error(fOutputLog, "TURunPropagation", "SetClass", message);
   }
   if (is_verbose)
      fprintf(f_log, "%s### Displayed CR data setup\n", indent.c_str());
   gr_sub_name[1] = "Display";
   gr_sub_name[2] = "QtiesExpsEType";
   string qties_expnames_etypes = init_pars->GetParEntry(init_pars->IndexPar(gr_sub_name)).GetVal();
   if (is_verbose)
      fprintf(fOutputLog, "%s   - Select subset matching %s\n", indent.c_str(), qties_expnames_etypes.c_str());

   // Fill
   if (fDisplayedData) delete fDisplayedData;
   if (qties_expnames_etypes != "ALL")
      fDisplayedData = fPropagModels[0]->TUDataSet::OrphanFormSubSet(qties_expnames_etypes, fOutputLog);
   else
      fDisplayedData = fPropagModels[0]->TUDataSet::Clone();
   //fDisplayedData->Print();
   if (fDisplayedData->GetNData() == 0) {
      string message = "No CR data to display for selection " + qties_expnames_etypes;
      TUMessages::Warning(fOutputLog, "TURunPropagation", "SetClass", message);
   }
   gr_sub_name[2] = "ErrType";
   string err_type = init_pars->GetParEntry(init_pars->IndexPar(gr_sub_name)).GetVal();
   TUEnum::Name2Enum(err_type, fDisplayedErrType);
   if (is_verbose)
      fprintf(fOutputLog, "%s   - Showing error-type=%s\n", indent.c_str(), err_type.c_str());

   gr_sub_name[2] = "FluxPowIndex";
   fDiplayedFluxEPower = atof(init_pars->GetParEntry(init_pars->IndexPar(gr_sub_name)).GetVal().c_str());
   if (is_verbose)
      fprintf(fOutputLog, "%s   - Display Fluxes*E^e_power with e_power=%f\n", indent.c_str(), fDiplayedFluxEPower);

   ///////////////////
   // USINE outputs //
   ///////////////////
   if (is_verbose) {
      fprintf(f_log, "%s[TURunPropagation::SetClass] <DONE>\n", indent.c_str());
      TUMessages::Separator(f_log, "=> READY FOR CALCULATIONS/DISPLAYS", 1);
   }

   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TURunPropagation::SumCiAndSigmaForXSLC(vector<Double_t> sum_lc_pars[], vector<Double_t> sigma_lc_pars[], vector<string> reacs_lc_pars[]) const
{
   //--- Returns, for current values of fFitPars, necessary values (sum Ci and sigma for each reac)
   //    in the case a linear combination of XS nuisance parameters is used.
   //  sum_lc_pars[]    [2=inel.or.prod][n_reac] Sum for LC coeffs
   //  sigma_lc_pars[]  [2=inel.or.prod][n_reac] sigma for reac
   //  reacs[]          [2=inel.or.prod][n_reac] Reac names

   // Initialise values
   for (Int_t i = 0; i < 2; ++i) {
      sum_lc_pars[i].assign(fPropagModels[0]->TUXSections::ParsXS_LinComb_GetNReac(i), 0.);
      sigma_lc_pars[i].assign(fPropagModels[0]->TUXSections::ParsXS_LinComb_GetNReac(i), 0.);
      reacs_lc_pars[i].assign(fPropagModels[0]->TUXSections::ParsXS_LinComb_GetNReac(i), "");
   }

   // Loop on free parameters
   Double_t chi2_nuis = 0.;
   for (Int_t l = 0; l < fFitPars->GetNPars(); ++l) {
      //par->Print();

      TUFreeParEntry *par = fFitPars->GetParEntry(l);
      Double_t par_val = par->GetVal(par->GetFitSampling());

      // Calculate sum and sigma (one per reaction) on XS LC nuisance parameter.
      Bool_t is_inel_ow_prod = true;
      if (par->GetFitName().find("LCInel") == string::npos)
         is_inel_ow_prod = false;
      // Find to which reaction this coefficient belong, and add ii to stored sum
      for (Int_t i_r = 0; i_r < (Int_t)sum_lc_pars[is_inel_ow_prod].size(); ++i_r) {
         reacs_lc_pars[is_inel_ow_prod][i_r] = fPropagModels[0]->TUXSections::ParsXS_LinComb_GetNameReac(i_r, is_inel_ow_prod);
         TUMisc::UpperCase(reacs_lc_pars[is_inel_ow_prod][i_r]);
         if (par->GetFitName().find(reacs_lc_pars[is_inel_ow_prod][i_r]) != string::npos) {
            sum_lc_pars[is_inel_ow_prod][i_r] += par_val;
            sigma_lc_pars[is_inel_ow_prod][i_r] = par->GetFitInitSigma();
            break;
         }
      }
      //cout << "    " << par->GetFitName() << ": " << par_val << endl;
      par = NULL;
   }
}

//______________________________________________________________________________
void TURunPropagation::TUI_ModifyPropagSwitches() const
{
   //--- Text User Interface to modify propagation switches (for all models at once).

   // Check that at least one model (warning if more than one model): only calculate for model 0
   if (fNPropagModels == 0) {
      TUMessages::Warning(stdout, "TURunPropagation", "TUI_ModifyPropagSwitches", "No propagation model loaded");
      return;
   }

   // Get reference propagation mode of first model
   TUPropagSwitches *ref_propag = fPropagModels[0]->TUPropagSwitches::Clone();

   // Modify it
   ref_propag->TUI_ModifyPropagSwitches();

   // Propagate change for all models
   for (Int_t n = 0; n < fNPropagModels; ++n)
      fPropagModels[n]->TUPropagSwitches::Copy(*ref_propag);

   // Free memory
   delete ref_propag;
   ref_propag = NULL;
}

//______________________________________________________________________________
void TURunPropagation::TUI_SelectListOfPhi(vector<Double_t> &list_phi)
{
   //--- Text User Interface to select Solar modul. levels phi (PHI=|Z|/A*phi) to display.
   // OUTPUT:
   //  list_phi          Vector of solar modulation level phi (PHI=|Z|/A*phi)

   string indent = TUMessages::Indent(true);

   // Select a list of modulation
   string phi;
   do {
      list_phi.clear();
      cout << indent
           << "   ### Select a comma-separated list (e.g., 0.,0.5,1) of modulation param phi (PHI=|Z|/A*phi) GV"
           << endl;
      cout << indent << "    >>  ";
      cin >> phi;
      TUMisc::UpperCase(phi);
      if ((phi.find_first_not_of("0123456789.,") == string::npos)
            && (phi.find_first_of("0123456789") != string::npos))
         break;
   } while (1);

   TUMisc::String2List(phi, ",", list_phi);
   TUMessages::Indent(false);
}

//______________________________________________________________________________
string TURunPropagation::UID(string const &combo, gENUM_ETYPE e_type, Double_t const &e_power, string const &model_name, string const &uid_solmod)
{
   //--- Sets a universal ID for retrieved quantity (useful to denote files and graphs).
   //  combo             CR combination (ratio, sum of isotopes/elements, etc.)
   //  e_type            Energy type of e_grid (among gENUM_ETYPE=kEKN,kR,...)
   //  e_power           Fluxes are multiplied by e_type^{e_power} (if not ratio, or <LnA>)
   //  coord_txyz        Coordinates (bins[4] or values[4]) for T, X, Y, Z
   //  model_name        Propagation model name
   //  uid_solmod        Modulation model unique

   string uid = combo + "_" + TUEnum::Enum2Name(e_type);
   if (fabs(e_power) > 1.e-3)
      uid = uid + "_" + TUMisc::FormatEPower(e_power);
   if (model_name != "")
      uid = uid + "_" + model_name;
   if (uid_solmod != "")
      uid = uid + "_" + uid_solmod;

   TUMisc::RemoveSpecialChars(uid);
   return uid;
}


//______________________________________________________________________________
void TURunPropagation::UpdateFitParsAndFitData(string const &usine_initfile, Bool_t is_verbose, Bool_t is_logandstdout)
{
   //--- Sets class from USINE initialisation file.
   //  usine_initfile    USINE initialisation file
   //  init_pars         USINE initialisation parameters
   //  is_verbose        Verbose on or off
   //  is_logandstdout   Print info in log file, but also on screen (if true)

   TUInitParList *init_pars = new TUInitParList();
   init_pars->SetClass(usine_initfile, is_verbose, stdout);
   UpdateFitParsAndFitData(init_pars, is_verbose, is_logandstdout);
   delete init_pars;
   init_pars = NULL;
}

//______________________________________________________________________________
void TURunPropagation::UpdateFitParsAndFitData(TUInitParList *init_pars, Bool_t is_verbose, Bool_t is_logandstdout)
{
   //--- Load fit parameters and data to be fit (required for minimisation).
   //  init_pars         USINE initialisation parameters
   //  is_verbose        Verbose on or off
   //  is_logandstdout   Print info in log file, but also on screen (if true)

   string indent = TUMessages::Indent(true);
   if (is_verbose)
      PrintLog(Form("%s[TURunPropagation::UpdateFitParsAndFitData]", indent.c_str()), is_logandstdout);


   if (GetNPropagModels() != 1) {
      string message = "Only one propagation model allowed for minimisation!";
      TUMessages::Error(fOutputLog, "TURunPropagation", "UpdateFitParsAndFitData", message);
   }
   if (GetNSolModModels() != 1) {
      string message = "Only one solar modulation model allowed for minimisation!";
      TUMessages::Error(fOutputLog, "TURunPropagation", "UpdateFitParsAndFitData", message);
   }


   //-----------------------------
   //  1. LOAD TOA DATA TO ANALYSE
   //-----------------------------
   const Int_t gg = 3;
   string gr_sub_name[gg] = {"UsineFit", "TOAData", ""};

   if (fPropagModels[0]->TUDataSet::GetNData() == 0) {
      string message = "No CR data to select from, check in initialisation file!";
      TUMessages::Error(fOutputLog, "TURunPropagation", "UpdateFitParsAndFitData", message);
   }
   if (is_verbose)
      PrintLog(Form("%s### Set TOA CR data and CR species to be analysed", indent.c_str()), is_logandstdout);

   gr_sub_name[2] = "QtiesExpsEType";
   string qties_expnames_etypes = init_pars->GetParEntry(init_pars->IndexPar(gr_sub_name)).GetVal();
   gr_sub_name[2] = "EminData";
   Double_t emin = atof(init_pars->GetParEntry(init_pars->IndexPar(gr_sub_name)).GetVal().c_str());
   gr_sub_name[2] = "EmaxData";
   Double_t emax = atof(init_pars->GetParEntry(init_pars->IndexPar(gr_sub_name)).GetVal().c_str());
   gr_sub_name[2] = "TStartData";
   string t_start = init_pars->GetParEntry(init_pars->IndexPar(gr_sub_name)).GetVal();
   gr_sub_name[2] = "TStopData";
   string t_stop = init_pars->GetParEntry(init_pars->IndexPar(gr_sub_name)).GetVal();
   fFitERange = Form("fit range = [%.2le,%.2le] GeV/n", emin, emax);
   if (is_verbose)
      PrintLog(Form("%s   - Fill CR data from %s in dates [%s | %s]", indent.c_str(), qties_expnames_etypes.c_str(), t_start.c_str(), t_stop.c_str()), is_logandstdout);
   // Fill
   // N.B.: cutting on the energy range must be done afterwards (in case kERRCOV is demanded)
   TUDataSet *fit_data_allerange = fPropagModels[0]->TUDataSet::OrphanFormSubSet(qties_expnames_etypes, fOutputLog, 1.e-10, 1.e40, t_start, t_stop);
   // Set error type for data to fit and search for covariance matrix files
   gr_sub_name[2] = "ErrType";
   string err_type = init_pars->GetParEntry(init_pars->IndexPar(gr_sub_name)).GetVal();
   string err_type_uc = err_type;
   TUMisc::UpperCase(err_type_uc);
   string dir_cov = "";
   if (err_type_uc.find("ERRCOV")) {
      vector<string> err_dir;
      TUMisc::String2List(err_type, ":", err_dir);
      err_type = err_dir[0];
      if (err_dir.size() == 2)
         dir_cov = err_dir[1];
   }
   TUEnum::Name2Enum(err_type, fFitDataErrType);
   if (is_verbose) {
      if (fFitDataErrType != kERRCOV)
         PrintLog(Form("%s   - Using error-type=%s", indent.c_str(), err_type.c_str()), is_logandstdout);
      else
         PrintLog(Form("%s   - Looking for covariance files (of data relative errors) in %s (error-type=%s)",
                       indent.c_str(), dir_cov.c_str(), err_type.c_str()), is_logandstdout);
   }
   // If covariance, search for all exp/qty/Etype, then search for cov.matrix:
   // if found load it, otherwise revert to total error for this exp/qty/Etype.
   if (fFitDataErrType == kERRCOV && fit_data_allerange->GetNData() > 0) {
      Bool_t is_at_least_1cov =false;

      // Loop on error types
      for (Int_t e = 0; e < gN_ETYPE; ++e) {
         gENUM_ETYPE e_type = TUEnum::gENUM_ETYPE_LIST[e];
         vector<pair<Int_t, Int_t> > idata_start_stop;
         fit_data_allerange->IndicesData4AllExpQty(e_type, idata_start_stop);
         for (Int_t i = 0; i < (Int_t)idata_start_stop.size(); ++i) {
            string expqtyetype = fit_data_allerange->GetEntry(idata_start_stop[i].first)->FormExpQtyEType();
            string expqtyetype_trimmed = expqtyetype;
            TUMisc::RemoveSpecialChars(expqtyetype_trimmed);
            string file_relcov = "cov_" + expqtyetype_trimmed + ".dat";
            string filefull_relcov = dir_cov;
            if (dir_cov[dir_cov.size() - 1] == '/')
               filefull_relcov += file_relcov;
            else
               filefull_relcov += "/" + file_relcov;
            PrintLog(Form("%s       * Exp_Qty_EType=%s => looking for file %s in %s",
                          indent.c_str(), expqtyetype.c_str(), file_relcov.c_str(), dir_cov.c_str()), is_logandstdout);

            // Load file
            if (!fit_data_allerange->LoadDataRelCov(idata_start_stop[i].first, idata_start_stop[i].second, filefull_relcov, fOutputLog))
               PrintLog(Form("%s         => no %s, enforce kERRTOT", indent.c_str(), filefull_relcov.c_str()), is_logandstdout);
            else {
               is_at_least_1cov = true;
               PrintLog(Form("%s         => load %s", indent.c_str(), filefull_relcov.c_str()), is_logandstdout);
            }
            // fit_data_allerange->PrintDataRelCov(fOutputLog, idata_start_stop[i].first, idata_start_stop[i].second);
         }
      }
      // Look for how to convert cov.mat. of relative errors into cov.mat of absolute errors
      if (is_at_least_1cov) {
         gr_sub_name[2] = "IsModelOrDataForRelCov";
         fIsModelOrDataForRelCov = TUMisc::String2Bool(init_pars->GetParEntry(init_pars->IndexPar(gr_sub_name)).GetVal().c_str());
         if (fIsModelOrDataForRelCov)
            PrintLog(Form("%s       => Use DATA values to convert covariance matrix of data relative errors", indent.c_str()), is_logandstdout);
         else
            PrintLog(Form("%s       => Use MODEL values to convert covariance matrix of data relative errors", indent.c_str()), is_logandstdout);
      }
   }

   // Ensure correct energy range is selected
   if (fFitData) delete fFitData;
   PrintLog(Form("%s   - Restrict data to selected E range [%.3le,%.3le]", indent.c_str(), emin, emax), is_logandstdout);
   fFitData = fit_data_allerange->OrphanFormSubSet(qties_expnames_etypes, fOutputLog, emin, emax);
   delete fit_data_allerange;
   fit_data_allerange = NULL;
   //fFitData->Print();
   if (fFitData->GetNData() == 0) {
      string message = "No CR data to minimize on for selection " + qties_expnames_etypes + "in selected E- and time-range";
      TUMessages::Error(fOutputLog, "TURunPropagation", "UpdateFitParsAndFitData", message);
   }

   // Check Energy range for all families involved
   for (Int_t i = 0; i < gN_CRFAMILY; ++i) {
      gENUM_CRFAMILY family = TUEnum::gENUM_CRFAMILY_LIST[i];
      TUAxis *e = fPropagModels[0]->TUAxesCrE::GetEFamily(family);
      vector<Double_t> min_max = fFitData->GetERange(family);
      string cr_family = "CR family=" + TUEnum::Enum2Name(family);
      if (min_max[0] < e->GetMin()) {
         string message = cr_family + ": IS flux min energy (" + (string)Form("%.3le", e->GetMin())
                          + ") larger than data min energy (" + (string)Form("%.3le", min_max[0])
                          + ") => decrease IS flux energy in initialisation file!";
         TUMessages::Error(fOutputLog, "TURunPropagation", "UpdateFitParsAndFitData", message);
      }
      if (min_max[1] > e->GetMax()) {
         string message = cr_family + ": IS flux max energy (" + (string)Form("%.3le", e->GetMax())
                          + ") smaller than data max energy (" + (string)Form("%.3le", min_max[1])
                          + ") => increase IS flux energy in initialisation file!";
         TUMessages::Error(fOutputLog, "TURunPropagation", "UpdateFitParsAndFitData", message);
      }
      e = NULL;
   }
   // Check that there is something to minimise
   if (!fFitData) {
      string message = "No selected TOA data to analyse, call UpdateFitData(...)!";
      TUMessages::Error(fOutputLog, "TURunPropagation", "UpdateFitParsAndFitData", message);
   }
   // Check that data to minimize are present in list of CRs
   string commasep_list_crs = fFitData->ExtractQties();
   vector<Int_t> cr_indices;
   fPropagModels[0]->TUCRList::Indices(commasep_list_crs, cr_indices, true /*is_inlist*/);


   // Whether to fit on 'central' E value or integrated value on bin size
   gr_sub_name[1] = "Config";
   if (is_verbose)
      PrintLog(Form("%s### Calculate model from \'integrated\' value on bin size?", indent.c_str()), is_logandstdout);
   gr_sub_name[2] = "IsUseBinRange";
   fIsUseBinRange = TUMisc::String2Bool(init_pars->GetParEntry(init_pars->IndexPar(gr_sub_name)).GetVal().c_str());
   gr_sub_name[2] = "NExtraInBinRange";
   fNExtraInBinRange = atoi(init_pars->GetParEntry(init_pars->IndexPar(gr_sub_name)).GetVal().c_str());
   if (is_verbose) {
      if (fIsUseBinRange)
         PrintLog(Form("%s   => Yes, and use %d intermediate points inside bin for calculation", indent.c_str(), fNExtraInBinRange), is_logandstdout);
      else
         PrintLog(Form("%s   => No, model is calculated at \'central\' E data point", indent.c_str()), is_logandstdout);
   }


   // Create sol.mod. parameters associated to data to fit
   if (is_verbose)
      PrintLog(Form("%s### Create modulation free parameters for each exp on which to minimize", indent.c_str()), is_logandstdout);
   fSolModModels[0]->ParsModExps_Create(fFitData);

   // Create nuisance parameters associated to data for which a covariance matrix is provided
   if (fFitDataErrType == kERRCOV) {
      if (is_verbose)
         PrintLog(Form("%s### Create nuisance parameters for data for each error types with a covariance", indent.c_str()), is_logandstdout);
      fFitData->ParsData_Create();
   }

   //--------------------
   //  3. FREE PARAMETERS
   //--------------------
   gr_sub_name[1] = "FreePars";
   if (is_verbose)
      PrintLog(Form("%s### Set free parameters (for the fit)", indent.c_str()), is_logandstdout);

   // Extract indices of parameters (to use as free pars)
   vector<Int_t> indices_fitpars;
   vector<string> type_fitpars;
   // Indices and type (FIT, FIXED, or NUISANCE) for solar modulation parameters
   vector<pair<Int_t, Int_t> > indices_solmodfitpars;
   vector<string> type_solmodfitpars;
   // Type (FIT, FIXED, or NUISANCE) and name for XS parameters
   vector<string> type_fitpars_xs;
   vector<string> name_fitpars_xs;
   // Type (FIT,FIXED, or NUISANCE) and name for CR parameters
   vector<string> type_fitpars_crs;
   vector<string> name_fitpars_crs;
   // Indices and types (FIT, FIXED, or NUISANCE) for data error parameters
   vector<pair<Int_t, Int_t> > indices_errnuispars;
   vector<string> type_errnuispars;
   const Int_t n_modelpars = 9;
   Bool_t is_srcsteadystate = false;
   string model_pars[n_modelpars] = {
      "Geometry", "ISM", "SrcPointLike", "SrcSteadyState",
      "Transport", "Modulation", "XSection", "DataErr",
      "CRs"
   };

   PrintLog(Form("%s   - Loop on fit-able parameters", indent.c_str()), is_logandstdout);
   ///////////////////////////////////////////
   // READ/EXTRACT USER-PROVIDED PARAMETERS //
   ///////////////////////////////////////////
   for (Int_t i = 0; i < n_modelpars; ++i) {
      // Get parameters for type i from init file
      gr_sub_name[2] = model_pars[i];
      Int_t i_par = init_pars->IndexPar(gr_sub_name, false);
      string str = "none";
      Int_t n_multi = 0;
      if (i_par >= 0)
         n_multi = init_pars->GetParEntry(i_par).GetNVals();
      if (i_par >= 0 && (n_multi > 1 || init_pars->GetParEntry(i_par).GetVal() != "-"))
         str = Form("found %d", + n_multi);
      PrintLog(Form("%s      - %s@%s@%-15s -> %s", indent.c_str(), gr_sub_name[0].c_str(), gr_sub_name[1].c_str(), gr_sub_name[2].c_str(), str.c_str()), is_logandstdout);
      if (i_par < 0 || str == "none")
         continue;
      // Loop on multiple entries for each parameter
      for (Int_t j = 0; j < n_multi; ++j) {
         string par_entry = init_pars->GetParEntry(i_par).GetVal(j);
         if (par_entry == "-")
            continue;

         // N.B.: the format here for free parameters is "par:X,Y,[val_min,val_max],val_init,sigma"
         // with X = 'FIT', 'FIXED', or 'NUISANCE' parameter, and Y = 'LIN' or 'LOG'
         vector<string> tmp;
         TUMisc::String2List(par_entry, ":", tmp);
         if (tmp.size() != 2) {
            string message = "Ill-formatted entry = " + par_entry
                             + ", should be \"par:X,Y,[val_min,val_max],val_init,sigma\" with X=\'FIT\', \'FIXED\', or \'NUISANCE\', and Y=\'LIN' or \'LOG\'";
            TUMessages::Error(fOutputLog, "TURunPropagation", "UpdateFitParsAndFitData", message);
         }
         // If reach this point, there are parameters!
         // Parameter name is tmp[0], its description is in tmp[1]
         string par_name = tmp[0];
         par_entry = tmp[1];

         // Specific status for sol.mod., XS, CR, and nuisance CovErr parameters.
         // They are dealt with later as they are not in fPropagModels[0]->TUFreeParList.
         if (model_pars[i] == "SrcSteadyState")
            is_srcsteadystate = true;
         else if (model_pars[i] == "Modulation") {
            // Search for indices of parameters
            pair<Int_t, Int_t > i_solmod;
            fSolModModels[0]->ParsModExps_Indices(par_name, i_solmod.first, i_solmod.second);
            if (i_solmod.first < 0 || i_solmod.second < 0) {
               string message = "Free parameter " + par_name + " not an existing free parameter for TOA data selection";
               TUMessages::Error(fOutputLog, "TURunPropagation", "UpdateFitParsAndFitData", message);
            }
            indices_solmodfitpars.push_back(i_solmod);
            type_solmodfitpars.push_back(par_entry);
            continue;
         } else if (model_pars[i] == "XSection") {
            type_fitpars_xs.push_back(par_entry);
            name_fitpars_xs.push_back(par_name);
            continue;
         } else if (model_pars[i] == "CRs") {
            type_fitpars_crs.push_back(par_entry);
            name_fitpars_crs.push_back(par_name);
            continue;
         } else if (model_pars[i] == "DataErr") {
            pair<Int_t, Int_t > i_errdata;

            // Search for indices of parameters
            fFitData->ParsData_Indices(par_name, i_errdata.first, i_errdata.second);
            if (i_errdata.first < 0 || i_errdata.second < 0) {
               string message = "Free parameter " + par_name + " not an existing free parameter for error data selection";
               TUMessages::Error(fOutputLog, "TURunPropagation", "UpdateFitParsAndFitData", message);
            }
            if (par_entry.substr(0, 8) != "NUISANCE") {
               string message = "Parameter " + par_name + " (for error data) can only be NUISANCE!";
               TUMessages::Error(fOutputLog, "TURunPropagation", "UpdateFitParsAndFitData", message);
            }
            indices_errnuispars.push_back(i_errdata);
            type_errnuispars.push_back(par_entry);
            continue;
         }

         // Search for user-selected parameter in list of fPropagModels[0]->TUFreeParList.
         // (contains only 'built-in' model parameters, i.e geometry, transport, sources)
         Int_t i_free_par = fPropagModels[0]->TUFreeParList::IndexPar(par_name);
         if (i_free_par >= 0) {
            indices_fitpars.push_back(i_free_par);
            type_fitpars.push_back(par_entry);
         } else {
            string message = "Free parameter " + par_name + " not an existing free parameter for @"
                             + fPropagModels[0]->GetModelName() + "@" + model_pars[i];
            if (model_pars[i] == "Geometry" && fPropagModels[0]->GetAxesTXYZ()->GetFreePars()) {
               message = message + ": valid free parameters are " + fPropagModels[0]->GetAxesTXYZ()->GetFreePars()->GetParNames();
            } else if (model_pars[i] == "ISM" && fPropagModels[0]->GetISM()->GetFreePars()->GetNPars() > 0)
               message = message + ", valid free parameters are " + fPropagModels[0]->GetISM()->GetFreePars()->GetParNames();
            else if (model_pars[i] == "SrcPointLike" ||  model_pars[i] == "SrcSteadyState") {
               // Check in source astro
               for (Int_t jj = 0; jj < fPropagModels[0]->GetNSrcAstro(); ++jj) {
                  if (fPropagModels[0]->GetSrcAstro(jj)->GetFreePars()->GetNPars() > 0)
                     message = message + ", valid free parameters are " + fPropagModels[0]->GetSrcAstro(jj)->GetFreePars()->GetParNames();
               }
               // Check in DM sources
               for (Int_t jj = 0; jj < fPropagModels[0]->GetNSrcDM(); ++jj) {
                  if (fPropagModels[0]->GetSrcDM(jj)->GetFreePars()->GetNPars() > 0)
                     message = message + ", valid free parameters are " + fPropagModels[0]->GetSrcDM(jj)->GetFreePars()->GetParNames();
               }
            } else if (model_pars[i] == "Transport" && fPropagModels[0]->GetTransport()->GetFreePars()->GetNPars() > 0)
               message = message + ", valid free parameters are " + fPropagModels[0]->GetTransport()->GetFreePars()->GetParNames();
            TUMessages::Error(fOutputLog, "TURunPropagation", "UpdateFitParsAndFitData", message);
         }
      }
   }

   // Abort if no 'built-in free parameter for minimisation (i.e. geometry, transport, and sources)!
   if (indices_fitpars.size() == 0 && indices_solmodfitpars.size() == 0) {
      string message = "No free parameters, cannot proceed!";
      TUMessages::Error(fOutputLog, "TURunPropagation", "UpdateFitParsAndFitData", message);
   }

   // fFitPars points to all the above selected parameters
   // Reset (allocation)
   if (fFitPars) delete fFitPars;
   fFitPars = new TUFreeParList();
   for (Int_t i = 0; i < (Int_t)indices_fitpars.size(); ++i) {
      TUFreeParEntry *par = fPropagModels[0]->TUFreeParList::GetParEntry(indices_fitpars[i]);
      par->SetFitInit(type_fitpars[i], fOutputLog);
      fFitPars->UseParEntry(par);
      par = NULL;
   }

   ////////////////////////////////////////////
   // ADD ADDITIONAL FIT/NUISANCE PARAMETERS //
   ////////////////////////////////////////////
   // Solar modulation parameters...
   for (Int_t i = 0; i < (Int_t)indices_solmodfitpars.size(); ++i) {
      TUFreeParEntry *par = fSolModModels[0]->ParsModExps_Get(indices_solmodfitpars[i].first)->GetParEntry(indices_solmodfitpars[i].second);
      par->SetFitInit(type_solmodfitpars[i], fOutputLog);
      fFitPars->UseParEntry(par);
      par = NULL;
   }

   // Data error nuisance parameters...
   for (Int_t i = 0; i < (Int_t)indices_errnuispars.size(); ++i) {
      TUFreeParEntry *par = fFitData->ParsData_Get(indices_errnuispars[i].first)->GetParEntry(indices_errnuispars[i].second);
      par->SetFitInit(type_errnuispars[i], fOutputLog);
      fFitPars->UseParEntry(par);
      //par->Print(stdout);
      // Find index of Nuisance parameter in data matching current name 'i'
      // Update 'fIsUseCovType' (TUDataEntry) for type used as nuisance
      Int_t i_exp = fFitData->ParsData_IndexExp(indices_errnuispars[i].first);
      Int_t i_qty = fFitData->ParsData_IndexQty(indices_errnuispars[i].first);
      fFitData->ParsData_SetIsNuisParsInFit(indices_errnuispars[i].first, true);
      gENUM_ETYPE e_type = fFitData->ParsData_EType(indices_errnuispars[i].first);
      for (Int_t i_data = fFitData->IndexData(i_exp, i_qty, e_type); i_data < fFitData->GetNData(i_exp, i_qty, e_type); ++i_data) {
         //cout << par->GetName() << " " << fFitData->GetEntry(i_data)->GetYErrRelCovType(indices_errnuispars[i].second) << endl;
         fFitData->GetEntry(i_data)->SetIsCovTypeNuisPar(indices_errnuispars[i].second);
      }
   }

   // and loop on exp/qty/data to print configuration for covariance
   for (Int_t i_exp = 0; i_exp < fFitData->GetNExps(); ++i_exp) {
      // Loop on CR quantities (several per sub-exp)
      vector<Int_t> indices_qties;
      fFitData->IndicesQtiesInExp(i_exp, indices_qties);
      for (Int_t i = 0; i < (Int_t)indices_qties.size(); ++i) {
         Int_t i_qty = indices_qties[i];
         // Retrieve info from first energy bin (for covariance)
         string qty = fFitData->GetQty(i_qty);
         for (Int_t e = 0; e < gN_ETYPE; ++e) {
            gENUM_ETYPE etype = TUEnum::gENUM_ETYPE_LIST[e];
            Int_t i_data = fFitData->IndexData(i_exp, i_qty, etype);
            if (i_data < 0)
               continue;
            if (fFitDataErrType == kERRCOV && fFitData->GetEntry(i_data)->GetNCovE() != 0) {
               TUDataEntry *data = fFitData->GetEntry(i_data);
               PrintLog(Form("%s   - How errors are dealt with for exp/qty/etype %s %s %s",
                             indent.c_str(), data->GetExpName().c_str(),
                             qty.c_str(), TUEnum::Enum2Name(etype).c_str()),
                        is_logandstdout);
               for (Int_t t = 0; t < data->GetNCovTypes(); ++t) {
                  string text = "add in covariance ";
                  if (data->IsCovTypeNuisPar(t))
                     text = "add as nuisance parameter ";
                  text += data->GetYErrRelCovType(t);
                  PrintLog(Form("%s     - %s", indent.c_str(), text.c_str()), is_logandstdout);
               }
               data = NULL;
            }
         }
      }
   }

   // XS parameters...
   if (type_fitpars_xs.size() > 0) {
      for (Int_t i = 0; i < (Int_t)type_fitpars_xs.size(); ++i) {
         TUFreeParEntry par;
         par.SetFitInit(type_fitpars_xs[i], fOutputLog);
         par.SetPar(name_fitpars_xs[i], "-", par.GetFitInit());
         fFitPars->AddPar(&par, false /*is_use_or_copy*/);
      }
      fPropagModels[0]->ParsXS_SetPars(fFitPars, true /*is_use_or_copy*/, false/*is_verbose*/, fOutputLog);
      // Call update 'once' to print found reactions in nuisance (check-only)
      fPropagModels[0]->ParsXS_UpdateFromFreeParsAndResetStatus((TUMediumEntry *)NULL, true, false, fOutputLog);
      if (is_logandstdout)
         fPropagModels[0]->ParsXS_UpdateFromFreeParsAndResetStatus((TUMediumEntry *)NULL, true, false, stdout);
   }

   // If XS nuisance parameters uses linear combinations
   if (fPropagModels[0]->TUXSections::ParsXS_LinComb_GetNReac(0) != 0
         || fPropagModels[0]->TUXSections::ParsXS_LinComb_GetNReac(1) != 0) {
      const Int_t n_xstype = 2;
      vector<Double_t> sum_lc_pars[n_xstype]; // Sum for inel. and prod. pars
      vector<Double_t> sigma_lc_pars[n_xstype]; // sigma for inel. and prod. pars
      vector<string> reac_lc_pars[n_xstype]; // Reac names for inel. and prod. pars
      Int_t n_lc_reacs[n_xstype] = {fPropagModels[0]->TUXSections::ParsXS_LinComb_GetNReac(0),
                                    fPropagModels[0]->TUXSections::ParsXS_LinComb_GetNReac(1)
                                   };

      // Retrieve Sum and sigma for all reactions in LC (and names)
      SumCiAndSigmaForXSLC(&sum_lc_pars[0], &sigma_lc_pars[0], &reac_lc_pars[0]);

      PrintLog(Form("%s      - Linear combination constraint (each reaction): sum_coeffs = 1. (as defined by default value of \'Chi2FromNuisance\')", indent.c_str()), is_logandstdout);
      PrintLog(Form("%s      - Sigma on constraint:", indent.c_str()), is_logandstdout);
      for (Int_t is_inel_ow_prod = 1; is_inel_ow_prod >= 0; --is_inel_ow_prod)
         for (Int_t i_r = 0; i_r < n_lc_reacs[is_inel_ow_prod]; ++i_r)
            PrintLog(Form("%s         -> %f for %s (initial sum_Ci=%f)", indent.c_str(), sigma_lc_pars[is_inel_ow_prod][i_r],
                          reac_lc_pars[is_inel_ow_prod][i_r].c_str(), sum_lc_pars[is_inel_ow_prod][i_r]), is_logandstdout);
   }


   // CR parameters...
   if (type_fitpars_crs.size() > 0) {
      Int_t i_first = fFitPars->GetNPars();
      Int_t i_last = i_first + type_fitpars_crs.size();
      for (Int_t i = 0; i < (Int_t)type_fitpars_crs.size(); ++i) {
         TUFreeParEntry par;
         par.SetFitInit(type_fitpars_crs[i], fOutputLog);
         par.SetPar(name_fitpars_crs[i], "Myr", par.GetFitInit());
         fFitPars->AddPar(&par, false /*is_use_or_copy*/);
      }
      fPropagModels[0]->ParsHalfLife_AddPars(fFitPars, i_first, i_last, true /*is_use_or_copy*/, false/*is_verbose*/, fOutputLog);
   }


   //-----------------
   //  4. OUTPUTS
   //-----------------
   if (fFitPars->GetNPars() != 0) {
      // Outputs after fit
      gr_sub_name[1] = "Outputs";

      if (is_verbose)
         PrintLog(Form("%s### Set extra-outputs after fit", indent.c_str()), is_logandstdout);
      // Whether to print or not Covariance matrix
      gr_sub_name[2] = "IsPrintCovMatrix";
      if (init_pars->IndexPar(gr_sub_name, false) >= 0)
         fIsPrintCovMatrix = TUMisc::String2Bool(init_pars->GetParEntry(init_pars->IndexPar(gr_sub_name)).GetVal().c_str());
      PrintLog(Form("%s   - Print covariance matrix [%s]", indent.c_str(), fIsPrintCovMatrix ? "true" : "false"), is_logandstdout);
      // Whether to print or not Hessian matrix
      gr_sub_name[2] = "IsPrintHessianMatrix";
      if (init_pars->IndexPar(gr_sub_name, false) >= 0)
         fIsPrintHessianMatrix = TUMisc::String2Bool(init_pars->GetParEntry(init_pars->IndexPar(gr_sub_name)).GetVal().c_str());
      PrintLog(Form("%s   - Print Hessian matrix [%s]", indent.c_str(), fIsPrintHessianMatrix ? "true" : "false"), is_logandstdout);


      // Extract scans to perform: syntax is X,N, with
      //   X = 'parname' or 'ALL' (form all possible parameter pairs)
      //   N = number of points used to draw the contour
      // Example: Va:10
      ClearScansOrProfiles(true);
      gr_sub_name[2] = "Scans";
      PrintLog(Form("%s   - Scans to perform...", indent.c_str()), is_logandstdout);
      ExtractIndicesScansOrProfiles(init_pars, init_pars->IndexPar(gr_sub_name, false), true);
      if (GetNScans() == 0)
         PrintLog(Form("%s      -> no scan to perform", indent.c_str()), is_logandstdout);
      else {
         for (Int_t i = 0; i < GetNScans(); ++i) {
            PrintLog(Form("%s      - %s (using %d points)", indent.c_str(),
                          fFitPars->GetParEntry(fScans[i].i_par)->GetName().c_str(), fScans[i].n_pts),
                     is_logandstdout);
         }
      }


      // Extract scans to perform: syntax is X,N, with
      //   X = 'parname' or 'ALL' (form all possible parameter pairs)
      //   N = number of points used to draw the contour
      // Example: Va:10
      ClearScansOrProfiles(false);
      gr_sub_name[2] = "Profiles";
      PrintLog(Form("%s   - Profiles (likelihood) to extract...", indent.c_str()), is_logandstdout);
      ExtractIndicesScansOrProfiles(init_pars, init_pars->IndexPar(gr_sub_name, false), false);
      if (GetNProfiles() == 0)
         PrintLog(Form("%s      -> no profile to extract", indent.c_str()), is_logandstdout);
      else {
         for (Int_t i = 0; i < GetNProfiles(); ++i) {
            PrintLog(Form("%s      - %s (using %d points)", indent.c_str(),
                          fFitPars->GetParEntry(fProfiles[i].i_par)->GetName().c_str(), fProfiles[i].n_pts),
                     is_logandstdout);
         }
      }


      // Extract contours to plot: syntax is X,N,{CL1,CL2,...}, with
      //   X = 'parname1,parname2' or 'ALL' (form all possible parameter pairs)
      //   N = number of points used to draw the contour
      //   {CL1,CL2...} = confidence levels (in sigma) for with to plot contours
      // Example: Va,Vc":10:{1,2,3}
      ClearContours();
      gr_sub_name[2] = "Contours";
      PrintLog(Form("%s   - Contours to draw...", indent.c_str()), is_logandstdout);
      ExtractIndicesContours(init_pars, init_pars->IndexPar(gr_sub_name, false));
      if (GetNContours() == 0)
         PrintLog(Form("%s      -> no contour to plot", indent.c_str()), is_logandstdout);
      else {
         for (Int_t i = 0; i < GetNContours(); ++i) {
            PrintLog(Form("%s      - %s vs %s (using %d points) for CLs={%s}", indent.c_str(),
                          fFitPars->GetParEntry(fContours[i].i_par1)->GetName().c_str(),
                          fFitPars->GetParEntry(fContours[i].i_par2)->GetName().c_str(),
                          fContours[i].n_pts, TUMisc::List2String(fContours[i].cls, ',', 1).c_str()),
                     is_logandstdout);
         }
      }
   }



   //------------------------
   //  5. UPDATE DATA TO NORM (depending on free parameters set)
   //------------------------
   // If use norm list and fitted source parameters, update content of norm list to use
   if (IsUseNormList() && is_srcsteadystate) {
      if (is_verbose)
         PrintLog(Form("%s### Search and remove from NormList quantities whose normalisation is fitted", indent.c_str()), is_logandstdout);

      // N.B.: only one model in minimisation
      TUModelBase *model = GetPropagModel(0);
      // Retrieve norm list
      TUNormList *norm_list = model;
      Int_t n_norm = norm_list->GetNNorm();
      // Loop on all astro sources for model
      for (Int_t j = 0; j < model->GetNSrcAstro(); ++j) {
         // Retrieve names of free parameters for normalisation
         string norm_name = model->GetSrcAstro(j)->GetNameSrcNormPar();

         if (is_verbose)
            PrintLog(Form("%s   - For model %s and source %s (normalisation parameter is \'%s\')",
                          indent.c_str(), model->GetModelName().c_str(),
                          model->GetSrcAstro(j)->GetSrcName().c_str(), norm_name.c_str()), is_logandstdout);
         norm_name += "_";
         // Free pars for source normalisation are 'norm_name'_CR,
         // e.g., if 'norm_name'=q, the parameters are q_1H, q_2H, etc.
         // We loop on free parameters starting by 'q_', and if a CR isotope
         // to fit is present in NormList (directly as the isotope or as element),
         // we remove it from this list
         vector<string> crs_fitted;
         // Find list of CRs fitted
         for (Int_t p = 0; p < fFitPars->GetNPars(); ++p) {
            string par_name = fFitPars->GetParEntry(p)->GetName();
            if (par_name.substr(0, norm_name.size()) == norm_name) {
               crs_fitted.push_back(par_name.substr(norm_name.size(), par_name.size()));
               //cout << "Found and add par=" << par_name << "  i.e. CR=" << crs_fitted[crs_fitted.size()-1] << endl;
            }
         }
         // Loop on these CRS to sort by Z/CRsand extract Z/CRs not to norm in propagation
         if (crs_fitted.size() != 0) {
            vector<Int_t> z_not_to_norm = fPropagModels[0]->TUAxesCrE::SortByGrowingZ(crs_fitted);
            vector<vector<string> > crs_not_to_norm;
            // Loop on Z not to norm
            for (Int_t k = 0; k < (Int_t)z_not_to_norm.size(); ++k) {
               vector<string> tmp;
               for (Int_t i = 0; i < (Int_t)crs_fitted.size(); ++i) {
                  if (z_not_to_norm[k] == fPropagModels[0]->GetCREntry(fPropagModels[0]->TUAxesCrE::Index(crs_fitted[i], false)).GetZ()) {
                     tmp.push_back(crs_fitted[i]);
                     //cout << " ADD TO REMOVE (Z=" << z_not_to_norm[k] << ")=" << crs_fitted[i] << endl;
                  }
               }
               crs_not_to_norm.push_back(tmp);
            }
            fPropagModels[0]->TUNormList::UpdateAndFillNotToNorm(z_not_to_norm, crs_not_to_norm, is_verbose, fOutputLog);
         }
      }
      if (is_verbose) {
         if (n_norm == norm_list->GetNNorm())
            PrintLog(Form("%s      => Norm list unchanged", indent.c_str()), is_logandstdout);
         else {
            PrintLog(Form("%s      => %d norm removed, %d left", indent.c_str(), n_norm - norm_list->GetNNorm(), n_norm), is_logandstdout);
            norm_list->PrintNormData(fOutputLog);
            if (fOutputLog != stdout)
               norm_list->PrintNormData(stdout);
         }
      }
      norm_list = NULL;
      model = NULL;
   }

   if (is_verbose)
      PrintLog(Form("%s[TURunPropagation::UpdateFitParsAndFitData] <DONE>", indent.c_str()), is_logandstdout);
   TUMessages::Indent(false);
}