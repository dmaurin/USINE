// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

// C/C++ include
// ROOT include
// USINE include
#include "../include/TUAxesCrE.h"
#include "../include/TUMath.h"
#include "../include/TUValsTXYZEGrid.h"
#include "../include/TUMessages.h"

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TUValsTXYZEGrid                                                      //
//                                                                      //
// Values on a multi-dimensional grid (space-time and energy).          //
//                                                                      //
// The class TUValsTXYZEFormula provides values on a grid for           //
// quantities depending on, at most, space-time (TXYZ) and energy       //
// coordinates (it derives from the abstract class TUValsTXYZEVirtual). //
// It also applies to the two following restricted sub-spaces:          //
//    - only time and space coordinates T, X, Y, Z (pure space-time);
//    - only energy coordinate (no space-time).
//                                                                      //
// Space-time description is enabled from to 0D to 4D (T+3D), and the   //
// space coordinates are always associated to X for 1D, XY for 2D, and  //
// XYZ for 3D (the description of what X, Y and Z actually are is left  //
// to the class TUAxesTXYZ).                                            //
//                                                                      //
// The multidimensional array of values is accessed and manipulated     //
// using the methods AllocateVals(), SetVals(), AddVals(),              //
// SubtractVals(), MultiplyVals(), KeepLowerVals()...                   //
//                                                                      //
//    1. The number of cells in each dimensions is obtained
//       using GetNX(), GetNT()... methods.
//    2. The subset of methods whose name contains ...ValsTXYZ() are
//       recommended to be used whenever the class describes a pure
//       space-time dependent quantity (no E 'coordinate'). In that case
//       an array bin_txyz[4] is used to identify a TXYZ bin, valid
//       whether the geometry is 1D, 2D, or 3D (w/wo any T dependence).
//                                                                      //
//  Note that internally, values are stored in a 1D array 'fVals',      //
//  and private methods of the class convert the 1D global bin index    //
//  to the corresponding T,X,Y,Z,E bin indices (and vice-versa).        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

ClassImp(TUValsTXYZEGrid)

//______________________________________________________________________________
TUValsTXYZEGrid::TUValsTXYZEGrid() : TUValsTXYZEVirtual()
{
   // ****** normal constructor ******

   Initialise(false);
}

//______________________________________________________________________________
TUValsTXYZEGrid::TUValsTXYZEGrid(TUValsTXYZEGrid const &vals) : TUValsTXYZEVirtual()
{
   // ****** Copy constructor ******
   //  vals              Object to copy

   Initialise(false);
   Copy(vals);
}

//______________________________________________________________________________
TUValsTXYZEGrid::~TUValsTXYZEGrid()
{
   // ****** Default destructor ******

   Initialise(true);
}

//______________________________________________________________________________
void TUValsTXYZEGrid::AddOrSubtractVals(TUValsTXYZEGrid *array, Bool_t is_add_or_sub)
{
   //--- Adds (or subtracts) 'array' to current class values.
   //  array             Array of values
   //  is_add_or_sub     If true (this+array); if false (this-array)

   // Check whether the class is based on a formula/spline or array of values
   if (ValsTXYZEFormat() != 1) {
      TUMessages::Error(stdout, "TUValsTXYZEGrid", "AddOrSubtractVals",
                        "class is not a grid: cannot set values");
      return;
   }

   if (IsSameFormat(array)) {
      if (is_add_or_sub) {
         for (Int_t global_bin = fNCells - 1; global_bin >= 0; --global_bin)
            fVals[global_bin] += array->GetVal(global_bin);
      } else {
         for (Int_t global_bin = fNCells - 1; global_bin >= 0; --global_bin)
            fVals[global_bin] -= array->GetVal(global_bin);
      }
   }
}

//______________________________________________________________________________
void TUValsTXYZEGrid::AddOrSubtractVals(TUValsTXYZEGrid *array, Int_t *bins_txyz,
                                        Bool_t is_add_or_sub)
{
   //--- Adds (or subtracts) 'array' (all position/energy).
   //  array             Array of values
   //  bins_txyz         Indices[4] for space-time coordinates T, X, Y, Z
   //  is_add_or_sub     If true (this+array); if false (this-array)

   // Check whether the class is based on a formula/spline or array of values
   if (ValsTXYZEFormat() != 1) {
      TUMessages::Error(stdout, "TUValsTXYZEGrid", "AddOrSubtractVals",
                        "class is not an grid: cannot set values");
      return;
   }

   if (IsSameFormat(array)) {
      Double_t *vals_to_add = array->GetAddressVals(bins_txyz);
      Double_t *vals = GetAddressVals(bins_txyz);

      if (is_add_or_sub) {
         for (Int_t k = GetNE() - 1; k >= 0; --k)
            vals[k] += vals_to_add[k];
      } else {
         for (Int_t k = GetNE() - 1; k >= 0; --k)
            vals[k] -= vals_to_add[k];
      }
      vals_to_add = NULL;
      vals = NULL;
   }
}

//______________________________________________________________________________
void TUValsTXYZEGrid::AllocateVals(TUAxesTXYZ *axes_txyz, Int_t ne)
{
   //--- Allocates *fVals for NT*NX*NY*NZ*NE taken from axes_txyz.
   //  axes_txyz         T,X,Y,Z axes from which we get NT,NX,NY,NT (if exist)
   //  ne                Energy bins

   if (!axes_txyz)
      return;

   Int_t nt = 0, nx = 0, ny = 0, nz = 0;
   if (axes_txyz->IsT())
      nt = axes_txyz->GetNT();
   if (axes_txyz->IsX())
      nx = axes_txyz->GetNX();
   if (axes_txyz->IsY())
      ny = axes_txyz->GetNY();
   if (axes_txyz->IsZ())
      nz = axes_txyz->GetNZ();

   AllocateVals(ne, nt, nx, ny, nz);
}

//______________________________________________________________________________
void TUValsTXYZEGrid::AllocateVals(Int_t ne, Int_t nt, Int_t nx, Int_t ny, Int_t nz)
{
   //--- Allocates *fVals for NT*NX*NY*NZ*NE.
   //  ne                Number of Energy bins
   //  nt                Number of time bins
   //  nx                Number of x (1st spatial coordinate) bins
   //  ny                Number of y (2nd spatial coordinate) bins
   //  nz                Number of z (3rd spatial coordinate) bins

   // Free memory if necessary
   Initialise(true);

   // Set simple class members
   fNCellsTXYZ = max(1, nx) * max(1, ny) * max(1, nz) * max(1, nt);
   fNCells = fNCellsTXYZ * max(1, ne);
   fNDimXYZ = 0;
   if (nx > 1)
      ++fNDimXYZ;
   if (ny > 1)
      ++fNDimXYZ;
   if (nz > 1)
      ++fNDimXYZ;

   // Check whether coordinates are rightly set
   // (i.e. fNDimXYZ=1->X, fNDimXYZ=2->XY, fNDimXYZ=3->XYZ)
   if (fNDimXYZ == 1 && nx < 1)
      TUMessages::Error(stdout, "TUValsTXYZEGrid", "Allocate",
                        "For 1D, X 'axis' must be used!");
   else if (fNDimXYZ == 2 && nz > 1)
      TUMessages::Error(stdout, "TUValsTXYZEGrid", "Allocate",
                        "For 2D, XY 'axes' must be used!");


   // Allocate and fill fN (one for each T,X,Y,Z,E coordinate)
   fN.push_back(max(0, nt));
   fN.push_back(max(0, nx));
   fN.push_back(max(0, ny));
   fN.push_back(max(0, nz));
   fN.push_back(max(0, ne));

   // Allocate and initialise fVals
   fVals = new Double_t[fNCells];
   for (Int_t i = 0; i < fNCells; ++i)
      fVals[i] = 0.;
}

//______________________________________________________________________________
void TUValsTXYZEGrid::Copy(TUValsTXYZEGrid const &vals)
{
   //--- Copies to current class.
   //  vals              Formula/array of values to copy from

   // Free memory if necessary
   Initialise(true);

   // Allocate memory for class and copy values in fVals
   AllocateVals(vals.GetNE(), vals.GetNT(), vals.GetNX(), vals.GetNY(), vals.GetNZ());
   for (Int_t global_bin = fNCells - 1; global_bin >= 0; --global_bin)
      fVals[global_bin] = vals.GetVal(global_bin);
}

//______________________________________________________________________________
void TUValsTXYZEGrid::GetBinETXYZ(Int_t global_bin, Int_t &bine, Int_t *bins_txyz) const
{
   //--- Returns bint, binx, biny, and binz corresponding to global_bin
   //    global_bin = binz*NY*NX*NT*NE + biny*NX*NT*NE + binx*NT*NE + bint*NE + bine
   //               = bine + NE*(bint + NT*(binx+NX*(biny+NY*binz)))).
   // INPUT:
   //  global_bin        Global bin index
   // OUTPUTS:
   //  bine              Index of E corresponding to global_bin
   //  bins_txyz         Indices[4] for time coord(T), 1st(X), 2nd(Y), and 3rd(Z) spatial coord

   // Set defaults values
   bine = 0;
   Int_t bint = 0;
   Int_t binx = 0;
   Int_t biny = 0;
   Int_t binz = 0;

   // If E coordinates
   if (IsE()) {
      bine = global_bin % GetNE();
      //tmp1 = (global_bin-bine)/NE = bint + NT*(binx+NX*(biny+NY*binz)))
      Int_t tmp1 = (global_bin - bine) / GetNE();

      // If time coordinate (and possibly XYZ)
      if (IsT()) {
         bint = tmp1 % GetNT();
         if (IsX()) {
            //tmp2 = (tmp1-bint)/NT = binx+NX*(biny+NY*binz)
            Int_t tmp2 = (tmp1 - bint) / GetNT();
            binx = tmp2 % GetNX();
            if (IsY()) {
               //tmp3 = (tmp2-binx)/NX = biny+NY*binz
               Int_t tmp3 = (tmp2 - binx) / GetNX();
               biny = tmp3 % GetNY();
               if (IsZ()) {
                  binz = (tmp3 - biny) / GetNY();
               }
            }
         }
         // If pure spatial coord.
      } else {
         if (IsX()) {
            binx = tmp1 % GetNX();
            if (IsY()) {
               //tmp2 = (tmp1-binx)/NX = biny+NY*binz
               Int_t tmp2 = (tmp1 - binx) / GetNX();
               biny = tmp2 % GetNY();
               if (IsZ()) {
                  binz = (tmp2 - biny) / GetNY();
               }
            }
         }
      }

      // No E coordinate (pure spatial and time coords.)
   } else {
      // If time coordinate (and possibly XYZ)
      if (IsT()) {
         // globalbin = binz*NY*NX*NT + biny*NX*NT + binx*NT + bint
         //           = bint + NT*(binx+NX*(biny+NY*binz))
         bint = global_bin % GetNT();
         if (IsX()) {
            //tmp1 = (global_bin-bint)/NT = binx+NX*(biny+NY*binz)
            Int_t tmp1 = (global_bin - bint) / GetNT();
            binx = tmp1 % GetNX();
            if (IsY()) {
               //tmp2 = (tmp1-binx)/NX = biny+NY*binz
               Int_t tmp2 = (tmp1 - binx) / GetNX();
               biny = tmp2 % GetNY();
               if (IsZ()) {
                  binz = (tmp2 - biny) / GetNY();
               }
            }
         }
         // If pure spatial coord.
      } else {
         // globalbin = binz*NY*NX + biny*NX + binx
         //           = binx+NX*(biny+NY*binz))
         if (IsX()) {
            binx = global_bin % GetNX();
            if (IsY()) {
               Int_t tmp1 = (global_bin - binx) / GetNX();
               biny = tmp1 % GetNY();
               if (IsZ()) {
                  binz = (tmp1 - biny) / GetNY();
               }
            }
         }
      }
   }

   bins_txyz[0] = bint;
   bins_txyz[1] = binx;
   bins_txyz[2] = biny;
   bins_txyz[3] = binz;
}

//______________________________________________________________________________
void TUValsTXYZEGrid::GetBinETXYZ(Int_t global_bin, Int_t &bine, TUCoordTXYZ *coord_txyz) const
{
   //--- Returns bint, binx, biny, and binz (in coord_txyz) corresponding to global_bin
   //      global_bin = binz*NY*NX*NT*NE + biny*NX*NT*NE + binx*NT*NE + bint*NE + bine
   //                 = bine + NE*(bint + NT*(binx+NX*(biny+NY*binz)))).
   // INPUT:
   //  global_bin        Global bin index
   // OUTPUTS:
   //  bine              Index of E corresponding to global_bin
   //  coord_txyz       Space-time bin coord(T), 1st(X), 2nd(Y), and 3rd(Z) spatial coord

   Int_t *bins = coord_txyz->GetBins();
   GetBinETXYZ(global_bin, bine, bins);
   bins = NULL;
}

//______________________________________________________________________________
Int_t TUValsTXYZEGrid::GetBinGlobal(Int_t bine, Int_t *bins_txyz) const
{
   //--- Returns Global bin number corresponding to e, and t,x,y,z if exists (see also GetBinTXYZ)
   //      global_bin = binz*NY*NX*NT*NE + biny*NX*NT*NE + binx*NT*NE + bint*NE + bine.
   //  bine              Index of E coordinate corresponding to global_bin
   //  bins_txyz         Indices[4] for time coord(T), 1st(X), 2nd(Y), and 3rd(Z) spatial coord

   // In 'bins_txyz', an unused bin is set to -1. For the formula
   // below to work, we have to set the corresponding bins to 0
   Int_t bint = 0;
   Int_t binx = 0;
   Int_t biny = 0;
   Int_t binz = 0;
   if (bins_txyz) {
      bint = max(0, bins_txyz[0]);
      binx = max(0, bins_txyz[1]);
      biny = max(0, bins_txyz[2]);
      binz = max(0, bins_txyz[3]);
   }

   if (IsE()) { // E coordinates
      if (bine < 0 || bine >= GetNE()) {
         string message = "E bin =" + (string)Form("%d", bine) + " is out of range!";
         TUMessages::Error(stdout, "TUValsTXYZEGrid", "GetBinGlobal", message);
      }

      if (IsT())  // If time coordinate (and possibly XYZ)
         return bine + GetNE() * (bint + GetNT() * (binx + GetNX() * (biny +  GetNY() * binz)));
      else        // If pure spatial coord.
         return bine + GetNE() * (binx + GetNX() * (biny +  GetNY() * binz));
   } else {       // No E coordinates (pure spatial and time coords.)
      if (IsT())  // If time coordinate (and possibly XYZ)
         return bint + GetNT() * (binx + GetNX() * (biny +  GetNY() * binz));
      else        // If pure spatial coord.
         return binx + GetNX() * (biny +  GetNY() * binz);
   }
}

//______________________________________________________________________________
void TUValsTXYZEGrid::Initialise(Bool_t is_delete)
{
   //--- Deletes/Initialises members.
   //  is_delete         Whether to delete or not class

   if (is_delete) {
      if (fVals) delete[] fVals;
   }

   fFileName = "";
   fN.clear();
   fNCells = 0;
   fNCellsTXYZ = 0;
   fNDimXYZ = 0;
   fVals = NULL;
}

//______________________________________________________________________________
Bool_t TUValsTXYZEGrid::IsSameFormat(TUValsTXYZEGrid *vals) const
{
   //--- Checks whether the format of vals matches that of the class.
   //  vals              Array/formula to check

   if (!vals)
      return false;


   if (vals->ValsTXYZEFormat() != 1)
      return false;

   // Class not allocated
   if (!fVals || fN.size() == 0) {
      TUMessages::Error(stdout, "TUValsTXYZEGrid", "IsSameFormat",
                        "Members fVals and fN not allocated yet: nothing to do");
      return false;
   }

   // array_txyz not allocated
   if (!IsT() && !IsE() && !IsX()) {
      TUMessages::Error(stdout, "TUValsTXYZEGrid", "IsSameFormat",
                        "vals passed is empty: nothing to do");
      return false;
   }

   // If not the same dimension
   if (GetNX() != vals->GetNX() || GetNY() != vals->GetNY() || GetNZ() != vals->GetNZ() ||
         GetNT() != vals->GetNT() || GetNE() != vals->GetNE()) {
      TUMessages::Error(stdout, "TUValsTXYZEGrid", "IsSameFormat",
                        "vals passed and current class format are different, cannot proceed!");
      return false;
   }

   return true;
}

//______________________________________________________________________________
Bool_t TUValsTXYZEGrid::IsSameFormat(Int_t ne, TUAxesTXYZ *axes_txyz) const
{
   //--- Checks whether the format of e and TXYZ axes match that of the class.
   //  ne                Number of E points
   //  axes_txyz         Class of TXYZ grid values

   if (!axes_txyz)
      return false;

   // If these are not arrays
   if (ValsTXYZEFormat() != 1) {
      TUMessages::Error(stdout, "TUValsTXYZEGrid", "IsSameFormat",
                        "class is not a grid: compare format is disabled");
      return false;
   }

   // Class not allocated
   if (!fVals || fN.size() == 0) {
      TUMessages::Error(stdout, "TUValsTXYZEGrid", "IsSameFormat",
                        "Members fVals and fN not allocated yet: nothing to do");
      return false;
   }

   // If not the same dimension
   if (GetNCellsTXYZ() > 1) {
      TUMessages::Error(stdout, "TUValsTXYZEGrid", "IsSameFormat",
                        "axes_txyz is empty whereas class' TXYZ is not, cannot proceed!");
      return false;
   } else if (GetNE()  !=  ne || !IsSameFormat(axes_txyz)) {
      TUMessages::Error(stdout, "TUValsTXYZEGrid", "IsSameFormat",
                        "array passed and current class have different fNE, cannot proceed!");
      return false;
   }

   return true;
}

//______________________________________________________________________________
Bool_t TUValsTXYZEGrid::IsSameFormat(TUAxesTXYZ *axes_txyz) const
{
   //--- Checks whether the format of axes matches that of the class (in terms
   //    of NDim and #of steps).
   //  axes_txyz         Class of TXYZ grid values

   if (!axes_txyz)
      return false;

   // If these are formulae, not arrays
   if (ValsTXYZEFormat() != 1) {
      TUMessages::Error(stdout, "TUValsTXYZEGrid", "IsSameFormat",
                        "class is not a grid: compare format is disabled");
      return false;
   }

   // Class not allocated
   if (!fVals || fN.size() == 0) {
      TUMessages::Error(stdout, "TUValsTXYZEGrid", "IsSameFormatTXYZ",
                        "Members fVals and fN not allocated yet: nothing to do");
      return false;
   }

   // axes not allocated
   if (!axes_txyz->IsT() && axes_txyz->GetNDim() == 0) {
      TUMessages::Error(stdout, "TUValsTXYZEGrid", "IsSameFormatTXYZ",
                        "TXYZ axes passed is empty: nothing to do");
      return false;
   }

   // If not the same dimension
   if (GetNT() != axes_txyz->GetNT() || GetNX() != axes_txyz->GetNX()
         || GetNY() != axes_txyz->GetNY() || GetNZ() != axes_txyz->GetNZ()) {
      TUMessages::Error(stdout, "TUValsTXYZEGrid", "IsSameFormatTXYZ",
                        "TXYZ axes passed and current class format are different, cannot proceed!");
      return false;
   }

   return true;
}

//______________________________________________________________________________
void TUValsTXYZEGrid::KeepLowerVals(TUValsTXYZEGrid *array)
{
   //--- Compares and keep for each bin the lower value.
   //  array             Array of values

   if (IsSameFormat(array)) {
      for (Int_t global_bin = fNCells - 1; global_bin >= 0; --global_bin)
         fVals[global_bin] = min(fVals[global_bin], array->GetVal(global_bin));
   }
}

//______________________________________________________________________________
void TUValsTXYZEGrid::KeepUpperVals(TUValsTXYZEGrid *array)
{
   //--- Compares and keep for each bin the upper value.
   //  array             Array of values

   if (IsSameFormat(array)) {
      for (Int_t global_bin = fNCells - 1; global_bin >= 0; --global_bin)
         fVals[global_bin] = max(fVals[global_bin], array->GetVal(global_bin));
   }
}

//______________________________________________________________________________
void TUValsTXYZEGrid::MultiplyVals(Double_t const &val)
{
   //--- Multiplies all values to val.
   //  val               Value to copy

   // Check whether the class is formula/spline or array of values
   if (ValsTXYZEFormat() != 1) {
      TUMessages::Error(stdout, "TUValsTXYZEGrid", "MultiplyVals",
                        "class is not a grid: cannot set values");
      return;
   }

   for (Int_t global_bin = fNCells - 1; global_bin >= 0; --global_bin)
      fVals[global_bin] *= val;
}

//______________________________________________________________________________
void TUValsTXYZEGrid::PrintSummary(FILE *f, string const &description) const
{
   //--- Prints class content summary.
   //  f                 File in which to print
   //  description        Single word

   string indent = TUMessages::Indent(true);
   fprintf(f, "%s - %s", indent.c_str(), description.c_str());

   if (fFileName != "")
      fprintf(f, "%s   => Read from %s\n", indent.c_str(), fFileName.c_str());
   else
      PrintVals(f);
   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUValsTXYZEGrid::PrintVals(FILE *f) const
{
   //--- Prints energy-dependent vals (all energies) in class for a all CRs and bins.
   //  f                 File in which to print

   if (ValsTXYZEFormat() != 1)
      TUMessages::Error(stdout, "TUValsTXYZEGrid", "PrintVals",
                        "this class content is not an array of values!");

   TUCoordTXYZ coord_txyz;
   Int_t bine = -1;

   for (Int_t global_bin = 0; global_bin < GetNCells(); ++global_bin) {
      GetBinETXYZ(global_bin, bine, &coord_txyz);
      if (GetNE() == 0 || (GetNE() > 0 && bine == 0))
         PrintVals(f, coord_txyz.GetBins());
   }
}

//______________________________________________________________________________
void TUValsTXYZEGrid::PrintVals(FILE *f, Int_t bine) const
{
   //--- Prints for all positions TXYZ values for CR and E bins.
   //  f                 File in which to print

   if (ValsTXYZEFormat() != 1)
      TUMessages::Error(stdout, "TUValsTXYZEGrid", "PrintVals",
                        "this class content is not an array of values!");

   string indent = TUMessages::Indent(true);
   TUCoordTXYZ coord_txyz;
   Int_t be = -1;
   fprintf(f, "%s  bin_e=%d\n", indent.c_str(), bine);

   for (Int_t global_bin = 0; global_bin < GetNCells(); ++global_bin) {
      GetBinETXYZ(global_bin, be, &coord_txyz);
      if (bine == be)
         fprintf(f, "%s   %s   val=%le\n", indent.c_str(),
                 (coord_txyz.FormBins(true)).c_str(), GetVal(bine, coord_txyz.GetBins()));
   }
   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUValsTXYZEGrid::PrintVals(FILE *f, Int_t *bins_txyz) const
{
   //--- Prints energy-dependent vals (all energies) in class for a txyz bin (if not null).
   //  f                 File in which to print
   //  bins_txyz         Indices for spacetime coordinates T, X, Y, and Z

   if (ValsTXYZEFormat() != 1)
      TUMessages::Error(stdout, "TUValsTXYZEGrid", "PrintVals",
                        "this class content is not an array of values!");

   string indent = TUMessages::Indent(true);

   for (Int_t bine = 0; bine < GetNE(); ++bine) {
      if (bins_txyz) {
         // Set in TUCoordTXYZ to easily get formatted output
         TUCoordTXYZ coords;
         coords.SetBins(bins_txyz);
         fprintf(f, "%s  bin_e=%d   %s   val=%le\n", indent.c_str(), bine,
                 (coords.FormBins(false)).c_str(), GetVal(bine, bins_txyz));
      } else
         fprintf(f, "%s  bin_e=%d   [no-spacetime]   val=%le\n",
                 indent.c_str(), bine, GetVal(bine, bins_txyz));
   }
   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUValsTXYZEGrid::PrintValsTXYZ(FILE *f) const
{
   //--- Prints for all positions TXYZ values (pure-spacetime).
   //  f                 File in which to print

   if (ValsTXYZEFormat() != 1)
      TUMessages::Error(stdout, "TUValsTXYZEGrid", "PrintVals",
                        "this class content is not an array of values!");

   fprintf(f, " => Values\n");

   // Set in TUCoordTXYZ to easily get formatted output
   TUCoordTXYZ coords;
   vector<Int_t> bins(4, -1);

   string indent = TUMessages::Indent(true);
   Int_t be = -1;
   for (Int_t global_bin = 0; global_bin < GetNCells(); ++global_bin) {
      GetBinETXYZ(global_bin, be, &bins[0]);
      coords.SetBins(&bins[0]);
      fprintf(f, "%s  %s   val=%le\n", indent.c_str(),
              (coords.FormBins(true)).c_str(), GetValTXYZ(&bins[0]));
   }
   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUValsTXYZEGrid::SetClass(string const &file, Bool_t is_verbose, FILE *f_log, string const &commasep_vars,
                               TUFreeParList *free_pars, Bool_t is_use_or_copy_freepars)
{
   //--- Updates this class (array) from values read in file 'file'.
   //  file                     File name to read
   //  is_verbose               Verbose on or off
   //  f_log                    Log for USINE
   //  commasep_vars            List of comma-separated variables, e.g., "t,x,y,z" (default is no var)
   //  free_pars                List of free parameters (used only if formula and free_pars!=NULL)
   //  is_use_or_copy_freepars  Whether the parameters passed must be added (copied) or just used (pointed at)

   // Clear class
   Initialise(true);

   // TO DO
   TUMessages::Error(f_log, "TUValsTXYZEGrid", "SetClass",
                     "Fill class from a file => function to write!");
   if (is_verbose)
      return;

   if (!is_use_or_copy_freepars && file == "" && commasep_vars == "")
      delete free_pars;
}

//______________________________________________________________________________
void TUValsTXYZEGrid::SetVals(Double_t const &val)
{
   //--- Sets all values to val.
   //  val               Value to copy

   // Check whether the class is based on a formula or arrya of values
   if (ValsTXYZEFormat() != 1) {
      TUMessages::Error(stdout, "TUValsTXYZEGrid", "SetVals",
                        "class is not a grid: cannot set values");
      return;
   }

   for (Int_t global_bin = fNCells - 1; global_bin >= 0; --global_bin)
      fVals[global_bin] = val;
}

//______________________________________________________________________________
void TUValsTXYZEGrid::SetVals(TUValsTXYZEGrid *array)
{
   //--- Sets vals to 'array' (all position/energy).
   //  array             Array of values

   if (IsSameFormat(array)) {
      for (Int_t global_bin = fNCells - 1; global_bin >= 0; --global_bin)
         fVals[global_bin] = array->GetVal(global_bin);
   }
}

//______________________________________________________________________________
void TUValsTXYZEGrid::SetValsE(TUAxesCrE *axes_cre, Int_t bincr, vector<Double_t> const &vals)
{
   //--- Sets E-dependent vals for a given CR (bincr CR in axes_cre) from a vector.
   //  axes_cre                 List of CR and E axes (should be similar to the CR list from which fVals was built)
   //  bincr                    CR index for which to apply formula
   //  vals                     [NE] values

   // If class was previously a formula: initialise and allocate
   if (ValsTXYZEFormat() != 1) {
      Initialise(true);
      AllocateVals(axes_cre->GetNE());
   }

   // Check dimension of E
   if (GetNE() != axes_cre->GetNE())
      TUMessages::Error(stdout, "TUValsTXYZEGrid", "SetValsE",
                        Form("axes_cre (%d) and class (%d) do not have the same number NE",
                             axes_cre->GetNE(), GetNE()));

   // Check dimension of E
   if (GetNE() != (Int_t)vals.size())
      TUMessages::Error(stdout, "TUValsTXYZEGrid", "SetValsE",
                        Form("Number of elements in vals (%d) different from NE (%d)", (Int_t)vals.size(), GetNE()));

   // Check if CR bin is in range
   if (bincr < 0 || bincr >= axes_cre->GetNCRs()) {
      string message = "CR bin =" + (string)Form("%d", bincr) + " is out of range!";
      TUMessages::Error(stdout, "TUValsTXYZEGrid", "GetBinGlobal", message);
   }

   // Create TFormula and fill
   for (Int_t global_bin = fNCells - 1; global_bin >= 0; --global_bin) {
      Int_t bine = global_bin % GetNE();
      fVals[global_bin] = vals[bine];
   }
}

//______________________________________________________________________________
void TUValsTXYZEGrid::TEST(TUInitParList *init_pars, FILE *f)
{
   //--- Test function for this class (called in usine -t).
   //  init_pars         TUInitParList class of initialisation parameters
   //  f                 Output file for test

   string message =
      "Container for space-time+E dependent quantities on a grid";
   TUMessages::Test(f, "TUValsTXYZEGrid", message);

   /***************************/
   /*  Array-related methods  */
   /***************************/

   // Set class and test methods
   fprintf(f, "   > AllocateVals(ne=3, nt=0, nx=4, ny=2, nz=0);\n");
   AllocateVals(3, 0, 4, 2, 0);
   fprintf(f, "   > GetNE();                => %d\n", GetNE());
   fprintf(f, "   > GetNT();                => %d\n", GetNT());
   fprintf(f, "   > GetNX();                => %d\n", GetNX());
   fprintf(f, "   > GetNY();                => %d\n", GetNY());
   fprintf(f, "   > GetNZ();                => %d\n", GetNZ());
   fprintf(f, "   > GetNDimXYZ();           => %d\n", GetNDimXYZ());
   fprintf(f, "   > GetNCellsTXYZ();        => %d\n", GetNCellsTXYZ());
   fprintf(f, "   > GetNCells();            => %d\n", GetNCells());
   fprintf(f, "   > IsE();                  => %d\n", IsE());
   fprintf(f, "   > ValsTXYZEFormat();=> %d\n", ValsTXYZEFormat());
   fprintf(f, "   > IsT();                  => %d\n", IsT());
   fprintf(f, "   > IsX();                  => %d\n", IsX());
   fprintf(f, "   > IsY();                  => %d\n", IsY());
   fprintf(f, "   > IsZ();                  => %d\n", IsZ());
   fprintf(f, "\n");


   fprintf(f, " * Test private methods GetBinETXYZ() and GetBinGlobal() is involutive:\n");
   TUCoordTXYZ coords;
   Int_t bine = -1;
   for (Int_t global_bin = 0; global_bin < GetNCells(); ++global_bin) {
      GetBinETXYZ(global_bin, bine, &coords);
      fprintf(f, "     glob_bin=%3d: GetBinETXYZ()=> bin_e=%d  %s",
              global_bin, bine, (coords.FormBins(true)).c_str());
      fprintf(f, "   GetBinGlobal()=> bin_glob=%3d\n",
              GetBinGlobal(bine, coords.GetBins()));
   }
   fprintf(f, "\n");


   fprintf(f, " * Set and print 'array' methods\n");
   fprintf(f, "   > TUCoordTXYZ coords_xy;      [create generic bin coord for txyz]\n");
   TUCoordTXYZ coords_xy;
   coords_xy.SetBinXY(2, 0);
   fprintf(f, "   > GetBinGlobal(bine=0, bins_xy=(2,0));   => %d\n",
           GetBinGlobal(0, coords_xy.GetBins()));
   fprintf(f, "   > PrintVals(f, coords_xy.GetBins());\n");
   PrintVals(f, coords_xy.GetBins());
   fprintf(f, "\n");

   fprintf(f, "   > SetVal(val=10,bine=1, bins_xy);\n");
   SetVal(10., 1, coords_xy.GetBins());
   fprintf(f, "   > PrintVals(f, coords_xy.GetBins());\n");
   PrintVals(f, coords_xy.GetBins());
   fprintf(f, "\n");

   fprintf(f, "   > SetVals(val=2);\n");
   SetVals(2);
   fprintf(f, "   > PrintVals(f, coords_xy.GetBins());\n");
   PrintVals(f, coords_xy.GetBins());
   fprintf(f, "\n");
   fprintf(f, "\n");


   fprintf(f, " * Copy, add, and multiply 'array' methods\n");
   fprintf(f, "   > TUValsTXYZ *array = new TUValsTXYZ();\n");
   TUValsTXYZEGrid *array = new TUValsTXYZEGrid();
   fprintf(f, "   > array->Copy(*this);\n");
   array->Copy(*this);
   fprintf(f, "   > array->PrintVals(f, coords_xy.GetBins());\n");
   array->PrintVals(f, coords_xy.GetBins());
   fprintf(f, "   > IsSameFormat(array);\n");
   IsSameFormat(array);
   fprintf(f, "   > array->SetVals(0.5);\n");
   array->SetVals(0.5);
   fprintf(f, "   > array->PrintVals(f, coords_xy.GetBins());\n");
   array->PrintVals(f, coords_xy.GetBins());
   fprintf(f, "\n");

   fprintf(f, "   > PrintVals(f, coords_xy.GetBins());\n");
   PrintVals(f, coords_xy.GetBins());
   fprintf(f, "   > AddVals(array);\n");
   AddVals(array);
   fprintf(f, "   > PrintVals(f, coords_xy.GetBins());\n");
   PrintVals(f, coords_xy.GetBins());
   fprintf(f, "   > Subtract(array);\n");
   SubtractVals(array);
   fprintf(f, "   > PrintVals(f, coords_xy.GetBins());\n");
   PrintVals(f, coords_xy.GetBins());
   fprintf(f, "\n");

   fprintf(f, "   > AddVals(array);\n");
   AddVals(array);
   fprintf(f, "   > PrintVals(f);\n");
   PrintVals(f);
   fprintf(f, "   > SubtractVals(array);\n");
   SubtractVals(array);
   fprintf(f, "   > PrintVals(f);\n");
   PrintVals(f);
   fprintf(f, "\n");

   fprintf(f, "   > MultiplyVals(factor=10);\n");
   MultiplyVals(10.);
   fprintf(f, "   > PrintVals(f);\n");
   PrintVals(f);

   fprintf(f, "\n");
   fprintf(f, "   > MultiplyVals(factor=3.);\n");
   MultiplyVals(3.);
   fprintf(f, "   > PrintVals(f);\n");
   PrintVals(f);

   fprintf(f, "\n");
   fprintf(f, "\n");
   fprintf(f, " * Set values E (no spacetime) from vector (reinitialise first):\n");
   fprintf(f, "   > Initialise(is_delete=true);\n");
   Initialise(kTRUE);
   fprintf(f, "   > TUAxesCrE *axes_cre = new TUAxesCrE();\n");
   fprintf(f, "   > axes_cre->SetClass(init_pars, f_log=f);\n");
   TUAxesCrE *axes_cre = new TUAxesCrE();
   axes_cre->SetClass(init_pars, false, f);
   fprintf(f, "   > AllocateVals(axes_cre->GetNE());\n");
   AllocateVals(axes_cre->GetNE());
   Int_t bincr = 10;
   fprintf(f, "   > SetValsE(axes_cre, bincr=10, vals from pow(Ekn,-2);\n");
   vector<Double_t> vals;
   for (Int_t k = 0; k < axes_cre->GetNE(); ++k)
      vals.push_back(pow(axes_cre->GetE(bincr, k, kEKN), -2));
   SetValsE(axes_cre, bincr, vals);
   Int_t *bins_txyz = NULL;
   fprintf(f, "   > axes_cre->GetE(bincr)->PrintSummary(f);\n");
   axes_cre->GetE(bincr)->PrintSummary(f);
   fprintf(f, "   > PrintVals(f, bins_txyz=NULL);\n");
   PrintVals(f, bins_txyz);


   fprintf(f, " * Clone method\n");
   fprintf(f, "   > TUValsTXYZEGrid *vals2 = Clone();\n");
   TUValsTXYZEGrid *vals2 = Clone();
   fprintf(f, "   > vals2->PrintValsTXYZ(f);\n");
   vals2->PrintValsTXYZ(f);
   fprintf(f, "\n");

   delete array;
   array = NULL;
   delete axes_cre;
   axes_cre = NULL;
   delete vals2;
   vals2 = NULL;
}
