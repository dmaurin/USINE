// @(#)USINE/include:$Id:
// Author(s): Maurin et al. (1999-2019)

// C/C++ include
#include <unistd.h>
#include <fstream>
// ROOT include
#include <TLegend.h>
#include <TRint.h>
#include <TROOT.h>
// USINE include
#include "../include/TUInteractions.h"
#include "../include/TUNumMethods.h"
#include "../include/TUModelBase.h"
#include "../include/TUModel0DLeakyBox.h"
#include "../include/TUModel1DKisoVc.h"
#include "../include/TUModel2DKisoVc.h"
#include "../include/TURunPropagation.h"
#include "../include/TURunOutputs.h"
#include "../include/TUSolMod0DFF.h"
#include "../include/TUSrcMultiCRs.h"
#include "../include/TUSrcPointLike.h"
#include "../include/TUSrcSteadyState.h"

Bool_t DrawFromFitAndCovMat(vector<vector<Double_t>> &drawn_values, TUFreeParList &loaded_pars, string const &fit_file, string const &covmat_file = "NONE", Int_t n_samples = 0, TURunPropagation *run = NULL);

//______________________________________________________________________________
Bool_t DrawFromFitAndCovMat(vector<vector<Double_t>> &drawn_values, TUFreeParList &loaded_pars,
                            string const &fit_file, string const &covmat_file, Int_t n_samples,
                            TURunPropagation *run)
{
   //--- Returns vector of drawn parameters sampled from fit and covariance matrix.
   //    We ensure that drawn parameters are within the range of run configuration.
   //    Returns true if parameters were drawn (i.e. covmat_file != "NONE")
   // INPUTS:
   //  fit_file             File name for best-fit parameters
   //  covmat_file          File name for covariance-matrix of errors (in NONE, do not fill)
   //  n_samples            Number of samples to draw (0 if not drawn)
   //  run                  Used to check boundary of parameters (if covmat_file used, i.e. drawn parameters)
   // OUTPUTS:
   //  drawn_values         [Nsamples][Npars] Values of parameters drawn
   //  loaded_pars          Best-fit values loaded from reading 'fit_file'


   // If covmat_file=='NONE', only use best-fit transport parameters, otherwise, load
   // cov. matrix of errors on best-fit parameters to propagate transport uncertainties.
   vector<Int_t> dummy;
   printf("    - Load parameter best-fit values (USINE-formatted file %s)\n", fit_file.c_str());
   loaded_pars.LoadFitValues(fit_file, dummy, /*is_skipped_fixed*/true, /*is_verbose*/false, stdout);
   //loaded_pars.PrintFitValsAndInit(stdout);
   drawn_values.clear();
   string cov_uc = covmat_file;
   TUMisc::UpperCase(cov_uc);
   if (cov_uc == "NONE")
      return false;


   // If covariance matrix to draw from
   printf("    - Load cov. matrix from fit (USINE-formatted file %s)\n", covmat_file.c_str());
   loaded_pars.LoadFitCovMatrix(covmat_file, dummy, /*is_verbose*/false, stdout);
   //loaded_pars.PrintFitCovMatrix(stdout);
   printf("        => check two files have same parameters (FIXED skipped in fit file)\n");
   loaded_pars.CheckMatchingParNamesAndCovParNames(stdout);

   // Extract best-fit vector and cov.matrix in suitable format to later draw from
   TVectorD bestfit = loaded_pars.ExtractBestFitVals();
   TMatrixDSym covmat = loaded_pars.GetFitCovMatrix();

   Int_t i_sample = 0;
   for (Int_t i_try = 0; i_sample < n_samples; ++i_try) {
      // Draw new parameters
      TVectorD pars_gen;
      TUMath::DrawFromMultiGauss(bestfit, covmat, pars_gen);
      // Update loaded pars from new generated parameters. Note that drawn
      // parameters might be in 'log', and need to be converted appropriately.
      vector<Double_t> drawn(loaded_pars.GetNPars(), 0.);
      for (Int_t i = 0; i < loaded_pars.GetNPars(); ++i) {
         loaded_pars.GetParEntry(i)->SetVal(pars_gen(i), loaded_pars.GetParEntry(i)->GetFitSampling());
         drawn[i] = pars_gen(i);
      }
      // Update current run parameters from content of 'loaded_pars'
      if (!run->GetFitPars()->UpdateParVals(loaded_pars, /*gENUM_FITPARTYPE*/kANY, /*is_enforce_allfound*/ true, /*is_check_range*/true, stdout))
         continue;
      else
         drawn_values.push_back(drawn);

      ++i_sample;
   }
   return true;
}

//______________________________________________________________________________
//______________________________________________________________________________
Int_t main(Int_t argc, char *argv[])
{
   //--- USINE run!
   string git_tag = TUMisc::SystemExec("git --git-dir=$USINE/.git describe --abbrev=7 --always --tags");
   git_tag = TUMisc::RemoveBlancksFromStartStop(TUMisc::RemoveChars(git_tag, "\n"));
   string usine_version_print = gUSINE_VERSION.substr(6) + ", git_ID=" + git_tag;
   printf("                    (%s)\n", usine_version_print.c_str());


   // ROOT: OPEN root application and set USINE plot style
   TApplication *my_app = new TApplication("App", NULL, NULL);
   TUMisc::RootSetStylePlots();


   // Minimisation on TOA fluxes
   if (argc < 11) {
      printf("USAGE:   %s init_file  init_file_parents  output_dir  bestfit_file  cov_file  xs_dir  is_fluxerr  n_samples  e_power  is_showplots\n"
             "EXAMPLE: %s $USINE/PBAR/init.BIG.par  $USINE/PBAR/init.BIG.par  $USINE/output  $USINE/PBAR/fit_result.BIG.out $USINE/PBAR/fit_covmatrix.BIG.out  \"$USINE/PBAR/param*\"  0  100  0.  1\n"
             "\n"
             "   [init_file]          USINE initialisation file for antinuclei\n"
             "   [init_file_parents]  USINE initialisation file for fit of parent fluxes\n"
             "   [output_dir]         Directory for outputs (plots, files, etc.)\n"
             "   [bestfit_file]       Best-fit file (USINE format)\n"
             "   [cov_file]           Covariance file to propagate propagation errors: use NONE for best-fit only\n"
             "   [xs_dir]             Directory of XS to use (to propagate errors): use NONE to use XS file from initialisation file\n"
             "   [is_fluxerr]         Fit primary source param only (false) or use derived covariance matrix to propagate errors\n"
             "   [n_samples]          Number of samples to draw from covariance matrix (as many calculations)\n"
             "   [e_power]            Fluxes are multiplied by E^e_power, where E is e_type\n"
             "   [is_showplots]       Show plots (1) or batch mode (0)\n",
             argv[0], argv[0]);
      printf("\n");
      return 0;
   }

   // Read user parameters
   string init_file = argv[1];
   string init_file_parents = argv[2];
   string output_dir = argv[3];
   string fit_file = argv[4];
   string cov_file = argv[5];
   string xs_dir = argv[6];
   Bool_t is_flux_err = atoi(argv[7]);
   Int_t n_samples = atoi(argv[8]);
   Double_t e_power = atof(argv[9]);
   Bool_t is_showplots = atoi(argv[10]);
   Bool_t is_median = true;
   Bool_t is_1sigma = true;
   Bool_t is_2sigma = true;
   Bool_t is_3sigma = false;
   string combos_etypes_phiff = "1H-BAR:kR:0.";
   Bool_t is_verbose = true;
   Bool_t is_saveinfiles = true;
   gROOT->SetBatch(!is_showplots);



   TUMessages::Separator(stdout, "INITIALISATION PBAR", 1);
   // -> Set Output dir and log file
   printf("### Output dir (for run): %s\n", output_dir.c_str());
   TUMisc::CreateDir(output_dir, "usine_pbar", "");
   string f_name = output_dir + "/usine.last_run.log";
   FILE *f_log = fopen(f_name.c_str(), "w");
   printf("### Log file: %s\n", f_name.c_str());
   //
   // -> Initialise run for antinuclei
   printf("### USINE parameters for antinuclei:\n");
   printf("    - Load %s\n", init_file.c_str());
   TUInitParList *init_pars = new TUInitParList();
   init_pars->SetClass(init_file, is_verbose, f_log);
   printf("    - Initialise class for antinuclei and fit primaries\n");
   TURunPropagation *run = new TURunPropagation();
   run->SetClass(init_pars, is_verbose, output_dir, f_log);
   run->SetIsSaveInFiles(is_saveinfiles);
   printf("    - Current parameters (set to kFIXED) used in run:\n");
   run->UpdateFitParsAndFitData(init_pars, is_verbose, true);
   if (run->GetNPropagModels() > 1)
      TUMessages::Error(stdout, "usine_pbar", "", "Option not enabled if more than 1 propagation model selected (antinuclei)");
   printf("\n\n");
   // N.B.: is this run, all parameters are set to FIXED (use Minimiser in 'simple' calculation mode)
   for (Int_t i_par = 0; i_par < run->GetFitPars()->GetNPars(); ++i_par)
      run->GetFitPars()->GetParEntry(i_par)->SetFitType(kFIXED);
   run->GetFitPars()->ResetStatusParsAndParList();
   run->GetFitPars()->PrintPars(stdout);
   run->SetMinuitPrintLevel(0);
   run->SetIsPrintHessianMatrix(false);
   run->SetIsPrintCovMatrix(false);
   run->SetIsPrintBestFit(false);
   run->SetIsCLMode(true);
   printf("\n\n");


   TUMessages::Separator(stdout, "INITIALISATION PARENTS", 1);
   // -> Initialise run for parents
   printf("### USINE parameters for parents:\n");
   printf("    - Load %s\n", init_file_parents.c_str());
   TUInitParList *init_pars_parents = new TUInitParList();
   init_pars_parents->SetClass(init_file_parents, is_verbose, f_log);
   TURunPropagation *run_parents = new TURunPropagation();
   run_parents->SetClass(init_pars_parents, true /*is_verbose*/, output_dir, f_log);
   run_parents->SetIsSaveInFiles(is_saveinfiles);
   if (run_parents->GetNPropagModels() > 1)
      TUMessages::Error(stdout, "usine_pbar", "", "Option not enabled if more than 1 propagation model selected (parents)");
   printf("         => minimise for these transport parameters\n");
   run_parents->UpdateFitParsAndFitData(init_pars_parents, is_verbose, true);
   printf("\n\n");
   run_parents->GetFitPars()->PrintFitInit(stdout);


   // Configuration for error propagation
   TUMessages::Separator(stdout, "CONFIGURATION: UNCERTAINTIES PROPAGATED", 1);
   //
   // Transport:
   //    - if cov_file=='NONE', only use best-fit transport parameters,
   //    - otherwise, loads cov. matrix of best-fit params and draw sample values
   vector<vector<Double_t>> drawn_vals;
   TUFreeParList loaded_pars;
   Bool_t is_propag_cls = DrawFromFitAndCovMat(drawn_vals, loaded_pars, fit_file, cov_file, n_samples, run);
   if (!is_propag_cls)
      printf("        => Transport uncertainties NOT propagated\n");
   else
      printf("        => Propagation of transport uncertainties: use params drawn from cov.matrix\n");
   //
   // Source:
   //    - if is_flux_err = false: only use best-fit source parameters (minimisation performed later)
   //    - otherwise: after minimisation, draw parameters from cov.mat of best-fit source parameters (see below)
   if (!is_flux_err)
      printf("        => Source uncertainties NOT propagated\n");
   else
      printf("        => Propagation of source uncertainties: use params drawn from cov.matrix\n");
   //
   // Propagation of errors for XS
   //    - if xs_dir=='NONE': only use current XS file (from 'init_file' file)
   //    - otherwise: load all files from xs_dir and use them to propagate XS uncertainties.
   string xsdir_uc = xs_dir;
   TUMisc::UpperCase(xsdir_uc);
   vector<string> xs_files;
   Bool_t is_xs_cls = false;
   if (xsdir_uc == "NONE")
      printf("        => XS uncertainties NOT propagated\n");
   else {
      printf("        => Propagation of XS uncertainties: use all XS files from %s\n", xs_dir.c_str());
      xs_files = TUMisc::ExtractFromDir(xs_dir, /*is_print*/ true, stdout, /*is_selection*/ false, /*exclude_pattern*/"", /*is_test*/false);
      is_xs_cls = true;
   }


   ///////////////////////////////////////
   ///////// START CALCULATION ///////////
   ///////////////////////////////////////

   string message = "Uncertainties for " + combos_etypes_phiff;
   TUMessages::Separator(stdout, message, 1);

   // Loop on number of configurations to calculate
   Int_t n_digits = TUMisc::Number2Digits(n_samples);
   vector<TURunOutputs> sample_results;
   if (is_flux_err && is_propag_cls) {
      printf("    - Loop on %dx%d configurations to calculate %s\n", n_samples, n_samples, combos_etypes_phiff.c_str());
      run->SetNSamples(n_samples * n_samples);
      sample_results.resize(n_samples * n_samples);
   } else {
      printf("    - Loop on %d configurations to calculate %s\n", n_samples, combos_etypes_phiff.c_str());
      run->SetNSamples(n_samples);
      sample_results.resize(n_samples);
   }

   // If draw transport parameters, we save corresponding phi for further usage
   FILE *fp_phi_drawn_bc;
   if (is_propag_cls) {
      string output_phi = output_dir + "/phi_drawn4samples.txt";
      fp_phi_drawn_bc = fopen(output_phi.c_str(), "w");
      fprintf(fp_phi_drawn_bc, "i_sample  #sample  phi_B/C[GV]/\n");
   }


   // Loop on samples to calculate
   //-----------------------------
   for (Int_t i_sample = 0; i_sample < n_samples; ++i_sample) {
      // ID of config
      string id_config = Form("%0*d", n_digits, i_sample + 1);
      if ((!is_propag_cls && i_sample == 0) || is_propag_cls) {
         string message = Form("propagation config. #%s/%d", id_config.c_str(), n_samples);
         TUMessages::Separator(stdout, message, 2);
         //run->GetFitPars()->PrintPars(stdout);
      }

      // -> Transport uncertainties
      if (is_propag_cls) {
         printf("   - Update transport parameters from drawn values\n");
         // Update loaded pars from new generated parameters.
         for (Int_t i = 0; i < loaded_pars.GetNPars(); ++i)
            loaded_pars.GetParEntry(i)->SetVal(drawn_vals[i_sample][i], loaded_pars.GetParEntry(i)->GetFitSampling());
         loaded_pars.PrintPars();
      }
      // Update run parameters values if first time or if CLs
      if (is_propag_cls || i_sample == 0) {
         run->GetFitPars()->UpdateParVals(loaded_pars, /*gENUM_FITPARTYPE*/kANY, /*is_enforce_allfound*/ true, /*is_check_range*/false, stdout);
         run->GetFitPars()->PrintPars();
      }

      //////////////////////////////
      // Parent flux minimisation //
      //////////////////////////////
      string output_dir_parents = output_dir + "/parents_propagdrawn_" + id_config;
      if (!is_propag_cls)
         output_dir_parents = output_dir + "/parents_propagbestfit";
      string fit_file_parents = output_dir_parents + "/fit_result.out";
      string cov_file_parents = output_dir_parents + "/fit_covmatrix.out";
      printf("   - Initialise class for parents\n");
      printf("        => output dir (for parents): %s\n", output_dir_parents.c_str());
      run_parents->SetOutputDir(output_dir_parents);
      TUMisc::CreateDir(output_dir_parents, "usine_pbar", "");
      if ((!is_propag_cls && i_sample == 0) || is_propag_cls) {
         string tmp_file = output_dir_parents + "/drawn_pars.out";
         FILE *fp_pars_parents = fopen(tmp_file.c_str(), "w");
         loaded_pars.PrintPars(fp_pars_parents);
         fclose(fp_pars_parents);
         // Update parent parameters from transport parameters
         run_parents->GetFitPars()->UpdateParVals(loaded_pars, /*gENUM_FITPARTYPE*/kFIXED, /*is_enforce_allfound*/ false, /*is_check_range*/false, stdout);
         // SOLAR MODULATION PARAMETERS
         // Init file for pbar contains modulation level for B/C:
         //    phi_AMS02_201105201605_
         // Init file for parents contains modulation level for H, He, C, O
         //    phi_AMS02_201105201311_
         //    phi_AMS02_201105201605_
         // Given the similar periods, we assume that parents have the same phi as B/C,
         // and as phi_B/C changes for each drawing (correlations with other transport
         // parameters), we update their value at each drawing
         Bool_t is_test = false;
         if (is_test) {
            printf("################# TEST ##################\n");
            run_parents->GetFitPars()->PrintPars(stdout);
         }
         string name_phi_bc = "phi_AMS02_201105201605_";
         Int_t i_phi_bc = run->GetFitPars()->IndexPar(name_phi_bc);
         if (i_phi_bc < 0)
            TUMessages::Error(stdout, "usine_pbar", "", (string)Form("%s is not in list", name_phi_bc.c_str()));
         Double_t phi_bc = run->GetFitPars()->GetParEntry(i_phi_bc)->GetVal(run->GetFitPars()->GetParEntry(i_phi_bc)->GetFitSampling());
         if (is_propag_cls)
            fprintf(fp_phi_drawn_bc, "%6d %6d   %le\n", i_sample, i_sample + 1, phi_bc);
         //
         const Int_t n_phi_parents = 2;
         string name_phi_parents[n_phi_parents] = {"phi_AMS02_201105201311_", "phi_AMS02_201105201605_"};
         printf("   - Enforce same Solar modulation level (%.3f MV) for %s and:\n", phi_bc, name_phi_bc.c_str());
         for (Int_t ii = 0; ii < n_phi_parents; ++ii) {
            Int_t i_phi = run_parents->GetFitPars()->IndexPar(name_phi_parents[ii]);
            printf("      - %s\n", name_phi_parents[ii].c_str());
            if (i_phi < 0)
               TUMessages::Error(stdout, "usine_pbar", "", (string)Form("%s is not in list", name_phi_parents[ii].c_str()));
            run_parents->GetFitPars()->GetParEntry(i_phi)->SetVal(phi_bc, run_parents->GetFitPars()->GetParEntry(i_phi)->GetFitSampling());
         }
         if (is_test) {
            run->GetFitPars()->PrintPars(stdout);
            run_parents->GetFitPars()->PrintPars(stdout);
            printf("############### END TEST ################\n");
         }

         // Copy FIXED values into init (for print purpose only (not to have ambiguity on which values are used)
         for (Int_t i = 0; i < run_parents->GetFitPars()->GetNPars(); ++i) {
            if (run_parents->GetFitPars()->GetParEntry(i)->GetFitType() == kFIXED)
               run_parents->GetFitPars()->GetParEntry(i)->CopyValInFitInit();
         }
      }

      // If fit only once and fit-file already generated, only read it, otherwise, perform minimisation
      ifstream f_fit(fit_file_parents.c_str(), std::ifstream::in);
      if (!is_propag_cls && f_fit) {
         run_parents->SetIsPrintHessianMatrix(false);
         run_parents->SetIsPrintCovMatrix(false);
         run_parents->SetIsPrintBestFit(false);
         run_parents->SetIsCLMode(true);
      } else {
         run_parents->SetIsPrintHessianMatrix(true);
         run_parents->SetIsPrintCovMatrix(true);
         run_parents->SetIsPrintBestFit(true);
         run_parents->MinimiseTOAFluxes(init_pars_parents, is_verbose, true);
         TURunOutputs results;
         run_parents->FillSpectra(results, run_parents->GetFitData()->GetQtiesEtypesphiFF(), e_power);
         gROOT->SetBatch(true);
         run_parents->PlotSpectra(my_app, results, false/*is_sep_isot*/, false/*is_print*/, stdout/*fprint*/);
         gROOT->SetBatch(false);
      }
      vector<vector<Double_t>> drawn_vals_parents;
      TUFreeParList loaded_pars_parents;
      if (is_flux_err)
         DrawFromFitAndCovMat(drawn_vals_parents, loaded_pars_parents, fit_file_parents, cov_file_parents, n_samples, run_parents);
      else
         DrawFromFitAndCovMat(drawn_vals_parents, loaded_pars_parents, fit_file_parents);
      run_parents->GetFitPars()->UpdateParVals(loaded_pars_parents, /*gENUM_FITPARTYPE*/kANY, /*is_enforce_allfound*/ false, /*is_check_range*/false, stdout);


      // -> XS uncertainties
      if (is_xs_cls) {
         if (!is_propag_cls && !is_flux_err) {
            string message = Form("XS config. #%s/%d", id_config.c_str(), n_samples);
            TUMessages::Separator(stdout, message, 2);
         }
         // Update XS values from random file number among XS files provided
         Int_t index_xs = rand() % min((Int_t)xs_files.size(), n_samples);
         printf("   - Update XS from %s (out of %d files)\n", xs_files[index_xs].c_str(), (Int_t)xs_files.size());
         run->GetPropagModel(0)->TUXSections::LoadOrUpdateProd(xs_files[index_xs], false, stdout);
         // Force update for calculation (for new XS)
         run->GetPropagModel(0)->InitialiseForCalculation(/*is_init_or_update*/false, /*is_force_update*/true, /*is_verbose*/true, stdout);
         run->GetFitPars()->UpdateParVals(loaded_pars, /*gENUM_FITPARTYPE*/kANY, /*is_enforce_allfound*/ true, /*is_check_range*/false, stdout);
         run->GetFitPars()->UpdateParVals(loaded_pars_parents, /*gENUM_FITPARTYPE*/kANY, /*is_enforce_allfound*/ true, /*is_check_range*/false, stdout);
      }


      // -> Parents flux uncertainties
      if (is_flux_err) {
         for (Int_t i_fit = 0; i_fit < n_samples; ++i_fit) {
            string id_fit_config = Form("%0*d", n_digits, i_fit + 1);
            string message = Form("parents fit config. #%s/%d", id_fit_config.c_str(), n_samples);
            TUMessages::Separator(stdout, message, 2);
            //run->GetFitPars()->PrintPars(stdout);

            printf("      - Update source parameters from drawn values\n");
            // Update loaded pars from new generated parameters.
            for (Int_t i = 0; i < loaded_pars_parents.GetNPars(); ++i)
               loaded_pars_parents.GetParEntry(i)->SetVal(drawn_vals_parents[i_fit][i], loaded_pars_parents.GetParEntry(i)->GetFitSampling());
            loaded_pars_parents.PrintPars();
            run_parents->GetFitPars()->UpdateParVals(loaded_pars_parents, /*gENUM_FITPARTYPE*/kANY, /*is_enforce_allfound*/ true, /*is_check_range*/false, stdout);
            run->GetFitPars()->UpdateParVals(loaded_pars_parents, /*gENUM_FITPARTYPE*/kANY, /*is_enforce_allfound*/ true, /*is_check_range*/false, stdout);
            // Calculate and retrieve local spectra
            run->SetIndexSample(i_sample * n_samples + i_fit);
            Double_t chi2_real = run_parents->Chi2_TOAFluxes(&run_parents->GetFitPars()->ExtractParsValsForChi2()[0]);
            Double_t chi2_dummy = run->Chi2_TOAFluxes(&run->GetFitPars()->ExtractParsValsForChi2()[0]);
            printf("    => chi2/ndof = %le/%d = %le\n", chi2_real, run_parents->GetNdof(), chi2_real / run_parents->GetNdof());
            //run->MinimiseTOAFluxes(init_pars, false, false);
            run->FillSpectra(sample_results[i_sample * n_samples + i_fit], combos_etypes_phiff, e_power);
         }
         // If no propagation of errors for transport, we are done!
         if (!is_propag_cls)
            break;
      } else {
         // Update parameters of run from best-fit parameter values of run_parents
         run->GetFitPars()->UpdateParVals(loaded_pars_parents, /*gENUM_FITPARTYPE*/kANY, /*is_enforce_allfound*/ false, /*is_check_range*/false, stdout);
         // Calculate and retrieve local spectra
         run->SetIndexSample(i_sample);
         Double_t chi2 = run->Chi2_TOAFluxes(&run->GetFitPars()->ExtractParsValsForChi2()[0]);
         run->FillSpectra(sample_results[i_sample], combos_etypes_phiff, e_power);
      }
   }

   if (is_propag_cls)
      fclose(fp_phi_drawn_bc);


   // Extract CLs
   TURunOutputs results_cls;
   if (n_samples > 1) {
      results_cls.FillAsCLs(sample_results, is_median, is_1sigma, is_2sigma, is_3sigma);
      run->PlotSpectra(my_app, results_cls, false/*is_sep_isot*/);
   } else
      run->PlotSpectra(my_app, sample_results[0], false/*is_sep_isot*/);

   // Free memory
   if (my_app)
      delete my_app;
   if (init_pars)
      delete init_pars;
   if (run)
      delete run;

   fclose(f_log);
   f_log = NULL;

   return 1;
}

