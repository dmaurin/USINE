// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

// C/C++ include
#include <cerrno>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <glob.h>
#include <sys/stat.h>
//#include <cstdio>
#include <memory>
//#include <stdexcept>
#include <array>
using namespace std;
// ROOT include
// DAVID ROOT
#include <TAxis.h>
#include <TList.h>
#include <TStyle.h>
// USINE include
#include "../include/TUAtomElements.h"
#include "../include/TUEnum.h"
#include "../include/TUMath.h"
#include "../include/TUMessages.h"
#include "../include/TUMisc.h"

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TUMisc                                                               //
//                                                                      //
// Encapsulate queries related to CR combos sorting, formatting, etc.   //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

NamespaceImp(TUMisc)

//______________________________________________________________________________
vector<Double_t> TUMisc::ApplyBias(vector<Double_t> &x_orig, vector<Double_t> &y_orig, vector<Double_t> &x_bias, vector<Double_t> &y_bias)
{
   // Returns y_biased, i.e. y_orin(x_orin) biased by y_bias(x_bias) values.
   //  x_orig            [n_orig] Vector of original values (x-axis)
   //  y_orig            [n_orig] Vector of original values (y-axis)
   //  x_bias            [n_bias] Vector of biases (x-axis)
   //  x_bias            [n_bias] Vector of biases (y-axis)

   Int_t n_orig = x_orig.size();
   Int_t n_bias = x_bias.size();

   vector<Double_t> y_biased(n_orig, 0.);

   // Loop on x_orig and bias
   for (Int_t g = 0; g < n_orig; ++g) {
      if (x_orig[g] < x_bias[0] + 1.e-10)
         y_biased[g] = y_orig[g] * y_bias[0];
      else if (x_orig[g] > x_bias[n_bias - 1] - 1.e-10)
         y_biased[g] = y_orig[g] * y_bias[n_bias - 1];
      else {
         Int_t k = TUMath::BinarySearch(n_bias, &x_bias[0], x_orig[g]);
         y_biased[g] = TUMath::Interpolate(x_orig[g], x_bias[k], x_bias[k + 1], y_bias[k], y_bias[k + 1], kLOGLIN);
      }
   }
   return y_biased;
}

//______________________________________________________________________________
vector<Double_t> TUMisc::Array2Vector(Double_t const *ptr, Int_t n)
{
   //--- Simple method to return vector from array (pointer).
   //  ptr               [n] Array of values to put in vector
   //  n                 Number of elements in array

   vector<Double_t> vec(n, 0.);
   for (Int_t i = 0; i < n; ++i)
      vec[i] = ptr[i];

   return vec;
}

//______________________________________________________________________________
string TUMisc::CreateDir(string const &dir, string const &class_name, string const &method)
{
   //--- Create dir if not exists.
   //  dir               Directory to check if exists/create
   //  class_name        Class from which this function is called
   //  method            Method from which this function is called

   // If empty, local directory
   if (dir == "")
      return dir;

   string dir_full = TUMisc::GetPath(dir);

   // Create the directory if not existing
   struct stat st; // temporary variable to check that directory exists
   if (!((stat(dir_full.c_str(), &st) == 0) && S_ISDIR(st.st_mode))) {
      Int_t err = mkdir(dir_full.c_str(), 0755);
      if (err != 0)
         TUMessages::Error(stdout, class_name, method,
                           Form("ERROR[mkdir %s]: %s\n", dir_full.c_str(), strerror(errno)));
   }
   return dir_full;
}

//______________________________________________________________________________
vector<string> TUMisc::ExtractFromDir(string const &pattern, Bool_t is_verbose, FILE *f,
                                      Bool_t is_selection, string const &exclude_pattern,
                                      Bool_t is_test)
{
   //--- Extracts and returns vector of files (following pattern) found/selected in directory.
   //  pattern           Files to look for
   //  is_verbose        Print or not found files on screen
   //  f                 Output for chatter
   //  is_selection      If true, Text-User-Interface to select among files found
   //  exclude_pattern   Exclude files containing this pattern
   //  is_test           Whether run in test mode

   string pattern_abs = GetPath(pattern);
   string usine = GetPath("$USINE");

   // Get glob list
   glob_t glob_result;
   //glob(pattern.c_str(), GLOB_TILDE | GLOB_MARK, NULL, &glob_result);
   glob(pattern_abs.c_str(), GLOB_TILDE, NULL, &glob_result);

   // Push in vector
   vector<string> files;
   for (UInt_t i = 0; i < glob_result.gl_pathc; ++i) {
      string tmp = glob_result.gl_pathv[i];
      //cout << i << " " << tmp << "   **" << exclude_pattern << "**" << endl;
      if (exclude_pattern != "" && tmp.find(exclude_pattern) != string::npos)
         continue;
      else
         files.push_back(tmp);
   }

   // Free glob
   globfree(&glob_result);
   if (is_verbose || is_selection) {
      string indent = TUMessages::Indent(true);
      if (is_test)
         fprintf(f, "%s   %d files found for %s:\n", indent.c_str(), (Int_t)files.size(), pattern.c_str());
      else
         fprintf(stdout, "%s   %d files found for %s:\n", indent.c_str(), (Int_t)files.size(), pattern.c_str());
      for (UInt_t i = 0; i < files.size(); ++i) {
         string tmp = files[i];
         std::size_t pos = tmp.find(usine);
         if (pos != string::npos)
            tmp.replace(pos, usine.length(), "$USINE");
         if (is_test)
            fprintf(f, "%s      %2d %s\n", indent.c_str(), i + 1, tmp.c_str());
         else {
            if (files.size() < 20 || i < 5 || i > files.size() - 6)
               fprintf(stdout, "%s      %2d %s\n", indent.c_str(), i + 1, tmp.c_str());
            else if (i == 6)
               fprintf(stdout, "%s      ...\n", indent.c_str());
         }
      }
      TUMessages::Indent(false);


      // To select a subset
      if (is_selection) {
         do {
            string selection;
            if (!is_test) {
               cout << indent
                    << "   ### Select list to use (comma-separated indices or A to use all):"
                    << endl;
               cout << indent << "    >>  ";
               cin >> selection;
            } else
               selection = "1,3";
            UpperCase(selection);


            // If all
            if (selection == "A")
               return files;
            else if (selection.find_first_not_of(",0123456789") != string::npos)
               continue;

            // If selection
            vector<Int_t> list_indices;
            String2List(selection, ",", list_indices);
            RemoveDuplicates(list_indices);
            vector<string> files_selected;
            for (Int_t i = 0; i < (Int_t)list_indices.size(); ++i) {
               Int_t i_file = list_indices[i] - 1;
               if (i_file >= 0 && i_file < (Int_t)files.size())
                  files_selected.push_back(files[i_file]);
            }
            return files_selected;
         } while (1);
      }
   }

   // Return
   return files;
}

//______________________________________________________________________________
Int_t TUMisc::IndexInList(vector<Int_t> const &list, Int_t val_to_find)
{
   //--- Returns index of 'val_to_find' in 'list' (-1 if not found).
   //  list              List of indices
   //  val_to_find       Value for which to find index

   for (Int_t k = 0; k < (Int_t)list.size(); ++k) {
      if (list[k] == val_to_find)
         return k;
   }
   return -1;
}

//______________________________________________________________________________
Int_t TUMisc::IndexInList(vector<string> const &list, string const &nametofind, Bool_t is_comp_uppercase)
{
   //--- Returns index of 'nametofind' is 'list' (-1 if not found).
   //    N.B.: comparison is between upper-cased names if 'is_check_uppercase'=true.
   //  list              List of names
   //  nametofind        Name to search for in list
   //  is_comp_uppercase Whether comparison is made on upper-cases names

   string nametofind_uc = nametofind;
   if (is_comp_uppercase)
      UpperCase(nametofind_uc);

   for (Int_t k = 0; k < (Int_t)list.size(); ++k) {
      string list_uc = list[k];
      if (is_comp_uppercase)
         UpperCase(list_uc);

      if (nametofind_uc == list_uc)
         return k;
   }
   return -1;
}

//______________________________________________________________________________
string TUMisc::FormatCRQty(string const &qty, Int_t switch_format)
{
   //--- Returns data qty according to desired format.
   //  qty               Original format for the data (i.e. upper cases and full name)
   //  switch_format     0=usine-name, 1=text-formatted, 2=rootplot_formatted

   string tmp;
   string qty_uc = qty;
   TUMisc::UpperCase(qty_uc);
   if (qty_uc == "POSITRON") {
      if (switch_format == 2) tmp = "e^{+}";
      else tmp = "e+";
   } else if (qty_uc == "ELECTRON") {
      if (switch_format == 2) tmp = "e^{-}";
      else tmp = "e-";
   } else if (qty_uc == "<LNA>") {
      if (switch_format == 2) tmp = "<ln(A)>";
      else tmp = "<lnA>";
   } else if (qty_uc == "ALLSPECTRUM") {
      if (switch_format == 2) tmp = "All-Spectrum";
      else tmp = "AllSpectrum";
   } else if (qty_uc == "ELECTRON+POSITRON") {
      if (switch_format == 2) tmp = "e^{+}+e^{-}";
      else tmp = "(e-+e+)";
   } else if (qty_uc == "POSITRON/ELECTRON+POSITRON") {
      if (switch_format == 2) tmp = "e^{+}/(e^{-}+e^{+})";
      else tmp = "e+/(e-+e+)";
   } else if (qty_uc == "SUBFE/FE") {
      tmp = "SubFe/Fe";
   } else if (qty_uc == "1H-BAR") {
      tmp = "#bar{p}";
   } else if (qty_uc == "2H-BAR") {
      tmp = "#bar{d}";
   } else if (qty_uc == "1H-BAR/1H") {
      tmp = "#bar{p}/p";
   } else if (qty_uc == "1H-BAR/H-BAR") {
      tmp = "#bar{p}/#bar{H}";
   } else if (qty_uc.find("-BAR") != string::npos) {
      tmp = qty_uc.substr(0, qty_uc.size() - 4);
      tmp = "#bar{" + tmp + "}";
   } else {
      vector<string> numden;
      TUMisc::String2List(qty_uc, "/", numden);
      for (Int_t i = 0; i < (Int_t)numden.size(); ++i) {
         // For isotope or element, the second letter is a lower case
         size_t pos = numden[i].find_first_not_of("0123456789");
         // Lower case for second character
         if (numden[i].size() - pos > 0)
            TUMisc::LowerCase(numden[i][pos + 1]);

         // If isotope, use exponent
         size_t pos2 = numden[i].find_first_of("0123456789");
         if (pos2 != string::npos && switch_format == 2)
            numden[i] = "^{" + numden[i].substr(0, pos) + "}"
                        + numden[i].substr(pos);
      }
      tmp = numden[0];
      if (numden.size() == 2)
         tmp = tmp + "/" + numden[1];
   }
   return tmp;
}
//______________________________________________________________________________
void TUMisc::FormatCRQtyName(string &qty)
{
   //--- Convert qty in suitable name.
   //  qty               Original name

   UpperCase(qty);

   // Alternative names for qty
   if (qty == "E+")
      qty = "POSITRON";
   else if (qty == "E-")
      qty = "ELECTRON";
   else if (qty == "E-+E-")
      qty = "ELECTRON+POSITRON";
   else if (qty == "E+/E-+E+" || qty == "E+/(E-+E+)")
      qty = "POSITRON/ELECTRON+POSITRON";
   else if (qty == "PBAR")
      qty = "1H-BAR";
   else if (qty == "PBAR/P")
      qty = "1H-BAR/1H";
   else if (qty == "DBAR")
      qty = "2H-BAR";
   else if (qty == "DBAR/PBAR")
      qty = "2H-BAR/1H-BAR";
   else if (qty == "DBAR/P")
      qty = "2H-BAR/1H";
}

//______________________________________________________________________________
string TUMisc::FormatEPower(Double_t const &e_power)
{
   //--- Returns string (for display and file names) when spectra multiplied by some power of 'e_power'.
//   //  e_power               Index of E^e_power

   // If no power, nothing to do
   if (fabs(e_power) < 1.e-3)
      return "";

   string name = "";
   if (e_power > 1.e-3)
      name = Form("%.3f", e_power);
   else if (e_power < -1.e-3)
      name = Form("m%.3f", -e_power);

   return name;
}


//______________________________________________________________________________
string TUMisc::GetDatime()
{
   //--- Returns time as a string, formatted as follows: %d-%m-%Y %I:%M:%S.

   time_t today;
   time(&today);
   struct tm *timeinfo = localtime(&today);

   char buffer[80];
   strftime(buffer, 80, "%d-%m-%Y %I:%M:%S", timeinfo);
   return string(buffer);
}

//______________________________________________________________________________
string TUMisc::GetPath(string const &str)
{
   //--- Extracts and returns the absolute path from an environment variable '$env',
   //    or from a string formatted as '$env/relative_path'. If no $ (no path), returns
   //    str as it is.
   //  str               Name from which to extract path

   if (str.find_first_of("$") == string::npos)
      return str;

   // Finds how 'str' is formatted (if contains '/')
   size_t last = str.find_first_of("/");
   string path = "";
   if (last == string::npos)
      last = str.size();
   else {
      path = str.substr(last + 1, str.size());
      last -= 1;
   }
   string env_path = str.substr(str.find_first_of("$") + 1, last);
   if ((int)env_path.size() != 0) {
      char *env = getenv(env_path.c_str());
      if (env == NULL) {
         string message = "The environment variable $" + env_path + " is not defined!";
         TUMessages::Warning(stdout, "TUMisc", "GetPath", message);
      } else {
         if (path != "")
            path = (string) env + "/" + path;
         else
            path = (string) env;
      }
   }

   return path;
}

//______________________________________________________________________________
Bool_t TUMisc::IsFileExist(ifstream &f, string const &f_name, Bool_t is_abort)
{
   //--- Checks whether ifstream file exists: if not, reports defective file and aborts.
   //  f                 Ifstream address to check
   //  f_name            File name to checked
   //  is_abort          Abort on or off

   if (!f) {
      if (!is_abort)
         return false;
      printf("----------------- ERROR -----------------\n");
      printf("COULD NOT OPEN FILE %s\n", f_name.c_str());
      printf("----------------- ABORT -----------------\n");
      abort();
   }
   return true;
}

//______________________________________________________________________________
Bool_t TUMisc::IsYaxisLog(string const &combo)
{
   //--- Checks whether y-axis of a combo is in log-scale (e.g., spectrum and
   //    some other specific case) or log-scale (for most ratios).
   //  combo             Name to check

   // Exceptions
   string qty = combo;
   UpperCase(qty);
   if (qty == "1H-BAR/1H" || qty == "2H-BAR/1H" || qty == "1H-BAR/H"
         || qty == "2H-BAR/H" || qty == "E+/LEPTONS")
      return true;

   // If ratio
   if ((qty.find("/", 0) != string::npos) || (qty == "<LNA>"))
      return false;
   // otherwise
   else
      return true;
}

//______________________________________________________________________________
string TUMisc::List2String(vector<Double_t> const &list, char delimiter, Int_t n_digits)
{
   //--- Returns a concatenated string separated by delimiters from a list of double.
   //  list              Vector of double to concatenate
   //  delimiters        Separator searched for in str
   //  n_digits          Number of digits to use

   vector<string> list_string;
   for (Int_t i = 0; i < (Int_t)list.size(); ++i)
      list_string.push_back((string)Form("%.*f", abs(n_digits), list[i]));
   return List2String(list_string, delimiter);
}

//______________________________________________________________________________
string TUMisc::List2String(vector<Int_t> const &list, char delimiter)
{
   //--- Returns a concatenated string separated by delimiters from a list of int.
   //  list              Vector of int to concatenate
   //  delimiters        Separator searched for in str

   vector<string> list_string;
   for (Int_t i = 0; i < (Int_t)list.size(); ++i)
      list_string.push_back((string)Form("%d", list[i]));

   return List2String(list_string, delimiter);
}

//______________________________________________________________________________
string TUMisc::List2String(vector<string> const &list, char delimiter)
{
   //--- Returns a concatenated string separated by delimiters from a list of strings.
   //  list              Vector of string to concatenate
   //  delimiters        Separator searched for in str

   Int_t n_list = list.size();
   if (n_list == 0)
      return "";

   string concat_list = "";
   for (Int_t i = 0; i < n_list - 1; ++i)
      concat_list = concat_list + list[i] + delimiter;

   concat_list = concat_list + list[n_list - 1];
   return concat_list;
}

//______________________________________________________________________________
void TUMisc::OptimiseRange(Double_t *array, Double_t const &threshold, Int_t n,
                           Int_t &k_min, Int_t &k_max, Bool_t &is_force_adapt)
{
   //--- Searches the smallest range on which array is above threshold. If
   //    'is_force_adapt' is true, we force to adapt range, otherwise this
   //    variables tells us whether or not the range found contains a 'power
   //    of two' number of bins.
   // INPUTS:
   //  array           Vector of values
   //  threshold       Quantity to which array values are compared to
   //  k_min           Initial start index
   //  k_max           Initial stop index
   //  n               Size of array
   //  k_min           Initial start index
   //  k_max           Initial stop index
   //  is_force_adapt If true, force range found such as (k_max-k_min)=2^j
   // OUTPUTS:
   //  k_min           Index min for values above threshold
   //  k_max           Index max for values above threshold

   // Adapt range (values above threshold)
   Int_t k_ref_min = k_min;
   Int_t k_ref_max = k_max;
   for (Int_t k = k_ref_min; k < n - 2; ++k) {
      if (array[k] < threshold)
         k_min = k + 1;
      else
         break;
   }
   for (Int_t k = k_ref_max; k > k_min + 1; --k) {
      if (array[k] < threshold)
         k_max = k - 1;
      else
         break;
   }

   // N.B.: for adaptive integrations, the number of steps must be 2^jmax+1
   // (for the doubling step to work)
   Int_t nk = k_max - k_min + 1;
   Int_t jmax = (Int_t)floor(log(Double_t(nk - 1)) / log(2.));
   // N.B.: we take jmax+1 here because we slightly extend the
   // integration range w.r.t. the 'above-threshold' range
   Int_t nkok = (Int_t)pow(2., jmax + 1) + 1;

   // If only test whether the range is 'power of two'
   if (!is_force_adapt) {
      if (nk == nkok)
         is_force_adapt = true;
      return;
   } else {
      // If not a power of 2, try to adapt
      if (nk != nkok) {
         Int_t n_more = nkok - nk;
         if (k_max + n_more < n)
            k_max += n_more;
         else if (k_min - n_more >= 0)
            k_min -= n_more;
      }
   }
}

//______________________________________________________________________________
void TUMisc::OptimiseRange(Double_t *array, Double_t *arraybis, Double_t const &threshold,
                           Int_t n, Int_t &k_min, Int_t &k_max, Bool_t &is_force_adapt)
{
   //--- Searches the smallest range on which array is above threshold. If
   //    'is_force_adapt' is true, we force to adapt range, otherwise this
   //    variables tells us whether or not the range found contains a 'power of two'
   //    number of bins.
   // INPUTS:
   //  array           Vector of values
   //  arraybis        Vector of values
   //  threshold       Quantity to which array values are compared to
   //  n               Size of array
   //  k_min           Initial start index
   //  k_max           Initial stop index
   //  is_force_adapt If true, force range found such as (k_max-k_min)=2^j
   // OUTPUTS:
   //  k_min           Index min for values above threshold
   //  k_max           Index max for values above threshold

   // Adapt range (values above threshold)
   Int_t k_ref_min = k_min;
   Int_t k_ref_max = k_max;
   for (Int_t k = k_ref_min; k < n - 2; ++k) {
      if (array[k]*arraybis[k] < threshold)
         k_min = k + 1;
      else
         break;
   }
   for (Int_t k = k_ref_max; k > k_min + 1; --k) {
      if (array[k]*arraybis[k] < threshold) {
         k_max = k - 1;
      } else
         break;
   }


   // N.B.: for adaptive integrations, the number of steps must be 2^jmax+1
   // (for the doubling step to work)
   Int_t nk = k_max - k_min + 1;
   Int_t jmax = (Int_t)floor(log(Double_t(nk - 1)) / log(2.));
   // N.B.: we take jmax+1 here because we slightly extend the
   // integration range w.r.t. the 'above-threshold' range
   Int_t nkok = (Int_t)pow(2., jmax + 1) + 1;

   // If only test whether the range is 'power of two'
   if (!is_force_adapt) {
      if (nk == nkok)
         is_force_adapt = true;
      return;
   } else {
      // If not a power of 2, try to adapt
      if (nk != nkok) {
         Int_t n_more = nkok - nk;
         if (k_max + n_more < n)
            k_max += n_more;
         else if (k_min - n_more >= 0)
            k_min -= n_more;
      }
   }
}

//______________________________________________________________________________
void TUMisc::Print(FILE *f, TGraph *gr, Int_t opt_0xy_1err_2asymerr)
{
   //--- Print content of graph in file f.
   //  f                 Output file for test
   //  gr                Graph to print
   //  is_axis           Whether to print or not axis names
   //  opt_0xy_1err_2asymerr Whether to print (x,y) w/wo error or asymm. errors

   if (!gr)
      return;

   fprintf(f, "#  [graph_name] %s\n", gr->GetName());
   fprintf(f, "#  [graph_title] %s\n", gr->GetTitle());
   fprintf(f, "#   N=%d points\n", gr->GetN());
   if (gr->GetN() < 0)
      return;
   if (gr->GetXaxis() && gr->GetYaxis())
      fprintf(f, "# %-12s %-12s", gr->GetXaxis()->GetTitle(), gr->GetYaxis()->GetTitle());
   if (opt_0xy_1err_2asymerr == 1)
      fprintf(f, "  (x_err)      (y_err)");
   else if (opt_0xy_1err_2asymerr == 2)
      fprintf(f, "  (x_lo)       (x_hi)         (y_lo)       (y_hi)");
   fprintf(f, "\n");

   for (Int_t i = 0; i < gr->GetN(); ++i) {
      Double_t x, y;
      gr->GetPoint(i, x, y);
      fprintf(f, "%le  %le", x, y);
      if (opt_0xy_1err_2asymerr == 1)
         fprintf(f, "   %le %le", gr->GetErrorYlow(i), gr->GetErrorYhigh(i));
      else if (opt_0xy_1err_2asymerr == 2)
         fprintf(f, "   %le %le   %le %le",
                 gr->GetErrorXlow(i), gr->GetErrorXhigh(i),
                 gr->GetErrorYlow(i), gr->GetErrorYhigh(i));
      fprintf(f, "\n");
   }
   fprintf(f, "\n");
}

//______________________________________________________________________________
void TUMisc::Print(FILE *f, TMultiGraph *mg, Int_t opt_0xy_1err_2asymerr)
{
   //--- Print content of graph in file f.
   //  f                 Output file for test
   //  gr                Graph to print
   //  opt_0xy_1err_2asymerr Whether to print (x,y) w/wo error or asymm. errors

   fprintf(f, "#  [multigraph_name] %s\n", mg->GetName());
   fprintf(f, "#  [multigraph_title]%s\n", mg->GetTitle());

   if (!mg->GetListOfGraphs() || mg->GetListOfGraphs()->GetSize() == 0)
      return;

   Int_t n_gr = mg->GetListOfGraphs()->GetSize();
   fprintf(f, "#  [# of graph] %d\n", n_gr);
   fprintf(f, "\n");
   if (n_gr > 0) {
      TIter next(mg->GetListOfGraphs());
      Int_t i = 0;
      TGraph *gr = NULL;
      while ((gr = (TGraph *)next())) {
         ++i;
         fprintf(f, "#  [graph %d/%d in multigraph]\n", i, n_gr);
         if (gr)
            Print(f, gr, opt_0xy_1err_2asymerr);
         gr = NULL;
      }
   }
   fprintf(f, "\n");
}

//______________________________________________________________________________
void TUMisc::PrintUSINEHeader(FILE *f, Bool_t is_print_datime)
{
   //--- Print header information (USINE version and date/time.
   //  f                 Output file for test
   //  is_print_datime   Whether to print data and time

   fprintf(f, "# %s\n", gUSINE_VERSION.c_str());
   if (is_print_datime)
      fprintf(f, "# File generated %s\n", GetDatime().c_str());
}

//______________________________________________________________________________
TText *TUMisc::OrphanUSINEAdOnPlots()
{
   //--- Returns ROOT TText of USINE link (to show on plots).

   string text = gUSINE_VERSION + " (https://lpsc.in2p3.fr/usine)";
   TText *usine_txt = new TText(0.12, 0.965, text.c_str());
   usine_txt->SetNDC(kTRUE);
   usine_txt->SetTextFont(92);
   usine_txt->SetTextAlign(11);
   usine_txt->SetTextSize(0.035);
   return usine_txt;
}

//______________________________________________________________________________
string TUMisc::RemoveBlancksFromStartStop(string const &str)
{
   //--- Removes blanks (or tabs) at the beginning and ending of a string.
   //  str               String to trim

   // Trim tabs
   if (str.size() == 0) return str;

   // Trim blanck spaces and tabs
   std::string::size_type begin = str.find_first_not_of("\t ");
   std::string::size_type end   = str.find_last_not_of("\t ");

   // And this is not the end, because from other platforms (Windows, Mac)
   // funny '\r' can happen at the end of the lines...
   string tmp = str.substr(begin, end - begin + 1);
   if (tmp.size() && tmp[tmp.size() - 1] == '\r')
      return (tmp.substr(0, tmp.size() - 1));
   else
      return tmp;
}

//______________________________________________________________________________
string TUMisc::RemoveChars(string const &str, string const &delimiters)
{
   //--- Drops chars in a string.
   //  str               String to analyse
   //  delimiters        Characters to drop in str

   string tmp = str;
   size_t found = tmp.find_first_of(delimiters);
   while (found != string::npos) {
      tmp.erase(found, 1);
      found = tmp.find_first_of(delimiters, found);
   };
   return tmp;
}

//______________________________________________________________________________
void TUMisc::RemoveDuplicates(vector<Int_t> &list)
{
   //--- Removes duplicate strings in list.
   // INPUT/OUTPUT
   //  list              String to inspect

   for (Int_t i = 0; i < (Int_t)list.size(); ++i) {
      for (Int_t j = list.size() - 1; j >= i + 1; --j) {
         if (list[i] == list[j])
            list.erase(list.begin() + j, list.begin() + j + 1);
      }
   }
}

//______________________________________________________________________________
void TUMisc::RemoveDuplicates(vector<string> &list)
{
   //--- Removes duplicate strings in list.
   //  list              String to inspect

   for (Int_t i = 0; i < (Int_t)list.size(); ++i) {
      for (Int_t j = list.size() - 1; j >= i + 1; --j) {
         if (list[i] == list[j])
            list.erase(list.begin() + j, list.begin() + j + 1);
      }
   }
}

//______________________________________________________________________________
void TUMisc::RemoveDuplicates(vector<Double_t> &list, Double_t const &thresh)
{
   //--- Removes duplicate (within precision double in list.
   //  list              String to inspect
   //  thresh            Threshold below which considered same value

   for (Int_t i = 0; i < (Int_t)list.size(); ++i) {
      for (Int_t j = list.size() - 1; j >= i + 1; --j) {
         if ((list[i] < thresh && list[j] < thresh)
               || fabs((list[i] - list[j]) / list[i]) < thresh)
            list.erase(list.begin() + j, list.begin() + j + 1);
      }
   }
}

//______________________________________________________________________________
void TUMisc::RemoveSpecialChars(string &s)
{
   //--- Discards special character "{}*~#$ -:?!,;/+=[]()&|><." and replaces
   //    '(', ')', and '.' by '_' in s, in order to have ROOT-compliant object names.
   //  s                 String to trim
   //  is_remove_dot     Whether to also remove '.' or not

   // Trim string
   //string trim = "{}*~#$ -:?!,;/+=[].&|><";
   string trim = "{}*~#$ -:?!,;/+=[]&|><";
   s = TUMisc::RemoveChars(s, trim);

   // Replace with '_'
   for (Int_t i = 0; i < (Int_t)s.size(); ++i) {
      if (s[i] == '(' || s[i] == ')' || s[i] == '.')
         s[i] = '_';
   }
}

//______________________________________________________________________________
Int_t TUMisc::RootColor(Int_t i)
{
   //--- Converts index into better ROOT colour index.
   //  i                 Index to convert

   if (i > 13) i = (i % 14);
   switch (i) {
      case 0 :
         return kBlack;
      case 1 :
         return kRed;
      case 2 :
         return kBlue;
      case 3 :
         return kGreen + 1;
      case 4 :
         return kOrange + 1;
      case 5 :
         return kViolet;
      case 6 :
         return kMagenta - 7;
      case 7 :
         return kGray + 1;
      case 8 :
         return kRed + 2;
      case 9 :
         return kCyan + 1;
      case 10 :
         return kSpring;
      case 11 :
         return kPink - 4;
      case 12 :
         return kAzure + 4;
      case 13 :
         return kYellow + 1;
      default :
         return i;
   }
}

//______________________________________________________________________________
Int_t TUMisc::RootFill(Int_t i)
{
   //--- Converts index into ROOT fill style index.
   //  i                 Index to convert

   if (i > 6) i = (i % 7);
   switch (i) {
      case 0 :
         return 3004;
      case 1 :
         return 3005;
      case 2 :
         return 3006;
      case 3 :
         return 3007;
      case 4 :
         return 3003;
      case 5 :
         return 3002;
      case 6 :
         return 3001;
      default:
         return 3009;
   }
}

//______________________________________________________________________________
Int_t TUMisc::RootMarker(Int_t i)
{
   //--- Converts index into better ROOT marker index.
   //  i                 Index to convert

   if (i > 15) i = (i % 16) + 1;
   switch (i) {
      case 0 :
         return 5;
      case 1 :
         return 2;
      case 2 :
         return 24;
      case 3 :
         return 25;
      case 4 :
         return 26;
      case 5 :
         return 32;
      case 6 :
         return 30;
      case 7 :
         return 28;
      case 8 :
         return 27;
      case 9  :
         return 20;
      case 10 :
         return 21;
      case 11 :
         return 22;
      case 12 :
         return 23;
      case 13 :
         return 29;
      case 14 :
         return 33;
      case 15 :
         return 34;
      default :
         return 1;
   }
}

//______________________________________________________________________________
void TUMisc::RootSave(TCanvas *c, string const &dir, string const &file_name, string const &opt)
{
   //--- Save canvas content.
   //  c                 Root canvas whose content is to be saved
   //  dir               Directory where to save file
   //  file_name         File name to save
   //  opt               Type of files saved
   //                      C => .C
   //                      E => .eps
   //                      G => .gif
   //                      J => .jpeg NOTE: JPEG's lossy compression will make all sharp edges fuzzy.
   //                      N => .png
   //                      P => .pdf
   //                      R => .root
   //                      S => .svg
   //                      T => .tiff
   //                      X => .xpm

   // if no canvas, nothing to do, return
   if (!c)
      return;

   string f_name = file_name;
   RemoveSpecialChars(f_name);
   string name = dir + "/" + f_name;
   c->cd();
   c->SetSelected(c);

   if (opt.find("C") != string::npos)
      gPad->SaveAs((name + ".C").c_str());
   if (opt.find("E") != string::npos)
      gPad->SaveAs((name + ".eps").c_str());
   if (opt.find("G") != string::npos)
      gPad->SaveAs((name + ".gif").c_str());
   if (opt.find("J") != string::npos)
      gPad->SaveAs((name + ".jpeg").c_str());
   if (opt.find("N") != string::npos)
      gPad->SaveAs((name + ".png").c_str());
   if (opt.find("P") != string::npos)
      gPad->SaveAs((name + ".pdf").c_str());
   if (opt.find("R") != string::npos)
      gPad->SaveAs((name + ".root").c_str());
   if (opt.find("S") != string::npos)
      gPad->SaveAs((name + ".svg").c_str());
   if (opt.find("T") != string::npos)
      gPad->SaveAs((name + ".tiff").c_str());
   if (opt.find("X") != string::npos)
      gPad->SaveAs((name + ".xpm").c_str());
}

//______________________________________________________________________________
void TUMisc::RootSetStylePlots()
{
   //--- Sets NM style for plots

   TStyle *style = new  TStyle("style", "style");

   //style->SetFillColor(0);  // => If uncomment, leads to issues with histograms!
   style->SetFillStyle(0);

   // Canvas style
   style->SetCanvasBorderMode(0);
   style->SetCanvasBorderSize(0);
   style->SetCanvasColor(0);
   style->SetCanvasDefH(350);
   style->SetCanvasDefW(500);
   style->SetCanvasDefX(10);
   style->SetCanvasDefY(10);

   // Frame style (no colour framed around plots)
   style->SetFrameBorderMode(0);
   style->SetFrameBorderSize(0);
   style->SetFrameFillColor(0);
   style->SetFrameFillStyle(0);
   style->SetFrameLineColor(0);
   style->SetFrameLineStyle(0);
   style->SetFrameLineWidth(1);

   // Pad style
   style->SetPadBorderMode(0);
   style->SetPadBorderSize(0);
   style->SetPadColor(0);
   style->SetPadGridX(0);
   style->SetPadGridY(0);
   style->SetPadTickX(1);
   style->SetPadTickY(1);
   style->SetPadTopMargin(0.05);
   style->SetPadRightMargin(0.02);
   style->SetPadLeftMargin(0.12);
   style->SetPadBottomMargin(0.13);

   //use the primary colour palette
   style->SetPalette(1, 0);
   //set stat off
   style->SetOptLogx(1);
   style->SetOptLogy(1);
   style->SetOptLogz(1);
   style->SetOptStat(0);
   style->SetOptFit(0);

   //set histogram default
   style->SetHistFillColor(0);
   style->SetHistFillStyle(0);
   style->SetHistLineColor(kBlack);
   style->SetHistLineStyle(1);
   style->SetHistLineWidth(2);
   //set function default
   style->SetFuncColor(kRed);
   style->SetFuncStyle(1);
   style->SetFuncWidth(4);

   //set label, legend, and title default
   style->SetLabelColor(kBlack, "XYZ");
   style->SetLabelFont(132, "XYZ");
   style->SetLabelOffset(0.002, "X");
   style->SetLabelOffset(0.002, "Y");
   style->SetLabelSize(0.055, "XYZ");
   style->SetLegendBorderSize(0);
   style->SetLegendFillColor(0);
   style->SetLegendFont(132);
   style->SetTitleAlign(21);
   style->SetTitleBorderSize(0);
   style->SetTitleColor(kBlack, "XYZ");
   style->SetTitleFillColor(0);
   style->SetTitleFont(132, "XYZ");
   style->SetTitleSize(0.055, "XYZ");
   style->SetTitleFontSize(0.06);
   style->SetTitleOffset(1.1, "X");
   style->SetTitleOffset(1.0, "Y");
   style->SetTitleOffset(1, "Z");
   style->SetTitleStyle(0);
   style->SetTitleTextColor(kBlack);

   //set the number of divisions to show
   style->SetNdivisions(512, "xy");

   style->cd();
   gStyle = style;
   style = NULL;
}

//______________________________________________________________________________
Bool_t TUMisc::String2Bool(string const &str)
{
   //--- Convert string to bool (abort if not a boolean).
   //  str               String to analyse

   string uc = str;
   UpperCase(uc);
   if (uc == "1" || uc == "TRUE")
      return true;
   else if (uc == "0" || uc == "FALSE")
      return false;
   else {
      string message = str + " is not a boolean, use 0, 1, true, or false";
      TUMessages::Error(stdout, "TUMisc", "String2Bool", message);
   }
   return false;
}

//______________________________________________________________________________
void TUMisc::String2List(string const &str, string const &delimiters,
                         vector<string> &list, Int_t parse_n_first)
{
   //--- Seeks and extracts the number of Elements in a list separated by any
   //    separator. This extraction checks possible mistakes in the input, e.g.
   //    if separator=',', it is able to extract correctly from str=",,ddf,dd,,ee,"
   //    the list={"ddf","dd","ee"}.
   // INPUT:
   //  str               String to analyse
   //  delimiters        Separator searched for
   //  parse_n_first     Parse only n first strings found (if <0 parse all)
   // OUTPUT:
   //  list              Vector of string found in str

   // Skip delimiters at beginning.
   string::size_type last_pos = str.find_first_not_of(delimiters, 0);
   // Find first "non-delimiter".
   string::size_type pos     = str.find_first_of(delimiters, last_pos);
   Int_t n_found = 0;
   while (string::npos != pos || string::npos != last_pos) {
      ++n_found;
      // Found a token, add it to the vector.
      string sub = str.substr(last_pos, pos - last_pos);
      string entry = RemoveBlancksFromStartStop(sub);
      if (entry == "" || entry.find_first_not_of(" ") == string::npos)
         list.push_back("");
      else
         list.push_back(entry);

      // Skip delimiters.
      last_pos = str.find_first_not_of(delimiters, pos);
      // Find next "non-delimiter"
      pos = str.find_first_of(delimiters, last_pos);

      // If already extract the parse_n_first first strings, stop here!
      if (n_found == parse_n_first && parse_n_first > 0)
         break;
   }
}

//______________________________________________________________________________
void TUMisc::String2List(string const &str, string const &delimiters,
                         vector<Double_t> &list)
{
   //--- Extracts a list of double from a string.
   // INPUTS:
   //  str               String to analyse
   //  delimiters        Separator searched for
   // OUTPUT:
   //  list              Vector of double found in str

   vector<string> tokens;
   String2List(str, delimiters, tokens);

   // Convert in Double_t
   for (Int_t i = 0; i < (int)tokens.size(); ++i)
      list.push_back(atof(tokens[i].c_str()));
}

//______________________________________________________________________________
void TUMisc::String2List(string const &str, string const &delimiters,
                         vector<Int_t> &list)
{
   //--- Extracts a list of int from a string.
   // INPUTS:
   //  str               String to analyse
   //  delimiters        Separator searched for
   // OUTPUT:
   //  list              Vector of int found in str

   vector<string> tokens;
   String2List(str, delimiters, tokens);

   // Convert in Int_t
   for (Int_t i = 0; i < (Int_t)tokens.size(); ++i)
      list.push_back(atoi(tokens[i].c_str()));
}

//______________________________________________________________________________
void TUMisc::SubstituteEnvInPath(string &path, string const &env)
{
   //--- Returns path with shorter environment variable substituted.
   // INPUTS:
   //  path         Absolute path in which to search for env.
   //  env          Environment variable to look for
   // OUTPUT:
   //  path         Path with env substituted

   // Find if $USINE, if yes, we replace it in file
   string full_env = GetPath(env);
   std::size_t found = path.find(full_env);
   if (found != std::string::npos)
      path.replace(found, full_env.length(), env);
}

//______________________________________________________________________________
string TUMisc::SystemExec(const char *cmd)
{
   //--- Returns string from shell command
   //  cmd          Command

   std::array<char, 128> buffer;
   std::string result;
   std::shared_ptr<FILE> pipe(popen(cmd, "r"), pclose);
   //if (!pipe) throw std::runtime_error("popen() failed!");
   while (!feof(pipe.get())) {
      if (fgets(buffer.data(), 128, pipe.get()) != nullptr)
         result += buffer.data();
   }
   return result;
}

//______________________________________________________________________________
void TUMisc::TEST(FILE *f)
{
   //--- Test function for this class (called in usine -t).
   //  f                 Output file for test

   string message =
      "The namespace TUMisc contains functions\n"
      "to sort/cast/extract lists from strings and vice-versa.";
   TUMessages::Test(f, "TUMisc", message);


   /***************************/
   //--- String extraction ---//
   /***************************/
   fprintf(f, " * String extractions (from list of integers, doubles, and strings...\n");
   fprintf(f, "   N.B.: blank spaces are systematically discarded (see in last example).\n\n");
   // string into list of integers
   string list1 = "??2?5??2?4?56?2";
   vector<Int_t> l_int;
   String2List(list1, "?", l_int);
   fprintf(f, "   > String2List(string=%s, separator=\"?\", output=vector<string>);\n",
           list1.c_str());
   for (Int_t i = 0; i < (Int_t)l_int.size(); i++)
      fprintf(f, "       %d ", l_int[i]);
   fprintf(f, "\n");
   fprintf(f, "   > RemoveDuplicates(vector<int>);\n");
   RemoveDuplicates(l_int);
   for (Int_t i = 0; i < (Int_t)l_int.size(); i++)
      fprintf(f, "       %d ", l_int[i]);
   fprintf(f, "\n");

   // string into list of doubles
   string list2 = "fff2.5f3.1ffff2.51ff2.5099999fff";
   vector<Double_t> l_Double_t;
   String2List(list2, "f", l_Double_t);
   fprintf(f, "   > String2List(string=%s, separator=\"f\", output=vector<string>);\n",
           list2.c_str());
   for (Int_t i = 0; i < (Int_t)l_Double_t.size(); i++)
      fprintf(f, "       %le ", l_Double_t[i]);
   fprintf(f, "\n");
   Double_t thresh = 1.e-4;
   fprintf(f, "   > RemoveDuplicates(vector<string>, thresh=%le);\n", thresh);
   RemoveDuplicates(l_Double_t, thresh);
   for (Int_t i = 0; i < (Int_t)l_Double_t.size(); i++)
      fprintf(f, "       %le ", l_Double_t[i]);
   fprintf(f, "\n");
   thresh = 1.e-1;
   fprintf(f, "   > RemoveDuplicates(vector<string>, thresh=%le);\n", thresh);
   RemoveDuplicates(l_Double_t, thresh);
   for (Int_t i = 0; i < (Int_t)l_Double_t.size(); i++)
      fprintf(f, "       %le ", l_Double_t[i]);
   fprintf(f, "\n");

   // string into list of strings, and remove duplicates
   string list3 = "ar#&AA&~ghh,AA;kk::bb,AA<sdd!ss,ggg,AA,";
   vector<string> l_string;
   String2List(list3, ",:<;", l_string);
   fprintf(f, "   > String2List(string=%s, separators=\",:<;\", output=vector<string>);\n",
           list3.c_str());
   for (Int_t i = 0; i < (Int_t)l_string.size(); i++)
      fprintf(f, "       %s ", l_string[i].c_str());
   fprintf(f, "\n");
   fprintf(f, "   > RemoveDuplicates(vector<string>);\n");
   RemoveDuplicates(l_string);
   for (Int_t i = 0; i < (Int_t)l_string.size(); i++)
      fprintf(f, "       %s ", l_string[i].c_str());
   fprintf(f, "\n");

   // another test
   string pfile_line = "Double_t  @DensityH  @hidden=0";
   vector<string> l_string2;
   String2List(pfile_line, "@", l_string2);
   fprintf(f, "   > String2List(string=%s, separator=\"@\", output=vector<string>);\n",
           pfile_line.c_str());
   for (Int_t i = 0; i < (Int_t)l_string2.size(); i++)
      fprintf(f, "       %s ", (l_string2[i].substr(l_string2[i].find_first_not_of(" "),
                                l_string2[i].find_last_not_of(" ") + 1)).c_str());
   fprintf(f, "\n");

   string reconst = List2String(l_string2, ',');
   fprintf(f, "   > List2String(input=vector<string> above, \',\');  =>  %s\n",
           reconst.c_str());
   fprintf(f, "\n");

   string blanks_and_tabs = " \t \t bob \t \t";
   string blanks_removed = RemoveBlancksFromStartStop(blanks_and_tabs);
   fprintf(f, "   > RemoveBlancksFromStartStop(blanks=\"%s\");  =>  \"%s\"\n",
           blanks_and_tabs.c_str(), blanks_removed.c_str());
   fprintf(f, "\n");

   fprintf(f, "   > RemoveChars(\"B/C-(68)\",\"/-()\");  =>  %s\n",
           (RemoveChars("B/C-(68)", "/-()")).c_str());
   fprintf(f, "\n");

   fprintf(f, "   > vector<string> = ExtractFromDir($USINE/inputs/XS*, is_verbose=true, f_out=f, is_selection=false, exclude_pattern=\"\", is_test=true);\n");
   ExtractFromDir("$USINE/inputs/XS*", true, f, false, "", true);
   fprintf(f, "   > vector<string> = ExtractFromDir($USINE/inputs/XS_ANTINUC/dSdEProd_pbar*, is_verbose=true, f_out=f, is_selection=false, exlude_pattern=\"\", is_test=true));\n");
   ExtractFromDir("$USINE/inputs/XS_ANTINUC/dSdEProd_pbar*", true, f, false, "", true);
   fprintf(f, "   > vector<string> = ExtractFromDir($USINE/inputs/XS_ANTINUC/dSdEProd_pbar*, is_verbose=true, f_out=f, is_selection=false, exlude_pattern=\"tertiary\", is_test=true));\n");
   ExtractFromDir("$USINE/inputs/XS_ANTINUC/dSdEProd_pbar*", true, f, false, "tertiary", true);
   fprintf(f, "\n");

   /**************/
   //--- Case ---//
   /**************/
   fprintf(f, " * Upper- and lower-case-isation\n");

   char c = 'c';
   fprintf(f, "   > UpperCase(%c);  =>  ", c);
   UpperCase(c);
   fprintf(f, "%c\n", c);

   string tmp = "test";
   fprintf(f, "   > UpperCase(%s)  =>  ", tmp.c_str());
   UpperCase(tmp);
   fprintf(f, "%s\n", tmp.c_str());
   fprintf(f, "\n");

   /**************************/
   //--- Formatted CR qty ---//
   /**************************/
   fprintf(f, " * Format for print or root-displays\n");

   string qty = "POSITRON/ELECTRON+POSITRON";
   fprintf(f, "   > FormatCRQty(qty=POSITRON/ELECTRON+POSITRON, switch_format=1);  => %s\n",
           FormatCRQty(qty, 1).c_str());
   fprintf(f, "   > FormatCRQty(qty=POSITRON/ELECTRON+POSITRON, switch_format=2);  => %s\n",
           FormatCRQty(qty, 2).c_str());
   qty = "10Be/9Be";
   fprintf(f, "   > FormatCRQty(qty=10Be/9Be, switch_format=1);  => %s\n",
           FormatCRQty(qty, 1).c_str());
   fprintf(f, "   > FormatCRQty(qty=10Be/9Be, switch_format=2);  => %s\n",
           FormatCRQty(qty, 2).c_str());
   qty = "E+/(e-+e+)";
   fprintf(f, "   > FormatCRQtyName(qty=%s);  => qty=", qty.c_str());
   FormatCRQtyName(qty);
   fprintf(f, "%s\n", qty.c_str());
   fprintf(f, "\n");
   fprintf(f, "\n");

   qty = "AMS-02(2011/06-2013/07)";
   fprintf(f, "   > RemoveSpecialChars(\"%s\") = ", qty.c_str());
   RemoveSpecialChars(qty);
   fprintf(f, "%s\n", qty.c_str());
   fprintf(f, "\n");

   //fprintf(f, "   > GetDatime() = %s", GetDatime().c_str());
   //fprintf(f, "\n");
}
