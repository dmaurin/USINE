// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

// C/C++ include
// ROOT include
// USINE include
#include "../include/TUDataEntry.h"

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TUDataEntry                                                          //
//                                                                      //
// Experimental data point for a CR quantity depending on energy.       //
//                                                                      //
// The TUDataEntry class provides a data point defined to be            //
//    - <E>_bin and bin range (with E=Ekn, Ek, or Etot);
//    - Measured Y(E) value + stat. and syst. asymmetric uncertainties;
//    - Experiment name, date/time, and URL of publication;
//    - Solar modulation level in 1-parameter models.
//                                                                      //
// Note that all the necessary information to evaluate the modulation   //
// parameter(s) in more detailed models, is in principle contained in   //
// the date/time of the experiment.                                     //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

ClassImp(TUDataEntry)

//______________________________________________________________________________
TUDataEntry::TUDataEntry()
{
   // ****** Default constructor ******

   Initialise();
}

//______________________________________________________________________________
TUDataEntry::TUDataEntry(TUDataEntry const &entry)
{
   // ****** Copy constructor ******
   //  entry           Object to copy

   Copy(entry);
}

//______________________________________________________________________________
TUDataEntry::~TUDataEntry()
{
   // ****** Default destructor ******
}

//______________________________________________________________________________
void TUDataEntry::AllocateYErrRelCov(Int_t n_types, Int_t ne_cov)
{
   //--- Reset and allocate data covariance (of relative errors) for error types.
   //  n_types           Number of types
   //  ne_cov            Number of energies

   // Set fYErrRelCov
   fYErrRelCov.clear();
   fYErrRelCov.resize(n_types);
   fIsCovTypeNuisPar.clear();
   fIsCovTypeNuisPar.assign(n_types, false);
   fIsAtLeast1NuisPar = false;

   for (Int_t i = 0; i < n_types; ++i)
      fYErrRelCov[i].assign(ne_cov, 0.);
   AllocateYErrRelCovTypes(n_types);
}

//______________________________________________________________________________
void TUDataEntry::Convert(gENUM_ETYPE e_type, TUCREntry const &cr)
{
   //--- Convert data entry from 'native' format fEType to desired 'e-type'.
   //    Changes if necessary all energies and Y values. Note that it should
   //    not be used for ratio of data corresponding to aggregated species.
   //  e_type            Selects x axis among gENUM_ETYPE (kEKN, kR,...)
   //  cr                CR charts used to convert data to e_type (if different from data fEType)

   Double_t e_in = fEMean;

   fEMean = TUPhysics::ConvertE(fEMean, fEType, e_type, cr.GetA(), cr.GetZ(), cr.GetmGeV());
   fEBinL = TUPhysics::ConvertE(fEBinL, fEType, e_type, cr.GetA(), cr.GetZ(), cr.GetmGeV());
   fEBinU = TUPhysics::ConvertE(fEBinU, fEType, e_type, cr.GetA(), cr.GetZ(), cr.GetmGeV());

   Double_t convert = TUPhysics::dNdEin_to_dNdEout(e_in, fEType, e_type, cr.GetA(), cr.GetZ(), cr.GetmGeV());

   fY = convert * fY;
   fYErrStatL = convert * fYErrStatL;
   fYErrStatU = convert * fYErrStatU;
   fYErrSystL = convert * fYErrSystL;
   fYErrSystU = convert * fYErrSystU;
}

//______________________________________________________________________________
void TUDataEntry::Copy(TUDataEntry const &entry)
{
   //--- Copies from entry in current class.
   //  entry             TUDataEntry to copy from

   Initialise();

   fBibURL = entry.GetBibURL();
   fEBinL = entry.GetEBinL();
   fEBinU = entry.GetEBinU();
   fEMean = entry.GetEMean();
   fEType = entry.GetEType();
   fExpDatimes.clear();
   for (Int_t i = 0; i < entry.GetNDatimes(); ++i)
      fExpDatimes.push_back(entry.GetExpDatime(i));
   fExpDistance = entry.GetExpDistance();
   fExpName = entry.GetExpName();
   fExpphi = entry.GetExpphi();
   fIsUpperLimit = entry.IsUpperLimit();
   fQty = entry.GetQty();
   fY = entry.GetY();
   // Fill covariance-related members
   if (entry.GetNCovTypes() != 0) {
      fIsAtLeast1NuisPar = entry.IsAtLeast1NuisPar();
      AllocateYErrRelCov(entry.GetNCovTypes(), entry.GetNCovE());
      for (Int_t t = 0; t < GetNCovTypes(); ++t) {
         // Copy fIsCovTypeNuisPar
         if (entry.IsCovTypeNuisPar(t))
            SetIsCovTypeNuisPar(t);
         // Copy fYErrRelCovTypes
         SetYErrCovType(t, entry.GetYErrRelCovType(t));
         // Copy fYErrRelCov
         for (Int_t i = 0; i < entry.GetNCovE(); ++i)
            SetYErrCov(t, i, entry.GetYErrRelCov(t, i));
      }
   }

   fYErrStatL = entry.GetYErrStatL();
   fYErrStatU = entry.GetYErrStatU();
   fYErrSystL = entry.GetYErrSystL();
   fYErrSystU = entry.GetYErrSystU();
}

//______________________________________________________________________________
string TUDataEntry::GetExpDatimes() const
{
   //--- Retrieves flight datimes [yyyy/mm/dd-hhmmss:yyyy/mm/dd-hhmmss;...] for exp.

   string datimes_usine = "";
   for (Int_t i = 0; i < GetNDatimes(); ++i) {
      // Get Start-stop (std format)
      string start = fExpDatimes[i].GetStart();
      string stop = fExpDatimes[i].GetStop();
      // Convert to usine format
      fExpDatimes[0].ConvertSTD2USINE(start);
      fExpDatimes[0].ConvertSTD2USINE(stop);

      if (i > 0)
         datimes_usine += ";";
      datimes_usine += start + ":" + stop;
   }
   return datimes_usine;
}

//______________________________________________________________________________
Double_t TUDataEntry::GetYErr(gENUM_ERRTYPE err_type, Bool_t is_lower_or_upper) const
{
   //--- Returns data entry error for gEnum_ERRTYPE selected.
   //  err_type          Selects uncertainties among gENUM_ERRTYPE (kERRSTAT, kERRSYST, kERRTOT, kERRCOV)
   //  is_lower_or_upper If true, returns for ErrL, otherwise for ErrU

   switch (err_type) {
      case kERRSTAT:
         return (is_lower_or_upper) ? GetYErrStatL() : GetYErrStatU();
      case kERRSYST:
         return (is_lower_or_upper) ? GetYErrSystL() : GetYErrSystU();
      case kERRTOT:
      case kERRCOV:
         return (is_lower_or_upper) ? GetYErrTotL() : GetYErrTotU();
   }
   return 0.;
}

//______________________________________________________________________________
void TUDataEntry::Initialise()
{
   //--- Set default values

   fBibURL = "";
   fEBinL = -1.;
   fEBinU = -1.;
   fEMean = -1.;
   fEType = kEKN;
   fExpDatimes.clear();
   fExpDistance = -1.;
   fExpName = "";
   fExpphi = -1.;
   fIsUpperLimit = false;
   fQty = "";
   fY = -1.;
   fYErrRelCov.clear();
   fYErrRelCovTypes.clear();
   fYErrStatL = -1.;
   fYErrStatU = -1.;
   fYErrSystL = -1.;
   fYErrSystU = -1.;

}

//______________________________________________________________________________
Bool_t TUDataEntry::IsSameEntry(TUDataEntry const &entry) const
{
   //--- Checks whether entry matches current entry.
   //  entry             TUDataEntry to check

   if (fBibURL == entry.GetBibURL()
         && fabs(fEBinL - entry.GetEBinL()) / fEBinL < 1.e-3
         && fabs(fEBinU - entry.GetEBinU()) / fEBinU < 1.e-3
         && fabs(fEMean - entry.GetEMean()) / fEMean < 1.e-3
         && fEType == entry.GetEType()
         && fIsUpperLimit == entry.IsUpperLimit()
         && fQty == entry.GetQty()
         && fabs(fExpDistance - entry.GetExpDistance()) / fExpDistance < 1.e-3
         && fExpName == entry.GetExpName()
         && fExpphi == entry.GetExpphi()
         && fabs(fY - entry.GetY()) / fY < 1.e-3)
      return true;

   return false;
}

//______________________________________________________________________________
Bool_t TUDataEntry::IsSameQtyExpEtype(TUDataEntry const &entry) const
{
   //--- Checks whether entry matches current entry qty/exp/Etype.
   //  entry             TUDataEntry to check

   if (fBibURL == entry.GetBibURL()
         && fEType == entry.GetEType()
         && fIsUpperLimit == entry.IsUpperLimit()
         && fQty == entry.GetQty()
         && (
            (fabs(fExpDistance) < 1.e-30 && fabs(entry.GetExpDistance()) < 1e-30)
            || (fabs(fExpDistance - entry.GetExpDistance()) / fExpDistance < 1.e-3)
         )
         && fExpName == entry.GetExpName()
         && fExpphi == entry.GetExpphi())
      return true;

   return false;
}
//______________________________________________________________________________
void TUDataEntry::Print(FILE *f, Int_t switch_print, Bool_t is_header) const
{
   //--- Prints data-formatted in file f.
   //  f                 File in which to print
   //  switch_print      Decide how to print data (0=short, 1=full)
   //  is_header         If true, print info on column content

   string indent = TUMessages::Indent(true);

   switch (switch_print) {
      case 0: // short description
         if (is_header) {
            fprintf(f, "%s   Qty        Exp-Name                     "
                    "Date  phi(A/Z*PHI) E-type   <E>       Measurement       Publi.ADS\n", indent.c_str());
            fprintf(f, "%s    -             -                        "
                    "  -     [GV]              %-8s %-20s  -\n", indent.c_str(), GetEUnit(true).c_str(), GetYUnit(true).c_str());
         }
         if (fIsUpperLimit)
            fprintf(f, "%s   %-10s %-28s %-4s    %.3f      %-4s %.3le    <%.3le     %s\n",
                    indent.c_str(), GetQty(1).c_str(), fExpName.c_str(),
                    (GetExpDatimes().substr(0, 4)).c_str(), fExpphi,
                    TUEnum::Enum2Name(GetEType()).c_str(), fEMean, fY, fBibURL.c_str());
         else
            fprintf(f, "%s   %-10s %-28s %-4s    %.3f      %-4s  %.3le    %.3le     %s\n",
                    indent.c_str(), GetQty(1).c_str(), fExpName.c_str(),
                    (GetExpDatimes().substr(0, 4)).c_str(), fExpphi,
                    TUEnum::Enum2Name(GetEType()).c_str(), fEMean, fY, fBibURL.c_str());
         break;
      case 1:  // Full description
         if (!fIsUpperLimit)
            fprintf(f, "%s   %-15s %.1e (+%.1e/-%.1e) %-4s %-8s    %.1e (+%.1e/-%.1e)_STAT (+%.1e/-%.1e)_SYST %-23s %s  %s\n",
                    indent.c_str(), GetQty(0).c_str(), fEMean, fEBinU, fEBinL,
                    TUEnum::Enum2Name(GetEType()).c_str(), GetEUnit(true).c_str(),
                    fY, GetYErrStatU(), GetYErrStatL(), GetYErrSystU(), GetYErrSystL(),
                    GetYUnit(true).c_str(), fBibURL.c_str(), fExpName.c_str());
         else
            fprintf(f, "%s   %-15s %.1e (+%.1e/-%.1e) %-4s %-8s    <%.1e (+%.1e/-%.1e)_STAT (+%.1e/-%.1e)_SYST %-23s %s  %s\n",
                    indent.c_str(), GetQty(0).c_str(), fEMean, fEBinU, fEBinL,
                    TUEnum::Enum2Name(GetEType()).c_str(), GetEUnit(true).c_str(),
                    fY, GetYErrStatU(), GetYErrStatL(), GetYErrSystU(), GetYErrSystL(),
                    GetYUnit(true).c_str(), fBibURL.c_str(), fExpName.c_str());

         break;
      default:
         TUMessages::Indent(false);
         return;
   }
   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUDataEntry::PrintDataRelCov(FILE *f, Int_t t_type, Int_t ke_start, Int_t ke_stop, Bool_t is_header) const
{
   //--- Prints data covariance.
   //  f                 File in which to print
   //  t_type            Index of covariance error type (default=-1, means all types)
   //  ke_start          Index of starting e_cov (default=-1, means starts from 0)
   //  ke_stop           Index of stoping e_cov (default=-1, means stops at GetNCovE())
   //  is_header         If true, print info on column content

   string indent = TUMessages::Indent(true);

   if (ke_start < 0)
      ke_start = 0;
   if (ke_stop < 0 || ke_stop > GetNCovE() - 1)
      ke_stop = GetNCovE();

   Int_t t_min = t_type;
   Int_t t_max = t_type + 1;
   if (t_type < 0 || t_type > GetNCovTypes() - 1) {
      t_min = 0;
      t_max = GetNCovTypes();
   }

   if (is_header) {
      if (ke_start == 0 && ke_stop == GetNCovE())
         fprintf(f, "%s      (Covariance)   [%d points]\n", indent.c_str(), GetNCovE());
      else
         fprintf(f, "%s      (Covariance)   [%d to %d, out of %d points]\n", indent.c_str(), ke_start, ke_stop, GetNCovE());
   }
   for (Int_t t = t_min; t < t_max; ++t) {
      fprintf(f, "%s %6s: ", indent.c_str(), GetYErrRelCovType(t).c_str());
      for (Int_t ke = ke_start; ke < ke_stop; ++ke)
         fprintf(f, "%+le  ", GetYErrRelCov(t, ke));
      fprintf(f, "   @ %le %s\n", GetEMean(), GetEUnit(true).c_str());
   }
   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUDataEntry::PrintExp(FILE *f, Bool_t is_header) const
{
   //--- Prints data-formatted in file f.
   //  f                 File in which to print
   //  is_header         If true, print info on column content

   string indent = TUMessages::Indent(true);

   if (is_header)
      fprintf(f, "%s  SUBEXP_NAME             FF.phi[GV]  Datimes   COORD[AU]\n",
              indent.c_str());
   fprintf(f, "%s   %-24s  %.3f   %-30s  %.2f\n", fExpName.c_str(),
           indent.c_str(), fExpphi, GetExpDatimes().c_str(), fExpDistance);

   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUDataEntry::SetExpDatimes(string const &datimes)
{
   //--- Set vector of datimes from string (USINE formatted).
   //  datimes           [yyyy/mm/dd-hhmmss:yyyy/mm/dd-hhmmss;...] for exp.

   fExpDatimes.clear();
   vector<string> l_datimes;
   TUMisc::String2List(datimes, ";", l_datimes);
   for (Int_t i = 0; i < (Int_t)l_datimes.size(); ++i) {
      vector<string> start_stop;
      TUMisc::String2List(l_datimes[i], ":", start_stop);

      TUDatime entry;
      entry.ConvertUSINE2STD(start_stop[0]);
      entry.ConvertUSINE2STD(start_stop[1]);
      entry.SetStartStop(start_stop[0], start_stop[1]);

      // Add to vector of datimes
      fExpDatimes.push_back(entry);
   }
}

//______________________________________________________________________________
void TUDataEntry::SetEntry(string const &entry_line, FILE *f_log)
{
   //--- Set entry from (USINE formatted) line.
   //  entry_line        USINE-formatted line.
   //  f_log             Log file

   // N.B.: template for entry line is 16 entries separated by spaces
   // [0] #   Col.1  -  QUANTITY NAME (case insensitive)
   // [1] #   Col.2  -  SUB-EXP NAME (case insensitive, no space)
   // [2] #   Col.3  -  EAXIS TYPE: EKN, EK, R, or ETOT
   // [3] #   Col.4  -  <E>: mean value bin [GeV/n, GeV, GV, or GeV]
   // [4] #   Col.5  -  EBIN_LOW
   // [5] #   Col.6  -  EBIN_HIGH
   // [6] #   Col.7  -  QUANTITY VALUE: [#/sr/s/m2/EAxis] if flux , no unit if ratio
   // [7] #   Col.8  -  ERR_STAT-
   // [8] #   Col.9  -  ERR_STAT+
   // [9] #   Col.10 -  ERR_SYST-
   // [10]#   Col.11 -  ERR_SYST+
   // [11]#   Col.12 -  ADS URL FOR PAPER REF (no space)
   // [12]#   Col.13 -  phi [MV]
   // [13]#   Col.14 -  DISTANCE EXP IN SOLAR SYSTEM [AU]
   // [14]#   Col.15 -  DATIMES: format = yyyy/mm/dd-hhmmss:yyyy/mm/dd-hhmmss;...
   // [15]#   Col.16 -  IS UPPER LIMIT: format = 0 or 1

   Int_t n_cols_to_read = 16;
   vector<string> params;
   TUMisc::String2List(entry_line, " \t", params);
   if (Int_t(params.size()) != n_cols_to_read) {
      string message = Form("Wrong format for\n    %s\n --> requires %d entries, %d read)!",
                            entry_line.c_str(), n_cols_to_read, (Int_t)params.size());
      TUMessages::Error(f_log, "TUDataEntry", "SetEntry", message);
   }

   fBibURL = params[11];
   SetExpDatimes(params[14]);
   fEMean = fabs(atof(params[3].c_str()));
   fEBinL = fabs(atof(params[4].c_str()));
   fEBinU = fabs(atof(params[5].c_str()));
   gENUM_ETYPE e_type = kEKN;
   TUEnum::Name2Enum(params[2], e_type);
   fEType = e_type;
   fIsUpperLimit = atoi(params[15].c_str());
   SetQty(params[0]);
   fExpDistance = atof(params[13].c_str());
   fExpName = params[1];
   TUMisc::UpperCase(fExpName);
   fExpphi = 1.e-3 * atof(params[12].c_str());
   fY = atof(params[6].c_str());
   fYErrStatL = fabs(atof(params[7].c_str()));
   fYErrStatU = fabs(atof(params[8].c_str()));
   fYErrSystL = fabs(atof(params[9].c_str()));
   fYErrSystU = fabs(atof(params[10].c_str()));
}

//______________________________________________________________________________
void TUDataEntry::TEST(FILE *f)
{
   //--- Test function for this class (called in usine -t).
   //  f                 Output file for test

   string message = "Data entry storage";
   TUMessages::Test(f, "TUDataEntry", message);

   fprintf(f, " * Fill class (dummy entry) and test various print\n");
   string entry_line = "B/C  EXP1(1965)  EKN   0.1  0.08 0.2    0.4   0.2 0.1  0.05 0.05  ref1  400.  1.  "
                       + (string) "  1965/07/05-130000:1965/07/05-180000;1965/08/05-130000:1965/08/05-180000  0 ";
   SetEntry(entry_line);
   fprintf(f, "    > PrintExp(f, true);\n");
   PrintExp(f, true);
   fprintf(f, "    > Print(f, 0,true);\n");
   Print(f, 0, true);
   fprintf(f, "    > Print(f, 0);\n");
   Print(f, 0);
   fprintf(f, "    > Print(f, 1);\n");
   Print(f, 1);

   // Test covariance
   fprintf(f, "\n");
   fprintf(f, "\n");
   fprintf(f, "   > AllocateYErrRelCov(2, 5);\n");
   AllocateYErrRelCov(2, 5);
   fprintf(f, "   > GetNCovTypes() = %d\n", GetNCovTypes());
   fprintf(f, "   > GetNCovE() = %d\n", GetNCovE());
   fprintf(f, "   > SetYErrCovType(0, back);\n");
   SetYErrCovType(0, "back");
   fprintf(f, "   > SetYErrCovType(1, unf);\n");
   SetYErrCovType(1, "unf");
   fprintf(f, "   > GetYErrRelCovTypes() = %s;\n", GetYErrRelCovTypes().c_str());
   fprintf(f, "   > SetYErrCov(0,1,0.1);\n");
   fprintf(f, "   > SetYErrCov(0,2,0.3);\n");
   fprintf(f, "   > SetYErrCov(0,4,0.2);\n");
   fprintf(f, "   > SetYErrCov(1,1,0.01);\n");
   fprintf(f, "   > SetYErrCov(1,2,0.03);\n");
   fprintf(f, "   > SetYErrCov(1,4,0.02);\n");
   SetYErrCov(0, 1, 0.1);
   SetYErrCov(0, 2, 0.3);
   SetYErrCov(0, 4, 0.2);
   SetYErrCov(1, 1, 0.01);
   SetYErrCov(1, 2, 0.03);
   SetYErrCov(1, 4, 0.02);
   fprintf(f, "   > PrintDataRelCov(f, -1, -1, -1, is_header=true);\n");
   PrintDataRelCov(f, -1, -1, -1, true);
   fprintf(f, "   > PrintDataRelCov(f, 1, 1, 3, is_header=true);\n");
   PrintDataRelCov(f, 1, 1, 3, true);
   fprintf(f, "\n");
   fprintf(f, "\n");

   fprintf(f, " * Test copy method:\n");
   fprintf(f, "   > TUDataEntry data; data.Copy(*this);\n");
   TUDataEntry data;
   data.Copy(*this);
   fprintf(f, "   > Print(f);\n");
   Print(f);
   fprintf(f, "   > PrintDataRelCov(f);\n");
   PrintDataRelCov(f);
   fprintf(f, "   > data.Print(f);\n");
   data.Print(f);
   fprintf(f, "   > data.PrintDataRelCov(f);\n");
   data.PrintDataRelCov(f);
   fprintf(f, "\n");

}

//______________________________________________________________________________
void TUDataEntry::YTimesPowerOfE(Double_t const &e_power)
{
   //--- Multiplies all Y quantities by <E>^{e_power}.
   //  e_power           Power index (positive or negative)

   Double_t factor = pow(fEMean, e_power);

   fY *= factor;
   fYErrStatL *= factor;
   fYErrStatU *= factor;
   fYErrSystL *= factor;
   fYErrSystU *= factor;
}
