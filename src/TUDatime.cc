// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

// C/C++ include
#include <iostream>
// ROOT include
// USINE include
#include "../include/TUDatime.h"
#include "../include/TUMisc.h"

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TUDatime                                                             //
//                                                                      //
// Stores date and time (1s precision) in two strings (start and stop). //
//                                                                      //
// The class TUDatime provides an UTC date and time concatenated in a   //
// single string - hence datime -, whose format is YYYY-MM-DD HH:MM:SS. //
// For instance, a valid datime is 2009-10-25 23:05:02 (23h05mn02s,     //
// October 25th, 2009). Conversion functions ConvertSTD2USINE() and     //
// ConvertUSINE2STD() allow to move from this format to USINE format    //
// (and vice-versa) for datime strings YYYY/MM/DD-HHMMSS instead.       //
//                                                                      //
// For completeness, we also provide initialisation from more standard  //
// Unix format based on local or UTC time. Note however that intervals  //
// and time operations are hard-coded in TUdatime to avoid trouble for  //
// dates before 1970 (which is not always correctly handled by Unix     //
// times, which are:                                                    //
//    - time_t (from <ctime>): Integral value representing the number
//      of seconds elapsed since 00:00 hours, Jan 1, 1970 UTC (i.e.,
//      a unix timestamp)
//    - struct tm (from <ctime>): structure with the following fields
//        - tm_sec  (int): seconds after the minute, 0-61*
//        - tm_min  (int): minutes after the hour, 0-59
//        - tm_hour (int): hours since midnight, 0-23
//        - tm_mday (int): day of the month, 1-31
//        - tm_mon  (int): months since January, 0-11
//        - tm_year (int): years since 1900
//        - tm_wday (int): days since Sunday,  0-6
//        - tm_yday (int): days since January 1,  0-365
//        - tm_isds (int): Daylight Saving Time flag
//  *tm_sec is generally 0-59. The extra range is to accommodate for    //
//   leap seconds in certain systems.                                   //
//                                                                      //
// Always be careful when you attempt conversions of dates into ROOT    //
// TTimeStamp or time_t. Both rely on time_t which gives a number of    //
// seconds relative to Jan 1, 1970 00:00:00 UTC, and sometimes lag a    //
// day or worse when used out of their range.                           //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

ClassImp(TUDatime)

//______________________________________________________________________________
TUDatime::TUDatime()
{
   //--- Creates a TUDatime.

   Initialise();
}

//______________________________________________________________________________
TUDatime::TUDatime(TUDatime const &datime)
{
   // ****** Copy constructor ******
   //  datime            Datime used to fill class

   Copy(datime);
}

//______________________________________________________________________________
void TUDatime::AddMonth(Bool_t is_start)
{
   //--- Adds a month to start or stop date.
   //  is_start          Whether start or stop date

   Int_t year = GetYear(is_start);
   Int_t month = GetMonth(is_start);
   Int_t day = GetDay(is_start);

   if (month < 12)
      month += 1;
   else {
      month = 1;
      year += 1;
   }

   string new_datime = Form("%4d-%02d-%02d %s", year, month, day, GetTime().c_str());
   Set(new_datime, is_start);
}

//______________________________________________________________________________
void TUDatime::AddSec(ULong_t const &dt, Bool_t is_start)
{
   //--- Adds a number of seconds to start or stop date.
   //  dt                Number of seconds to add
   //  is_start          Whether start or stop date

   // Cast the number of seconds as a number of days, hours, minute, seconds
   Int_t n_days = dt / 86400;
   Int_t n_hours = (dt - n_days * 86400) / 3600;
   Int_t n_minutes = (dt - n_days * 86400 - n_hours * 3600) / 60;
   Int_t n_seconds = dt % 60;

   Int_t new_sec = GetSecond(is_start) + n_seconds;
   Int_t new_mn = GetMinute(is_start) + n_minutes;
   Int_t new_hr = GetHour(is_start) + n_hours;
   Int_t new_day = GetDay(is_start) + n_days;
   Int_t new_month = GetMonth(is_start);
   Int_t new_year = GetYear(is_start);

   // Add seconds, minutes, and days
   if (new_sec >= 60) {
      new_sec -= 60;
      new_mn += 1;
   }
   if (new_mn >= 60) {
      new_mn -= 60;
      new_hr += 1;
   }
   if (new_hr >= 24) {
      new_hr -= 24;
      new_day += 1;
   }

   // Now we have the number of days to add...
   do {
      Int_t n_days_in_month = DaysInMonth(new_month, new_year);
      if (new_day > n_days_in_month) {
         new_month += 1;
         if (new_month > 12) {
            new_month = 1;
            new_year += 1;
         }
         new_day -= n_days_in_month;
      } else break;
   } while (1);

   string tmp = Form("%04d-%02d-%02d %02d:%02d:%02d", new_year, new_month, new_day, new_hr, new_mn, new_sec);

   Set(tmp, is_start);
}

//______________________________________________________________________________
Bool_t TUDatime::CheckDateFormat(string const &date, Bool_t std_or_usine, Bool_t is_abort)
{
   //--- Check Date correctly formatted for two possible format.
   //  date              Date to check
   //  std_or_usine      Format checked is:
   //                       YYYY-MM-DD (std_or_usine=true)
   //                       YYYY/MM/DD (std_or_usine=false)
   //  is_abort          Abort if does not pass check

   if ((std_or_usine && (date.size() != 10 || date[4] != '-' || date[7] != '-'))
         || (!std_or_usine && (date.size() != 10 || date[4] != '/' || date[7] != '/'))) {
      string message = "";
      if (std_or_usine)
         message = "Bad format for \"" + date + "\": STD must be YYYY-MM-DD";
      else
         message = "Bad format for \"" + date + "\", USINE must be YYYY/MM/DD";
      if (is_abort)
         TUMessages::Error(stdout, "TUDatime", "CheckDateFormat", message);
      else
         TUMessages::Warning(stdout, "TUDatime", "CheckDateFormat", message);
      return false;
   } else
      return true;
}

//______________________________________________________________________________
Bool_t TUDatime::CheckDatimeFormat(string const &datime, Bool_t std_or_usine, Bool_t is_abort)
{
   //--- Check Datime correctly formatted for two possible format.
   //  datime            Datime to check
   //  std_or_usine      Format checked is:
   //                       YYYY-MM-DD HH:MM:SS  (std_or_usine=true)
   //                       YYYY/MM/DD-HHMMSS    (std_or_usine=false)
   //  is_abort          Abort if does not pass check

   if ((std_or_usine && (datime.size() != 19 || datime[4] != '-' || datime[7] != '-' || datime[10] != ' ' || datime[13] != ':' || datime[16] != ':'))
         || (!std_or_usine && (datime.size() != 17 || datime[4] != '/' || datime[7] != '/' || datime[10] != '-'))) {
      string message = "";
      if (std_or_usine)
         message = "Bad format for \"" + datime + "\": STD must be YYYY-MM-DD HH:MM:SS";
      else
         message = "Bad format for \"" + datime + "\", USINE must be YYYY/MM/DD-HHMMSS";
      if (is_abort)
         TUMessages::Error(stdout, "TUDatime", "CheckDatimeFormat", message);
      else
         TUMessages::Warning(stdout, "TUDatime", "CheckDatimeFormat", message);
      return false;
   } else
      return true;
}

//______________________________________________________________________________
void TUDatime::ConvertUSINE2STD(string &datime)
{
   //--- Converts datime format from USINE (YYYY/MM/DD-HHMMSS) to STD (YYYY-MM-DD HH:MM:SS).
   // IN:
   //  datime            Datime in USINE format (YYYY/MM/DD-HHMMSS)
   // OUT:
   //  datime            Datime in STD format (YYYY-MM-DD HH:MM:SS)

   // Check that it is USINE formatted
   CheckDatimeFormat(datime, false, true);

   // Convert to STD
   datime[4] = '-';
   datime[7] = '-';
   datime[10] = ' ';
   datime.insert(15, ":", 1);
   datime.insert(13, ":", 1);

   return;
}
//______________________________________________________________________________
void TUDatime::ConvertSTD2USINE(string &datime)
{
   //--- Converts datime format from STD (YYYY-MM-DD HH:MM:SS) to USINE (YYYY/MM/DD-HHMMSS).
   // IN:
   //  datime            Datime in STD format (YYYY-MM-DD HH:MM:SS)
   // OUT:
   //  datime            Datime in USINE format (YYYY/MM/DD-HHMMSS)

   // Check that it is STD formatted
   CheckDatimeFormat(datime, true, true);

   // Convert to USINE
   datime[4] = '/';
   datime[7] = '/';
   datime[10] = '-';
   datime.erase(datime.begin() + 13);
   datime.erase(datime.begin() + 15);

   return;
}

//______________________________________________________________________________
void TUDatime::Copy(TUDatime const &datime)
{
   //--- Copies datime in current object.
   //  datime            Datime to set

   fStart = datime.GetStart();
   fStop = datime.GetStop();
}

//______________________________________________________________________________
Int_t TUDatime::DaysInMonth(Int_t month, Int_t year)
{
   //--- Number of days in month (for a given year)?
   //  month         Month to test
   //  year          Year to test

   if (month == 2) {
      if (IsLeapYear(year))
         return 29;
      else return 28;
   } else if (month == 2 || month == 4 || month == 6 || month == 9 || month == 11)
      return 30;
   else
      return 31;
}

//______________________________________________________________________________
Int_t TUDatime::DaysInYear(Int_t year)
{
   //--- Number of days in year?
   //  year          Year to test

   if (IsLeapYear(year))
      return 366;
   else
      return 365;
}

//______________________________________________________________________________
vector<TUDatime> TUDatime::CommonDatimes(vector<vector<TUDatime> > const &datimes)
{
   //--- Returns vector of datimes common to list of vectors (sorted by growing time).
   //   datimes      Vector of vector of datimes

   Int_t n = datimes.size();
   if (n == 1)
      return datimes[0];

   vector<TUDatime> common = datimes[0];
   for (Int_t i = 1; i < n; ++i)
      common = TUDatime::CommonDatimes(common, datimes[i]);

   return common;
}

//______________________________________________________________________________
vector<TUDatime> TUDatime::CommonDatimes(vector<TUDatime> const &datimes1, vector<TUDatime> const &datimes2)
{
   //--- Returns vector of datimes common to two input lists (sorted by growing time).
   //   datimes1     First vector of datimes
   //   datimes2     Second vector of datimes

   vector<TUDatime> common;

   // Loop on all indices (for the two datimes)
   Int_t i1 = 0, i2 = 0;
   do {
      string dt1 =  datimes1[i1].GetStart();
      string dt2 =  datimes2[i2].GetStart();

      // If same datime, go to next for both (nothing to do)
      if (dt1 == dt2) {
         common.push_back(datimes1[i1]);
         ++i1;
         ++i2;
         continue;
      }

      // Find which time to add next
      if (TUDatime::IsStartBeforeStop(dt1, dt2)) {
         common.push_back(datimes1[i1]);
         ++i1;
      } else {
         common.push_back(datimes2[i2]);
         ++i2;
      }
   } while (i1 < (Int_t)datimes1.size() && i2 < (Int_t)datimes2.size());

   return common;
}

//______________________________________________________________________________
time_t TUDatime::GetDatime_t(Bool_t is_start) const
{
   //--- Returns Datime in time_t format.
   //  is_start          Returns start or stop date

   struct tm t = GetDatime_tm(is_start);
   return mktime(&t);
}

//______________________________________________________________________________
struct tm TUDatime::GetDatime_tm(Bool_t is_start) const {
   //--- Returns Datime in struct tm format.
   //  is_start          Returns start or stop date

   struct tm datime;
   datime.tm_year = GetYear(is_start) - 1900;
   datime.tm_mon = GetMonth(is_start) - 1;
   datime.tm_mday = GetDay(is_start);
   datime.tm_hour = GetHour(is_start);
   datime.tm_min = GetMinute(is_start);
   datime.tm_sec = GetSecond(is_start);
   datime.tm_isdst = -1;
   /*printf("  datime= %02d-%02d-%02d %02d:%02d:%02d\n",
        datime.tm_year+1900, datime.tm_mon+1, datime.tm_mday,
        datime.tm_hour, datime.tm_min, datime.tm_sec);*/

   return datime;
}

//______________________________________________________________________________
string TUDatime::GetStartStop(string const &separator, Int_t datime0_date1_time2) const
{
   //--- Returns start and stop datime (or date or time).
   //  separator         Separator to use between start and stop
   //  datime0_date1_time2 Returns datime, date or time

   switch (datime0_date1_time2) {
      case 0:
         return (fStart + separator + fStop);
      case 1:
         return (GetDate(true) + separator + GetDate(false));
      case 2:
         return (GetTime(true) + separator + GetTime(false));
      default:
         return (fStart + separator + fStop);
   }

   return (fStart + separator + fStop);
}

//______________________________________________________________________________
Int_t TUDatime::IndexClosest(vector<TUDatime> &datimes, TUDatime &datime) const
{
   //--- Finds index k for which datimes[k] is closest to 'datime' (but smaller).
   //  datimes           List of datimes
   //  datime            Datime to search for

   std::vector<TUDatime>::iterator low;
   low = std::lower_bound(datimes.begin(), datimes.end(), datime, *this);
   //up = std::upper_bound(datimes.begin(), datimes.end(), datime, *this);

   Int_t index = low - datimes.begin();
   if (index == (Int_t)datimes.size())
      return index - 1;
   else return index;
}

//______________________________________________________________________________
ULong_t TUDatime::IntervalSec(TUDatime const &datime1, TUDatime const &datime2) const
{
   //--- Time interval in seconds between datime1 and datime2. We do not directly
   //    use difftime and time_t as there are some weird issues, whatever we tried.
   //    Instead we calculate it from the number of years, months...

   Int_t year_start = datime1.GetYear();
   Int_t year_stop = datime2.GetYear();
   ULong_t n_sec = 0;
   if (year_start == year_stop)
      n_sec = datime1.SecondsTillEndOfYear() - datime2.SecondsTillEndOfYear();
   else {
      n_sec = datime1.SecondsTillEndOfYear() + datime2.SecondsFromStartOfYear();
      // Add all years in-between
      for (Int_t year = year_start + 1; year < year_stop; ++year)
         n_sec += SecondsInYear(year);
   }

   //// Fails for some weird dates (not even in the 70's...)
   //struct tm tm1 = datime1.GetDatime_tm();
   //struct tm tm2 = datime2.GetDatime_tm();
   //ULong_t dt_diff = difftime(mktime(&tm2), mktime(&tm1));
   //ULong_t dt_make = mktime(&tm2) - mktime(&tm1);
   //cout << " Mine=" << n_sec << " vs difftime=" << dt_diff << " vs mktime=" << dt_make << endl;

   return n_sec;
}

//______________________________________________________________________________
Bool_t TUDatime::IntervalsCheckTimeOrderedNonOverlapping(vector<TUDatime> const &intervals, Bool_t is_abort, FILE *f)
{
   //--- Checks that intervals are time-ordered and non-overlapping: if does
   //    not abort return true if intervals as required, false otherwise.
   //  intervals         List of intervals
   //  is_abort          Abort if does not pass check
   //  f                 Output file for chatter

   Int_t n = intervals.size();

   // Check that intervals are time-ordered and non-overlapping (and not ill-formed)
   for (Int_t i = 0; i < n; ++i) {
      // Check current interval not ill-formed
      if (!intervals[i].IsStartBeforeStop()) {
         string message = Form("Interval %d is ill-formed: start=%s > stop=%s",
                               i, (intervals[i].GetStart()).c_str(),
                               (intervals[i].GetStop()).c_str());
         if (is_abort)
            TUMessages::Error(f, "TUDatime", "IntervalsCheckTimeOrderedNonOverlapping", message);
         else {
            TUMessages::Warning(f, "TUDatime", "IntervalsCheckTimeOrderedNonOverlapping", message);
            return false;
         }
      }
      if (i == (n - 1))
         return true;

      // Check time-ordered
      if (!IsStartBeforeStop(intervals[i].GetStart(), intervals[i + 1].GetStart())) {
         string message = Form("Non time-ordered intervals %d (start=%s) and %d (start=%s)",
                               i, (intervals[i].GetStart()).c_str(),
                               i + 1, (intervals[i + 1].GetStart()).c_str());
         if (is_abort)
            TUMessages::Error(f, "TUDatime", "IntervalsCheckTimeOrderedNonOverlapping", message);
         else {
            TUMessages::Warning(f, "TUDatime", "IntervalsCheckTimeOrderedNonOverlapping", message);
            return false;
         }
      }

      // Check non-overlapping
      if (!IsStartBeforeStop(intervals[i].GetStop(), intervals[i + 1].GetStart())) {
         string message = Form("Overlapping intervals %d (stop=%s) and %d (start=%s)",
                               i, (intervals[i].GetStop()).c_str(),
                               i + 1, (intervals[i + 1].GetStart()).c_str());
         if (is_abort)
            TUMessages::Error(f, "TUDatime", "IntervalsCheckTimeOrderedNonOverlapping", message);
         else {
            TUMessages::Warning(f, "TUDatime", "IntervalsCheckTimeOrderedNonOverlapping", message);
            return false;
         }
      }
   }
   return true;
}

//______________________________________________________________________________
void TUDatime::IntervalsExtract(TUDatime const &user_datime, vector<TUDatime> &intervals, vector<Int_t> &indices_new, FILE *f)
{
   //--- Returns the list of time intervals, by merging an existing list with new intervals
   //    calculated from the overlap of a user define interval 'user_datime' and the
   // previous intervals. Note that the intervals are time ordered, and that the
   // 'stop' of an interval is before or at the date of the following 'start' interval.
   // INPUTS:
   //  user_datime       User time interval to consider
   //  intervals         List of non-overlapping time-ordered intervals
   //  f                 Output file for chatter
   // OUTPUTS:
   //  intervals         New list of non-overlapping time-ordered intervals
   //  indices_new       List of indices corresponding to new intervals added in intervals


   indices_new.clear();

   // If no previous intervals, use user_datime as only interval!
   if (intervals.size() == 0) {
      intervals.push_back(user_datime);
      indices_new.push_back(0);
   }


   // Check that user_datime is not ill-formed
   if (!user_datime.IsStartBeforeStop()) {
      string message = "Variable user_datime is ill-formed: start="
                       + user_datime.GetStart() + " > stop="
                       + user_datime.GetStop();
      TUMessages::Error(f, "TUDatime", "IntervalsExtract", message);
   }


   // Check that time intervals are time-ordered, non-overlapping, not ill-defined
   IntervalsCheckTimeOrderedNonOverlapping(intervals, true, f);


   // Find index of intervals encompassing user_datime;
   Int_t n = intervals.size();
   Int_t i_min = n - 1, i_max = 0;
   Bool_t is_start_in_or_after = true;
   Bool_t is_stop_in_or_before = true;
   for (Int_t i = 0; i < n; ++i) {
      // Search start interval
      if (!IsStartBeforeStop(user_datime.GetStart(), intervals[i].GetStart()))
         i_min = i;
      else if (i == 0)
         i_min = -1;
      if (IsStartBeforeStop(user_datime.GetStart(), intervals[i].GetStop()))
         is_start_in_or_after = true;
      else
         is_start_in_or_after = false;

      // Search stop interval
      if (IsStartBeforeStop(intervals[i].GetStop(), user_datime.GetStop())) {
         i_max = i + 1;
         if (i == n - 1)
            i_max = n;
      }
      if (IsStartBeforeStop(user_datime.GetStop(), intervals[i].GetStart()))
         is_stop_in_or_before = false;
      else
         is_stop_in_or_before = true;
   }
   // cout << "     i_min=" << i_min << "  i_max=" << i_max << endl;

   // If user interval in an interval, nothing to do
   if (i_min >= i_max)
      return;

   // Loop on all intervals:
   //   - intervals outside above ones must be accounted for [not new];
   //   - intervals insides above ones must be accounted for [not new];
   //   - intervals in-between above ones must be added [new]
   TUDatime tmp;
   // Add all existing and new intervals in i_min,i_max
   for (Int_t i = i_min; i < i_max; ++i) {
      if (i == -1) {
         // if i_min==-1, user-start before first interval!
         tmp.SetStart(user_datime.GetStart());
         if (i_max == 0 && !is_stop_in_or_before)
            tmp.SetStop(user_datime.GetStop());
         else
            tmp.SetStop(intervals[0].GetStart());
         indices_new.push_back(0);
         intervals.insert(intervals.begin(), tmp);
         i_max++;
         n++;
         i++;
      } else if (i == n - 1) {
         // if i_max=n, user-stop after last interval!
         tmp.SetStop(user_datime.GetStop());
         if (i_min == n - 1 && !is_start_in_or_after)
            tmp.SetStart(user_datime.GetStart());
         else
            tmp.SetStart(intervals[n - 1].GetStop());
         indices_new.push_back(n);
         intervals.push_back(tmp);
         i_max++;
         n++;
         i++;
      } else {
         // all new intervals must be inserted
         if (i == i_min && !is_start_in_or_after)
            tmp.SetStart(user_datime.GetStart());
         else if (i == i_max && !is_start_in_or_after)
            tmp.SetStop(user_datime.GetStop());
         else {
            if (!tmp.IsStartBeforeStop(intervals[i].GetStop(), user_datime.GetStart()))
               tmp.SetStart(intervals[i].GetStop());
            else
               tmp.SetStart(user_datime.GetStart());

            if (!tmp.IsStartBeforeStop(user_datime.GetStop(), intervals[i + 1].GetStart()))
               tmp.SetStop(intervals[i + 1].GetStart());
            else
               tmp.SetStop(user_datime.GetStop());
         }
         indices_new.push_back(i + 1);
         intervals.insert(intervals.begin() + i + 1, tmp);
         i_max++;
         n++;
         i++;
      }
   }
}

//______________________________________________________________________________
void TUDatime::IntervalsMerge(vector<TUDatime> const &intervals, vector<TUDatime> &merged) const
{
   //--- Merge list of time intervals into minimal set of non-overlapping time-ordered intervals
   // INPUT:
   //  intervals         List of intervals
   // OUTPUTS:
   //  merged           List of merged intervals (non-overlapping time-ordered)

   // Check that intervals are not ill-formed (i.e. start>stop!)
   Int_t n = intervals.size();
   for (Int_t i = 0; i < n; ++i) {
      if (!intervals[i].IsStartBeforeStop()) {
         string message = Form("Interval %d is ill-formed: start=%s > stop=%s",
                               i, (intervals[i].GetStart()).c_str(), (intervals[i].GetStop()).c_str());
         TUMessages::Error(stdout, "TUDatime", "IntervalsMerge", message);
      }
   }

   // Time-order by start datime
   merged = intervals;
   sort(merged.begin(), merged.end(), *this);

   // Stores index of last element in output merged (modified arr[])
   Int_t index = 0;
   // Traverse all input Intervals
   for (Int_t i = 0; i < n; i++) {
      // If this is not first Interval and overlaps with the previous one
      if (index != 0 && !merged[0].IsStartBeforeStop(merged[index - 1].GetStop(), merged[i].GetStart())) {
         while (index != 0 && !merged[0].IsStartBeforeStop(merged[index - 1].GetStop(), merged[i].GetStart())) {
            // Merge previous and current Intervals
            merged[index - 1].Set(max(merged[index - 1].GetStop(), merged[i].GetStop()), false);
            merged[index - 1].Set(min(merged[index - 1].GetStart(), merged[i].GetStart()), true);
            index--;
         }
      } else // Doesn't overlap with previous, add to solution
         merged[index].Copy(merged[i]);
      index++;
   }
   merged.resize(index);
}

//______________________________________________________________________________
Bool_t TUDatime::IsLeapYear(Int_t year)
{
   //--- Is it a leap year or not?
   //  year          Year to test

   if (year % 4 != 0)
      return false;
   else if (year % 100 != 0)
      return true;
   else if (year % 400 != 0)
      return false;
   else
      return true;
}

//______________________________________________________________________________
Bool_t TUDatime::IsStartBeforeStop(string datime_start, string datime_stop)
{
   //--- Checks whether datime_start<datime_stop or not (format is YYYY-MM-DD HH:MM:SS).
   //  datime            Datime to set
   //  is_start          Whether we fill fStart or fStop

   Int_t date_start = atoi(TUMisc::RemoveChars(datime_start.substr(0, 10), "-").c_str());
   Int_t date_stop = atoi(TUMisc::RemoveChars(datime_stop.substr(0, 10), "-").c_str());

   Int_t time_start = atoi(TUMisc::RemoveChars(datime_start.substr(11, 8), ":").c_str());
   Int_t time_stop = atoi(TUMisc::RemoveChars(datime_stop.substr(11, 8), ":").c_str());

   if (date_start < date_stop || ((date_start == date_stop) && time_start < time_stop))
      return true;
   else
      return false;
}

//______________________________________________________________________________
ULong_t TUDatime::SecondsFromStartOfYear(Bool_t is_start) const
{
   //--- Calculates number of seconds elapsed from start of year till this datime.
   //  is_start          Whether we fill fStart or fStop

   Int_t year = GetYear(is_start);
   Int_t month = GetMonth(is_start);
   Int_t n_days = GetDay(is_start) - 1;

   // Calculate number of days in months elapsed
   for (Int_t i = 1; i < month; ++i)
      n_days += DaysInMonth(i, year);

   // Calculate numbers of seconds
   ULong_t n_seconds = ULong_t(n_days) * 86400;
   // Add seconds from current day
   ULong_t sec_left_in_day = GetHour(is_start) * 3600 + GetMinute(is_start) * 60 + GetSecond(is_start);
   n_seconds += sec_left_in_day;
   return n_seconds;
}

//______________________________________________________________________________
void TUDatime::Set(time_t const &raw, Bool_t is_start, Bool_t raw_utc_or_local)
{
   //--- Sets fStart (or fStop) from time_t.
   //  raw               Number of seconds since 00:00 hrs, Jan 1, 1970
   //  is_start          Whether we fill fStart or fStop
   //  raw_utc_or_local  Whether 'raw' is from UTC (true) or local time (false)

   struct tm *t;
   if (raw_utc_or_local)
      t = gmtime(&raw);
   else
      t = localtime(&raw);

   Set(*t, is_start);
}

//______________________________________________________________________________
void TUDatime::Set(struct tm const &raw, Bool_t is_start)
{
   //--- Sets fStart (or fStop) from struct tm.
   //  raw               Number of seconds since 00:00 hrs, Jan 1, 1970
   //  is_start          Whether we fill fStart or fStop

   // A date is "YYYY-MM-DD HH:MM:SS"
   string date = Form("%d-%02d-%02d %02d:%02d:%02d",
                      raw.tm_year + 1900, raw.tm_mon + 1, raw.tm_mday,
                      raw.tm_hour, raw.tm_min, raw.tm_sec);

   if (is_start)
      fStart = date;
   else
      fStop = date;
}

//______________________________________________________________________________
void TUDatime::Set(string const &datime, Bool_t is_start)
{
   //--- Sets fStart (or fStop) from STD datime string (YYYY-MM-DD HH:MM:SS)
   //  datime            Datime to set
   //  is_start          Whether we fill fStart or fStop

   // Check datime has the correct format
   CheckDatimeFormat(datime, true, true);
   if (is_start)
      fStart = datime;
   else
      fStop = datime;
}

//______________________________________________________________________________
void TUDatime::Set(string const &date, string const &time, Bool_t is_start)
{
   //--- Sets fStart (or fStop) from date (YYYY-MM-DD) and time (HH:MM:SS).
   //  date              Date
   //  time              Time
   //  is_start          Whether we fill fStart or fStop (format YYYY-MM-DD HH:MM:SS)

   // Check datime has the correct format
   string datime = date + " " + time;
   CheckDatimeFormat(datime, true, true);

   Set(datime, is_start);
}

//______________________________________________________________________________
void TUDatime::SubtractMonth(Bool_t is_start)
{
   //--- Subtract a month to start or stop date.
   //  is_start          Whether start or stop date

   Int_t year = GetYear(is_start);
   Int_t month = GetMonth(is_start);
   Int_t day = GetDay(is_start);

   if (month == 1) {
      month = 12;
      year -= 1;
   } else
      month -= 1;

   string new_datime = Form("%4d-%02d-%02d %s", year, month, day, GetTime().c_str());
   Set(new_datime, is_start);
}

//______________________________________________________________________________
void TUDatime::TEST(FILE *f)
{
   //--- Test function for this class (called in usine -t).
   //  f                 Output file for test

   string message = "Stores start-stop datimes (date+time) @ 1s precision)";
   TUMessages::Test(f, "TUDatime", message);

   /*****************************/
   /*   Array-based description  */
   /*****************************/

   // Set class and test methods
   fprintf(f, " * Check single datime\n");
   fprintf(f, "   > Set(datime=1950-03-24 08:45:12, is_start=true); PrintInterval(f);\n");
   Set("1950-03-24 08:45:12", true);
   PrintInterval(f);
   fprintf(f, "   > Set(date=1950-03-24, time=08:45:12, is_start=true); PrintInterval(f);\n");
   Set("1950-03-24", "08:45:12", true);
   PrintInterval(f);
   fprintf(f, "   > SetStartStop(start=1950-03-24 08:45:12, stop=1990-03-24 08:45:12); PrintInterval(f);\n");
   SetStartStop("1950-03-24 08:45:12", "1990-03-24 08:45:12");
   PrintInterval(f);
   fprintf(f, "\n");

   // Uncomment the following for 'real' today date!
   //fprintf(f, "   > time_t today; time(today);\n");
   //time_t today;
   //time(&today);
   fprintf(f, "   > time_t today = 1483271549;\n");
   time_t today = 1483271549;
   fprintf(f, "   > Set(t_today, is_start = false, utc_or_local = true); PrintInterval(f);\n");
   Set(today, false, true);
   PrintInterval(f);
   fprintf(f, "   > Set(t_today, is_start = false, utc_or_local = false); PrintInterval(f);\n");
   Set(today, false, false);
   PrintInterval(f);
   fprintf(f, "   > struct tm *ptm = gmtime(t_today);\n");
   struct tm *ptm = gmtime(&today);
   fprintf(f, "   > Set(ptm, is_start = true); PrintInterval(f);\n");
   Set(*ptm, true);
   PrintInterval(f);
   fprintf(f, "   > today =  GetDatime_t(true);\n");
   today =  GetDatime_t(true);
   fprintf(f, "   > Set(t_today, is_start = true, utc_or_local = true); PrintInterval(f);\n");
   Set(today, true, true);
   PrintInterval(f);
   fprintf(f, "   > *ptm =  GetDatime_tm(false);\n");
   *ptm =  GetDatime_tm(false);
   fprintf(f, "   > Set(*ptm, is_start = false); PrintInterval(f);\n");
   Set(*ptm, false);
   PrintInterval(f);
   fprintf(f, "\n");


   fprintf(f, " * Print using GetYear(), GetMonth(), GetDay(), "
           "GetHour(), GetMinute(), GetSecond()\n");
   fprintf(f, "Year=%02d Month=%02d Day=%02d ; Hour=%02d Min=%02d Sec=%02d\n", GetYear(),
           GetMonth(), GetDay(), GetHour(), GetMinute(), GetSecond());
   fprintf(f, "\n");

   fprintf(f, " * Check TStart-TStop\n");
   fprintf(f, "   > Set(date=1971-06-10, time=22:12:21, is_start=true);\n");
   Set("1971-06-10", "22:12:21", true);
   PrintInterval(f);
   fprintf(f, "   > GetDate(is_start=true);  =>  %s\n", GetDate(true).c_str());
   fprintf(f, "   > GetDate(is_start=false); =>  %s\n", GetDate(false).c_str());
   fprintf(f, "   > GetTime(is_start=true);  =>  %s\n", GetTime(true).c_str());
   fprintf(f, "   > GetTime(is_start=false); =>  %s\n", GetTime(false).c_str());
   fprintf(f, "   > GetStart();              =>  %s\n", GetStart().c_str());
   fprintf(f, "   > GetStop();               =>  %s\n", GetStop().c_str());
   fprintf(f, "   > IntervalSec();       =>  %lu (s)\n", IntervalSec());
   fprintf(f, "\n");
   fprintf(f, "   > GetStart() => %s\n", GetStart().c_str());
   fprintf(f, "   > GetStop()  => %s\n", GetStop().c_str());
   fprintf(f, "   > time_t t  = GetDatime_t(true/false); Set(datime, true/false);\n");
   time_t t = GetDatime_t(true);
   Set(t, true);
   t = GetDatime_t(false);
   Set(t, false);
   fprintf(f, "   > GetStart() => %s\n", GetStart().c_str());
   fprintf(f, "   > GetStop()  => %s\n", GetStop().c_str());
   fprintf(f, "\n");

   fprintf(f, "\n");
   fprintf(f, " * Check Copy\n");
   fprintf(f, "   > TUDatime datime;\n");
   TUDatime datime;
   fprintf(f, "   > datime.SetStartStop(\"2010-11-14 02:05:59\", \"2012-05-10 00:00:01\")\n");
   datime.SetStartStop("2010-11-14 02:05:59", "2012-05-10 00:00:01");
   fprintf(f, "   > datime.PrintInterval(f);\n");
   datime.PrintInterval(f);
   fprintf(f, "   > Copy(datime);\n");
   Copy(datime);
   fprintf(f, "   > PrintInterval(f);\n");
   PrintInterval(f);
   fprintf(f, "   > IsStartBeforeStop() = %d   IsStartEqualStop()=%d\n", IsStartBeforeStop(), IsStartEqualStop());
   fprintf(f, "   > SetStartStop(\"2010-09-14 00:00:00\", \"2010-10-14 00:00:00\")\n");
   SetStartStop("2010-09-14 00:00:00", "2010-10-14 00:00:00");
   fprintf(f, "   > IsStartBeforeStop() = %d   IsStartEqualStop()=%d\n", IsStartBeforeStop(), IsStartEqualStop());
   fprintf(f, "   > SetStartStop(\"2010-10-14 00:00:00\", \"2010-10-14 00:00:00\")\n");
   SetStartStop("2010-10-14 00:00:00", "2010-10-14 00:00:00");
   fprintf(f, "   > IsStartBeforeStop() = %d   IsStartEqualStop()=%d\n", IsStartBeforeStop(), IsStartEqualStop());
   fprintf(f, "   > SetStartStop(\"2010-11-14 00:00:00\", \"2010-11-14 00:01:00\")\n");
   SetStartStop("2010-11-14 00:00:00", "2010-11-14 00:01:00");
   fprintf(f, "   > IsStartBeforeStop() = %d   IsStartEqualStop()=%d\n", IsStartBeforeStop(), IsStartEqualStop());
   fprintf(f, "\n");
   PrintInterval(f);
   fprintf(f, "   > AddMonth(is_start=true);\n");
   AddMonth(true);
   PrintInterval(f);
   fprintf(f, "   > AddMonth(is_start=true);\n");
   AddMonth(true);
   PrintInterval(f);
   fprintf(f, "   > AddMonth(is_start=false);\n");
   AddMonth(false);
   PrintInterval(f);
   fprintf(f, "   > SubtractMonth(is_start=true);\n");
   SubtractMonth(true);
   PrintInterval(f);
   fprintf(f, "   > SubtractMonth(is_start=true);\n");
   SubtractMonth(true);
   PrintInterval(f);
   fprintf(f, "   > SubtractMonth(is_start=false);\n");
   SubtractMonth(false);
   PrintInterval(f);
   fprintf(f, " * Check AddSec()\n");
   PrintDatime(f);
   fprintf(f, "   > AddSec(30);\n");
   AddSec(30);
   PrintDatime(f);
   fprintf(f, "   > AddSec(61);\n");
   AddSec(61);
   PrintDatime(f);
   fprintf(f, "   > AddSec(3601);\n");
   AddSec(3601);
   PrintDatime(f);
   fprintf(f, "   > AddSec(86401);\n");
   AddSec(86401);
   PrintDatime(f);
   fprintf(f, "   > AddSec(2678401);\n");
   AddSec(2678401);
   PrintDatime(f);

   fprintf(f, "\n");
   fprintf(f, "\n");
   fprintf(f, "   > vector<TUDatime> intervals; => Fill with values\n");
   vector<TUDatime> intervals;
   // Fill intervals
   TUDatime tmp;
   tmp.SetStartStop("2001-10-00 00:00:00", "2003-06-00 00:00:00");
   intervals.push_back(tmp);
   tmp.SetStartStop("2010-06-00 00:00:00", "2010-09-00 00:00:00");
   intervals.push_back(tmp);
   tmp.SetStartStop("2010-11-00 00:00:00", "2012-07-00 00:00:00");
   intervals.push_back(tmp);
   tmp.SetStartStop("1997-10-00 00:00:00", "1999-06-00 00:00:00");
   intervals.push_back(tmp);
   tmp.SetStartStop("1986-03-00 00:00:00", "1986-06-00 00:00:00");
   intervals.push_back(tmp);
   tmp.SetStartStop("1992-03-00 00:00:00", "1992-06-00 00:00:00");
   intervals.push_back(tmp);
   tmp.SetStartStop("2004-12-00 00:00:00", "2006-08-00 00:00:00");
   intervals.push_back(tmp);
   tmp.SetStartStop("2001-01-00 00:00:00", "2001-04-00 00:00:00");
   intervals.push_back(tmp);
   tmp.SetStartStop("1991-05-00 00:00:00", "1991-08-00 00:00:00");
   intervals.push_back(tmp);
   tmp.SetStartStop("2001-12-00 00:00:00", "2003-08-00 00:00:00");
   intervals.push_back(tmp);
   for (Int_t i = 0; i < (Int_t)intervals.size(); ++i)
      fprintf(f, "     - Interval %d: %s\n", i, (intervals[i].GetStartStop(" ; ", false)).c_str());
   fprintf(f, "   > IntervalsCheckTimeOrderedNonOverlapping(intervals, is_abort=false, f_out=f);\n");
   IntervalsCheckTimeOrderedNonOverlapping(intervals, false, f);

   fprintf(f, "\n");
   fprintf(f, "   > IntervalsSort(intervals);\n");
   IntervalsSort(intervals);
   for (Int_t i = 0; i < (Int_t)intervals.size(); ++i)
      fprintf(f, "     - Interval %d: %s\n", i, (intervals[i].GetStartStop(" ; ", false)).c_str());
   fprintf(f, "   > IntervalsCheckTimeOrderedNonOverlapping(intervals, is_abort=false, f_out=f);\n");
   IntervalsCheckTimeOrderedNonOverlapping(intervals, false, f);

   fprintf(f, "\n");
   vector<TUDatime> merged;
   fprintf(f, "   > IntervalsMerge(intervals, vector<TUDatime> &merged);\n");
   IntervalsMerge(intervals, merged);
   for (Int_t i = 0; i < (Int_t)merged.size(); ++i)
      fprintf(f, "     - Interval %d: %s\n", i, (merged[i].GetStartStop(" ; ", false)).c_str());

   fprintf(f, "\n");
   TUDatime user_datime;
   user_datime.SetStart("1920-01-01 00:00:00");
   fprintf(f, "   > IndexClosest(intervals, 1920-01-01 00:00:00) = %d\n", IndexClosest(merged, user_datime));
   for (Int_t i = 0; i < (Int_t)merged.size(); ++i)
      fprintf(f, "   > IndexClosest(intervals, intervals[%d]) = %d\n", i, IndexClosest(merged, merged[i]));
   user_datime.SetStart("1988-01-01 00:00:00");
   fprintf(f, "   > IndexClosest(intervals, 1988-01-01 00:00:00) = %d\n", IndexClosest(merged, user_datime));
   user_datime.SetStart("1992-01-01 00:00:00");
   fprintf(f, "   > IndexClosest(intervals, 1992-01-01 00:00:00) = %d\n", IndexClosest(merged, user_datime));
   user_datime.SetStart("2010-10-01 00:00:00");
   fprintf(f, "   > IndexClosest(intervals, 2010-10-01 00:00:00) = %d\n", IndexClosest(merged, user_datime));
   user_datime.SetStart("2020-01-01 00:00:00");
   fprintf(f, "   > IndexClosest(intervals, 2020-01-01 00:00:00) = %d\n", IndexClosest(merged, user_datime));


   fprintf(f, "\n");
   fprintf(f, "\n");
   fprintf(f, " * Common \'datimes\' lists\n");
   fprintf(f, "   > vector<vector<TUDatime> > datimes_list; => Fill with values\n");
   vector<vector<TUDatime> > datimes_list;
   vector<TUDatime> datime_tmp;
   fprintf(f, "  List 1:\n");
   tmp.SetStart("1993-04-00 00:00:00");
   datime_tmp.push_back(tmp);
   tmp.SetStart("1994-03-00 00:00:00");
   datime_tmp.push_back(tmp);
   tmp.SetStart("1998-08-00 00:00:00");
   datime_tmp.push_back(tmp);
   tmp.SetStart("2005-03-00 00:00:00");
   datime_tmp.push_back(tmp);
   tmp.SetStart("2010-06-00 00:00:00");
   datime_tmp.push_back(tmp);
   for (Int_t i = 0; i < (Int_t)datime_tmp.size(); ++i)
      datime_tmp[i].PrintDatime(f);
   datimes_list.push_back(datime_tmp);
   datime_tmp.clear();

   fprintf(f, "  List 2:\n");
   tmp.SetStart("1994-03-00 00:00:00");
   datime_tmp.push_back(tmp);
   tmp.SetStart("1997-07-00 00:00:00");
   datime_tmp.push_back(tmp);
   tmp.SetStart("2001-08-00 00:00:00");
   datime_tmp.push_back(tmp);
   tmp.SetStart("2003-06-00 00:00:00");
   datime_tmp.push_back(tmp);
   tmp.SetStart("2007-10-00 00:00:00");
   datime_tmp.push_back(tmp);
   tmp.SetStart("2012-02-00 00:00:00");
   datime_tmp.push_back(tmp);
   for (Int_t i = 0; i < (Int_t)datime_tmp.size(); ++i)
      datime_tmp[i].PrintDatime(f);
   datimes_list.push_back(datime_tmp);
   datime_tmp.clear();

   fprintf(f, "  List 3:\n");
   tmp.SetStart("1985-06-00 00:00:00");
   datime_tmp.push_back(tmp);
   tmp.SetStart("1988-10-00 00:00:00");
   datime_tmp.push_back(tmp);
   tmp.SetStart("1992-03-00 00:00:00");
   datime_tmp.push_back(tmp);
   tmp.SetStart("1999-12-00 00:00:00");
   datime_tmp.push_back(tmp);
   tmp.SetStart("2014-09-00 00:00:00");
   datime_tmp.push_back(tmp);
   for (Int_t i = 0; i < (Int_t)datime_tmp.size(); ++i)
      datime_tmp[i].PrintDatime(f);
   datimes_list.push_back(datime_tmp);
   datime_tmp.clear();

   fprintf(f, "  List 4:\n");
   tmp.SetStart("1986-10-00 00:00:00");
   datime_tmp.push_back(tmp);
   tmp.SetStart("1988-03-00 00:00:00");
   datime_tmp.push_back(tmp);
   tmp.SetStart("2001-08-00 00:00:00");
   datime_tmp.push_back(tmp);
   tmp.SetStart("2005-05-00 00:00:00");
   datime_tmp.push_back(tmp);
   tmp.SetStart("2005-09-00 00:00:00");
   datime_tmp.push_back(tmp);
   for (Int_t i = 0; i < (Int_t)datime_tmp.size(); ++i)
      datime_tmp[i].PrintDatime(f);
   datimes_list.push_back(datime_tmp);
   datime_tmp.clear();

   fprintf(f, "   > vector<TUDatime> ttt = CommonDatimes(vector<vector<TUDatime> > const &datimes_list);\n");
   vector<TUDatime> ttt = CommonDatimes(datimes_list);
   for (Int_t i = 0; i < (Int_t)ttt.size(); ++i)
      ttt[i].PrintDatime(f);


   // Test different interval combinations
   fprintf(f, "\n");
   fprintf(f, "\n");
   fprintf(f, "   > TUDatime user_datime;\n");
   vector<Int_t> indices_new;
   //
   fprintf(f, "\n");
   fprintf(f, "   => user_data no overlap with intervals: [1986-03-00,1987-12-31]\n");
   user_datime.SetStartStop("1986-03-00 00:00:00", "1987-12-31 00:00:00");
   fprintf(f, "   > IntervalsExtract(user_datime, intervals, vector<Int_t> &indices_new);\n");
   intervals = merged;
   IntervalsExtract(user_datime, intervals, indices_new);
   for (Int_t i = 0; i < (Int_t)intervals.size(); ++i) {
      if (find(indices_new.begin(), indices_new.end(), i) != indices_new.end())
         fprintf(f, "     - Interval %d: %s  [NEW]\n", i, (intervals[i].GetStartStop(" ; ", false)).c_str());
      else
         fprintf(f, "     - Interval %d: %s\n", i, (intervals[i].GetStartStop(" ; ", false)).c_str());
   }
   //
   fprintf(f, "\n");
   fprintf(f, "   => user_data no overlap with intervals: [2030-01-01,2030-12-31]\n");
   user_datime.SetStartStop("2030-01-01 00:00:00", "2030-12-31 00:00:00");
   fprintf(f, "   > IntervalsExtract(user_datime, intervals, vector<Int_t> &indices_new);\n");
   intervals = merged;
   IntervalsExtract(user_datime, intervals, indices_new);
   for (Int_t i = 0; i < (Int_t)intervals.size(); ++i) {
      if (find(indices_new.begin(), indices_new.end(), i) != indices_new.end())
         fprintf(f, "     - Interval %d: %s  [NEW]\n", i, (intervals[i].GetStartStop(" ; ", false)).c_str());
      else
         fprintf(f, "     - Interval %d: %s\n", i, (intervals[i].GetStartStop(" ; ", false)).c_str());
   }
   //
   fprintf(f, "\n");
   fprintf(f, "   => user_data no overlap with intervals: between interval 4 and 5\n");
   user_datime.SetStartStop(intervals[4].GetStop(), intervals[5].GetStart());
   user_datime.PrintInterval(f);
   fprintf(f, "   > IntervalsExtract(user_datime, intervals, vector<Int_t> &indices_new);\n");
   intervals = merged;
   IntervalsExtract(user_datime, intervals, indices_new);
   for (Int_t i = 0; i < (Int_t)intervals.size(); ++i) {
      if (find(indices_new.begin(), indices_new.end(), i) != indices_new.end())
         fprintf(f, "     - Interval %d: %s  [NEW]\n", i, (intervals[i].GetStartStop(" ; ", false)).c_str());
      else
         fprintf(f, "     - Interval %d: %s\n", i, (intervals[i].GetStartStop(" ; ", false)).c_str());
   }
   //i_min
   fprintf(f, "\n");
   fprintf(f, "   => user_data full overlap with intervals 4\n");
   user_datime.SetStartStop(intervals[4].GetStart(), intervals[4].GetStop());
   user_datime.PrintInterval(f);
   fprintf(f, "   > IntervalsExtract(user_datime, intervals, vector<Int_t> &indices_new);\n");
   intervals = merged;
   IntervalsExtract(user_datime, intervals, indices_new);
   for (Int_t i = 0; i < (Int_t)intervals.size(); ++i) {
      if (find(indices_new.begin(), indices_new.end(), i) != indices_new.end())
         fprintf(f, "     - Interval %d: %s  [NEW]\n", i, (intervals[i].GetStartStop(" ; ", false)).c_str());
      else
         fprintf(f, "     - Interval %d: %s\n", i, (intervals[i].GetStartStop(" ; ", false)).c_str());
   }
   //
   fprintf(f, "\n");
   fprintf(f, "   => partial overlap: [1998-01-01,2005-12-31]\n");
   user_datime.SetStartStop("1998-01-01 00:00:00", "2005-12-31 00:00:00");
   fprintf(f, "   > IntervalsExtract(user_datime, intervals, vector<Int_t> &indices_new);\n");
   intervals = merged;
   IntervalsExtract(user_datime, intervals, indices_new);
   for (Int_t i = 0; i < (Int_t)intervals.size(); ++i) {
      if (find(indices_new.begin(), indices_new.end(), i) != indices_new.end())
         fprintf(f, "     - Interval %d: %s  [NEW]\n", i, (intervals[i].GetStartStop(" ; ", false)).c_str());
      else
         fprintf(f, "     - Interval %d: %s\n", i, (intervals[i].GetStartStop(" ; ", false)).c_str());
   }
   //
   fprintf(f, "\n");
   fprintf(f, "   => encompass overlap: [1973-01-01,2025-12-31]\n");
   user_datime.SetStartStop("1973-01-01 00:00:00", "2025-12-31 00:00:00");
   fprintf(f, "   > IntervalsExtract(user_datime, intervals, vector<Int_t> &indices_new);\n");
   intervals = merged;
   IntervalsExtract(user_datime, intervals, indices_new);
   for (Int_t i = 0; i < (Int_t)intervals.size(); ++i) {
      if (find(indices_new.begin(), indices_new.end(), i) != indices_new.end())
         fprintf(f, "     - Interval %d: %s  [NEW]\n", i, (intervals[i].GetStartStop(" ; ", false)).c_str());
      else
         fprintf(f, "     - Interval %d: %s\n", i, (intervals[i].GetStartStop(" ; ", false)).c_str());
   }
}
