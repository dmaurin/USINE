// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

// C/C++ include
// ROOT include
// USINE include
#include "../include/TUSrcTemplates.h"

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TUSrcTemplates                                                       //
//                                                                      //
// Storage class for source templates (name, values, free parameters).  //
//                                                                      //
// The class TUSrcTemplates is a template class for the description of  //
// possible CR sources. The term 'source' is to be taken in a broad     //
// sense, i.e., it can correspond to a spectrum (depending on energy),  //
// or a spatial distribution (depending on spatial coordinates). The    //
// use of these 'templates' in the comprehensive description of a CR    //
// source (spectral shape and spatial distribution for many species)    //
// is done for instance in TUSrcMultiCRs.                               //
//                                                                      //
// The simple and useful methods of the class are:                      //
//    - SetClass(): add all source provided in the USINE
//      initialisation file in the list of templates;
//    - Copy(), Clone(), and Create(), GetSrc(), and GetSrcFreePars().
//                                                                      //
// N.B.: formula-based templates are associated with free parameters.   //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

ClassImp(TUSrcTemplates)

//______________________________________________________________________________
TUSrcTemplates::TUSrcTemplates()
{
   // ****** Default constructor ******

   Initialise(false);
}

//______________________________________________________________________________
TUSrcTemplates::TUSrcTemplates(TUSrcTemplates const &src_templ)
{
   // ****** Copy constructor ******

   Initialise(false);
   Copy(src_templ);
}

//______________________________________________________________________________
TUSrcTemplates::~TUSrcTemplates()
{
   // ****** Default destructor ******

   Initialise(true);
}

//______________________________________________________________________________
void TUSrcTemplates::Copy(TUSrcTemplates const &src_templ)
{
   //--- Copies in class.
   //  src_templ                  Object to copy from

   Initialise(true);

   fNTemplates = src_templ.GetNTemplates();
   fTemplates = new TUValsTXYZEVirtual*[fNTemplates];
   for (Int_t i = 0; i < GetNTemplates(); ++i) {
      fIsSpectrumOrSpatDist.push_back(src_templ.IsSpectrumOrSpatDist(i));
      TUValsTXYZEVirtual *tmp = src_templ.GetSrc(i);
      if (tmp)
         fTemplates[i] = tmp->Clone();
      else
         fTemplates[i] = NULL;
      tmp = NULL;
   }
}

//______________________________________________________________________________
Int_t TUSrcTemplates::IndexSrcTemplate(string const &src_templ, Bool_t is_spectrum_or_spatdist) const
{
   //--- Returns index found for 'template' (spectrum or spatial distribution).
   //
   //  src_templ                Name of the template (user-defined)
   //  is_spectrum_or_spatdist  Whether this is a spectrum or spatial dist.

   // Template search must be case insensitive
   string name = src_templ;
   TUMisc::UpperCase(name);

   // Loop on all entries (templates)
   for (Int_t i = 0; i < (Int_t)fIsSpectrumOrSpatDist.size(); ++i) {

      // Template must be of similar stuff (spectrum or spatial distribution)
      if (is_spectrum_or_spatdist != IsSpectrumOrSpatDist(i))
         continue;

      // Find matching names
      if (name == GetTemplateName(i))
         return i;
   }
   return -1;
}

//______________________________________________________________________________
void TUSrcTemplates::Initialise(Bool_t is_delete)
{
   //--- Initialises and free memory of object members.
   //  is_delete         Whether to delete or just initialise

   if (is_delete) {
      if (fTemplates) {
         for (Int_t i = 0; i < GetNTemplates(); ++i) {
            if (fTemplates[i]) delete fTemplates[i];
            fTemplates[i] = NULL;
         }
         delete[] fTemplates;
      }
   }
   fTemplates = NULL;
   fNTemplates = 0;
   fIsSpectrumOrSpatDist.clear();
}

//______________________________________________________________________________
void TUSrcTemplates::PrintSrcTemplate(FILE *f, Int_t i_templ) const
{
   //--- Prints in file f the 'i_templ'-th src template.
   //  f              File in which to print
   //  i_templ        Index of source template

   string templ_name;
   if (IsSpectrumOrSpatDist(i_templ))
      templ_name = "Energy spectrum template: ";
   else
      templ_name = "Spatial distribution template: ";

   templ_name += GetTemplateName(i_templ);
   fTemplates[i_templ]->PrintSummary(f, templ_name);
}

//______________________________________________________________________________
void TUSrcTemplates::PrintSrcTemplates(FILE *f) const
{
   //--- Prints in file f the 'i_templ'-th src template.
   //  f              File in which to print

   // Print spectral templates first
   for (Int_t i = 0; i < GetNTemplates(); ++i)
      if (IsSpectrumOrSpatDist(i))
         PrintSrcTemplate(f, i);

   // Print spatial distribution templates second
   for (Int_t i = 0; i < GetNTemplates(); ++i)
      if (!IsSpectrumOrSpatDist(i))
         PrintSrcTemplate(f, i);
}

//______________________________________________________________________________
void TUSrcTemplates::SetClass(TUInitParList *init_pars, Bool_t is_verbose, FILE *f_log)
{
   //--- Sets templates found in 'init_pars': a template belongs to
   //       GROUP = TemplSpectrum
   //       GROUP = TemplSpatialDist
   //    for which the parameters ParNames, ParUnits, and Description are mandatory.
   //    N.B.: for this class, each SUBGROUP corresponds to the template name.
   //
   //  init_pars         TUInitParList object of initialisation parameters
   //  is_verbose        Verbose or not when class is set
   //  f_log             Log for USINE

   string indent = TUMessages::Indent(true);
   if (is_verbose)
      fprintf(f_log, "%s[TUSrcTemplates::SetClass]\n", indent.c_str());

   Initialise(true);

   const Int_t n_gr = 2;
   string group[n_gr] = {"TemplSpectrum", "TemplSpatialDist"};

   // Spatial distribution generically depends on "t,x,y,z"
   string vars_spacetime = "t,x,y,z";
   // Spectrum generically depends on "t,x,y,z,Edep-vars", where Edep-var are
   // energy variables (beta, gamma, p, R, ...) defined in the class TUAxesCrE
   TUAxesCrE dummy;
   dummy.LoadEVarsNamesAndUnits(is_verbose, f_log);
   string vars_spacetime_edep = vars_spacetime + "," + dummy.GetEVarNames();

   if (is_verbose)
      fprintf(f_log, "%s### Add template functions declared in %s\n",
              indent.c_str(), init_pars->GetFileNames().c_str());

   // Loop on template groups (find number of templates)
   fNTemplates = 0;
   for (Int_t i = 0; i < n_gr; ++i)
      fNTemplates += init_pars->GetNSubGroups(init_pars->IndexGroup(group[i]));

   // Loop on template groups (fill templates)
   fTemplates = new TUValsTXYZEVirtual*[fNTemplates];
   Int_t i_src = 0;
   for (Int_t i = 0; i < n_gr; ++i) {
      Int_t i_gr = init_pars->IndexGroup(group[i]);
      for (Int_t i_sub = 0; i_sub < init_pars->GetNSubGroups(i_gr); ++i_sub) {
         string templ_name = init_pars->GetNameSubGroup(i_gr, i_sub);
         if (is_verbose)
            fprintf(f_log, "%s   - Add template %s in class list\n",
                    indent.c_str(), templ_name.c_str());

         // Set free parameters in TUFreePars object
         TUFreeParList *free_pars = new TUFreeParList();
         free_pars->AddPars(init_pars, group[i], templ_name, false, f_log);

         Bool_t is_spectrum_or_spatdist = true;
         if (i == 1) is_spectrum_or_spatdist = false;

         // Check whether Description is formula or read file (and retrieve formula or file name)
         Int_t i_definition = init_pars->IndexPar(group[i], templ_name, "Definition");
         string formula_or_file = "";
         gENUM_FCNTYPE format = init_pars->ExtractFormat(i_definition, 0, formula_or_file, is_verbose, f_log);
         if (format == kFORMULA) {
            fTemplates[i_src] = new TUValsTXYZEFormula();
            if (is_spectrum_or_spatdist)
               fTemplates[i_src]->SetClass(formula_or_file, is_verbose, f_log, vars_spacetime_edep, free_pars, false);
            else
               fTemplates[i_src]->SetClass(formula_or_file, is_verbose, f_log, vars_spacetime, free_pars, false);
         } else if (format == kGRID) {
            fTemplates[i_src] = new TUValsTXYZEGrid();
            fTemplates[i_src]->SetClass(formula_or_file, is_verbose, f_log);
         } else if (format == kSPLINE) {
            TUMessages::Error(f_log, "TUTransport", "SetClass", "Spline not implemented yet for this function");
         }

         // Set name
         string name = templ_name;
         TUMisc::UpperCase(name);
         fTemplates[i_src]->SetName(name);

         // Check that template to add does not already exist
         Int_t i_check = IndexSrcTemplate(templ_name, is_spectrum_or_spatdist);
         if (i_check >= 0) {
            string message = "Template " + templ_name + " already in list, nothing to do!";
            TUMessages::Warning(f_log, "TUSrcTemplates", "SetClass", message);
            return;
         }
         fIsSpectrumOrSpatDist.push_back(is_spectrum_or_spatdist);
         ++i_src;

         // Free memory (src is just reset to NULL, but free pars must be deleted)
         delete free_pars;
         free_pars = NULL;
      }
   }

   if (is_verbose)
      fprintf(f_log, "%s[TUSrcTemplates::SetClass] <DONE>\n\n", indent.c_str());

   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUSrcTemplates::TEST(TUInitParList *init_pars, FILE *f)
{
   //--- Test function for this class (called in usine -t).
   //  init_pars   TUInitParList class of initialisation parameters
   //  f           Output file for test

   string message = "Source templates formulae or grid values (spectrum and spatial distribution)";
   TUMessages::Test(f, "TUSrcTemplates", message);


   // Set class and test methods
   fprintf(f, " * Base class:\n");
   fprintf(f, "   > SetClass(init_pars=\"%s\", is_verbose=true, f_log=f);\n", init_pars->GetFileNames().c_str());
   SetClass(init_pars, true, f);
   fprintf(f, "\n");
   fprintf(f, "   > PrintSrcTemplates(f);\n");
   PrintSrcTemplates(f);
   fprintf(f, "\n");

   fprintf(f, " * Copy class:\n");
   fprintf(f, "   > TUSrcTemplates src_templ;\n");
   TUSrcTemplates src_templ;
   fprintf(f, "   > src_templ.Copy(*this);\n");
   src_templ.Copy(*this);
   fprintf(f, "   > src_templ.PrintSrcTemplates(f);\n");
   src_templ.PrintSrcTemplates(f);
   fprintf(f, "\n");
}
