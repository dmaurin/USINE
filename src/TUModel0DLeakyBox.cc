// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

// C/C++ include
// ROOT include
// USINE include
#include "../include/TUInteractions.h"
#include "../include/TUMath.h"
#include "../include/TUModel0DLeakyBox.h"
#include "../include/TUNumMethods.h"

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TUModel0DLeakyBox                                                    //
//                                                                      //
// Leaky-Box Model for propagation (includes energy losses and reac.).  //
//                                                                      //
// The LB model is a 0D model (steady-state, no space coordinates). It  //
// is said to be semi-analytical because the solution without E gains   //
// or losses is algebraic, the general equation being 2nd order in E    //
// (solved using an explicit numerical method). Note that LBM should    //
// never be used for electrons and positrons, or for radioactive        //
// species.                                                             //
//                                                                      //
// See the online documentation for more details and references.        //
//////////////////////////////////////////////////////////////////////////

ClassImp(TUModel0DLeakyBox)

//______________________________________________________________________________
TUModel0DLeakyBox::TUModel0DLeakyBox() : TUModelBase()
{
   // ****** Default constructor ******

   Initialise(false);
}

//______________________________________________________________________________
TUModel0DLeakyBox::~TUModel0DLeakyBox()
{
   // ****** Default destructor ******

   Initialise(true);
}

//______________________________________________________________________________
Double_t TUModel0DLeakyBox::Aterm(Int_t j_cr, Int_t k_ekn)
{
   //--- Returns Aterm = 1/tau_esc + v*Sum_ISM{ni.sigtoti} + gamma_decay  [/Myr].
   //  j_cr              Index of CR
   //  k_ekn             Index of energy

   Double_t a = 1. / ValueTauEsc_Myr(j_cr, k_ekn);

   // If inelastic interaction
   if (TUPropagSwitches::IsDestruction())
      a += TUXSections::Get_nvSigTotIA_ISM0D(j_cr, k_ekn);

   // If BETA decay
   if (TUPropagSwitches::IsDecayBETA() && TUCRList::GetCREntry(j_cr).IsUnstable(true))
      a += TUAxesCrE::GammaRadBETA_perMyr(j_cr, k_ekn);

   // If EC decay (TO DO)

   return a;
}

//______________________________________________________________________________
Bool_t TUModel0DLeakyBox::CalculateChargedCRs(Int_t jcr_start, Int_t jcr_stop, Bool_t is_verbose, FILE *f_log)
{
   //--- Computes differential density dNdEkn^j (j=(anti-) nuclei index) corresponding
   //    to the propagated differential density in the LB model (returns true if this function exist).
   //  jcr_start         Index of CR for first to calculate
   //  jcr_stop          Index of CR for last to calculate
   //  is_verbose        Verbose or not when class is set
   //  f_log             Log for USINE
   //
   //    For each nucleus j, the differential equation for N^j with E losses (B^j),
   //    gains (Ci^j), and source term (S^j) reads (see class documentation):
   //       A^j(Ekn)*N^j(Ekn) + d/dEtot( B^j*N^j - C^j dN^j/dEtot ) = S^j(Ekn),
   //    which is recast into the more-convenient form (we drop j, the CR index)
   //       N + (fCoefPrefactor * d/dLnEkn(fCoef1stOrder*N -  fCoef2ndOrderHalfBin * dN/dLnEkn) = Sterm/A
   //    with
   //       fCoefPrefactor = 1/(A*Ek)
   //       fCoef1stOrder  = B
   //       fCoef2ndOrderHalfBin  = C/Ek
   //    N.B.: if neither losses nor energy gains (i.e., B^j=C^j=0), dNdEkn=Sterm;
   //          otherwise a numerical inversion is required
   //    N.B.: if for some reason, some parameters of the models have been changed
   //          (e.g. lambda_esc), do not forget to update FillCoefPreFactorAndATerm()
   //
   //    In the following, Sterm^j is the sum of various contributions that includes
   //       (1) Secondary contributions
   //       (2) Primary standard contributions
   //       (3) BETA-radioactive contribution
   //       (4) EC-radioactive contribution [TODO]
   //    N.B.: We chose [Sterm^j]=[N0] = (GeV/n)^{-1}.m^{-3}

   string indent = TUMessages::Indent(true);

   // Check indices to run
   if (jcr_start < 0)
      jcr_start = 0;
   if (jcr_stop > GetNCRs() - 1)
      jcr_stop = GetNCRs() - 1;
   if (jcr_start > jcr_stop)
      TUMessages::Warning(f_log, "TUModel2DKisoVc", "CalculateChargedCRs", "jcr_start>jcr_stop, nothing to do");

   // local verbose (for checks only)
   Bool_t is_check = false;
   if (is_verbose && is_check)
      fprintf(f_log, "%sTUModel0DLeakyBox::CalculateChargedCRs\n", indent.c_str());

   // Allocate source term
   Double_t *sterm_tot = new Double_t[GetNE()];
   Double_t *sterm_prim = new Double_t[GetNE()];

   // Loop on CRs (heaviest to lightest)
   for (Int_t j_cr = jcr_stop; j_cr >= jcr_start; --j_cr) {
      // If BETA rad parent for j_cr (and BETA decay mode
      // switched on), store its index for further usage
      Int_t j_radbeta_parent = -1;
      if (TUPropagSwitches::IsDecayFedBETA())
         j_radbeta_parent = TUCRList::IndexParentBETA(j_cr);


      /*****************************/
      /* A. Calculate source terms */
      /*****************************/
      //         Sterm = (Q_prim+Q_sec+Q_rad) / Aterm = sterm / Aterm
      // [/(GeV/n m3)] =   [/(GeV/n m3 Myr)]  /  [/Myr]
      // N.B.: in practice we calculate sterm, and divide by Aterm
      // after having added all contributions (in order to get Sterm)

      // Initialise [Sterm] = [dNdEkn] = (GeV/n)^{-1}.m^{-3}
      // first used as [sterm] = (GeV/n)^{-1}.m^{-3} / Myr
      for (Int_t k_ekn = GetNE() - 1; k_ekn >= 0; --k_ekn) {
         sterm_tot[k_ekn] = 0.;
         sterm_prim[k_ekn] = 0.;
      }


      // A(1) Secondary contrib. (parent+targ->j_cr)
      //-------------------------------------------
      if (TUPropagSwitches::IsSecondaries()) {
         // N.B.: valid whether straight-ahead approx. or differential production
         //     sterm_sec =  Q_sec
         // [/(GeV/n m3 Myr)] = [/(GeV/n m3 Myr)]

         // Loop on list of parents (for j_cr)
         for (Int_t k_in_lop = TUCRList::GetNParents(j_cr) - 1; k_in_lop >= 0; --k_in_lop) {
            // If only zeros in X-sec, skip it!Eps
            if (TUModelBase::IsProdNull(k_in_lop, j_cr))
               continue;

            // Adds in sterm_tot secondary production from dndekn_proj
            Double_t *dndekn_proj = GetdNdEknTot(TUCRList::IndexInCRList_Parent(j_cr, k_in_lop));
            TUXSections::SecondaryProduction(dndekn_proj, k_in_lop, j_cr, fISMEntry,
                                             sterm_tot, GetEpsIntegr(), true /*is_ism0D*/, f_log);
            dndekn_proj = NULL;
         }
      }


      // All other contributions (within an ekn loop)
      for (Int_t k_ekn = GetNE() - 1; k_ekn >= 0; --k_ekn) {
         // A(2) Primary contrib.
         //---------------------
         if (TUPropagSwitches::IsPrimStandard() && !TUAxesCrE::IsPureSecondary(j_cr)) {
            Double_t src_contrib = 0.;

            // Loop on all available astrophysical sources
            // N.B.: normalisation for source spectrum must be in [/(GeV/n m3 Myr)]
            for (Int_t s_src = 0; s_src < TUModelBase::GetNSrcAstro(); ++s_src) {

               // Skip if j_cr is not present in the s_src-th astro source
               if (!TUModelBase::IsExistSrc(s_src, j_cr, true))
                  continue;

               // Update energy coordinate and calculate spectrum
               TUModelBase::UpdateCoordE(j_cr, k_ekn);
               src_contrib += TUModelBase::ValueSrcAstroSpectrum(s_src, j_cr, TUModelBase::GetCoordE());
            }
            // Add in total (all contribs) and prim (primary contrib only)
            // N.B.: we divide by Aterm after having added all contributions
            sterm_tot[k_ekn] += src_contrib;
            sterm_prim[k_ekn] += src_contrib;
            //printf("CR[%d]=%s  Ekn[%d]=%le  sterm_prim=%le\n", j_cr, GetCREntry(j_cr).GetName().c_str(), k_ekn, TUAxesCrE::GetEkn(j_cr, k_ekn), src_contrib);
         }

         // A(3) BETA-rad. parent contrib.
         //-------------------------------
         //         sterm_rad =  dN/dE * Gamma_rad
         // [/(GeV/n m3 Myr)] =  [/(GeV/n m3)] * [/Myr]
         if (j_radbeta_parent != -1)
            sterm_tot[k_ekn] += GetdNdEknTot(j_radbeta_parent, k_ekn)
                                * TUAxesCrE::GammaRadBETA_perMyr(j_radbeta_parent, k_ekn);

         // A(4) EC-rad. parent contrib.
         //-----------------------------
         // TO DO


         // All contributions are now added:
         //         Sterm =   sterm   /  Aterm
         // [/(GeV/n m3)] = [/(GeV/n m3 Myr)] / [/Myr]
         sterm_prim[k_ekn] /= GetATerm(j_cr, k_ekn);
         sterm_tot[k_ekn] /= GetATerm(j_cr, k_ekn);
         //printf("CR[%d]=%s  Ekn[%d]=%le  sterm_prim/Aterm=%le\n", j_cr, GetCREntry(j_cr).GetName().c_str(), k_ekn, TUAxesCrE::GetEkn(j_cr, k_ekn), sterm_tot[k_ekn]);
      }


      /***********************************/
      /* B. Solve dNdEkn for source term */
      /***********************************/
      InvertForELossGain(j_cr, fISMEntry, GetCoefPreFactor(j_cr), sterm_tot, sterm_prim, fInvertScheme, GetdNdEknTot(j_cr), GetdNdEknPrim(j_cr));


      /*********************************/
      /* C. Add tertiaries (iterative) */
      /*********************************/
      if (TUPropagSwitches::IsTertiaries() && TUXSections::IsTertiary(j_cr))
         IterateTertiariesdNdEkn(j_cr, sterm_tot, sterm_prim, GetEpsIterConv(), is_verbose, f_log);
   }

   // Free memory
   delete[] sterm_tot;
   sterm_tot = NULL;
   delete[] sterm_prim;
   sterm_prim = NULL;
   if (is_verbose && is_check)
      fprintf(f_log, "%sTUModel0DLeakyBox::CalculateChargedCRs <DONE>\n", indent.c_str());

   TUMessages::Indent(false);
   return true;
}

//______________________________________________________________________________
Double_t TUModel0DLeakyBox::CoeffReac1stOrder(Int_t j_cr, Int_t k_ekn)
{
   //--- Returns first order term from reacceleration.
   //  j_cr              Index of CR
   //  k_ekn             Index of energy (here running from -1 to NE)

   if (k_ekn >= 0 && k_ekn < GetNE())
      return (1. + GetBeta(j_cr, k_ekn) * GetBeta(j_cr, k_ekn)) * ValuePseudoKpp(j_cr, k_ekn) / GetEtot(j_cr, k_ekn);
   else {
      Double_t ekn = TUAxesCrE::GetE(j_cr)->EvalOutOfRange(k_ekn);
      Double_t beta = TUPhysics::Beta_mAEkn(TUAxesCrE::GetCREntry(j_cr).GetmGeV(), TUAxesCrE::GetCREntry(j_cr).GetA(), ekn);
      TUModelBase::UpdateCoordE(j_cr, ekn);
      Double_t kxx = TUModelBase::ValueK(TUModelBase::GetCoordE());
      Double_t tau_esc = kxx / (fISMEntry->MeanMassDensity() * beta * TUPhysics::C_cmperMyr());
      Double_t pseudo_kpp = TUModelBase::ValueKpp(TUModelBase::GetCoordE()) * tau_esc;
      Double_t etot = TUPhysics::Ekn_to_E(ekn, TUAxesCrE::GetCREntry(j_cr).GetA(), TUAxesCrE::GetCREntry(j_cr).GetmGeV());
      return
         (1. + beta * beta) * pseudo_kpp / etot;
   }
}

//______________________________________________________________________________
Double_t TUModel0DLeakyBox::CoeffReac2ndOrderLowerHalfBin(Int_t j_cr, Int_t k_ekn)
{
   //--- Returns second order term from reacceleration: evaluated at lower half-bin.
   //  j_cr              Index of CR
   //  k_ekn             Index of energy (here running from 0 to NE)

   if (k_ekn != GetNE())  // Return at k_ekn-1/2
      return TUAxesCrE::GetBeta(j_cr, k_ekn, -1) * TUAxesCrE::GetBeta(j_cr, k_ekn, -1)
             * ValuePseudoKpp(j_cr, k_ekn, -1) / TUAxesCrE::GetEk(j_cr, k_ekn, -1);
   else  // Return at NE-1/2 = (NE-1) + 1/2
      return TUAxesCrE::GetBeta(j_cr, GetNE() - 1, 1) * TUAxesCrE::GetBeta(j_cr, GetNE() - 1, 1)
             * ValuePseudoKpp(j_cr, GetNE() - 1, 1) / TUAxesCrE::GetEk(j_cr, GetNE() - 1, 1);
}

//______________________________________________________________________________
Double_t TUModel0DLeakyBox::EvalFluxPrim(Int_t j_cr, Int_t k_ekn, TUCoordTXYZ *coord_txyz)
{
   //--- Returns from differential density the primary flux in model [/GeV/s/cm2/sr].
   //  j_cr              Index of CR
   //  k_ekn             Index of energy
   //  coord_txyz       Unused in this model (no geometry)

   Bool_t is_verbose = false;
   if (coord_txyz && is_verbose)
      TUMessages::Warning(stdout, "TUModel0DLeakyBox", "EvalFluxPrim", "Position passed is unused (no geometry)");

   return GetdNdEknPrim(j_cr, k_ekn) * GetBeta(j_cr, k_ekn)
          * TUPhysics::C_mpers() / (4.*TUMath::Pi());
}

//______________________________________________________________________________
Double_t TUModel0DLeakyBox::EvalFluxTot(Int_t j_cr, Int_t k_ekn, TUCoordTXYZ *coord_txyz)
{
   //--- Returns from differential density the total flux in model [/GeV/s/cm2/sr].
   //  j_cr              Index of CR
   //  k_ekn             Index of energy
   //  coord_txyz       Unused in this model (no geometry)

   Bool_t is_verbose = false;
   if (coord_txyz && is_verbose)
      TUMessages::Warning(stdout, "TUModel0DLeakyBox", "EvalFluxTot", "Position passed is unused (no geometry)");

   return GetdNdEknTot(j_cr, k_ekn) * GetBeta(j_cr, k_ekn)
          * TUPhysics::C_mpers() / (4.*TUMath::Pi());
}

//______________________________________________________________________________
void TUModel0DLeakyBox::FillCoefPreFactorAndATerm()
{
   //--- Fills fCoefPreFactor=1/(Aterm*Ek) [Myr/GeV] and ATerm [/Myr].

   // Allocate memory
   if (fCoefPreFactor) delete[] fCoefPreFactor;
   fCoefPreFactor = new Double_t[GetNE()*GetNCRs()];

   if (fATerm) delete[] fATerm;
   fATerm = new Double_t[GetNE()*GetNCRs()];


   // Loop on all CRs to fill fATerm and fCoefPreFactor
   for (Int_t j_cr = GetNCRs() - 1; j_cr >= 0; --j_cr) {
      for (Int_t k_ekn = GetNE() - 1; k_ekn >= 0; --k_ekn) {
         TUModelBase::UpdateCoordE(j_cr, k_ekn, 1);
         if (j_cr == -1)
            printf("CR[%d]=%s  Ekn[%d]=%.3le  1/tau_esc=%.3le  nvsig=%.3le  rad=%.3le  aterm(=1/tau+nvs+rad)=%.3le    %d %s\n",
                   j_cr, GetCREntry(j_cr).GetName().c_str(), k_ekn, TUAxesCrE::GetEkn(j_cr, k_ekn),
                   1 / ValueTauEsc_Myr(j_cr, k_ekn), TUXSections::Get_nvSigTotIA_ISM0D(j_cr, k_ekn),
                   TUAxesCrE::GammaRadBETA_perMyr(j_cr, k_ekn), Aterm(j_cr, k_ekn),
                   TUModelBase::GetCoordE()->GetHalfBinStatus(), TUModelBase::GetCoordE()->FormValsE().c_str());

         SetATerm(j_cr, k_ekn, Aterm(j_cr, k_ekn));
         Double_t factor = 1. / (GetATerm(j_cr, k_ekn) * TUAxesCrE::GetEk(j_cr, k_ekn));
         SetCoefPreFactor(j_cr, k_ekn, factor);
      }
   }
}

//______________________________________________________________________________
void TUModel0DLeakyBox::Initialise(Bool_t is_delete)
{
   //--- Initialises members specific to this model.
   //    N.B.: fISMEntry never needs to be deleted, because it relies on
   //    TUModelBase::fISM which is created and deleted elsewhere.
   //
   //  is_delete         Whether to delete or just initialise

   if (is_delete) {
      if (fATerm) delete[] fATerm;
      if (fCoefPreFactor) delete[] fCoefPreFactor;
      if (fdNdEknTot) delete[] fdNdEknTot;
      if (fdNdEknPrim) delete[] fdNdEknPrim;
   }

   fATerm = NULL;
   fCoefPreFactor = NULL;
   fdNdEknTot = NULL;
   fdNdEknPrim = NULL;
   fInvertScheme = kTRID;
   fISMEntry = NULL;
}

//______________________________________________________________________________
void TUModel0DLeakyBox::InitialiseForCalculation(Bool_t is_init_or_update, Bool_t is_force_update, Bool_t is_verbose, FILE *f_log)
{
   //--- Initialises and/or update (optimised or not) model members and values
   //    required for model calculation.
   //
   //  is_init_or_update Whether initialise class or update (look for changed parameters)
   //  is_force_update   If is_init_or_update=false (update), force to to it even if no parameter changed (useful in a few occasions)
   //  is_verbose        Verbose or not
   //  f_log             Log for USINE

   string indent = TUMessages::Indent(true);

   // Model-specific quantities: only for initialisation
   if (is_init_or_update) {
      if (is_verbose) {
         fprintf(f_log, "%s[TUModel0DLeakyBox::InitialiseForCalculation]\n", indent.c_str());
         fprintf(f_log, "%s   - Allocate fdNdEknTot[NCRs*NE] and fdNdEknPrim[NCRs*NE]\n", indent.c_str());
      }
      // Set TXYZ axes
      TUModelBase::GetAxesTXYZ()->UpdateFromFreePars();
      if (TUModelBase::GetAxesTXYZ()->GetFreePars())
         TUModelBase::GetAxesTXYZ()->GetFreePars()->ResetStatusParsAndParList();


      // Allocate memory for dNdEkn coeffs.
      if (fdNdEknTot) delete[] fdNdEknTot;
      fdNdEknTot = new Double_t[GetNE()*GetNCRs()];
      if (fdNdEknPrim)  delete[] fdNdEknPrim;
      fdNdEknPrim = new Double_t[GetNE()*GetNCRs()];
      // And initialise
      for (Int_t i = 0; i < GetNE()*GetNCRs(); ++i) {
         fdNdEknTot[i] = 0.;
         fdNdEknPrim[i] = 0.;
      }

      // If initialisation, propagation is mandatory (set to not propagated)
      TUModelBase::SetPropagated(false);
   } else {
      // If update, check whether model is updated
      TUModelBase::TUFreeParList::UpdateParListStatusFromIndividualStatus();
      if (TUModelBase::TUFreeParList::GetParListStatus())
         TUModelBase::SetPropagated(false);
   }

   // Update par list status for all parameters
   TUModelBase::UpdateAllParListStatusFromIndividualParStatus();

   // ISM: initialisation, update (if ISM was modified), or force update
   if (is_init_or_update || TUModelBase::GetFreeParsStatusISM() || is_force_update) {

      // Set fISMEntry to TUMediumTXYZ entry
      if (is_verbose) {
         if (is_init_or_update)
            fprintf(f_log, "%s   - Set fISMEntry (0D) from TUMediumTXYZ\n", indent.c_str());
         else
            fprintf(f_log, "%s   - Update fISMEntry (0D)\n", indent.c_str());
      }

      TUModelBase::GetISM()->UpdateFromFreeParsAndResetStatus();
      TUCoordTXYZ *dummy_txyz = NULL;
      TUMediumTXYZ *medium = TUModelBase::GetISM();
      medium->FillMediumEntry(dummy_txyz);
      fISMEntry = medium->GetMediumEntry();
      medium = NULL;
      if (is_init_or_update && is_verbose)
         fISMEntry->Print(f_log);

      // Fill or update nuclear rates (because ISM was changed)
      if (is_verbose) {
         if (is_init_or_update)
            fprintf(f_log, "%s   - Fill nuclear rates (optimised for constant density ISM)\n", indent.c_str());
         else
            fprintf(f_log, "%s   - Update nuclear rates (optimised for constant density ISM)\n", indent.c_str());
      }
      TUXSections::FillInterRate_ISM0D(fISMEntry, true);
   }


   // XSection: initialisation, update (if XS were modified), or force update
   if (is_init_or_update || TUModelBase::GetFreeParsStatusXSections() || is_force_update) {

      // Set/Update XS from free parameters
      if (is_verbose) {
         if (!is_init_or_update)
            fprintf(f_log, "%s   - Update XSections and nuclear rates (optimised for constant density ISM)\n", indent.c_str());
      }
      TUModelBase::TUXSections::ParsXS_UpdateFromFreeParsAndResetStatus(fISMEntry);

      // We have to force update for the remaining parameters (coeff to calculate are related to rates)
      is_force_update = true;
   }


   // Transport: initialisation, update (if transport or half-lives were modified), or force update
   if (is_init_or_update || TUModelBase::GetFreeParsStatusTransport()
         || TUModelBase::GetFreeParsStatusHalfLives() || is_force_update) {

      if (is_verbose) {
         if (is_init_or_update)
            fprintf(f_log, "%s   - Fill pre-factor terms\n", indent.c_str());
         else
            fprintf(f_log, "%s   - Update pre-factor terms\n", indent.c_str());
      }
      TUModelBase::GetTransport()->UpdateFromFreeParsAndResetStatus();
      TUModelBase::TUAxesCrE::TUCRList::ParsHalfLife_UpdateFromFreeParsAndResetStatus();
      FillCoefPreFactorAndATerm();
   }


   // Sources: only for update (if sources were modified)
   if (!is_init_or_update) {
      for (Int_t i = 0; i < GetNSrcAstro(); ++i)
         TUModelBase::GetSrcAstro(i)->UpdateFromFreeParsAndResetStatus();
      for (Int_t i = 0; i < GetNSrcDM(); ++i)
         TUModelBase::GetSrcDM(i)->UpdateFromFreeParsAndResetStatus();
   }


   // Propagation switches: just reset status
   TUPropagSwitches::SetStatus(false);
   // Update and reset model parameters
   TUModelBase::ResetStatusParsAndParList();
   if (is_init_or_update && is_verbose)
      fprintf(f_log, "%s[TUModel0DLeakyBox::InitialiseForCalculation] <DONE>\n", indent.c_str());

   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUModel0DLeakyBox::IterateTertiariesdNdEkn(Int_t j_cr, Double_t *sterm_tot, Double_t *sterm_prim,
      Double_t const &eps, Bool_t is_verbose, FILE *f_log)
{
   //--- Solves iteratively E redistribution from inel. non-annihil. rescattering (INA).
   //    With INA iterations, the diffusion equation becomes an integro-differential
   //    equation (2nd order derivative in E, one integration for E redistribution).
   //    The integral term involves the equilibrium spectrum, which is calculated
   //    iteratively (convergence is reached after a couple of iterations in practice).
   //    The formula below is slightly changed in practise (see TUXSections::TertiaryProduction)
   //    to perform the calculation on a log-scale energy:
   //       - step 1: calculate tertiary contrib. (over ISM components) from current dN/dEkn:BEGIN_LATEX(fontsize=14, separator='=', align='lcl')
   //          S_{ter} = #frac{Q_{ter}}{A_{term}}, with
   //          Q_{ter}(E) = #int_{E}^{#infty} n v' #frac{d#sigma^{INA}_{CR+ISM -> CR+X+ISM}}{dE} (E' -> E) #frac{dN}{dEkn}(E')dE' - nv#sigma^{INA}_{CR+ISM -> CR+X+ISM} (E) #frac{dN}{dEkn}(E)
   //END_LATEX
   //       - step 2: calculate new equilibrium spectra dN/dEkn (adding tertiary term)
   //       - step 3: go back to step 1 and iterate with new dN/dEkn until convergence reached
   //
   // INPUTS:
   //  j_cr              CR index
   //  sterm_tot         Source term (prim+sec contribs.) before tertiaries
   //  sterm_prim        Source term (prim. only) before tertiaries
   //  eps               Precision sought before stopping iterations
   //  is_verbose        Chatter on or off
   //  f_log             Log for USINE
   // OUTPUTS:
   //  sterm_tot         Source term (prim+sec contribs.) including tertiaries
   //  sterm_prim        Source term (prim. only) including tertiaries


   // If j_cr not a tertiary, nothing to do
   if (!TUXSections::IsTertiary(j_cr))
      return;

   string indent = TUMessages::Indent(true);
   TUMessages::Indent(false);

   // Set maximum iteration number
   const Int_t n_iter_max = 100;

   // Allocate and initialise sterm (enable comparison before/after iteration)
   Double_t max_dev = 0.;
   Double_t *dnde_ref = new Double_t[TUModelBase::GetNE()];
   Double_t *stermtot_ter = new Double_t[TUModelBase::GetNE()];
   Double_t *stermtot_ter_old = new Double_t[TUModelBase::GetNE()];

   // Update new/old Sterm for primary contrib. only (if required)
   Double_t *stermprim_ter = NULL;
   Double_t *stermprim_ter_old = NULL;
   if (!TUAxesCrE::IsPureSecondary(j_cr)) {
      stermprim_ter = new Double_t[TUModelBase::GetNE()];
      stermprim_ter_old = new Double_t[TUModelBase::GetNE()];
      for (Int_t k = TUModelBase::GetNE() - 1; k >= 0; --k) {
         stermtot_ter[k] = 0.;
         stermprim_ter[k] = 0.;
         dnde_ref[k] = GetdNdEknTot(j_cr, k);
      }
   } else {
      for (Int_t k = TUModelBase::GetNE() - 1; k >= 0; --k) {
         stermtot_ter[k] = 0.;
         dnde_ref[k] = GetdNdEknTot(j_cr, k);
      }
   }

   // Iterate until convergence for any energy:
   //   |(N_{iter i+1} - N_{iter i})| / N_{iter i} < eps
   Int_t counter = 0;
   Int_t k_converged = 0;
   do {
      /******************************/
      /* Step 1: calculate tertiary */
      /******************************/
      ++counter;
      Int_t k_converged_iter = TUModelBase::GetNE() - 1;

      // Max difference after/before iteration (over all CR energies)
      max_dev = 0.;

      // Check convergence on total calculated flux: limiting convergence
      // is expected for higher energy points (less points to integrate
      // on tertiary production)
      for (Int_t k = TUModelBase::GetNE() - 1 ; k >= 0; --k) {
         // sterm_ter = Qterm/Aterm   [sterm_ter]=[sterm]= [dNdEkn]

         Bool_t is_converged = false;

         // Update new/old Sterm (primary and secondary contrib.)
         stermtot_ter_old[k] = stermtot_ter[k];
         stermtot_ter[k] = TUXSections::TertiaryProduction(GetdNdEknTot(j_cr), j_cr, k, fISMEntry, eps, true, is_converged)
                           / GetATerm(j_cr, k);

         Double_t delta = stermtot_ter[k] - stermtot_ter_old[k];
         Double_t rel_diff = 0.;
         if (sterm_tot[k] + delta < 0.)
            sterm_tot[k] = 0.;
         else {
            sterm_tot[k] += delta;
            // Check convergence of integration at this energy only if rel_dif>eps
            rel_diff = fabs((delta) / fabs(sterm_tot[k]));
         }
         // Check if converged (contrib to tertiary needs to be
         // significant and flux non null)
         //cout << "         k=" << k << "   is_converged=" << is_converged
         //     << "   sterm_tot[k]=" << sterm_tot[k] << "   rel_diff=" << rel_diff << endl;
         if (!is_converged && sterm_tot[k] > 1.e-40 && rel_diff > eps)
            k_converged_iter = k;

         // Keep only maximum difference to check overall convergence of this iteration
         max_dev = TMath::Max(max_dev, rel_diff);

         // Update new/old Sterm for primary contrib. only (if required)
         if (!TUAxesCrE::IsPureSecondary(j_cr)) {
            stermprim_ter_old[k] = stermprim_ter[k];
            stermprim_ter[k] = TUXSections::TertiaryProduction(GetdNdEknPrim(j_cr), j_cr, k, fISMEntry, eps, true, is_converged)
                               / GetATerm(j_cr, k);
            sterm_prim[k] += stermprim_ter[k] - stermprim_ter_old[k];
         }
      }
      /*********************************/
      /* Step 2: calculate new dN/dEkn */
      /*********************************/
      InvertForELossGain(j_cr, fISMEntry, GetCoefPreFactor(j_cr), sterm_tot, sterm_prim, fInvertScheme, GetdNdEknTot(j_cr), GetdNdEknPrim(j_cr));

      k_converged = max(k_converged, k_converged_iter);
      // cout << "****** iter=" << counter << "   k_converged_iter="
      //      << k_converged_iter << "   k_converged=" << k_converged << endl;

      // Stop if reached maximum allowed number of iterations
      if (counter > n_iter_max) break;

      // Stop if convergence reached
   } while (max_dev > eps);

   // Warning message if convergence of numerical integration in tertiary
   // contribution (that matters, i.e. for which the difference w/o tertiary
   // is larger than required precision) is not reached
   Int_t k_matters = TUModelBase::GetNE();
   for (Int_t k = TUModelBase::GetNE() - 1 ; k >= 0; --k) {
      if (dnde_ref[k] > 0. && fabs(dnde_ref[k] - GetdNdEknTot(j_cr, k)) / GetdNdEknTot(j_cr, k) > eps) {
         k_matters = k;
         break;
      }
   }

   if (k_converged < TUModelBase::GetNE() && k_matters > k_converged) {
      string message = Form("Precision eps=%f (for tertiary numerical integration)"
                            " not reached for %s for all Ekn in range [%.3le,%.3le] [GeV/n]",
                            eps, TUAxesCrE::GetCREntry(j_cr).GetName().c_str(), TUAxesCrE::GetEkn(j_cr, k_converged),
                            TUAxesCrE::GetEkn(j_cr, k_matters));
      TUMessages::Warning(f_log, "TUModel0DLeakyBox", "IterateTertiariesdNdEkn", string(message));
      fprintf(f_log, "\n");
   }

   if (is_verbose) {
      string qty = TUCRList::GetCREntry(j_cr).GetName();
      if (counter <= n_iter_max)
         fprintf(f_log, "%s    %s: tertiary contribution convergence (%.2le) reached after %d iterations\n",
                 indent.c_str(), qty.c_str(), eps, counter);
      else {
         string message = Form("%s%s: WARNING -- tertiary contribution HAS NOT CONVERGED after %d iterations",
                               indent.c_str(), qty.c_str(), counter);
         TUMessages::Warning(f_log, "TUModel1DKisoVc", "IterateTertiariesN0", string(message));
      }
   }

   // Free memory
   delete[] stermtot_ter;
   stermtot_ter = NULL;
   delete[] stermtot_ter_old;
   stermtot_ter_old = NULL;

   delete[] dnde_ref;
   dnde_ref = NULL;

   if (stermprim_ter)
      delete[] stermprim_ter;
   stermprim_ter = NULL;
   if (stermprim_ter_old)
      delete[] stermprim_ter_old;
   stermprim_ter_old = NULL;
}

//______________________________________________________________________________
Bool_t TUModel0DLeakyBox::SetClass_ModelSpecific(TUInitParList *init_pars, Bool_t is_verbose, FILE *f_log)
{
   //--- Set class members specific to model selected.
   //  init_pars         USINE initialisation parameters
   //  is_verbose        Verbose or not when class is set
   //  f_log             Log for USINE

   if (is_verbose) {
      string indent = TUMessages::Indent(true);
      fprintf(f_log, "%s[TUModel0DLeakyBox::SetClass_ModelSpecific]\n", indent.c_str());
      fprintf(f_log, "%s   => No model-specific action required to set the class\n", indent.c_str());
      fprintf(f_log, "%s[TUModel0DLeakyBox::SetClass_ModelSpecific] <DONE>\n\n", indent.c_str());
      TUMessages::Indent(false);
   }

   return true;
}

//______________________________________________________________________________
void TUModel0DLeakyBox::TEST(TUInitParList *init_pars, FILE *f)
{
   //--- Test function for this class (called in usine -t).
   //  init_pars         USINE initialisation parameters
   //  f                 Output file for test

   string message = "Load Leaky Box Model and test.";
   TUMessages::Test(f, "TUModel0DLeakyBox", message);

   // Set class and test methods
   fprintf(f, " * Load class\n");
   fprintf(f, "   > TUModelBase::SetClass(init_pars=\"%s\", Model0DLeakyBox, is_verbose=false, f_log=f);\n", init_pars->GetFileNames().c_str());
   TUModelBase::SetClass(init_pars, "Model0DLeakyBox", false, f);
   fprintf(f, "\n");

   fprintf(f, " * Basic prints\n");
   fprintf(f, "   > PrintSummaryBase(f)\n");
   PrintSummaryBase(f);
   fprintf(f, "   > PrintPars(f)\n");
   PrintPars(f);
   fprintf(f, "   > PrintSummaryModel(f)\n");
   PrintSummaryModel(f);
   fprintf(f, "   > PrintSummaryTransport(f)\n");
   PrintSummaryTransport(f);
   fprintf(f, "   > PrintSources(f, true)\n");
   PrintSources(f, true);
   fprintf(f, "\n");

   fprintf(f, "   > PrintSummary(f)\n");
   PrintSummary(f);
   fprintf(f, "\n");

   fprintf(f, "N.B.: run ./bin/usine to see the model in action!");
   fprintf(f, "\n");
}

//______________________________________________________________________________
Double_t TUModel0DLeakyBox::ValuePseudoKpp(Int_t j_cr, Int_t k_ekn, Int_t halfbin_status)
{
   //--- Returns PseudoKpp for a given CR and energy bin [GeV2/Myr].
   //    In the initialisation file for LB model, we have:
   //       K00_USINE = lambda_esc (= lambda0*Rig^(-delta)*beta^eta_t)  [g/cm2]
   //       kpp_USINE = (4./3.)*(kmpers_to_kpcperMyr*pseudoVa*p)^2/(delta*(4-delta^2)*(4-delta)) [GeV2/Myr2]
   //       with [pseudoVa] = km/s/kpc.
   //
   //    However, the LB formula is
   //       kpp_LB  =  kpp_USINE  *  tau_esc
   //      [GeV2/Myr] = [GeV2/Myr2] * [Myr]
   //
   //  j_cr              CR index
   //  k_ekn             Energy index
   //  halfbin_status    Evaluated at bine (0), bine-1/2 (-1), or bine+1/2 (+1)


   // Update E coordinate
   TUModelBase::UpdateCoordE(j_cr, k_ekn, halfbin_status);

   return TUModelBase::ValueKpp(TUModelBase::GetCoordE())
          * ValueTauEsc_Myr(j_cr, k_ekn, halfbin_status);
}

//______________________________________________________________________________
Double_t TUModel0DLeakyBox::ValueTauEsc_Myr(Int_t j_cr, Int_t k_ekn, Int_t halfbin_status)
{
   //--- Returns Tau escape [/Myr] for a given CR and energy.
   //  j_cr              Index of CR
   //  k_ekn             Index of energy
   //  halfbin_status    Evaluated at bine (0), bine-1/2 (-1), or bine+1/2 (+1)

   // Update E coordinate
   //Int_t halfbin_status = (is_halfupperbin ? 1 : 0);
   //TUModelBase::UpdateCoordE(j_cr, k_ekn, halfbin_status);

   TUModelBase::UpdateCoordE(j_cr, k_ekn, halfbin_status);

   // Tau_esc = Lambda_esc /  (v  *  <nm> )
   //  [/Myr] =  [g/cm^2]  / ([cm/Myr]*[g/cm^3])
   return TUModelBase::ValueK(TUModelBase::GetCoordE())
          / (fISMEntry->MeanMassDensity()
             * TUAxesCrE::GetVelocity_cmperMyr(j_cr, k_ekn, halfbin_status));
}
