// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

// C/C++ include
#include <cstdlib>
// ROOT include
// USINE include
#include "../include/TUMessages.h"
#include "../include/TUNumMethods.h"

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TUNumMethods                                                         //
//                                                                      //
// Encapsulate numerical methods (integration, crank-Nicholson...).     //
//                                                                      //
// The namespace TUNumMethods encapsulates simple numerical             //
// methods, which are taken (or adapted) from numerical recipes         //
// (http://www.nr.com/):                                                //
//    - Integration (fixed and adaptive step);
//    - Matrix Inversion (tridiagonal inversion or Gauss-Jordan pivot);
//    - Solve differential equations (Crank-Nicholson);
//////////////////////////////////////////////////////////////////////////

NamespaceImp(TUNumMethods)

void dummy_fn(Double_t &x, Double_t par[1], Double_t &res)
{
   par[0] = 0.;
   res = x * x;
}

//______________________________________________________________________________
Bool_t TUNumMethods::IntegrationSimpsonAdapt(TUAxis *axis, Double_t *integ1, Double_t *integ2,
      Int_t &kmin, Int_t &kmax, Double_t &s, Double_t const &eps, string const &classmethod,
      Bool_t is_verbose, Int_t j_min_dubling)
{
   //--- Linear- or log-step Simpson's rule integration for integrand=integ1*integ2
   //    (adapted from numerical recipes) on a grid "axis" (optimal if 2^M+1 points).
   //    N.B.: if j_min_dubling==-1, does not abort if convergence did not reach
   //    precision eps, but in that case returns false (otherwise returns true).
   // INPUTS:
   //  axis              Grid on which to integrate (lin- or log-step, depending on 'is_log')
   //  integ1            Array (on grid) to integrate is integ1*integ2
   //  integ2            Array (on grid) to integrate is integ1*integ2
   //  kmin              Index from which to start integration
   //  kmax              Index from which to stop integration
   //  eps               Precision sought for integration
   //  classmethod       Name of class::method (useful to trace method used)
   //  is_verbose        Chatter on or off
   //  j_min_doubling    Minimum range of doubling step: integrates at least on 2^(j+1) steps
   //                    If set to -1, returns the results (and false) if convergence not reached
   // OUTPUT:
   //  s                 Integration results

   Double_t ost = -1.e-40;
   Double_t os = -1.e-40;
   Double_t st = 0.;

   // N.B.: the number of steps must be 2^jmax+1 for the doubling adaptive
   // step to work. Thus, we need to check if we have the right number of
   // steps, otherwise, decrease kmax to get it.
   Int_t nk = kmax - kmin + 1;
   Int_t jmax = (Int_t)floor(log(Double_t(nk - 1)) / log(2.));
   Int_t nkok = (Int_t)pow(2., jmax) + 1;
   if (nk != nkok) {
      kmax = kmin + nkok - 1;
      string message = "use n=pow(2,j)+1 instead of " + string(Form("%d", axis->GetN()))
                       + " (for adaptive step integration): the upper integration bound "
                       + string(Form("%.3le", axis->GetVal(kmax))) + " is replaced by "
                       + string(Form("%.3le", axis->GetVal(kmax)))
                       + " to fulfil the above condition";
      TUMessages::Warning(stdout, "TUNumMethods", "IntegrationSimpsonAdapt", message);
   }


   // Specific case: warning and minimum number of doubling steps (in that case)
   Bool_t is_bypass_warning = false;
   if (j_min_dubling == -1) {
      is_bypass_warning = true;
      j_min_dubling = min(jmax, 3);
   }


   for (Int_t j = 1; j <= jmax; ++j) {
      IntegrationTrapezeRefine(axis, integ1, integ2, kmin, kmax, st, j);
      s = (4.*st - ost) / 3.;
      if ((fabs(s - os) < eps * fabs(os) || s < 1.e-30) && j >= j_min_dubling) {
         if (is_verbose) {
            string step_used = "(log-step)";
            if (axis->GetAxisType() == kLIN) step_used = "(lin-step)";
            cout << ">>>>> IntegrationSimpsonAdapt " + step_used
                 + ": convergence Dres/res<"
                 << eps << " reached after "
                 << j << " refinement (i.e. "
                 << pow(2., j) << " steps)" << endl;
         }
         // If result null, s=3.33e-41 from s = (4.*st - ost) / 3.;
         if (fabs(fabs(s) - 3.33333e-41) / 3.33333e-41 < 1.e-3)
            s = 0.;
         return true;
      }
      os = s;
      ost = st;
   }

   // If reach this point, means that the integration fails
   if (is_bypass_warning)
      return false;

   //cout << "  !!!!! X-AXIS AND INTEGRAND THAT FAILED TO REACH PRECISION" << endl;
   //for (Int_t i = kmin; i < kmax; ++i)
   //   cout << axis->GetVal(i) << " " << integ1[i]*integ2[i] << endl;
   string message = "Using " + (string)Form("%d", (kmax - kmin)) + " steps for integration from "
                    + classmethod + " is not enough to reach the desired precision eps="
                    + (string)Form("%.3le", eps) + ". Increase this number (it should be n=pow(2,j)+1)";
   TUMessages::Error(stdout, "TUNumMethods", "IntegrationSimpsonAdapt", message);
   return false;
}

//______________________________________________________________________________
Bool_t TUNumMethods::IntegrationSimpsonAdapt(TUAxis *axis, Double_t *integrand,
      Int_t &kmin, Int_t &kmax, Double_t &s, Double_t const &eps, string const &classmethod,
      Bool_t is_verbose, Int_t j_min_dubling)
{
   //--- Linear- or log-step Simpson's rule integration (adapted from numerical
   //    recipes) on a grid "axis" (optimal if 2^M+1 points). Returns false if
   //    convergence not reached!
   //    N.B.: if j_min_dubling==-1, does not abort if convergence did not reach
   //    precision eps, but in that case returns false (otherwise returns true).
   // INPUTS:
   //  axis              Grid on which to integrate (lin- or log-step, depending on 'is_log')
   //  integrand         Array (on grid) to integrate
   //  kmin              Index from which to start integration
   //  kmax              Index from which to stop integration
   //  eps               Precision sought for integration
   //  classmethod       Name of class::method (useful to trace method used)
   //  is_verbose        Chatter on or off
   //  j_min_doubling    Minimum range of doubling step: integrates at least on 2^(j+1) steps
   //                    If set to -1, returns the results (and false) if convergence not reached
   // OUTPUT:
   //  s                 Integration results

   Double_t ost = -1.e-40;
   Double_t os = -1.e-40;
   Double_t st = 0.;

   // N.B.: the number of steps must be 2^jmax+1 for the doubling adaptive
   // step to work. Thus, we need to check if we have the right number of
   // steps, otherwise, decrease kmax to get it.
   Int_t nk = kmax - kmin + 1;
   Int_t jmax = (Int_t)floor(log(Double_t(nk - 1)) / log(2.));
   Int_t nkok = (Int_t)pow(2., jmax) + 1;
   if (nk != nkok) {
      kmax = kmin + nkok - 1;
      string message = "use n=pow(2,j)+1 instead of " + string(Form("%d", axis->GetN()))
                       + " (for adaptive step integration): the upper integration bound "
                       + string(Form("%.3le", axis->GetVal(kmax))) + " is replaced by "
                       + string(Form("%.3le", axis->GetVal(kmax)))
                       + "to fulfil the above condition";
      TUMessages::Warning(stdout, "TUNumMethods", "IntegrationSimpsonAdapt", message);
   }

   // Specific case: warning and minimum number of doubling steps (in that case)
   Bool_t is_bypass_warning = false;
   if (j_min_dubling == -1) {
      is_bypass_warning = true;
      j_min_dubling = min(jmax, 3);
   }

   for (Int_t j = 1; j <= jmax; ++j) {
      IntegrationTrapezeRefine(axis, integrand, kmin, kmax, st, j);
      s = (4.*st - ost) / 3.;
      if ((fabs(s - os) < eps * fabs(os) || s < 1.e-30) && j >= j_min_dubling) {
         if (is_verbose) {
            string step_used = "(log-step)";
            if (axis->GetAxisType() == kLIN) step_used = "(lin-step)";
            cout << ">>>>> IntegrationSimpsonAdapt " + step_used
                 + ": convergence Dres/res<"
                 << eps << " reached after "
                 << j << " refinement (i.e. "
                 << pow(2., j) << " steps)" << endl;
         }
         // If result null, s=3.33e-41 from s = (4.*st - ost) / 3.;
         if (fabs(fabs(s) - 3.33333e-41) / 3.33333e-41 < 1.e-3)
            s = 0.;
         return true;
      }
      os = s;
      ost = st;
   }

   // If reach this point, means that the integration fails
   if (is_bypass_warning)
      return false;

   //cout << "  !!!!! X-AXIS AND INTEGRAND THAT FAILED TO REACH PRECISION" << endl;
   //for (Int_t i = kmin; i < kmax; ++i)
   //   cout << axis->GetVal(i) << " " << integrand[i] << endl;
   string message = "Using " + (string)Form("%d", (kmax - kmin)) + " steps for integration from "
                    + classmethod + " is not enough to reach the desired precision eps="
                    + (string)Form("%.3le", eps) + ". Increase this number (it should be n=pow(2,j)+1)";
   TUMessages::Error(stdout, "TUNumMethods", "IntegrationSimpsonAdapt", message);
   return false;
}

//______________________________________________________________________________
void TUNumMethods::IntegrationSimpsonAdapt(void (*fn)(Double_t &, Double_t *, Double_t &),
      Double_t &xmin, Double_t &xmax, Double_t par[], Double_t &s, Double_t const &eps,
      Bool_t is_log, Bool_t is_verbose)
{
   //--- Linear- or log-step Simpson's rule integration (adapted from numerical
   //    recipes) for a function fn.
   // INPUTS:
   //  fn                Prototype of the function to integrate
   //  xmin              Lower boundary integration
   //  xmax              Upper boundary integration
   //  par[]             Parameters of fn
   //  eps               Precision sought for integration
   //  is_log            Whether to use a log-step or a lin-step integration
   //  is_verbose        Chatter on or off
   // OUTPUT:
   //  s                 Integration results

   Double_t ost = -1.e-40;
   Double_t os = -1.e-40;
   Int_t jmax = 25;
   Double_t st = 0.;
   if (xmin >= xmax) {
      s = 0.;
      return;
   }
   for (Int_t j = 1; j <= jmax; ++j) {
      IntegrationTrapezeRefine(fn, xmin, xmax, par, st, is_log, j);
      s = (4.*st - ost) / 3.;
      if ((fabs(s - os) < eps * fabs(os) || s < 1.e-30) && j >= 3) {
         if (is_verbose) {
            string step_used = "(log-step)";
            if (!is_log) step_used = "(lin-step)";
            cout << ">>>>> IntegrationSimpsonAdapt " + step_used
                 + ": convergence Dres/res<"
                 << eps << " reached after "
                 << j << " refinement (i.e. "
                 << pow(2., j) << " steps)" << endl;
         }
         // If result null, s=3.33e-41 from s = (4.*st - ost) / 3.;
         if (fabs(fabs(s) - 3.33333e-41) / 3.33333e-41 < 1.e-3)
            s = 0.;
         return;
      }
      os = s;
      ost = st;
   }
   cout << ">>>>> Too many steps!" << endl;
   abort();
}

//______________________________________________________________________________
void TUNumMethods::IntegrationTrapezeRefine(TUAxis *axis, Double_t *integ1, Double_t *integ2,
      Int_t &kmin, Int_t &kmax, Double_t &s, Int_t n)
{
   //--- IntegrationTrapeze refinement step for integrand=integ1*integ2
   //    (called by Simpson, do not call it directly!).
   // INPUTS:
   //  axis              Grid on which to integrate (lin- or log-step, depending on 'is_log')
   //  integ1            Array (on grid) to integrate is integ1*integ2
   //  integ2            Array (on grid) to integrate is integ1*integ2
   //  kmin              Index from which to start integration
   //  kmax              Index from which to stop integration
   //  n                 Number of doubling steps
   // OUTPUT:
   //  s                 Integration results

   Int_t tnm = 0, del = 0, k = 0;
   Double_t sum = 0.;

   if (axis->GetAxisType() == kLOG) {
      if (axis->GetVal(kmin) <= 0.)
         TUMessages::Error(stdout, "TUNumMethods", "IntegrationTrapezeRefine",
                           ">>>>> log step with xmin=0!!!!");
      if (n == 1) {
         s += axis->GetVal(kmin) * integ1[kmin] * integ2[kmin];
         s += axis->GetVal(kmax) * integ1[kmax] * integ2[kmax];
         s *= 0.5 * log(axis->GetVal(kmax) / axis->GetVal(kmin));
      } else {
         tnm = (Int_t)pow(2., n - 2);
         del = (kmax - kmin) / tnm;
         k = kmin + del / 2;
         sum = 0.0;
         for (Int_t j = 1; j <= tnm; ++j) {
            sum += axis->GetVal(k) * integ1[k] * integ2[k];
            k += del;
         }
         s = 0.5 * (s +  log(axis->GetVal(kmax) / axis->GetVal(kmin))
                    * sum / (Double_t)tnm);
      }
   } else {
      if (n == 1) {
         s += (integ1[kmin] * integ2[kmin] + integ1[kmax] * integ2[kmax]);
         s *= 0.5 * (axis->GetVal(kmax) - axis->GetVal(kmin));
      } else {
         tnm = (Int_t) pow(2., n - 2);
         del = (kmax - kmin) / tnm;
         k = kmin + del / 2;
         sum = 0.0;
         for (Int_t j = 1; j <= tnm; ++j) {
            sum += integ1[k] * integ2[k];
            k += del;
         }
         s = 0.5 * (s + (axis->GetVal(kmax) - axis->GetVal(kmin))
                    / Double_t(tnm) * sum);
      }
   }
   return;
}

//______________________________________________________________________________
void TUNumMethods::IntegrationTrapezeRefine(TUAxis *axis, Double_t *integrand,
      Int_t &kmin, Int_t &kmax, Double_t &s, Int_t n)
{
   //--- IntegrationTrapeze refinement step (called by Simpson, do not call it directly!).
   // INPUTS:
   //  axis              Grid on which to integrate (lin- or log-step, depending on 'is_log')
   //  integrand         Array (on grid) to integrate
   //  kmin              Index from which to start integration
   //  kmax              Index from which to stop integration
   //  n                 Number of doubling steps
   // OUTPUT:
   //  s                 Integration results

   Int_t tnm = 0, del = 0, k = 0;
   Double_t sum = 0.;

   if (axis->GetAxisType() == kLOG) {
      if (axis->GetVal(kmin) <= 0.)
         TUMessages::Error(stdout, "TUNumMethods", "IntegrationTrapezeRefine",
                           ">>>>> log step with xmin=0!!!!");
      if (n == 1) {
         s += axis->GetVal(kmin) * integrand[kmin];
         s += axis->GetVal(kmax) * integrand[kmax];
         s *= 0.5 * log(axis->GetVal(kmax) / axis->GetVal(kmin));
      } else {
         tnm = (Int_t)pow(2., n - 2);
         del = (kmax - kmin) / tnm;
         k = kmin + del / 2;
         sum = 0.0;
         for (Int_t j = 1; j <= tnm; ++j) {
            sum += axis->GetVal(k) * integrand[k];
            k += del;
         }
         s = 0.5 * (s +  log(axis->GetVal(kmax) / axis->GetVal(kmin))
                    * sum / (Double_t)tnm);
      }
   } else {
      if (n == 1) {
         s += (integrand[kmin] + integrand[kmax]);
         s *= 0.5 * (axis->GetVal(kmax) - axis->GetVal(kmin));
      } else {
         tnm = (Int_t) pow(2., n - 2);
         del = (kmax - kmin) / tnm;
         k = kmin + del / 2;
         sum = 0.0;
         for (Int_t j = 1; j <= tnm; ++j) {
            sum += integrand[k];
            k += del;
         }
         s = 0.5 * (s + (axis->GetVal(kmax) - axis->GetVal(kmin))
                    / Double_t(tnm) * sum);
      }
   }
   return;
}

//______________________________________________________________________________
void TUNumMethods::IntegrationTrapezeRefine(void (*fn)(Double_t &, Double_t *, Double_t &),
      Double_t &xmin, Double_t &xmax, Double_t par[], Double_t &s, Bool_t is_log, Int_t n)
{
   //--- IntegrationTrapeze refinement step (called by Simpson, do not call it
   //    directly!) for a function fn.
   // INPUTS:
   //  fn                Prototype of the function to integrate
   //  xmin              Lower boundary integration
   //  xmax              Upper boundary integration
   //  par[]             Parameters of fn
   //  is_log            Whether to use a log-step or a lin-step integration
   //  n                 Number of doubling steps
   // OUTPUT:
   //  s                 Integration results

   Int_t tnm = 0;
   Double_t del = 0., sum = 0., r = 0.;

   // If log-step
   if (is_log) {
      if (xmin <= 0.)
         TUMessages::Error(stdout, "TUNumMethods", "IntegrationTrapezeRefine",
                           ">>>>> log step with xmin=0!!!!");
      if (n == 1) {
         Double_t fn_res = 0.;
         fn(xmin, par, fn_res);
         s += fn_res * xmin;
         fn(xmax, par, fn_res);
         s += fn_res * xmax;
         s *= 0.5 * log(xmax / xmin);
      } else {
         tnm = (Int_t)pow(2., n - 2);
         del =  pow(xmax / xmin, 1. / (Double_t)tnm);
         r = xmin * sqrt(del);
         sum = 0.0;
         for (Int_t j = 1; j <= tnm; ++j) {
            Double_t fn_res;
            fn(r, par, fn_res);
            sum += fn_res * r;
            r *= del;
         }
         s = 0.5 * (s +  log(xmax / xmin) * sum / (Double_t)tnm);
      }
      // If lin-step
   } else {
      if (n == 1) {
         Double_t fn_res = 0.;
         fn(xmin, par, fn_res);
         s += fn_res;
         fn(xmax, par, fn_res);
         s += fn_res;
         s *= 0.5 * (xmax - xmin);
      } else {
         tnm = (Int_t)pow(2., n - 2);
         del = (xmax - xmin) / (Double_t)tnm;
         r = xmin + 0.5 * del;
         sum = 0.0;
         for (Int_t j = 1; j <= tnm; ++j) {
            Double_t fn_res;
            fn(r, par, fn_res);
            sum += fn_res;
            r += del;
         }
         s = 0.5 * (s + del * sum);
      }
   }
   return;
}

//______________________________________________________________________________
void TUNumMethods::IntegrationSimpsonLin(TUAxis *axis, Double_t *integrand,
      Int_t kmin, Int_t kmax, Double_t &s)
{
   //--- Simpson's rule lin-step integration with n steps (adapted from numerical
   //    recipes) for an array of values.
   // INPUTS:
   //  axis              Grid on which to integrate (lin- or log-step, depending on 'is_log')
   //  integrand         Array (on grid) to integrate
   //  kmin              Index from which to start integration
   //  kmax              Index from which to stop integration
   // OUTPUT:
   //  s                 Integration results

   // The number of steps must be odd for a Simpson integration
   if ((kmax - kmin) % 2 != 0)
      kmax -= 1;
   Int_t nstep = (kmax - kmin);
   // First and last steps
   s  = integrand[kmin] + integrand[kmax];
   // First 4/3 step
   s += 4. * integrand[kmin + 1];
   // All remaining 2/3 and 4/3 steps
   for (Int_t i = 1 ; i < nstep / 2; i++) {
      // Even step
      Int_t i_even = kmin + 2 * i;
      s += 2. * integrand[i_even];
      // Odd step
      Int_t i_odd = i_even + 1;
      s += 4. * integrand[i_odd];
   }
   s *= axis->GetStep() / 3.;
   return;
}

//______________________________________________________________________________
void TUNumMethods::IntegrationSimpsonLog(TUAxis *axis, Double_t *integ1, Double_t *integ2,
      Int_t kmin, Int_t kmax, Double_t &s)
{
   //--- Simpson's rule log-step integration with n steps (adapted from numerical
   //    recipes) for an array of values.
   // INPUTS:
   //  axis              Grid on which to integrate (lin- or log-step, depending on 'is_log')
   //  integ1            Array (on grid) to integrate is integ1*integ2
   //  integ2            Array (on grid) to integrate is integ1*integ2
   //  kmin              Index from which to start integration
   //  kmax              Index from which to stop integration
   // OUTPUT:
   //  s                 Integration results

   // The number of steps must be odd for a Simpson integration
   if ((kmax - kmin) % 2 != 0)
      kmax -= 1;
   Int_t nstep = (kmax - kmin);

   // Multiply by "x" because of the logarithmic step
   // First and last steps
   s  = axis->GetVal(kmin) * integ1[kmin] * integ2[kmin]
        + axis->GetVal(kmax) * integ1[kmax] * integ2[kmax];
   // First 4/3 step
   s += 4. * axis->GetVal(kmin + 1) * integ1[kmin + 1] * integ2[kmin + 1];
   // All remaining 2/3 and 4/3 steps
   for (Int_t i = 1 ; i < nstep / 2; i++) {
      // Even step
      Int_t i_even = kmin + 2 * i;
      s += 2. * axis->GetVal(i_even) * integ1[i_even] * integ2[i_even];
      // Odd step
      Int_t i_odd = i_even + 1;
      s += 4. * axis->GetVal(i_odd) * integ1[i_odd] * integ2[i_odd];
   }
   s *= log(axis->GetStep()) / 3.;
   return;
}

//______________________________________________________________________________
void TUNumMethods::IntegrationSimpsonLog(void (*fn)(Double_t &, Double_t *, Double_t &),
      Double_t &xmin, Double_t &xmax, Double_t par[], Double_t &s, Int_t nstep)
{
   //--- Simpson's rule log-step integration with n steps (adapted from numerical
   //    recipes) for a function fn.
   // INPUTS:
   //  fn                Prototype of the function to integrate
   //  xmin              Lower boundary integration
   //  xmax              Upper boundary integration
   //  par[]             Parameters of fn
   //  nstep             Number of steps used
   //  is_verbose        Chatter on or off
   // OUTPUT:
   //  s                 Integration results

   if (xmin <= 0.) {
      cout << ">>>>> log step with xmin=0!!!! Abort!" << endl;
      abort();
   }

   Double_t log_step = pow(xmax / xmin, 1. / (Double_t(2 * nstep)));
   // We take 2*nstep (to ensure an even number of steps in total)
   // Simpson steps are 1/3, 4/3, 2/3, 4/3,...., 4/3, 1/3

   // First and last step (simpson =1/3)
   // multiplied by "x" because of the logarithmic step
   Double_t fn_res = 0.;
   fn(xmin, par, fn_res);
   s += fn_res * xmin;
   fn(xmax, par, fn_res);
   s += fn_res * xmax;

   // First 4/3 step
   Double_t x = xmin * log_step;
   fn(x, par, fn_res);
   s +=  4. * fn_res * x;

   // All remaining 2/3 and 4/3 steps
   for (Int_t i = 1 ; i < nstep; i++) {
      // Even step
      x *= log_step;
      fn(x, par, fn_res);
      s += 2. * fn_res * x;
      // Odd step
      x *= log_step;
      fn(x, par, fn_res);
      s += 4. * fn_res * x;
   }
   s *= log(log_step) / 3.;
   return;
}

//______________________________________________________________________________
void TUNumMethods::MatrixInversionTriDiagonal(Double_t *ainf, Double_t *adiag,
      Double_t *asup, Double_t *r, Double_t *u, Int_t n)
{
   //--- Performs tridiagonal inversion (pivoting is not used so that this
   //    routine is not "robust"). Use MatrixInversionGaussJordan inversion
   //    method below if necessary (slower). Consider the matrix equation
   //      [A] * [u] = [r],
   //    where [A] is a matrix n*n, and [u] and [r] are vectors of size n.
   //    This routines returns
   //      [u] = [A]^{-1} [r]
   //    where [A] is a tridiagonal matrix.
   // INPUTS:
   //  ainf[n-1]         Lower diagonal elements of tridiagonal matrix A to invert
   //  adiag[n]          Diagonal elements of tridiagonal matrix A to invert
   //  asup[n-1]         Upper diagonal elements of tridiagonal matrix A to invert
   //  r[n]              Vector corresponding to  [A] * [u] = [r]
   //  n                 Size of vectors
   // OUTPUT:
   //  u[n]              Vector solution of [u] = [A]^{-1} [r]

   Int_t j;
   Double_t bet;
   Double_t *gam = new Double_t[n];

   // Inversion with this algorithm fails if the first diagonal coefficient is null
   if (adiag[0] == 0.0) {
      TUMessages::Error(stdout, "TUNumMethods", "MatrixInversionTriDiagonal",
                        "Problem in the tridiagonal inversion...");
      printf(" adiag[0] = %.5e", adiag[0]);
      abort();
   }

   bet  = adiag[0];
   u[0] = r[0] / bet;
   for (j = 1; j < n; j++) {
      gam[j] = asup[j - 1] / bet;
      bet = adiag[j] - ainf[j - 1] * gam[j];
      // ... it also fails here
      if (bet == 0.0) {
         TUMessages::Error(stdout, "TUNumMethods", "MatrixInversionTriDiagonal",
                           "Problem in the tridiagonal inversion...");
         printf(" j = %ld , beta_j_j = %.5e \n", (long)j, bet);
         abort();
      }
      u[j] = (r[j] - ainf[j - 1] * u[j - 1]) / bet;
   }
   for (j = n - 2; j >= 0; j--) {
      u[j] = u[j] - gam[j + 1] * u[j + 1];
   }

   delete[] gam;
   gam = NULL;
}

//______________________________________________________________________________
void TUNumMethods::MatrixInversionTriDiagonal(Double_t *a, Double_t *b,
      Double_t *c, Double_t *u, Int_t n)
{
   //--- Performs tridiagonal inversion. Consider the matrix equation
   //      [A] * [u] = [r],
   //    where [A] is a matrix n*n, and [u] and [r] are vectors of size n.
   //    This routines returns
   //      [u] = [A]^{-1} [r]
   //    where [A] is a tridiagonal matrix.
   // INPUTS:
   //  a[n-1]            Lower diagonal elements of tridiagonal matrix A to invert
   //  b[n]              Diagonal elements of tridiagonal matrix A to invert
   //  c[n-1]            Upper diagonal elements of tridiagonal matrix A to invert
   //  u[n]              Vector corresponding to [r] in [A] * [u] = [r] (optimisation)
   //  n                 Size of vectors
   // OUTPUT:
   //  u[n]              Vector solution of [u] = [A]^{-1} [r]


   n--; // since we start from x0 (not x1)
   c[0] /= b[0];
   u[0] /= b[0];

   for (Int_t i = 1; i < n; i++) {
      c[i] /= b[i] - a[i - 1] * c[i - 1];
      u[i] = (u[i] - a[i - 1] * u[i - 1]) / (b[i] - a[i - 1] * c[i - 1]);
   }
   u[n] = (u[n] - a[n - 1] * u[n - 1]) / (b[n] - a[n - 1] * c[n - 1]);

   for (Int_t i = n; i-- > 0;)
      u[i] -= c[i] * u[i + 1];


// Int_t j;
//   Double_t bet;
//   Double_t *gam = new Double_t[n];

//   // Inversion with this algorithm fails if the first diagonal coefficient is null
//   if (adiag[0] == 0.0) {
//      TUMessages::Error(stdout, "TUNumMethods", "MatrixInversionTriDiagonal",
//                        "Problem in the tridiagonal inversion...");
//      printf(" adiag[0] = %.5e", adiag[0]);
//      abort();
//   }

//   bet  = adiag[0];
//   u[0] = u[0] / bet;
//   for (j = 1; j < n; j++) {
//      gam[j] = asup[j - 1] / bet;
//      bet = adiag[j] - ainf[j - 1] * gam[j];
//      // ... it also fails here
//      if (bet == 0.0) {
//         TUMessages::Error(stdout, "TUNumMethods", "MatrixInversionTriDiagonal",
//                           "Problem in the tridiagonal inversion...");
//         printf(" j = %ld , beta_j_j = %.5e \n", (unsigned long)j, bet);
//         abort();
//      }
//      u[j] = (u[j] - ainf[j - 1] * u[j - 1]) / bet;
//   }
//   for (j = n - 2; j >= 0; j--) {
//      u[j] = u[j] - gam[j + 1] * u[j + 1];
//   }

//   delete[] gam;
//   gam = NULL;

}

//______________________________________________________________________________
void TUNumMethods::MatrixInversionGaussJordan(Double_t *a[], Int_t n, Double_t *adiag[], Int_t m)
{
   //--- Performs Gauss-Jordan inversion (adapted from numerical recipes)
   //    Consider the matrix equation
   //       [A] * [u] = [r],
   //    where [A] is a matrix n*n, and [u] and [r] are matrices of size n*m.
   //    This routines returns
   //      [u] = [A]^{-1} [r]
   //    where [A] is any matrix. The inverse of [A] is gradually built-in and
   //    returned as the input matrix "a" is destroyed: this is the Gauss-Jordan
   //    elimination with full pivoting.
   // INPUTS:
   //  a[n*n]            Matrix A to invert
   //  adiag[n*m]            Matrix [b] in [A] * [u] = [b]
   //  n                 Matrices a and b have size n*m
   //  m                 Matrices a and b have size n*m
   // OUTPUT:
   //  adiag[n*m]            Matrix [u] = [A]^{-1} [r]

   Int_t *indxc = new Int_t[n];
   Int_t *indxr = new Int_t[n];
   Int_t *ipiv = new Int_t[n];

   Int_t i, icol = 0, irow = 0, j, k, l, ll;
   Double_t big, dum, pivinv;

   for (j = 0; j < n; j++) ipiv[j] = 0;
   for (i = 0; i < n; i++) {
      big = 0.0;
      for (j = 0; j < n; j++)
         if (ipiv[j] != 1)
            for (k = 0; k < n; k++) {
               if (ipiv[k] == 0) {
                  if (fabs(a[j][k]) >= big) {
                     big = fabs(a[j][k]);
                     irow = j;
                     icol = k;
                  }
               }
            }
      ++(ipiv[icol]);

      if (irow != icol) {
         for (l = 0; l < n; l++) swap(a[irow][l], a[icol][l]);
         for (l = 0; l < m; l++) swap(adiag[irow][l], adiag[icol][l]);
      }
      indxr[i] = irow;
      indxc[i] = icol;
      pivinv = 1.0 / a[icol][icol];
      a[icol][icol] = 1.0;

      for (l = 0; l < n; l++) a[icol][l] *= pivinv;
      for (l = 0; l < m; l++) adiag[icol][l] *= pivinv;

      for (ll = 0; ll < n; ll++)
         if (ll != icol) {
            dum = a[ll][icol];
            a[ll][icol] = 0.0;
            for (l = 0; l < n; l++) a[ll][l] -= a[icol][l] * dum;
            for (l = 0; l < m; l++) adiag[ll][l] -= adiag[icol][l] * dum;
         }
   }
   for (l = n - 1; l >= 0; l--) {
      if (indxr[l] != indxc[l])
         for (k = 0; k < n; k++)
            swap(a[k][indxr[l]], a[k][indxc[l]]);
   }

   delete[] indxc;
   indxc = NULL;
   delete[] indxr;
   indxr = NULL;
   delete[] ipiv;
   ipiv = NULL;
}

//______________________________________________________________________________
void TUNumMethods::SolveEq_1D2ndOrder_Explicit(Int_t nx, Double_t const &dx, Double_t *a_term,
      Double_t *b_term, Double_t *c_term, Double_t *s_term, gENUM_MATRIXINVERT scheme,
      Double_t const &b0, Double_t const &c0, Double_t const &an, Double_t const &bn)
{
   //--- Solves the second-order propagation equation.
   //    N.B.: works for lin- or log-step given that the a, b,c terms,
   //          and dx are filled appropriately
   //
   //    Let us consider the generic second-order equation
   //      s + A d/dx( B*s - C ds/dx ) = S0                          (1)
   //    Expressed in a Crank-Nicholson finite difference, it reads
   //      a_j s(j-1)   + b_j s(j)   +  c_j s(j+1)  =  S0(j)         (2)
   //    with
   //      a_j= -A(j)*B(j-1)/2dx - A(j)*C(j-1/2)/(dx)^2              (3a)
   //      b_j = 1 + A(j)*(C(j-1/2)+C(j+1/2))/(dx)^2                 (3b)
   //      c_j=  A(j)*B(j+1)/2dx - A(j)*C(j+1/2)/(dx)^2              (3c)
   //    This a tridiagonal matrix. It can be inverted given boundary conditions.
   //
   // INPUTS:
   //  nx                Dimension of the vectors (number of bins)
   //  dx                Delta x
   //  a_term            [nx] Coefficients A of Eq.(1)
   //  b_term            [nx] Coefficients B of Eq.(1)
   //  c_term            [nx] Coefficients C of Eq.(1)
   //  s_term            [nx] r.h.s. S0 of Eq.(1)
   //  scheme            Matrix inversion method:
   //                      - kGAUSSJORDAN (Gauss-Jordan) full pivoting (safer but slower)
   //                      - kTRID (tridiagonal inversion)
   //  b0                Boundary condition at lower end (diagonal term)
   //  c0                Boundary condition at lower end (upper diagonal)
   //  an                Boundary condition at higher end (lower diagonal)
   //  bn                Boundary condition at higher end (diagonal term)
   // OUTPUT:
   //  s_term            [nx] Solution s of Eq.(1)

   // vector indices:
   //   - vdiag[0...nx-1] = b_j[0...nx-1]
   //   - vsup[0...nx-2] = c_j[0...nx-2]
   //   - vinf[0...nx-2] = a_j[1...nx-1]
   Double_t *vdiag = new Double_t[nx];
   Double_t *vsup = new Double_t[nx - 1];
   Double_t *vinf  = new Double_t[nx - 1];

   for (Int_t k = nx - 2; k > 0; --k) {
      // Right-hand side vector = s_term[0...nx-1] (see above)
      // Left-hand side operator (tridiagonal matrix)
      // NB: in vector passed, c_term[k] is c_term evaluated at k-1/2
      vdiag[k] = 1. + a_term[k] * (c_term[k] + c_term[k + 1]) / (dx * dx);
      vsup[k] = (a_term[k] / dx) * (b_term[k + 1] / 2. - c_term[k + 1] / dx);
      vinf[k - 1] = (a_term[k] / dx) * (- b_term[k - 1] / 2. - c_term[k] / dx);
   }

   // Set boundary condition
   // Lower end
   vdiag[0] = b0;
   vsup[0] = c0;
   // Upper end
   vdiag[nx - 1] = bn;
   vinf[nx - 2] = an;


   // MAtrix inversion
   if (scheme == kTRID)      // Tridiagonal
      TUNumMethods::MatrixInversionTriDiagonal(vinf, vdiag, vsup, s_term, nx);
   else if (scheme == kGAUSSJORDAN) { // Gauss-Jordan
      Double_t **matrix = new Double_t *[nx];
      Double_t **s0 = new Double_t *[nx];
      for (Int_t row = nx - 1; row >= 0; --row) {
         matrix[row] = new Double_t[nx];
         s0[row] = new Double_t[(Int_t)1];
         for (Int_t col = nx - 1; col >= 0; --col)
            matrix[row][col] = 0.0;
      }

      // Loop over position X to fill LeftOperator and Vec of solutions.
      for (Int_t i_x = nx - 1; i_x >= 0; --i_x) {
         // Right-hand side vector = s0[0...nx-1] (see above)
         s0[i_x][0] = s_term[i_x];
         // Left-hand side operator (tridiagonal matrix)
         matrix[i_x][i_x] = vdiag[i_x];
         if (i_x > 0) matrix[i_x][i_x - 1] = vinf[i_x - 1];
         if (i_x < nx - 1) matrix[i_x][i_x + 1] = vsup[i_x];
      }
      // Inversion of the tridiagonal matrix...
      TUNumMethods::MatrixInversionGaussJordan(matrix, (Int_t)nx, s0, (Int_t)1);
      for (Int_t i_x = nx - 1; i_x >= 0; --i_x) s_term[i_x] = s0[i_x][0];

      // --- FREE MEMORY
      delete[] matrix;
      matrix = NULL;
      delete[] s0;
      s0 = NULL;
   } else
      TUMessages::Error(stdout, "TUNumMethods", "SolveEq_1D2ndOrder_Explicit",
                        "Inversion schemes (see TUEnum.h) are kGAUSSJORDAN or kTrid");

   delete[] vinf;
   vinf = NULL;
   delete[] vdiag;
   vdiag = NULL;
   delete[] vsup;
   vsup = NULL;
}

//______________________________________________________________________________
void TUNumMethods::SolveEq_T1D2ndOrder_CranckNicholson(Int_t nt, Int_t nx, Double_t const &dt, Double_t const &dx,
      Double_t **fn2_t1_2, Double_t **fn1_t1_2, Double_t **fn0_t1_2,
      Double_t **v_w, gENUM_MATRIXINVERT scheme)
{
   //--- Performs 1D Crank-Nicholson inversion (general and applies to various problems).
   // INPUTS:
   //  nt                Number of time steps
   //  nx                Number of positions
   //  dt                Time step length
   //  dx                Position step length
   //  fn2_t1_2[nx][nt]  Zero-th order coefficients of Eq.(1) berow
   //  fn1_t1_2[nx][nt]  First order coefficients of Eq.(1) below
   //  fn0_t1_2[nx][nt]  Second order coefficients of Eq.(1) below
   //  v_w[nx][nt]       Contains initial values in v_w[nx][0]
   //  scheme            Matrix inversion method:
   //                      - kGAUSSJORDAN (Gauss-Jordan) full pivoting (safer but slower)
   //                      - kTRID (tridiagonal inversion)
   // OUTPUT:
   //  v_w[nx][nt]       Final result after CN inversion
   //
   // The crank-Nicholson finite-differencing scheme allows to solve
   // partial diffusion-like equation by means of repeated tridiagonal inversion.
   // This is adapted to boundary condition (constraInt_t on the boundary at each
   // time) plus initial conditions.
   // The principle is thus to solve recursively the equation
   //                L(n) u(n+1) = R(n) u(n)
   //                  dim(L)=dim(R)=m*m ; dim(u)=m [vector]
   // where L(n) and R(n) are tridiagonal operators evaluates at step "n+1/2"
   // (related to functions fc_t1_2) and u(n) and u(n+1) are solution vectors
   // at steps "n" and "n+1" respectively
   // The solution "v_w" (2D array) is obtained at each time step: the boundary
   // conditions (at r=rmin and r=rmax) can evolve at each time step dt
   //   - 3 coefficients at each spatial node and each temporal node (but evaluated at time t+1/2)
   //   - 2D array (solution) for which IC for all spatial nodes and BCs at each time are given

   // Here are more details. The equation we wish to solve is
   //      dw/dt=fc2(x,t)d2w/dx2+fc1(x,t)dw/dx+fc0(x,t)w   [eq.1]
   // where "d" stands for partial derivatives
   //
   // Here, w=w(r,t) is the quantity depending on time (t) and position (x)
   // we wish to know. The coefficients fc2(x,t), fc1(x,t) and fn0(x,t) are
   // known and are the coefficient of the second derivative term, the first
   // derivative term and w(x,t) respectively. Obviously, any formally similar
   // equation can be tackled with the same method, even if the parameters are
   // not t and r. For instance, for solar modulation, t=ln(Ek) and r=position
   // in the solar cavity.
   //
   // Let {x_0, x_1, ...x_i ... x_n} be the position on the grid (n+1 points, step = dx)
   // and {t_0, t_1, ...t_j ... t_m} be the position on the grid (m+1 points, step = dt).
   // The above diffusion equation expressed in crank-Nicholson finite-difference form reads
   //                     [i->x, j->t]
   // a(i,j+1/2)*w(i-1,j+1) + b(i,j+1/2)*w(i,j+1) + c(i,j+1/2)*w(i+1,j+1) =
   // -a(i,j+1/2)*w(i-1,j) + (2-b(i,j+1/2))*w(i,j) - c(i,j+1/2)w(i+1,j)    [eq. 2]
   //                        where
   //   a(i,j) = -fn2(i,j+1/2)*dt/(2*dx*dx) + fn1(i,j+1/2)*dt/(4*dx)
   //   b(i,j) = 1 + fn2(i,j+1/2)*dt/(dx*dx) - fn0(i,j+1/2)*dt/2
   //   c(i,j) = - fn2(i,j+1/2)*dt/(2*dx*dx) - fn1(i,j+1/2)*dt/(4*dx)
   //
   // Actually, this connects the solution at time "j+1" to the solution
   // at time "j". In particular, as a,b and c are known functions at
   // each step. The solution appears more clearly in its matrix form:
   //
   //  | L0j  L0j  0 . . . . . . .  0 |  | w(j+1)_0 |   | R0j  R0j  0 . . . . . . .  0 |  | w(j)_0 |
   //  | L1j  L1j  L1j .            . |  | w(j+1)_1 |   | R1j  R1j  R1j .            . |  | w(j)_1 |
   //  |  0   L2j  .   .  .         . |  |    .     |   |  0   r2j  .   .  .         . |  |    .   |
   //  |  .   .   .   .   .  .      . |  |    .     | = |  .   .   .   .   .  .      . |  |    .   |
   //  |  .      .   .   .   .  .   . |  |    .     |   |  .      .   .   .   .  .   . |  |    .   |
   //  |  .         .   .   .   .   0 |  |    .     |   |  .         .   .   .   .   0 |  |    .   |
   //  |  .            .   .   .Ln-1,j|  |    .     |   |  .            .   .   .Rn-1,j|  |    .   |
   //  |  0 . . . . . . . 0  Ln,j Ln,j|  | w(j+1)_n |   |  0 . . . . . . . 0  Rn,j Rn,j|  | w(j)_n |
   //
   // where Lij and Rij are coefficients evaluated at time j+1/2 for each
   // position i={0...n}. "L" stands for the Left-hand side operator in eq.(2)
   // and "R" for the Right-hand side. To proceed further with the solution, the
   // algorithm is very simple.
   //  First step : at time j=0, w(0)_i is known for all position "i". The right
   //      hand side of the matrix equation above is then reduced to a vector
   //      column of numbers, i.e. [L(0)][w(1)] = [R(0)][w(0)] = [v(0)]
   //  Then, at each step, one has to solve [L(j)][w(j+1)] = [v(j)], where [v(j)]
   //  is known from w(j) obtained in the previous step through [v(j)] = [R(j)][w(j)].

   //-----------------//
   // If Gauss-Jordan //
   //-----------------//
   if (scheme == kGAUSSJORDAN) {
      // NB: the sol. must be found on "nx-2" nodes (BCs are specified,
      // i.e end points are fixed) so that we work only on the x={1...nx-2}
      // components excluding end points (O,nx-1)
      Double_t **LOMatrix     = new Double_t *[nx - 2];
      Double_t **sol = new Double_t *[nx - 2];
      for (Int_t rows = 0; rows < nx - 2; rows++) {
         LOMatrix[rows]     = new Double_t[nx - 2];
         sol[rows] = new Double_t[(Int_t)1];
      }

      Double_t norm_test = 1.;
      // CORE OF THE CRANK-NICHOLSON PROCEDURE
      // Loop over time (step 0 gives the solution for time t=1)
      for (Int_t t = 0; t < nt - 1; t++) {
         // Initialisation is required at each time step since the GJ inversion
         // returns LOMatrix as the inverse of the original matrix...
         for (Int_t raw = 0; raw < nx - 2; raw++)
            for (Int_t col = 0; col < nx - 2; col++)
               LOMatrix[raw][col] = 0.0;

         // Loop over position X to fill LeftOperator and Vec of solutions.
         // As Dirichlet boundary conditions are used, the tridiagonal inversion
         // will be done on the (nx-2) remaining positions x = {1...nx-1} on
         // the grid ( v_w(t,x=0) and v_w(t,x=nx-1) are known)
         for (Int_t x = 1; x < nx - 1; x++) {
            // NB: The three function fc?_t1_2 are evaluated to the index t
            // corresponding to the time t+1/2
            Double_t mu = dt / (2.*dx * dx);
            Double_t nu = dt / (4.*dx);
            Double_t a = - mu * fn2_t1_2[x][t] + nu * fn1_t1_2[x][t];
            Double_t b = 1. + 2.*mu * fn2_t1_2[x][t] - 0.5 * dt * fn0_t1_2[x][t];
            Double_t c = - mu * fn2_t1_2[x][t] - nu * fn1_t1_2[x][t];
            if (x == 1) norm_test = b;

            // Right-hand side vector = [R][v_w] (see above)
            Int_t ix = x - 1;
            sol[ix][0] = - a * v_w[x - 1][t] + (2. - b) * v_w[x][t] - c * v_w[x + 1][t];
            a /= norm_test;
            b /= norm_test;
            c /= norm_test;
            sol[ix][0] /= norm_test;


            // Left-hand side operator (tridiagonal matrix)
            LOMatrix[ix][ix] = b;
            if (ix > 0) LOMatrix[ix][ix - 1] = a;
            if (ix < nx - 3) LOMatrix[ix][ix + 1] = c;

            printf("t=%2d  x=%2d  dt=%.3le  dx=%.3le  (f,g,h)=(%.3le,%.3le,%.3le)   (a,b,c)=(%.3le,%.3le,%.3le)  v_w[%d,%d,%d][%d]=(%.3le,%.3le,%.3le)  sol_before[%2d]=%.3le\n",
                   t, x, dt, dx, fn2_t1_2[x][t], fn1_t1_2[x][t], fn0_t1_2[x][t], a, b, c, x - 1, x, x + 1, t, v_w[x - 1][t], v_w[x][t], v_w[x + 1][t], ix, sol[ix][0]);
         }
         // Inversion of the tridiagonal matrix...
         MatrixInversionGaussJordan(LOMatrix, (Int_t)(nx - 2), sol, (Int_t)1);
         //for (Int_t x = 1; x < nx - 1; x++) {
         for (Int_t x = 0; x < nx; x++) {
            if (x > 0 && x < nx - 1) {
               v_w[x][t + 1] = sol[x - 1][0];
            }
            printf("t=%2d  x=%2d     [GJ]    sol_before[%2d][%2d]=%le  sol_after[%2d][%2d]=%le\n", t, x, x, t, v_w[x][t], x, t + 1, v_w[x][t + 1]);
         }
      }

      // free memory
      if (LOMatrix) delete[] LOMatrix;
      LOMatrix = NULL;
      if (sol) delete[] sol;
      sol = NULL;
   }

   //----------------//
   // If Tridiagonal //
   //----------------//
   else if (scheme == kTRID) {

      // NB: the sol. must be found on "nx-2" nodes (BCs are specified,
      // i.e end points are fixed) so that we work only on the x={1...nx-2}
      // components excluding end points (O,nx-1)
      Double_t *LOinf     = new Double_t[nx - 3];
      Double_t *LOdiag    = new Double_t[nx - 2];
      Double_t *LOsup     = new Double_t[nx - 3];
      Double_t *sol = new Double_t[nx - 2];

      // CORE OF THE CRANK-NICHOLSON PROCEDURE
      // Loop over time (step 0 gives the solution for time t=1, the last
      // step "nt-2" gives the desired result for the last time belonging
      // to the grid)
      for (Int_t t = 0; t < nt - 1; ++t) {
         // Loop over position X to fill LO and tmpSol of solutions. As
         // Dirichlet boundary conditions are used, the tridiagonal inversion
         // will be done on the (nx-2) remaining positions x = {1...nx-1} on
         // the grid ( v_w(t,x=0) and v_w(t,x=nx-1) are known)
         for (Int_t x = 1; x < nx - 1; ++x) {
            Int_t ix = x - 1;
            // NB: The three function fc?_t1_2 are evaluated to the index t
            // corresponding to the time t+1/2
            Double_t mu = dt / (2.*dx * dx);
            Double_t nu = dt / (4.*dx);
            Double_t a = -mu * fn2_t1_2[x][t] + nu * fn1_t1_2[x][t];
            Double_t b = 1. + 2.*mu * fn2_t1_2[x][t] - 0.5 * dt * fn0_t1_2[x][t];
            Double_t c = - mu * fn2_t1_2[x][t] - nu * fn1_t1_2[x][t];

            // Right-hand side vector = [R][v_w] (see above)
            sol[ix] = - a * v_w[x - 1][t] + (2. - b) * v_w[x][t] - c * v_w[x + 1][t];
            // Left-hand side operator (diagonal, inf and sup component)
            LOdiag[ix] = b;
            // if (x < nx - 2) {
            //    LOinf[ix] = a;
            //    LOsup[ix] = c;
            // }
            if (x < nx - 2)
               LOinf[ix] = a;
            if (x > 1 && x < nx - 1)
               LOinf[ix - 1] = c;

            //printf("t=%2d  x=%2d  dt=%.3le  dx=%.3le  (a,b,c)=(%.3le,%.3le,%.3le)  v_w[%d,%d,%d][%d]=(%.3le,%.3le,%.3le)  sol_before[%2d]=%.3le\n",
            //        t, x, dt, dx, a, b, c, x-1,x,x+1, t, v_w[x-1][t], v_w[x][t], v_w[x+1][t], ix, sol[ix]);
         }
         MatrixInversionTriDiagonal(LOinf, LOdiag, LOsup, sol, (Int_t)(nx - 2));
         for (Int_t x = 1; x < nx - 1; x++)
            v_w[x][t + 1] = sol[x - 1];
         //for (Int_t ix = 0; ix < nx-2 ; ix++)
         //   printf("t=%2d  ix=%2d    [TRID]   sol_after[%2d]=%le\n", t, ix, ix, sol[ix]);
      }
      if (LOinf) delete[] LOinf;
      LOinf = NULL;
      if (LOdiag) delete[] LOdiag;
      LOdiag = NULL;
      if (LOsup) delete[] LOsup;
      LOsup = NULL;
      if (sol) delete[] sol;
      sol = NULL;
   } else
      TUMessages::Error(stdout, "TUNumMethods", "SolveEq_T1D2ndOrder_CranckNicholson",
                        "Inversion schemes (see TUEnum.h) are kGAUSSJORDAN or kTrid");
}

//______________________________________________________________________________
void TUNumMethods::TEST(FILE *f)
{
   //--- Test function for this class (called in usine -t).
   //  f                 Output file for test

   string message = "The namespace TUNumMethods contains some\n"
                    "simple numerical methods (adapted from Numerical Recipes).";
   TUMessages::Test(f, "TUNumMethods", message);

   /*******************/
   /*   Integration   */
   /*******************/
   // Search for the index of an element in a sorted list
   fprintf(f, " * Simpson integration (lin/log/adaptive) = int_{1}^{5} x*x dx\n");
   fprintf(f, "      => res(analytical) = 4.133333e+01\n");
   Double_t xmin = 1., xmax = 5., par[1] = {1.};
   Double_t res = 0.;
   Int_t n_steps = (Int_t)pow(2., 10) + 1;
   Bool_t is_verbose = false;
   Double_t eps = 1.e-3;

   fprintf(f, "   > IntegrationSimpsonAdapt(dummy_fn, xmin=1, xmax=5, par[]={1}, res, eps=1.e-3, is_log=true, is_verbose=false);\n");
   IntegrationSimpsonAdapt(dummy_fn, xmin, xmax, par, res, eps, true, is_verbose);
   fprintf(f, "      => res(log-adapt) = %le\n", res);

   fprintf(f, "   > IntegrationSimpsonAdapt(dummy_fn, xmin=1, xmax=5, par[]={1}, res, eps=1.e-3, is_log=false, is_verbose=false);\n");
   IntegrationSimpsonAdapt(dummy_fn, xmin, xmax, par, res, eps, false, is_verbose);
   fprintf(f, "      => res(lin-adapt) = %le\n", res);

   fprintf(f, "   > IntegrationSimpsonLog(dummy_fn, xmin=1, xmax=5, par[], res, n_steps=%d);\n", n_steps);
   IntegrationSimpsonLog(dummy_fn, xmin, xmax, par, res, n_steps);
   fprintf(f, "      => res(fixed # of steps) = %le\n", res);
   fprintf(f, "\n");

   fprintf(f, " * Simpson integration: test using arrays (linear, Nmax=%d)\n", n_steps);
   Int_t kmin = 0, kmax = n_steps - 1;
   TUAxis x_axis;
   // Linear steps
   x_axis.SetClass(xmin, xmax, n_steps, "", "", kLIN);
   vector<Double_t> y(n_steps, 0.), sqrty(n_steps, 0);
   for (Int_t i = 0; i < x_axis.GetN(); ++i) {
      y[i] = x_axis.GetVal(i) * x_axis.GetVal(i);
      sqrty[i] = sqrt(y[i]);
   }
   fprintf(f, "   > IntegrationSimpsonAdapt(x_axis, y[], i=0, i=n-1, [output]=res, eps=1.e-3, \"this->TEST()\");\n");
   IntegrationSimpsonAdapt(&x_axis, &y[0], kmin, kmax, res, eps, "this->TEST()");
   fprintf(f, "      => res(lin-adapt. grid) [y] = %le\n", res);
   fprintf(f, "   > IntegrationSimpsonAdapt(x_axis, sqrty[], sqrty[], i=0, i=n-1, [output]=res, eps=1.e-3, \"this->TEST()\");\n");
   IntegrationSimpsonAdapt(&x_axis, &sqrty[0], &sqrty[0], kmin, kmax, res, eps, "this->TEST()");
   fprintf(f, "      => res(lin-adapt. grid) [sqrty*sqrty] = %le\n", res);
   fprintf(f, "   > IntegrationSimpsonLin(x_axis, y[], i=0, i=n-1, [output]=res);\n");
   IntegrationSimpsonLin(&x_axis, &y[0], kmin, kmax, res);
   fprintf(f, "      => res(lin. fixed-grid) [y] = %le\n", res);
   fprintf(f, "\n");

   // Log steps
   fprintf(f, " * Simpson integration: test using arrays (log) of Nmax=%d steps\n", x_axis.GetN());
   x_axis.SetClass(xmin, xmax, n_steps, "", "", kLOG);
   for (Int_t i = 0; i < x_axis.GetN(); ++i) {
      y[i] = x_axis.GetVal(i) * x_axis.GetVal(i);
      sqrty[i] = sqrt(y[i]);
   }
   fprintf(f, "   > IntegrationSimpsonLog(x_axis, sqrty[], sqrty[], i=0, i=n-1, [output]=res);\n");
   IntegrationSimpsonLog(&x_axis, &sqrty[0], &sqrty[0], kmin, kmax, res);
   fprintf(f, "      => res(log. fixed-grid) [y] = %le\n", res);
   fprintf(f, "   > IntegrationSimpsonAdapt(x_axis, y[], i=0, i=n-1, [output]=res, eps=1.e-3, \"this->TEST()\", is_verbose = false);\n");
   IntegrationSimpsonAdapt(&x_axis, &y[0], kmin, kmax, res, eps, "this->TEST()", kFALSE);
   fprintf(f, "      => res(log-adapt. grid) = %le\n", res);
   kmin = 20;
   kmax = 84;
   fprintf(f, "   > IntegrationSimpsonLog(x_axis, sqrty[], sqrty[], i=20, i=84, [output]=res);\n");
   IntegrationSimpsonLog(&x_axis, &sqrty[0], &sqrty[0], kmin, kmax, res);
   fprintf(f, "      => res(log. fixed-grid) [y] = %le\n", res);
   fprintf(f, "   > IntegrationSimpsonAdapt(x_axis, y[], i=20, i=84, [output]=res, eps=1.e-3, \"this->TEST()\", is_verbose = false);\n");
   IntegrationSimpsonAdapt(&x_axis, &y[0], kmin, kmax, res, eps, "this->TEST()", kFALSE);
   fprintf(f, "      => res(log-adapt. grid) = %le\n", res);
   fprintf(f, "\n");

   /********************/
   /* Matrix Inversion */
   /********************/
   // // Check resolution of eq. [A] * [u] = [r] in the case of a tridiagonal matrix   [A]
   // fprintf(f, "\n");
   // fprintf(f, " * Matrix inversion: tridiagonal vs Gauss-Jordan\n");
   // fprintf(f, "   > MatrixInversionGaussJordan(a_gj, n=4, u_gj, m=1);\n");
   // fprintf(f, "   > MatrixInversionTriDiagonal(a_inf, a_diag, a_sup, r, u, n=4);\n");

   // for Tridiagonal inversion...
   const Int_t n = 4;
   Double_t a_diag[n] = {3., -2, 1., 4.};
   Double_t r[n] = {7., 3., -1., 7.};
   Double_t u[n] = {0., 0., 0., 0.};
   Double_t a_inf[n - 1] = {1., 8., -3};
   Double_t a_sup[n - 1] = {2., 2., -5.};

   // for Gauss-Jordan inversion...
   Double_t **u_gj = new Double_t *[n];
   Double_t **a_gj = new Double_t *[n];
   for (Int_t i = 0; i < n; i++) {
      a_gj[i] = new Double_t[n];
      for (Int_t j = 0; j < n; j++) a_gj[i][j] = 0.0;
      u_gj[i] = new Double_t[1];
      u_gj[i][0] = 0.0;
   }
   for (Int_t i = 0; i < n; ++i) {
      a_gj[i][i] = a_diag[i];
      if (i > 0) a_gj[i][i - 1] = a_inf[i - 1];
      if (i < n - 1) a_gj[i][i + 1] = a_sup[i];
      u_gj[i][0] = r[i];
   }

   MatrixInversionGaussJordan(a_gj, 4, u_gj, 1);
   MatrixInversionTriDiagonal(a_inf, a_diag, a_sup, r, u, 4);
   MatrixInversionTriDiagonal(a_inf, a_diag, a_sup, r, 4);

   fprintf(f, "        [A]      * [u] = [r]          Trid1 Trid2  G-J  (sol.)\n");
   fprintf(f, "  | 3  2  0  0 |   |x|   | 7|    |x| = |%.0f|   |%.0f|   |%.0f|   |1|\n"
           "  | 1 -2  2  0 | * |y| = | 3| => |y| = |%.0f|   |%.0f|   |%.0f|   |2|\n"
           "  | 0  8  1 -5 |   |z|   |-1|    |z| = |%.0f|   |%.0f|   |%.0f|   |3|\n"
           "  | 0  0 -3  4 |   |t|   | 7|    |t| = |%.0f|   |%.0f|   |%.0f|   |4|\n",
           u[0], r[0], u_gj[0][0],
           u[1], r[1], u_gj[1][0],
           u[2], r[2], u_gj[2][0],
           u[3], r[3], u_gj[3][0]);

   // Free memory
   for (Int_t j = 0; j < n; j++) {
      if (a_gj[j]) {
         delete[] a_gj[j];
         a_gj[j] = NULL;
      }
      if (u_gj[j]) {
         delete[] u_gj[j];
         u_gj[j] = NULL;
      }
   }
   if (a_gj) {
      delete[] a_gj;
      a_gj = NULL;
   }
   if (u_gj) {
      delete[] u_gj;
      u_gj = NULL;
   }
}
