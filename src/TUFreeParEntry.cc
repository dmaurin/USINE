// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

// C/C++ include
#include <cmath>
#include <cstdlib>
// ROOT include
// USINE include
#include "../include/TUFreeParEntry.h"
#include "../include/TUMessages.h"
#include "../include/TUMisc.h"

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TUFreeParEntry                                                       //
//                                                                      //
// Free parameter whose value is meant to change at runtime.            //
//                                                                      //
// The class TUFreeParEntry provides a free parameters entry:           //
//   - Name (case sensitive);
//   - Unit (default is [-], i.e. no unit);
//   - Value;
//   - Status (monitors whether Value has been modified during run).
//                                                                      //
// Whenever these parameters are used in model fitting, the class       //
// members used are:                                                    //
//   - Fit range (init val, min, max, sigma);
//   - Fit sampling (lin or log, free or nuisance parameter);
//   - Fit output (best fit, symmetric and/or asymmetric errors).
//                                                                      //
//////////////////////////////////////////////////////////////////////////

ClassImp(TUFreeParEntry)

//______________________________________________________________________________
TUFreeParEntry::TUFreeParEntry()
{
   // ****** normal constructor ******

   Initialise();
}

//______________________________________________________________________________
TUFreeParEntry::TUFreeParEntry(TUFreeParEntry const &par)
{
   // ****** Copy constructor ******
   //  par               Object to copy

   Copy(par);
}

//______________________________________________________________________________
TUFreeParEntry::~TUFreeParEntry()
{
   // ****** Default destructor ******
}

//______________________________________________________________________________
void TUFreeParEntry::Copy(TUFreeParEntry const &par)
{
   //--- Copies 'par' in class.
   //  par               Object to copy from

   Initialise();

   fFitInit = par.GetFitInit();
   fFitInitMin = par.GetFitInitMin();
   fFitInitMax = par.GetFitInitMax();
   fFitInitSigma = par.GetFitInitSigma();
   fFitSampling = par.GetFitSampling();
   fFitType = par.GetFitType();
   fFitValBest = par.GetFitValBest();
   fFitValErr = par.GetFitValErr();
   fFitValErrAsymmLo = par.GetFitValErrAsymmLo();
   fFitValErrAsymmUp = par.GetFitValErrAsymmUp();
   fIndexInExtClass = par.IndexInExtClass();
   fIsLowerLimitedPar = par.IsLowerLimitedPar();
   fIsUpperLimitedPar = par.IsUpperLimitedPar();
   fName = par.GetName();
   fStatus = par.GetStatus();
   fUnit = par.GetUnit(false);
   fVal = par.GetVal(false);
}

//______________________________________________________________________________
void TUFreeParEntry::ExtractFromFitName(string &fit_name, gENUM_AXISTYPE &sampling, gENUM_FREEPARTYPE &type) const
{
   //--- Sets parameter name, type and sampling used from fit name printed
   // INPUT:
   //  fit_name          Fit-name
   // OUTPUTS:
   //  fit_name          Extracted name
   //  sampling          Extracted sampling
   //  type              Extracted type

   Int_t n_remove = 0;

   // Set type
   if (fit_name.find("_FIXED") != string::npos) {
      type = kFIXED;
      n_remove = 6;
   } else if (fit_name.find("_NUISANCE") != string::npos) {
      type = kNUISANCE;
      n_remove = 9;
   } else if (fit_name.find("_FIT") != string::npos) {
      type = kFIT;
      n_remove = 4;
   }

   // Set sampling
   if (fit_name.find("log10") != string::npos) {
      fit_name.erase(fit_name.begin(), fit_name.begin() + 6);
      sampling = kLOG;
   } else
      sampling = kLIN;

   // Set name
   fit_name.erase(fit_name.end() - n_remove, fit_name.end());
}

//______________________________________________________________________________
string TUFreeParEntry::FormInitPar(Int_t n_digits) const
{
   // Returns string for initialisation parameters given a number of digits to print.
   //  n_digits          Number of digits to print with option %.[n_digits]le

   string range = "";

   if (fIsLowerLimitedPar && fIsUpperLimitedPar)
      range = Form("[%+.*le,%+.*le]", abs(n_digits), fFitInitMin, abs(n_digits), fFitInitMax);
   else if (fIsLowerLimitedPar)
      range = Form("[%+.*le,-]", abs(n_digits), fFitInitMin);
   else if (fIsUpperLimitedPar)
      range = Form("[-,%+.*le]", abs(n_digits), fFitInitMax);
   else
      range = "[-,-]";

   return range + Form(",%+.*le,%+.*le", abs(n_digits), fFitInit, abs(n_digits), fFitInitSigma);
}

//______________________________________________________________________________
void TUFreeParEntry::Initialise()
{
   //--- Initialises with default values

   fFitInit = 0.;
   fFitInitMin = 0.;
   fFitInitMax = 0.;
   fFitInitSigma = 0.;
   fFitSampling = kLIN;
   fFitType = kFIT;
   fFitValBest = 0.;
   fFitValErr = 0.;
   fFitValErrAsymmLo = 0.;
   fFitValErrAsymmUp = 0.;
   fIndexInExtClass = -1;
   fIsLowerLimitedPar = false;
   fIsUpperLimitedPar = false;
   fName = "";
   fStatus = false;
   fUnit = "-";
   fVal = 0.;
}

//______________________________________________________________________________
Bool_t TUFreeParEntry::IsSamePar(TUFreeParEntry const &par) const
{
   //--- Returns true if same parameter (name, unit, fit type and sampling)
   //  par               Object to copy from


   if (fFitSampling == par.GetFitSampling() && fFitType == par.GetFitType()
         && fName == par.GetName() && fUnit == par.GetUnit(false))
      return true;
   else
      return false;
}

//______________________________________________________________________________
string TUFreeParEntry::ParNameValUnit(Int_t uid0_base1_fit2, string const &use_name) const
{
   //--- Forms a string from parameter name, value, and unit.
   //  uid0_base1_fit2  Option to form string for the parameter
   //                       0: unique ID to use, e.g., in file names
   //                       1: current value
   //                       2: best fit value and error
   //  use_name         If not empty, use 'use_name' instead of parameter name

   string sign = "neg";
   if (fVal >= 0)
      sign = "";

   string tmp = "";
   if (uid0_base1_fit2 == 0) {
      string unit = GetUnit(true);
      TUMisc::RemoveSpecialChars(unit);
      string name = fName;
      if (use_name != "")
         name = use_name;
      TUMisc::RemoveSpecialChars(name);
      if (fabs(fVal) < 1.e-40)
         tmp = Form("%s%s0.00%s", name.c_str(), sign.c_str(), unit.c_str());
      else if (fabs(fVal) < 1.e-2 || fabs(fVal) > 100.)
         tmp = Form("%s%s%.3le%s", name.c_str(), sign.c_str(), fabs(fVal), unit.c_str());
      else if (fabs(fVal) < 1.)
         tmp = Form("%s%s%.3lf%s", name.c_str(), sign.c_str(), fabs(fVal), unit.c_str());
      else
         tmp = Form("%s%s%.2lf%s", name.c_str(), sign.c_str(), fabs(fVal), unit.c_str());
   } else if (uid0_base1_fit2 == 1)
      tmp = Form("%s=%.3le%s", fName.c_str(), fVal, GetUnit(true).c_str());
   else if (uid0_base1_fit2 == 2)
      tmp = Form("%s_best=%.3le(%.3le)%s", fName.c_str(), fFitValBest, fFitValErr, GetUnit(true).c_str());
   else {
      string message = Form("uid0_base1_fit2=%d is not a valid option", uid0_base1_fit2);
      TUMessages::Error(stdout, "TUFreeParEntry", "ParNameValUnit", message);
   }
   return tmp;
}

//______________________________________________________________________________
void TUFreeParEntry::Print(FILE *f) const
{
   //--- Prints in file f.
   //  f                 File where to print
   //  is_range          Whether to print or not range

   string indent = TUMessages::Indent(true);

   if (fStatus)
      fprintf(f, "%s   %45s = %+.4le %-16s {updated value}\n",
              indent.c_str(), fName.c_str(), fVal, GetUnit(true).c_str());
   else
      fprintf(f, "%s   %45s = %+.4le %-16s\n",
              indent.c_str(), fName.c_str(), fVal, GetUnit(true).c_str());

   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUFreeParEntry::PrintFitRelated(FILE *f, Int_t all0_fitonly1_initonly2) const
{
   //--- Prints in file f all fFit... members.
   //  f                       File where to print
   //  all0_fitonly1_initonly2 Whether to print
   //                             0: fit and init values
   //                             1: fit values only
   //                             2: init values only

   string indent = TUMessages::Indent(true);
   TUMessages::Indent(false);

   if (all0_fitonly1_initonly2 == 2) {
      string init = FormInitPar(3);
      fprintf(f, "%s   %45s was set from %s  %s\n", indent.c_str(), GetFitName().c_str(), init.c_str(), GetUnit(true).c_str());
   } else {
      // Check is symmetric and/or asymmetric error calculated
      Bool_t is_asymm = false;
      if (fabs(fFitValErrAsymmLo) > 1.e-40 || fabs(fFitValErrAsymmUp) > 1.e-40)
         is_asymm = true;

      string best_fit = "";
      if (fFitType == kFIXED)
         best_fit = Form("%+le", fFitValBest);
      else if (is_asymm)
         best_fit = Form("%+le(+/-%.2le, asymm=%+.2le,%+.2le)", fFitValBest, fFitValErr, fFitValErrAsymmLo, fFitValErrAsymmUp);
      else
         best_fit = Form("%+le(+/-%.2le)", fFitValBest, fFitValErr);

      if (all0_fitonly1_initonly2 == 0)
         fprintf(f, "%s   %45s = %s %-16s from initial setting %s=%s\n", indent.c_str(), GetFitName().c_str(),
                 best_fit.c_str(), GetUnit(true).c_str(), GetFitName(false).c_str(), FormInitPar(1).c_str());
      else if (all0_fitonly1_initonly2 == 1)
         fprintf(f, "%s   %45s = %s %s\n", indent.c_str(), GetFitName().c_str(), best_fit.c_str(), GetUnit(true).c_str());
   }
}

//______________________________________________________________________________
void TUFreeParEntry::SetFitInit(string const &fit_param_format, FILE *f_log)
{
   //--- Sets parameter used for any fit.
   //  fit_param_format  Format is "X,LIN_or_LOG,(log)val,(log)val_min,(log)val_max,(log)sigma" with
   //                       X=FIT, FIXED, or X=NUISANCE
   //  f_log             Log for USINE

   // Cast in vector
   vector<string> args;
   TUMisc::String2List(fit_param_format, "[,]", args);

   // Check correct number of arguments
   string format = "\"X,Y,[min,max],val,sigma\" with X=FIT, FIXED, or NUISANCE and Y=LIN or LOG";
   if (args.size() != 6) {
      string message = Form("Expected 6 entries (format = %s) in `%s', found %d",
                            format.c_str(), fit_param_format.c_str(), (Int_t)args.size());
      TUMessages::Error(f_log, "TUFreeParEntry", "SetFit", (string)message);
   }

   // Check first
   TUMisc::UpperCase(args[0]);
   TUEnum::Name2Enum(args[0], fFitType);

   // Check second
   string sampling = args[1];
   TUEnum::Name2Enum(sampling, fFitSampling);
   fFitInit = atof(args[4].c_str());
   fStatus = true;
   if (args[2] == "-")
      fIsLowerLimitedPar = false;
   else {
      fIsLowerLimitedPar = true;
      fFitInitMin = atof(args[2].c_str());
   }
   if (args[3] == "-")
      fIsUpperLimitedPar = false;
   else {
      fIsUpperLimitedPar = true;
      fFitInitMax = atof(args[3].c_str());
   }
   fFitInitSigma = atof(args[5].c_str());
}

//______________________________________________________________________________
void TUFreeParEntry::TEST(FILE *f)
{
   //--- Test function for this class (called in usine -t).
   //  f                 Output file for test

   string message = "Definition of a free parameter entry (simple class!)";
   TUMessages::Test(f, "TUFreeParEntry", message);

   // Fill and print class content
   fprintf(f, " * Base members and class:\n");
   fprintf(f, "   > SetPar(\"Vc\", \"km/s\", 10.);\n");
   SetPar("Vc", "km/s", 10.);
   fprintf(f, "   > Print(f);\n");
   Print(f);
   fprintf(f, "   > ParNameAndVal(0); => %s\n", ParNameValUnit(0).c_str());
   fprintf(f, "   > ParNameAndVal(1); => %s\n", ParNameValUnit(1).c_str());
   fprintf(f, "   > ParNameAndVal(2); => %s\n", ParNameValUnit(2).c_str());
   fprintf(f, "   > GetStatus();      => %d\n", GetStatus());
   fprintf(f, "   > Print(f);\n");
   Print(f);
   fprintf(f, "   > SetStatus(false);\n");
   SetStatus(false);
   fprintf(f, "   > Print(f);\n");
   Print(f);
   fprintf(f, "   > GetStatus();     => %d\n", GetStatus());
   fprintf(f, "   > SetPar(\"Vc\",\"km/s\",10);\n");
   SetPar("Vc", "km/s", 10.);
   fprintf(f, "   > Print(f);\n");
   Print(f);
   fprintf(f, "\n");
   fprintf(f, "\n");

   fprintf(f, " * Fit-related members (format is \'X,Y,[min,max],init,sigma\' with X=FIT or NUISANCE, and X=LIN or LOG\n");
   fprintf(f, "   > SetFitInit(\"NUISANCE,LIN,[5,25],20,1.3\", stdout);\n");
   SetFitInit("NUISANCE,LIN,[5,25],20,1.3", stdout);
   fprintf(f, "   > PrintFitInit(f);\n");
   PrintFitInit(f);
   fprintf(f, "   > PrintFitVals(f);\n");
   PrintFitVals(f);
   fprintf(f, "   > PrintFitValsAndInit(f);\n");
   PrintFitValsAndInit(f);
   fprintf(f, "\n");
   fprintf(f, "   > SetFitInit(\"FIT,LOG,[5,25],20,1.3\", f);\n");
   SetFitInit("FIT,LOG,[5,25],20,1.3", f);
   fprintf(f, "   > PrintFitValsAndInit(f);\n");
   PrintFitValsAndInit(f);
   fprintf(f, "   > SetFitInit(\"FIT,LOG,[-,25],20,1.3\", f);\n");
   SetFitInit("FIT,LOG,[-,25],20,1.3", f);
   fprintf(f, "   > PrintFitInit(f);\n");
   PrintFitInit(f);
   fprintf(f, "   > SetFitInit(\"FIT,LOG,[5,-],20,1.3\", f);\n");
   SetFitInit("FIT,LOG,[5,-],20,1.3", f);
   fprintf(f, "   > PrintFitInit(f);\n");
   PrintFitInit(f);
   fprintf(f, "   > SetFitInit(\"FIT,LOG,[-,-],20,1.3\", f);\n");
   SetFitInit("FIT,LOG,[-,-],20,1.3", f);
   fprintf(f, "   > PrintFitInit(f);\n");
   PrintFitInit(f);
   fprintf(f, "   > SetFitInit(\"FIXED,LOG,[5,25],20,1.3\", f);\n");
   SetFitInit("FIXED,LOG,[5,25],20,1.3", f);
   fprintf(f, "   > PrintFitInit(f);\n");
   PrintFitInit(f);
   fprintf(f, "   > SetFitValBest(10.);\n");
   SetFitValBest(10.);
   fprintf(f, "   > SetFitValErrAsymm(-1.9e-3/*lo*/, 2.1e-3 */up*/);\n");
   SetFitValErrAsymm(-1.9e-3, 2.1e-3);
   fprintf(f, "   > CopyFitValBestInVal()\n");
   CopyFitValBestInVal();
   fprintf(f, "   > PrintFitValsAndInit(f);\n");
   PrintFitValsAndInit(f);
   fprintf(f, "   > SetFitType(kFIT);\n");
   SetFitType(kFIT);
   fprintf(f, "   > PrintFitValsAndInit(f);\n");
   PrintFitValsAndInit(f);
   fprintf(f, "   > Print(f);\n");
   Print(f);
   fprintf(f, "\n");
   fprintf(f, "\n");

   // Copy
   fprintf(f, " * Test Copy method:\n");
   fprintf(f, "   > TUFreeParEntry par; par.Copy(*this);\n");
   TUFreeParEntry par;
   par.Copy(*this);
   fprintf(f, "   > par.Print(f);\n");
   par.Print(f);
   fprintf(f, "   > par.PrintFitInit(f);\n");
   par.PrintFitInit(f);
   fprintf(f, "   > par.PrintFitVals(f);\n");
   par.PrintFitVals(f);
   fprintf(f, "\n");
}
