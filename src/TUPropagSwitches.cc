// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

// C/C++ include
#include <iostream>
// ROOT include
// USINE include
#include "../include/TUMessages.h"
#include "../include/TUPropagSwitches.h"

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TUPropagSwitches                                                     //
//                                                                      //
// Propagation options activated in USINE (decay, energy losses...).    //
//                                                                      //
// The class TUPropagSwitches provides a list of options to             //
// switch on or off for a propagation run. These options include        //
//    - BETA and EC decay;
//    - Inelastic interactions (IsDestruction);
//    - Energy losses (matter- or wind-related) + gain (reacceleration);
//    - Primary (standard and exotic) + secondary contributions.
// The options are set from the USINE initialisation file.              //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

ClassImp(TUPropagSwitches)

//______________________________________________________________________________
TUPropagSwitches::TUPropagSwitches()
{
   // ****** Default constructor ******

   Initialise();
}

//______________________________________________________________________________
TUPropagSwitches::TUPropagSwitches(TUPropagSwitches const &prop_switch)
{
   // ****** Copy constructor ******
   //  prop_switch          Pointer used to fill class

   Initialise();
   Copy(prop_switch);
}

//______________________________________________________________________________
TUPropagSwitches::TUPropagSwitches(TUInitParList *init_pars, Bool_t is_verbose, FILE *f_log)
{
   // ****** Default constructor ******
   //  init_pars         TUInitParList class of initialisation parameters
   //  is_verbose        Verbose or not when class is set
   //  f_log             Log for USINE

   Initialise();
   SetClass(init_pars, is_verbose, f_log);
}

//______________________________________________________________________________
TUPropagSwitches::~TUPropagSwitches()
{
   // ****** Default destructor ******

}

//______________________________________________________________________________
void TUPropagSwitches::Copy(TUPropagSwitches const &prop_switch)
{
   //--- Copies in member.
   //  prop_switch      Object to copy from

   for (Int_t i = 0; i < GetNSwitches(); ++i) {
      fIsOn[i] = prop_switch.IsOn(i);
   }
   fStatus = true;
}

//______________________________________________________________________________
void TUPropagSwitches::ExtractConfigurations(TUPropagSwitches const &on_off, vector<TUPropagSwitches> &list) const
{
   //--- Extracts from the list of switches on_off (to set to on and then off),
   //    all combinations of the configurations possible, using as the base
   //    configuration the current one.
   // INPUT:
   //  on_off           Switches to set to on and off
   // OUTPUT:
   //  list             List of configurations

   list.clear();

   TUPropagSwitches tmp;
   tmp.Copy(*this);

   vector<Bool_t> is_mod_ref(GetNSwitches(), false);
   vector<Int_t> i_index(GetNSwitches(), 0);
   for (Int_t i = 0; i < GetNSwitches(); ++i) {
      if (on_off.IsOn(i))
         is_mod_ref[i] = true;
   }

   // Loop on all combinations of possible switches
   for (i_index[0] = (Int_t)on_off.IsOn(0); i_index[0] >= 0 ; --i_index[0]) {
      for (i_index[1] = (Int_t)on_off.IsOn(1); i_index[1] >= 0 ; --i_index[1]) {
         for (i_index[2] = (Int_t)on_off.IsOn(2); i_index[2] >= 0 ; --i_index[2]) {
            for (i_index[3] = (Int_t)on_off.IsOn(3); i_index[3] >= 0 ; --i_index[3]) {
               for (i_index[4] = (Int_t)on_off.IsOn(4); i_index[4] >= 0 ; --i_index[4]) {
                  for (i_index[5] = (Int_t)on_off.IsOn(5); i_index[5] >= 0 ; --i_index[5]) {
                     for (i_index[6] = (Int_t)on_off.IsOn(6); i_index[6] >= 0 ; --i_index[6]) {
                        for (i_index[7] = (Int_t)on_off.IsOn(7); i_index[7] >= 0 ; --i_index[7]) {
                           for (i_index[8] = (Int_t)on_off.IsOn(8); i_index[8] >= 0 ; --i_index[8]) {
                              for (i_index[9] = (Int_t)on_off.IsOn(9); i_index[9] >= 0 ; --i_index[9]) {
                                 for (i_index[10] = (Int_t)on_off.IsOn(10); i_index[10] >= 0 ; --i_index[10]) {
                                    for (i_index[11] = (Int_t)on_off.IsOn(11); i_index[11] >= 0 ; --i_index[11]) {
                                       for (i_index[12] = (Int_t)on_off.IsOn(12); i_index[12] >= 0 ; --i_index[12]) {
                                          for (i_index[13] = (Int_t)on_off.IsOn(13); i_index[13] >= 0 ; --i_index[13]) {
                                             for (i_index[14] = (Int_t)on_off.IsOn(14); i_index[14] >= 0 ; --i_index[14]) {
                                                for (i_index[15] = (Int_t)on_off.IsOn(15); i_index[15] >= 0 ; --i_index[15]) {
                                                   for (i_index[16] = (Int_t)on_off.IsOn(16); i_index[16] >= 0 ; --i_index[16]) {
                                                      // Set the right propagation mode
                                                      for (Int_t i = 0; i < GetNSwitches(); ++i) {
                                                         if (is_mod_ref[i])
                                                            tmp.SetSwitch(i, (1 - i_index[i]));
                                                      }
                                                      //tmp.PrintPropagSwitches();
                                                      list.push_back(tmp);
                                                   }
                                                }
                                             }
                                          }
                                       }
                                    }
                                 }
                              }
                           }
                        }
                     }
                  }
               }
            }
         }
      }
   }
}

//______________________________________________________________________________
string TUPropagSwitches::ExtractNameSwitch(Int_t index_switch, Int_t d0_root1_short2) const
{
   //--- Gets name for effect index.
   //  index_switch      Index to update
   //  d0_root1_short2   Switch name format
   //                       0: full name
   //                       1: ROOT name (e.g., for legend)
   //                       2  short text name

   if (!CheckIndex(index_switch))
      return "";
   else {
      if (d0_root1_short2 == 0)
         return fNameSwitch[index_switch];
      else if (d0_root1_short2 == 1)
         return fNameSwitchRoot[index_switch];
      else if (d0_root1_short2 == 2)
         return fNameSwitchShort[index_switch];
   }
   return "";
}

//______________________________________________________________________________
string TUPropagSwitches::ExtractNameSwitches(Int_t d0_root1_short2, TUPropagSwitches *ref_config) const
{
   //--- Returns a string summarising (in abbreviated form) the current
   //    mode set for propagation.
   //
   //  d0_root1_short2   Switch name format
   //                       0: full name
   //                       1: ROOT name (e.g., for legend)
   //                       2  short text name
   //  w_or_wo           Names of effects switched on (true) or switched off (false)
   //  ref_config        Reference configuration (if exists)


   // If ref_config given , only provide text w.r.t. changes in ref
   vector<Int_t> indices;
   vector<Bool_t> is_on;
   Bool_t is_same = true;
   if (ref_config) {
      for (Int_t i = 0; i < GetNSwitches(); ++i) {
         if (ref_config->IsOn(i) != IsOn(i)) {
            indices.push_back(i);
            is_on.push_back(IsOn(i));
            Int_t n_on = is_on.size();
            if (n_on > 1 && is_on[n_on - 1] != is_on[n_on - 2])
               is_same = false;
         }
      }
   } else {
      for (Int_t i = 0; i < GetNSwitches(); ++i) {
         if (IsOn(i)) {
            indices.push_back(i);
            is_on.push_back(true);
         }
      }
   }


   // Add effects (with or without)
   string prov = "";
   for (Int_t iii = 0; iii < (Int_t)indices.size(); ++iii) {
      Int_t i = indices[iii];
      string name = ExtractNameSwitch(i, d0_root1_short2);
      if (!is_same) {
         if (is_on[iii])
            name =  "w-" + name;
         else
            name =  "wo-" + name;
      }
      if (prov == "")
         prov = name;
      else
         prov = prov + "/" + name;
   }
   if (prov != "") {
      if (is_same) {
         if (is_on[0])
            prov = "with " + prov;
         else
            prov = "no " + prov;
      }
      prov = "[" + prov + "]";
   } else
      prov = "[ref]";
   return prov;
}

//______________________________________________________________________________
void TUPropagSwitches::Initialise()
{
   //--- Initialises class members.

   // Clear content
   fIsOn.clear();
   fNameSwitch.clear();
   fNameSwitchRoot.clear();
   fNameSwitchShort.clear();

   // Add effects
   // 0:
   fNameSwitch.push_back("Primary (standard) contrib.");
   fNameSwitchRoot.push_back("SrcPrim");
   fNameSwitchShort.push_back("srcprim");
   fIsOn.push_back(true);
   // 1: Secondaries
   fNameSwitch.push_back("Secondary contribution");
   fNameSwitchRoot.push_back("SrcSec");
   fNameSwitchShort.push_back("srcsec");
   fIsOn.push_back(true);
   // 2: Destruction
   fNameSwitch.push_back("Inelastic inter. (destruct.)");
   fNameSwitchRoot.push_back("#Gamma_{inel}");
   fNameSwitchShort.push_back("gamma_inel");
   fIsOn.push_back(true);
   // 3: ELossBrem
   fNameSwitch.push_back("Bremsstrahlung losses");
   fNameSwitchRoot.push_back("E^{loss}_{Bremss}");
   fNameSwitchShort.push_back("ElossBremss");
   fIsOn.push_back(true);
   // 4: ELossCoulomb
   fNameSwitch.push_back("Coulomb losses");
   fNameSwitchRoot.push_back("E^{loss}_{Coul}");
   fNameSwitchShort.push_back("ElossCoulomb");
   fIsOn.push_back(true);
   // 5: ELossIC
   fNameSwitch.push_back("Inverse Compton losses");
   fNameSwitchRoot.push_back("E^{loss}_{IC}");
   fNameSwitchShort.push_back("ElossIC");
   fIsOn.push_back(true);
   // 6: ELossIon
   fNameSwitch.push_back("Ionisation losses");
   fNameSwitchRoot.push_back("E^{loss}_{Ion}");
   fNameSwitchShort.push_back("ElossIon");
   fIsOn.push_back(true);
   // 7: ELossSynch
   fNameSwitch.push_back("Synchrotron losses");
   fNameSwitchRoot.push_back("E^{loss}_{Synch}");
   fNameSwitchShort.push_back("ElossSynch");
   fIsOn.push_back(true);
   // 8: ELossAdiabatic
   fNameSwitch.push_back("Adiabatic losses (if Vgal)");
   fNameSwitchRoot.push_back("E^{loss}_{wind}");
   fNameSwitchShort.push_back("Ewind");
   fIsOn.push_back(true);
   // 9: EReacceleration
   fNameSwitch.push_back("Reacceleration");
   fNameSwitchRoot.push_back("E^{reac}");
   fNameSwitchShort.push_back("Ereac");
   fIsOn.push_back(true);
   //10: DecayBETA
   fNameSwitch.push_back("BETA-decay");
   fNameSwitchRoot.push_back("#beta");
   fNameSwitchShort.push_back("beta");
   fIsOn.push_back(true);
   //11: DecayEC
   fNameSwitch.push_back("EC-decay");
   fNameSwitchRoot.push_back("EC");
   fNameSwitchShort.push_back("EC");
   fIsOn.push_back(true);
   //12: DecayFedBETA
   fNameSwitch.push_back("BETA-fed");
   fNameSwitchRoot.push_back("#beta-fed");
   fNameSwitchShort.push_back("beta-fed");
   fIsOn.push_back(true);
   //13: DecayFedEC
   fNameSwitch.push_back("EC-fed");
   fNameSwitchRoot.push_back("EC-fed");
   fNameSwitchShort.push_back("EC-fed");
   fIsOn.push_back(true);
   //14: Tertiaries
   fNameSwitch.push_back("Tertiary contribution");
   fNameSwitchRoot.push_back("SrcTert");
   fNameSwitchShort.push_back("srctert");
   fIsOn.push_back(true);
   //15: PrimExotic
   fNameSwitch.push_back("Exotic contribution");
   fNameSwitchRoot.push_back("SrcExotic");
   fNameSwitchShort.push_back("srcexotic");
   fIsOn.push_back(true);
   //16: Wind
   fNameSwitch.push_back("Wind");
   fNameSwitchRoot.push_back("Wind");
   fNameSwitchShort.push_back("Wind");
   fIsOn.push_back(true);
}

//______________________________________________________________________________
void TUPropagSwitches::PrintPropagSwitches(FILE *f, Bool_t is_comparison) const
{
   //--- Prints propagation mode in file f.
   //  f                 File in which to print
   //  is_comparison     Standard print or comparison mode print

   string indent = TUMessages::Indent(true);

   // Switches status
   fprintf(f, "%s   --------------------------------\n", indent.c_str());
   for (Int_t i = 0; i < GetNSwitches(); ++i) {
      if (IsOn(i)) {
         if (is_comparison)
            fprintf(f, "%s   %2d. Switch to compare w/wo %-28s is ON\n", indent.c_str(), i + 1, fNameSwitch[i].c_str());
         else
            fprintf(f, "%s   %2d. %-28s ON\n", indent.c_str(), i + 1, fNameSwitch[i].c_str());
      } else {
         if (is_comparison)
            fprintf(f, "%s   %2d. Switch to compare w/wo %-28s is OFF\n", indent.c_str(), i + 1, fNameSwitch[i].c_str());
         else
            fprintf(f, "%s   %2d. %-28s OFF\n", indent.c_str(), i + 1, fNameSwitch[i].c_str());
      }

   }
   fprintf(f, "%s   --------------------------------\n", indent.c_str());

   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUPropagSwitches::SetAllSwitches(Bool_t true_or_false)
{
   //--- Sets all values to true or false.
   //  true_or_false     Value to set

   for (Int_t i = 0; i < GetNSwitches(); ++i)
      fIsOn[i] = true_or_false;
}

//______________________________________________________________________________
void TUPropagSwitches::SetClass(TUInitParList *init_pars, Bool_t is_verbose, FILE *f_log)
{
   //--- Updates class (fills list of CRs).
   //  init_pars         TUInitParList class of initialisation parameters
   //  is_verbose        Verbose or not when class is set
   //  f_log             Log for USINE

   string group = "UsineRun", subgroup = "OnOff";
   string indent = TUMessages::Indent(true);
   if (is_verbose) {
      fprintf(f_log, "%s[TUPropagSwitches::SetClass]\n", indent.c_str());
      fprintf(f_log, "%s### Set propagation scheme (take or not into account energy losses, reacceleration, beta-contributions,...) [%s#%s#Is...]\n",
              indent.c_str(), group.c_str(), subgroup.c_str());
   }

   SetIsDecayBETA(TUMisc::String2Bool(init_pars->GetParEntry(init_pars->IndexPar(group, subgroup, "IsDecayBETA")).GetVal().c_str()));
   SetIsDecayEC(TUMisc::String2Bool(init_pars->GetParEntry(init_pars->IndexPar(group, subgroup, "IsDecayEC")).GetVal().c_str()));
   SetIsDecayFedBETA(TUMisc::String2Bool(init_pars->GetParEntry(init_pars->IndexPar(group, subgroup, "IsDecayFedBETA")).GetVal().c_str()));
   SetIsDecayFedEC(TUMisc::String2Bool(init_pars->GetParEntry(init_pars->IndexPar(group, subgroup, "IsDecayFedEC")).GetVal().c_str()));
   SetIsDestruction(TUMisc::String2Bool(init_pars->GetParEntry(init_pars->IndexPar(group, subgroup, "IsDestruction")).GetVal().c_str()));
   SetIsELossAdiabatic(TUMisc::String2Bool(init_pars->GetParEntry(init_pars->IndexPar(group, subgroup, "IsELossAdiabatic")).GetVal().c_str()));
   SetIsELossBremsstrahlung(TUMisc::String2Bool(init_pars->GetParEntry(init_pars->IndexPar(group, subgroup, "IsELossBremss")).GetVal().c_str()));
   SetIsELossCoulomb(TUMisc::String2Bool(init_pars->GetParEntry(init_pars->IndexPar(group, subgroup, "IsELossCoulomb")).GetVal().c_str()));
   SetIsELossIon(TUMisc::String2Bool(init_pars->GetParEntry(init_pars->IndexPar(group, subgroup, "IsELossIon")).GetVal().c_str()));
   SetIsELossIC(TUMisc::String2Bool(init_pars->GetParEntry(init_pars->IndexPar(group, subgroup, "IsELossIC")).GetVal().c_str()));
   SetIsELossSynchrotron(TUMisc::String2Bool(init_pars->GetParEntry(init_pars->IndexPar(group, subgroup, "IsELossSynchrotron")).GetVal().c_str()));
   SetIsEReacceleration(TUMisc::String2Bool(init_pars->GetParEntry(init_pars->IndexPar(group, subgroup, "IsEReacc")).GetVal().c_str()));
   SetIsPrimExotic(TUMisc::String2Bool(init_pars->GetParEntry(init_pars->IndexPar(group, subgroup, "IsPrimExotic")).GetVal().c_str()));
   SetIsPrimStandard(TUMisc::String2Bool(init_pars->GetParEntry(init_pars->IndexPar(group, subgroup, "IsPrimStandard")).GetVal().c_str()));
   SetIsSecondaries(TUMisc::String2Bool(init_pars->GetParEntry(init_pars->IndexPar(group, subgroup, "IsSecondaries")).GetVal().c_str()));
   SetIsTertiaries(TUMisc::String2Bool(init_pars->GetParEntry(init_pars->IndexPar(group, subgroup, "IsTertiaries")).GetVal().c_str()));
   SetIsWind(TUMisc::String2Bool(init_pars->GetParEntry(init_pars->IndexPar(group, subgroup, "IsWind")).GetVal().c_str()));

   if (is_verbose) {
      PrintPropagSwitches(f_log);
      fprintf(f_log, "%s[TUPropagSwitches::SetClass] <DONE>\n\n", indent.c_str());
   }
   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUPropagSwitches::SetSwitch(Int_t index_switch, Bool_t is)
{
   //--- Updates switch from index.
   //  index_switch      Index to update
   //  is                True or false

   if (CheckIndex(index_switch)) {
      if (fIsOn[index_switch] != is)
         fStatus = true;
      fIsOn[index_switch] = is;
   }
}

//______________________________________________________________________________
void TUPropagSwitches::TEST(TUInitParList *init_pars, FILE *f)
{
   //--- Test function for this class (called in usine -t).
   //  init_pars         TUInitParList class of initialisation parameters
   //  f                 Output file for test

   string message =
      "Switches for propagation options (decay, energy losses,...).";
   TUMessages::Test(f, "TUPropagSwitches", message);

   // Set class and summary content
   fprintf(f, "   > SetClass(init_pars=\"%s\", is_verbose=false, f_log=f);\n", init_pars->GetFileNames().c_str());
   SetClass(init_pars, false, f);
   fprintf(f, "   > PrintPropagSwitches(f);\n");
   PrintPropagSwitches(f);
   fprintf(f, "   > ExtractNameSwitches(0);  =>  %s\n", ExtractNameSwitches(0).c_str());
   fprintf(f, "   > SetIsDestruction(kFALSE);\n");
   SetIsDestruction(kFALSE);
   fprintf(f, "   > SetIsEReacceleration(kFALSE);\n");
   SetIsEReacceleration(kFALSE);
   fprintf(f, "   > PrintPropagSwitches(f);\n");
   PrintPropagSwitches(f);
   fprintf(f, "   > ExtractNameSwitches(1);  =>  %s\n", ExtractNameSwitches(1).c_str());
   fprintf(f, "\n");

   fprintf(f, "   > TUI_ModifyPropagSwitches();  [uncomment in TEST() to enable it];\n");
   //TUI_ModifyPropagSwitches();
   fprintf(f, "   > PrintPropagSwitches(f);\n");
   PrintPropagSwitches(f);
   fprintf(f, "   > ExtractNameSwitches(2);  =>  %s\n", ExtractNameSwitches(2).c_str());
   fprintf(f, "\n");
}

//______________________________________________________________________________
void TUPropagSwitches::TUI_ModifyPropagSwitches()
{
   //--- Text User Interface: change propagation switch values (for all models).
   //  is_test           If run in test mode

   string indent = TUMessages::Indent(true);

   string user_choice2;
   do {
      PrintPropagSwitches();

      // Apply change to first model, then apply to all other models
      cout << indent << ">> Select propagation option to switch on/off (Q to save and quit): ";
      cin >> user_choice2;
      Int_t index_switch = atoi(user_choice2.c_str()) - 1;
      if (CheckIndex(index_switch))
         SetSwitch(index_switch, !IsOn(index_switch));

      TUMisc::UpperCase(user_choice2);
   } while (user_choice2 != "Q");

   TUMessages::Indent(false);
}