// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

// C/C++ include
#include <fstream>
// ROOT include
#include <TAxis.h>
#include <TLegend.h>
#include <TLegendEntry.h>
// USINE include
#include "../include/TUDataSet.h"
#include "../include/TUSolMod0DFF.h"

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TUDataSet                                                            //
//                                                                      //
// CR data entries and queries to form subsets (qty, energy range...).  //
//                                                                      //
// The class TUDataSet is based on a list of TUDataEntry. These         //
// data can be read from different ASCII files, as long as they         //
// comply to the USINE format which basically contains qty, exp_name,   //
// energy (mean and bin range), value (ratio/flux and uncertainties),   //
// date/time of observation (for solar modulation), etc. The default    //
// file used is the one generated from the web CR database (see below). //
//                                                                      //
// Once the DataSet is filled, any data subset (also a TUDataSet        //
// object) can be formed from a (comma-separated) list of experiment,   //
// names, energy range, etc. to be printed and/or plotted. Note also    //
// that this class keeps track of the list of independent experiments   //
// (different names and/or flight times). They are denoted 'Exp' in the //
// members and methods of the class below (aka SUB-EXP in CRDB). The    //
// class also lists the indices sorted by exp and quantities loaded     //
// in class (this is also given when a subset is formed).               //
//                                                                      //
// BEGIN_HTML
// <b>Useful link:</b> <a href="http://lpsc.in2p3.fr/crdb/" target="_blank">Cosmic-ray database</a><br>
// <b>To display CR data: <tt>$USINE/bin/usine -id</tt>)
// END_HTML
//////////////////////////////////////////////////////////////////////////

ClassImp(TUDataSet)

//______________________________________________________________________________
TUDataSet::TUDataSet()
{
   // ****** Default constructor ******

   Initialise();
   fIsNuisParsInFit.clear();
   fNuisPars.clear();
   fMapDataIndex2NuisPars.clear();
   fMapNuisPars2ExpQty.clear();
   fMapNuisPars2EType.clear();
}

//______________________________________________________________________________
TUDataSet::TUDataSet(TUDataSet const &dataset)
{
   // ****** Copy constructor ******
   //  dataset         Object to copy

   Initialise();
   fIsNuisParsInFit.clear();
   fNuisPars.clear();
   fMapDataIndex2NuisPars.clear();
   fMapNuisPars2ExpQty.clear();
   fMapNuisPars2EType.clear();
   Copy(dataset);
}

//______________________________________________________________________________
TUDataSet::TUDataSet(TUInitParList *init_pars, Bool_t is_verbose, FILE *f_log)
{
   // ****** Default constructor ******
   //  init_pars         Initialisation parameters
   //  is_verbose        Verbose or not when class is set
   //  f_log             Log for USINE

   Initialise();
   fIsNuisParsInFit.clear();
   fNuisPars.clear();
   fMapDataIndex2NuisPars.clear();
   fMapNuisPars2ExpQty.clear();
   fMapNuisPars2EType.clear();

   SetClass(init_pars, is_verbose, f_log);
}

//______________________________________________________________________________
TUDataSet::~TUDataSet()
{
   // ****** Default destructor ******

   Initialise();
   ParsData_Delete();
}

//______________________________________________________________________________
void TUDataSet::Copy(TUDataSet const &dataset)
{
   //--- Copies from dataset in class.
   //  dataset           Object to copy ifrom

   Initialise();

   // Copy fData[NData]
   for (Int_t i = 0; i < dataset.GetNData(); ++i) {
      TUDataEntry *entry = dataset.GetEntry(i);
      fData.push_back(*entry);
      entry = NULL;
   }

   // Copy fFileNames[NFileNames]
   for (Int_t i = 0; i < dataset.GetNFiles(); ++i)
      fFileNames.push_back(dataset.GetFileName(i));

   // Copy fQtiesEtypesphiFF
   fQtiesEtypesphiFF = dataset.GetQtiesEtypesphiFF();

   // Copy fQty[NQties]
   for (Int_t i = 0; i < dataset.GetNQties(); ++i)
      fQty.push_back(dataset.GetQty(i));

   // Copy fExps[NExps]
   for (Int_t i_exp = 0; i_exp < dataset.GetNExps(); ++i_exp)
      fExps.push_back(dataset.GetExp(i_exp));


   // Copy fDataPerExp[gN_ETYPE][NExps][NDataPerExp]
   // and fDataPerQtyExp[gN_ETYPE][NExps][NQties][NDataPerQtyExp]
   for (Int_t e = 0; e < gN_ETYPE; ++e) {
      gENUM_ETYPE e_type = TUEnum::gENUM_ETYPE_LIST[e];
      for (Int_t i_exp = 0; i_exp < dataset.GetNExps(); ++i_exp) {
         // Copy fDataPerExp[gN_ETYPE][NExp][NDataPerExp]
         vector<Int_t> tmp_exp;
         for (Int_t j = 0; j < dataset.GetNData(i_exp, e_type); ++j)
            tmp_exp.push_back(dataset.IndexData(i_exp, e_type) + j);
         fDataPerExp[e].push_back(tmp_exp);

         // Copy fDataPerQtyExp[gN_ETYPE][NExps][NQties][NDataPerQtyExp]
         vector<vector<Int_t> > tmp2vec;
         for (Int_t i_qty = 0; i_qty < dataset.GetNQties(); ++i_qty) {
            vector<Int_t> tmp1vec;
            for (Int_t j = 0; j < dataset.GetNData(i_exp, i_qty, e_type); ++j)
               tmp1vec.push_back(dataset.IndexData(i_exp, i_qty, e_type) + j);
            tmp2vec.push_back(tmp1vec);
         }
         fDataPerQtyExp[e].push_back(tmp2vec);
      }
   }
}

//______________________________________________________________________________
void TUDataSet::FillExpsAndDataPerExp()
{
   ///--- Returns for the class the indices of non-identical experiment names,
   //     and for each of them, finds the indices of all corresponding data in
   //     the dataset.

   // Fill indices for non-identical exp-names/datimes
   fExps.clear();
   if (GetNData() == 0)
      return;

   // Fill experiment names
   for (Int_t i = 0; i < GetNData(); ++i)
      fExps.push_back(fData[i].GetExpName());
   TUMisc::RemoveDuplicates(fExps);


   // Fill indices of data for each indices_exp
   for (Int_t e = 0; e < gN_ETYPE; ++e) {
      fDataPerExp[e].clear();
      for (Int_t i_exp = 0; i_exp < GetNExps(); ++i_exp) {
         vector<Int_t> tmp;
         for (Int_t i_data = 0; i_data < GetNData(); ++i_data) {
            if (fData[i_data].GetExpName() == GetExp(i_exp)
                  && fData[i_data].GetEType() == TUEnum::gENUM_ETYPE_LIST[e])
               tmp.push_back(i_data);
         }
         fDataPerExp[e].push_back(tmp);
      }
   }
}

//______________________________________________________________________________
void TUDataSet::FillQtiesAndDataPerQtyExp()
{
   //--- Fills fQty (list of all qties in class) and fDataPerQtyExp.

   // Fill fQty
   fQty.clear();

   string qty = fData[0].GetQty();
   TUMisc::UpperCase(qty);
   fQty.push_back(qty);
   for (Int_t i_data = 1; i_data < GetNData(); ++i_data) {
      qty = fData[i_data].GetQty();
      if (!TUMisc::IsInList(fQty, qty, true))
         fQty.push_back(qty);
   }

   // Fill indices of exp/qti/data
   for (Int_t e = 0; e < gN_ETYPE; ++e) {
      fDataPerQtyExp[e].clear();
      // Loop on exp
      for (Int_t i_exp = 0; i_exp < GetNExps(); ++i_exp) {
         vector<vector<Int_t> > tmp;
         // Loop on all qties in fQty
         for (Int_t i_qty = 0; i_qty < GetNQties(); ++i_qty) {
            vector<Int_t> data;
            // Search for data in expdata matching qty name
            for (Int_t i_expdata = 0; i_expdata < GetNData(i_exp, TUEnum::gENUM_ETYPE_LIST[e]); ++i_expdata) {
               Int_t i_data = fDataPerExp[e][i_exp][i_expdata];
               string qty2 = fData[i_data].GetQty();
               TUMisc::UpperCase(qty2);
               if (fQty[i_qty] == qty2)
                  data.push_back(i_data);
            }
            //N.B.: even if empty, we need to add it!
            tmp.push_back(data);
         }
         fDataPerQtyExp[e].push_back(tmp);
      }
   }
}

//______________________________________________________________________________
vector<Double_t> TUDataSet::GetERange(gENUM_CRFAMILY cr_family) const
{
   //--- Returns a vector of 2 values (Emin and Emax in 'native units) over all
   //    CR data in class belonging top this family.
   //  gENUM_CRFAMILY    kNUC, kANTINUC, kLEPTON (see TUEnum.h)

   // Initialise min and max
   vector<Double_t> min_max;
   min_max.push_back(1.e40);
   min_max.push_back(-1.e40);

   // Loop on all data in class
   for (Int_t i_data = 0; i_data < GetNData(); ++i_data) {

      // Find families involved in the i-th data
      gENUM_CRFAMILY family = TUEnum::ExtractCRFamily(fData[i_data].GetQty());
      // If matches cr_family asked for, check its energy
      if (family == cr_family) {
         if (fData[i_data].GetEMean() < min_max[0]) min_max[0] = fData[i_data].GetEMean();
         if (fData[i_data].GetEMean() > min_max[1]) min_max[1] = fData[i_data].GetEMean();
      }
   }
   return min_max;
}

//______________________________________________________________________________
Int_t TUDataSet::IndexData(TUDataEntry const &entry) const
{
   //--- Finds and returns index of 'entry' in current dataset (-1 if not found).
   //   entry            Data entry to seek

   for (Int_t k = 0; k < GetNData(); ++k) {
      if (fData[k].IsSameEntry(entry))
         return k;
   }
   return -1;
}

//______________________________________________________________________________
Int_t TUDataSet::IndexDataClosestMatch(string const &qty, string const &l_exp, Double_t e_val, gENUM_ETYPE e_axis, FILE *f) const
{
   //--- Returns data entry index (-1 if not found) matching qty and the closest
   //    to e_val=ekn among experiment names matching (even partially) names given
   //    in l_exp (beware that if some exps share similar substrings, you might end
   //    up with other data than those you have asked for!).
   //  qty               Quantity to look for (e.g., B/C, O...)
   //  l_exp             Comma-separated (case-insensitive) list of exps ("ALL" for all exp.)
   //  e_val             Energy value to match
   //  e_axis            Type of x-axis (see gENUM_ETYPE in TUEnum.h)
   //  f                 Output file for chatter

   // Get first subset of data that matches quantity:experiment for a given e_axis
   string qty_exp_type = qty + ":" + l_exp + ":" + TUEnum::Enum2Name(e_axis);

   TUDataSet *subset = OrphanFormSubSet(qty_exp_type, f);
   if (subset->GetNData() < 1) {
      string message = "no data found for " + qty
                       + " in the experiment list " + l_exp
                       + " for e_axis=" + TUEnum::Enum2Name(e_axis);
      TUMessages::Warning(f, "TUDataSet", "GetClosestMatch", message);
      delete subset;
      subset = NULL;
      return -1;
   }

   Double_t dx_best = 1.e30;
   Int_t i_best = -1;
   for (Int_t i_data = 0; i_data < subset->GetNData(); ++i_data) {
      Double_t x_i =  subset->GetEntry(i_data)->GetEMean();
      Double_t dx = fabs(x_i - e_val);
      if (dx < dx_best) {
         i_best = i_data;
         dx_best = dx;
      }
   }

   Int_t i_data = IndexData(*subset->GetEntry(i_best));
   delete subset;
   subset = NULL;
   return i_data;
}

//______________________________________________________________________________
Int_t TUDataSet::IndexInExps(string exp_name) const
{
   //--- Returns index of experiment corresponding to exp_name (-1 if not found).
   //  exp_name          Name of experiment to find

   TUMisc::UpperCase(exp_name);
   for (Int_t i_exp = 0; i_exp < GetNExps(); ++i_exp) {
      string exp = GetExp(i_exp);
      TUMisc::UpperCase(exp);
      if (exp == exp_name)
         return i_exp;
   }
   return -1;
}

//______________________________________________________________________________
Int_t TUDataSet::IndexInQties(string qty) const
{
   //--- Returns index in fQty for qty (-1 if not found).
   //  qty               Name of qty to find

   TUMisc::UpperCase(qty);
   for (Int_t i = 0; i < GetNQties(); ++i) {
      string tmp = fQty[i];
      TUMisc::UpperCase(tmp);
      if (qty == tmp)
         return i;
   }
   return -1;
}

//______________________________________________________________________________
vector<Int_t> TUDataSet::IndicesData(string const &qty) const
{
   //---Returns a vector of data indices for sub-exp that measured a given qty.
   //  qty               Quantity measured

   string qty_uc = qty;
   TUMisc::UpperCase(qty_uc);
   vector<Int_t> list;
   for (Int_t i_exp = 0; i_exp < GetNExps(); ++i_exp) {
      for (Int_t e = 0; e < gN_ETYPE; ++e) {
         gENUM_ETYPE etype = TUEnum::gENUM_ETYPE_LIST[e];
         for (Int_t i = 0; i < GetNData(i_exp, etype); ++i) {
            string qty_exp = fData[fDataPerExp[e][i_exp][i]].GetQty();
            TUMisc::UpperCase(qty_exp);
            if (qty_uc == qty_exp) {
               list.push_back(i_exp);
               break;
            }
         }
      }
   }
   TUMisc::RemoveDuplicates(list);
   return list;
}

//______________________________________________________________________________
void TUDataSet::IndicesData4AllExpQty(gENUM_ETYPE etype, vector<pair<Int_t, Int_t> > &idata_start_stop) const
{
   //--- Fills indices of quantities (for which data are found) for this experiment.
   //  INPUT:
   //   etype            E-type of data to collect
   // OUTPUT:
   //   idata_start_stop Vector of pair of incices (first/last data) for each Exp/Qty/Etype

   idata_start_stop.clear();

   // Loop on all experiments
   for (Int_t i_exp = 0; i_exp < GetNExps(); ++i_exp) {
      //cout << "i_exp=" << i_exp << "/" << GetNExps() << " => " << GetExp(i_exp) << endl;

      // Get all quantities for this experiment
      vector<Int_t> indices_qties;
      IndicesQtiesInExp(i_exp, indices_qties);
      // Loop on all quantities
      for (Int_t i = 0; i < (Int_t)indices_qties.size(); ++ i) {
         Int_t i_qty = indices_qties[i];

         pair<Int_t, Int_t> start_stop;
         start_stop.first = IndexData(i_exp, i_qty, etype);
         if (start_stop.first < 0)
            continue;
         start_stop.second = start_stop.first + GetNData(i_exp, i_qty, etype) - 1;
         if (start_stop.second < 0)
            continue;
         idata_start_stop.push_back(start_stop);
         //cout << "    i_qty=" << i_qty << " => " << GetQty(i_qty) << "  NData=" << start_stop.second << endl;
         //GetEntry(start_stop.first)->Print();
         //GetEntry(start_stop.second)->Print();
         //cout << "nsets=" << idata_start_stop.size() << endl;
      }
   }
}

//______________________________________________________________________________
void TUDataSet::IndicesQtiesInExp(Int_t i_exp, vector<Int_t> &indices_qties) const
{
   //--- Fills indices of quantities (for which data are found) for this experiment.
   //  INPUT:
   //   i_exp            Index of experiment
   //  OUTPUT
   //   indices_qties    Indices of quantities for which there are data for this exp

   indices_qties.clear();
   for (Int_t i_qty = 0; i_qty < GetNQties(); ++i_qty) {
      for (Int_t e = 0; e < gN_ETYPE; ++e) {
         gENUM_ETYPE etype = TUEnum::gENUM_ETYPE_LIST[e];
         if (GetNData(i_exp, i_qty, etype) > 0)
            indices_qties.push_back(i_qty);
      }
   }
   TUMisc::RemoveDuplicates(indices_qties);
}

//______________________________________________________________________________
void TUDataSet::Initialise()
{
   //--- Empty all vectors.

   fData.clear();
   for (Int_t e = 0; e < gN_ETYPE; ++e) {
      fDataPerExp[e].clear();
      fDataPerQtyExp[e].clear();
   }
   fFileNames.clear();
   fExps.clear();
   fQty.clear();
   fQtiesEtypesphiFF = "";
}

//______________________________________________________________________________
string TUDataSet::ListOfExpphi() const
{
   //--- Returns comma-separated list of phi values for all experiments.

   vector<Double_t> phi;
   for (Int_t i_exp = 0; i_exp < GetNExps(); ++i_exp) {
      for (Int_t e = 0; e < gN_ETYPE; ++e) {
         gENUM_ETYPE etype = TUEnum::gENUM_ETYPE_LIST[e];
         phi.push_back(fData[IndexData(i_exp, etype)].GetExpphi());
      }
   }

   TUMisc::RemoveDuplicates(phi, 1.e-3);
   return TUMisc::List2String(phi, ',', 3);
}

//______________________________________________________________________________
string TUDataSet::ListOfExpphi(string const &qties_exps_etype, FILE *f,
                               Double_t e_min, Double_t e_max,
                               string const &t_start, string const &t_stop) const
{
   //--- Returns comma-separated list of experiments phi for a single combo of qties_expnames
   //  qties_exps_etype       Single combo for qties:exp_names:etype (e.g. B/C:HEAO,VOYAGER:kEKN)
   //  f                      Output file for chatter
   //  e_min                  Lower range of x-axis interval (default = 1.e-40)
   //  e_max                  Upper range of x-axis interval (default = 1.e40)
   //  t_start                Min date allowed (format is 'YYYY-MM-DD HH:MN:SS', default is "1940-01-01 00:00:00")
   //  t_stop                 Max date allowed (format is 'YYYY-MM-DD HH:MN:SS', default is "2100-01-01 00:00:00")

   // Split "qties:expnames:etype"
   vector<string> l_tmp;
   TUMisc::String2List(qties_exps_etype, ":", l_tmp);
   if (l_tmp.size() != 3) {
      string message = "Expected format for \'qties_exps_etype\' is  \'commasep_qties:commasep_exps:etype\', but is " + qties_exps_etype;
      TUMessages::Error(f, "TUDataSet", "ListOfExpphi", message);
   }

   vector<string> l_qties;
   TUMisc::String2List(l_tmp[0], ",", l_qties);
   vector<string> l_exp;
   TUMisc::String2List(l_tmp[1], ",", l_exp);
   gENUM_ETYPE e_type;
   TUEnum::Name2Enum(l_tmp[2], e_type);

   vector<Double_t> l_phiff;
   // Loop on quantities
   for (Int_t i = 0; i < (Int_t)l_qties.size(); ++i) {

      // If qty not in list, skip
      Int_t i_qty = IndexInQties(l_qties[i]);
      if (i_qty < 0)
         continue;

      // Loop on all experiments
      TUMisc::UpperCase(l_tmp[1]);
      for (Int_t i_exp = 0; i_exp < GetNExps(); ++i_exp) {
         Int_t i_data = IndexData(i_exp, i_qty, e_type);
         if (i_data < 0)
            continue;

         Int_t i_data_last = i_data + GetNData(i_exp, i_qty, e_type) - 1;
         if ((fData[i_data_last].GetEMean() < e_min) || (fData[i_data].GetEMean() > e_max))
            continue;

         // Check whether fData[i_data] is in the right time range
         TUDatime exp_datime_start = fData[i_data].GetExpDatime(0);
         TUDatime exp_datime_stop = fData[i_data].GetExpDatime(fData[i_data].GetNDatimes() - 1);
         if (exp_datime_start.GetStart() < t_start || exp_datime_stop.GetStop() > t_stop)
            continue;

         // Special case if ALL exps
         Bool_t is_add = false;
         if (l_tmp[1] == "ALL")
            is_add = true;
         else {
            // Loop on sought names
            for (Int_t j = 0; j < (Int_t)l_exp.size(); ++j) {
               // We have to account for the fact that the experiment
               // name might be just part of the full name
               if (GetExp(i_exp).find(l_exp[j]) != string::npos) {
                  is_add = true;
                  break;
               }
            }
         }
         if (!is_add)
            continue;

         l_phiff.push_back(fData[i_data].GetExpphi());
      }
   }
   // Remove duplicated (within threshold)
   TUMisc::RemoveDuplicates(l_phiff, 1.e-3);
   return TUMisc::List2String(l_phiff, ',', 3);
}

//______________________________________________________________________________
string TUDataSet::ListOfQties(Int_t i_exp, gENUM_ETYPE etype) const
{
   //--- Returns a comma-separated list of the quantities measured in a given sub-exp.
   //  i_exp             Index of exp in fExps
   //  etype             E-type of data to collect

   vector<string> list;
   string qty = fData[fDataPerExp[etype][i_exp][0]].GetQty();
   TUMisc::UpperCase(qty);
   list.push_back(qty);
   for (Int_t i = 1; i < GetNData(i_exp, etype); ++i) {
      qty = fData[fDataPerExp[etype][i_exp][i]].GetQty();
      TUMisc::UpperCase(qty);
      for (Int_t j = 0; j < (Int_t)list.size(); ++j) {
         if (qty == list[j])
            break;
         else if (j == (Int_t)list.size() - 1)
            list.push_back(qty);
      }
   }
   TUMisc::RemoveDuplicates(list);
   return TUMisc::List2String(list, ',');
}

//______________________________________________________________________________
void TUDataSet::LoadData(string const &filename, Bool_t is_replace_or_add, Bool_t is_verbose, FILE *f_log)
{
   //--- Fills (or adds in) current class all data found in file.
   //  filename          File name of data to read
   //  is_replace_or_add Whether to add the data (to existing ones) or replace them
   //  is_verbose        Verbose or not when class is set
   //  f_log             Log for USINE

   // If input file exist, read it (else abort)
   string f_name = TUMisc::GetPath(filename);
   ifstream f_read(f_name.c_str());
   TUMisc::IsFileExist(f_read, f_name);

   // If replace, clear all filled data
   if (is_replace_or_add)
      Initialise();


   // Find if $USINE, if yes, we replace it in file
   string file = filename;
   TUMisc::SubstituteEnvInPath(file, "$USINE");


   // Check if filename previously filled
   for (Int_t i = 0; i < GetNFiles(); ++i) {
      if (file == fFileNames[i]) {
         string message = file + " was previously read, nothing to do!";
         TUMessages::Warning(f_log, "TUDataSet", "LoadData", message);
         return;
      }
   }

   fFileNames.push_back(file);

   string indent = TUMessages::Indent(true);
   if (is_verbose)
      fprintf(f_log, "%s   => Read %s\n", indent.c_str(), file.c_str());

   // Read data (line by line)
   string tmp_line;
   Int_t n_lines = 0;
   while (getline(f_read, tmp_line)) {
      ++ n_lines;
      string line = TUMisc::RemoveBlancksFromStartStop(tmp_line);
      if (line[0] == '#' || line.empty()) continue;

      TUDataEntry entry;
      entry.SetEntry(line, f_log);
      fData.push_back(entry);
   }

   // And fill exp indices to ease further data retrieving
   FillExpsAndDataPerExp();
   FillQtiesAndDataPerQtyExp();
   TUMessages::Indent(false);
}

//______________________________________________________________________________
Bool_t TUDataSet::LoadDataRelCov(Int_t i_data_start, Int_t i_data_stop, string const &file_relcov, FILE *f_log) const
{

   //--- Fills data covariance matrix of relative errors [i_start_data,i_stop_data] from file file_relcov.
   //  i_data_start      Index of first data for exp/qty/etype (should correspond to lowest energy point)
   //  i_data_stop       Index of last data for exp/qty/etype (should correspond to lowest energy point)
   //  file_relcov       File to use/search for to load covariance matrix of data relative errors
   //  f_log             Log for USINE

   // If file does not exist, nothing to do
   ifstream f_read(TUMisc::GetPath(file_relcov).c_str());
   if (!TUMisc::IsFileExist(f_read, file_relcov, false))
      return false;

   // Read file (if reach this point, file exists)
   Int_t ne_cov = i_data_stop - i_data_start + 1;
   vector<Double_t> cov(ne_cov * ne_cov, 0.);
   vector<vector<Double_t> > cov_all;
   vector<string> err_types;
   string line;
   Int_t i_line = 0;
   Int_t n_covlines = 0;

   while (getline(f_read, line)) {
      i_line += 1;
      // if it is a comment (start with #) or a blanck line, skip it
      string line_trimmed = TUMisc::RemoveBlancksFromStartStop(line);
      if (line_trimmed[0] == '#' || line_trimmed.empty()) continue;
      // Check if err.type (line is 'ErrorType: name')
      if (line.find("ErrorType:") != string::npos) {
         // Check correctly formatted
         vector<string> dummy_name;
         TUMisc::String2List(line, " ", dummy_name);
         if (dummy_name.size() != 2) {
            string message = Form("Line %d: expect \"ErrorType: name\", found %s", i_line, line.c_str());
            TUMessages::Error(f_log, "TUDataSet", "LoadDataRelCov", message);
         } else {
            // Check only once in file
            TUMisc::UpperCase(dummy_name[1]);
            if (TUMisc::IsInList(err_types, dummy_name[1], true)) {
               string message = Form("Error type %s found twice in file...", dummy_name[1].c_str());
               TUMessages::Error(f_log, "TUDataSet", "LoadDataRelCov", message);
            } else err_types.push_back(dummy_name[1]);
         }
         // If reach this line, valid error type: fill data after second time
         if (err_types.size() > 1) {
            //    => push cov, check n_covlines and reset
            cov_all.push_back(cov);
            // Check number of rows in matrix as expected

            if (n_covlines != ne_cov) {
               string message = Form("Expected %d*%d matrix, found %d rows for %s", ne_cov, ne_cov, n_covlines, line.c_str());
               TUMessages::Error(f_log, "TUDataSet", "LoadDataRelCov", message);
            }
         }
         n_covlines = 0;
      } else {
         n_covlines += 1;
         // Extract from line
         vector<string> tmp;
         TUMisc::String2List(line, " \t", tmp);

         // Check number of energies as expected (in line)
         if ((Int_t)tmp.size() != ne_cov) {
            string message = Form("Line %d: expect NECov=%d numbers, found %d", i_line, ne_cov, (Int_t)tmp.size());
            TUMessages::Error(f_log, "TUDataSet", "LoadDataRelCov", message);
         } else {
            for (Int_t k = 0; k < ne_cov; ++k)
               cov[(n_covlines - 1)*ne_cov + k] = atof(tmp[k].c_str());
         }
      }
   };
   // When exit, need to fill last data!
   cov_all.push_back(cov);

   LoadDataRelCov(i_data_start, err_types, ne_cov, cov_all, f_log);
   return true;
}

//______________________________________________________________________________
void TUDataSet::LoadDataRelCov(Int_t i_data, vector<string> const &err_types, Int_t ne_cov, vector<vector<Double_t> > const &cov, FILE *f_log) const
{
   //--- Fills data covariance matrix of relative errors for ne_cov data points.
   //  i_data            Index of data for exp/qty/etype (should correspond to lowest energy point)*
   //  err_types         Vector of error type names
   //  ne_cov            Number of E data for exp/qty/etype
   //  cov               cov[n_errtypes][ne_cov*ne_cov] for n_errtypes and ne_cov data points
   //  f_log             Log for USINE


   // MULTIPLE CHECK
   // Check range
   if (i_data < 0) {
      string message = Form("i_data=%d is out of range", i_data);
      TUMessages::Error(f_log, "TUDataSet", "LoadDataRelCov", message);
   }
   if (i_data + ne_cov - 1 > GetNData()) {
      string message = Form("i_data+ne_cov-1=%d is out of range", i_data + ne_cov - 1);
      TUMessages::Error(f_log, "TUDataSet", "LoadDataRelCov", message);
   }
   // Chack that data just before and just after are not the same exp/qty/etype
   TUDataEntry *ref = GetEntry(i_data);
   if ((i_data - 1 >= 0) && ref->IsSameQtyExpEtype(*GetEntry(i_data - 1))) {
      string message = Form("Same Exp/Qty/Etype for data %d and %d which is not expected!", i_data, i_data - 1);
      TUMessages::Error(f_log, "TUDataSet", "LoadDataRelCov", message);
   }
   if ((i_data + ne_cov > GetNData()) && ref->IsSameQtyExpEtype(*GetEntry((i_data + ne_cov)))) {
      string message = Form("Same Exp/Qty/Etype for data %d and %d which is not expected!", i_data, i_data + ne_cov);
      TUMessages::Error(f_log, "TUDataSet", "LoadDataRelCov", message);
   }
   // Check that data in [i_data, i_data+ne_cov-1] correspond
   // to the same exp/qty/etype, and are sorted by growing E
   for (Int_t i_current = i_data + 1; i_current < i_data + ne_cov; ++i_current) {
      // Check same entry
      if (!ref->IsSameQtyExpEtype(*GetEntry(i_current))) {
         string message = Form("Different Exp/Qty/Etype for data %d and %d which is not expected!", i_data, i_current);
         TUMessages::Error(f_log, "TUDataSet", "LoadDataRelCov", message);
      }
      // Check growing E
      if (GetEntry(i_current)->GetEMean() <= GetEntry(i_current - 1)->GetEMean()) {
         string message = Form("EMean[%d]=%le is smaller than EMean[%d]=%le whereas the opposite is requested",
                               i_current, GetEntry(i_current)->GetEMean(), i_current - 1, GetEntry(i_current - 1)->GetEMean());
         TUMessages::Error(f_log, "TUDataSet", "LoadDataRelCov", message);
      }
   }

   // FILL VALUES (if reach this points, all checks are OK)
   // Loop on ne_cov data points to fill covariance
   Int_t n_types = err_types.size();
   for (Int_t k_data = 0; k_data < ne_cov; ++k_data) {
      Int_t i_current = i_data + k_data;
      // Allocate
      GetEntry(i_current)->AllocateYErrRelCov(n_types, ne_cov);
      // Loop on ne_cov correlations for each E point
      for (Int_t t = 0; t < n_types; ++t) {
         GetEntry(i_current)->SetYErrCovType(t, err_types[t]);
         for (Int_t ke = 0; ke < ne_cov; ++ke)
            GetEntry(i_current)->SetYErrCov(t, ke, cov[t][k_data * ne_cov + ke]);
      }
   }
}

//______________________________________________________________________________
TUDataSet *TUDataSet::OrphanFormSubSet(string qties_expnames_etypes, FILE *f,
                                       Double_t e_min, Double_t e_max,
                                       string const &t_start, string const &t_stop,
                                       Bool_t is_elem_into_isotopes) const
{
   //--- Returns subset of current data (to be deleted after use) formed
   //    by those in the list of qties/exps/energy range asked for.
   //  qties_expnames         User-choice Q1:EXP1,EXP2,EXP3:kEKN;Q2:ALL:kEK,Q3=EXP1,EPX4,EXP5:kR (e.g. B/C:HEAO,VOYAGER;O,N:ALL:kEKN)
   //  f                      Output file for chatter
   //  e_min                  Lower range of x-axis interval (default = 1.e-40)
   //  e_max                  Upper range of x-axis interval (default = 1.e40)
   //  t_start                Min date allowed (format is 'YYYY-MM-DD HH:MN:SS', default is "1940-01-01 00:00:00")
   //  t_stop                 Max date allowed (format is 'YYYY-MM-DD HH:MN:SS', default is "2100-01-01 00:00:00")
   //  is_elem_into_isotopes  If true,  add element names by their most abundant isotope (e.g., H=>1H)

   TUDataSet *subset = new TUDataSet();;

   if (qties_expnames_etypes == "") {
      cout << ">>>>> Enter a list of quantities/experiments/Etype (e.g. B/C:HEAO,VOYAGER,CRIS,IMP:kEKN;O,N:ALL:kEKN): ";
      cin >> qties_expnames_etypes;
      cout << ">>>>> Enter energy range (GeV/n or GeV or GV depending on EType) sought (e.g., \"0.1,100.\", or \"ALL\" for all energies): ";
      string tmp;
      cin >> tmp;
      if (tmp == "ALL") {
         e_min = 1.e-40;
         e_max =  1.e40;
      } else {
         vector<Double_t> l_xval;
         TUMisc::String2List(tmp, ",", l_xval);
         e_min = l_xval[0];
         e_max = l_xval[1];
      }
   }

   // Find all sets of "qty:data:etype" demanded (semi-colon separated)
   subset->SetQtiesEtypesphiFF("");
   vector<string> l_combo;
   TUMisc::String2List(qties_expnames_etypes, ";", l_combo);
   // Loop on the quantities
   for (Int_t i = 0; i < (Int_t)l_combo.size(); ++i) {
      // Each combo is formatted as cr1,cr2...:exp1,exp2...:etype
      vector<string> l_combo_qty_exp_etype;
      TUMisc::String2List(l_combo[i], ":", l_combo_qty_exp_etype);
      if (l_combo_qty_exp_etype.size() != 3) {
         string message = "Expected format for \'qties_expnames_etypes\' is  \'cr1,cr2...:exp1,exp2...:etype\', but gave " + l_combo[i];
         TUMessages::Error(f, "TUDataSet", "OrphanFormSubSet", message);
      }

      // Extract list of quantities and list of experiments
      // for the first combo i: convert all string to upper case.
      string l_combo_qty = l_combo_qty_exp_etype[0];
      string l_combo_exp = l_combo_qty_exp_etype[1];
      vector<string> l_qty, l_exp;
      TUMisc::String2List(l_combo_qty, ",", l_qty);
      TUMisc::String2List(l_combo_exp, ",", l_exp);
      gENUM_ETYPE e_type;
      TUEnum::Name2Enum(l_combo_qty_exp_etype[2], e_type);

      // Fill fQtiesEtypesphiFF
      string tmp = l_combo_qty + ":" + l_combo_qty_exp_etype[2]
                   + ":" + ListOfExpphi(l_combo[i], f, e_min, e_max, t_start, t_stop);
      if (subset->GetQtiesEtypesphiFF() == "")
         subset->SetQtiesEtypesphiFF(tmp);
      else
         subset->SetQtiesEtypesphiFF(subset->GetQtiesEtypesphiFF() + ";" + tmp);

      // If is_elem_into_isotopes=true, we also add data for which
      // qty is an element, if the quantity asked for corresponds
      // to the most abundant isotope of this element
      vector<string> elem_toconvertin_isotopes;
      vector<string> elem_isotopename;
      if (is_elem_into_isotopes) {
         TUAtomElements atom_elements;
         for (Int_t j = 0; j < (Int_t)l_qty.size(); ++j) {
            TUMisc::UpperCase(l_qty[j]);
            // Check qty is not a ratio neither an element
            if (TUMisc::IsRatio(l_qty[j]) || atom_elements.IsElement(l_qty[j])) continue;
            // seek element corresponding to this CR name
            string element = atom_elements.CRNameToCRElement(l_qty[j]);

            // Check whether the most abundant isotope for this element
            // matches the current CR. If it is, and if the element is not
            // already in the current list, add it.
            if (l_qty[j] == atom_elements.MostAbundantIsotope(element)) {
               if (!TUMisc::IsInList(l_qty, element, true)) {
                  elem_toconvertin_isotopes.push_back(element);
                  elem_isotopename.push_back(l_qty[j]);
               }
            }
         }
      }

      for (Int_t j = 0; j < (Int_t)l_qty.size(); ++j) {
         TUMisc::UpperCase(l_qty[j]);
         // Alternative names for qty
         if (l_qty[j] == "E+") l_qty[j] = "POSITRON";
         else if (l_qty[j] == "E-") l_qty[j] = "ELECTRON";
         else if (l_qty[j] == "E-+E+") l_qty[j] = "ELECTRON+POSITRON";
         else if (l_qty[j] == "E+/(E-+E+)") l_qty[j] = "POSITRON/ELECTRON+POSITRON";
         else if (l_qty[j] == "PBAR") l_qty[j] = "1H-BAR";
         else if (l_qty[j] == "PBAR/P") l_qty[j] = "1H-BAR/1H";
         else if (l_qty[j] == "DBAR") l_qty[j] = "2H-BAR";
      }
      for (Int_t j = 0; j < (Int_t)l_exp.size(); ++j)
         TUMisc::UpperCase(l_exp[j]);

      // Loop on all fData[i] and find matching data
      // to add to the subset
      for (Int_t k = 0; k < GetNData(); ++k) {
         // Check whether data has the right xaxis
         if (fData[k].GetEType() != e_type)
            continue;

         // Check whether fData[k] is in the right energy range
         Double_t val_k = fData[k].GetEMean();
         if ((val_k < e_min) || (val_k > e_max))
            continue;

         // Check whether fData[k] is in the right time range
         TUDatime exp_datime_start = fData[k].GetExpDatime(0);
         TUDatime exp_datime_stop = fData[k].GetExpDatime(fData[k].GetNDatimes() - 1);
         if (exp_datime_start.GetStart() < t_start || exp_datime_stop.GetStop() > t_stop)
            continue;

         string name_k = fData[k].GetQty();
         TUMisc::UpperCase(name_k);
         string exp_k = fData[k].GetExpName();
         TUMisc::UpperCase(exp_k);
         // Loop on all quantities l_qty demanded
         for (Int_t j = 0; j < (Int_t)l_qty.size(); ++j) {
            if (l_qty[j] != name_k) continue;
            else {
               // If experiment demanded differs from "ALL", loop
               // on all l_exp demanded
               if (l_exp[0] == "ALL") {
                  subset->AddDataEntry(fData[k]);
                  continue;
               }
               for (Int_t l = 0; l < (Int_t)l_exp.size(); ++l) {
                  if (exp_k.find(l_exp[l], 0) != string::npos)
                     subset->AddDataEntry(fData[k]);
               }
            }
         }
         // If is_elem_into_isotopes=true, add to list elem_toconvertin_isotopes[]
         // and convert their names (data) into their equivalent isotope name
         if (is_elem_into_isotopes) {
            // Loop on all quantities l_qty demanded
            for (Int_t j = 0; j < (Int_t)elem_toconvertin_isotopes.size(); ++j) {
               if (elem_toconvertin_isotopes[j] != name_k) continue;
               else {
                  // If experiment demanded differs from "ALL", loop
                  // on all l_exp demanded
                  if (l_exp[0] == "ALL") {
                     subset->AddDataEntry(fData[k]);
                     // Change data name: element name is replaced by most abundant isotope name
                     subset->GetEntry(subset->GetNData() - 1)->SetQty(elem_isotopename[j]);
                     continue;
                  }
                  for (Int_t l = 0; l < (Int_t)l_exp.size(); ++l) {
                     if (exp_k.find(l_exp[l], 0) != string::npos) {
                        subset->AddDataEntry(fData[k]);
                        // Change data name: element name is replaced by most abundant isotope name
                        subset->GetEntry(subset->GetNData() - 1)->SetQty(elem_isotopename[j]);
                     }
                  }
               }
            }
         }
      }
   }

   if (subset->GetNData() == 0) {
      string message = "No experimental data found for " + qties_expnames_etypes;
      TUMessages::Warning(f, "TUDataSet", "OrphanFormSubSet", message);
      return subset;
   }

   // Fill for subset indices data sorted by exp, qties...
   subset->FillExpsAndDataPerExp();
   subset->FillQtiesAndDataPerQtyExp();

   // Update covariance matrix if needed (if user energy sub-range selected)
   if (e_min > 1.e-39 || e_max < 1.e39) {
      vector<pair<Int_t, Int_t> > i_start_stop;
      for (Int_t e = 0; e < gN_ETYPE; ++e) {
         gENUM_ETYPE etype = TUEnum::gENUM_ETYPE_LIST[e];
         subset->IndicesData4AllExpQty(etype, i_start_stop);
         for (Int_t i = 0; i < (Int_t)i_start_stop.size(); ++i) {
            Int_t i_data_subset = i_start_stop[i].first;
            Int_t ne_cov = subset->GetEntry(i_data_subset)->GetNCovE();
            //Int_t ne_types = subset->GetEntry(i_data_subset)->GetNCovTypes();

            // If no covariance data for this exp_qty_etype, nothing to do
            if (ne_cov == 0)
               continue;

            // Else, we need to check if energies were removed from subset,
            // and remove accordingly some values in covariance data.
            Int_t i_data = IndexData(*subset->GetEntry(i_data_subset));
            if (ne_cov == i_start_stop[i].second - i_start_stop[i].first + 1)
               continue;
            // If reach here, means that subset selection cuts energies,
            // and we need to find which ones (energies are sorted in growing
            // order) at start and stop, and remove accordingly the covariance values
            //   - number to remove at low energy
            TUDataEntry *entry_ref = GetEntry(i_data);
            //entry_ref->Print();
            Int_t i_try;
            for (i_try = i_data - 1; i_try >= 0; --i_try) {
               if (!entry_ref->IsSameQtyExpEtype(*GetEntry(i_try)))
                  break;
            }
            Int_t n_start2remove = i_data - (i_try + 1);
            //   - number to remove at high energy
            i_data = IndexData(*subset->GetEntry(i_start_stop[i].second));
            for (i_try = i_data + 1; i_try < GetNData(); ++i_try) {
               if (!entry_ref->IsSameQtyExpEtype(*GetEntry(i_try)))
                  break;
            }
            entry_ref = NULL;
            Int_t n_stop2remove = (i_try - 1) - i_data;
            //printf("  n_start2remove=%d,  n_stop2remove=%d\n", n_start2remove, n_stop2remove);
            // Resize covariance for all in subset
            fprintf(f, "      [first & last data found in subset]\n");
            subset->GetEntry(i_start_stop[i].first)->Print(f);
            subset->GetEntry(i_start_stop[i].second)->Print(f);
            for (i_data_subset = i_start_stop[i].first; i_data_subset <= i_start_stop[i].second; ++i_data_subset) {
               //subset->PrintEntry(stdout, i_data_subset);
               //printf("  BEFORE:\n");
               //subset->GetEntry(i_data_subset)->Print();
               //subset->GetEntry(i_data_subset)->PrintDataRelCov(stdout, 0);
               //cout << subset->GetEntry(i_data_subset)->GetNCovE() << endl;
               subset->GetEntry(i_data_subset)->EraseYErrRelCov(ne_cov - n_stop2remove, ne_cov);
               //cout << subset->GetEntry(i_data_subset)->GetNCovE() << endl;
               subset->GetEntry(i_data_subset)->EraseYErrRelCov(0, n_start2remove);
               //cout << subset->GetEntry(i_data_subset)->GetNCovE() << endl;
               //printf("  AFTER:\n");
               //subset->GetEntry(i_data_subset)->Print();
               //subset->GetEntry(i_data_subset)->PrintDataRelCov(stdout, 0);
               //cout << subset->GetEntry(i_data_subset)->GetNCovE() << endl;
            }
         }
      }
   }
   return subset;
}

//______________________________________________________________________________
TGraphAsymmErrors *TUDataSet::OrphanGetGraph(Int_t i_data, gENUM_ERRTYPE err_type, gENUM_ETYPE e_type, Double_t const &e_power)
{
   //--- Returns TGraphAsymmErrors for all data similar to i_data.
   //  i_data            Data to look for
   //  err_type          Selects uncertainties among gENUM_ERRTYPE (kERRSTAT, kERRSYST, kERRTOT)
   //  e_type            Selects x axis among gENUM_ETYPE (kEKN, kR,...)
   //  e_power           Y-axis is multiplied by e_type^{e_power} if qty is a flux or "ALLSPECTRUM"

   string exp_name = fData[i_data].GetExpName();
   string qty = fData[i_data].GetQty();
   Int_t i_exp = IndexInExps(exp_name);
   Int_t i_qty = IndexInQties(qty);
   Int_t n_data = GetNData(i_exp, i_qty, e_type);
   TGraphAsymmErrors *gr = new TGraphAsymmErrors(n_data);

   // set name
   string name_eindex = "";
   if (e_power > 1.e-3)
      name_eindex = Form("%.3f", e_power);
   else if (e_power < -1.e-3)
      name_eindex = Form("m%.3f", -e_power);
   string name = "data_" + qty + "_" + exp_name + "_ "
                 + TUEnum::Enum2Name(err_type) + "_ " + TUEnum::Enum2Name(e_type);
   if (fabs(e_power) > 1.e-3)
      name = name + "_" + name_eindex;
   TUMisc::RemoveSpecialChars(name);
   gr->SetName(name.c_str());

   // set titles (and axes names)
   string x_title = "", y_title = "";
   TUEnum::XYTitles(x_title, y_title, qty, e_type, e_power);
   string gr_titles = ";" + x_title + ";" + y_title;
   gr->SetTitle(gr_titles.c_str());

   // fill data
   for (Int_t i = 0; i < n_data; ++i) {
      Int_t j = IndexData(i_exp, i_qty, e_type) + i;
      TUDataEntry entry(*GetEntry(j));

      if (fabs(e_power) > 1.e-5 && !TUMisc::IsRatio(qty))
         entry.YTimesPowerOfE(e_power);
      gr->SetPoint(i, entry.GetEMean(), entry.GetY());
      gr->SetPointError(i, fabs(entry.GetEMean() - entry.GetEBinL()),
                        fabs(entry.GetEMean() - entry.GetEBinU()),
                        entry.GetYErrL(err_type), entry.GetYErrU(err_type));
   }

   return gr;
}

//______________________________________________________________________________
TMultiGraph *TUDataSet::OrphanGetMultiGraph(string const &qty, string const &list_exp,
      gENUM_ERRTYPE err_type, gENUM_ETYPE e_type,
      Double_t e_power, Bool_t is_demodulate, TLegend *leg,
      FILE *f, Int_t z_demodul, Int_t a_demodul)
{
   //--- Returns TMultiGraph (graph + legend) of "qty" measured by "list_exp".
   //  qty                      Quantity to plot
   //  list_exp                 Comma-separated (case-insensitive) list of experiments
   //  err_type                 Selects uncertainties among gENUM_ERRTYPE (kERRSTAT, kERRSYST, kERRTOT)
   //  e_type[def=kEKN]         Selects x axis among gENUM_ETYPE (kEKN, kR,...)
   //  e_power[def=0]           Y-axis is multiplied by e_type^{e_power} if qty is a flux or "ALLSPECTRUM"
   //  is_demodulate[def=false] Enables Force-Field demodulation (approximate if ratio)
   //  leg[def=NULL]            Legend for data in multigraph
   //  f                        Output file for chatter
   //  z_demodul                If is_demodulate=true, user must command-line Z (unless a positive value is passed)
   //  a_demodul                If is_demodulate=true, user must command-line A (unless a positive value is passed)

   TMultiGraph *mg = NULL;

   string qty_uc = qty;
   TUMisc::FormatCRQtyName(qty_uc);

   // Get subset (returns mg=NULL if empty)
   string qty_exps_etype = qty_uc + ":" + list_exp + ":" + TUEnum::Enum2Name(e_type);
   TUDataSet *subset = OrphanFormSubSet(qty_exps_etype, f);

   if (subset->GetNData() == 0) {
      //   string message = "no data found for the combination QTY:EXP:ETYPE = " + qty_exps_etype;
      //   TUMessages::Warning(stdout, "TUDataSet", "OrphanGetMultiGraph", message);
      return mg;
   }

   // Allocate, set mg and axes title
   mg = new TMultiGraph();
   string x_title = "", y_title = "";
   TUEnum::XYTitles(x_title, y_title, qty, e_type, e_power);
   string mg_titles = "; " + x_title + "; " + y_title;
   mg->SetTitle(mg_titles.c_str());

   // If 'demodulate' and qty=ratio or qty is an element,
   // the demodulation is only approximate: a 'pseudo' cr
   // is created to proceed nonetheless
   Bool_t is_ratio = TUMisc::IsRatio(qty);
   if (is_demodulate) {
      TUCREntry pseudo_cr;
      if (is_ratio) {
         string message = "demodulate a ratio is approximate (exact in the limit "
                          "where all isotopes in the ratio have the same Z/A)";
         TUMessages::Warning(f, "TUDataSet", "OrphanGetMultiGraph", message);
         if (qty_uc == "POSITRON/ELECTRON+POSITRON")
            pseudo_cr.SetEntry(1/*A*/, 1/*Z*/, TUPhysics::me_GeV(), "LEPTONS");
         else {
            if (z_demodul < 0 && a_demodul < 0) {
               printf("   > Choose Z A (e.g. 5 10): ");
               cin >> z_demodul >> a_demodul;
            }
            fprintf(f, "    => demodulation is performed using Z=%d and  A=%d\n", z_demodul, a_demodul);
            pseudo_cr.SetEntry(a_demodul, z_demodul, Double_t(a_demodul));
         }
      } else {
         TUAtomElements atoms;
         // If qty is a CR
         if (qty_uc.find_first_of("0123456789") != string::npos)
            pseudo_cr.SetEntry(atoms.CRNameToZ(qty_uc), atoms.CRNameToA(qty_uc), (Double_t)pseudo_cr.GetA(), qty_uc);
         else if (qty_uc == "ELECTRON" || qty_uc == "POSITRON" || qty_uc == "ELECTRON+POSITRON")
            pseudo_cr.SetEntry(1, 1, TUPhysics::me_GeV(), "LEPTONS");
         else // if element
            pseudo_cr.SetEntry(2 * pseudo_cr.GetZ(), atoms.ElementNameToZ(qty_uc),
                               (Double_t)pseudo_cr.GetA(),
                               Form("%d%s", pseudo_cr.GetA(), qty_uc.c_str()));
      }

      // Create the Force-Field model, and modulate data
      // (assuming it correspond to the 'pseudo_cr')
      TUSolMod0DFF *solmod = new TUSolMod0DFF();
      solmod->SetRig0(0.2);
      // Demodulate data

      for (Int_t i = 0; i < subset->GetNData(); ++i)
         solmod->DemodulateData(pseudo_cr, subset->GetEntry(i), is_ratio, false);
   }

   //--- Multiply flux by X^index if required
   if (fabs(e_power) > 1.e-5 && !is_ratio) {
      for (Int_t i = 0; i < subset->GetNData(); ++i)
         subset->GetEntry(i)->YTimesPowerOfE(e_power);
   }

   // Fill a graph for each EXP: also use a different graph
   // for different datimes within the same experiment.
   // N.B.: the legend will be generated automatically afterwards
   // in the canvas with canvas->BuildLegend(), which takes each
   // graph title (for the legend).

   TGraphAsymmErrors *gr = NULL;
   for (Int_t i_exp = 0; i_exp < subset->GetNExps(); ++i_exp) {
      vector<Double_t> vx;
      vector<Double_t> vy;
      vector<Double_t> vx_l;
      vector<Double_t> vx_u;
      vector<Double_t> vy_l;
      vector<Double_t> vy_u;

      // Loop/add all data for this unique exp/datime
      for (Int_t j = 0; j < subset->GetNData(i_exp, e_type); ++j) {
         Int_t i_data = subset->IndexData(i_exp, e_type) + j;
         vx.push_back(subset->GetEntry(i_data)->GetEMean());
         vx_l.push_back(fabs(subset->GetEntry(i_data)->GetEMean() - subset->GetEntry(i_data)->GetEBinL()));
         vx_u.push_back(fabs(subset->GetEntry(i_data)->GetEMean() - subset->GetEntry(i_data)->GetEBinU()));
         vy.push_back(subset->GetEntry(i_data)->GetY());
         vy_l.push_back(subset->GetEntry(i_data)->GetYErrL(err_type));
         vy_u.push_back(subset->GetEntry(i_data)->GetYErrU(err_type));
      }

      gr = new TGraphAsymmErrors(vx.size(), &vx[0], &vy[0], &vx_l[0], &vx_u[0], &vy_l[0], &vy_u[0]);
      string exp_name = subset->GetExp(i_exp);
      string name_eindex = "";
      if (e_power > 1.e-3)
         name_eindex = Form("%.3f", e_power);
      else if (e_power < -1.e-3)
         name_eindex = Form("m%.3f", -e_power);
      string prefix = "data_";
      if (is_demodulate)
         prefix += "demodulated_";
      string name = prefix + qty + "_" + exp_name + "_ "
                    + "_ " + TUEnum::Enum2Name(err_type) + TUEnum::Enum2Name(e_type);
      if (fabs(e_power) > 1.e-3)
         name = name + "_" + (string)name_eindex;
      TUMisc::RemoveSpecialChars(name);
      gr->SetName(name.c_str());
      gr->SetTitle(Form("%s: %s", qty.c_str(), exp_name.c_str()));
      gr->SetMarkerColor(TUMisc::RootColor(i_exp));
      gr->SetLineColor(TUMisc::RootColor(i_exp));
      gr->SetMarkerStyle(TUMisc::RootMarker(i_exp));
      gr->SetMarkerSize(1.2);
      gr->SetDrawOption("P");
      gr->SetFillColor(0);
      gr->SetFillStyle(0);
      gr->GetXaxis()->SetTitle(x_title.c_str());
      gr->GetYaxis()->SetTitle(y_title.c_str());
      mg->Add(gr, "P");

      // If leg non NULL, fill it
      if (leg) {
         string exp_name_short = exp_name.substr(0, 24);
         string title_leg = Form("[#phi^{file}_{FF}=%.3f GV]  %s", subset->GetEntry(subset->IndexData(i_exp, e_type))->GetExpphi(), exp_name_short.c_str());
         TLegendEntry *entry = leg->AddEntry(gr, title_leg.c_str(), "P");
         entry->SetTextColor(TUMisc::RootColor(i_exp));
         entry = NULL;
      }
   }

   // Free memory
   gr = NULL;
   delete subset;
   subset = NULL;

   return mg;
}

//______________________________________________________________________________
void TUDataSet::ParsData_Create()
{
   //--- Creates as many sets of nuisance parameters as the number of exp/qty in data.
   //    We recall that an exp uniquely determines a data taking period, i.e. no overlap
   //    between different exp by construction in the CRDB (http::/lpsc.in2p3.fr/crdb).

   // Reset
   ParsData_Delete();

   // If no data, nothing to do
   if (GetNData() == 0)
      return;

   // set mapping to -1 (default, i.e. no fNuisPars associated)
   fMapDataIndex2NuisPars.assign(GetNData(), -1);

   // If exists, create and add in fNuisPars new nuisance parameters
   // for the data with name 'type_expqtyetype' (type is the error type found)
   for (Int_t i_exp = 0; i_exp < GetNExps(); ++i_exp) {
      // Loop on CR quantities (several per sub-exp)
      vector<Int_t> indices_qties;
      IndicesQtiesInExp(i_exp, indices_qties);
      //cout << "      * For i_exp=" << i_exp << " " << GetExp(i_exp) << ", Nqties=" << indices_qties.size() << endl;
      for (Int_t i = 0; i < (Int_t)indices_qties.size(); ++i) {
         Int_t i_qty = indices_qties[i];
         for (Int_t e = 0; e < gN_ETYPE; ++e) {
            gENUM_ETYPE etype = TUEnum::gENUM_ETYPE_LIST[e];
            Int_t i_data = IndexData(i_exp, i_qty, etype);
            if (i_data < 0)
               continue;

            // Search for covariance matrix in this expqty
            if (GetEntry(i_data)->GetNCovTypes() == 0)
               continue;
            // If exist, add nuisance parameter for each covariance error
            // (keep all at this stage: 'stat' error cannot be a nuisance
            //  parameter, but discarded elsewhere)
            TUDataEntry *data = GetEntry(i_data);
            TUFreeParList *new_pars = new TUFreeParList();
            for (Int_t t = 0; t < data->GetNCovTypes(); ++t) {
               string par_name = data->GetYErrRelCovType(t) + "_" + GetExp(i_exp) + "_" + GetQty(i_qty);
               TUMisc::RemoveSpecialChars(par_name);
               new_pars->AddPar(par_name, "-" /*unit*/, 1./*val*/);
            }
            // Set mapping
            for (Int_t jj = i_data; jj < i_data + GetNData(i_exp, i_qty, etype); ++jj)
               fMapDataIndex2NuisPars[jj] = ParsData_GetN();
            // Add parameter
            fNuisPars.push_back(new_pars);
            fIsNuisParsInFit.push_back(false);

            //fNuisPars[ParsData_GetN()-1]->PrintPars();

            // And store indices for Exp/Qty for further usage
            pair<Int_t, Int_t> tmp_pair;
            tmp_pair.first = i_exp;
            tmp_pair.second = i_qty;
            fMapNuisPars2ExpQty.push_back(tmp_pair);
            fMapNuisPars2EType.push_back(etype);
         }
      }
   }
}

//______________________________________________________________________________
void TUDataSet::ParsData_Delete()
{
   //--- Deletes fNuisPars and fMapDataIndex2NuisPars

   for (Int_t i = 0; i < ParsData_GetN(); ++i) {
      delete fNuisPars[i];
      fNuisPars[i] = NULL;
   }
   fIsNuisParsInFit.clear();
   fNuisPars.clear();
   fMapDataIndex2NuisPars.clear();
   fMapNuisPars2ExpQty.clear();
   fMapNuisPars2EType.clear();
}

//______________________________________________________________________________
void TUDataSet::ParsData_Indices(string const &par_name, Int_t &i_expqty, Int_t &t_type) const
{
   //--- Looks for indices (which expqty and which error type parameter for this index) matching par_name.
   // INPUT:
   //  par_name      Name of parameter sought
   // OUTPUTS:
   //  i_expqty      Index of expqty matching par_name (-1 if not found)
   //  t_type        Index of type matching par_name (-1 if not found)

   i_expqty = -1;
   t_type = -1;
   if (ParsData_GetN() == 0)
      return;

   for (Int_t i = 0; i < ParsData_GetN(); ++i) {
      for (Int_t j = 0; j < fNuisPars[i]->GetNPars(); ++j) {
         if (fNuisPars[i]->GetParEntry(j)->GetName() == par_name) {
            i_expqty = i;
            t_type = j;
            return;
         }
      }
   }
   return;
}

//______________________________________________________________________________
Double_t TUDataSet::ParsData_ModelBias(Int_t i_data) const
{
   //--- Calculate model bias due to nuisance parameters:
   //       bias = \prod_{i=types} (1+vu_i\sigma_i)
   //    with nu_i the nuisance parameter and \sigma_i the relative error taken
   //    here from the covariance matrix (square root of the diagonal term).
   //  i_data            Index of data

   // Associated nuisance parameters?
   if (ParsData_GetN() == 0)
      return 1;
   Int_t i_parserr = fMapDataIndex2NuisPars[i_data];
   if (i_parserr < 0)
      return 1.;

   // Need to know the starting index of data for exp/qty
   Int_t idata_expqty = IndexData(ParsData_IndexExp(i_parserr), ParsData_IndexQty(i_parserr), ParsData_EType(i_parserr));

   // Calculate bias, looping on all types acting as nuisance
   Double_t bias = 1.;
   for (Int_t t_type = 0; t_type < GetEntry(i_data)->GetNCovTypes(); ++t_type) {
      if (!GetEntry(i_data)->IsCovTypeNuisPar(t_type))
         continue;
      Double_t sigma_i = sqrt(GetEntry(i_data)->GetYErrRelCov(t_type, i_data - idata_expqty));
      //sigma_i = 0.03;
      bias *= (1. + ParsData_Get(i_parserr)->GetParEntry(t_type)->GetVal(false) * sigma_i);
      //cout << " E=" << GetEntry(i_data)->GetEMean() << "  sigma=" << sigma_i << "  nuis(" << GetEntry(i_data)->GetYErrRelCovType(t_type)  << ")=" << ParsData_Get(i_parserr)->GetParVal(t_type) << " bias=" << bias << endl;
   }
   return bias;
}

//______________________________________________________________________________
void TUDataSet::Print(FILE *f, Int_t switch_print) const
{
   //--- Prints data-formatted in file f.
   //  f                 File in which to print
   //  switch_print      Decide how to print data (0=short, 1=full)

   if (GetNData() > 0)
      fData[0].Print(f, switch_print, true);
   for (Int_t i = 1; i < GetNData(); ++i)
      fData[i].Print(f, switch_print, false);
}

//______________________________________________________________________________
void TUDataSet::PrintDataRelCov(FILE *f, Int_t k_type, Int_t i_data_start, Int_t i_data_stop) const
{
   //--- Prints data covariance matrix of relative errors in file f.
   //  f                 File in which to print
   //  k_type            Index of error type to print
   //  i_data_start      First data for which to print covariance
   //  i_data_stop       Last data for which to print covariance

   // Check
   if (!GetEntry(i_data_start)->IsSameQtyExpEtype(*GetEntry(i_data_stop))) {
      string message = Form("Different Exp/Qty/Etype for data %d and %d which is not expected!", i_data_start, i_data_start - 1);
      TUMessages::Error(f, "TUDataSet", "PrintCov", message);
   }

   string indent = TUMessages::Indent(true);
   Int_t n_data = i_data_stop - i_data_start + 1;

   // Print header
   fprintf(f, "%s    Covariance for %s  [%d points] \n", indent.c_str(),
           GetEntry(i_data_start)->FormExpQtyEType().c_str(), GetEntry(i_data_start)->GetNCovE());
   for (Int_t i_data = i_data_start; i_data < i_data_stop + 1; ++i_data) {
      if (n_data != GetEntry(i_data)->GetNCovE()) {
         string message = Form("NECov=%d (for i_data=%d), whereas print on n_data=%d", GetEntry(i_data)->GetNCovE(), i_data, n_data);
         TUMessages::Error(f, "TUDataSet", "PrintCov", message);
      }
      GetEntry(i_data)->PrintDataRelCov(f, k_type);
   }

   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUDataSet::PrintFileNames(FILE *f) const
{
   //--- Prints file names in file f.
   //  f                 File in which to print

   fprintf(f, "   Data read from:\n");
   for (Int_t i = 0; i < GetNFiles(); ++i)
      fprintf(f, "    - %s\n", GetFileName(i).c_str());
}

//______________________________________________________________________________
void TUDataSet::PrintFromExpList(FILE *f, string const &commasep_exps) const
{
   //--- Prints data-formatted in file for experiments names matching commasep_exps.
   //  f                 File in which to print
   //  commasep_exps     List of comma-separated (case-insensitive) exp. names

   string title = "Data found for exp.name in " + commasep_exps;
   TUMessages::Separator(f, title);

   vector<string> l_exp;
   TUMisc::String2List(commasep_exps, ",", l_exp);

   for (Int_t i = 0; i < (Int_t)l_exp.size(); ++i) {
      fprintf(f, ">> Data found for exp.name containing \'%s\':\n", l_exp[i].c_str());
      TUMisc::UpperCase(l_exp[i]);

      // Scan data and find position of "matching" data
      for (Int_t k = 0; k < GetNData(); ++k) {
         // Does the file correspond to the right experiment?
         string exp_k = fData[k].GetExpName();
         TUMisc::UpperCase(exp_k);
         if (strstr(exp_k.c_str(), l_exp[i].c_str()) || l_exp[i] == "ALL") fData[k].Print(f);
      }
      fprintf(f, "\n");
   }
}

//______________________________________________________________________________
void TUDataSet::PrintExps(FILE *f) const
{
   //---Prints in file f list of all exps.
   //  f                 File in which to print

   // Loop on experiment names
   for (Int_t i_exp = 0; i_exp < GetNExps(); ++i_exp) {
      // Loop on e_types and stop after first found
      for (Int_t e = 0; e < gN_ETYPE; ++e) {
         gENUM_ETYPE e_type = TUEnum::gENUM_ETYPE_LIST[e];
         Int_t i_data = IndexData(i_exp, e_type);
         if (i_data >= 0) {
            fData[i_data].PrintExp(f, false);
            break;
         }
      }
   }
}

//______________________________________________________________________________
void TUDataSet::PrintExps(FILE *f, string const &qty) const
{
   //---Prints in file f list of exp that measured qty.
   //  f                 File in which to print
   //  qty               Quantity measured to search for

   string indent = TUMessages::Indent(true);
   fprintf(f, "%s       %s:\n", indent.c_str(), qty.c_str());
   vector<Int_t> indices = IndicesData(qty);
   for (Int_t i = 0; i < (Int_t)indices.size(); ++i)
      fData[indices[i]].PrintExp(f, false);
   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUDataSet::PrintExps(FILE *f, string const &commasep_qties, Double_t ekn_min, Double_t ekn_max,
                          gENUM_ETYPE e_type) const
{
   //--- Prints in file f experiment names which have measured any of l_qty in the range [ekn_min,ekn_max].
   //  f                 File in which to print
   //  commasep_qties    Comma-separated list of quantities (case-insensitive)
   //  ekn_min           Lower range of energy interval
   //  ekn_max           Upper range of energy interval
   //  e_type            Type of x-axis (see gENUM_ETYPE in TUEnum.h)


   string qty_exps_etypes = commasep_qties + ":ALL" + ":" + TUEnum::Enum2Name(e_type);
   TUDataSet *subset = OrphanFormSubSet(qty_exps_etypes, f, ekn_min, ekn_max);
   if (subset->GetNExps() == 0) {
      delete subset;
      subset = NULL;
      return;
   }

   string title = "Experiments found for " + commasep_qties;
   title = title + Form(" in <E>=[%.3le,%.3le] GeV/n", ekn_min, ekn_max);
   TUMessages::Separator(f, title);

   // Print only if subset not empty
   for (Int_t i = 0; i < subset->GetNExps(); ++i) {
      TUDataEntry *entry = subset->GetEntry(subset->IndexData(i, e_type));
      if (i == 0) entry->PrintExp(f, true);
      else entry->PrintExp(f, false);
      entry = NULL;
   }
   fprintf(f, "\n");

   delete subset;
   subset = NULL;
}

//______________________________________________________________________________
void TUDataSet::PrintExpsQties(FILE *f, Bool_t is_expqty_or_qtyexp) const
{
   //---Prints in file f list of experiments and quantities.
   //  f                    File in which to print
   //  is_expqty_or_qtyexp  Sorted by exp, or by qty

   string indent = TUMessages::Indent(true);
   if (is_expqty_or_qtyexp) {
      for (Int_t i_exp = 0; i_exp < GetNExps(); ++i_exp) {
         fprintf(f, "%s       %s: ", indent.c_str(), GetExp(i_exp).c_str());
         vector<Int_t> indices_qties;
         IndicesQtiesInExp(i_exp, indices_qties);
         Int_t n_qties = indices_qties.size();
         for (Int_t j = 0; j < n_qties; ++j) {
            Int_t j_qty = indices_qties[j];
            string qty =  fQty[j_qty];
            if (j < n_qties - 1)
               fprintf(f, "%s,", qty.c_str());
            else
               fprintf(f, "%s\n", qty.c_str());
         }
      }
   } else {
      for (Int_t j_qty = 0; j_qty < GetNQties(); ++j_qty) {
         fprintf(f, "%s       %s: ", indent.c_str(), GetQty(j_qty).c_str());
         vector<Int_t>  i_exps = IndicesData(GetQty(j_qty));
         for (Int_t i = 0; i < (Int_t)i_exps.size(); ++i) {
            string exp = GetExp(i_exps[i]);
            if (i < (Int_t)i_exps.size() - 1)
               fprintf(f, "%s, ", exp.c_str());
            else
               fprintf(f, "%s\n", exp.c_str());
         }
      }
   }
   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUDataSet::SetClass(TUInitParList *init_pars, Bool_t is_verbose, FILE *f_log)
{
   //--- Updates content of class.
   //  init_pars         TUInitParList class of initialisation parameters
   //  is_verbose        Verbose or not when class is set
   //  f_log             Log for USINE

   Initialise();

   const Int_t gg = 3;
   string gr_sub_name[gg] = {"Base", "CRData", "fCRData"};

   string indent = TUMessages::Indent(true);
   if (is_verbose) {
      fprintf(f_log, "%s[TUDataSet::SetClass]\n", indent.c_str());
      fprintf(f_log, "%s### Read experimental data [%s#%s#%s]\n",
              indent.c_str(), gr_sub_name[0].c_str(), gr_sub_name[1].c_str(), gr_sub_name[2].c_str());
   }
   // Find parameter
   Int_t i_par = init_pars->IndexPar(gr_sub_name);
   for (Int_t i = 0; i < init_pars->GetParEntry(i_par).GetNVals(); ++i) {
      string file = TUMisc::GetPath(init_pars->GetParEntry(i_par).GetVal(i));
      LoadData(file, false, is_verbose, f_log);
   }

   if (is_verbose)
      fprintf(f_log, "%s[TUDataSet::SetClass] <DONE>\n", indent.c_str());

   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUDataSet::TEST(TUInitParList *init_pars, FILE *f)
{
   //--- Test function for this class (called in usine -t).
   //  init_pars         TUInitParList class of initialisation parameters
   //  f                 Output file for test

   string message = "Store data read from file, form subsets from\n"
                    "   - experiment names\n"
                    "   - quantity measured\n"
                    "   - energy range";
   TUMessages::Test(f, "TUDataSet", message);


   // Set class and test methods
   fprintf(f, " * Set class:\n");
   fprintf(f, "   > SetClass(init_pars=\"%s\", is_verbose=false, f_log=f);\n", init_pars->GetFileNames().c_str());
   SetClass(init_pars, false, f);
   fprintf(f, "   > SetClass(init_pars=\"%s\", true, f);\n", init_pars->GetFileNames().c_str());
   SetClass(init_pars, true, f);
   fprintf(f, "\n");
   fprintf(f, " * Possible prints (e.g., all data from a list of exps):\n");
   fprintf(f, "   > PrintFileNames(f);\n");
   PrintFileNames(f);
   fprintf(f, "   > PrintEntry(f, i=30);\n");
   PrintEntry(f, 30);
   fprintf(f, "   > PrintExp(f, i=30);\n");
   PrintExp(f, 30);
   fprintf(f, "\n");
   fprintf(f, "   > PrintFromExpList(f, commasep_exps=\"BALLOON(1980,CRIS(1998\");\n");
   PrintFromExpList(f, "BALLOON(1980,CRIS(1998");
   fprintf(f, "\n");
   fprintf(f, "   > PrintExps(f, l_qty=\"SubFe/Fe\", eknmin=2, eknmax=10);\n");
   PrintExps(f, "SubFe/Fe", 2., 10.);
   fprintf(f, "\n");
   fprintf(f, "\n");

   fprintf(f, " * Queries and subsets:\n");
   Int_t ii = IndexDataClosestMatch("B/C", "HEAO,CREAM", 10., kEKN, f);
   fprintf(f, "   > IndexDataClosestMatch(\"B/C\", \"HEAO,CREAM\", e_val= 10., e_axis=kEKN, f);  => index=%d\n", ii);
   fprintf(f, "   > PrintEntry(f, %d);\n", ii);
   PrintEntry(f, ii);
   string qties_exp_etype = "B/C:HEAO,ACE:kEKN;C,N,O:HEAO,CREAM,TRACER:kEKN";
   fprintf(f, "   > TUDataSet *subset = OrphanFormSubSet(\"%s\", f, ekn_min=0.1, ekn_max=100.)\n", qties_exp_etype.c_str());
   TUDataSet *subset = OrphanFormSubSet(qties_exp_etype, f, 0.1, 100.);
   fprintf(f, "   > subset->GetQtiesEtypesphiFF() = %s\n", subset->GetQtiesEtypesphiFF().c_str());
   fprintf(f, "   > subset->Print(f, 0);\n");
   subset->Print(f, 0);
   fprintf(f, "   > subset->PrintExpsQties(f, is_expqty_or_qtyexp=false);\n");
   subset->PrintExpsQties(f, false);
   fprintf(f, "   > subset->PrintExpsQties(f, is_expqty_or_qtyexp=true);\n");
   subset->PrintExpsQties(f, true);
   fprintf(f, "   > delete subset;\n");
   delete subset;
   fprintf(f, "\n");
   fprintf(f, "\n");


   fprintf(f, "   > TUDataSet *subset = OrphanFormSubSet(\"%s\", f, ekn_min=0.1, ekn_max=100., tstart=\"1998-01-01 00:00:00\",tstop=\"2002-01-01 00:00:00\")\n", qties_exp_etype.c_str());
   subset = OrphanFormSubSet(qties_exp_etype, f, 0.1, 100., "1998-01-01 00:00:00", "2002-01-01 00:00:00");
   fprintf(f, "   > subset->GetQtiesEtypesphiFF() = %s\n", subset->GetQtiesEtypesphiFF().c_str());
   fprintf(f, "   > subset->Print(f, 0);\n");
   subset->Print(f, 0);
   fprintf(f, "   > subset->IndicesData4AllExpQty(gENUM_ETYPE etype=kEKN, vector<pair<Int_t,Int_t> > idata_start_stop)\n");
   vector<pair<Int_t, Int_t> > idata_start_stop;
   subset->IndicesData4AllExpQty(kEKN, idata_start_stop);
   fprintf(f, "      - Loop on list and print Exp_Qty_Etype [start,stop] and fill/print covariance:\n");
   for (Int_t i = 0; i < (Int_t)idata_start_stop.size(); ++i) {
      TUDataEntry *first = subset->GetEntry(idata_start_stop[i].first);
      TUDataEntry *last = subset->GetEntry(idata_start_stop[i].second);
      Int_t n_data = idata_start_stop[i].second - idata_start_stop[i].first + 1;
      if (i == 0) {
         fprintf(f, "        [%s, %s], with Ndata = %d\n", first->FormExpQtyEType().c_str(), last->FormExpQtyEType().c_str(), n_data);
         Int_t n_types = 2;
         vector<vector<Double_t> > cov_all(n_types, vector<Double_t>(n_data * n_data, 0.));
         vector<string> err_types(n_types);
         err_types[0] = "test1";
         err_types[1] = "test2";
         fprintf(f, "     [covariance values used for test]\n");
         for (Int_t t = 0; t < n_types; t += 1) {
            for (Int_t k = 0; k < n_data; ++k) {
               fprintf(f, "     ");
               for (Int_t kk = 0; kk < n_data; ++kk) {
                  cov_all[t][k * n_data + kk] = 1. - (t + 1) * 0.01 * fabs(k + kk) * fabs(k - kk);
                  fprintf(f, "  cov_all[%d][%d][%d]=%.1le", t, k, kk, cov_all[t][k * n_data + kk]);
               }
               fprintf(f, "\n");
            }
         }
         fprintf(f, "     > subset->LoadDataRelCov(i_data=%d, err_types; ne_cov=%d, cov_all, f_log=f)\n", idata_start_stop[i].first, n_data);
         subset->LoadDataRelCov(idata_start_stop[i].first, err_types, n_data, cov_all, f);
         fprintf(f, "     > subset->PrintDataRelCov(f, k_type=1, i_data_start=%d, i_data_stop=%d)\n", idata_start_stop[i].first, idata_start_stop[i].second);
         subset->PrintDataRelCov(f, -1, idata_start_stop[i].first, idata_start_stop[i].second);
      }
   }
   fprintf(f, "   > delete subset;\n");
   delete subset;
   fprintf(f, "\n");
   fprintf(f, "\n");


   fprintf(f, "   > TUDataSet *subset = OrphanFormSubSet(\"B/C:AMS:kR\", f)\n");
   subset = OrphanFormSubSet("B/C:AMS:kR", f);
   fprintf(f, "   > subset->GetQtiesEtypesphiFF() = %s\n", subset->GetQtiesEtypesphiFF().c_str());
   fprintf(f, "   > subset->Print(f, 0);\n");
   subset->Print(f, 0);
   fprintf(f, "   > subset->IndicesData4AllExpQty(gENUM_ETYPE etype=kR, vector<pair<Int_t,Int_t> > idata_start_stop)\n");
   idata_start_stop.clear();
   subset->IndicesData4AllExpQty(kR, idata_start_stop);
   fprintf(f, "      - Loop on list and print Exp_Qty_Etype [start,stop] and fill/print covariance:\n");
   Int_t i_start = idata_start_stop[0].first;
   Int_t i_stop = idata_start_stop[0].second;
   string file_relcov = "$USINE/inputs/CRDATA_COVARIANCE/cov_AMS02_201105201605__BC_R.dat";
   fprintf(f, "     > subset->LoadDataRelCov(i_data_start=%d, i_data_stop=%d, file_relcov=%s, f_log=f)\n", i_start, i_stop, file_relcov.c_str());
   subset->LoadDataRelCov(i_start, i_stop, file_relcov, f);
   fprintf(f, "     > subset->GetEntry(i_start+2)->PrintDataRelCov(f, t_type=-1, 2, 4)\n");
   subset->GetEntry(i_start + 2)->PrintDataRelCov(f, -1, 2, 4);
   fprintf(f, "     > subset->GetEntry(i_start+3)->PrintDataRelCov(f, t_type=-1, 2, 4)\n");
   subset->GetEntry(i_start + 3)->PrintDataRelCov(f, -1, 2, 4);
   fprintf(f, "     > subset->GetEntry(i_start+4)->PrintDataRelCov(f, t_type=-1, 2, 4)\n");
   subset->GetEntry(i_start + 4)->PrintDataRelCov(f, -1, 2, 4);
   Double_t emin = 5., emax = 9.;

   fprintf(f, "   > TUDataSet *subsub = subset->OrphanFormSubSet(\"B/C:AMS:kR\", f, emin=%f.1, emax=%f.1);\n", emin, emax);
   TUDataSet *subsub = subset->OrphanFormSubSet("B/C:AMS:kR", f, emin, emax);
   fprintf(f, "   > subset->GetQtiesEtypesphiFF() = %s\n", subset->GetQtiesEtypesphiFF().c_str());
   fprintf(f, "   > subsub->Print(f, 0);\n");
   subsub->Print(f, 0);
   fprintf(f, "   > subsub->PrintDataRelCov(f, k_type=-1, 0, subsub->GetNData()-1=%d);\n", subsub->GetNData() - 1);
   subsub->PrintDataRelCov(f, -1, 0, subsub->GetNData() - 1);
   fprintf(f, "   > delete subsub;\n");
   delete subsub;
   fprintf(f, "   > delete subset;\n");
   delete subset;
   fprintf(f, "\n");
   fprintf(f, "\n");

   fprintf(f, "   > TUDataSet *subset = OrphanFormSubSet(\"ELECTRON,POSITRON:PAMELA,AMS:kEK\", f)\n");
   subset = OrphanFormSubSet("ELECTRON,POSITRON:PAMELA,AMS:kEK", f);
   fprintf(f, "   > subset->GetQtiesEtypesphiFF() = %s\n", subset->GetQtiesEtypesphiFF().c_str());
   fprintf(f, "   > subset->Print(f, 0);\n");
   subset->Print(f, 0);
   fprintf(f, "   > delete subset;\n");
   delete subset;
   fprintf(f, "\n");
   fprintf(f, "\n");

   subset = NULL;
}
