// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

// C/C++ include
// ROOT include
// USINE include
#include "../include/TUMath.h"
#include "../include/TURunOutputs.h"

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TURunOutputs                                                         //
//                                                                      //
// Storage container for USINE propagation runs.                        //
//                                                                      //
// The class TURunOutputs is a class containing user-queried combos,    //
// models, and solar modulation levels, and the corresponding results   //
// (fluxes, ratios, isotopic content, primary fraction, etc.). These    //
// quantities are then used in plotting functions.                      //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

ClassImp(TURunOutputs)

//______________________________________________________________________________
TURunOutputs::TURunOutputs()
{
   // ****** Default constructor ******

   fAxesCrE = NULL;
   fFitData = NULL;
   Initialise();
}

//______________________________________________________________________________
TURunOutputs::TURunOutputs(TURunOutputs const &outputs)
{
   // ****** Copy constructor ******
   //  output            Object to copy

   Copy(outputs);
}

//______________________________________________________________________________
TURunOutputs::~TURunOutputs()
{
   // ****** Default destructor ******

   Initialise();
}

//______________________________________________________________________________
void TURunOutputs::Copy(TURunOutputs const &outputs)
{
   //--- Copies content of axis in class.
   //  outputs           Values to copy from


   // Models & combo-related
   SetCombosAndResize(outputs.GetModels(), outputs.GetAxesCrE(),
                      outputs.FormComboEtypephiff(), outputs.GetEPower(),
                      false, stdout);

   // Exp-related
   // fIndexExp4FitData[NCombos][NExps]
   fIndexExp4FitData.clear();
   fIndexExp4FitData.resize(outputs.GetNCombos());
   // fIsExpSolModNuis[NCombos][NExps]
   fIsExpSolModNuis.clear();
   fIsExpSolModNuis.resize(outputs.GetNCombos());
   CopyExpFitData(outputs);

   // TOA/IS quantities
   CopyISAndTOA(outputs);
}

//______________________________________________________________________________
void TURunOutputs::CopyExpFitData(TURunOutputs const &outputs)
{
   //--- Copies fIndexExp4FitData and fIsExpSolModNuis elements.
   //  outputs           Values to copy from

   for (Int_t i = 0; i < outputs.GetNCombos(); ++i) {
      fIndexExp4FitData[i].resize(GetNExp4FitData(i));
      fIsExpSolModNuis[i].resize(GetNExp4FitData(i));
      for (Int_t e = 0; e < outputs.GetNExp4FitData(i); ++e) {
         fIndexExp4FitData[i][e] = outputs.GetIndexExp4FitData(i, e);
         fIsExpSolModNuis[i][e] = outputs.GetIsExpSolModNuis(i, e);
      }
   }
}

//______________________________________________________________________________
void TURunOutputs::CopyISAndTOA(TURunOutputs const &outputs)
{
   //--- Copies fTOACombos[0-2], fTOACombosRatios, fIsotFrac, and fIsotFracAndZ.
   //  outputs           Values to copy from

   for (Int_t m = 0; m < outputs.GetNModels(); ++m) {
      for (Int_t e = 0; e < GetNE(); ++e) {

         // Loop on combos (TOA)
         for (Int_t i = 0; i < outputs.GetNCombos(); ++i) {
            for (Int_t p = 0; p < outputs.GetNphiFF(i); ++p) {
               // [3][NCombos][NModels][NSolMod][NE]
               for (Int_t t = 0; t < 3; ++t)
                  SetTOACombo(t, i, m, p, e, outputs.GetTOACombo(t, i, m, p, e));

               // [NCombos][NRatios][NModels][NSolMod][NE]
               for (Int_t r = 0; r < outputs.GetNCombosRatios(i); ++r)
                  SetTOAComboRatio(i, r, m, p, e, outputs.GetTOAComboRatio(i, r, m, p, e));
            }
         }

         // Loop on all CRs in combos (IS isotopic fraction)
         // [NIsot][NModels][NE]
         for (Int_t i = 0; i < outputs.GetNIsot(); ++i)
            SetIsotFrac(i, m, e, outputs.GetIsotFrac(i, m, e));

         // Loop on all CRs and Z in combos (IS primary fraction)
         // [NIsotAndZ][NModels][NE]
         for (Int_t i = 0; i < outputs.GetNIsotAndZ(); ++i)
            SetIsotAndZPrimFrac(i, m, e, outputs.GetIsotAndZPrimFrac(i, m, e));
      }
   }
}

//______________________________________________________________________________
void TURunOutputs::FillAsCLs(vector<TURunOutputs> const &sample_results, Bool_t is_median,
                             Bool_t is_1sigma, Bool_t is_2sigma, Bool_t is_3sigma)
{
   //--- Fills CLs from a vector of 'TURunOutputs' values. The returned 'results' can be directly passed to
   //    the PlotSpectra() routine in TURunPropagation.
   //  sample_results        Vector of structure containing all results
   //  is_median             Whether or not to fill median
   //  is_1sigma             Whether or not to fill 1-sigma CL
   //  is_2sigma             Whether or not to fill 2-sigma CL
   //  is_3sigma             Whether or not to fill 3-sigma CL


   // Extract model names for display for the selected CLs
   //    -3\sigma => 0.13th percentile
   //    -2\sigma => 2.28th percentile
   //    −1\sigma => 15.87th percentile
   //     0\sigma => 50th percentile
   //    +1\sigma => 84.13th percentile
   //    +2\sigma => 97.72nd percentile
   //    +3\sigma => 99.87th percentile
   vector<Double_t> percentiles;
   vector<string> cl_models;
   Int_t n_quant = 0;
   if (is_median) {
      ++n_quant;
      cl_models.push_back("Median");
      percentiles.push_back(50.);
   }
   if (is_1sigma) {
      n_quant += 2;
      cl_models.push_back("-1#sigma");
      cl_models.push_back("+1#sigma");
      percentiles.push_back(15.87);
      percentiles.push_back(84.13);
   }
   if (is_2sigma) {
      n_quant += 2;
      cl_models.push_back("-2#sigma");
      cl_models.push_back("+2#sigma");
      percentiles.push_back(2.28);
      percentiles.push_back(97.72);
   }
   if (is_3sigma) {
      n_quant += 2;
      cl_models.push_back("-3#sigma");
      cl_models.push_back("+3#sigma");
      percentiles.push_back(0.13);
      percentiles.push_back(99.87);
   }

   // Set combo and resize (no fit data when quantiles used)
   Initialise();
   SetCombosAndResize(cl_models, sample_results[0].GetAxesCrE(),
                      sample_results[0].FormComboEtypephiff(), sample_results[0].GetEPower(),
                      false, stdout);


   Int_t i_propmod = 0; // Only allows or one propagation model
   Int_t n_samples = sample_results.size();

   // TOA QUANTILES
   for (Int_t i_combo = 0; i_combo < GetNCombos(); ++i_combo) {
      for (Int_t i_phi = 0; i_phi < GetNphiFF(i_combo); ++i_phi) {
         for (Int_t k = 0; k < GetNE(); ++k) {
            // fTOACombos[0-2][NCombos][NQuant][NphiFF][NE]
            for (Int_t i_pst = 0; i_pst < 3; ++i_pst) {
               vector<Double_t> values(n_samples, 0.);
               for (Int_t i_sample = 0; i_sample < n_samples; ++i_sample)
                  values[i_sample] = sample_results[i_sample].GetTOACombo(i_pst, i_combo, i_propmod, i_phi, k);
               // Extract quantiles
               vector<Double_t> qvals;
               TUMath::Quantiles(values, percentiles, qvals);
               // Copy values(quantiles) found assuming each quantile is a model
               for (Int_t i_q = 0; i_q < n_quant; ++i_q)
                  SetTOACombo(i_pst, i_combo, i_q, i_phi, k, qvals[i_q]);
            }

            // fTOACombosRatios[NCombos][NRatios][NQuant][NphiFF][NE]
            for (Int_t i_r = 0; i_r < GetNCombosRatios(i_combo); ++i_r) {
               vector<Double_t> values(n_samples);
               // Loop on samples
               for (Int_t i_sample = 0; i_sample < n_samples; ++i_sample)
                  values[i_sample] = sample_results[i_sample].GetTOAComboRatio(i_combo, i_r, i_propmod, i_phi, k);
               // Extract quantiles
               vector<Double_t> qvals;
               TUMath::Quantiles(values, percentiles, qvals);
               // Copy values(quantiles) found assuming each quantile is a model
               for (Int_t i_q = 0; i_q < n_quant; ++i_q)
                  SetTOAComboRatio(i_combo, i_r, i_q, i_phi, k, qvals[i_q]);
            }
         }
      }
   }

   // Quantiles fIsotFrac[NIsot][NQuant][NE]
   for (Int_t i = 0; i < GetNIsot(); ++i) {
      for (Int_t k = 0; k < GetNE(); ++k) {
         vector<Double_t> values(n_samples);
         for (Int_t i_sample = 0; i_sample < n_samples; ++i_sample)
            values[i_sample] = sample_results[i_sample].GetIsotFrac(i, i_propmod, k);
         vector<Double_t> qvals;
         TUMath::Quantiles(values, percentiles, qvals);
         for (Int_t i_q = 0; i_q < n_quant; ++i_q)
            SetIsotFrac(i, i_q, k, qvals[i_q]);
      }
   }

   // Quantiles fIsotAndZPrimFrac[NIsotAndZ][NQuant][NE]
   for (Int_t i = 0; i < GetNIsotAndZ(); ++i) {
      for (Int_t k = 0; k < GetNE(); ++k) {
         vector<Double_t> values(n_samples);
         for (Int_t i_sample = 0; i_sample < n_samples; ++i_sample)
            values[i_sample] = sample_results[i_sample].GetIsotAndZPrimFrac(i, i_propmod, k);
         vector<Double_t> qvals;
         TUMath::Quantiles(values, percentiles, qvals);
         for (Int_t i_q = 0; i_q < n_quant; ++i_q)
            SetIsotAndZPrimFrac(i, i_q, k, qvals[i_q]);
      }
   }


   // Quantiles for model parameters
   for (Int_t i = 0; i < sample_results[0].GetNPropModTitles(); ++i) {
      fPropModIndexStart.push_back(sample_results[0].GetPropModIndexStart(i));
      fPropModTitles.push_back(sample_results[0].GetPropModTitles(i));
   }
   Int_t n_propmodpars = sample_results[0].GetNPropModPars();
   for (Int_t i = 0; i < n_propmodpars; ++i)
      fPropModParNames.push_back(sample_results[0].GetPropModParNames(i));

   fPropModParValues.resize(n_propmodpars);
   fPropModParUnits.resize(n_propmodpars);

   for (Int_t i = 0; i < n_propmodpars; ++i) {
      vector<Double_t> values(n_samples);
      for (Int_t i_sample = 0; i_sample < n_samples; ++i_sample)
         values[i_sample] = sample_results[i_sample].GetPropModParValues(i);
      vector<Double_t> qvals;
      TUMath::Quantiles(values, percentiles, qvals);
      string vals_units = "";
      for (Int_t i_q = 0; i_q < n_quant; ++i_q) {
         if (i_q % 2 != 0)
            continue;
         if (i_q > 0)
            vals_units += Form("+/-%.2le", qvals[i_q] - qvals[0]);
      }
      vals_units += " " + sample_results[0].GetPropModParUnits(i);
      fPropModParValues[i] = qvals[0];
      fPropModParUnits[i] = vals_units;
   }

}

//______________________________________________________________________________
string TURunOutputs::FormComboEtypephiff() const
{
   //--- Form combos_etypes_phiff from class fCombos, fETypes, and fphiFF.

   string combos_etypes_phiff = "";
   for (Int_t i = 0; i < GetNCombos(); ++i) {
      string tmp_phi = "";
      for (Int_t p = 0; p < GetNphiFF(i); ++p) {
         if (p != GetNphiFF(i) - 1)
            tmp_phi += (string)Form("%le", GetphiFF(i, p)) + ",";
         else
            tmp_phi += Form("%le", GetphiFF(i, p));
      }
      string type = TUEnum::gENUM_ETYPE_NAME[GetEType(i)];
      TUMisc::UpperCase(type);
      string tmp = GetCombo(i) + ":k" + type;
      if (i != GetNCombos() - 1)
         combos_etypes_phiff += tmp + ":" + tmp_phi + ";";
      else
         combos_etypes_phiff += tmp + ":" + tmp_phi;
   }

   return combos_etypes_phiff;
}

//______________________________________________________________________________
void TURunOutputs::Initialise()
{
   //--- Empty all vectors
   if (fAxesCrE)
      delete fAxesCrE;
   fAxesCrE = NULL;
   fCombos.clear();
   fCombosRatios.clear();
   fEaxisNamesUnits.clear();
   fEaxisVals.clear();
   fEPower = 0.;
   fETypes.clear();
   if (fFitData)
      delete fFitData;
   fFitData = NULL;
   fIndexExp4FitData.clear();
   fIsExpSolModNuis.clear();
   fIsot.clear();
   fIsotFrac.clear();
   fIsotAndZ.clear();
   fIsotAndZPrimFrac.clear();
   fModels.clear();
   fphiFF.clear();
   fPropModIndexStart.clear();
   fPropModTitles.clear();
   fPropModParNames.clear();
   fPropModParValues.clear();
   fPropModParUnits.clear();
   fTOACombos[0].clear();
   fTOACombos[1].clear();
   fTOACombos[2].clear();
   fTOACombosRatios.clear();
}

//______________________________________________________________________________
void TURunOutputs::SetCombosAndResize(vector<string> const &models, TUAxesCrE *axes_cre, string const &combos_etypes_phiff, Double_t const &e_power, Bool_t is_print, FILE *f_print)
{
   //--- Sets necessary class members from query, and resize storage members (for IS and TOA fluxes)
   //  models                [NModels] Names of models for which calculations is performed
   //  axes_cre              CRs and energy considered
   //  combos_etypes_phiff   List of CR fluxes/ratios and associated E types/phi_FF to calculate (e.g., B/C,O:kEKN:0.73,0.5;C,N,O:kR:0.38)
   //  e_power               Fluxes are multiplied by e_type^{e_power} (if not ratio, or <LnA>)
   //  is_print              Whether to print or not
   //  f_print               File in which to print

   Initialise();

   // Set models and CR/E axes
   if (!axes_cre)
      TUMessages::Error(f_print, "TURunOutputs", "SetCombosAndResize", "Empty TUAxesCrE, cannot proceed");
   fAxesCrE = new TUAxesCrE();
   fAxesCrE->Copy(*axes_cre);

   fModels = models;
   fEPower = e_power;

   // Extract fCombo[NCombos], fEtype[NCombos], and fphiFF[NCombos][NSolMod]
   vector<gENUM_ETYPE> etypes;
   vector<vector<Double_t> > phiff;
   fAxesCrE->ExtractComboEtypephiff(combos_etypes_phiff, fCombos, fETypes, fphiFF, is_print, f_print);
   if (fCombos.size() == 0) {
      string message = "No valid combo found in " + combos_etypes_phiff;
      TUMessages::Error(f_print, "TURunOutputs", "SetCombosAndResize", message);
   }

   // Extract quantities derived from combo
   //  -> all CRs in all combos
   fAxesCrE->CombosToCRNames(fCombos, fIsot);
   //  -> all CRs and elements in all combos
   fAxesCrE->CombosToElementAndCRNames(fCombos, fIsotAndZ);

   //  -> if combo a ratio, extract possible ratio with isotopes
   //     in the numerator and also extract energy values
   fCombosRatios.resize(GetNCombos());
   fEaxisVals.resize(GetNCombos());
   fEaxisNamesUnits.resize(GetNCombos());
   for (Int_t i = 0; i < GetNCombos(); ++i) {
      // Set energy units
      fEaxisNamesUnits[i] = TUEnum::Enum2NameUnit(GetEType(i));

      // Set energies
      fEaxisVals[i].clear();
      TUAxis *e_grid =  fAxesCrE->OrphanGetECombo(GetCombo(i), GetEType(i), f_print);
      fEaxisVals[i] = TUMisc::Array2Vector(e_grid->GetVals(), e_grid->GetN());
      delete e_grid;
      e_grid = NULL;

      // Set combo ratios
      fCombosRatios[i].clear();
      // Extract numerator of combo:
      //   if ratio: num_denom[0]=numerator, num_denom[1]=denominator
      //   if not ratio: num_denom[0]=combo;
      vector<string> num_denom;
      TUMisc::String2List(fCombos[i], "/", num_denom);
      // Search for all CRs and elements in numerator
      vector<string> num_crs;
      Int_t dummy_n = 0;
      fAxesCrE->ComboToCRNames(num_denom[0], num_crs, dummy_n, false, stdout);
      if (num_crs.size() == 1)
         continue;

      // Loop on numerator quantities
      for (Int_t kk = 0; kk < (Int_t)num_crs.size(); ++kk) {
         // If ratio, form isotopic ratio and add in list of names
         string isot = num_crs[kk];
         if (num_denom.size() == 2) isot += "/" + num_denom[1];
         fCombosRatios[i].push_back(isot);
      }
   }

   // Resize members
   ResizeFluxes(f_print);
}

//______________________________________________________________________________
void TURunOutputs::ResizeFluxes(FILE *f_print)
{
   //--- Resizes all vectors (if applies) for query.
   //  f_print                 File in which to print log

   if (GetNCombos() == 0)
      TUMessages::Error(f_print, "TURunOutputs", "ResizeFluxes", "No combo, cannot proceed");
   else if (GetNModels() == 0)
      TUMessages::Error(f_print, "TURunOutputs", "ResizeFluxes", "No model, cannot proceed");
   else if (GetNE() == 0)
      TUMessages::Error(f_print, "TURunOutputs", "ResizeFluxes", "NE = 0, cannot proceed");
   else if (GetNIsot() == 0)
      TUMessages::Error(f_print, "TURunOutputs", "ResizeFluxes", "fIsot empty, cannot proceed");
   else if (GetNIsotAndZ() == 0)
      TUMessages::Error(f_print, "TURunOutputs", "ResizeFluxes", "fIsotAndZ empty, cannot proceed");

   // fIsotFrac[NIsot][NModels][NE]
   fIsotFrac.clear();
   fIsotFrac.resize(GetNIsot(), vector<vector<Double_t> >(GetNModels(), vector<Double_t>(GetNE(), 0.)));

   // fIsotAndZPrimFrac[NIsotAndZ][NModels][NE]
   fIsotAndZPrimFrac.clear();
   fIsotAndZPrimFrac.resize(GetNIsotAndZ(), vector<vector<Double_t> >(GetNModels(), vector<Double_t>(GetNE(), 0.)));


   // fTOACombos[3][NCombos][NModels][NSphiFF][NE]
   for (Int_t l = 0; l < 3; ++l) {
      fTOACombos[l].clear();
      fTOACombos[l].resize(GetNCombos());

      for (Int_t i = 0; i < GetNCombos(); ++i)
         fTOACombos[l][i].resize(GetNModels(), vector<vector<Double_t> >(GetNphiFF(i), vector<Double_t>(GetNE(), 0.)));
   }

   // fTOACombosRatios[NCombos][NRatios][NModels][NSolMod][NE]
   fTOACombosRatios.clear();
   fTOACombosRatios.resize(GetNCombos());
   for (Int_t i = 0; i < GetNCombos(); ++i)
      fTOACombosRatios[i].resize(GetNCombosRatios(i), vector<vector<vector<Double_t> > >(GetNModels(), vector<vector<Double_t> >(GetNphiFF(i), vector<Double_t>(GetNE(), 0.))));

}

//______________________________________________________________________________
void TURunOutputs::SetFitDataRelated(TUDataSet const *fit_data, vector<vector<Double_t>> const &phiff, vector<vector<Int_t> > const &list_iexp_fitdata, vector<vector<Bool_t> > const &list_isphinuis)
{
   //--- Sets FitData dependent quantities (used for display later on)
   //  fit_data          Data used in fit
   //  list_phiff        Vector of phiFF values associated to each list_combo[i]
   //  list_iexp_fitdata Vector of indices (exp) associated to each list_combo[i] (with list_phi[i].size=list_iexp_fitdata[i].size())
   //  list_isphinuis    Vector of boolean to track whether exp has an associated nuisance parameter for each list_combo[i]

   fIndexExp4FitData.clear(),
                           fIsExpSolModNuis.clear();

   if (fFitData)
      delete fFitData;
   fFitData = new TUDataSet();

   if (fit_data) {
      fFitData->Copy(*fit_data);
      fphiFF.clear();

      fphiFF = phiff;
      fIndexExp4FitData = list_iexp_fitdata;
      fIsExpSolModNuis = list_isphinuis;

   }
}
