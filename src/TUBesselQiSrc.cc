// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

// C/C++ include
#include <fstream>
// ROOT include
// USINE include
#include "../include/TUBesselQiSrc.h"
#include "../include/TUNumMethods.h"
#include "../include/TUSrcTemplates.h"
#include "../include/TUValsTXYZEVirtual.h"

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TUBesselQiSrc                                                        //
//                                                                      //
// Fourier-Bessel(FB) decomposition of sources along radial coordinate. //
//                                                                      //
// The class TUBesselQiSrc calculates and stores Fourier-Bessel (FB)    //
// coefficients for sources in a thin disc (e.g., astrophysical src     //
// with spatial distribution q(r)) or for sources in the diffusive      //
// halo (e.g., dark matter halo, with spatial distribution q(r,z)).     //
//                                                                      //
// The most important data members of this class are:                   //
//     - fQi: FB coefficients for sources in the disc;
//     - fQiz: FB coefficients on z-grid for sources in the halo;
// The most important method is that calculating Qi coefficients        //
// (which uses Bessel J0 and J1 functions, see TUBesselJ0.h), with      //
//BEGIN_LATEX(fontsize=14, separator='=', align=rcl)
// Q_{i}=#frac{2}{R^{2} J_{1}^{2}(#zeta_{i}))} #times #int_{0}^{R} r q(r) J_{0}(#zeta_{i} r/R) dr.
// such as
// q(r)=#sum_{i#equiv 1}^{#infty} Q_{i} #times J_{0} #left( #zeta_{i} #frac{r}{R} #right).
//END_LATEX
//                                                                      //
// Note that each calculation is stored in a file whose name is formed  //
// from a unique ID (UID) of the spatial distribution and parameters.   //
// For more details and references on how this class is used, see the   //
// 2D (cylindrical symmetry) propagation model TUModel2DKisoVc.h.       //
//////////////////////////////////////////////////////////////////////////


ClassImp(TUBesselQiSrc)

//______________________________________________________________________________
TUBesselQiSrc::TUBesselQiSrc()
{
   // ****** Default constructor ******

   Initialise(false);
}

//______________________________________________________________________________
TUBesselQiSrc::~TUBesselQiSrc()
{
   // ****** default destructor ******

   Initialise(true);
}

//______________________________________________________________________________
void TUBesselQiSrc::CalculateAndStoreQi(TUBesselJ0 *bessel, TUValsTXYZEVirtual *spat_dist, Bool_t is_verbose, FILE *f_log)
{
   //--- Calculates, fills, and stores fQi Fourier-Bessel coefficients for a given
   //    spatial distribution q(r) = spat_dist, and save results in a file with
   //    a unique ID uniquely related to this distrib (see TUBesselJ0.h for more
   //    details on bessels used here).
   //    N.B.: Qi has the same dimension (unit) as q(r).
   //
   //  bessel            Bessel functions used for calculation
   //  spat_dist         Spatial distribution for which to calculate FB coefs.
   //  is_verbose        If true, compare q(r) to value obtained from resummation of Qi
   //  f_log             Log for USINE
   //
   //BEGIN_LATEX(fontsize=14)
   // Q_{i}=#frac{2}{R^{2} J_{1}^{2}(#zeta_{i}))} #times #int_{0}^{R} r q(r) J_{0}(#zeta_{i} r/R) dr.
   //END_LATEX

   // Get unique ID for this
   fTemplateName = spat_dist->GetName();
   fUID = spat_dist->UID();

   if (!fAxisr)
      TUMessages::Error(f_log, "TUBesselQiSrc", "CalculateAndStoreQi", "fAxisr is NULL, set it first!");
   if (!bessel || bessel->GetNBessel() == 0)
      TUMessages::Error(f_log, "TUBesselQiSrc", "CalculateAndStoreQi", "bessel is NULL or empty, fill it first!");

   ResetQiz();
   ResetQi();
   fNi = bessel->GetNBessel();
   fQi = new Double_t[fNi];
   fIsUseQi = false;

   // Fill values of q(r) on grid
   vector<Double_t> qr;
   TUCoordTXYZ coord_txyz;
   for (Int_t i_r = 0; i_r < fAxisr->GetN(); ++i_r) {
      coord_txyz.SetVal(1 /*x*/, fAxisr->GetVal(i_r));
      Double_t res = spat_dist->ValueTXYZ(&coord_txyz);
      qr.push_back(res);
   }

   // N.B: we test for several cases which have analytical Qi
   //    - constant
   //    - step_up:
   //       * 0 for r<r1
   //       * cst for r>r1
   //    - step_down:
   //       * cst for r<r1
   //       * 0 for r>r1
   //    - square:
   //       * 0 for r<r1
   //       * cst for r1<r<r2
   //       * 0 for r>r2
   vector<Int_t> indices_r;
   vector<Double_t> vals_qr;
   vals_qr.push_back(qr[0]);
   Bool_t is_analytical = true;
   for (Int_t i_r = 1; i_r < fAxisr->GetN(); ++i_r) {

      // If more than 3 different values in tested, not analytical
      if (indices_r.size() > 3) {
         is_analytical = false;
         break;
      }

      // Check whether change of value w.r.t. last in vals
      Double_t last_vals = vals_qr[vals_qr.size() - 1];
      if ((fabs(qr[i_r]) > 1.e-40 && (fabs((qr[i_r] - last_vals) / qr[i_r]) > 1.e-3))
            || ((fabs(qr[i_r]) < 1.e-40 && fabs(qr[i_r] - last_vals) > 1.e-40))) {
         vals_qr.push_back(qr[i_r]);
         indices_r.push_back(i_r);
      }
   }

   // Check number of values over interval to decide if analytical
   if (is_analytical) {
      if (indices_r.size() == 0) {
         // Constant
         for (Int_t i = 0; i < bessel->GetNBessel(); ++i) {
            Double_t qi = 2. / (bessel->GetZero(i) * bessel->GetJ1Zero(i));
            fQi[i] = vals_qr[0] * qi;
         }
      } else if (indices_r.size() == 1) {
         Double_t r_step = fAxisr->GetVal(indices_r[0]);
         Double_t R = fAxisr->GetMax();
         // step-up
         if (fabs(vals_qr[0]) < 1.e-40) {
            for (Int_t i = 0; i < bessel->GetNBessel(); ++i) {
               Double_t qi = 2. * (bessel->GetJ1Zero(i)
                                   - r_step / R * TUMath::JpOptimized(1, r_step / R * bessel->GetZero(i)))
                             / (bessel->GetZero(i) * TUMath::Sqr(bessel->GetJ1Zero(i)));
               fQi[i] = vals_qr[1] * qi;
            }
         } else if (fabs(vals_qr[1]) < 1.e-40) {
            // step-down
            for (Int_t i = 0; i < bessel->GetNBessel(); ++i) {
               Double_t qi = 2. / (bessel->GetZero(i) * bessel->GetJ1Zero(i))
                             - 2. * (bessel->GetJ1Zero(i)
                                     - r_step / R * TUMath::JpOptimized(1, r_step / R * bessel->GetZero(i)))
                             / (bessel->GetZero(i) * TUMath::Sqr(bessel->GetJ1Zero(i)));
               fQi[i] = vals_qr[0] * qi;
            }
         }
      } else if (indices_r.size() == 2) {
         // square
         if (fabs(vals_qr[0]) < 1.e-40 && fabs(vals_qr[2]) < 1.e-40) {
            Double_t rin = fAxisr->GetVal(indices_r[0]);
            Double_t rout = fAxisr->GetVal(indices_r[1]);
            Double_t R = fAxisr->GetMax();
            for (Int_t i = 0; i < bessel->GetNBessel(); ++i) {
               Double_t qi = 2.*(rout / R * TUMath::JpOptimized(1, rout / R * bessel->GetZero(i))
                                 - rin / R * TUMath::JpOptimized(1, rin / R * bessel->GetZero(i)))
                             / (bessel->GetZero(i) * TUMath::Sqr(bessel->GetJ1Zero(i)));
               fQi[i] = vals_qr[1] * qi;
            }
         }
      }
   } else {
      // No analytical calculation available -> perform numerical integration
      for (Int_t i = 0; i < bessel->GetNBessel(); ++i) {
         // Qi=2/R^2 J1^2(zero_i) * int_0^R r q(r) J0(zero_i*r/R) dr
         // -> integrand = r q(r) J0(zero_i*r/R)
         // Fill integrand
         vector<Double_t> integrand;
         for (Int_t i_r = 0; i_r < fAxisr->GetN(); ++i_r) {
            Double_t r = fAxisr->GetVal(i_r);
            Double_t arg_j = r / fAxisr->GetMax() * bessel->GetZero(i);
            integrand.push_back(r * qr[i_r]* TUMath::J0(arg_j));
         }

         Double_t res = 0.;
         TUNumMethods::IntegrationSimpsonLin(fAxisr, &integrand[0], 0, fAxisr->GetN() - 1, res);
         fQi[i] = res * 2. / (TUMath::Sqr(fAxisr->GetMax() * bessel->GetJ1Zero(i)));
      }
   }

   // Whether to check result or not
   if (is_verbose) {
      fprintf(f_log, "   # Check Qi coeffs for %s in range [%le,%le] %s (with Bringmann weight)\n",
              fUID.c_str(), fAxisr->GetMin(), fAxisr->GetMax(), fAxisr->GetUnit(true).c_str());
      fprintf(f_log, "   r       q(r)        ");
      if (fNi >= 9)
         fprintf(f_log, "9 coefs     ");
      if (fNi >= 100)
         fprintf(f_log, "100 coefs    ");
      if (fNi >= 200)
         fprintf(f_log, "200 coefs    ");
      if (fNi >= 500)
         fprintf(f_log, "500 coefs   ");
      if (fNi >= 1000)
         fprintf(f_log, "1000 coefs    ");
      fprintf(f_log, "\n");
      Int_t n_r = qr.size();
      for (Int_t i_r = 0; i_r < n_r; ++i_r) {
         if (i_r != 0 && i_r != n_r - 2) {
            Int_t mult = n_r / 20;
            if (mult != 0 && i_r % mult != 0)
               continue;
         }

         Double_t r = fAxisr->GetVal(i_r);
         Double_t rho = r / fAxisr->GetMax();
         fprintf(f_log, "%5.2f   %.4e   ", r, qr[i_r]);
         if (fNi >= 9)
            fprintf(f_log, "%+.4le   ", bessel->Coeffs2Func(rho, fQi, 9, true));
         if (fNi >= 100)
            fprintf(f_log, "%+.4le   ", bessel->Coeffs2Func(rho, fQi, 100, true));
         if (fNi >= 200)
            fprintf(f_log, "%+.4le   ", bessel->Coeffs2Func(rho, fQi, 200, true));
         if (fNi >= 500)
            fprintf(f_log, "%+.4le   ", bessel->Coeffs2Func(rho, fQi, 500, true));
         if (fNi >= 1000)
            fprintf(f_log, "%+.4le   ", bessel->Coeffs2Func(rho, fQi, 1000, true));
         fprintf(f_log, "\n");
      }
      fprintf(f_log, "\n");
   }

   // Prints Qi coeffs and info in '$USINE/inputs/qi_base.dat'
   string file = "$USINE/inputs/" + FormFileNameBase(true) + ".dat";
   fFileName = file;
   string indent = TUMessages::Indent(true);
   TUMessages::Indent(false);
   fprintf(f_log, "%s - Qi coeffs for spatial distribution %s saved in:\n", indent.c_str(), GetTemplateName().c_str());
   fprintf(f_log, "%s   %s\n\n", indent.c_str(), file.c_str());
   file = TUMisc::GetPath(file);
   FILE *fp = fopen(file.c_str(), "w");
   PrintQi(fp);
   fclose(fp);
}

//______________________________________________________________________________
void TUBesselQiSrc::CalculateAndStoreQiz(TUBesselJ0 *bessel, TUValsTXYZEVirtual *spat_dist, Bool_t is_verbose, FILE *f_log)
{
   //--- Calculates, fills, and stores fQiz Fourier-Bessel coefficients for a given
   //    spatial distribution q(r,z) = spat_dist, and save results in a file with
   //    a unique ID uniquely related to this distrib (see TUBesselJ0.h for more
   //    details on bessels used here).
   //    N.B.: Qi(z) has the same dimension (unit) as q(r,z).
   //
   //  bessel            Bessel functions used for calculation
   //  spat_dist         Spatial distribution for which to calculate FB coefs.
   //  is_verbose        If true, compare q(r) to value obtained from resummation of Qi
   //  f_log             Log for USINE
   //
   //BEGIN_LATEX(fontsize=14)
   // Q_{i}(z)=#frac{2}{R^{2} J_{1}^{2}(#zeta_{i}))} #times #int_{0}^{R} r q(r,z) J_{0}(#zeta_{i} r/R) dr.
   //END_LATEX

   // Get unique ID for this distribution
   fTemplateName = spat_dist->GetName();
   fUID = spat_dist->UID();

   if (!fAxisr)
      TUMessages::Error(f_log, "TUBesselQiSrc", "CalculateAndStoreQiz", "fAxisr is NULL, set it first!");
   if (!fAxisz)
      TUMessages::Error(f_log, "TUBesselQiSrc", "CalculateAndStoreQiz", "fAxisz is NULL, set it first!");
   if (!bessel || bessel->GetNBessel() == 0)
      TUMessages::Error(f_log, "TUBesselQiSrc", "CalculateAndStoreQiz", "bessel is NULL or empty, fill it first!");

   ResetQi();
   ResetQiz();
   fNi = bessel->GetNBessel();
   fQiz = new Double_t[GetNz()*fNi];
   fIsUseQiz = false;

   // Loop on z-grid
   TUCoordTXYZ coord_txyz;
   for (Int_t i_z = 0; i_z < GetNz(); ++i_z) {

      // q(r,z) for current z
      coord_txyz.SetVal(2, fAxisz->GetVal(i_z));
      vector<Double_t> qr;
      for (Int_t i_r = 0; i_r < fAxisr->GetN(); ++i_r) {
         coord_txyz.SetVal(0, fAxisr->GetVal(i_r));
         qr.push_back(spat_dist->ValueTXYZ(&coord_txyz));
      }

      // Perform numerical integration for each bessel order
      for (Int_t i = 0; i < bessel->GetNBessel(); ++i) {
         // Qiz=2/R^2 J1^2(zero_i) * int_0^R r q(r,z) J0(zero_i*r/R) dr
         // -> integrand = r q(r,z) J0(zero_i*r/R)
         // Fill integrand
         vector<Double_t> integrand;
         for (Int_t i_r = 0; i_r < fAxisr->GetN(); ++i_r) {
            Double_t r = fAxisr->GetVal(i_r);
            Double_t arg_j = r / fAxisr->GetMax() * bessel->GetZero(i);
            integrand.push_back(r * qr[i_r]* TUMath::J0(arg_j));
         }

         Double_t res = 0.;
         TUNumMethods::IntegrationSimpsonLin(fAxisr, &integrand[0], 0, fAxisr->GetN() - 1, res);
         fQiz[i_z * fNi + i] = res * 2. / (TUMath::Sqr(fAxisr->GetMax() * bessel->GetJ1Zero(i)));
      }


      // Whether to check result or not
      if (is_verbose) {
         if (i_z == 0) {
            if (is_verbose) {
               fprintf(f_log, "   # Check Qiz coeffs for %s in range r=[%le,%le] %s and range z=[%le,%le] %s (with Bringmann weight)\n",
                       fUID.c_str(), fAxisr->GetMin(), fAxisr->GetMax(), fAxisr->GetUnit(true).c_str(),
                       fAxisz->GetMin(), fAxisz->GetMax(), fAxisz->GetUnit(true).c_str());
               fprintf(f_log, "   r      z       q(r,z)      ");
               if (fNi >= 9)
                  fprintf(f_log, "9 coefs     ");
               if (fNi >= 25) {
                  fprintf(f_log, "25 coefs     ");
                  fprintf(f_log, "25 coefs+B05 ");
               }
               if (fNi >= 200)
                  fprintf(f_log, "200 coefs    ");
               if (fNi >= 500)
                  fprintf(f_log, "500 coefs   ");
               if (fNi >= 1000)
                  fprintf(f_log, "1000 coefs    ");
               fprintf(f_log, "\n");
            }
         }

         if (i_z != 0 && i_z != fAxisz->GetN() - 2) {
            Int_t multz = fAxisz->GetN() / 5;
            if (multz != 0 && i_z % multz != 0)
               continue;
         }

         Double_t z = fAxisz->GetVal(i_z);
         Int_t n_r = qr.size();
         for (Int_t i_r = 0; i_r < n_r; ++i_r) {
            if (i_r != 0 && i_r != n_r - 2) {
               Int_t multr = n_r / 10;
               if (multr != 0 && i_r % multr != 0)
                  continue;
            }

            Double_t r = fAxisr->GetVal(i_r);
            Double_t rho = r / fAxisr->GetMax();
            fprintf(f_log, "%5.2f   %5.2f   %.4e   ", r, z, qr[i_r]);
            if (fNi >= 9)
               fprintf(f_log, "%+.4le   ", bessel->Coeffs2Func(rho, &fQiz[i_z * fNi], 9, true));
            if (fNi >= 25) {
               fprintf(f_log, "%+.4le   ", bessel->Coeffs2Func(rho, &fQiz[i_z * fNi], 25, false));
               fprintf(f_log, "%+.4le   ", bessel->Coeffs2Func(rho, &fQiz[i_z * fNi], 25, true));
            }
            if (fNi >= 200)
               fprintf(f_log, "%+.4le   ", bessel->Coeffs2Func(rho, &fQiz[i_z * fNi], 200, true));
            if (fNi >= 500)
               fprintf(f_log, "%+.4le   ", bessel->Coeffs2Func(rho, &fQiz[i_z * fNi], 500, true));
            if (fNi >= 1000)
               fprintf(f_log, "%+.4le   ", bessel->Coeffs2Func(rho, &fQiz[i_z * fNi], 1000, true));
            fprintf(f_log, "\n");
         }
         fprintf(f_log, "\n");
      }
   }

// // N.B. : necessary for a Moore 98 profile with Gamma=1.5 otherwise eta=3/(3-2Gamma) diverges
// Double_t g = Gamma;
// if (g == 1.5) g = 1.49999999;
//
// Double_t eta = 3. / (3. - 2.*g);
// Double_t rho_c = pow(TUsinePhysics::Rsol_kpc() / r_c, g);
// // Approx 1.: we set a constant density for the profile for r<r_c, demanding an equivalent number
// // of annihilations in this region (this is unphysical as it leads to a discontinuity...)
// q_r_z = r * rho_c * rho_c * eta * pow((1. + pow(TUsinePhysics::Rsol_kpc() / ScaleRadius, Alpha)) / (1. + pow(r_spher / ScaleRadius, Alpha)), 2.*(Beta - g) / Alpha);;
//
//
// // Approx. 2: Barrau et al., astro-ph/0506389 (the formula given in Maurin, Taillet Combet is false!!!!)
// q_r_z = r * (rho_c * rho_c + 2.*TUsineMath::TUMath::Sqr(TMath::Pi() * rho_c) / 3.*(eta - 1.) * TUsineMath::TUMath::Sqr(sin(TMath::Pi() * r_spher / r_c) / (TMath::Pi() * r_spher / r_c)))
//         * pow((1. + pow(TUsinePhysics::Rsol_kpc() / ScaleRadius, Alpha)) / (1. + pow(r_spher / ScaleRadius, Alpha)), 2.*(Beta - g) / Alpha);



   // Prints in file '$USINE/qiz_id.dat' values of fQiz for fNi first coeffs.
   string file = "$USINE/inputs/" + FormFileNameBase(false) + ".dat";
   fFileName = file;
   string indent = TUMessages::Indent(true);
   TUMessages::Indent(false);
   fprintf(f_log, "%s - Qiz coeffs for spatial distribution %s saved in:", indent.c_str(), GetTemplateName().c_str());
   fprintf(f_log, "%s   %s\n\n", indent.c_str(), file.c_str());
   file = TUMisc::GetPath(file);
   FILE *fp = fopen(file.c_str(), "w");
   PrintQiz(fp);
   fclose(fp);
}

//______________________________________________________________________________
string TUBesselQiSrc::FormFileNameBase(Bool_t is_qi_or_qiz) const
{
   //--- Returns string for file name to be used when writing fQi or fQiz.
   //  is_qi_or_qiz      Whether to return name for fQi coeffs or fQiz coeffs


   string name = fUID;
   string axisr = "";
   if (fAxisr) {
      if (fAxisr->GetMax() < 100.)
         axisr = Form("_Rmax%.3lf%s_Nr%d", fAxisr->GetMax(), fAxisr->GetUnit(false).c_str(), fAxisr->GetN());
      else
         axisr = Form("_Rmax%.3le%s_Nr%d", fAxisr->GetMax(), fAxisr->GetUnit(false).c_str(), fAxisr->GetN());
   }
   string axisz = "";
   if (fAxisz) {
      if (fAxisz->GetMax() < 100.)
         axisz = Form("_zmax%.3lf%s_Nz%d", fAxisz->GetMax(), fAxisz->GetUnit(false).c_str(), fAxisz->GetN());
      else
         axisz = Form("_zmax%.3le%s_Nz%d", fAxisz->GetMax(), fAxisz->GetUnit(false).c_str(), fAxisz->GetN());
   }

   if (is_qi_or_qiz)
      name = "qi_" + name + axisr;
   else
      name = "qiz_" + name + axisr + axisz;

   return name;
}

//______________________________________________________________________________
Int_t TUBesselQiSrc::IndexClostestz(Double_t z, Bool_t is_warning) const
{
   //--- Returns node closest to z from fAxisz content.
   //  z                 Value to search for
   //  is_warning        If true, print message if z[node] far away from z

   if (!fAxisz)
      TUMessages::Error(stdout, "TUBesselQiSrc", "IndexClostestz", "fAxisz is NULL, set it first!");

   z = fabs(z);
   Int_t node = fAxisz->IndexClosest(z);
   if (node < fAxisz->GetN() - 1 && (fabs(z - fAxisz->GetVal(node + 1)) < fabs(z - fAxisz->GetVal(node))))
      node += 1;

   if (is_warning && (fabs(z) > 0 && fabs(z - fAxisz->GetVal(node)) / z > 0.1)) {
      string message = Form("You asked for z=%.3le, but the closest value available is %.3le, increase N in fAxisz to better match this value",
                            z, fAxisz->GetVal(node));
      TUMessages::Error(stdout, "TUBesselQiSrc", "IndexClostestz", message);
   }
   return node;
}

//______________________________________________________________________________
void TUBesselQiSrc::Initialise(Bool_t is_delete)
{
   //--- Initialises class members.
   //  is_delete         Whether to delete or only initialise

   if (is_delete) {
      ResetAxisr();
      ResetAxisz();
      ResetQi();
      ResetQiz();
   }

   fAxisr = NULL;
   fAxisz = NULL;
   fFileName = "";
   fIsUseAxisr = false;
   fIsUseAxisz = false;
   fIsUseQi = false;
   fIsUseQiz = false;
   fNi = 0;
   fQi = NULL;
   fQiz = NULL;
   fTemplateName = "";
   fUID = "NoFreePars";
}


//______________________________________________________________________________
void TUBesselQiSrc::PrintQi(FILE *f) const
{
   //--- Prints in file f values of fQi for fNi first coeffs as well as information
   //    relevant for its calculation (grid fAxisr used to perform calculation, etc.).
   //  f                 File in which to print

   if (!fQi)
      TUMessages::Warning(f, "TUBesselQiSrc", "PrintQi", "fQi is empty, nothing to write!");

   // Print header in file
   fprintf(f, "# Fourier-Bessel coefficients Qi along radial direction\n");
   fprintf(f, "# N.B.: USINE-generated file %s\n", TUMisc::GetDatime().c_str());
   fprintf(f, "#\n");

   // Print Qi values and relevant infos in file
   fprintf(f, "Template: %s\n", GetTemplateName().c_str());
   fprintf(f, "UID: %s\n", UID().c_str());
   fprintf(f, "Rmax[kpc]: %lf\n", fAxisr->GetMax());
   fprintf(f, "Nr: %d\n", fAxisr->GetN());
   fprintf(f, "Ni: %d\n", fNi);

   if (fQi) {
      for (Int_t i = 0; i < fNi; ++i)
         fprintf(f, "%+le\n", fQi[i]);
   } else {
      string message = "fQi is not allocated for " + fUID;
      TUMessages::Error(f, "TUBesselQiSrc", "PrintQi", message);
   }
}

//______________________________________________________________________________
void TUBesselQiSrc::PrintQiz(FILE *f) const
{
   //--- Prints in file f values of fQiz for fNi first coeffs, also writes infos
   //    relevant for its calculation (grid fAxisr used to perform calculation).
   //  f                 File in which to print

   if (!fQiz)
      TUMessages::Warning(f, "TUBesselQiSrc", "PrintQiz", "fQiz is empty, nothing to write!");

   // Print header in file
   fprintf(f, "# Fourier-Bessel coefficients Qiz along radial direction\n");
   fprintf(f, "# N.B.: USINE-generated file %s\n", TUMisc::GetDatime().c_str());
   fprintf(f, "#\n");

   // Print Qi values and relevant infos in file
   fprintf(f, "Template: %s\n", GetTemplateName().c_str());
   fprintf(f, "UID: %s\n", UID().c_str());
   fprintf(f, "Rmax[kpc]: %lf\n", fAxisr->GetMax());
   fprintf(f, "Nr: %d\n", fAxisr->GetN());
   fprintf(f, "z[kpc]: %lf\n", fAxisz->GetMax());
   fprintf(f, "Nz: %d\n", fAxisz->GetN());
   fprintf(f, "Ni: %d\n", fNi);

   if (fQiz) {
      for (Int_t i = 0; i < fNi; ++i) {
         for (Int_t i_z = 0; i_z < fAxisz->GetN(); ++i_z)
            fprintf(f, "%+le  ", fQiz[i_z * fNi + i]);
         fprintf(f, "\n");
      }
   } else {
      string message = "fQiz is not allocated for " + fUID;
      TUMessages::Error(f, "TUBesselQiSrc", "PrintQiz", message);
   }
}

//______________________________________________________________________________
void TUBesselQiSrc::ReadQi(string const &f_name, Int_t n_qi, Bool_t is_verbose, FILE *f_log)
{
   //--- Fills fQi from 'f_name'.
   //  f_name            Path to file
   //  n_qi              Fill the first n_qi (abort if n_qi > #Qi in file)
   //  is_verbose        If true print content of file read
   //  f_log             Log for USINE

   Initialise(true);

   // Find if $USINE, if yes, we replace it in file
   string file = f_name;
   TUMisc::SubstituteEnvInPath(file, "$USINE");

   // Print info
   string indent = TUMessages::Indent(true);
   TUMessages::Indent(false);
   fprintf(f_log, "%s - Fill fQi coefficients from: \n", indent.c_str());
   fprintf(f_log, "%s   %s\n", indent.c_str(), file.c_str());

   // If file does not exist, abort
   ifstream f_read(f_name.c_str(), std::ifstream::in);
   TUMisc::IsFileExist(f_read, f_name);
   fFileName = file;

   // Read file
   string line;
   Double_t r = 0.;
   Int_t nr = 0;
   vector<Double_t> qi;
   Int_t n_header = 0;
   while (getline(f_read, line)) {
      // if it is a comment (start with #) or a blanck line, skip it

      string line_trimmed = TUMisc::RemoveBlancksFromStartStop(line);
      if (line_trimmed[0] == '#' || line_trimmed.empty()) continue;
      // Extract from line
      vector<string> params;
      TUMisc::String2List(line, " \t", params);

      // Header parameters
      if (params.size() == 2) {
         if (params[0] == "z[kpc]:" || params[0] == "Nz:") {
            string message = "You are trying to fill Qi from Qiz values";
            TUMessages::Error(f_log, "TUBesselQiSrc", "ReadQi", message);
         }
         if (params[0] == "Template:") {
            fTemplateName = params[1];
            ++ n_header;
         } else if (params[0] == "UID:") {
            fUID = params[1];
            ++ n_header;
         } else if (params[0] == "Rmax[kpc]:") {
            r = atof(params[1].c_str());
            ++ n_header;
         } else if (params[0] == "Nr:") {
            nr = atoi(params[1].c_str());
            ++ n_header;
         } else if (params[0] == "Ni:") {
            fNi = atoi(params[1].c_str());
            if (n_qi > fNi) {
               string tmp = Form("Looking for %d fQi, but only %d in file", n_qi, fNi);
               TUMessages::Error(f_log, "TUBesselQiSrc", "ReadQi", (string)tmp);
            }
            ++n_header;
         }
         if (is_verbose)
            fprintf(f_log, "%s %s\n", params[0].c_str(), params[1].c_str());
         continue;
      }
      if (n_header < 5) {
         string message = (string)"Wrongly formatted file: \"Template:\", \"UID\","
                          + (string)" \"Rmax[kpc]:\", \"Nr:\", or \"Ni\" is missing";
         TUMessages::Error(f_log, "TUBesselQiSrc", "ReadQi", message);
      } else if (n_header > 5) {
         string message = "Wrongly formatted file: more than 5 entries in header";
         TUMessages::Error(f_log, "TUBesselQiSrc", "ReadQi", message);
      }

      // Qi values
      qi.push_back(atof(line_trimmed.c_str()));
   };

   SetAxisr(r, nr);
   fQi = new Double_t[fNi];
   fIsUseQi = false;
   for (Int_t i = 0; i < fNi; ++i) {
      fQi[i] = qi[i];
      if (is_verbose)
         fprintf(f_log, "%+le\n", fQi[i]);
   }
}

//______________________________________________________________________________
void TUBesselQiSrc::ReadQiz(string const &f_name, Int_t n_qi, Bool_t is_verbose, FILE *f_log)
{
   //--- Fills fQiz from 'f_name'.
   //  f_name            Path to file
   //  n_qi              Fill the first n_qi (abort if n_qi > #Qi in file)
   //  is_verbose        If true print content of file read
   //  f_log             Log for USINE

   Initialise(true);

   // Find if $USINE, if yes, we replace it in file
   string file = f_name;
   TUMisc::SubstituteEnvInPath(file, "$USINE");

   // Print info
   string indent = TUMessages::Indent(true);
   TUMessages::Indent(false);
   fprintf(f_log, "%s - Fill fQiz coefficients from: \n", indent.c_str());
   fprintf(f_log, "%s   %s\n", indent.c_str(), file.c_str());

   // If file does not exist, abort
   ifstream f_read(f_name.c_str());
   TUMisc::IsFileExist(f_read, f_name);
   fFileName = file;

   // Read file
   string line;
   Double_t r = 0.;
   Int_t nr = 0;
   Double_t z = 0.;
   Int_t nz = 0;
   vector<string> qiz;
   Int_t n_header = 0;
   while (getline(f_read, line)) {
      // if it is a comment (start with #) or a blanck line, skip it
      string line_trimmed = TUMisc::RemoveBlancksFromStartStop(line);
      if (line_trimmed[0] == '#' || line_trimmed.empty()) continue;

      // Extract from line
      vector<string> params;
      TUMisc::String2List(line, " \t", params);

      // Header parameters
      if (params.size() == 2) {
         if (params[0] == "Template:") {
            fTemplateName = params[1];
            ++ n_header;
         } else if (params[0] == "UID:") {
            fUID = params[1];
            ++ n_header;
         } else if (params[0] == "Rmax[kpc]:") {
            r = atof(params[1].c_str());
            ++ n_header;
         } else if (params[0] == "Nr:") {
            nr = atoi(params[1].c_str());
            ++ n_header;
         } else if (params[0] == "z[kpc]:") {
            z = atof(params[1].c_str());
            ++ n_header;
         } else if (params[0] == "Nz:") {
            nz = atoi(params[1].c_str());
            ++ n_header;
         } else if (params[0] == "Ni:") {
            fNi = atoi(params[1].c_str());
            if (n_qi > fNi) {
               string tmp = Form("Looking for %d Qiz, but only %d in file", n_qi, fNi);
               TUMessages::Error(f_log, "TUBesselQiSrc", "ReadQiz", (string)tmp);
            }
            ++n_header;
         }
         if (is_verbose)
            fprintf(f_log, "%s %s\n", params[0].c_str(), params[1].c_str());
         continue;
      }
      if (n_header < 7) {
         string message = (string)"Wrongly formatted file: \"Template:\", \"UID\","
                          + (string)" \"Rmax[kpc]:\", \"Nr:\", \"z[kpc]:\", \"Nz:\", or \"Ni\" is missing";
         TUMessages::Error(f_log, "TUBesselQiSrc", "ReadQzi", message);
      } else if (n_header > 7) {
         string message = "Wrongly formatted file: more than 7 entries in header";
         TUMessages::Error(f_log, "TUBesselQiSrc", "ReadQiz", message);
      }

      // Qi values
      qiz.push_back(line);
   };

   SetAxisr(r, nr);
   SetAxisz(z, nz);
   fQiz = new Double_t[fNi * nz];
   fIsUseQiz = false;
   for (Int_t i = 0; i < fNi; ++i) {
      vector<string> tmp;
      TUMisc::String2List(qiz[i], " \t", tmp);
      if (nz != (Int_t)tmp.size()) {
         string message = "Number of values per line in file differ from nz value in header";
         TUMessages::Error(f_log, "TUBesselQiSrc", "ReadQiz", message);
      }
      for (Int_t i_z = 0; i_z < nz; ++i_z) {
         fQiz[i_z * fNi + i] = atof(tmp[i_z].c_str());
         if (is_verbose)
            fprintf(f_log, "%+le ", fQiz[i_z * fNi + i]);
      }
      if (is_verbose)
         fprintf(f_log, "\n");
   }
}

//______________________________________________________________________________
void TUBesselQiSrc::ResetAxisr()
{
   //--- Resets fAxisr (delete or set to NULL).

   if (fAxisr && !fIsUseAxisr)
      delete fAxisr;

   fAxisr = NULL;
   fIsUseAxisr = false;
}

//______________________________________________________________________________
void TUBesselQiSrc::ResetAxisz()
{
   //--- Resets fAxisz (delete or set to NULL).

   if (fAxisz && !fIsUseAxisz)
      delete fAxisz;

   fAxisz = NULL;
   fIsUseAxisz = false;
}

//______________________________________________________________________________
void TUBesselQiSrc::ResetQi()
{
   //--- Resets fQi (delete or set to NULL).

   if (fQi && !fIsUseQi)
      delete[] fQi;

   fQi = NULL;
   fIsUseQi = false;
}

//______________________________________________________________________________
void TUBesselQiSrc::ResetQiz()
{
   //--- Resets fQiz (delete or set to NULL).

   if (fQiz && !fIsUseQiz)
      delete[] fQiz;

   fQiz = NULL;
   fIsUseQiz = false;
}

//______________________________________________________________________________
void TUBesselQiSrc::TEST(TUInitParList *init_pars, FILE *f)
{
   //--- Test function for this class (called in usine -t).
   //  init_pars          TUInitParList class of initialisation parameters
   //  f                  Output file for test

   string message = "Calculate Fourier-Bessel coeff. for various sources.";
   TUMessages::Test(f, "TUBesselQiSrc", message);

   fprintf(f, "N.B.: increase number of r bins to improve precision on Qi (large #bins required)!\n");

   // Set class and test methods
   fprintf(f, " * Load Src templates, Bessels, and other quantities:\n");
   Double_t rmax_kpc = 20.;
   Int_t n_integr = 5000;
   fprintf(f, "   > SetAxisr(rmax_kpc=%le, n_integr=%d);\n", rmax_kpc, n_integr);
   SetAxisr(rmax_kpc, n_integr);
   Double_t zmax_kpc = 6.;
   Int_t n_integrz = 7;
   fprintf(f, "   > SetAxisz(zmax_kpc=%le, n_integr=%d);\n", zmax_kpc, n_integrz);
   SetAxisz(zmax_kpc, n_integrz);
   fprintf(f, "   > TUSrcTemplates templ; templ.SetClass(init_pars=\"%s\", is_verbose=false, f_log=f);\n", init_pars->GetFileNames().c_str());
   TUSrcTemplates templ;
   templ.SetClass(init_pars, false, f);
   fprintf(f, "\n");

   Double_t z = 3.;
   Int_t i_z = IndexClostestz(z);
   fprintf(f, "   > Int_t i_z = IndexClostestz(z=%lf) = %d   => Getz(%d)=%lf;\n", z, i_z, i_z, Getz(i_z));
   fprintf(f, "\n");
   fprintf(f, "\n");

   Int_t n_bessel = 100;
   fprintf(f, "   > TUBesselJ0 bessel; bessel.SetZerosAndJ1Zeros(n_bessel=%d);\n", n_bessel);
   TUBesselJ0 bessel;
   bessel.SetZerosAndJ1Zeros(n_bessel);
   fprintf(f, "\n");
   fprintf(f, "\n");

   fprintf(f, " * Loop on several q(r) distributions (in templates) and calculate Qi:\n");
   fprintf(f, "   > CalculateAndStoreQi(&bessel, spat_dist, is_verbose=true, f_log=f);\n");
   const Int_t n_testr = 6;
   string dist_qr[n_testr] = {"STEPUP", "STEPDOWN", "DOOR", "SNRMODIFIED", "SNRMODIFIED", "CST"};
   for (Int_t i = 0; i < n_testr; ++i) {
      Int_t i_templ = templ.IndexSrcTemplate(dist_qr[i], false);
      if (i_templ < 0) {
         fprintf(f, " Spatial-distribution name %s not found in templates\n", dist_qr[i].c_str());
         continue;
      }
      TUValsTXYZEVirtual *spat_dist = templ.GetSrc(i_templ);
      TUFreeParList *pars = spat_dist->GetFreePars();
      // Set values for parameters
      if (dist_qr[i] == "STEPUP" || dist_qr[i] == "STEPDOWN" || dist_qr[i] == "DOOR") {
         pars->SetParVal(0, 5.);
         if (dist_qr[i] == "DOOR")
            pars->SetParVal(1, 12.);
      } else if (dist_qr[i] == "SNRMODIFIED") {
         // 4 free parameters: rsol_snr[kpc], a_snr [-], b_snr [-], dex_snr [dex/kpc]
         //   - Stecker & Jones (1977): 10., 0.6, -3., 0.
         //   - Case & Bhattacharya (A&AS 120C [1996], ApJ 504, 761 [1998]): 8.5, 2., -3.53, 0.
         //   - Strong & Moskalenko (ApJ 509 [1999]): 8.5, 0.5, -1., 0.
         //   N.B: metalicity Gradient (see Maurin, Cass� & Vangioni-Flam 2003): -0.05
         pars->SetParVal(0, 8.5);
         pars->SetParVal(1, 2.);
         pars->SetParVal(2, 3.53);
         if (i == 4)
            pars->SetParVal(3, 0.);
         else
            pars->SetParVal(3, 0.05);
      }
      spat_dist->PrintSummary(f, spat_dist->UID());
      pars->PrintPars(f);
      CalculateAndStoreQi(&bessel, spat_dist, true, f);
      spat_dist = NULL;
      pars = NULL;
   }

   Int_t n_qi = 100;
   fprintf(f, "   > ReadQi(f_name, n_qi=%d, is_verbose=true, f_log=f);\n", n_qi);
   ReadQi(TUMisc::GetPath(GetFileName()).c_str(), n_qi, true, f);
   fprintf(f, "\n");

   fprintf(f, "   > And resummation:\n");
   Int_t n_resum1 = 9;
   Int_t n_resum2 = 21;
   fprintf(f, "   r       %dcoefs   %dcoef  %dcoef+Bringmann\n", n_resum1, n_resum2, n_resum2);
   for (Int_t i_r = 0; i_r < GetNr(); ++i_r) {
      if (i_r != 0 && i_r != GetNr() - 2) {
         Int_t mult = GetNr() / 20;
         if (mult != 0 && i_r % mult != 0)
            continue;
      }
      Double_t r = fAxisr->GetVal(i_r);
      Double_t rho = r / fAxisr->GetMax();
      fprintf(f, "%5.2f   %+.4le   %+.4le   %+.4le\n", r,
              bessel.Coeffs2Func(rho, fQi, n_resum1, false),
              bessel.Coeffs2Func(rho, fQi, n_resum2, false),
              bessel.Coeffs2Func(rho, fQi, n_resum2, true));
   }
   fprintf(f, "\n");
   fprintf(f, "\n");


   fprintf(f, " * Loop on several q(r,z) distributions (in templates) and calculate Qiz:\n");
   fprintf(f, "   > CalculateAndStoreQiz(&bessel, spat_dist, is_verbose=true, f_log=f);\n");
   SetAxisz(zmax_kpc, n_integrz);
   const Int_t n_testrz = 1;
   string dist_qrz[n_testrz] = {"EINASTO_CYL"};
   for (Int_t i = 0; i < n_testrz; ++i) {
      Int_t i_templ = templ.IndexSrcTemplate(dist_qrz[i], false);
      if (i_templ < 0) {
         fprintf(f, " Spatial-distribution name %s not found in templates\n",  dist_qrz[i].c_str());
         continue;
      }
      TUValsTXYZEVirtual *spat_dist = templ.GetSrc(i_templ);
      TUFreeParList *pars = spat_dist->GetFreePars();
      // Set values for parameters; 3 free parameters: rhos[Msol/kpc3], rs [kpc], alpha [-]
      //   - Springel et al. (2008): xxx, 21.7, 0.17
      pars->SetParVal(0, 1.);
      pars->SetParVal(1, 21.7);
      pars->SetParVal(2, 0.17);
      spat_dist->PrintSummary(f, spat_dist->UID());
      pars->PrintPars(f);
      CalculateAndStoreQiz(&bessel, spat_dist, true, f);
      spat_dist = NULL;
      pars = NULL;
   }
   fprintf(f, "   > ReadQiz(f_name, n_qi=%d, is_verbose=true, f_log=f);\n", n_qi);
   ReadQiz(TUMisc::GetPath(GetFileName()).c_str(), n_qi, true, f);

   fprintf(f, "   > And resummation:\n");
   fprintf(f, "   r,z=0   %dcoefs      %dcoefs  %dcoeff+Bringmann\n", n_resum1, n_resum2, n_resum2);
   for (Int_t i_r = 0; i_r < GetNr(); ++i_r) {
      if (i_r != 0 && i_r != GetNr() - 2) {
         Int_t mult = GetNr() / 20;
         if (mult != 0 && i_r % mult != 0)
            continue;
      }
      Double_t r = fAxisr->GetVal(i_r);
      Double_t rho = r / fAxisr->GetMax();
      fprintf(f, "%5.2f   %+.4le  %+.4le  %+.4le\n", r,
              bessel.Coeffs2Func(rho, &fQiz[0], n_resum1, false),
              bessel.Coeffs2Func(rho, &fQiz[0], n_resum2, false),
              bessel.Coeffs2Func(rho, &fQiz[0], n_resum2, true));
   }
   fprintf(f, "\n\n");
}
