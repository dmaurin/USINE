// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

// C/C++ include
#include <cmath>
#include <fstream>
// ROOT include
// USINE include
#include "../include/TUAtomProperties.h"
#include "../include/TUMessages.h"
#include "../include/TUMisc.h"
#include "../include/TUPhysics.h"

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TUAtomProperties                                                     //
//                                                                      //
// Atomic properties for elements (inherits from TUAtomElements).       //
//                                                                      //
// The class TUAtomProperties reads/stores atomic properties            //
// for all elements in TUAtomElements:                                  //
//    - first ionisation potential (FIP);
//    - volatility of the elements;
//    - ionisation energy of the Kshell (for e-capture cross-section);
//    - ...
//                                                                      //
// N.B.: to account for other atomic properties, add the new property   //
// as another data member and modify the SetClass and Print methods     //
// (and don't forget to add the Get methods to access the data member). //
//                                                                      //
// Here are some examples of the properties stored:                     //
//    - top panel: FIP vs Z;
//    - middle panel: FIP bias vs FIP value;
//    - bottom panel: volatility vs FIP value.
// BEGIN_HTML
// <b>Example of file:</b> <a href="../inputs/atomic_properties.dat" target="_blank">$USINE/inputs/atomic_properties.dat</a>.
// END_HTML
//////////////////////////////////////////////////////////////////////////

ClassImp(TUAtomProperties)

//______________________________________________________________________________
TUAtomProperties::TUAtomProperties() : TUAtomElements()
{
   // ****** Default constructor ******
   // (initialises all atomic properties to -1)

   Initialise();
}

//______________________________________________________________________________
TUAtomProperties::TUAtomProperties(TUAtomProperties const &atom_prop) : TUAtomElements()
{
   // ****** Copy constructor ******
   //  atom_prop         Object to copy

   Copy(atom_prop);
}

//______________________________________________________________________________
TUAtomProperties::TUAtomProperties(TUInitParList *init_pars, Bool_t is_verbose, FILE *f_log) : TUAtomElements()
{
   // ****** recommended constructor ******
   //  init_pars         TUInitParList class of initialisation parameters
   //  is_verbose        Verbose or not when class is set
   //  f_log             Log for USINE

   // Fills class members (atomic properties) from user-selected file in init_pars
   Initialise();
   SetClass(init_pars, is_verbose, f_log);
}

//______________________________________________________________________________
TUAtomProperties::~TUAtomProperties()
{
   // ****** Default destructor ******
}

//______________________________________________________________________________
void TUAtomProperties::Copy(TUAtomProperties const &atom_prop)
{
   //--- Copies atom_prop in current class.
   //  atom_prop         Object to copy from

   Initialise();
   fFileName = atom_prop.GetFileName();
   for (Int_t i = 0; i < GetNElements(); ++i) {
      Int_t z = i + 1;
      fAMeanEarthWeighted[i] =  atom_prop.GetAMeanEarthWeighted(z);
      fEionKshell[i] =  atom_prop.GetAMeanEarthWeighted(z);
      fFIP[i] =  atom_prop.GetFIP(z);
      fIMean[i] =  atom_prop.GetIMean(z);
      fTcVolatility[i] =  atom_prop.GetTcVolatility(z);
   }
}

//______________________________________________________________________________
Double_t TUAtomProperties::FIPBias(Double_t const &fip)
{
   //--- Returns the FIP bias (FIP).
   //  fip               First Ionisation Potential value

   // N.B.: formulae taken from Binns et al, ApJ 346, 997 (1989)
   // Feel free to change it with your own parametrization
   if (fip < 7.) return 1.;
   if (fip > 13.6) return 0.168;
   else return (exp(-0.27 * (fip - 7.)));
}

//______________________________________________________________________________
Double_t TUAtomProperties::FIPBias(Int_t z) const
{
   //--- Returns FIP bias(z).
   //  z                 Charge

   z = abs(z);
   TUAtomElements::CheckZ(z);
   if (z != 0) {
      if (GetFIP(z) < 0) {
         string message = "Atomic properties (FIP) not filled!";
         TUMessages::Error(stdout, "TUAtomProperties", "FIPBias", message);
      }
      return FIPBias(GetFIP(z));
   } else return 1.;
}

//______________________________________________________________________________
void TUAtomProperties::Initialise(void)
{
   //--- Initialises (all atomic properties set to -1).

   Int_t n = GetNElements();
   fAMeanEarthWeighted.assign(n, -1.);
   fEionKshell.assign(n, -1.);
   fFileName = "";
   fFIP.assign(n, -1.);
   fIMean.assign(n, -1.);
   fTcVolatility.assign(n, -1.);
}

//______________________________________________________________________________
void TUAtomProperties::Print(FILE *f, Int_t z_min, Int_t z_max) const
{
   //--- Prints in f the atomic properties of elements for the range [z_min,z_max].
   //  f                 Output file
   //  z_min             Minimal charge to print
   //  z_max             Maximal charge to print

   TUMessages::Separator(f, "Atomic Properties");
   if (z_min > z_max) swap(z_min, z_max);
   if (z_min <= 0) z_min = 1;
   if (z_max > TUPhysics::Zmax()) z_max = TUPhysics::Zmax();

   fprintf(f, " Z  name   WeightIsotM   FIP    50%%Tc      K       <I>\n");
   for (Int_t i = z_min - 1; i < z_max; ++i) {
      string atom = TUAtomElements::ZToElementName(i + 1);
      string::size_type last_pos = atom.find_first_not_of("0123456789", 0);
      TUMisc::LowerCase(atom[last_pos + 1]);
      fprintf(f, "%3d  %2s      %6.2f   %6.2f   %6.1f   %6.2f   %6.2f\n",
              i + 1, atom.c_str(), fAMeanEarthWeighted[i],
              fFIP[i], fTcVolatility[i], fEionKshell[i], fIMean[i]);
   }
}

//______________________________________________________________________________
void TUAtomProperties::SetClass(TUInitParList *init_pars, Bool_t is_verbose, FILE *f_log)
{
   //--- Fills atomic properties from the content of init_pars.
   //  init_pars         TUInitParList class of initialisation parameters
   //  is_verbose        Verbose or not when class is set
   //  f_log             Log for USINE


   const Int_t gg = 3;
   string gr_sub_name[gg] = {"Base", "ListOfCRs", "fAtomicProperties"};
   string indent = TUMessages::Indent(true);
   if (is_verbose) {
      fprintf(f_log, "%s[TUAtomProperties::SetClass]\n", indent.c_str());
      fprintf(f_log, "%s### Set atomic properties [%s#%s#%s]\n",
              indent.c_str(), gr_sub_name[0].c_str(), gr_sub_name[1].c_str(), gr_sub_name[2].c_str());
   }
   Int_t i = init_pars->IndexPar(gr_sub_name);
   string f_atomprop = TUMisc::GetPath(init_pars->GetParEntry(i).GetVal());

   if (is_verbose)
      fprintf(f_log, "%s   - Load from %s\n", indent.c_str(), init_pars->GetParEntry(i).GetVal().c_str());
   SetClass(f_atomprop, f_log);
   if (is_verbose)
      fprintf(f_log, "%s[TUAtomProperties::SetClass] <DONE>\n\n", indent.c_str());
   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUAtomProperties::SetClass(string const &f_atomprop, FILE *f_log)
{
   //--- Fills atomic properties read from file (if not found, warning message).
   //  f_atomprop        File of atomic properties
   //  f_log             Log for USINE

   // If input file exist, read it (else abort)
   ifstream f_read(f_atomprop.c_str());
   TUMisc::IsFileExist(f_read, f_atomprop.c_str());

   fFileName = f_atomprop;

   // Read file
   string name;
   string tmp_line;
   Int_t n_lines = 0;
   while (getline(f_read, tmp_line)) {
      // if it is a comment (start with #) or a blanck line, skip it
      string line = TUMisc::RemoveBlancksFromStartStop(tmp_line);
      if (line[0] == '#' || line.empty()) continue;

      // Set number of columns in file
      Int_t n_cols_to_read = 8;
      vector<string> params;
      TUMisc::String2List(line, " \t", params, n_cols_to_read);
      if (Int_t(params.size()) < n_cols_to_read) {
         string message = Form("line %d in %s: wrong format!", n_lines, f_atomprop.c_str());
         TUMessages::Error(f_log, "TUAtomProperties", "SetClass", message);
      }

      // Read columns and fill class members
      // # Col.1  -  Z
      // # Col.2  -  Element name
      // # Col.3  -  Weighted isotopic abundances [amu]
      // # Col.4  -  First Ionisation Potential [eV]
      // # Col.5  -  Volatility temperature [K]
      // # Col.6  -  Half-fraction volatility [K]
      // # Col.7  -  K-shell ionisation energy [keV]
      // # Col.8  -  Mean Ionisation potential [eV]
      // Check Z is correct (w.r.t. to what is given)
      Int_t z = atoi(params[0].c_str());
      name = params[1];
      if (name == "") break;
      TUMisc::UpperCase(name);
      // Check is this is a possible elements
      if ((ElementNameToZ(name) != z) || (ZToElementName(z) !=  name)) {
         string message = Form("in file %s, %s with Z=%d is an ill-formed element, it is discarded",
                               f_atomprop.c_str(), name.c_str(), z);
         TUMessages::Warning(f_log, "TUAtomProperties", "SetClass", message);
         continue;
      }

      // A given Z corresponds to the index i=(z-1) (inherited see TUAtomElements)
      // Fill atomic properties in the file
      Int_t i = z - 1;
      fAMeanEarthWeighted[i] =  atof(params[2].c_str());
      fEionKshell[i] = atof(params[6].c_str());
      fFIP[i] = atof(params[3].c_str());
      fIMean[i] = atof(params[7].c_str());
      fTcVolatility[i] = atof(params[5].c_str()); // Tc 50%
   };

   // Check that all the elements are filled
   for (Int_t i = 0; i < TUPhysics::Zmax(); ++i) {
      if (fAMeanEarthWeighted[i] == -1.) {
         string message = "no atomic properties found for " + name
                          + " in " + f_atomprop;
         TUMessages::Warning(f_log, "TUAtomProperties", "SetClass", message);
      }
   }
}

//______________________________________________________________________________
void TUAtomProperties::TEST(TUInitParList *init_pars, FILE *f)
{
   //--- Test function for this class (called in usine -t).
   //  init_pars         TUInitParList class of initialisation parameters
   //  f                 Output file for test

   string message =
      "Contains atomic properties for all elements 1<=Z<=109:\n"
      "   - first ionisation potential (FIP)\n"
      "   - volatility\n"
      "   - ionisation energy for the Kshell\n"
      "   -...\n"
      "N.B.: It is up to the user to add other properties by adding more\n"
      "properties as class members, and add the corresponding information\n"
      "in the read file";
   TUMessages::Test(f, "TUAtomProperties", message);

   // Set class and test methods
   fprintf(f, " * Fill class\n");
   fprintf(f, "   > SetClass(init_pars=\"%s\", is_verbose=false, f_log = f);\n", init_pars->GetFileNames().c_str());
   SetClass(init_pars, false, f);
   fprintf(f, "   > SetClass(init_pars=\"%s\", default=true, f_log= f);\n", init_pars->GetFileNames().c_str());
   SetClass(init_pars, true, f);

   fprintf(f, "\n");
   fprintf(f, " * Test all methods with Z as an argument\n");
   fprintf(f, "   > FIPBias(10);         => %le\n", FIPBias(10));
   fprintf(f, "   > FIPBias(11);         => %le\n", FIPBias(11));
   fprintf(f, "   > FIPBias(12);         => %le\n", FIPBias(12));
   fprintf(f, "   > GetFIP(12);          => %le\n", GetFIP(12));
   fprintf(f, "   > GetTcVolatility(12); => %le\n", GetTcVolatility(12));
   fprintf(f, "   > GetEionKshell(12);   => %le\n", GetEionKshell(12));
   fprintf(f, "   > Print(stdout, Zmin=4, Zmax=13)\n");
   Print(f, 4, 13);
   fprintf(f, "\n");

   // Copy
   fprintf(f, "\n");
   fprintf(f, " * Test Copy method:\n");
   fprintf(f, "   > TUAtomProperties atom_prop; atom_prop.Copy(*this);\n");
   TUAtomProperties atom_prop;
   atom_prop.Copy(*this);
   fprintf(f, "   > atom_prop.Print(stdout, Zmin=4, Zmax=13);\n");
   atom_prop.Print(f, 4, 13);
   fprintf(f, "\n");
}
