// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

// C/C++ include
// ROOT include
// USINE include
#include "../include/TUValsTXYZEFormula.h"
#include "../include/TUMessages.h"

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TUValsTXYZEFormula                                                   //
//                                                                      //
// Formula with free parameters for generic F(T,X,Y,Z,E-vars) function. //
//                                                                      //
// The class TUValsTXYZEFormula provides a formula-based description    //
// depending on space-time (txyz) and energy (beta,Rig...) coordinates, //
// using the fparser library http://warp.povusers.org/FunctionParser/.  //
// This external library has the following capabilities:                //
//    1. Constants in formula: "f:=x*x+cst" (variable 'x', constant 'cst').
//       -> SetFormula("f:=x*x+cst", "x",  TUFreeParList *constants)
//    2. Multi-level formulae: "x:=r*cos(theta);y:=r*sin(theta);z:=cst1;x*x+y*y+z*z"
//       -> main function 'z' depends on 'x' and 'y' functions (variable
//       'r' and 'theta', and user constant 'cst1'). Note that the scope
//        of y and z is valid only within the FunctionParser element.
//                                                                      //
// The class derives from the abstract class TUValsTXYZEVirtual() and   //
// TUFreeParList (which provides a list of user-defined parameters).    //
// These parameters are used as 'constant' in fparser (see above).      //
// Note that whenever one of the free parameter value is changed at     //
// run time (e.g., by a minimisation routine), a call to the method     //
// UpdateFromFreeParsAndResetStatus() is required to ensure that     //
// the so-called 'constant' values in fparser formula are updated to    //
// their new values.                                                    //
//                                                                      //
// The core methods of the class are:                                   //
//    - SetClass(): set formula (and its free pars) from init. file;
//    - SetOrUpdateFormula(): set from two strings (formula, parameters);
//    - EvalFormula(): value for given t,x,y,z,e values.
//                                                                      //
// BEGIN_HTML
// <b>URL for FunctionParser:</b> <a href="http://warp.povusers.org/FunctionParser/" target="_blank">warp.povusers.org/FunctionParser</a>
// END_HTML
//////////////////////////////////////////////////////////////////////////

ClassImp(TUValsTXYZEFormula)

//______________________________________________________________________________
TUValsTXYZEFormula::TUValsTXYZEFormula() : TUValsTXYZEVirtual()
{
   // ****** normal constructor ******

   Initialise(false);
}

//______________________________________________________________________________
TUValsTXYZEFormula::TUValsTXYZEFormula(TUValsTXYZEFormula const &vals) : TUValsTXYZEVirtual()
{
   // ****** Copy constructor ******
   //  vals              Object to copy


   Initialise(false);
   Copy(vals);
}

//______________________________________________________________________________
TUValsTXYZEFormula::~TUValsTXYZEFormula()
{
   // ****** Default destructor ******

   Initialise(true);
}

//______________________________________________________________________________
void TUValsTXYZEFormula::Copy(TUValsTXYZEFormula const &vals)
{
   //--- Copies to current class.
   //  vals              Object to copy from

   // Free memory if necessary
   Initialise(true);

   // Set fFormula
   SetFormula(vals.GetFormulaString(), vals.GetFormulaVars(), vals.GetFreePars(), false);

   fName = vals.GetName();
   fNVar = vals.GetNVar();
}

//______________________________________________________________________________
Double_t TUValsTXYZEFormula::EvalFormula(Double_t *vals_txyz, Double_t *edep_vals)
{
   //--- Returns formula value 'vals_txyz' and 'edep_vals' variables. Note that
   //    in fFormula, space-times coordinates are always given before E-dep ones.
   //  bins_txyz         Values of space-time coordinates
   //  edep_vars         Values of E-dependent variables

   // Update formula if required, and then set status to updated
   UpdateFromFreeParsAndResetStatus(false);

   // Create single array pointing to formula variable
   // N.B.: 4 space-time + (fNVar-4) energy
   vector<Double_t> all_vars(max(4, fNVar), 0.);
   for (Int_t i = 0; i < 4; ++i)
      all_vars[i] = vals_txyz[i];

   for (Int_t i = 4; i < fNVar; ++i)
      all_vars[i] = edep_vals[i - 4];

   return fFormula.Eval(&all_vars[0]);
}

//______________________________________________________________________________
void TUValsTXYZEFormula::Initialise(Bool_t is_delete)
{
   //--- Deletes/Initialises members.
   //  is_delete         Unused here but required (virtual function member)

   if (is_delete && fFreePars && fIsDeleteFreePars)
      delete fFreePars;

   fFormulaVars = "";
   fFormulaString = "";
   fFreePars = NULL;
   fIsDeleteFreePars = true;
}

//______________________________________________________________________________
void TUValsTXYZEFormula::PrintFormula(FILE *f) const
{
   //--- Prints formula summary.
   //  f                 File in which to print

   string indent = TUMessages::Indent(true);
   string vars = fFormulaVars;
   string names = "";
   if (fFreePars)
      names = fFreePars->GetParNames();
   if (vars  == "") vars = "NONE";
   if (names == "") names = "NONE";
   fprintf(f, "%s    [formula] = %s\n", indent.c_str(), fFormulaString.c_str());
   fprintf(f, "%s    - base variables: %s\n", indent.c_str(), vars.c_str());
   fprintf(f, "%s    - user-defined variables: %s\n", indent.c_str(), names.c_str());
   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUValsTXYZEFormula::PrintSummary(FILE *f, string const &description) const
{
   //--- Prints class content summary.
   //  f                 File in which to print
   //  description       Single word

   string indent = TUMessages::Indent(true);
   if (description != "")
      fprintf(f, "%s    - %s\n", indent.c_str(), description.c_str());
   PrintFormula(f);
   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUValsTXYZEFormula::SetClass(string const &formula, Bool_t is_verbose, FILE *f_log,
                                  string const &commasep_vars, TUFreeParList *free_pars,
                                  Bool_t is_use_or_copy_freepars)
{
   //--- Updates this class (array of formula).
   //  formula                   Formula to implement (http://warp.povusers.org/FunctionParser/fparser.html)
   //  is_verbose                Verbose on or off
   //  f_log                     Log for USINE
   //  commasep_vars             List of comma-separated variables, e.g., "t,x,y,z" (default is no var)
   //  free_pars                 List of free parameters (used only if formula and free_pars!=NULL)
   //  is_use_or_copy_freepars   Whether the parameters passed must be added (copied) or just used (pointed at)

   // Clear class
   Initialise(true);
   SetFormula(formula, commasep_vars, free_pars, is_use_or_copy_freepars);

   if (is_verbose)
      PrintFormula(f_log);
}

//______________________________________________________________________________
void TUValsTXYZEFormula::SetOrUpdateFormula(string const &fparser_formula,
      string const &commasep_vars, TUFreeParList *formula_constants,
      Bool_t is_use_or_copy_freepars, Bool_t is_update_formula)
{
   //--- Sets fFormula from 'fparser_formula' and a list of parameters 'par_list'.
   //    N.B.: for possible formula and format, http://warp.povusers.org/FunctionParser/fparser.html.
   //  fparser_formula           Formula(t,x,y,z,par1,par2,par3...) with par1,...parN defined in par_names [case sensitive]
   //  commasep_vars             List of comma-separated variables, e.g., "t,x,y,z"
   //  formula_constants         List of parameter names added to formula (fFormula.AddConstant())
   //  is_use_or_copy_freepars   Whether the parameters passed must be added (copied) or just used (pointed at)
   //  is_update_formula         If true, means that fFormula only needs to be updated



   // If it is not an update (fill the formula for the first time)
   if (!is_update_formula) {
      Initialise(true);
      fFormulaString = fparser_formula;
      fFormulaVars = commasep_vars;
      if (formula_constants) {
         if (is_use_or_copy_freepars) {
            fFreePars = formula_constants;
            fIsDeleteFreePars = false;
         } else {
            fFreePars = new TUFreeParList(*formula_constants);
            fIsDeleteFreePars = true;
         }
      }
      // Extract number fNVar of variables in formula
      vector<string> vars;
      TUMisc::String2List(commasep_vars, ", ", vars);
      fNVar = (Int_t)vars.size();

      if (commasep_vars == "" || fparser_formula == "") {
         string message = "fparser_formula is empty and no variable, cannot set formula!";
         TUMessages::Error(stdout, "TUValsTXYZEFormula", "SetOrUpdateFormula", message);
      }
   }

   // Add constant for formula (user free parameters)
   FunctionParser formula;
   if (fFreePars) {
      for (Int_t i = 0; i < fFreePars->GetNPars(); ++i)
         formula.AddConstant(fFreePars->GetParEntry(i)->GetName(), fFreePars->GetParEntry(i)->GetVal(false));
   }
   fFormula = formula;

   // 'Parse' formula
   Int_t check = fFormula.Parse(fFormulaString, fFormulaVars);
   if (check != -1) {
      string error_loc = fparser_formula.substr(0, check)
                         + " !!! " + fparser_formula.substr(check);
      string message = "Error parsing formula " + fFormulaString
                       + " with variables " + fFormulaVars + " and constants "
                       + fFreePars->GetParNames() + "\n    =>  error here " + error_loc;
      TUMessages::Error(stdout, "TUValsTXYZEFormula", "SetFormula", message);
   }
}

//______________________________________________________________________________
void TUValsTXYZEFormula::TEST(FILE *f)
{
   //--- Test function for this class (called in usine -t).
   //  f                 Output file for test

   string message = "Container for space-time+E dependent formula";
   TUMessages::Test(f, "TUValsTXYZEFormula", message);

   fprintf(f, " * Set formula based on a 'Function Parser'-formatted string. In this example,\n"
           "     - we use a pure space-time function (variables are t, x, y, and z);\n"
           "     - free parameters of the function are 'par1' and 'par2'.\n"
           "  This is done in 3 steps: i) create free parameters; ii) fill formula; iii) use formula.\n\n");

   fprintf(f, "   > TUFreeParList *par_list = new TUFreeParList();\n");
   fprintf(f, "   > par_list->AddPar(par1,\"-\",-1);\n");
   fprintf(f, "   > par_list->AddPar(par2,\"-\",-2);\n");
   TUFreeParList *par_list = new  TUFreeParList();
   par_list->AddPar("par1", "-", -1);
   par_list->AddPar("par2", "-", -1);
   fprintf(f, "   > par_list->PrintPars(f);\n");
   par_list->PrintPars(f);
   fprintf(f, "\n");

   fprintf(f, " * Check TXYZ formula\n");
   fprintf(f, "   > SetFormula(formula=\"x1:=par1*cos(x);x2:=par2*sin(y);x1*x1+y1*y1\", var=\"t,x,y,z\", free_par=par_list, is_use_or_copy_freepars=true);\n");
   SetFormula("x1:=par1*cos(x);y1:=par2*sin(y);x1*x1+y1*y1", "t,x,y,z", par_list, true);
   fprintf(f, "   > ValsTXYZEFormat();  =>  %d\n", ValsTXYZEFormat());
   fprintf(f, "   > PrintFormula(f);\n");
   PrintFormula(f);
   fprintf(f, "   > GetFreePars()->PrintPars(f);\n");
   GetFreePars()->PrintPars(f);
   fprintf(f, "   > GetFreePars()->GetParListStatus() = %d;\n", GetFreePars()->GetParListStatus());
   fprintf(f, "   > par_list->GetParListStatus() = %d;\n", par_list->GetParListStatus());

   const Int_t n = 4;
   Double_t vals_txyz[n] = {0, 1, 1, 0};
   fprintf(f, "   > EvalFormulaTXYZ(vals_txyz={0,1,1,0});    with (par1=-1,par2=-2) =>  %le\n", EvalFormulaTXYZ(vals_txyz));
   fprintf(f, "   > GetFreePars()->GetParListStatus() = %d;\n", GetFreePars()->GetParListStatus());
   fprintf(f, "   > par_list->GetParListStatus() = %d;\n", par_list->GetParListStatus());
   vals_txyz[1] = 2;
   fprintf(f, "   > EvalFormulaTXYZ(vals_txyz={0,2,1,0});    with (par1=-1,par2=-2) =>  %le\n", EvalFormulaTXYZ(vals_txyz));
   fprintf(f, "   > GetFreePars()->GetParListStatus() = %d;\n", GetFreePars()->GetParListStatus());
   fprintf(f, "   > par_list->GetParListStatus() = %d;\n", par_list->GetParListStatus());
   Bool_t is_same = ((par_list == GetFreePars()) ? 1 : 0);
   fprintf(f, " N.B.: par_list and GetFreePars() point to the same address: comparison of addresses = %d\n", is_same);
   fprintf(f, "   ---\n");
   fprintf(f, "   > par_list->SetParVal(par2, 2);\n");
   par_list->SetParVal(par_list->IndexPar("par2"), 2);
   fprintf(f, "   > par_list->PrintPars(f);\n");
   par_list->PrintPars(f);
   fprintf(f, "   > par_list->GetParListStatus() = %d;\n", par_list->GetParListStatus());
   fprintf(f, "   > GetFreePars()->PrintPars(f);\n");
   GetFreePars()->PrintPars(f);
   fprintf(f, "   > GetFreePars()->GetParListStatus() = %d;\n", GetFreePars()->GetParListStatus());
   fprintf(f, "   > UpdateFromFreeParsAndResetStatus(is_force_update=false)\n");
   UpdateFromFreeParsAndResetStatus(false);
   fprintf(f, "   > GetFreePars()->GetParListStatus() = %d;\n", GetFreePars()->GetParListStatus());
   fprintf(f, "   > EvalFormulaTXYZ(vals_txyz={0,1,1,0});    with (par1=-1,par2=2) =>  %le\n", EvalFormulaTXYZ(vals_txyz));
   vals_txyz[1] = 2;
   fprintf(f, "   > EvalFormulaTXYZ(vals_txyz={0,2,1,0});    with (par1=-1,par2=2) =>  %le\n", EvalFormulaTXYZ(vals_txyz));
   fprintf(f, "   > TUCoordTXYZ *coord_txyz = new TUCoordTXYZ(); coord_txyz->SetVals(vals_txyz);\n");
   TUCoordTXYZ *coord_txyz = new TUCoordTXYZ();
   coord_txyz->SetVals(vals_txyz);
   fprintf(f, "   > ValueTXYZ(TUCoordTXYZ *coord_txyz));                         =>  %le\n", ValueTXYZ(coord_txyz));
   fprintf(f, "\n");
   fprintf(f, "\n");

   fprintf(f, " * Check E-dep formula\n");
   fprintf(f, "   > SetFormula(formula=\"par1*beta*Etot^2\", var=\"beta,gamma,Etot\", free_par=par_list);\n");
   SetFormula("par1*beta*Etot^2", "beta,gamma,Etot", par_list);
   fprintf(f, "   > ValsTXYZEFormat();  =>  %d\n", ValsTXYZEFormat());
   fprintf(f, "   > PrintFormula(f);\n");
   PrintFormula(f);
   const Int_t ne = 3;
   Double_t edepvals[ne] = {0.99, 2., 3.};
   fprintf(f, "   > EvalFormulaE(edepvals={0.99, 2., 3.}); with (par1=-1) =>  %le\n", EvalFormulaE(edepvals));
   edepvals[0] = 0.5;
   fprintf(f, "   > TUCoordE *coord_e = new TUCoordE(); coord_txyz->SetCoordE(edepvals, 3, 0);\n");
   TUCoordE *coord_e = new TUCoordE();
   coord_e->SetCoordE(edepvals, 3, 0);
   fprintf(f, "   > ValueE(coord_e);                                      =>  %le\n", ValueE(coord_e));
   fprintf(f, "   > ValueETXYZ(coord_e, coord_txyz);                      =>  %le\n", ValueETXYZ(coord_e, coord_txyz));
   fprintf(f, "         N.B.: FAILS AS \"t,x,y,z\" VARIABLES NOT DECLARED IN SetFormula()\n");
   fprintf(f, "   > SetFormula(formula=\"par1*beta*Etot^2\", var=\"t,x,y,z,beta,gamma,Etot\", free_par=par_list);\n");
   SetFormula("par1*beta*Etot^2", "t,x,y,z,beta,gamma,Etot", par_list);
   fprintf(f, "   > ValueETXYZ(coord_e, coord_txyz);                      =>  %le\n", ValueETXYZ(coord_e, coord_txyz));
   fprintf(f, "         N.B.: NOW CORRECT, EVEN THOUGH coord_txyz UNUSED!\n");
   fprintf(f, "   > SetFormula(formula=\"par1*x*beta*Etot^2\", var=\"t,x,y,z,beta,gamma,Etot\", free_par=par_list);\n");
   SetFormula("par1*x*beta*Etot^2", "t,x,y,z,beta,gamma,Etot", par_list);
   fprintf(f, "   > ValueETXYZ(coord_e, coord_txyz);                      =>  %le\n", ValueETXYZ(coord_e, coord_txyz));
   fprintf(f, "\n");
   fprintf(f, "\n");


   fprintf(f, " * Copy formula\n");
   fprintf(f, "   > TUValsTXYZEFormula *formula = new TUValsTXYZEFormula();\n");
   TUValsTXYZEFormula *formula = new TUValsTXYZEFormula();
   fprintf(f, "   > formula->Copy(*this);\n");
   formula->Copy(*this);
   fprintf(f, "   > formula->PrintFormula(f);\n");
   formula->PrintFormula(f);
   fprintf(f, "\n");

   fprintf(f, " * Clone method\n");
   fprintf(f, "   > TUValsTXYZEFormula *formula2 = Clone();\n");
   TUValsTXYZEFormula *formula2 = Clone();
   fprintf(f, "   > formula2->PrintFormula(f);\n");
   formula2->PrintFormula(f);
   fprintf(f, "\n");

   delete par_list;
   par_list = NULL;
   delete coord_txyz;
   coord_txyz = NULL;
   delete coord_e;
   coord_e = NULL;
   delete formula;
   formula = NULL;
   delete formula2;
   formula2 = NULL;
}

//______________________________________________________________________________
void TUValsTXYZEFormula::UpdateFromFreeParsAndResetStatus(Bool_t is_force_update)
{
   //--- Updates formula and reset status if free par status is updated (i.e.,
   //    at least one parameter of inherited class TUFreeParList has changed).
   //  is_force_update           If true, force update, otherwise check that free pars have changed

   if (fFreePars && (fFreePars->GetParListStatus() || is_force_update)) {
      if (!is_force_update)
         fFreePars->UpdateParListStatusFromIndividualStatus();
      SetOrUpdateFormula("", "", NULL, true, true);
      fFreePars->ResetStatusParsAndParList();
   }
}
