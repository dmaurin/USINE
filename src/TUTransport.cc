// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

// C/C++ include
// ROOT include
// USINE include
#include "../include/TUAxesCrE.h"
#include "../include/TUMessages.h"
#include "../include/TUTransport.h"
#include "../include/TUValsTXYZEFormula.h"
#include "../include/TUValsTXYZEGrid.h"

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TUTransport                                                          //
//                                                                      //
// Transport parameters (convection, spatial and momentum diffusion).   //
//                                                                      //
// The class TUTransport provides a formula- or grid-based description  //
// (relying on the TUValsTXYZEVirtual abstract class) of the following  //
// transport parameters:                                                //
//    - fWind[3] is a vector of the wind components (up to 3 axes);
//    - fVA is the Alfvénic speed;
//    - fK[9] is the spatial diffusion tensor (up to 9 components);
//    - fKpp is the diffusion coefficient in momentum space.
// Whenever a formula-based description is used, it depends on the      //
// following variables only (see TUAxesCrE):                            //
//    - {x,y,z,t} (space-time generic coordinates): e.g., fWind[] and fVa;
//    - {beta,Rig,gamma,Etot,Ekn,x,y,z,t}: e.g., fK and fKpp;
// with beta=v/c (of the particle), Rig=p/Z (rigidity), gamma=Etot/m,   //
// Etot is the total energy, and Ekn the kinetic energy per nucleon.    //
//                                                                      //
// Free parameters and formulae:                                        //
// -+-+-+-+-+-+-+-+-+-+-+-+-+-
// Some class members can be described by formulae (depending on free   //
// parameter values, see TUValsTXYZVirtual). Whenever a parameter value //
// is changed, it has to be propagated to the formula. There are two    //
// ways to achieve that:
//    A. you can decide not to care about it: the class TUValsTXYZEFormula
//      will do the job automatically for you, whenever any method ValueXXX()
//      of this class is called. Indeed, the latter will invariably end up
//      calling TUValsTXYZEFormula::EvalFormula() or the like of it, and
//      then TUValsTXYZEFormula::UpdateFromFreeParsAndResetStatus()
//     to do the job.
//    B. call UpdateFromFreeParsAndResetStatus() to update all formula found
//      in this class (and reset TUFreeParList status to 'updated'.
//                                                                      //
//                                                                      //
// For the often used isotropic diffusion, several forms have been      //
// proposed in the literature, base on theoretical or phenomenological  //
// considerations. We reproduce below some of these formulae for        //
// reference (for instance, in the example below, the free parameters   //
// would be delta, eta_t, Va, K0):                                      //
// Free 'eta' [Maurin et al., A&A 516, A67 (2010)]                      //
//    * K00 = beta^eta_t*K0*(Rig/1.GV)^(delta)
//    * Kpp = 2/9*Va^2*beta^2*Etot^2/K
// Leaky-Box Inspired (special case of free eta with eta_t=0)           //
//    * K = K0*(Rig/1.GV)^(delta)
//    * Kpp = 2/9*Va^2*beta^2*Etot^2/K
// Slab-Alfven mode - Std form (special case of free eta with eta_t=1)  //
//    * K = beta*K0*(Rig/1.GV)^(delta)
//    * Kpp = 2/9*Va^2*beta^2*Etot^2/K
// Isotropic Fast Magnetosonic mode [Schlickeiser, CR Astrophys. (2002)]//
//    * K = beta^(2-DELTA)*K0*(Rig/1.GV)^(delta)
//    * Kpp = 2/9*Va^2*p^2*Etot^2*beta^(1-beta)*ln(v/Va)/K
// Mixture Slab/Magnetosonic mode [Schlickeiser, CR Astrophys. (2002)]  //
//    * K = beta^(1-DELTA)*K0*(rigidity/1.GV)^(delta)
//    * Kpp = 2/9*Va^2*p^2*Etot^2*beta^(1-beta)*ln(v/Va)/K
// Maurin et al., ApJ 555, 585 (2001)                                   //
//    * K = beta*K0*(Rig/1.GV)^(delta)
//    * Kpp = 2/9*Va^2*beta^4*Etot^2/K
//////////////////////////////////////////////////////////////////////////

ClassImp(TUTransport)

//______________________________________________________________________________
TUTransport::TUTransport()
{
   // ****** Default constructor ******

   Initialise(false);
}

//______________________________________________________________________________
TUTransport::TUTransport(TUTransport const &transport)
{
   // ****** Copy constructor ******
   //  transport         Object to copy from

   Initialise(false);
   Copy(transport);
}


//______________________________________________________________________________
TUTransport::~TUTransport()
{
   // ****** Default destructor ******

   Initialise(true);
}

//______________________________________________________________________________
void TUTransport::Copy(TUTransport const &transport)
{
   //--- Copies transport in current class.
   //  transport         Object to copy from

   // Initialise
   Initialise(false);

   TUFreeParList *pars = transport.GetFreePars();
   if (pars)
      fFreePars = pars->Clone();
   pars = NULL;

   // N.B.: the order of copy matters!
   TUValsTXYZEVirtual *vals = NULL;
   // Wind[1->3]
   fWind = new TUValsTXYZEVirtual*[3];
   for (Int_t i = 0; i < 3; ++i) {
      vals = transport.GetWind(i);
      if (vals)
         fWind[i] = vals->Clone();
      else
         fWind[i] = NULL;
   }
   fNWind = transport.GetNWind();

   // VA
   vals = transport.GetVA();
   if (vals)
      fVA = vals->Clone();

   // K[1->9]
   fK = new TUValsTXYZEVirtual*[9];
   for (Int_t row = 0; row < 3; ++row) {
      for (Int_t col = 0; col < 3; ++col) {
         vals = transport.GetK(row, col);
         if (vals)
            fK[col * 3 + row] = vals->Clone();
         else
            fK[col * 3 + row] = NULL;
      }
   }
   fNK = transport.GetNK();

   // Kpp
   vals = transport.GetKpp();
   if (vals)
      fKpp = vals->Clone();
   vals = NULL;
}

//______________________________________________________________________________
void TUTransport::Initialise(Bool_t is_delete)
{
   //---Initialises class members (and delete if required).
   //  is_delete         Whether to delete or just initialise

   if (is_delete) {
      if (fFreePars) delete fFreePars;
      if (fKpp) delete fKpp;
      if (fVA) delete fVA;

      if (fK) {
         for (Int_t row = 0; row < 3; ++row) {
            for (Int_t col = 0; col < 3; ++col) {
               if (fK[col * 3 + row]) delete fK[col * 3 + row];
               fK[col * 3 + row] = NULL;
            }
         }
         delete[] fK;
      }

      if (fWind) {
         for (Int_t i = 0; i < 3; ++i) {
            if (fWind[i]) delete fWind[i];
            fWind[i] = NULL;
         }
         delete[] fWind;
      }
   }

   fFreePars = NULL;
   fK = NULL;
   fKpp = NULL;
   fVA = NULL;
   fWind = NULL;
}

//______________________________________________________________________________
void TUTransport::SetClass(TUInitParList *init_pars, string const &group, string const &subgroup, Bool_t is_verbose, FILE *f_log)
{
   //--- Updates class formula/file from parameter file (belonging to Group#Subgroup).
   //  init_pars         Initialisation parameters
   //  group             Group name
   //  subgroup          Subgroup name
   //  is_verbose        Verbose or not when class is set
   //  f_log             Log for USINE

   const Int_t gg = 3;
   string gr_sub_name[gg] = {group, subgroup, ""};

   string indent = TUMessages::Indent(true);
   if (is_verbose)
      fprintf(f_log, "%s[TUTransport::SetClass]\n", indent.c_str());

   // Clear class
   Initialise(true);

   // Allocate and add free transport parameters
   // (delete if no free param. found)
   fFreePars = new TUFreeParList();
   fFreePars->AddPars(init_pars, group, subgroup, is_verbose, f_log);
   if (is_verbose)
      fFreePars->PrintPars(f_log);
   if (fFreePars->GetNPars() == 0) {
      delete fFreePars;
      fFreePars = NULL;
   }

   if (is_verbose)
      fprintf(f_log, "%s### Set transport description (diffusion, convection...)\n", indent.c_str());


   const Int_t n_transport = 4;
   string par[n_transport] = {"Wind", "VA", "K", "Kpp"};
   string vars_spacetime = "t,x,y,z";
   TUAxesCrE dummy;
   dummy.LoadEVarsNamesAndUnits(is_verbose, f_log);
   string vars_spacetime_e = "t,x,y,z," + dummy.GetEVarNames();


   // Loop on transport coefficient to fill
   for (Int_t i = 0; i < n_transport; ++i) {
      // Find parameter index and number of entries (for this par)
      gr_sub_name[2] = par[i];
      Int_t i_par = init_pars->IndexPar(gr_sub_name);
      Int_t n_multi = init_pars->GetParEntry(i_par).GetNVals();
      if (n_multi == 1 && init_pars->GetParEntry(i_par).GetVal() == "-") continue;
      // If parameter found, allocate and fill relevant class member
      if (i_par < 0)
         continue;
      else if (is_verbose)
         fprintf(f_log, "%s  - %s %s", indent.c_str(),  par[i].c_str(), ("["
                 + gr_sub_name[0] + "@" + gr_sub_name[1] + "@"
                 + gr_sub_name[2] + "]").c_str());

      if (i == 0) { // fWind: 3 components allowed (W0, W1, W2)
         fNWind = n_multi;
         if (fNWind > 3)
            TUMessages::Error(f_log, "TUTransport", "SetClass",
                              "fWind is the galactic wind vector, it cannot have more than 3 components!");
         fWind = new TUValsTXYZEVirtual*[3];
         for (Int_t l = 0; l < 3; ++l)
            fWind[l] = NULL;
         // Loop on multi-found, check that they exist, and fill them
         for (Int_t j = 0; j < fNWind; ++j) {
            string multi_name = (init_pars->GetParEntry(i_par).GetVal(j)).substr(0, 2);
            if (multi_name == "W0" || multi_name == "W1" || multi_name == "W2") {
               Int_t k = atoi((multi_name.substr(1, 1)).c_str());

               // Check whether formula or read file
               // (and retrieve formula or file name)
               string formula_or_file = "";
               gENUM_FCNTYPE format = init_pars->ExtractFormat(i_par, j, formula_or_file, is_verbose, f_log);
               if (format == kFORMULA) {
                  fWind[k] = new TUValsTXYZEFormula();
                  fWind[k]->SetClass(formula_or_file, is_verbose, f_log, vars_spacetime, fFreePars);
               } else if (format == kGRID) {
                  fWind[k] = new TUValsTXYZEGrid();
                  fWind[k]->SetClass(formula_or_file, is_verbose, f_log);
               } else if (format == kSPLINE) {
                  TUMessages::Error(f_log, "TUTransport", "SetClass", "Spline not implemented yet for this function");
               }
            } else {
               string message = "Component " + group + "@" + subgroup
                                + "@" + gr_sub_name[2] + "@" + init_pars->GetParEntry(i_par).GetVal(j)
                                + " is not a Wi";
               TUMessages::Error(f_log, "TUTransport", "SetClass", message);
            }
         }

      } else if (i == 1) { // fVA (scalar)
         // Check whether formula or read file (and retrieve formula or file name)
         string formula_or_file = "";
         gENUM_FCNTYPE format = init_pars->ExtractFormat(i_par, 0, formula_or_file, is_verbose, f_log);
         if (format == kFORMULA) {
            fVA = new TUValsTXYZEFormula();
            fVA->SetClass(formula_or_file, is_verbose, f_log, vars_spacetime, fFreePars);
         } else if (format == kGRID) {
            fVA = new TUValsTXYZEGrid();
            fVA->SetClass(formula_or_file, is_verbose, f_log);
         } else if (format == kSPLINE) {
            TUMessages::Error(f_log, "TUTransport", "SetClass", "Spline not implemented yet for this function");
         }

      } else if (i == 2) { // fK: up to 9 components allowed (K00, K01, ... K22)
         fNK = n_multi;
         if (fNK > 9)
            TUMessages::Error(f_log, "TUTransport", "SetClass",
                              "fK is the diffusion tensor, it cannot have more than 9 components!");
         fK = new TUValsTXYZEVirtual*[9];
         for (Int_t l = 0; l < 9; ++l)
            fK[l] = NULL;


         // Loop on multi-found, check that they exist, and fill them
         for (Int_t j = 0; j < fNK; ++j) {
            string multi_name = (init_pars->GetParEntry(i_par).GetVal(j)).substr(0, 3);
            if (multi_name == "K00" || multi_name == "K01" || multi_name == "K02"
                  || multi_name == "K10" || multi_name == "K11" || multi_name == "K12"
                  || multi_name == "K20" || multi_name == "K21" || multi_name == "K22") {
               Int_t row = atoi((multi_name.substr(1, 1)).c_str());
               Int_t col = atoi((multi_name.substr(2, 1)).c_str());

               // Check whether formula or read file (and retrieve formula or file name)
               string formula_or_file = "";
               gENUM_FCNTYPE format = init_pars->ExtractFormat(i_par, j, formula_or_file, is_verbose, f_log);
               if (format == kFORMULA) {
                  fK[col * 3 + row] = new TUValsTXYZEFormula();
                  fK[col * 3 + row]->SetClass(formula_or_file, is_verbose, f_log, vars_spacetime_e, fFreePars);
               } else if (format == kGRID) {
                  fK[col * 3 + row] = new TUValsTXYZEGrid();
                  fK[col * 3 + row]->SetClass(formula_or_file, is_verbose, f_log);
               } else if (format == kSPLINE) {
                  TUMessages::Error(f_log, "TUTransport", "SetClass", "Spline not implemented yet for this function");
               }
            } else {
               string message = "Component " + group + "@" + subgroup + "@" + gr_sub_name[2]
                                + "@" + init_pars->GetParEntry(i_par).GetVal(j) + " is not a Kij";
               TUMessages::Error(f_log, "TUTransport", "SetClass", message);
            }
         }

      } else if (i == 3) { // fKpp (scalar, but VA and Kij may be used in Kpp!)
         // Check whether formula or read file (and retrieve formula or file name)
         string formula_or_file = "";
         gENUM_FCNTYPE format = init_pars->ExtractFormat(i_par, 0, formula_or_file, is_verbose, f_log);

         string kpp_val = init_pars->GetParEntry(i_par).GetVal();
         string kpp_form = (init_pars->GetParEntry(i_par).GetVal()).substr(kpp_val.find_first_of("|") + 1);
         // If Kpp is a formula, we check if VA and Kij are used
         if (format == kFORMULA) {
            // Check whether VA formula is used in this transport formula
            if (kpp_form.find("VA") != string::npos)
               kpp_form = "VA:=" + fVA->GetFormulaString() + ";" + kpp_form;

            // Check whether Kij formulae are used in this transport formula
            for (Int_t row = 0; row < 3; ++row) {
               for (Int_t col = 0; col < 3; ++col) {
                  string kij = Form("K%d%d", row, col);
                  if (kpp_form.find(kij) != string::npos)
                     kpp_form = (string)kij + ":=" + fK[col * 3 + row]->GetFormulaString() + ";" + kpp_form;
               }
            }
            string kpp_new = kpp_val.substr(0, kpp_val.find_first_of("|") + 1) + kpp_form;
            init_pars->SetVal(i_par, kpp_new);

            // N.B.: need to call a second time to update with kpp_new formula
            init_pars->ExtractFormat(i_par, 0, formula_or_file, is_verbose, f_log);
            fKpp = new TUValsTXYZEFormula();
            fKpp->SetClass(formula_or_file, is_verbose, f_log, vars_spacetime_e, fFreePars);
         } else if (format == kGRID) {
            fKpp = new TUValsTXYZEGrid();
            fKpp->SetClass(formula_or_file, is_verbose, f_log);
         } else if (format == kSPLINE) {
            TUMessages::Error(f_log, "TUTransport", "SetClass", "Spline not implemented yet for this function");
         }
      }
   }
   if (is_verbose)
      fprintf(f_log, "%s[TUTransport::SetClass] <DONE>\n\n", indent.c_str());
   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUTransport::PrintSummary(FILE *f) const
{
   //--- Prints transport summary.
   //  f                 File in which to print

   if (fWind) {
      for (Int_t i = 0; i < GetNWind(); ++i)
         PrintWind(f, i);
   }

   if (fVA) PrintVA(f);

   if (fK) {
      for (Int_t row = 0; row < 3; ++row) {
         for (Int_t col = 0; col < 3; ++col) {
            PrintK(f, row, col);
         }
      }
   }

   if (fKpp) PrintKpp(f);
}

//______________________________________________________________________________
void TUTransport::TEST(TUInitParList *init_pars, FILE *f)
{
   //--- Test function for this class (called in usine -t).
   //  init_pars         TUInitParList object of USINE parameters
   //  f                 Output file for test


   string message = "Transport coefficients/parameters\n"
                    "The optional quantities that can be filled by either a formula- or\n"
                    "an array based-description are:\n"
                    "   - spatial diffusion tensor (up to 9 components);\n"
                    "   - momentum diffusion (scalar);\n"
                    "   - Alfvénic speed;\n"
                    "   - Wind velocity (up to 3 directions).";
   TUMessages::Test(f, "TUTransport", message);

   /******************************/
   /*   Array-based description  */
   /******************************/

   /*****************************/
   /* Formula-based description */
   /*****************************/
   // Set class and test methods
   fprintf(f, " * Check formula-based description (for Leaky-Box model)\n");
   fprintf(f, "   > SetClass(init_pars=\"%s\", group=Model0DLeakyBox, subgroup=Transport, is_verbose=true, f_log=f);\n",
           init_pars->GetFileNames().c_str());
   SetClass(init_pars, "Model0DLeakyBox", "Transport", true, f);
   fprintf(f, "   > PrintSummary(f)\n");
   PrintSummary(f);

   // Set class and test methods
   fprintf(f, "\n");
   fprintf(f, "\n");
   fprintf(f, "\n");
   fprintf(f, " * Check more complex formula-based description (for ModelTEST)\n");
   fprintf(f, "   > SetClass(init_pars,group=ModelTEST, subgroup=Transport, false, f);\n");
   SetClass(init_pars, "ModelTEST", "Transport", false, f);
   fprintf(f, "\n");
   fprintf(f, "   > PrintSummary(f);\n");
   PrintSummary(f);
   fprintf(f, "\n");
   fprintf(f, "   > TUFreeParList *free_pars = GetFreePars();\n");
   TUFreeParList *free_pars = GetFreePars();
   fprintf(f, "   > free_pars->ResetStatusParsAndParList();\n");
   free_pars->ResetStatusParsAndParList();
   fprintf(f, "   > free_pars->PrintPars(f);\n");
   free_pars->PrintPars(f);

   //fprintf(f, "   > PrintSummary(f)\n");
   //PrintSummary(f);
   // Retrieve e_var variables used
   fprintf(f, "   > TUCoordTXYZ coord_txyz; TUCoordE coord_e;\n");
   TUCoordTXYZ coord_txyz;
   TUCoordE coord_e;
   fprintf(f, "   > coord_txyz.PrintVals(f);\n");
   coord_txyz.PrintVals(f);
   fprintf(f, "   > ValueVA(&coord_txyz)       =>  %le;\n", ValueVA(&coord_txyz));
   fprintf(f, "   > ValueWind(&coord_txyz, 0)  =>  %le;\n", ValueWind(&coord_txyz, 0));
   fprintf(f, "   > ValueWind(&coord_txyz, 1)  =>  %le;\n", ValueWind(&coord_txyz, 1));
   fprintf(f, "   > ValueWind(&coord_txyz, 2)  =>  %le;\n", ValueWind(&coord_txyz, 2));
   fprintf(f, "\n");
   fprintf(f, "   > coord_e.PrintVals(f);\n");
   coord_e.PrintVals(f);
   fprintf(f, "   > fK[0]->GetFormulaVars() = %s;\n", (fK[0]->GetFormulaVars()).c_str());
   vector<string> e_vars;
   TUMisc::String2List(fK[0]->GetFormulaVars(), ",", e_vars);
   Int_t n_edep = e_vars.size() - 4;
   fprintf(f, "   > Double_t edep_vals[%d] = {2, 2, 2....};\n", n_edep);
   vector<Double_t> edep_vals(n_edep, 2.);
   fprintf(f, "   > coord_e.SetValsE(&edepvals[0], %d, halfbin_status=0, is_use_or_copy=true);\n", n_edep);
   coord_e.SetValsE(&edep_vals[0], n_edep, 0, true);
   fprintf(f, "   > coord_e.PrintVals(f);\n");
   coord_e.PrintVals(f);
   fprintf(f, "   > ValueK(&coord_e, 0, 0)  =>  %le;  [space-time discarded, taken to be 0,0,0,0]\n", ValueK(&coord_e, 0, 0));
   fprintf(f, "   > ValueK(&coord_e, 1, 1)  =>  %le;  [space-time discarded, taken to be 0,0,0,0]\n", ValueK(&coord_e, 1, 1));
   fprintf(f, "   > ValueK(&coord_e, 2, 2)  =>  %le;  [space-time discarded, taken to be 0,0,0,0]\n", ValueK(&coord_e, 2, 2));
   fprintf(f, "   > ValueK(&coord_e, 1, 2)  =>  %le;  [space-time discarded, taken to be 0,0,0,0]\n", ValueK(&coord_e, 1, 2));
   fprintf(f, "   > ValueKpp(&coord_e)      =>  %le;  [space-time discarded, taken to be 0,0,0,0]\n", ValueKpp(&coord_e));
   fprintf(f, "\n");
   fprintf(f, "\n");
   fprintf(f, "   > GetFreePars()->PrintPars(f);\n");
   GetFreePars()->PrintPars(f);
   fprintf(f, "   > coord_txyz.PrintVals(f);\n");
   coord_txyz.PrintVals(f);
   fprintf(f, "   > coord_e.PrintVals(f);\n");
   coord_e.PrintVals(f);
   fprintf(f, "   > free_pars->ResetStatusParsAndParList();;\n");
   free_pars->ResetStatusParsAndParList();;
   fprintf(f, "   > free_pars->SetParVal(free_pars->IndexPar(\"delta\"), 1.);\n");
   free_pars->SetParVal(free_pars->IndexPar("delta"), 1.);
   fprintf(f, "   > free_pars->PrintPars(f);\n");
   free_pars->PrintPars(f);
   fprintf(f, "   > UpdateFromFreeParsAndResetStatus();\n");
   UpdateFromFreeParsAndResetStatus();
   fprintf(f, "   > ValueVA(&coord_txyz)                =>  %le;\n", ValueVA(&coord_txyz));
   fprintf(f, "   > ValueWind(&coord_txyz)              =>  %le;\n", ValueWind(&coord_txyz, 0));
   fprintf(f, "   > ValueK(&coord_e)                    =>  %le;  [space-time discarded, taken to be 0,0,0,0]\n", ValueK(&coord_e));
   fprintf(f, "   > ValueK(&coord_e, &coord_txyz, 0, 0) =>  %le;  [with space-time dependence]\n", ValueK(&coord_e, &coord_txyz, 0, 0));
   fprintf(f, "   > ValueK(&coord_e, &coord_txyz, 1, 1) =>  %le;\n", ValueK(&coord_e, &coord_txyz, 1, 1));
   fprintf(f, "   > ValueKpp(&coord_e)                  =>  %le;\n", ValueKpp(&coord_e));
   fprintf(f, "   > ValueK(&coord_e)                    =>  %le;  [if coord_txyz not used, default values are 0.]\n", ValueK(&coord_e));
   fprintf(f, "   > coord_txyz.SetValXY(1.,2.);\n");
   coord_txyz.SetValXY(1., 2.);
   fprintf(f, "   > ValueK(&coord_e, &coord_txyz, 0, 0) =>  %le;\n", ValueK(&coord_e, &coord_txyz, 0, 0));
   fprintf(f, "   > ValueK(&coord_e, &coord_txyz, 1, 1) =>  %le;\n", ValueK(&coord_e, &coord_txyz, 1, 1));
   fprintf(f, "   > coord_txyz.SetValXY(1.,0.);\n");
   coord_txyz.SetValXY(1., 0.);
   fprintf(f, "   > ValueK(&coord_e, &coord_txyz, 0, 0) =>  %le;\n", ValueK(&coord_e, &coord_txyz, 0, 0));
   fprintf(f, "   > ValueK(&coord_e, &coord_txyz, 1, 1) =>  %le;\n", ValueK(&coord_e, &coord_txyz, 1, 1));
   fprintf(f, "   > free_pars->SetParVal(free_pars->IndexPar(\"delta\"), 2.);\n");
   free_pars->SetParVal(free_pars->IndexPar("delta"), 2.);
   free_pars->PrintPars(f);
   fprintf(f, "   > ValueK(&coord_e)                    =>  %le;\n", ValueK(&coord_e, &coord_txyz, 0, 0));
   fprintf(f, "\n");
   fprintf(f, "\n");

   fprintf(f, " * N.B.: every time ValueXXX() or UpdateFromFreeParsAndResetStatus() is called, update all class members.\n");
   fprintf(f, "   > GetFreePars()->GetParListStatus() = %d;\n", GetFreePars()->GetParListStatus());
   fprintf(f, "   > ValueK(&coord_e, &coord_txyz, 0, 0) =>  %le;\n", ValueK(&coord_e, &coord_txyz, 0, 0));
   fprintf(f, "   > ValueK(&coord_e, &coord_txyz, 1, 1) =>  %le;\n", ValueK(&coord_e, &coord_txyz, 1, 1));
   fprintf(f, "   ---\n");
   fprintf(f, "   > GetFreePars()->GetParListStatus() = %d;\n", GetFreePars()->GetParListStatus());
   fprintf(f, "   > GetFreePars()->SetParVal(delta, 10)\n");
   GetFreePars()->SetParVal(GetFreePars()->IndexPar("delta"), 10.);
   fprintf(f, "   > GetFreePars()->PrintPars(f)\n");
   GetFreePars()->PrintPars(f);
   fprintf(f, "   > GetFreePars()->GetParListStatus() = %d;\n", GetFreePars()->GetParListStatus());
   fprintf(f, "   > ValueK(&coord_e, &coord_txyz, 0, 0) =>  %le;\n", ValueK(&coord_e, &coord_txyz, 0, 0));
   fprintf(f, "   > ValueK(&coord_e, &coord_txyz, 1, 1) =>  %le;\n", ValueK(&coord_e, &coord_txyz, 1, 1));
   fprintf(f, "   > GetFreePars()->GetParListStatus() = %d;\n", GetFreePars()->GetParListStatus());
   fprintf(f, "   ---\n");
   fprintf(f, "   > GetFreePars()->SetParVal(delta, 1.)\n");
   GetFreePars()->SetParVal(GetFreePars()->IndexPar("delta"), 1.);
   fprintf(f, "   > GetFreePars()->GetParListStatus() = %d;\n", GetFreePars()->GetParListStatus());
   fprintf(f, "   > UpdateFromFreeParsAndResetStatus();\n");
   UpdateFromFreeParsAndResetStatus();
   fprintf(f, "   > GetFreePars()->GetParListStatus() = %d;\n", GetFreePars()->GetParListStatus());
   fprintf(f, "   > ValueK(&coord_e, &coord_txyz, 0, 0) =>  %le;\n", ValueK(&coord_e, &coord_txyz, 0, 0));
   fprintf(f, "   > ValueK(&coord_e, &coord_txyz, 1, 1) =>  %le;\n", ValueK(&coord_e, &coord_txyz, 1, 1));
   fprintf(f, "\n");
   fprintf(f, "\n");


   fprintf(f, " * Values of transport parameters\n");
   fprintf(f, "   > free_pars->SetParVal(free_pars->IndexPar(\"delta\"), 2.);\n");
   free_pars->SetParVal(free_pars->IndexPar("delta"), 2.);
   // Define rigidity grid and CR entry (12C)
   Int_t n_e = 20;
   TUAxis *rig = new TUAxis();
   rig->SetClass(0.1, 20., n_e, "R", "GV", kLOG);
   TUAxesCrE *axes_cre = new TUAxesCrE();
   axes_cre->SetClass(init_pars, false, f);
   Int_t i_12c = axes_cre->Index("12C", false, f, false);
   fprintf(f, "   > vector<TUCoordE> e_coords;  => + fill it (to calculate on E-vars=%s\n", fK[0]->GetFormulaVars().c_str());
   vector<TUCoordE> e_coords;
   for (Int_t i = 0; i < n_e; ++i) {
      Double_t ekn = TUPhysics::R_to_Ekn(rig->GetVal(i), axes_cre->GetCREntry(i_12c).GetA(),
                                         axes_cre->GetCREntry(i_12c).GetmGeV(),
                                         axes_cre->GetCREntry(i_12c).GetZ());
      axes_cre->EvalEVarsVals(i_12c, ekn, edep_vals);
      TUCoordE tmp_e;
      tmp_e.SetValsE(&edep_vals[0], n_edep, 0, false);
      e_coords.push_back(tmp_e);
      fprintf(f, "      k=%d   ekn=%le   R=%le   K(R)=%le\n", i, ekn, rig->GetVal(i), ValueK(&coord_e, &coord_txyz, 0, 0));
   }
   e_coords.clear();
   delete axes_cre;
   delete rig;
   fprintf(f, "\n");

   // Copy
   fprintf(f, " * Test Copy method:\n");
   fprintf(f, "   > TUTransport transport; transport.Copy(*this);\n");
   TUTransport transport;
   transport.Copy(*this);
   fprintf(f, "   > transport.PrintSummary(f);\n");
   transport.PrintSummary(f);
   fprintf(f, "\n");

   free_pars = NULL;
}

//______________________________________________________________________________
void TUTransport::UpdateFromFreeParsAndResetStatus()
{
   //--- Updates all formulae from updated fFreePars values and reset fFreePars status.

   // If status of parameters has changed
   if (fFreePars && fFreePars->GetParListStatus()) {

      // Update VA
      if (fVA)
         fVA->UpdateFromFreeParsAndResetStatus(true);

      // Update fWind[0->2]
      if (fWind) {
         for (Int_t i = 0; i < 3; ++i) {
            if (fWind[i])
               fWind[i]->UpdateFromFreeParsAndResetStatus(true);
         }
      }

      // Update Kpp
      if (fKpp)
         fKpp->UpdateFromFreeParsAndResetStatus(true);

      // Update K[0->8]
      if (fK) {
         for (Int_t row = 0; row < 3; ++row) {
            for (Int_t col = 0; col < 3; ++col) {
               if (fK[col * 3 + row])
                  fK[col * 3 + row]->UpdateFromFreeParsAndResetStatus(true);
            }
         }
      }

      fFreePars->ResetStatusParsAndParList();
   }
}