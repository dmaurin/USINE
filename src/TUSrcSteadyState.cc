// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

// C/C++ include
// ROOT include
// USINE include
#include "../include/TUSrcSteadyState.h"

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TUSrcSteadyState                                                     //
//                                                                      //
// Steady-state source: spectrum, spatial distribution for a single CR. //
//                                                                      //
// The class TUSrcSteadyState provides the description of a generic     //
// steady-state source (the class derives from the abstract class       //
// TUSrcVirtual). It corresponds to a single species, for which the     //
// spectrum is a generic function of time and energy (based on          //
// TUValsTXYZE object), whereas the spatial distribution is a generic   //
// function of space coordinates (also based on a TUValsTXYZE object).  //
// The key parameters are:                                              //
//    - source name;
//    - spatial distribution (and its parameters);
//    - spectrum description (and its parameters).
//                                                                      //
// Free parameters and formulae:                                        //
// -+-+-+-+-+-+-+-+-+-+-+-+-+-
// Some class members can be described by formulae (which depend on free//
// parameter values, see TUValsTXYZVirtual). Whenever a parameter value //
// is changed, it has to be propagated to the formula. There are two    //
// ways to achieve that:
//    A. you can decide not to care about it: the class TUValsTXYZEFormula
//      will do the job automatically for you, whenever any method ValueXXX()
//      of this class is called. Indeed, the latter will invariably end up
//      calling TUValsTXYZEFormula::EvalFormula() or the like of it, and
//      then TUValsTXYZEFormula::UpdateFromFreeParsAndResetStatus()
//     to do the job.
//    B. call UpdateFromFreeParsAndResetStatus() to update all formula found
//      in this class (and reset TUFreeParList status to 'updated'.
//                                                                      //
//////////////////////////////////////////////////////////////////////////

ClassImp(TUSrcSteadyState)

//______________________________________________________________________________
TUSrcSteadyState::TUSrcSteadyState() : TUSrcVirtual()
{
   // ****** Default constructor ******

   Initialise(false);
}

//______________________________________________________________________________
TUSrcSteadyState::TUSrcSteadyState(TUSrcSteadyState const &src_steadystate) : TUSrcVirtual()
{
   // ****** Copy constructor ******
   //  src_steadystate   Pointer used to fill class

   Initialise(false);
   Copy(src_steadystate);
}

//______________________________________________________________________________
TUSrcSteadyState::~TUSrcSteadyState()
{
   // ****** Default destructor ******

   Initialise(true);
}

//______________________________________________________________________________
void TUSrcSteadyState::CopySpectrumOrSpatDist(TUValsTXYZEVirtual *spect_or_spatdist, Bool_t is_spect_or_spatdist)
{
   //--- Copies 'spect_or_spatdist' to describe spectrum or spatial distribution
   //    of this class.
   //  spect_or_spatdist      Object is the spectrum (or spatial distribution to copy)
   //  is_spect_or_spatdist   Spectrum or spatial distribution?

   if (is_spect_or_spatdist) {
      if (fIsDeleteSrcSpectrum && fSrcSpectrum)
         delete fSrcSpectrum;
      fSrcSpectrum = NULL;
      fIsDeleteSrcSpectrum = true;
      fSrcSpectrum = spect_or_spatdist->Clone();
   } else {
      if (fIsDeleteSrcSpatialDist && fSrcSpatialDist)
         delete fSrcSpatialDist;
      fSrcSpatialDist = NULL;
      fIsDeleteSrcSpatialDist = true;
      fSrcSpatialDist = spect_or_spatdist->Clone();
   }
}

//______________________________________________________________________________
void TUSrcSteadyState::Copy(TUSrcSteadyState const &src_steadystate)
{
   //--- Copies object in class.
   //  src_steadystate   Object to copy from

   Initialise(true);

   fSrcName = src_steadystate.GetSrcName();

   fIsDeleteSrcSpatialDist = src_steadystate.IsDeleteSrcSpatialDist();
   fIsDeleteSrcSpectrum = src_steadystate.IsDeleteSrcSpectrum();

   if (fIsDeleteSrcSpatialDist) {
      TUValsTXYZEVirtual *src = src_steadystate.GetSrcSpatialDist();
      if (src)
         fSrcSpatialDist = src->Clone();
      src = NULL;
   } else
      fSrcSpatialDist = src_steadystate.GetSrcSpatialDist();

   if (fIsDeleteSrcSpectrum) {
      TUValsTXYZEVirtual *src = src_steadystate.GetSrcSpectrum();
      if (src)
         fSrcSpectrum = src->Clone();
      src = NULL;
   } else
      fSrcSpectrum = src_steadystate.GetSrcSpectrum();
}

//______________________________________________________________________________
void TUSrcSteadyState::FillSpatialDistFromTemplate(TUSrcTemplates *templ, string const &templ_name, FILE *f_log)
{
   //--- Fills fSrcSpatialDist from template matching 'templ_name'.
   //  templ             List of spectra and spatial distrib. templates
   //  templ_name        Source spectrum template to search for
   //  f_log             Log for USINE

   if (!templ)
      return;

   string indent = TUMessages::Indent(true);
   fprintf(f_log, "%s### Create steady-state source spatial distribution from template %s\n",
           indent.c_str(), templ_name.c_str());

   // Search for index i template corresponding to templ_name
   Int_t i_templ = templ->IndexSrcTemplate(templ_name, false);
   if (i_templ < 0)
      TUMessages::Error(f_log, "TUSrcSteadyState", "FillSpatialDistFromTemplate",
                        string("Template" + templ_name + " not found!"));

   // Delete (if required) spectrum
   if (fSrcSpatialDist && fIsDeleteSrcSpatialDist)
      delete fSrcSpatialDist;

   fSrcSpatialDist = templ->GetSrc(i_templ)->Clone();
   fIsDeleteSrcSpatialDist = true;

   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUSrcSteadyState::FillSpectrumFromTemplate(TUSrcTemplates *templ, string const &templ_name, FILE *f_log)
{
   //--- Fills fSpectrum from template matching 'templ_name'.
   //  templ             List of spectra and spatial distrib. templates
   //  templ_name        Source spectrum template to search for
   //  is_verbose        Verbose on or off
   //  f_log             Log for USINE

   if (!templ)
      return;

   string indent = TUMessages::Indent(true);
   fprintf(f_log, "%s### Create steady-state source spectrum from template %s\n",
           indent.c_str(), templ_name.c_str());

   // Search for index i template corresponding to templ_name
   Int_t i_templ = templ->IndexSrcTemplate(templ_name, true);
   if (i_templ < 0)
      TUMessages::Error(f_log, "TUSrcSteadyState", "FillsFromTemplSpectrum",
                        string("Template" + templ_name + " not found!"));

   // Delete (if required) spectrum
   if (fSrcSpectrum && fIsDeleteSrcSpectrum)
      delete fSrcSpectrum;

   fSrcSpectrum = templ->GetSrc(i_templ)->Clone();
   fIsDeleteSrcSpectrum = true;

   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUSrcSteadyState::FillSpectrumOrSpatialDist(string const &formula, Bool_t is_verbose,
      FILE *f_log, string const &commasep_vars, TUFreeParList *free_pars,
      Bool_t is_use_or_copy_freepars, Bool_t is_spect_or_spatdist)
{
   //--- Allocates and updates fSrcSpectrum for formula.
   //  formula                  Formula to implement (see http://warp.povusers.org/FunctionParser/fparser.html)
   //  is_verbose               Verbose on or off
   //  f_log                    Log for USINE
   //  commasep_vars            List of comma-separated variables, e.g., "t,x,y,z" (default is no var)
   //  free_pars                List of free parameters (used only if formula and free_pars!=NULL)
   //  is_use_or_copy_freepars  Whether the parameters passed must be added (copied) or just used (pointed at)
   //  is_spect_or_spatdist     Spectrum or spatial distribution?

   // Allocate/fill spectrum or spatial distribution
   if (is_spect_or_spatdist) {
      if (fIsDeleteSrcSpectrum && fSrcSpectrum)
         delete fSrcSpectrum;

      fSrcSpectrum = new TUValsTXYZEFormula();
      fIsDeleteSrcSpectrum = true;
      fSrcSpectrum->SetClass(formula, is_verbose, f_log, commasep_vars, free_pars, is_use_or_copy_freepars);
   } else {
      if (fIsDeleteSrcSpatialDist && fSrcSpatialDist)
         delete fSrcSpatialDist;

      fSrcSpatialDist = new TUValsTXYZEFormula();
      fIsDeleteSrcSpatialDist = true;
      fSrcSpatialDist->SetClass(formula, is_verbose, f_log, commasep_vars, free_pars, is_use_or_copy_freepars);
   }
}

//______________________________________________________________________________
void TUSrcSteadyState::FillSpectrumOrSpatialDistFromTemplate(TUSrcTemplates *templ,
      string const &templ_name, Bool_t is_spect_or_spatdist, FILE *f_log)
{
   //--- Fills spectrum and spat.dist. ((none for point-like src) from template template.
   //  templ                  List of templates to use
   //  templ_name             Name of template to pick
   //  is_spect_or_spatdist   Whether we fill spectrum or spatial dist.
   //  is_verbose             Verbose on or off
   //  f_log                  Log for USINE

   if (!templ)
      return;

   if (is_spect_or_spatdist)
      FillSpectrumFromTemplate(templ, templ_name, f_log);
   else
      FillSpatialDistFromTemplate(templ, templ_name, f_log);
   return;
}

//______________________________________________________________________________
void TUSrcSteadyState::Initialise(Bool_t is_delete)
{
   //--- Initialises class members (and delete if required).
   //  is_delete         Whether to delete or just initialise

   if (is_delete) {
      if (fIsDeleteSrcSpatialDist && fSrcSpatialDist) delete fSrcSpatialDist;
      if (fIsDeleteSrcSpectrum && fSrcSpectrum) delete fSrcSpectrum;
   }

   fIsDeleteSrcSpatialDist = true;
   fIsDeleteSrcSpectrum = true;
   fSrcName = "";
   fSrcSpatialDist = NULL;
   fSrcSpectrum = NULL;
}

//______________________________________________________________________________
void TUSrcSteadyState::PrintSrc(FILE *f, Bool_t is_header) const
{
   //--- Prints in f, the (time-dependent) position of the source.
   //  f                 File in which to print
   //  header            Whether to write source info

   string indent = TUMessages::Indent(true);

   if (is_header)
      fprintf(f, "%s Steady-state source = %s\n", indent.c_str(), fSrcName.c_str());

   if (fSrcSpatialDist)
      fSrcSpatialDist->PrintSummary(f, "Spatial distribution");

   if (fSrcSpectrum)
      fSrcSpectrum->PrintSummary(f, "Spectrum");
   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUSrcSteadyState::TEST(TUInitParList *init_pars, FILE *f)
{
   //--- Test function for this class (called in usine -t).
   //  init_pars         TUInitParList class of initialisation parameters
   //  f                 Output file for test

   string message = "Generic steady-state source (spatial distribution and "
                    "source spectrum) for a single CR species";
   TUMessages::Test(f, "TUSrcSteadyState", message);


   // Set class and test methods
   fprintf(f, " * Base class:\n");
   fprintf(f, "   > SetSrcName(src_steadystate);\n");
   SetSrcName("src_test");
   fprintf(f, "\n");

   fprintf(f, " * Load source spectrum from available src templates:\n");
   fprintf(f, "   > TUSrcTemplates templ;\n");
   TUSrcTemplates templ;
   fprintf(f, "   > templ.SetClass(init_pars=\"%s\", is_verbose=false, f_log=f\n",
           init_pars->GetFileNames().c_str());
   templ.SetClass(init_pars, false, f);
   fprintf(f, "\n");
   fprintf(f, "   > FillSpectrumFromTemplate(&templ, POWERLAW, f_log=f);\n");
   FillSpectrumFromTemplate(&templ, "POWERLAW", f);
   fprintf(f, "   > fSrcSpectrum->PrintSummary(f);\n");
   fSrcSpectrum->PrintSummary(f);
   fprintf(f, "   > fSrcSpectrum->GetFreePars()->PrintPars(f);\n");
   fSrcSpectrum->GetFreePars()->PrintPars(f);
   fSrcSpectrum->GetFreePars()->SetParVal(0, 1.);
   fSrcSpectrum->GetFreePars()->SetParVal(1, 2.);
   fSrcSpectrum->GetFreePars()->SetParVal(2, -1.);
   fprintf(f, "   > fSrcSpectrum->GetFreePars()->SetParVal(0, 1.);\n");
   fprintf(f, "   > fSrcSpectrum->GetFreePars()->SetParVal(1, 2.);\n");
   fprintf(f, "   > fSrcSpectrum->GetFreePars()->SetParVal(2, -1.);\n");
   fprintf(f, "   > fSrcSpectrum->GetFreePars()->PrintPars(f);\n");
   fSrcSpectrum->GetFreePars()->PrintPars(f);
   fprintf(f, "   > FillSpatialDistFromTemplate(&templ, DUMMY, f_log=f);\n");
   FillSpatialDistFromTemplate(&templ, "DUMMY", f);
   fprintf(f, "   > fSrcSpatialDist->PrintSummary(f);\n");
   fSrcSpatialDist->PrintSummary(f);
   fprintf(f, "   > fSrcSpatialDist->GetFreePars()->PrintPars(f);\n");
   fSrcSpatialDist->GetFreePars()->PrintPars(f);
   fprintf(f, "   ---\n");
   fprintf(f, "   > PrintSrc(f);\n");
   PrintSrc(f);
   fprintf(f, "\n");
   fprintf(f, "\n");

   fprintf(f, " * Calculate spatial distribution and spectrum.\n");
   fprintf(f, "   > TUCoordTXYZ coord_txyz; coord_txyz.SetValTXYZ(1., 2., 0., 3.); coord_txyz.PrintVals(f);\n");
   TUCoordTXYZ coord_txyz;
   coord_txyz.SetValTXYZ(1., 2., 0., 3.);
   coord_txyz.PrintVals(f);
   fprintf(f, "   > TUCoordE coord_e;  ... set to {2., 2., 2....} for all E coordinates...; coord_e.PrintVals(f);\n");
   TUCoordE coord_e;
   vector<string> e_vars;
   TUMisc::String2List(fSrcSpectrum->GetFormulaVars(), ",", e_vars);
   Int_t n_edep = e_vars.size() - 4;
   vector<Double_t> edep_vals(n_edep, 2.);
   coord_e.SetValsE(&edep_vals[0], n_edep, 0, true);
   fprintf(f, "   > \n");
   coord_e.PrintVals(f);
   fprintf(f, "   > ValueSrcSpatialDistrib(&coord_txyz)         =>  %le;\n", ValueSrcSpatialDistrib(&coord_txyz));
   fprintf(f, "   > ValueSrcSpectrum(&coord_e, &coord_txyz)     =>  %le;\n", ValueSrcSpectrum(&coord_e, &coord_txyz));
   fprintf(f, "\n");
   fprintf(f, "\n");


   fprintf(f, " * N.B.: every time ValueXXX() or UpdateFromFreeParsAndResetStatus() is called, update all class members.\n");
   fprintf(f, "   > GetSrcFreePars(is_spect_or_spatdist=true)->GetParListStatus() = %d;\n", GetSrcFreePars(true)->GetParListStatus());
   fprintf(f, "   > GetSrcFreePars(is_spect_or_spatdist=false)->GetParListStatus() = %d;\n", GetSrcFreePars(false)->GetParListStatus());
   fprintf(f, "   > fSrcSpectrum->GetFreePars()->SetParVal(q, 10)\n");
   fSrcSpectrum->GetFreePars()->SetParVal(fSrcSpectrum->GetFreePars()->IndexPar("q"), 10.);
   fprintf(f, "   > fSrcSpectrum->GetFreePars()->PrintPars(f)\n");
   fSrcSpectrum->GetFreePars()->PrintPars(f);
   fprintf(f, "   > GetSrcFreePars(is_spect_or_spatdist=true)->GetParListStatus() = %d;\n", GetSrcFreePars(true)->GetParListStatus());
   fprintf(f, "   > GetSrcFreePars(is_spect_or_spatdist=false)->GetParListStatus() = %d;\n", GetSrcFreePars(false)->GetParListStatus());
   fprintf(f, "   > ValueSrcSpatialDistrib(&coord_txyz)         =>  %le;\n", ValueSrcSpatialDistrib(&coord_txyz));
   fprintf(f, "   > ValueSrcSpectrum(&coord_e, &coord_txyz)     =>  %le;\n", ValueSrcSpectrum(&coord_e, &coord_txyz));
   fprintf(f, "   > GetSrcFreePars(is_spect_or_spatdist=true)->GetParListStatus() = %d;\n", GetSrcFreePars(true)->GetParListStatus());
   fprintf(f, "   > GetSrcFreePars(is_spect_or_spatdist=false)->GetParListStatus() = %d;\n", GetSrcFreePars(false)->GetParListStatus());
   fprintf(f, "   ---\n");
   fprintf(f, "   > fSrcSpectrum->GetFreePars()->SetParVal(q, 2)\n");
   fSrcSpectrum->GetFreePars()->SetParVal(fSrcSpectrum->GetFreePars()->IndexPar("q"), 2.);
   fprintf(f, "   > fSrcSpectrum->GetFreePars()->PrintPars(f)\n");
   fSrcSpectrum->GetFreePars()->PrintPars(f);
   fprintf(f, "   > GetSrcFreePars(is_spect_or_spatdist=true)->GetParListStatus() = %d;\n", GetSrcFreePars(true)->GetParListStatus());
   fprintf(f, "   > GetSrcFreePars(is_spect_or_spatdist=false)->GetParListStatus() = %d;\n", GetSrcFreePars(false)->GetParListStatus());
   fprintf(f, "   > UpdateFromFreeParsAndResetStatus();\n");
   UpdateFromFreeParsAndResetStatus();
   fprintf(f, "   > GetSrcFreePars(is_spect_or_spatdist=true)->GetParListStatus() = %d;\n", GetSrcFreePars(true)->GetParListStatus());
   fprintf(f, "   > GetSrcFreePars(is_spect_or_spatdist=false)->GetParListStatus() = %d;\n", GetSrcFreePars(false)->GetParListStatus());
   fprintf(f, "   > ValueSrcSpatialDistrib(&coord_txyz)         =>  %le;\n", ValueSrcSpatialDistrib(&coord_txyz));
   fprintf(f, "   > ValueSrcSpectrum(&coord_e, &coord_txyz)     =>  %le;\n", ValueSrcSpectrum(&coord_e, &coord_txyz));
   fprintf(f, "\n");
   fprintf(f, "\n");


   fprintf(f, " * Copy class:\n");
   fprintf(f, "   > TUSrcSteadyState src_copy;\n");
   TUSrcSteadyState src_copy;
   fprintf(f, "   > src_copy.Copy(*this);\n");
   src_copy.Copy(*this);
   fprintf(f, "   > src_copy.PrintSrc(f);\n");
   src_copy.PrintSrc(f);
   fprintf(f, "   > src_copy.GetSrcFreePars(is_spect_or_spatdist=true)->PrintPars(f);\n");
   src_copy.GetSrcFreePars(true)->PrintPars(f);
   fprintf(f, "\n");


   fprintf(f, " * Modify parameters:\n");
   fprintf(f, "   > PrintSrc(f);\n");
   PrintSrc(f);
   fprintf(f, "   > TUI_ModifyFreeParameters();  [uncomment in TEST() to enable it]\n");
   //TUI_ModifyFreeParameters();
   fprintf(f, "   > PrintSrc(f);\n");
   PrintSrc(f);
   fprintf(f, "\n");
}

//______________________________________________________________________________
Bool_t TUSrcSteadyState::TUI_ModifyFreeParameters()
{
   //--- Text User Interface to modufy free parameters of this source.

   // Create a temporary TUFreeParList to handle all source parameters
   TUFreeParList tmp_pars;

   // Get free pars for spectrum
   if (fSrcSpectrum)
      tmp_pars.AddPars(fSrcSpectrum->GetFreePars(), true);

   // Get free pars for spectrum
   if (fSrcSpatialDist)
      tmp_pars.AddPars(fSrcSpatialDist->GetFreePars(), true);

   PrintSrc();
   tmp_pars.TUI_Modify();

   if (tmp_pars.GetParListStatus()) {
      UpdateFromFreeParsAndResetStatus();
      return true;
   } else
      return false;
}

//______________________________________________________________________________
void TUSrcSteadyState::UpdateFromFreeParsAndResetStatus()
{
   //--- Updates all formulae from updated fFreePars values and reset fFreePars status.

   // Update spectrum and spatial distribution distribution
   if (fSrcSpectrum)
      fSrcSpectrum->UpdateFromFreeParsAndResetStatus(true);

   if (fSrcSpatialDist)
      fSrcSpatialDist->UpdateFromFreeParsAndResetStatus(true);
}

//______________________________________________________________________________
void TUSrcSteadyState::UseSpectrumOrSpatDist(TUValsTXYZEVirtual *spect_or_spatdist, Bool_t is_spect_or_spatdist)
{
   //--- Uses 'spect_or_spatdist' to describe spectrum or spatial distribution
   //    of this class.
   //  spect_or_spatdist      Object is the spectrum (or spatial distribution to use)
   //  is_spect_or_spatdist   Spectrum or spatial distribution?

   if (is_spect_or_spatdist) {
      if (fIsDeleteSrcSpectrum && fSrcSpectrum)
         delete fSrcSpectrum;
      fSrcSpectrum = spect_or_spatdist;
      fIsDeleteSrcSpectrum = false;
   } else {
      if (fIsDeleteSrcSpatialDist && fSrcSpatialDist)
         delete fSrcSpatialDist;
      fSrcSpatialDist = spect_or_spatdist;
      fIsDeleteSrcSpatialDist = false;
   }
}
