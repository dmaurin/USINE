// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

// C/C++ include
#include <cmath>
#include <cstdlib>
// ROOT include
// USINE include
#include "../include/TUInitParEntry.h"
#include "../include/TUMessages.h"
#include "../include/TUMisc.h"

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TUInitParEntry                                                       //
//                                                                      //
// Initialisation parameter entry (case sensitive) formatted for USINE. //
//                                                                      //
// Any USINE model requires many input parameters, which are used to    //
// initialise the members of USINE classes (TUXXX). A typical USINE     //
// input parameter is read from a 'formatted' line.                     //
//                                                                      //
// Half of the members of the class are used to store and categorise    //
// the entry (which belongs to a @group@subgroup):                      //
//    - Entry group;
//    - Entry subgroup;
//    - Entry type (bool, double, int, string...);
//    - Entry name (case sensitive);
//    - Entry value(s);
//    - Entry unit (if applicable).
//                                                                      //
// The other half of the class members is used to decide how to         //
// display and interact with the entry value in the USINE graphical     //
// user interface (see link below):                                     //
//    - Is the value browsable (e.g., it is a file)?
//    - Is the entry hidden (unless you are expert)?
//    - Info/help about the entry...
//                                                                      //
//////////////////////////////////////////////////////////////////////////

ClassImp(TUInitParEntry)

//______________________________________________________________________________
TUInitParEntry::TUInitParEntry()
{
   // ****** Default constructor ******
   //
}

//______________________________________________________________________________
TUInitParEntry::TUInitParEntry(TUInitParEntry const &par)
{
   // ****** Copy constructor ******

   Copy(par);
}


//______________________________________________________________________________
TUInitParEntry::~TUInitParEntry()
{
   // ****** Default destructor ******
   //
}


//______________________________________________________________________________
void TUInitParEntry::Copy(TUInitParEntry const &par)
{
   //--- Copies entry in class.
   //  par               Object to copy from

   fGroup = par.GetGroup();
   fIsMultiVal = par.IsMultiVal();
   fName = par.GetName();
   fSubGroup = par.GetSubGroup();
   fVal.clear();
   for (Int_t i = 0; i < par.GetNVals(); ++i)
      fVal.push_back(par.GetVal(i));
}

//______________________________________________________________________________
Bool_t TUInitParEntry::IsSame(TUInitParEntry *par) const
{
   //--- Checks whether "par" is same parameter as current entry (i.e. same group,
   //    subgroup, type and name).
   //  par               TUInitParEntry object to copy

   if (par && (par->GetGroup() == fGroup) && (par->GetSubGroup() == fSubGroup)
         && (par->GetName() == fName))
      return true;
   else
      return false;
}

//______________________________________________________________________________
Bool_t TUInitParEntry::IsSame(string const &group, string const &subgroup,
                              string const &name) const
{
   //--- Checks whether the combination group@subgroup@name matches that of
   //    the current entry.
   //  group             Name of group to check
   //  subgroup          Name of subgroup (in group) to check
   //  name              Name of parameter (in group@subgroup) to check

   if ((fGroup == group) && (fSubGroup == subgroup) && (fName == name))
      return true;
   else
      return false;
}

//______________________________________________________________________________
void TUInitParEntry::Print(FILE *f) const
{
   //--- Prints in FILE *f, according to the format (space insensitive):
   //      group @ subgroup @ param @ M=? @ value
   //  f                 Output file

   for (Int_t i = 0; i < GetNVals(); ++i) {
      fprintf(f, "%s@%s@%s@M=%1d@%s\n",
              fGroup.c_str(), fSubGroup.c_str(), fName.c_str(),
              fIsMultiVal, fVal[i].c_str());
   }
}

//______________________________________________________________________________
void TUInitParEntry::SetEntry(string const &line_entry, string const &info_id)
{
   //--- Sets entry from a line read, which should have the following format
   //   (@ separated, as many spaces as desired):
   //       group@subgroup@param@M=?@value
   //  line_entry        String of the entry line to use
   //  info_id           Line number of line_entry, if come from a file read by TUInitParList

   fVal.clear();

   // Remove blanck spaces from line start/stop
   if (line_entry.find_first_not_of(" \t") == string::npos) return;
   string current_line = TUMisc::RemoveBlancksFromStartStop(line_entry);
   if (current_line[0] == '#')
      return;

   // Check line format
   vector<string> l_str;
   TUMisc::String2List(current_line, "@", l_str);
   if (l_str.size() != 5) {
      string message = (string)"Ill-defined format: a line should be\n"
                       + (string)"   group@subgroup@param@M=0/1@value\n"
                       + (string)"whereas in " + info_id + ", you used\n   "
                       + line_entry;
      TUMessages::Error(stdout, "TUInitParEntry", "SetEntry", message);
   }

   // Fill entry
   for (Int_t i = 0; i < (Int_t)l_str.size(); ++i) {
      if (l_str[i].find_first_not_of(" ") == string::npos
            || l_str[i].find_first_not_of("\t") == string::npos) {
         string message = "In line " + info_id + "\n   " + line_entry
                          + "\nmissing value for entry @" + (string)Form("%d", i)
                          + ": cannot proceed!";
         TUMessages::Error(stdout, "TUInitParEntry", "SetEntry", message);
      }
      if (i == 0)      fGroup = l_str[i];
      else if (i == 1) fSubGroup = l_str[i];
      else if (i == 2) fName = l_str[i];
      else if (i == 4) fVal.push_back(l_str[i]);
      else {
         string str_val = l_str[i].substr(l_str[i].find_first_of("=") + 1, l_str[i].size());
         if (str_val.find_first_not_of(" ") == string::npos) {
            string message = "In line " + info_id + "\n   " + line_entry
                             + "\n  => Missing value for entry @" + (string)Form("%d", i + 1)
                             + ": cannot proceed!";
            TUMessages::Error(stdout, "TUInitParEntry", "SetEntry", message);
         }
         if (i == 3) fIsMultiVal = atoi(str_val.c_str());
      }
   }
}

//______________________________________________________________________________
void TUInitParEntry::TEST(FILE *f)
{
   //--- Test function for this class (called in usine -t).
   //  f                 Output file for test

   string message = "Format of a parameter entry used for USINE initialisation.";
   TUMessages::Test(f, "TUInitParEntry", message);

   // Set entry
   string group = "LBModel";
   string subgroup = "ISM";
   //    group@subgroup@param@type@M=multival?@unit@val@H=hidden?@B=browsable?@Info
   string line = group + "@" + subgroup
                 + "@DensityH@M=0@0.9";
   fprintf(f, " * Fill class:\n");
   fprintf(f, "   > SetEntry(line);  //[with \'line\' formatted as in $USINE/inputs/init.TEST.par]\n");
   SetEntry(line);
   fprintf(f, "   > Print(f);\n");
   Print(f);
   fprintf(f, "\n");

   // Create copy, and check comparison methods
   fprintf(f, " * Create an entry copy \'par_ref\', and change either the group,"
           " subgroup, type, or name, of current entry for comparison methods"
           " between two entries:\n");
   string name_new = "densityh";

   fprintf(f, "   > TUInitParEntry par_ref;\n");
   TUInitParEntry par_ref;
   fprintf(f, "   > par_ref.Copy(*this);\n");
   par_ref.Copy(*this);
   fprintf(f, "   > par_ref.Print(f)\n");
   par_ref.Print(f);
   fprintf(f, "   > IsSame(&par_ref)=%d\n", IsSame(&par_ref));

   fprintf(f, "\n");
   Copy(par_ref);
   fprintf(f, " * Reset current entry (to ref) and change parameter \'name\':\n");
   fprintf(f, "   > Print(f);\n");
   Print(f);
   fprintf(f, "   > fName = name_new =%s:\n", name_new.c_str());
   fName = name_new;
   fprintf(f, "   > Print(f);\n");
   Print(f);
   fprintf(f, "   > IsSame(&par_ref)=%d\n", IsSame(&par_ref));
   fprintf(f, "\n");
   fprintf(f, "\n");


   // Check methods for multi-valued entries (i.e. one entry has several values)
   string multi = "Propag@Model0DLeakBox@ISM@M=1@HI:1:0.9";
   string val2 = "HII:1:0.033";
   string val3 = "H2:1:0.";
   string val4 = "He:1:0.1";
   fprintf(f, " * Create a multi-valued entry (option \'M=1\' in $USINE/inputs/init.TEST.par)"
           " and check multi-dependent methods\n");
   fprintf(f, "  multi=%s\n", multi.c_str());
   fprintf(f, "   > SetEntry(multi);\n");
   SetEntry(multi);
   fprintf(f, "   > Print(f);\n");
   Print(f);
   fprintf(f, "   > AddVal(%s);\n", val2.c_str());
   AddVal(val2);
   fprintf(f, "   > Print(f);\n");
   Print(f);
   fprintf(f, "   > AddVal(%s);\n", val3.c_str());
   AddVal(val3);
   fprintf(f, "   > AddVal(%s);\n", val4.c_str());
   AddVal(val4);
   fprintf(f, "   > Print(f);\n");
   Print(f);
   fprintf(f, "\n");
}
