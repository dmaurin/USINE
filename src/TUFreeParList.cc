// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

// C/C++ include
#include <iostream>
#include <fstream>
// ROOT include
// USINE include
#include "../include/TUFreeParList.h"
#include "../include/TUMessages.h"
#include "../include/TUMisc.h"

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TUFreeParList                                                        //
//                                                                      //
// List of free parameters (based on TUFreeParEntry).                   //
//                                                                      //
// The class TUFreeParList provides a list of free parameters           //
// (vector of TUFreeParEntry) that can change at runtime:               //
//    - name (case sensitive);
//    - value;
//    - range;
//    - best-fit;
//    - error.
// The status of each parameter is monitored: a specific flag is set    //
// whenever the value of at least one parameter in the list is          //
// changed (updated). This information is used, e.g., for all classes   //
// based on a formula (from TUValsTXYZEFormula), where it is checked,   //
// each time a calculation is performed, that the formula is updated    //
// (from the updated parameter values) beforehand.                      //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

ClassImp(TUFreeParList)

//______________________________________________________________________________
TUFreeParList::TUFreeParList()
{
   // ****** normal constructor ******

   Initialise(false);
}

//______________________________________________________________________________
TUFreeParList::TUFreeParList(TUFreeParList const &free_pars)
{
   // ****** Copy constructor ******
   //  free_pars            Object to copy

   Initialise(false);
   Copy(free_pars);
}

//______________________________________________________________________________
TUFreeParList::~TUFreeParList()
{
   // ****** Default destructor ******

   Initialise(true);
}

//______________________________________________________________________________
void TUFreeParList::AddPar(TUFreeParEntry *par, Bool_t is_use_or_copy)
{
   //--- Adds (points to or copy of) entry in the list fPars: aborts if already in list.
   //  par               Model parameter to add (by creating a new pointer)
   //  is_use_or_copy    Whether to point to or copy new entry 'par'

   // If parameter name is already in list, skip it!
   if (IndexPar(par->GetName()) >= 0)
      return;

   Int_t n_pars = GetNPars();
   fPars.resize(n_pars + 1);

   // If use, or if copy
   if (is_use_or_copy) {
      fPars[n_pars] = par;
      fIsDeletePar.push_back(false);
      UpdateParListStatusFromIndividualStatus();
   } else {
      // Create a new pointer which is a copy of 'par' and add to fPars
      fPars[n_pars] = par->Clone();
      fIsDeletePar.push_back(true);
      fParListStatus = true;
   }
}

//______________________________________________________________________________
void TUFreeParList::AddPar(string name, string unit, Double_t val)
{
   //--- Adds free parameter (based on TUFreeParEntry::SetPar() method).
   //  name              Name (case sensitive)
   //  unit              Unit (default set to [-], i.e. unitless)
   //  val               Entry value

   // Create and set par entry
   TUFreeParEntry par;
   par.SetPar(name, unit, val);

   // Add in fPars
   AddPar(&par, false);
}

//______________________________________________________________________________
void TUFreeParList::AddPars(TUFreeParList *pars, Bool_t is_use_or_copy)
{
   //--- Add pars in current list.
   //  pars              List of parameters to add
   //  is_use_or_copy    Whether to point to or copy new entry 'par'

   if (!pars)
      return;

   for (Int_t i_par = 0; i_par < pars->GetNPars(); ++i_par)
      AddPar(pars->GetParEntry(i_par), is_use_or_copy);
}

//______________________________________________________________________________
Bool_t TUFreeParList::AddPars(TUInitParList *init_pars, string group, string subgroup,
                              Bool_t is_verbose, FILE *f_log)
{
   //--- Adds initialisation parameters 'init_pars' (belonging to Group#Subgroup),
   //    providing that parameters in #group#subgroup contains the following
   //    parameters:
   //          - ParNames    [mandatory]
   //          - ParUnits    [mandatory]
   //          - ParVals     [optional]
   //    N.B.: returns false if no free parameters found.
   //
   //  init_pars         Initialisation parameters
   //  group             Group name
   //  subgroup          Subroup name
   //  is_verbose        Verbose or not for parameters loaded?
   //  f_log             Log for USINE

   string indent = TUMessages::Indent(true);
   if (is_verbose) {
      fprintf(f_log, "%s[TUFreeParList::AddPars]\n", indent.c_str());
      fprintf(f_log, "%s### Search for free parameters [%s#%s]\n",
              indent.c_str(), group.c_str(), subgroup.c_str());
   }

   // Extract names, units and vals from parfile
   const Int_t n = 3;
   string par_types[n] = {"ParNames", "ParUnits", "ParVals"};
   Int_t is_partype[n];
   vector<string> params[n];

   // Search for free parameters
   if (init_pars->IndexPar(group, subgroup, par_types[0]) < 0) {
      if (is_verbose)
         fprintf(f_log, "%s[TUFreeParList::AddPars] <DONE>\n\n", indent.c_str());
      TUMessages::Indent(false);
      return false;
   } else if (init_pars->GetParEntry(init_pars->IndexPar(group, subgroup, par_types[0])).GetVal() == "-") {
      if (is_verbose)
         fprintf(f_log, "%s[TUFreeParList::AddPars] <DONE>\n\n", indent.c_str());
      TUMessages::Indent(false);
      return false;
   }

   // String to lists
   string ref_names = "";
   for (Int_t i = 0; i < n; ++i) {
      // Check index par exist for mandatory parameters, skip otherwise
      Int_t i_par = init_pars->IndexPar(group, subgroup, par_types[i], false);
      is_partype[i] = true;
      if (i_par < 0) {
         is_partype[i] = false;
         if (n == 1) {
            string message = "No parameter found for #" + group + "#" + subgroup + "#" + par_types[n]
                             + " in" + init_pars->GetFileNames();
            TUMessages::Error(f_log, "TUFreeParList", "AddPars", message);
         } else
            continue;
      }

      // Extract parameter
      string tmp2 = init_pars->GetParEntry(init_pars->IndexPar(group, subgroup, par_types[i])).GetVal();
      string tmp = TUMisc::RemoveChars(tmp2, " \t");
      if (i == 0) ref_names = tmp;

      // Extract values
      TUMisc::String2List(tmp, ",", params[i]);

      // Check if same number of entries for ParNames, ParUnits, and ParVals
      if (params[i].size() != params[0].size()) {
         string message = "Not the same numbers of entries in "
                          + par_types[0] + " (=" + ref_names + ") and in "
                          + par_types[i] + " (=" + tmp + ")";
         TUMessages::Error(f_log, "TUFreeParList", "AddPars", message);
      }
   }
   // Fill class and print: loop on number of free parameters,
   // which is given by the number of parameters found in par[0]
   // (i.e. corresponding to the number of comma-separated names
   // found in name_ParNames)
   for (Int_t i = 0; i < (Int_t)params[0].size(); ++i) {
      if (IndexPar(params[0][i]) >= 0)
         continue;

      TUFreeParEntry entry;
      Double_t val = 0.;
      if (is_partype[2])
         val = atof((params[2][i]).c_str());
      entry.SetPar(params[0][i], params[1][i], val);
      AddPar(&entry, false);

      if (is_verbose) {
         Int_t j = GetNPars() - 1;
         TUFreeParEntry *par = GetParEntry(j);
         fprintf(f_log, "%s      - %-15s set to %.3le %-15s \n", indent.c_str(),
                 par->GetName().c_str(), par->GetVal(false), par->GetUnit(true).c_str());
         par = NULL;
      }
   }
   if (is_verbose)
      fprintf(f_log, "%s[TUFreeParList::AddPars] <DONE>\n\n", indent.c_str());
   TUMessages::Indent(false);
   return true;
}

//______________________________________________________________________________
void TUFreeParList::CheckMatchingParNamesAndCovParNames(FILE *f_log)
{
   //--- Check that same names in fPars (read from best-fit) fFitCovParNames (read from covariance matrix).
   //  f_log             Log for USINE

   // If all empty, OK
   if (GetNPars() == 0 && GetNCovPars() == 0)
      return;


   // Number and names
   string message = Form("%d fit names: %s\n%d cov names: %s",
                         GetNPars(), TUMisc::List2String(fFitCovParNames, ',').c_str(),
                         GetNCovPars(), GetParNames().c_str());

   // If not same number, aborts
   if (GetNPars() != GetNCovPars())
      TUMessages::Error(f_log, "TUFreeParList", "CheckMatchingParNamesAndCovParNames", message);

   // If not same names/ordering, aborts
   for (Int_t i = 0; i < GetNPars(); ++i) {
      if (GetFitCovParName(i) != GetParEntry(i)->GetFitName()) {
         message += Form("\n\n   => Not same name for parameter %d (%s vs %s)\n   => reorder lines in fit file if it is an ordering issue",
                         i, GetFitCovParName(i).c_str(), GetParEntry(i)->GetFitName().c_str());
         TUMessages::Error(f_log, "TUFreeParList", "CheckMatchingParNamesAndCovParNames", message);
      }
   }
}

//______________________________________________________________________________
void TUFreeParList::Copy(TUFreeParList const &free_pars)
{
   //--- Copies free_pars in current class.
   //  free_pars         Free param list to copy from/point to

   Initialise(true);

   for (Int_t i = 0; i < free_pars.GetNPars(); ++i) {
      fIsDeletePar.push_back(true);
      TUFreeParEntry *par = free_pars.GetParEntry(i)->Clone();
      fPars.push_back(par);
      par = NULL;
   }
   fParListStatus = true;
}

//______________________________________________________________________________
string TUFreeParList::GetParNames(Int_t i_start, Int_t i_stop) const
{
   //--- Returns comma-separated list of all names in list (between i_start & i_stop).
   //  i_start           Index of first param. to write
   //  i_stop            Index of last param. to write

   if (GetNPars() == 0) return "";

   if (i_start > i_stop) {
      Int_t tmp = i_start;
      i_start = i_stop;
      i_stop = tmp;
   }
   if (i_start < 0) i_start = 0;
   if (i_stop > GetNPars() - 1) i_stop = GetNPars() - 1;
   Int_t n_pars = i_stop - i_start + 1;
   if (n_pars == 0) return "";

   string names;
   names = fPars[i_start]->GetName();
   for (Int_t i = i_start + 1; i <= i_stop; ++i)
      names = names + "," + fPars[i]->GetName();
   return names;
}

//______________________________________________________________________________
Int_t TUFreeParList::IndexInExtClassToIndexPar(Int_t i_ext) const
{
   //--- Returns parameter index if it matches IndexInExtClass() (-1 if not found).

   for (Int_t i_par = 0; i_par < GetNPars(); ++i_par) {
      if (fPars[i_par]->IndexInExtClass() == i_ext)
         return i_par;
   }

   return -1;
}

//______________________________________________________________________________
Int_t TUFreeParList::IndexPar(string const &par_name) const
{
   //--- Finds index of par_name in list (return -1 if not present).
   //  par_name          Model parameter name to find

   for (Int_t i = 0; i < GetNPars(); ++i) {
      if (par_name == fPars[i]->GetName())
         return i;
   }
   return -1;
}

//______________________________________________________________________________
vector<Int_t> TUFreeParList::IndicesPars(gENUM_FREEPARTYPE par_type) const
{
   //--- Returns list if indices for parameters having type 'par_type'.
   //  par_type          Type of parameter selected (FIT, FIXED, NUISANCE)

   vector<Int_t> indices;

   // Loop on all par and add matching ones
   for (Int_t i = 0; i < GetNPars(); ++i) {
      if (fPars[i]->GetFitType() == par_type)
         indices.push_back(i);
   }

   return indices;
}

//______________________________________________________________________________
vector<Int_t> TUFreeParList::IndicesValUpdated() const
{
   //--- Returns list of indices for parameters whose value has been updated.

   vector<Int_t> indices;
   for (Int_t i = 0; i < GetNPars(); ++i) {
      if (fPars[i]->GetStatus())
         indices.push_back(i);
   }
   return indices;
}

//______________________________________________________________________________
void TUFreeParList::Initialise(Bool_t is_delete)
{
   //--- Deletes/Initialise members.
   //  is_delete         Whether to delete or only initialise

   if (is_delete) {
      for (Int_t i = 0; i < GetNPars(); ++i) {
         if (fIsDeletePar[i] && fPars[i]) delete fPars[i];
         fPars[i] = NULL;
      }
   }

   fParListStatus = false;
   fIsDeletePar.clear();
   fPars.clear();
}

//______________________________________________________________________________
void TUFreeParList::LoadFitCovMatrix(string const &file_fitcov, vector<Int_t> const &indices, Bool_t is_verbose, FILE *f_log)
{
   //--- Loads best-fit parameters from USINE file (fit_result.out).
   // INPUTS:
   //  file_fitcov       Name of file to read (e.g., fit_covmatrix.out after a fit)
   //  indices           Indices to load (load all if empty)
   //  is_verbose        Verbose or not
   //  f_log             Log for USINE

   // If file does not exist, nothing to do
   ifstream f_read(TUMisc::GetPath(file_fitcov).c_str());
   if (!TUMisc::IsFileExist(f_read, file_fitcov, true))
      return;

   // Reset
   fFitCovMatrix.Clear();
   fFitCovParNames.clear();

   string indent = TUMessages::Indent(true);
   if (is_verbose)
      fprintf(f_log, "%s[TUFreeParList::LoadFitCovMatrix]\n", indent.c_str());

   // Read file (if reach this point, file exists)
   string line = "";
   Bool_t is_first_line = true;
   Int_t i_row = 0, n_pars = 0, n_fill = 0;
   vector<string> all_parnames;

   while (getline(f_read, line)) {
      // if it is a comment (start with #) or a blank line, skip it
      string line_trimmed = TUMisc::RemoveBlancksFromStartStop(line);
      if (line_trimmed[0] == '#' || line_trimmed.empty())
         continue;

      vector<string> split_line;
      TUMisc::String2List(line, " ", split_line);

      // First line contains parameter names
      if (is_first_line) {
         n_pars = split_line.size();
         all_parnames = split_line;
         // If no selection, use all parameters
         if (indices.size() == 0) {
            fFitCovParNames = split_line;
            n_fill = n_pars;
         } else {
            // If selection, use only those
            for (Int_t k = 0; k < n_pars; ++k) {
               if (TUMisc::IsInList(indices, k))
                  fFitCovParNames.push_back(split_line[k]);
            }
            n_fill = fFitCovParNames.size();
         }
         fFitCovMatrix.ResizeTo(n_fill, n_fill);
         is_first_line = false;
      } else {
         // Find corresponding row, and skip if not to fill
         Int_t i_row2fill = i_row;
         if (indices.size() != 0) {
            i_row2fill = TUMisc::IndexInList(indices, i_row);
            if (i_row2fill < 0) {
               ++i_row;
               continue;
            }
         }

         // Check content/size of all subsequent lines
         if (split_line[0] != all_parnames[i_row]) {
            string message = Form("Cannot proceed, not same name for parameter #%d for row (%s) and column (%s)",
                                  i_row, all_parnames[i_row].c_str(), split_line[0].c_str());
            TUMessages::Error(f_log, "TUFreeParList", "LoadFitCovMatrix", message);
         }
         if (n_pars != (Int_t)split_line.size() - 1) {
            string message = Form("Cannot proceed, expected number of entries is %d, while %d found in line %s",
                                  n_pars, (Int_t)split_line.size() - 1, line_trimmed.c_str());
            TUMessages::Error(f_log, "TUFreeParList", "LoadFitCovMatrix", message);
         }

         // Fill covariance matrix (N.B.: i_col=0 is param name!)
         for (Int_t i_col = i_row; i_col < n_pars; i_col++) {
            // Find corresponding col, and skip if not to fill
            Int_t i_col2fill = i_col;
            if (indices.size() != 0) {
               i_col2fill = TUMisc::IndexInList(indices, i_col);
               if (i_col2fill < 0)
                  continue;
            }

            // Fill lower/upper triangle
            Double_t val = atof(split_line[i_col + 1].c_str());
            fFitCovMatrix(i_row2fill, i_col2fill) = val;
            fFitCovMatrix(i_col2fill, i_row2fill) = val;
         }

         // Got to next
         ++i_row;
      }
   }

   if (is_verbose)
      fprintf(f_log, "%s[TUFreeParList::LoadFitCovMatrix] <DONE>\n\n", indent.c_str());
   TUMessages::Indent(false);
   return;
}

//______________________________________________________________________________
void TUFreeParList::LoadFitValues(string const &file_fitvals, vector<Int_t> const &indices, Bool_t is_skipped_fixed, Bool_t is_verbose, FILE *f_log)
{
   //--- Loads best-fit parameters from USINE file (fit_result.out).
   // INPUTS:
   //  file_fitvals      Name of file to read (e.g. fit_result.out after a fit)
   //  indices           Indices to load (load all if empty)
   //  is_skipped_fixed  To skip FIXED parameters (to use when used with covariance matrix)
   //  is_verbose        Verbose or not
   //  f_log             Log for USINE

   // If file does not exist, nothing to do
   ifstream f_read(TUMisc::GetPath(file_fitvals).c_str());
   if (!TUMisc::IsFileExist(f_read, file_fitvals, true))
      return;

   // Initialise class
   Initialise(true);

   string indent = TUMessages::Indent(true);

   if (is_verbose)
      fprintf(f_log, "%s[TUFreeParList::LoadFitValues]\n", indent.c_str());

   // Read file (if reach this point, file exists)
   string line = "";
   Int_t index = 0;
   while (getline(f_read, line)) {
      // if it is a comment (start with #) or a blank line, skip it
      string line_trimmed = TUMisc::RemoveBlancksFromStartStop(line);
      if (line_trimmed[0] == '#' || line_trimmed.empty())
         continue;

      // Found lines for best-fit parameters
      // Check correctly formatted
      vector<string> dummy;
      TUMisc::String2List(line, " =", dummy);
      //par_names.push_back(TUMisc::RemoveBlancksFromStartStop(dummy_name[0]));
      //par_vals.push_back(atof(TUMisc::RemoveBlancksFromStartStop(dummy_name[1]).c_str()));
      if (dummy[0].find("chi2_min") != string::npos)
         continue;

      TUFreeParEntry par;
      Double_t fitval = 0., fitval_err = 0.;
      Double_t fitval_err_lo = 0., fitval_err_hi = 0.;
      Int_t n_remove = 0;
      string fit_name = dummy[0], unit;
      gENUM_AXISTYPE sampling;
      gENUM_FREEPARTYPE type;
      par.ExtractFromFitName(fit_name, sampling, type);
      par.SetFitType(type);
      par.SetFitSampling(sampling);

      // Extract init pars (passing 'type,sampling,[min,max],fitval,sigma')
      string par_init = TUEnum::Enum2Name(type) + "," +  TUEnum::Enum2Name(sampling) + "," + dummy[dummy.size() - 1];
      par.SetFitInit(par_init, f_log);
      // Extract unit, fitvalues and errors for kFIT, kNUISANCE, and kFIXED
      //  => 3 possible syntaxes
      //   TOTO_FIXED = 1.600686 [kpc^2/Myr] from initial setting K0= -1.660586e+00
      //   TOTO_xxx = +1.372682e-07(+/-5.90e-02) [-] from  initial setting LCInelBar94_16O+H=[+0.0e+00,+1.5e+00],+8.0e-01,+1.0e-01
      //   TOTO_xxx = +1.372682e-07(+/-5.90e-02, asymm=-2.34e-2,+2.55e-2) [-] from  initial setting LCInelBar94_16O+H=[+0.0e+00,+1.5e+00],+8.0e-01,+1.0e-01

      if (par.GetFitType() == kFIT || par.GetFitType() == kNUISANCE) {
         size_t pos = dummy[1].find_first_of("(");
         fitval = atof(dummy[1].substr(0, pos).c_str());
         dummy[1].erase(dummy[1].begin(), dummy[1].begin() + pos + 3);
         vector<string> tmp;
         TUMisc::String2List(dummy[1], "(/,=)", tmp);
         fitval_err = fabs(atof(tmp[0].c_str()));
         if (line.find("asymm=") != string::npos) {
            unit = TUMisc::RemoveChars(dummy[4], "[]");
            tmp.clear();
            TUMisc::String2List(dummy[3], ",)", tmp);
            fitval_err_lo = atof(tmp[0].c_str());
            fitval_err_hi = atof(tmp[1].c_str());
         } else
            unit = TUMisc::RemoveChars(dummy[2], "[]");
      } else if (par.GetFitType() == kFIXED) {
         size_t pos = dummy[1].find_first_of(" ");
         fitval = atof(dummy[1].substr(0, pos).c_str());
      } else
         TUMessages::Error(f_log, "TUFreeParList", "LoadFitValues", "Not defined if type not kFIT, kFIXED, or kNUISANCE");

      // Fill: we need to convert fitval to val correctly (depending on sampling)
      par.SetPar(fit_name, unit, fitval);
      par.SetVal(fitval, par.GetFitSampling());
      par.SetFitValBest(fitval);
      par.SetFitValErr(fitval_err);
      par.SetFitValErrAsymm(fitval_err_lo, fitval_err_hi);
      par.SetStatus(true);

      // If kFIXED and not desired, skip it
      if (is_skipped_fixed && par.GetFitType() == kFIXED)
         continue;

      // Add only if in index list (if not empty)
      if (indices.size() == 0 || TUMisc::IsInList(indices, index))
         AddPar(&par, false);
      ++index;
   }

   if (is_verbose)
      fprintf(f_log, "%s[TUFreeParList::LoadFitValues] <DONE>\n\n", indent.c_str());
   TUMessages::Indent(false);
   return;
}

//______________________________________________________________________________
string TUFreeParList::ParsNameValUnit(Int_t uid0_base1_fit2) const
{
   //--- Forms a string from parameter name, value, and unit.
   //  uid0_base1_fit2  Option to form string for the parameter
   //                       0: unique ID to use, e.g., in file names
   //                       1: comma-separated list of current value
   //                       2: comma-separated list of best fit value and error

   string id = "";
   for (Int_t i = 0; i < GetNPars(); ++i) {
      string current_id = fPars[i]->ParNameValUnit(uid0_base1_fit2);
      if (i < GetNPars() - 1) {
         if (uid0_base1_fit2 == 0)
            current_id = current_id + "_";
         else
            current_id = current_id + "; ";

         id = id + current_id;

      } else
         id = id + current_id;
   }

   if (id == "")
      id = "nofreepars";
   return id;
}

//______________________________________________________________________________
void TUFreeParList::PrintPars(FILE *f, gENUM_FREEPARTYPE par_type) const
{
   //--- Prints in file f all parameters of a given type (e.g., kFIT, kFIXED, kNUISANCE).
   //  f                 File in which to print
   //  par_type          Type of free parameters (kFIT, kFIXED, or kNUISANCE)

   string indent = TUMessages::Indent(true);

   for (Int_t i = 0; i < GetNPars(); ++i) {
      if (par_type == kANY || par_type == fPars[i]->GetFitType())
         fPars[i]->Print(f);
   }

   if (GetNPars(par_type) == 0)
      fprintf(f, "%s   => No %s parameters\n", indent.c_str(), TUEnum::Enum2Name(par_type).c_str());

   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUFreeParList::PrintPars(FILE *f, Int_t i_start, Int_t i_stop) const
{
   //--- Prints in file f.
   //  f                 File in which to print
   //  i_start           Index of first param. to write
   //  i_stop            Index of last param. to write

   string indent = TUMessages::Indent(true);

   if (i_start > i_stop) {
      Int_t tmp = i_start;
      i_start = i_stop;
      i_stop = tmp;
   }
   if (i_start < 0) i_start = 0;
   if (i_stop > GetNPars() - 1) i_stop = GetNPars() - 1;

   Int_t n_pars = i_stop - i_start + 1;
   if (n_pars > 0 && GetNPars() > 0) {
      if (n_pars == 1) fprintf(f, "%s   => 1 free parameter:\n", indent.c_str());
      else fprintf(f, "%s   => %d free parameters:\n", indent.c_str(), n_pars);

      for (Int_t i = i_start; i <= i_stop; ++i)
         fPars[i]->Print(f);
   } else
      fprintf(f, "%s   => No free parameters\n", indent.c_str());

   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUFreeParList::PrintFitCovMatrix(FILE *f) const
{
   //--- Prints in file f content of fFitCovMatrix.
   //  f                 File in which to print

   // Print matrix
   for (Int_t i = 0; i < GetNCovPars(); ++i) {
      // Print names (first line)
      if (i == 0) {
         fprintf(f, "                                                ");
         for (Int_t j = 0; j < GetNCovPars(); ++j)
            fprintf(f, "%-16s ", GetFitCovParName(j).c_str());
         fprintf(f, "\n");
      }
      // Print values
      fprintf(f, "%-45s", GetFitCovParName(i).c_str());
      for (Int_t j = 0; j < GetNCovPars(); ++j)
         fprintf(f, "  %+le  ", GetFitCovMatrix(i, j));
      fprintf(f, "\n");
   }
}

//______________________________________________________________________________
void TUFreeParList::PrintFitRelated(FILE *f, Int_t all0_fitonly1_initonly2, Int_t i_start, Int_t i_stop) const
{
   //--- Prints in file f init. values for fit and best-fit values (if is_result=true).
   //  f                       File in which to print
   //  all0_fitonly1_initonly2 Whether to print
   //                             0: fit and init values
   //                             1: fit values only
   //                             2: init values only
   //  i_start                 Index of first param. to write
   //  i_stop                  Index of last param. to write

   string indent = TUMessages::Indent(true);
   TUMessages::Indent(false);

   if (all0_fitonly1_initonly2 == 0)
      fprintf(f, "#  => Best-fit parameters after minimisation\n");
   else if (all0_fitonly1_initonly2 == 2)
      fprintf(f, "%s  - Fit parameter initial setup: [min,max],init,step [unit]\n", indent.c_str());

   if (i_start > i_stop) {
      Int_t tmp = i_start;
      i_start = i_stop;
      i_stop = tmp;
   }
   if (i_start < 0) i_start = 0;
   if (i_stop > GetNPars() - 1) i_stop = GetNPars() - 1;

   Int_t n_pars = i_stop - i_start + 1;
   if (n_pars > 0 && GetNPars() > 0) {
      for (Int_t i = i_start; i <= i_stop; ++i) {
         if (all0_fitonly1_initonly2 == 0)
            fPars[i]->PrintFitValsAndInit(f);
         else if (all0_fitonly1_initonly2 == 1)
            fPars[i]->PrintFitVals(f);
         else if (all0_fitonly1_initonly2 == 2)
            fPars[i]->PrintFitInit(f);
      }
   } else
      fprintf(f, "%s   => No free parameters\n", indent.c_str());
}

//______________________________________________________________________________
void TUFreeParList::ResetToReference(TUFreeParList *pars_ref, FILE *f_print)
{
   //--- Resets current parameters to pars_ref values (copy all parameters). Abort if pars_ref does not match current parameter
   //  pars_sref         Reference parameter values
   //  f_print           File in which to print

   // Must have same number of parameters
   if (GetNPars() != pars_ref->GetNPars()) {
      string message = Form("cannot proceed, not the same number of parameters (%d vd %d) in pars_ref",
                            GetNPars(), pars_ref->GetNPars());
      TUMessages::Error(f_print, "TUFreeParList", "ResetToReference", message);
   }

   for (Int_t i = 0; i < GetNPars(); ++i) {
      TUFreeParEntry *par_ref = pars_ref->GetParEntry(i);

      // If not same parameter, abort
      if (!fPars[i]->IsSamePar(*par_ref)) {
         string message = Form("par[#%d]=%s differs from par_ref[#%d]=%s, cannot proceed",
                               i, fPars[i]->GetFitName().c_str(), i, par_ref->GetFitName().c_str());
         TUMessages::Error(f_print, "TUFreeParList", "ResetToReference", message);
      }

      fPars[i]->SetFitValBest(par_ref->GetFitValBest());
      fPars[i]->SetFitValErr(par_ref->GetFitValErr());
      fPars[i]->SetFitValErrAsymm(par_ref->GetFitValErrAsymmLo(), par_ref->GetFitValErrAsymmUp());
      fPars[i]->SetVal(par_ref->GetVal(false), false);

      par_ref = NULL;
   }
}

//______________________________________________________________________________
void TUFreeParList::TEST(TUInitParList *init_pars, FILE *f)
{
   //--- Test function for this class (called in usine -t).
   //  init_pars         TUInitParList class of initialisation parameters
   //  f                 Output file for test

   string message = "Handles free parameters for a model. Two sets of\n"
                    "free parameters can be combined (still pointing to the original\n"
                    "parameters), updated (useful, e.g. is accessed from a minimisation\n"
                    "method), or printed...";
   TUMessages::Test(f, "TUFreeParList", message);

   // Set class and test methods
   fprintf(f, " * Base class:\n");
   fprintf(f, "   > AddPars(init_pars=\"%s\", group=Model0DLeakyBox, SUBGROUP=Transport, is_verbose=true, f_log=f);\n",
           init_pars->GetFileNames().c_str());
   AddPars(init_pars, "Model0DLeakyBox", "Transport", true, f);
   fprintf(f, "   > PrintPars(f);\n");
   PrintPars(f);
   fprintf(f, "   > Initialise(true);\n");
   Initialise(true);
   fprintf(f, "   > AddPars(init_pars, group=Model0DLeakyBox, SUBGROUP=Transport, is_verbose=false, f_log=f);\n");
   AddPars(init_pars, "Model0DLeakyBox", "Transport", false, f);
   fprintf(f, "   > PrintPars(f);\n");
   PrintPars(f);
   fprintf(f, "   > PrintPars(f, kFIT);\n");
   PrintPars(f, kFIT);
   fprintf(f, "   > IndicesPars(kFIT) = %s;\n", TUMisc::List2String(IndicesPars(kFIT), ',').c_str());
   fprintf(f, "   > PrintPars(f, kFIXED);\n");
   PrintPars(f, kFIXED);
   fprintf(f, "   > IndicesPars(kFIXED) = %s;\n", TUMisc::List2String(IndicesPars(kFIXED), ',').c_str());
   fprintf(f, "   > PrintPars(f, kNUISANCE);\n");
   PrintPars(f, kNUISANCE);
   fprintf(f, "   > IndicesPars(kNUISANCE) = %s;\n", TUMisc::List2String(IndicesPars(kNUISANCE), ',').c_str());


   fprintf(f, "\n");
   fprintf(f, "   > GetParListStatus();  =>  %d\n", GetParListStatus());
   fprintf(f, "   > fPars[0]->SetStatus(false); [fPars[0]->GetName())=%s]\n", fPars[0]->GetName().c_str());
   fprintf(f, "   > UpdateParListStatusFromIndividualStatus();\n");
   fPars[0]->SetStatus(false);
   UpdateParListStatusFromIndividualStatus();
   fprintf(f, "   > GetParListStatus();  =>  %d\n", GetParListStatus());
   fprintf(f, "   > fPars[1]->SetStatus(false); [fPars[1]->GetName())=%s]\n", fPars[1]->GetName().c_str());
   fprintf(f, "   > UpdateParListStatusFromIndividualStatus();\n");
   fPars[1]->SetStatus(false);
   UpdateParListStatusFromIndividualStatus();
   fprintf(f, "   > GetParListStatus();  =>  %d\n", GetParListStatus());
   fprintf(f, "   > PrintPars(f);\n");
   PrintPars(f);
   fprintf(f, "   > fPars[2]->SetStatus(false); [fPars[2]->GetName())=%s]\n", fPars[2]->GetName().c_str());
   fprintf(f, "   > UpdateParListStatusFromIndividualStatus();\n");
   fPars[2]->SetStatus(false);
   UpdateParListStatusFromIndividualStatus();
   fprintf(f, "   > GetParListStatus();  =>  %d\n", GetParListStatus());
   fprintf(f, "   > fPars[3]->SetStatus(false); [fPars[3]->GetName())=%s]\n", fPars[3]->GetName().c_str());
   fprintf(f, "   > UpdateParListStatusFromIndividualStatus();\n");
   fPars[3]->SetStatus(false);
   UpdateParListStatusFromIndividualStatus();
   fprintf(f, "   > GetParListStatus();  =>  %d\n", GetParListStatus());
   fprintf(f, "   > PrintPars(f);\n");
   PrintPars(f);
   fprintf(f, "\n");
   fprintf(f, "\n");

   fprintf(f, "   > SetParVal(i_par=0, val=10.);\n");
   fprintf(f, "   > SetParVal(i_par=2, val=5.);\n");
   SetParVal(0, 10);
   SetParVal(2, 5);
   fprintf(f, "   > PrintPars(f)\n");
   PrintPars(f);
   fprintf(f, "   > UID();              => %s\n", UID().c_str());
   fprintf(f, "   > GetParListStatus();   =>  %d\n", GetParListStatus());
   fprintf(f, "   > IndicesValUpdated(); => ");
   vector<Int_t> indices = IndicesValUpdated();
   for (Int_t i = 0; i < (Int_t)indices.size(); ++i)
      fprintf(f, "%d ", indices[i]);
   fprintf(f, "\n");
   fprintf(f, "\n");
   fprintf(f, "   > fPars[2]->SetStatus(false); \n");
   fPars[2]->SetStatus(false);
   fprintf(f, "   > PrintPars(f)\n");
   PrintPars(f);
   fprintf(f, "   > SetParListStatus(false)\n");
   SetParListStatus(false);
   fprintf(f, "   > GetParListStatus();   =>  %d\n", GetParListStatus());
   UpdateParListStatusFromIndividualStatus();
   fprintf(f, "   > UpdateParListStatusFromIndividualStatus()\n");
   fprintf(f, "   > GetParListStatus();   =>  %d\n", GetParListStatus());
   PrintPars(f);
   fprintf(f, "   > ResetStatusParsAndParList();\n");
   ResetStatusParsAndParList();
   fprintf(f, "   > PrintPars(f);\n");
   PrintPars(f);
   fprintf(f, "   > IndexPar(\"lambda0\")=%d\n", IndexPar("lambda0"));
   fprintf(f, "   > SetParVal(IndexPar(\"lambda0\"), val=5.);\n");
   SetParVal(IndexPar("lambda0"), 5.);
   //fprintf(f, "   > TUI_Modify();  [uncomment in TEST() to enable it]\n");
   //TUI_Modify();
   fprintf(f, "   > PrintPars(f)\n");
   PrintPars(f);
   fprintf(f, "\n");
   fprintf(f, "\n");

   fprintf(f, " * Add to the list a new parameter i) used parameter \"OtherPar\" and ii) created \"Vc\" (to be deleted by destructor)\n");
   fprintf(f, "   > TUFreeParEntry entry; entry.SetName(\"OtherPar\"); ...\n");
   TUFreeParEntry entry;
   entry.SetPar("OtherPar", "-", 3);
   fprintf(f, "   > UseParEntry(entry)\n");
   UseParEntry(&entry);
   fprintf(f, "   > PrintPars(f)\n");
   PrintPars(f);
   fprintf(f, "\n");
   fprintf(f, "   > AddPar(\"Vc\",\"km/s\",10);\n");
   AddPar("Vc", "km/s", 10.);
   fprintf(f, "   > PrintPars(f)\n");
   PrintPars(f);
   fprintf(f, "\n");
   fprintf(f, "\n");

   fprintf(f, " * The method IsDeletePar(i) indicates whether the parameter will be deleted by destructor\n");
   for (Int_t i = 0; i < (Int_t)GetNPars(); ++i)
      fprintf(f, "   > IsDeletePar(%d)=%d\n", i, IsDeletePar(i));
   fprintf(f, "\n");
   fprintf(f, "\n");

   fprintf(f, " * Checks that modifying a parameter in the class, if used, changes the value of the original\n");
   fprintf(f, "   > GetParEntry(i_par=4)->SetStatus(false);\n");
   GetParEntry(4)->SetStatus(false);
   fprintf(f, "   > GetParEntry(i_par=4)->Print(f);\n");
   GetParEntry(4)->Print(f);
   fprintf(f, "   > entry.Print(f)\n");
   entry.Print(f);
   fprintf(f, "   > SetParVal(i_par=4, new_val=12);\n");
   SetParVal(4, 12);
   fprintf(f, "   > GetParEntry(i_par=4)->Print(f);\n");
   GetParEntry(4)->Print(f);
   fprintf(f, "   > entry.Print(f)\n");
   entry.Print(f);
   fprintf(f, "\n");
   fprintf(f, "\n");

   // Copy
   fprintf(f, " * Test Copy method:\n");
   fprintf(f, "   > TUFreeParList pars; pars.Copy(*this);\n");
   TUFreeParList pars;
   pars.Copy(*this);
   fprintf(f, "   > pars.PrintPars(f);\n");
   pars.PrintPars(f);
   fprintf(f, "\n");
   fprintf(f, "   > PrintPars(f);\n");
   PrintPars(f);
   fprintf(f, "\n");
   fprintf(f, "\n");

   // Clone
   fprintf(f, " * Test Clone method:\n");
   fprintf(f, "   > TUFreeParList *pars_clone = Clone();\n");
   TUFreeParList *pars_clone = Clone();
   fprintf(f, "   > pars_clone->PrintPars(f);\n");
   pars_clone->PrintPars(f);
   fprintf(f, "\n\n");
   delete pars_clone;
   pars_clone = NULL;

   // Test load from best-fit
   fprintf(f, " * Test load from best-fit files:\n");
   string file = "$USINE/inputs/fit.TEST.result.out";
   vector<Int_t> selected_pars;
   fprintf(f, "   > LoadFitValues(file=%s, selected_pars=empty, is_skipped_fixed=false, is_verbose=true, f_log=f)\n", file.c_str());
   LoadFitValues(file, selected_pars, false, true, f);
   fprintf(f, "   > PrintPars(f);\n");
   PrintPars(f);
   fprintf(f, "   -> and print fit-like entries;\n");
   PrintFitValsAndInit(f);
   fprintf(f, "\n");
   fprintf(f, "\n");
   //
   selected_pars.clear();
   selected_pars.push_back(1);
   selected_pars.push_back(3);
   selected_pars.push_back(5);
   fprintf(f, "   > LoadFitValues(file=%s, selected_pars=[1,3,5], is_skipped_fixed=false, is_verbose=true, f_log=f)\n", file.c_str());
   LoadFitValues(file, selected_pars, false, true, f);
   fprintf(f, "   > PrintPars(f);\n");
   PrintPars(f);
   fprintf(f, "   -> and print fit-like entries;\n");
   PrintFitValsAndInit(f);
   fprintf(f, "\n");
   fprintf(f, "\n");
   //
   fprintf(f, "   > LoadFitValues(file=%s, selected_pars=[1,3,5], is_skipped_fixed=true, is_verbose=true, f_log=f)\n", file.c_str());
   LoadFitValues(file, selected_pars, true, true, f);
   fprintf(f, "   > PrintPars(f);\n");
   PrintPars(f);
   fprintf(f, "   -> and print fit-like entries;\n");
   PrintFitValsAndInit(f);
   fprintf(f, "\n");
   fprintf(f, "\n");
   //
   selected_pars.clear();
   fprintf(f, "   > LoadFitValues(file=%s, selected_pars=empty, is_skipped_fixed=true, is_verbose=true, f_log=f)\n", file.c_str());
   LoadFitValues(file, selected_pars, true, true, f);
   fprintf(f, "   -> and print fit-like entries;\n");
   PrintFitValsAndInit(f);
   fprintf(f, "\n");
   fprintf(f, "   > TVectorD vec=ExtractBestFitVals();\n");
   TVectorD vfit = ExtractBestFitVals();
   fprintf(f, "   > vfit(0)=%le\n", vfit(0));
   fprintf(f, "   > vfit(1)=%le\n", vfit(1));
   fprintf(f, "   > vfit(2)=%le\n", vfit(2));
   fprintf(f, "   > ...\n");
   fprintf(f, "   > vfit(%d)=%le\n", vfit.GetNrows() - 1, vfit(vfit.GetNrows() - 1));
   fprintf(f, "   > pars.PrintPars(f);\n");
   pars.PrintPars(f);
   fprintf(f, "   > UpdateParVals(TUFreePars pars, gENUM_FREEPARTYPE=kANY, Bool_t is_enforce_allfound=false, FILE*f);\n");
   UpdateParVals(pars, kANY, false, false, f);
   fprintf(f, "   > pars.PrintPars(f);\n");
   PrintPars(f);
   fprintf(f, "\n");
   fprintf(f, "\n");


   // Test load from best-fit
   fprintf(f, " * Test load covariance matrix:\n");
   file = "$USINE/inputs/fit.TEST.covmatrix.out";
   selected_pars.clear();
   selected_pars.push_back(1);
   selected_pars.push_back(3);
   selected_pars.push_back(5);
   fprintf(f, "   > LoadFitCovMatrix(file=%s, selected_pars=[1,3,5], is_verbose=true, f_log=f)\n", file.c_str());
   LoadFitCovMatrix(file, selected_pars, true, f);
   // Print matrix
   fprintf(f, "   > PrintFitCovMatrix(f_log=f);\n");
   PrintFitCovMatrix(f);
   fprintf(f, "\n");
   fprintf(f, "\n");
   //
   selected_pars.clear();
   fprintf(f, "   > LoadFitCovMatrix(file=%s, selected_pars=empty, is_verbose=true, f_log=f)\n", file.c_str());
   LoadFitCovMatrix(file, selected_pars, true, f);
   // Access matrix
   fprintf(f, "   > TUMisc::List2String(GetFitCovParNames()) => %s\n", TUMisc::List2String(GetFitCovParNames(), ',').c_str());
   fprintf(f, "   > GetFitCovMatrix(i_row=2,i_col=3) = %le\n", GetFitCovMatrix(2, 3));
   fprintf(f, "   > GetFitCovMatrix()(i_row=2,i_col=3) = %le\n", GetFitCovMatrix()(2, 3));
   // Print matrix
   fprintf(f, "   > PrintFitCovMatrix(f_log=f);\n");
   PrintFitCovMatrix(f);
   fprintf(f, "   > CheckMatchingParNamesAndCovParNames(f_log=f);\n");
   CheckMatchingParNamesAndCovParNames(f);
   fprintf(f, "\n");
}

//______________________________________________________________________________
Bool_t TUFreeParList::TUI_Modify()
{
   //--- Text-user interface to modify the model parameters. Returns true if at
   //    least one parameter has been changed.

   // Print all model parameters and their current values
   // Loop until 'Q' is entered.
   string choice = "";
   Bool_t is_modified;
   do {
      if (GetNPars() == 0) {
         cout << "   No free parameters!" << endl;
      } else
         cout << "   Current parameter(s) are" << endl;

      for (Int_t i = 0; i < GetNPars(); ++i)
         printf("   %2d    %20s = %le  %s\n", i, fPars[i]->GetName().c_str(),
                fPars[i]->GetVal(false), fPars[i]->GetUnit(true).c_str());
      cout << endl;

      // Loop until an existing 'index' is selected (or unless 'Q' is selected)
      Int_t j = 0;
      do {
         cout << "   -> Index of the parameter you wish to modify [Q to quit]: ";
         cin >> choice;
         if (choice == "Q") break;
         j = atoi(choice.c_str());
      } while (j < 0 || j >= GetNPars());
      if (choice == "Q") break;

      // Update new parameter value
      Double_t val_new;
      cout << " Enter new " << fPars[j]->GetName() << " " << fPars[j]->GetUnit(true) << ": ";
      cin >> val_new;
      SetParVal(j, val_new);
      is_modified = true;
      cout << endl;
   } while (1);
   return is_modified;
}

//______________________________________________________________________________
Bool_t TUFreeParList::UpdateParVals(TUFreeParList &pars, gENUM_FREEPARTYPE k_fittype, Bool_t is_enforce_allfound, Bool_t is_check_range, FILE *f_log)
{
   //--- Updates fPars from values in pars. Note that 'pars' can contain less parameters than
   //    fPars. Also enables check of correct range of parameters, and if not, return false.
   //  pars                Parameters to copy
   //  k_fittype           Select specific parameter type (in class) on which to enforce update (kANY for all)
   //  is_enforce_allfound If true, all parameters of pars_bestfit must exists in fPars
   //  is_check_range      If true, check range from initial parameters (in case boundaries)
   //  f_log               Log for USINE
   for (Int_t i = 0; i < pars.GetNPars(); ++i) {

      Int_t i_par = IndexPar(pars.GetParEntry(i)->GetName());
      // Check exists
      if (i_par < 0 && is_enforce_allfound) {
         string message = "Parameter " + pars.GetParEntry(i)->GetName()
                          + " does not exist in class (content is " + GetParNames() + ")";
         TUMessages::Error(f_log, "TUFreeParList", "UpdateParVals", message);
      } else if (i_par >= 0) {
         if (k_fittype != kANY &&  GetParEntry(i_par)->GetFitType() != k_fittype)
            continue;
         // Check range
         if (is_check_range) {
            Double_t par_val = pars.GetParEntry(i)->GetVal(GetParEntry(i_par)->GetFitSampling());
            if (par_val < GetParEntry(i_par)->GetFitInitMin() || par_val > GetParEntry(i_par)->GetFitInitMax())
               return false;
         }
         // Update value
         GetParEntry(i_par)->SetVal(pars.GetParEntry(i)->GetVal(false), false);
      }
   }
   return true;
}

//______________________________________________________________________________
void TUFreeParList::UpdateParListStatusFromIndividualStatus()
{
   //--- Updates fParListStatus, to check whether some parameters have a changed status.
   //       - if at least on parameter in list is updated => fParListStatus = true;
   //       - all parameters in list not updated => fParListStatus = false;

   // Check at least on free parameter has an updated status
   fParListStatus = false;
   for (Int_t i = 0; i < GetNPars(); ++i) {
      if (fPars[i]->GetStatus()) {
         fParListStatus = true;
         break;
      }
   }
}

//______________________________________________________________________________
void TUFreeParList::UseParEntries(TUFreeParList *par_list)
{
   //--- Adds all entries of 'par_list' (uses 'par_list' address) in the list of
   //    free parameters (abort if the parameter name already exists).
   //  par_list          List of parameters to use

   if (!par_list)
      return;

   for (Int_t i = 0; i < par_list->GetNPars(); ++i) {
      TUFreeParEntry *par = par_list->GetParEntry(i);
      if (par)
         UseParEntry(par);
      par = NULL;
   }

   // Update status of pars
   UpdateParListStatusFromIndividualStatus();
}
