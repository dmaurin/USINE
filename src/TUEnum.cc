// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

// C/C++ include
#include <cmath>
#include <string>
// ROOT include
#include <TRint.h> // DAVID ROOT
// USINE include
#include "../include/TUAtomElements.h"
#include "../include/TUEnum.h"
#include "../include/TUMessages.h"
#include "../include/TUMisc.h"

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TUEnum                                                               //
//                                                                      //
// List of global variables and encapsulated enumerations (lists).      //
//                                                                      //
// The namespace TUEnum encapsulates global (scope is the whole code)   //
// definitions (denoted gXXX) of enumerators (denoted kXXX):            //
//    - gN_XXX: number of allowed options for XXX;
//    - gENUM_XXX: list of allowed options for XXX.
// For instance, gN_ETYPE=4 means that we have 4 values for the CR      //
// energies, as enumerated in gENUM_ETYPE={kEKN, kR, kETOT, kEK}.       //
// Hence, kEKN, kR, and kETOT, are of type 'gENUM_XXX', and can be      //
// passed to functions, etc.                                            //
//                                                                      //
// The rest of the variables are encapsulated ones, to define the       //
// names, unit (when applicable) used for printing purpose of these     //
// enumeration lists.                                                   //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

NamespaceImp(TUEnum)

//______________________________________________________________________________
string TUEnum::CRQUnit(gENUM_ETYPE const etype, Bool_t is_bracket)
{
   //--- Returns energy axis unit for a CR quantity of energy axis etype.
   //  etype             gENUM_ETYPE corresponding to etype_name
   //  is_bracket        Unit is in bracket or not

   string tmp = Enum2Unit(etype, false);
   tmp += " m^{2} s sr";
   if (is_bracket)
      tmp = "[" + tmp + "]^{-1}";
   else
      tmp = "(" + tmp  + ")^{-1}";

   return tmp;
}

//______________________________________________________________________________
gENUM_CRFAMILY TUEnum::ExtractCRFamily(string qty)
{
   //--- Returns CR family involved in qty: kNUC, kANTINUC or kLEPTON (see TUEnum.h).
   //  qty                  Qty considered


   TUMisc::UpperCase(qty);
   if (qty.find("POSITRON") != string::npos || qty.find("ELECTRON") != string::npos)
      return kLEPTON;
   if (qty.find("-BAR") != string::npos)
      return kANTINUC;
   TUAtomElements elements;
   if (qty.find_first_of("0123456789") != string::npos
         || elements.IsContainElementName(qty))
      return kNUC;

   return kNUC;
}

//______________________________________________________________________________
void TUEnum::XYTitles(string &x_title, string &y_title, string const &qty,
                      gENUM_ETYPE e_type, Double_t const &e_power, Bool_t is_qty_cr_combo)
{
   //--- Returns x and y title to be used in displays.
   // INPUTS:
   //  qty                      Quantity to plot
   //  e_type[default=kEKN]     Selects x axis among gENUM_ETYPE (kEKN, kR,...)
   //  e_power[default=0]       Y-axis is multiplied by e_type^{e_power} (if qty a flux)
   //  is_qty_cr_combo          If true, check qty as CR comb to adapt y-unit
   //                           If false, use qty for y-axis text and assume it's a flux
   // OUTPUTS:
   //  x_title                  X title (name and unit) of graph
   //  y_title                  Y title (name and unit) of graph

   x_title = Enum2NameUnit(e_type);
   if (is_qty_cr_combo)
      y_title = TUMisc::FormatCRQty(qty, 2);
   else
      y_title = qty;

   string uc_qty = qty;
   TUMisc::UpperCase(uc_qty);
   if (uc_qty == "<LNA>" && is_qty_cr_combo)
      return;

   Bool_t is_ratio = TUMisc::IsRatio(qty);
   if (!is_ratio || !is_qty_cr_combo) {
      y_title = y_title + "  " + CRQUnit(e_type, true);
      if (fabs(e_power) > 0.5e-3)
         y_title = y_title + Form(" #times %s^{%.3f}", Enum2Name(e_type).c_str(), e_power);
   }
}