// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

// C/C++ include
// ROOT include
// USINE include
#include "../include/TUNormList.h"

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TUNormList                                                           //
//                                                                      //
// Query list of normalisation entries and best matching data found.    //
//                                                                      //
// The class TUNormList is based on a normalisation query list          //
// (i.e., a vector of TUNormEntry), and a list of entries found         //
// from a dataset that best match the queries (a vector of TUDataEntry).//
//                                                                      //
// As explained in the documentation of the class TUNormEntry, setting  //
// the CR source normalisation at runtime (to match experimental        //
// data) is necessary whenever the latter is not fitted (i.e. if it     //
// is not a free parameter of a fitting procedure) on the data. Hence,  //
// the two-step procedure is to:                                        //
//     - obtain a user 'wish list' of quantities/exps/energy to use
//       for this normalisation;
//     - find what data to use in practice, based on a given dataset;
//     - remove from the list fitted CRs.
// This is achieved by calling the class SetClass() on a parameter      //
// file and a specified dataset, which fills the members fQuery and     //
// fNormData, and then RemoveNorm() which removes the fitted quantities.//
//                                                                      //
// Whether the normalisation is enforced or not during the propagation  //
// stage is decided elsewhere.                                          //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

ClassImp(TUNormList)

//______________________________________________________________________________
TUNormList::TUNormList()
{
   // ****** Default constructor ******
   //

   Initialise();
}

//______________________________________________________________________________
TUNormList::TUNormList(TUNormList const &norm_list)
{
   // ****** Copy constructor ******
   //  norm_list         Object to copy

   Copy(norm_list);
}

//______________________________________________________________________________
TUNormList::TUNormList(TUInitParList *init_pars, TUDataSet *data, Bool_t is_verbose, FILE *f_log, TUCRList *crs)
{
   // ****** Default constructor ******
   //  init_pars         TUInitParList class of initialisation parameters
   //  data              TUDataSet object of CR data
   //  is_verbose        Verbose or not when class is set
   //  crs               List of CRs [optional]
   //  f_log             Log for USINE

   SetClass(init_pars, data, is_verbose, f_log, crs);
}

//______________________________________________________________________________
TUNormList::~TUNormList()
{
   // ****** Default destructor ******
   //

   Initialise();
}

//______________________________________________________________________________
void TUNormList::Initialise()
{
   //--- Clear member vectors and names.

   fQuery.clear();
   fListNameFilter = "";
   fMapNormDataToQuery.clear();
   fNormData.clear();
   fNormIndicesInFilter.clear();
   fCRsNotToNorm.clear();
   fZNotToNorm.clear();
}

//______________________________________________________________________________
void TUNormList::Copy(TUNormList const &norm_list)
{
   //--- Copies 'norm_list' in class.
   //  norm_list         Object to copy from

   Initialise();

   // Query-related members
   for (Int_t i = 0; i < norm_list.GetNQuery(); ++i)
      fQuery.push_back(norm_list.GetQuery(i));

   // Norm-related members
   fListNameFilter = norm_list.GetListNameFilter();
   for (Int_t i = 0; i < norm_list.GetNNorm(); ++i) {
      fNormData.push_back(norm_list.GetNormData(i));
      fMapNormDataToQuery.push_back(norm_list.IndexNormInQueryList(i));

      vector<Int_t> tmp;
      for (Int_t jcr = 0; jcr < norm_list.GetNCRIndicesInNorm(i); ++jcr)
         tmp.push_back(norm_list.IndexNormInFilter(i, jcr));
      fNormIndicesInFilter.push_back(tmp);
   }

   // fZNotToNorm
   for (Int_t i = 0; i < norm_list.GetNZNotToNorm(); ++i) {
      fZNotToNorm.push_back(norm_list.GetZNotToNorm(i));
      vector<string> tmp;
      for (Int_t j = 0; j < norm_list.GetNCRsNotToNorm(i); ++j)
         tmp.push_back(norm_list.GetCRNotToNorm(i, j));
      fCRsNotToNorm.push_back(tmp);
   }
}

//______________________________________________________________________________
void TUNormList::FillDataFromQuery(TUDataSet *exp_data, Bool_t is_verbose, FILE *f_log, TUCRList *crs)
{
   //--- Fills fNormData: search for closest match to fQuery elements in set
   //    of user-chosen data 'exp_data'. If a list of CRs is provided, check
   //    that each combo queried is present in crs, otherwise discard it.
   //  exp_data          TUDataSet object of CR data
   //  is_verbose        Verbose or not when class is set
   //  f_log             Log for USINE
   //  crs               List of CRs [optional]

   fNormData.clear();
   fMapNormDataToQuery.clear();

   if (!exp_data)
      return;

   if (GetNQuery() == 0)
      TUMessages::Warning(f_log, "TUNormList", "FillDataFromQuery",
                          "fQuery is empty, fill it first!");

   vector<Int_t> z_tmp;
   for (Int_t i = 0; i < GetNQuery(); ++i) {
      Int_t i_entry = exp_data->IndexDataClosestMatch(fQuery[i]);
      if (is_verbose)
         exp_data->Print(f_log, i_entry);
      if (i_entry >= 0) {
         // If CR filter, check that in list of CRs before adding it
         if (crs) {
            Bool_t is_in_crlist = false;
            for (Int_t jcr = 0; jcr < crs->GetNCRs(); ++jcr) {
               if (fQuery[i].GetZ() == crs->GetCREntry(jcr).GetZ()) {
                  is_in_crlist = true;
                  break;
               }
            }
            if (!is_in_crlist)
               continue;
         }

         // Add in fNormData (sort by growing Z)
         if (z_tmp.size() == 0) {
            z_tmp.push_back(fQuery[i].GetZ());
            TUDataEntry *tmp = exp_data->GetEntry(i_entry);
            fNormData.push_back(*tmp);
            tmp = NULL;
            fMapNormDataToQuery.push_back(i);
         } else {
            // Sort by growing Z
            Int_t ii = TUMath::BinarySearch(z_tmp.size(), &z_tmp[0], fQuery[i].GetZ());
            z_tmp.insert(z_tmp.begin() + ii + 1, fQuery[i].GetZ());
            TUDataEntry *tmp = exp_data->GetEntry(i_entry);
            fNormData.insert(fNormData.begin() + ii + 1, *tmp);
            fMapNormDataToQuery.insert(fMapNormDataToQuery.begin() + ii + 1, i);
            tmp = NULL;
         }
      }
   }

   // If cr filter used: fill fNormIndicesInFilter
   fNormIndicesInFilter.clear();
   if (crs) {
      fListNameFilter = crs->GetIDList();
      for (Int_t i = 0; i < GetNNorm(); ++i) {
         // Fill fNormIndicesInFilter
         vector<Int_t> cr_indices;
         if (IsElement(i))
            crs->ElementNameToCRIndices(GetNormName(i), cr_indices, false);
         else
            cr_indices.push_back(crs->Index(GetNormName(i), false));
         fNormIndicesInFilter.push_back(cr_indices);
      }
   }
}

//______________________________________________________________________________
Int_t TUNormList::IndexInNormData(string const &qty) const
{
   //--- Returns index in fNormData matching qty if found, return -1 otherwise.
   //  qty               Name of qty to search for

   Int_t i_found = -1;
   for (Int_t i = 0; i < GetNNorm(); ++i) {
      string qty_upper = qty;
      TUMisc::UpperCase(qty_upper);
      string data_qty_upper = fNormData[i].GetQty();
      TUMisc::UpperCase(data_qty_upper);
      if (qty_upper == data_qty_upper)
         return i;
   }
   return i_found;
}

//______________________________________________________________________________
string TUNormList::ListZNotToNormAndCRs() const
{
   //--- Returns list of Z and CR not to norm.

   if (GetNZNotToNorm() == 0)
      return "-";

   string list = Form("%d(%s)", GetZNotToNorm(0), TUMisc::List2String(fCRsNotToNorm[0], ',').c_str());
   for (Int_t i = 1; i < GetNZNotToNorm(); ++i)
      list += (string)Form(",%d(%s)", GetZNotToNorm(i), TUMisc::List2String(fCRsNotToNorm[i], ',').c_str());
   return list;
}

//______________________________________________________________________________
void TUNormList::PrintNormData(FILE *f) const
{
   //--- Prints in file f fNormData used as normalisation.
   //  f                 File in which to print

   string indent = TUMessages::Indent(true);
   string zcrs_removed = "";
   if (GetNZNotToNorm() != 0)
      zcrs_removed = " (Z=" + ListZNotToNorm() + " removed from selection, fitted abundances are " + ListZNotToNormAndCRs() + ")";
   if (fListNameFilter == "")
      fprintf(f, "%s   - List of data matching the query%s:\n", indent.c_str(), zcrs_removed.c_str());
   else
      fprintf(f, "%s   - List of data matching the query%s, using CR list filter %s:\n",
              indent.c_str(), zcrs_removed.c_str(), fListNameFilter.c_str());
   if (GetNNorm() > 0) fNormData[0].Print(f, 0, true);
   for (Int_t i = 1; i < GetNNorm(); ++i)
      fNormData[i].Print(f, 0, false);

   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUNormList::PrintNormQuery(FILE *f) const
{
   //--- Prints in file f fQuery asked to be used as normalisation.
   //  f                 File in which to print

   string indent = TUMessages::Indent(true);

   fprintf(f, "%s    Qty       Sub-exps      <Ekn>\n", indent.c_str());
   for (Int_t i = 0; i < GetNQuery(); ++i)
      fQuery[i].Print(f);

   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUNormList::RemoveNorm(Int_t i_norm)
{
   //--- Removes i_norm entry from fNormData (and related tables).
   //  i_norm            Norm entry to remove

   // Remove this norm
   fMapNormDataToQuery.erase(fMapNormDataToQuery.begin() + i_norm);
   fNormData.erase(fNormData.begin() + i_norm);
   fNormIndicesInFilter.erase(fNormIndicesInFilter.begin() + i_norm);
}

//______________________________________________________________________________
void TUNormList::SetClass(TUInitParList *init_pars, TUDataSet *data, Bool_t is_verbose, FILE *f_log, TUCRList *crs)
{
   //--- Sets class content: if a list of CRs is provided, check that each CR
   //    queried is present in crs, otherwise discard it.
   //  init_pars         TUInitParList class of initialisation parameters
   //  data              TUDataSet object of CR data
   //  is_verbose        Verbose or not when class is set
   //  crs               List of CRs [optional]
   //  f_log             Log for USINE

   string indent = TUMessages::Indent(true);
   if (is_verbose)
      fprintf(f_log, "%s[TUNormList::SetClass]\n", indent.c_str());

   Initialise();

   const Int_t gg = 3;
   string gr_sub_name[gg] = {"Base", "CRData", "NormList"};
   if (is_verbose) {
      fprintf(f_log, "%s### Set quantities/experiment/ekn (enable renormalisation of model calculation) [%s#%s#%s]\n",
              indent.c_str(), gr_sub_name[0].c_str(), gr_sub_name[1].c_str(), gr_sub_name[2].c_str());
   }

   // Set data used to renormalise calculations
   string norms = init_pars->GetParEntry(init_pars->IndexPar(gr_sub_name)).GetVal();


   // Format is qty1,qty2:Exp1|Ekn1|Etype1;qty3:Exp1,Exp2,Exp3|Ekn2|Etype2,
   // i.e. a single energy value and type per entry, with several entries
   // available (semicolon separated), within which, qties and exps can be
   // a comma-separated list.
   vector<string> list_norm;
   TUMisc::String2List(norms, ";", list_norm);

   // Loop on semicolon-separated entries
   for (Int_t i = 0; i < (Int_t)list_norm.size(); ++i) {
      if (is_verbose)
         fprintf(f_log, "%s   - from user-input %s\n", indent.c_str(), list_norm[i].c_str());

      // Extract qties:Exps|Ekn|Etype for this entry
      vector<string> tonorm;
      TUMisc::String2List(list_norm[i], ":|", tonorm);
      if (tonorm.size() != 4) {
         string message = "Bad format for ToNorm (should be quantities:exp:ekn)";
         TUMessages::Error(f_log, "TUNormList", "SetClass", message);
      }

      // Extract all species in qties above
      vector<string> species;
      TUMisc::String2List(tonorm[0], ",", species);
      //cout << tonorm[0] << endl;
      for (Int_t j = 0; j < (Int_t)species.size(); ++j) {
         //cout << species[j] << endl;
         TUNormEntry tmp;
         tmp.SetE(atof(tonorm[2].c_str()));
         tmp.SetQty(species[j]);
         tmp.SetExpList(tonorm[1]);

         gENUM_ETYPE etype;
         TUEnum::Name2Enum(tonorm[3], etype);
         tmp.SetEType(etype);
         AddQuery(tmp);
         //tmp.Print();
      }
   }

   // Print query
   if (is_verbose)
      PrintNormQuery(f_log);

   // Search for matching data
   FillDataFromQuery(data, is_verbose, f_log, crs);

   // Print data found
   if (is_verbose)
      PrintNormData(f_log);

   if (is_verbose)
      fprintf(f_log, "%s[TUNormList::SetClass] <DONE>\n\n", indent.c_str());

   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUNormList::TEST(TUInitParList *init_pars, FILE *f)
{
   //--- Test function for this class (called in usine -t).
   //  init_pars         TUInitParList class of initialisation parameters
   //  f                 Output file for test

   string message = "Query list and data found to use as normalisation entry.";
   TUMessages::Test(f, "TUNormList", message);

   // Requires a datafile
   fprintf(f, "   > string f_data = TUMisc::GetPath($USINE_CRDATA/crdata_crdb20141001.dat);\n");
   string f_data = TUMisc::GetPath("$USINE/inputs/crdata_crdb20141001.dat");
   fprintf(f, "   > TUDataSet data; data.LoadData(f_data, true, false, f);\n");
   TUDataSet data;
   data.LoadData(f_data, true, false, f);

   // Set class and summary content
   fprintf(f, "   > SetClass(init_pars=\"%s\", &data, is_verbose=false, f_log=f);\n", init_pars->GetFileNames().c_str());
   SetClass(init_pars, &data, false, f);
   fprintf(f, "   > PrintNormQuery(f);\n");
   PrintNormQuery(f);
   fprintf(f, "   > PrintNormData(f);\n");
   PrintNormData(f);
   fprintf(f, "\n");
   fprintf(f, "\n");

   // Keep only species in list of CRs
   fprintf(f, " * Check CR filter on queried data\n");
   fprintf(f, "   > string f_crs = TUMisc::GetPath($USINE/inputs/crchartsZmax30_ghost97.dat);\n");
   string f_crs = TUMisc::GetPath("$USINE/inputs/crcharts_Zmax30_ghost97.dat");
   string query_list = "1H,2H,3He,4He,12C,13C,14N,16O,26Al,40K,55Fe,56Fe,57Fe";
   fprintf(f, "   > TUCRList crs; crs.SetCRList(f_crs, \"%s\", is_fill_ghosts=false, list_name=\"CRLIST_TEST\");\n", query_list.c_str());
   TUCRList *crs = new TUCRList();
   crs->SetCRList(f_crs, query_list, false, "CRLIST_TEST");
   fprintf(f, "   > SetClass(init_pars=\"%s\", &data, false, f, &crs);\n",
           init_pars->GetFileNames().c_str());
   SetClass(init_pars, &data, false, f, crs);
   fprintf(f, "   > PrintNormQuery(f);\n");
   PrintNormQuery(f);
   fprintf(f, "   > PrintNormData(f);\n");
   PrintNormData(f);
   fprintf(f, "\n");
   fprintf(f, " * Check CR indices norm from filter for each norm\n");
   fprintf(f, "   > crs->PrintList(f);\n");
   crs->PrintList(f);
   for (Int_t i = 0; i < GetNNorm(); ++i) {
      fprintf(f, "      - GetNormName(i)=%s  [index=%d in Query list]\n",
              GetNormName(i).c_str(), IndexNormInQueryList(i));
      for (Int_t jcr = 0; jcr < GetNCRIndicesInNorm(i); ++jcr)
         fprintf(f, "           * IndexNormInFilter(i,jcr)=%d\n",
                 IndexNormInFilter(i, jcr));
   }
   fprintf(f, "\n");
   fprintf(f, "\n");

   // Keep only species in list of CRs
   fprintf(f, " * Check update from fitted abundances\n");
   fprintf(f, "    -> Create a list of Z, z_not2norm={1,6,2}, and for each Z a list of CRs not to norm, crs_not2norm={{1H}, {12C, 13C}, {4He}}\n");
   vector<Int_t> z_not2norm;
   z_not2norm.push_back(1);
   z_not2norm.push_back(6);
   z_not2norm.push_back(2);
   vector<vector<string> > crs_not2norm;
   vector<string> tmp;
   tmp.push_back("1H");
   crs_not2norm.push_back(tmp);
   tmp.clear();
   tmp.push_back("12C");
   tmp.push_back("13C");
   crs_not2norm.push_back(tmp);
   tmp.clear();
   tmp.push_back("4He");
   crs_not2norm.push_back(tmp);

   fprintf(f, "   > UpdateAndFillNotToNorm(z_not2norm, crs_not2norm, true, f);\n");
   UpdateAndFillNotToNorm(z_not2norm, crs_not2norm, true, f);
   fprintf(f, "   > PrintNormData(f);\n");
   PrintNormData(f);
   fprintf(f, "   > ListZNotToNorm() = %s\n", ListZNotToNorm().c_str());
   fprintf(f, "   > ListZNotToNormAndCRs() = %s\n", ListZNotToNormAndCRs().c_str());
   fprintf(f, "   > IndexInNormData(\"Au\") = %d\n", IndexInNormData("Au"));
   Int_t i_norm_o = IndexInNormData("O");
   fprintf(f, "   > IndexInNormData(\"O\") = %d\n", i_norm_o);
   fprintf(f, "   > GetNormData(%d).Print(f);\n", i_norm_o);
   GetNormData(i_norm_o).Print(f);
   fprintf(f, "   > RemoveNorm(%d)\n", i_norm_o);
   RemoveNorm(i_norm_o);
   fprintf(f, "   > PrintNormData(f);\n");
   PrintNormData(f);
   fprintf(f, "\n");
   fprintf(f, "\n");

   // Copy
   fprintf(f, " * Test Copy method:\n");
   fprintf(f, "   > TUNormList list; list.Copy(*this);\n");
   TUNormList list;
   list.Copy(*this);
   fprintf(f, "   > list.PrintNormQuery(f);\n");
   list.PrintNormQuery(f);
   fprintf(f, "   > list.PrintNormData(f);\n");
   list.PrintNormData(f);
   fprintf(f, "\n");

   // Free memory
   delete crs;
}

//______________________________________________________________________________
void TUNormList::UpdateAndFillNotToNorm(vector<Int_t> const &z_not_to_norm,
                                        vector<vector<string> > const &crs_not_to_norm,
                                        Bool_t is_verbose, FILE *f_log)
{
   //--- Check whether Z and CRs (for each of these Z) in selection are in norm list.
   //    If yes, remove them, and store in not to norm, and store in fCRsNotToNorm
   //    fZNotToNorm.
   //  z_not_to_norm     [nz] List of Z stored in crs_not_to_norm
   //  crs_not_to_norm   [nz][n_crs] For each Z not to norm, list of CR isotopes not to norm


   string indent = TUMessages::Indent(true);

   // Check that both vectors have the same size
   if (z_not_to_norm.size() != crs_not_to_norm.size()) {
      string message = Form("Two vectors should have the same size, while have %d and %d)",
                            (Int_t)z_not_to_norm.size(), (Int_t)crs_not_to_norm.size());
      TUMessages::Error(f_log, "TUNormList", "UpdateAndFillNotToNorm", message);
   }

   // Copy Z and CRs not to norm
   fCRsNotToNorm.clear();
   fZNotToNorm.clear();
   fZNotToNorm = z_not_to_norm;
   // Loop on Z not to norm and determine whether matching Z/CR are present in NormList
   for (Int_t i_z = 0; i_z < (Int_t)fZNotToNorm.size(); ++i_z) {
      fCRsNotToNorm.push_back(crs_not_to_norm[i_z]);
      Int_t n_norm = GetNNorm();
      for (Int_t i_norm = 0; i_norm < n_norm; ++i_norm) {
         string norm_name = GetNormName(i_norm);
         if ((!IsElement(i_norm) && TUMisc::IsInList(fCRsNotToNorm[i_z], norm_name, false))
               || (IsElement(i_norm) && fZNotToNorm[i_z] == GetNormZ(i_norm))) {
            // If norm is a CR and is in list to remove,
            // or norm is an element and in list of Z to remove
            // --> remove it!
            if (is_verbose)
               fprintf(f_log, "%s      ... remove %s from norm list\n",
                       indent.c_str(), GetNormName(i_norm).c_str());
            RemoveNorm(i_norm);
            --n_norm;
         }
      }
   }

   TUMessages::Indent(false);
}
