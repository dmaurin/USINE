// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

// C/C++ include
// ROOT include
// USINE include
#include "../include/TUAxis.h"
#include "../include/TUEnum.h"
#include "../include/TUMath.h"
#include "../include/TUMessages.h"

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TUAxis                                                               //
//                                                                      //
// Grid of linearly- or logarithmically-spaced values.                  //
//                                                                      //
// The class TUAxis is a 1D grid used as a coordinate axis. The         //
// methods allow to retrieve the Min, the Max, the value at half-bin,   //
// the step, etc. The grid is constructed from 6 parameters:            //
//      - val. min;
//      - val. max;
//      - number of bins;
//      - name (case sensitive, e.g. "x" for a spatial coordinate);
//      - unit (e.g. [kpc]);
//      - is log (true for log. step, false for lin. step).
//                                                                      //
//////////////////////////////////////////////////////////////////////////

ClassImp(TUAxis)

//______________________________________________________________________________
TUAxis::TUAxis()
{
   // ****** Default constructor ******

   fIsUpdated = false;
}

//______________________________________________________________________________
TUAxis::TUAxis(TUAxis const &axis)
{
   // ****** Copy constructor ******
   //  axis              Object to copy

   fIsUpdated = false;
   Copy(axis);
}

//______________________________________________________________________________
TUAxis::~TUAxis()
{
   // ****** Default destructor ******
}

//______________________________________________________________________________
void TUAxis::Copy(TUAxis const &axis)
{
   //--- Copies content of axis in class.
   //  axis              Grid to copy from

   fAxisType = axis.GetAxisType();
   fGrid.clear();
   fIsUpdated = axis.IsUpdated();
   fName =  axis.GetName();
   fStep = axis.GetStep();
   fUnit = axis.GetUnit(false);
   for (Int_t i = 0; i < axis.GetN(); ++i)
      fGrid.push_back(axis.GetVal(i));
}

//______________________________________________________________________________
Double_t TUAxis::GetValHalfBin(Int_t i, Bool_t is_upper) const
{
   //--- Returns value for the "i-1/2"-th or "i+1/2"-th element.
   //  i                 Index for which to calculate the half-lower bin
   // is_upper           Return for half-upper bin (true) or half-lover bin (false)

   if (is_upper) {
      if (fAxisType == kLOG)
         return fGrid[i] * sqrt(fStep);
      else
         return fGrid[i] + fStep / 2.;
      return 0.;
   } else {
      if (fAxisType == kLOG)
         return fGrid[i] / sqrt(fStep);
      else
         return fGrid[i] - fStep / 2.;
      return 0.;
   }
   return 0.;
}

//______________________________________________________________________________
Int_t TUAxis::IndexClosest(Double_t const &val) const
{
   //--- Returns index "k" for which fGrid[k] is closest to "val" (but smaller).
   //  val               Value to search in the grid

   // Check if in grid range
   if (val > GetMax()) return GetN() - 1;
   else if (val < GetMin()) return 0;

   // Retrieve position
   if (fAxisType == kLOG) {
      return Int_t(floor(log(val / GetMin()) / log(fStep)));
   } else if (fAxisType == kLIN) {
      return Int_t(floor((val - GetMin()) / fStep));
   } else {
      // Not used for now, but if ever non-equally-spaced grid,
      // this would become useful
      Int_t k = TUMath::BinarySearch(GetN(), &fGrid[0], val);
      if (k < 0)
         return 0;
      else if (k > GetN() - 1)
         return GetN() - 1;
      else {
         if (fabs((val - fGrid[k + 1]) / val) < 1.e-5)
            return k + 1;
         else return k;
      }
   }
}

//______________________________________________________________________________
void TUAxis::Initialise()
{
   //--- Initialises, i.e., empties the class.

   fAxisType = kLOG;
   fGrid.clear();
   fIsUpdated = false;
   fName = "";
   fStep = 0.;
   fUnit = "";
}

//______________________________________________________________________________
Bool_t TUAxis::IsSameGridValues(TUAxis *axis) const
{
   //--- Checks that current class and 'axis' have same energy grids
   //    (i.e., min, max, and step type).
   //  axis              Grid to compare to

   // Check lin vs log step
   if (fAxisType != axis->GetAxisType() || GetN() != axis->GetN())
      return false;

   // Check min/max values
   if (((fabs(GetMin()) < 1.e-50 && fabs(axis->GetMin()) < 1.e-50) ||
         fabs(GetMin() - axis->GetMin()) / fabs(GetMin()) < 1.e-5)
         && ((fabs(GetMax()) < 1.e-50 && fabs(axis->GetMax()) < 1.e-50) ||
             fabs(GetMax() - axis->GetMax()) / fabs(GetMax()) < 1.e-5)
      )
      return true;
   else
      return false;
}

//______________________________________________________________________________
void TUAxis::Print(FILE *f) const
{
   //--- Prints fGrid values in file f.
   //  f                 File in which to print

   if (GetN() == 0) {
      TUMessages::Warning(f, "TUAxis", "Print", "Nothing to write, fGrid is empty");
      return;
   }

   string indent = TUMessages::Indent(true);
   PrintSummary(f);
   for (Int_t k = 0; k < GetN(); ++k)
      fprintf(f, "%s %d %.4e\n", indent.c_str(), k, fGrid[k]);
   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUAxis::PrintSummary(FILE *f) const
{
   //--- Prints summary in file f.
   //  f                 File in which to print

   if (GetN() == 0) {
      TUMessages::Warning(f, "TUAxis", "PrintSummary", "Nothing to write, fGrid is empty");
      return;
   }
   if (GetN() == 0) {
      TUMessages::Warning(f, "TUAxis", "PrintSummary", "Nothing to write, fGrid is empty");
      return;
   }

   string indent = TUMessages::Indent(true);
   fprintf(f, "%s   %-5s %-10s: %d bins in [%.3le,%.3le]", indent.c_str(),
           fName.c_str(), GetUnit(true).c_str(), GetN(), GetMin(), GetMax());
   if (fAxisType == kLOG)
      fprintf(f, " (log-step=%.4le)\n", fStep);
   else
      fprintf(f, " (lin-step=%.4le)\n", fStep);

   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUAxis::SetClass(Double_t val_min, Double_t val_max, Int_t n,
                      string const &name, string const &unit, gENUM_AXISTYPE axis_type)
{
   //--- Fills n points in range [val_min,val_max], lin- or log- spaced,
   //    for coordinate 'name' of 'unit'.
   //  val_min           Lower boundary
   //  val_max           Upper boundary
   //  n                 Number of points
   //  name              Coordinate name
   //  unit              Unit name
   //  axis_type         Axis type (kLOG or kLIN, see TUEnum.h)

   fGrid.clear();

   fAxisType = axis_type;
   fName = name;
   fUnit = unit;

   // Calculate step
   //   - if lin step => return (val_max - val_min) / (npts - 1);
   //   - if log step => return pow(val_max/val_min , 1/(npts - 1)).
   if (fAxisType == kLOG) {
      if (fabs(val_max) < 1.e-60 || fabs(val_min) < 1.e-60) {
         string message = "val_min or val_max is 0 with log-step (for axis \'" + name + "\', cannot proceed!";
         TUMessages::Error(stdout, "TUAxis", "SetClass", message);
      }
      fStep = pow(val_max / val_min, 1. / (Double_t)(n - 1));
   } else
      fStep = (val_max - val_min) / (Double_t)(n - 1);

   // Copy first value "min"
   fGrid.push_back(val_min);

   // Calculate bin values between val_min and val_max
   if (fAxisType == kLOG) {
      for (Int_t l = 1; l < n - 1; ++l)
         fGrid.push_back(val_min * pow(fStep, (Double_t)l));
   } else {
      for (Int_t l = 1; l < n - 1; ++l)
         fGrid.push_back(val_min + (Double_t)l * fStep);
   }

   // Copy the last value to ensure we get "max"
   // and not an approximate value of max.
   fGrid.push_back(val_max);
}

//______________________________________________________________________________
void TUAxis::SetEknGrids(TUInitParList *init_pars, TUAxis *fEkn[gN_CRFAMILY], Bool_t is_verbose, FILE *f_log) const
{
   //--- Updates vector of fEkn[nCRs] grid from 'init_pars' for the 5 CR families
   //    given in gENUM_CRFAMILY: nuc=Ekn, antinuc=Ekn, leptons=Ek, gamma,nu=E.
   // INPUT:
   //  init_pars         TUInitParList class of initialisation parameters
   //  is_verbose        Verbose or not when class is set
   //  f_log             Log for USINE
   // OUTPUT:
   //  fEkn              Vector[gN_CRFAMILY] of Ekn grid for each gENUM_CRFAMILY

   string indent = TUMessages::Indent(true);
   if (is_verbose) {
      fprintf(f_log, "%s[TUAxis::UpdateEknGrids]\n", indent.c_str());
      fprintf(f_log, "%s### Set Energies of propagated CRs\n", indent.c_str());
   }

   // find number of bins (valid for all energy grids)
   string group = "Base";
   string subgroup = "EnergyGrid";
   Int_t n_bins = atoi(init_pars->GetParEntry(init_pars->IndexPar(group, subgroup, "NBins")).GetVal().c_str());

   // Loop on the CR families and load energy grids
   string par_names[gN_CRFAMILY] = {"NUC_EknRange", "ANTINUC_EknRange", "LEPTONS_EkRange"};
   string coord_names[gN_CRFAMILY] = {"Ekn", "Ekn", "Ek"};
   string unit_names[gN_CRFAMILY] = {"GeV/n", "GeV/n", "GeV"};
   for (Int_t f = 0; f < gN_CRFAMILY; ++f) {

      gENUM_CRFAMILY family = TUEnum::gENUM_CRFAMILY_LIST[f];
      Int_t i_par = init_pars->IndexPar(group, subgroup, par_names[f]);
      if (i_par >= 0) {
         vector<double> min_max;
         TUMisc::String2List(init_pars->GetParEntry(i_par).GetVal(), "[,]", min_max);

         // If fEkn[f] not allocated, create it (LOG step)!
         if (!fEkn[f])
            fEkn[f] = new TUAxis();
         fEkn[f]->SetClass(min_max[0], min_max[1], n_bins, coord_names[f],
                           unit_names[f], kLOG);


         if (is_verbose) {
            fprintf(f_log, "%s  => %-12s   ", indent.c_str(), TUEnum::Enum2Name(family).c_str());
            fEkn[f]->PrintSummary(f_log);
         }
      }
   }
   if (is_verbose)
      fprintf(f_log, "%s[TUAxis::UpdateEknGrids] <DONE>\n\n", indent.c_str());
   TUMessages::Indent(false);
}

//______________________________________________________________________________
void TUAxis::TEST(FILE *f)
{
   //--- Test function for this class (called in usine -t).
   //  f                 Output file for test

   string message = "Handles linear and logarithmic step grids";
   TUMessages::Test(f, "TUAxis", message);

   // Fill (log step
   fprintf(f, " * Example of logarithmic step:\n");
   fprintf(f, "   > SetClass(val_min=1.e-2, val_max=10., n=5, name=Ekn, unit=GeV/n, axis_type=kLOG);\n");
   SetClass(1.e-2, 10., 5, "Ekn", "GeV/n", kLOG);

   // Basic print
   fprintf(f, "   > PrintSummary(f);\n");
   PrintSummary(f);
   fprintf(f, "   > Print(f);\n");
   Print(f);

   // Change min and max boundaries on the fly
   fprintf(f, "   > UpdateMin(1.e-3);\n");
   UpdateMin(1.e-3);
   fprintf(f, "   > UpdateMax(20.);\n");
   UpdateMax(20.);
   fprintf(f, "   > Print(f);\n");
   Print(f);

   // "Advanced" functions
   fprintf(f, "   > EvalOutOfRange(-1);             => %le\n", EvalOutOfRange(-1));
   fprintf(f, "   > GetVal(0, halfbin_status=-1);   => %le\n", GetVal(0, -1));
   fprintf(f, "   > GetVal(0, halfbin_status= 0);   => %le\n", GetVal(0,  0));
   fprintf(f, "   > EvalOutOfRange(0);              => %le\n", EvalOutOfRange(0));
   fprintf(f, "   > GetVal(0, halfbin_status=+1);   => %le\n", GetVal(0, +1));
   fprintf(f, "   > GetVal(N-1, halfbin_status=-1); => %le\n", GetVal(GetN() - 1, -1));
   fprintf(f, "   > GetVal(N-1, halfbin_status=0);  => %le\n", GetVal(GetN() - 1, 0));
   fprintf(f, "   > EvalOutOfRange(N-1)             => %le\n", EvalOutOfRange(GetN() - 1));
   fprintf(f, "   > GetVal(N-1, halfbin_status=+1); => %le\n", GetVal(GetN() - 1, +1));
   fprintf(f, "   > EvalOutOfRange(N)               => %le\n", EvalOutOfRange(GetN()));
   Double_t tekn[4] = {5.e-5, 1., 5, 50.};
   for (Int_t i = 0; i < 4; ++i)
      fprintf(f, "   > IndexClosest(%le);  => %d\n", tekn[i], IndexClosest(tekn[i]));

   fprintf(f, "\n");
   fprintf(f, " * Example of linear step:\n");
   fprintf(f, "   > SetClass(val_min=0, val_max=20, n=8, name=r, unit=kpc, axis_type=kLIN);\n");
   SetClass(0, 20, 8, "r", "kpc", kLIN);
   Print(f);
   fprintf(f, "   > GetVal(2, half_bin_status=+1);  => %le\n", GetVal(2, 1));
   fprintf(f, "\n");

   fprintf(f, " * Test if two grids are equal:\n");
   fprintf(f, "   > SetClass(1.e-2, 10., 13, \"Ekn\", \"GeV/n\", kLOG);\n");
   fprintf(f, "   > TUAxis axis0(1.e-2, 10., 13, \"Ekn\", \"GeV/n\", kLOG);\n");
   fprintf(f, "   > TUAxis axis1(1.e-2, 10., 14, \"Ekn\", \"GeV/n\", kLOG);\n");
   fprintf(f, "   > TUAxis axis2(1.e-2, 10., 13, \"Ekn\", \"GeV/n\", kLIN);\n");
   fprintf(f, "   > TUAxis axis3(1.e-2, 11., 13, \"Ekn\", \"GeV/n\", kLOG);\n");
   fprintf(f, "   > TUAxis axis4(1.01e-2, 10., 13, \"Ekn\", \"GeV/n\", kLOG);\n");
   SetClass(1.e-2, 10., 13, "Ekn", "GeV/n", kLOG);
   TUAxis axis0(1.e-2, 10., 13, "Ekn", "GeV/n", kLOG);
   TUAxis axis1(1.e-2, 10., 14, "Ekn", "GeV/n", kLOG);
   TUAxis axis2(1.e-2, 10., 13, "Ekn", "GeV/n", kLIN);
   TUAxis axis3(1.e-2, 11., 13, "Ekn", "GeV/n", kLOG);
   TUAxis axis4(1.01e-2, 10., 13, "Ekn", "GeV/n", kLOG);
   fprintf(f, "   > IsSameGridValues(&axis0) = %d [expected kLOG]\n",
           IsSameGridValues(&axis0));
   fprintf(f, "   > IsSameGridValues(&axis1) = %d [expected kLIN]\n",
           IsSameGridValues(&axis1));
   fprintf(f, "   > IsSameGridValues(&axis2) = %d [expected kLIN]\n",
           IsSameGridValues(&axis2));
   fprintf(f, "   > IsSameGridValues(&axis3) = %d [expected kLIN]\n",
           IsSameGridValues(&axis3));
   fprintf(f, "   > IsSameGridValues(&axis4) = %d [expected kLIN]\n",
           IsSameGridValues(&axis4));
   fprintf(f, "\n");

   fprintf(f, " * Test copy method:\n");
   fprintf(f, "   > TUAxis axis; axis.Copy(*this);\n");
   TUAxis axis;
   axis.Copy(*this);
   fprintf(f, "   > Print(f);\n");
   Print(f);
   fprintf(f, "   > axis.Print(f);\n");
   axis.Print(f);
   for (Int_t i = 0; i < 4; ++i)
      fprintf(f, "   > IndexClosest(%le);  => %d\n", tekn[i], IndexClosest(tekn[i]));
   fprintf(f, "\n");
}

//______________________________________________________________________________
void TUAxis::UpdateMax(Double_t const &val_max)
{
   //--- Updates value of max in grid.
   //  val_max           New max value of grid

   SetClass(GetMin(), val_max, GetN(), GetName(), GetUnit(false), GetAxisType());
   fIsUpdated = true;
}

//______________________________________________________________________________
void TUAxis::UpdateMin(Double_t const &val_min)
{
   //--- Updates value of min in grid.
   //  val_min           New min value of grid

   SetClass(val_min, GetMax(), GetN(), GetName(), GetUnit(false), GetAxisType());
   fIsUpdated = true;
}

//______________________________________________________________________________
Double_t TUAxis::EvalOutOfRange(Int_t i) const
{
   //--- Returns value for index outside of grid.
   //  i                 Index for which to calculate value

   if (fAxisType == kLIN) {
      if (i < 0)
         return fGrid[0] + (Double_t)i * fStep;
      else if (i > GetN() - 1)
         return fGrid[GetN() - 1] + Double_t(i - (GetN() - 1)) * fStep;
      else
         return GetVal(i);
   } else if (fAxisType == kLOG) {
      if (i < 0)
         return fGrid[0] / ((Double_t)abs(i) * fStep);
      else if (i > GetN() - 1)
         return fGrid[GetN() - 1] * (Double_t(i - (GetN() - 1)) * fStep);
      else
         return GetVal(i);
   } else {
      string message = "No rule to calculate out-of-grid value with " + TUEnum::Enum2Name(fAxisType);
      TUMessages::Error(stdout, "TUAxis", "EvalOutOfRange", message);
      return 0.;
   }
}
