.. _rst_syntax_model_ism:

Model...\@ISM
~~~~~~~~~~~~~

ISM description for the model.

.. note:: All *Model...\@ISM\@ParNames* values are enabled as free parameters (see :ref:`rst_tutorial_update_fitpars`)!

.. tabularcolumns:: |p{5.cm}|p{10.5cm}|

.. csv-table::
   :header: "Group\@subgroup\@parameter", "Description"
   :widths: 30, 70
   :align: center

   "*Model...\@ISM\@ParNames* (M=0)", "See :numref:`tab_parnames`"
   "*Model...\@ISM\@ParUnits* (M=0)", "See :numref:`tab_parnames`"
   "*Model...\@ISM\@ParVals* (M=0)", "See :numref:`tab_parnames`"
   "*Model...\@ISM\@Density* (M=1)", "**element:FNCTYPE|X** with X=f(t,x,y,z; user-defined parameters) if FNCTYPE=FORMULA [1]_, or X=file_name if FNCTYPE=GRID. Gas density in :math:`cm^{-3}`, for instance ``He:FORMULA|0.133``. Add as many lines as the number of elements in *MediumCompo*, with H a special case requiring ``HI`` (atomic), ``HII`` ionised), and ``H2`` (molecular). Note that the full density nHI+nHII+2nH2 is used for XS on H"
   "*Model...\@ISM\@Te* (M=0)", "**FNCTYPE|X** with X=f(t,x,y,z; user-defined parameter) if FNCTYPE=FORMULA [1]_, or X=file_name if FNCTYPE=GRID. Plasma T° [K] for ISM plasma, e.g. ``FORMULA|1.e4``"

**Footnote**

.. [1] The syntaxt for formula is based on `fparser library <http://warp.povusers.org/FunctionParser>`_, see the documentation for the `formulae syntax <http://warp.povusers.org/FunctionParser/fparser.html>`_