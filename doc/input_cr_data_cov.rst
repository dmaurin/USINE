.. _rst_input_cr_data_cov:

CR data with covariance
--------------------------------------------------------------------------

With USINE, it is possible to use a covariance matrix of errors on selected CR data.

Context
~~~~~~~

:Error types in publications: For past measurements, at best, the overall uncertainties (statistical and systematics combined) were provided. In more recent data, at least statistical and systematic uncertainties are provided separately (format of data gathered in `CRDB <https://lpsc.in2p3.fr/crdb/>`_). Recent experiments (e.g., AMS02) go further and provide also systematics broken down to various contributions (see, e.g., :numref:`tab_ams_BC_errors`).

   .. _tab_ams_BC_errors:

   .. table:: Example of systematics from AMS-02 BC data (`PRL 111, 2016 <http://adsabs.harvard.edu/abs/2016PhRvL.117w1102A>`_)

      ===========  =======  =============================  ====== ====== ====== ======
      Name         Keyword  Origin                         2 GV   5 GV   50GV   1 TV
      ===========  =======  =============================  ====== ====== ====== ======
      Statistical  STAT     Number of events               0.6%   0.3%   1.2%   25.%
      Background   BACK     Contamination from C→B         0.4%   0.3%   0.5%   1.0%
      Acceptance   ACC      Survival prob., data/MC corr.  4.3%   2.7%   1.3%   1.4%
      Unfolding    UNF      Finite 1/R detector res.       0.5%   0.3%   0.5%   5.1%
      Scale        SCALE    Absolute rigidity scale        0.07%  0.03%  0.1%   2.4%
      ===========  =======  =============================  ====== ====== ====== ======


:What about the covariance: However, this may not be sufficient for the model analysis of high-precision data. The next step is the information encoded in the covariance matrix, which gives the error correlations between different energies for all types of errors in the instrument.


Covariance definition and :math:`\chi^2`
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Given data at :math:`n_E` energies, the covariance matrix is a symmetric :math:`n_E \times n_E` matrix containing the covariance of all pairs of data point (see more, e.g., in  `wikipedia <https://en.wikipedia.org/wiki/Covariance_matrix>`_). For :math:`n_\alpha` different types of systematic uncertainties, we have

  :math:`{\rm Cov}_{ij}^\alpha = \rho_{ij}^\alpha \times \sigma_i^\alpha \times \sigma_j^\alpha`,

so that

  :math:`\chi^2_{\rm cov} = \sum_{i,j \in ={0\dots n_E}} ({\rm data}_i-{\rm model}_i) \left(\sum_{\alpha}{\rm Cov}^{\alpha}\right)^{-1}_{ij} ({\rm data}_j-{\rm model}_j)`,

with

   - :math:`\alpha\in\{{\rm STAT}, {\rm BACK}\dots\}` for data in :numref:`tab_ams_BC_errors`, with :math:`i` and :math:`j \in \{R_1, R_2,\dots R_n\}`
   - :math:`\sigma_i^\alpha` the relative uncertainty (for error type :math:`\alpha` at rigidity :math:`R_i`)
   - :math:`\rho_{ij}` the correlation coefficient (e.g., 0=no correl., 1=full correl.)

   .. note:: :math:`{\rm Cov}_{ij}^\alpha` coefficients are symmetric (Gaussian errors), with :math:`\rho_{ii}^\alpha=1` (by construction), and :math:`\rho_{i\neq j}^{\rm STAT}=0` (by definition).


Covariance matrix from relative errors
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Actually, USINE relies on the covariance matrix of relative errors :math:`{\cal C}_{\rm rel}^\alpha`, and as discussed in `Derome et al. (2009) <http://adsabs.harvard.edu/abs/2019arXiv190408210D>`__, this matrix be related to the covariance :math:`{\cal C}_\alpha` required for the above equation in two different ways:

:math:`({\cal C}_{\rm model}^\alpha)_{ij} = ({\cal C}^\alpha_{\rm rel})_{ij} \times y^{\rm model}_i \times y^{\rm model}_j\quad\quad` or :math:`\quad\quad({\cal C}_{\rm data}^\alpha)_{ij} = ({\cal C}^\alpha_{\rm rel})_{ij} \times y^{\rm data}_i \times y^{\rm data}_j`

The initialisation keyword ``IsModelOrDataForRelCov`` allows to select one of the two options (see initialisation parameter in :ref:`rst_syntax_usinefit_toadata`). In particular, for the case of a global normalisation factor in the data (or equivalently, an infinite correlation length for the systematics), :math:`{\cal C}_{\rm model}` should be preferred over :math:`{\cal C}_{\rm data}` not to bias the reconstructed model parameters.

Relative covariance file format
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In USINE, a file for the covariance matrix (of data relative errors) is  uniquely associated to a measured quantity from an experiment for its data taking period. The user must provide a covariance matrix for each measured quantity selected as such in the minimisation procedure (see the initialisation parameter :ref:`rst_syntax_usinefit_toadata`).

Shown below is an excerpt of the file `inputs/CRDATA_COVARIANCE/cov_AMS02_201105201605__BC_R.dat <../../../inputs/CRDATA_COVARIANCE/cov_AMS02_201105201605__BC_R.dat>`_, a toy-model for the covariance of AMS02 B/C(R) data for the data taking period 2011/05-2016/05 (courtesy of L. Derome).

.. literalinclude:: ../inputs/CRDATA_COVARIANCE/cov_AMS02_201105201605__BC_R.dat
   :language: c
   :lines: 1-39