.. _rst_syntax_usinerun_display:

UsineRun\@Display
~~~~~~~~~~~~~~~~~

Display for standard run and minimisations.

.. note:: The quantities and errors on the data shown in the displays can be set differently from the data used in minimisations.

.. tabularcolumns:: |p{6.5cm}|p{8.5cm}|

.. csv-table::
   :header: "Group\@subgroup\@parameter", "Description"
   :widths: 30, 70
   :align: center

   "*UsineRun\@Display\@QtiesExpsEType* (M=0)", "**Qty1,Qty2:Exp1,Exp2:EType1;Qty3:Exp1,Exp3:EType2**: selection of quantities and experimental data to show in displays, e.g. ``He:AMS,BESS:kEKN;B/C:AMS:kR``. Use **ALL** to display all available data."
   "*UsineRun\@Display\@ErrType* (M=0)", "CR data errors shown in displays: ``kERRSTAT`` (statistical uncertainties only), ``kERRSYST`` (systematic uncertainties only), or ``kERRTOT`` (previous tow quadratically combined)"
   "*UsineRun\@Display\@FluxPowIndex* (M=0)", "Displayed fluxes are multiplied by :math:`E^{\rm FluxPowIndex}`, e.g. ``2.8``"