.. _rst_input_cr_charts:

Cosmic ray charts
--------------------------------------------------------------------------

CR charts files are *ASCII* files in `$USINE/inputs/ <../../../inputs/>`_ whose prefix is ``crcharts_*``.

:CR charts are not directly nuclear charts:
   For CR propagation, we only need to consider stable nuclei and unstable whose half-life is of the order of -- or not too small compared to -- the propagation time (typically tens of Myr at GeV/n energies). The CR charts below are based on these nuclei. There is a subtlety for EC-unstable (Electronic Capture) nuclei as their decay time may be tiny while their effective half-life, driven by electron attachment time (CRs are fully ionized above GeV/n energies), may be of the order of the propagation time. This becomes particularly messy for very heavy nuclei!

:Lists of ghost nuclei and what they are good for:
   Very short-lived isotopes whose decay chain leads to nucleus `X` are coined ghost nuclei (for short, `ghosts` below). There are not wanted in the list of CRs to propagate, but they are required to calculate the cumulative cross-section into `X` (because ghosts will be produced in the ISM and will decay, so they must be accounted for in addition to the direct production of `X`). A nice reference to better understand the reconstruction of the ghosts is `Letaw et al. (ApJS 56, 369, 1984) <http://adsabs.harvard.edu/abs/1984ApJS...56..369L>`_ and, if you read French, `Maurin's PhD thesis (2001) <http://tel.archives-ouvertes.fr/docs/00/04/78/51/PDF/tel-00008773.pdf>`_. Production cross section files for nuclei come in two flavours (:ref:`rst_input_xs_data`), those in which ghost reactions are explicitly provided, and those in which only effective cross section are provided---the list of ghosts is mandatory when `USINE` is run with the former files.

:Content and use of CR charts:
   In USINE, files for CR charts contain lists of CRs, their charts (A, Z, m, half-life, r\_rms), a keyword for the CR type (STABLE, BETA, EC, BETA-FED, EC-FED, MIX-FED), and lists of ghost nuclei. CR charts files are read by the class ``TUCREntry`` and ``TUCRList`` to set-up the list of CRs to propagate and their properties in USINE (see :ref:`rst_tutorial`).

   .. note::
      Several files of CR charts (valid for Z<=30) are provided (they only differ from their list of ghosts, see in the headers). The only reasons you might want to modify these files would be:

         - charts have been updated (e.g., the decay time for some nucleus is better measured)
         - CRs are missing from the list (a list for Z>30 will be provided in the next release)
         - update the list of ghosts (e.g., new metastable isomers that could contribute to the cumulative are discovered, or decay channels for heavy nuclei are better characterised). Depending on how you want to use it, recall that you need to provide the reaction and/or cumulative cross-sections for the new list of ghosts.

Shown below is an excerpt of the file `crcharts_Zmax30_ghost97.dat <../../../inputs/crcharts_Zmax30_ghost97.dat>`_. CRs need to be sorted by growing mass, starting with anti-nuclei (sorted by decreasing mass):


.. literalinclude:: ../inputs/crcharts_Zmax30_ghost97.dat
   :language: c
   :lines: 1-50