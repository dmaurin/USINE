.. _rst_general_release3.5:

Notes for V3.5 (July 2019)
~~~~~~~~~~~~~~~~~~~~~~~~~~

.. note:: V3.5 improves on V3.4, with more flexibility and more plots for minimisation analyses. V3.5 was developed for the B/C (`Derome et al. 2019 <https://arxiv.org/abs/1904.08210>`__ and `Génolini et al. 2019 <https://arxiv.org/abs/1904.08917>`__) and :math:`\bar{p}` (`Boudaud et al. 2019 <https://arxiv.org/abs/1906.07119>`__) analyses of AMS-02 data.


**Bug fixes (in master - after tag)**

2019/07/25
   - Bug fixed in ``./bin/usine/ -e`` (option D0)
   - Correct path added in documentation for B/C parameter files


Tag V3.5 (2019/07)
------------------

**Propagation of errors after minimisation (``./bin/usine -u``)**

   - New plot containing ran values of model params (i.e., declared as free pars in model)
   - Results of comparisons saved in files (all options)
   - Show %.2f instead of %.0 for energy in legend (D0)
   - On/off switches (E3-E8): bug fixed in legend for configuration names
   - Cross sections (E3-E8): bug fixed (:math:`\neq` XS sets were sometimes shown as the same)



**New features for runs and comparison plots (``./bin/usine -e``)**

   - All results saved in output ASCII files
   - Header (USINE version and time stamp) added for all saved ASCII files
   - New plot containing ran values of model params (i.e., declared as free pars in model)
   - Results of comparisons saved in files (all options)
   - Show %.2f instead of %.0 for energy in legend (D0)
   - On/off switches (E3-E8): bug fixed in legend for configuration names
   - Cross sections (E3-E8): bug fixed (:math:`\neq` XS sets were sometimes shown as the same)


**New features for fits** (config. params detailed in :numref:`rst_syntax_usinefit_config`, outputs in :numref:`rst_syntax_usinefit_outputs`)

   - New default value for parameter ``UsineFit@Config@NExtraInBinRange`` (0 instead of 5)
   - Additional initialisation parameter for fit strategy ``UsineFit@Config@Strategy@M=0@1`` (see :numref:`rst_syntax_usinefit_config`)
   - Additional plot for residuals (score)
   - Print/save covariance matrix of fit (and nuisance) parameters (``UsineFit@Outputs@IsPrintCovMatrix@M=0@true``) and Hessian
   - More prints/outputs of minimization (separate file for best-fit parameters)
   - *Unlimited values enabled* for FIT and NUISANCE parameters (if minimiser allows): the range ``[min,max]`` can now be also written ``[-,max]``, ``[min,-]``, and ``[-,-]`` where ``-`` is interpreted as *unlimited* lower and/or upper value
   - *New Keyword* ``FIXED`` for ``UsineFit @ FreePars`` parameters. For instance, setting ``delta:FIXED,LIN,[-,-],0.5,0.02`` allows to keep the parameter ``delta`` fixed during the minimisation (it is set to the value 0.5 here)
   - *Scan* for FIT and NUISANCE parameters (plot and ): syntax (in initialisation file) is ``UsineFit@Outputs@Scans@M=1@X:N``, with ``X`` the parameter name (use keyword ``ALL`` to scan all fit parameters), and ``N`` the number of points to use in the scan. The output is a plot of :math:`\chi2(X)`, also saved in a file
   - *Profile likelihood* for FIT and NUISANCE parameters: syntax (in initialisation file) is ``UsineFit@Outputs@Profiles@M=1@X:N``, with ``X`` the parameter name (use keyword ``ALL`` to scan all fit parameters), and ``N`` the number of points to use in the scan. The output is a plot of the profiled parameter (slower than a scan, as a minimisation is required at each step), also saved in a file
   - *Contour* for FIT and NUISANCE parameters: syntax (in initialisation file) is ``UsineFit@Outputs@Contours@M=1@X1,X2:N:{CL1,CL2...}``, with ``X1`` and ``X2`` the parameter names (use keyword ``ALL`` to get all contour combinations of fit parameters), ``N`` the number of points to use in the contour, ``CLi`` the contours to draw in unit of sigmas. The output is a plot of ``X1`` vs ``X2``, also saved in a file. Note that extracting contours can be very slow and sometimes it fails to get (and thus plot) the sought contours
   - New keys *PERCR* and *LIST* for source param ``Model...@SrcSteadyState@SpectValsPerCR``, e.g. ``ASTRO_STD|q[PERCR:DEFAULT=1.e-5];alpha[PERZ:DEFAULT=2.3e,H=2.1,He=2.2];eta_s[LIST:DEFAULT=-1,1H_He=-1.5,C_N_O=0.]``. To recap, the enabled parameters are (we also give parameters that can be let free in the minimisation from the above example):

      - ``SHARED:val`` → universal parameter for all CRs
      - ``PERCR:DEFAULT=val0,CR1=val1,CR2=val2...`` → one par per CR (``q_1H``, ``q_2H``, etc.)
      - ``PERZ:DEFAULT=val0,Z1=val1,Z2=val2...`` → one par per Z (``alpha_H``, ``alpha_He``, etc.)
      - ``LIST:DEFAULT=val0,1H_H_HE=val1,C_N_O=val2...`` → one par per list, with *DEFAULT* taken to be the list of all remaining CRs not in user-defined lists (``eta_s_1H_H_He``, ``eta_s_C_N_O``, and  ``eta_s_DEFAULT``)

   - New initialisation parameter ``IsModelOrDataForRelCov`` to choose whether to use model values or data values to rescale covariance matrix of data relative errors into covariance matrix of data errors. The previous USINE version was using model only (see also Derome et al. 2019).


**New features for nuisance parameters** (all detailed in :numref:`tab_fitparam`)

   - *New prefix keywords* for XS nuis.par. to form linear combinations :math:`\sigma_{\rm LC} = \sum_i C_i \times \sigma_i`:

      - Inelastic: ``LCInelBar94_``, ``LCInelTri99_``, ``LCInelWeb03_``, ``LCInelWel97_``
      - Production: ``LCProdGal17_``, ``LCProdSou01_``, ``LCProdWeb03_``, ``LCProdWKS98_``

      In the fit, nuis. par. associated to each coefficient of the LC of XS is discarded and replaced by a global penalty. In practice, for each reaction (inelastic or production) whose nuisance is a LC of XS, there is an extra penalty :math:`(C-C_{\rm user})^2/\sigma^2_{\rm user}`, where

      - :math:`C_{\rm user}` is hard-coded and set to 1 (can be changed in function Chi2_Nuisance())``
      - :math:`\sigma_{\rm user}` is picked from sigma set by the user in the nuisance parameter (values for parameters from the same reaction must be set to the same value!)
      - :math:`C = \sum_i C_i`, internally calculated in at each step of the minimisation

   - *New reaction keyword* ``ANY`` to enable single nuisance parameter for series of reactions (e.g., any projectile and/or any target and/or any fragment (for production reaction). The reaction keywords cover now all the following situations:

      -  Inelastic XS:

         - ``12C+H``   → specific reaction
         - ``ANY+H``   → any CR on given target
         - ``12C+ALL`` → 12C on all ISM targets
      - Production XS:

         - ``12C+H->10Be``   → specific reaction
         - ``ANY+H->10Be``   → any CRs
         - ``12C+ANY->10Be`` → any target
         - ``12C+H->ANY``    → any fragment
      - Tertiary XS (tot. and diff. similarly biased):

         - ``1H-BAR+H->1H-BAR``   → specific reaction
         - ``1H-BAR+ANY->1H-BAR`` → any target
      - ``ALL`` → if not linear combination of XS, applies to all XS (inel, prod, and tertiary); otherwise, can be applied for all linear combination of inelastic and/or production XS

   Note that overlapping reactions with the same key are forbidden.

**New -u1 and -u2 options for uncertainties**

   - ``./bin/usine -u1``: show 1D parameters and 2D correlations drawn from covariance matrix of fit parameters (obtained when using option ``-m2``)
   - ``./bin/usine -u2``: show median and confidence levels on any quantity, as calculated from drawing samples of parameters from the best-fit values and covariance matrix (obtained when using option ``-m2``)

**Documentation**

   - All new keywords added, with usage and example
   - Initialisation files and fit outputs of `Génolini et al. (2019) <https://arxiv.org/abs/1904.08917>`__ B/C analysis of AMS-02 data available in :ref:`rst_publications`)

**Bug fixes**

   - Multiple covariance matrices now OK
   - Display of phi value for fits (when phi is a nuisance parameter)
   - Inelastic cross sections on He (for *sigInelBarashenkov94.dat*, *sigInelWellish97.dat*, and *sigInelWellish97.dat* in ``inputs/XS_NUCLEI/``). The values are now correctly based on *Ferrando et al. (1988)*. We also removed *sigInelLetaw83.dat* because it is an older and less accurate version of *sigInelWellish97.dat*.
   - Copy method of TUFreeParsList
   - Quantities not to normalise now allows nuclei as well as elements in list
   - Typo corrected for CR source unit
   - Enhancement XS (keyword **EnhancePowHE** for XS nuisance parameters) now applies to inelastic and production XS. For consistency, the enhancement in production XS is related to the projectile energy, not the fragment

**Refactoring**

   - Separate calculation from plots in several ``TURunPropagation`` functions
   - New class ``TURunOutputs`` to store all quantities for a calculation