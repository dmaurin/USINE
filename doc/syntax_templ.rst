.. _rst_syntax_templ:

*Templ...* params
--------------------------------------------------------------------------

The group *Templ...* and its subgroups are described in :numref:`tab_templspectrum` and see :numref:`tab_templspatdist`.

   .. note:: You can define as many templates as you wish for spectrum and spatial distribution!


   .. tabularcolumns:: |p{5.cm}|p{9.cm}|

   .. _tab_template:

   .. csv-table:: User-defined templates, whose name is taken to be the subgroup name (here X).
      :header: "Group\@subgroup\@parameter", "Description"
      :widths: 30, 70
      :align: center

      "*Templ...\@X\@ParNames* (M=0)", "Comma-separated list of parameters (that can later be used as free parameters) for use-defined template **X**, where the template can be used to pick the spectrum (*SpectTempl*) or spatial distribution (*SpatialTempl*) for point-like or steady-state source. For instance ``q,alpha,eta_s``  for X=POWERLAW."
      "*Templ...\@X\@ParUnits* (M=0)", "Comma-separated associated units (``-`` for params without unit). For instance, ``/(GeV/n/m3/Myr),-,-`` for X=POWERLAW."
      "*Templ...\@X\@Definition* (M=0)", "Comma-separated associated values. For instance ``FORMULA|q*beta^(eta_s)*Rig^(-alpha)`` for X=POWERLAW."