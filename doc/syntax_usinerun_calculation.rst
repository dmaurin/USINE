.. _rst_syntax_usinerun_calculation:

UsineRun\@Calculation
~~~~~~~~~~~~~~~~~~~~~

Boundary conditions and numerical precision (integration, convergence...).

**Keywords for boundary conditions (BC)**

   - ``kNOCHANGE``         → Solution w/o E-losses = solution w/ losses at boundary
   - ``kD2NDLNEKN2_ZERO``  → :math:`d^2N/d(\ln E_{k/n})^2=0` at boundary
   - ``kNOCURRENT``        → :math:`j_E = 0` at boundary
   - ``kDFDP_ZERO``        → Flux derivative null at boundary (e.g., used in Dragon code)


**Parameters related to boundary conditions (BC)**

.. tabularcolumns:: |p{7.5cm}|p{8.1cm}|

.. csv-table::
   :header: "Group\@subgroup\@parameter", "Description"
   :widths: 30, 70
   :align: center

   "*UsineRun\@Calculation\@BC_ANTINUC_LE* (M=0)", "Low-energy BC for anti-nuclei (``kD2NDLNEKN2_ZERO``)"
   "*UsineRun\@Calculation\@BC_ANTINUC_HE* (M=0)", "High-energy BC for anti-nuclei (``kNOCHANGE``)"
   "*UsineRun\@Calculation\@BC_NUC_LE* (M=0)", "Low-energy BC for nuclei (``kD2NDLNEKN2_ZERO``)"
   "*UsineRun\@Calculation\@BC_NUC_HE* (M=0)", "High-energy BC for nuclei (``kNOCHANGE``)"
   "*UsineRun\@Calculation\@BC_LEPTON_LE* (M=0)", "Low-energy BC for leptons (``kNOCURRENT``)"
   "*UsineRun\@Calculation\@BC_LEPTON_HE* (M=0)", "High-energy BC for leptons (``kNOCURRENT``)"

**Parameters related to calculation precision**

.. tabularcolumns:: |p{7.5cm}|p{8.1cm}|

.. csv-table::
   :header: "Group\@subgroup\@parameter", "Description"
   :widths: 30, 70
   :align: center

   "*UsineRun\@Calculation\@EPS_ITERCONV* (M=0)", "Relative precision sought for integrations (e.g., secondary contribution from differential XS): default is ``1.e-6``"
   "*UsineRun\@Calculation\@EPS_INTEGR* (M=0)", "Relative precision sought for iterative procedure (e.g, tertiaries for anti-nuclei): default is ``1.e-4``"
   "*UsineRun\@Calculation\@EPS_NORMDATA* (M=0)", "Relative precision sought for normalisation to data (if enabled): default is ``1.e-10``. A high precision is especially required for minimization!"
   "*UsineRun\@Calculation\@IsUseNormList* (M=0)", "Whether to norm source abundances to values listed in *NormList* parameter (``true``) or to use default values set in sources (``false``). For runs performing fits, if set to true, normalise only CR species whose normalisation not set as free parameters."