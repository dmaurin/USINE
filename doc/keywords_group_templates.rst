.. _rst_keywords_group_templates:

Group = ``Templ...``
^^^^^^^^^^^^^^^^^^^^

It includes the groups ``TemplSpectrum`` and ``TemplSpatialDist``. The syntax of the associated parameters is detailed in :numref:`rst_syntax_templ`.

.. _tab_templspectrum:

.. table:: Template formulae :math:`f(\beta,\gamma,p,R,E,E_{k/n})` or grid values.

   ==========================  =============================================
   Group\@subgroup             Description
   ==========================  =============================================
   *TemplSpectrum\@POWERLAW*   Power-law template (name of template is subgroup)
   *TemplSpectrum\@TEST*       Test spectrum (name of template is subgroup)
   *TemplSpectrum\@...*        Any user-defined template :math:`f(\beta,\gamma,p,R,E,E_{k/n})`
   ==========================  =============================================

.. _tab_templspatdist:

.. table:: Template formulae :math:`f(t,x,y,z)` of grid values.

   ======================================  =========================================
   Group\@subgroup                         Description
   ======================================  =========================================
   *TemplSpatialDist\@CST*                 Constant distribution :math:`q(r) = 1`
   *TemplSpatialDist\@DOOR*                :math:`q(r) = H(r-r_{\rm up}) \times H(r_{\rm lo}-r)`, see also STEPUP, STEPDOWN...
   *TemplSpatialDist\@SNRMODIFIED*         :math:`q(r)= 10^{dex(r-R_\odot)} \times (r/R_\odot)^a \times \exp(b(r-R_\odot)/R_\odot)`
   *TemplSpatialDist\@...*                 Any user-define template :math:`f(t,x,y,z)`
   ======================================  =========================================