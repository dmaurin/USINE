.. _rst_syntax_usinefit_config:

UsineFit\@Config
~~~~~~~~~~~~~~~~

Configuration of the minimiser (and its parameters) for minimization runs.

.. note:: The implementation of minimizers in USINE is based on the wrapper `ROOT::Math::Minimizer <https://root.cern.ch/root/html/ROOT__Math__Minimizer.html>`_.

.. tabularcolumns:: |p{7.cm}|p{8.cm}|

.. csv-table::
   :header: "Group\@subgroup\@parameter", "Description"
   :widths: 30, 70
   :align: center

   "*UsineFit\@Config\@Minimiser* (M=0)", "Minimiser used: possible options (depending on whether you installed the corresponding packages) are powell, minuit2, fumili, GSLMultiMin, GSLMultiFit, GSLSimAn, genetic). Default is ``Minuit2`` (see online manual `here <https://root.cern.ch/guides/minuit2-manual>`_)."
   "*UsineFit\@Config\@Algorithm* (M=0)", "Depends on *Minimiser* selected: possible options are ``migrad``, ``simplex``, ``combined``, ``scan``, or ``fumili2`` (for minuit2, default is ``combined``); ``-`` (for fumili, GSLMultiFit, GSLSimAn, genetic); ``ConjugateFR``, ``ConjugatePR``, ``BFGS``, ``BFGS2``, ``SteepestDescent`` (for GSLMultiMin)."
   "*UsineFit\@Config\@NMaxCall* (M=0)", "Maximum number of calls (minuit) or iteration (GSL), e.g. ``100000``."
   "*UsineFit\@Config\@Tolerance* (M=0)", "Tolerance (for minuit): value used for terminating iteration procedure. For example, MIGRAD will stop iterating when edm (expected distance from minimum) will be: :math:`{\rm edm} < {\rm tolerance} \times 10^{-3}`. Default in minuit is 0.1, and in USINE, default is ``1.e-2``."
   "*UsineFit\@Config\@Strategy* (M=0)", "Strategy level to get reliably the best fit parameters. For instance, as indicated for minuit, three different levels exist: low (0), medium (1=default) and high (2) quality. Value 0 (low) economizes function calls (intended for cases where there are many variable parameters and/or the function takes a long time to calculate and/or the user is not interested in very precise values for parameter errors) whereas value 2 (high) indicates is intended for cases where the function is evaluated in a relatively short time and/or where the parameter errors must be calculated reliably."
   "*UsineFit\@Config\@Precision* (M=0)", "Minuit expects double precision, but if this is not the case for the code functions called, a different machine precision must be set. In that case, the user may find that certain features sensitive to first and second differences (HESSE, MINOS, CONTOURS) do not work properly. The precision cannot be better than that of *EPS_NORMDATA* (for nuclei) or *EPS_ITERCONV* and *EPS_INTEGR* (for anti-nuclei). Default is ``1.e-6``."
   "*UsineFit\@Config\@PrintLevel* (M=0)", "Print level (for minuit) chatter during minimisation. Default is ``2``."
   "*UsineFit\@Config\@IsMINOS* (M=0)", "Switch on (true) or off (false) MINOS in minuit to get accurate uncertainties on model parameters (takes much longer to run!). Default is ``false``."
   "*UsineFit\@Config\@IsUseBinRange* (M=0)", "Whether to fit using central energy point (false) or integrated value in bin size (true): for the latter, we assume a power-law behaviour in the bin range, and we properly calculate integrated values for isotopes before forming element and/or ratios. This range can be further subdivided for more precision if a power-law is not a good description over the bin size range (see next parameter). Default is ``false``."
   "*UsineFit\@Config\@NExtraInBinRange* (M=0)", "If *IsUseBinRange* = true, the model calculation is done in each E-range with *NExtraInBinRange* sub-bins. This provides a more accurate but only very slightly longer calculation, assuming a power-law behaviour in 2+NExtraInBinRange intermediate bins (default is ``0``, corresponding to the minimal integration in the bin range)."
   "*UsineFit\@Config\@XS_LC_...* (M=0)", "These parameters (``XS_LC_Constraint``, ``XS_LCInel_Sigma``, ``XS_LCProd_Sigma``) are used only if, in the fit, nuisance parameters for cross sections (XS) are taken as linear combination (LC) of existing XS (hence their name), i.e., :math:`\sigma_{\rm LC} = \sum_i C_i \times \sigma_i` (see :numref:`tab_fitparam`).  These parameters allow to enforce the constraint :math:`\sum_i C_i={\rm XS\_LC\_Constraint}\pm{\rm XS\_LCInel\_Sigma}` as a penalty in the :math:`\chi^2`: :math:`\chi^2_{XS\_LC}=(C-\sum_iC_i)^2/\sigma^2_C`."