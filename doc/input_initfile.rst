.. _rst_initfile:

Initialisation file
--------------------------------------------------------------------------

Any calculation with USINE starts by loading an ``ASCII`` USINE-formatted *initialisation* file. The file usage is flexible enough so that you should not need to recompile the code or go into it. The price to pay is a complicated syntax for the parameter values, which are described in :ref:`rst_syntax`. The present section focusses on the structure and goal of the initialisation file.


What for?
~~~~~~~~~

:Quantities initialised: List of CRs to propagate and their parents (and associated charts); energy ranges for the various species (nuclei, anti-nuclei, leptons); cross sections and CR data to use; propagation (ISM, source, transport) and solar modulation models...

:When/how should I modify these files?: If you want to change some inputs files (XS, CR data), model parameter values (e.g., diffusion coefficient, ISM density...), etc., you only need to edit parameter values in the file: many examples are provided in :ref:`rst_tutorial`.

:What about fit parameters?: The parameters to fit, their range, and whether to use them as free or nuisance parameters is also completely handled by the same `ASCII` file (specific parameters, see below). See :ref:`rst_tutorial_update_fitpars`.


File (and line) format
~~~~~~~~~~~~~~~~~~~~~~

Initialisation files consist of USINE-formatted lines: ``group @ subgroup @ parameter @ M=0,1 @ value``

   | - ``group``: pre-defined keyword, see :ref:`rst_groups`
   | - ``subgroup``: pre-defined keyword, see :ref:`rst_groups`
   | - ``parameter``: pre-defined keyword, see :ref:`rst_syntax`
   | - ``M=0,1``: *boolean*, with M=1 for *true*, M=0 for *false*
   | - ``value``: editable value of the parameter, see :ref:`rst_syntax`


:Group/Subgroup/Parameter: With this syntax, a parameter belongs to a subgroup, which itself belongs to a group. This allows to easily associate a parameter to a given physics ingredient. The list of keywords is of course a matter of the developer's taste, and for the user's point of view, it is what it is!

:Multiple-entry parameter (M=0 or 1): Most of the time, parameters are single-valued ones (M=0). However, for instance for CR data and XS files (but not only), it proves useful to enable multiple-entries (e.g., to load several files): in that case, the files are read sequentially, and the last read values always overrides previously read values (if applies). In practice, the user can add as many lines as parameter values for this parameter, see examples in :ref:`rst_tutorial_update_config`.

:Value: Controlling all the model parameters in a single *ASCII* file has a price; the syntax of the parameter value is quite specific and is often parameter-dependent. The flexibility it enables for the description of transport, sources, etc. is detailed in :ref:`rst_syntax` along with the appropriate syntax.

A typical initialisation file is `init.TEST.par <../../../inputs/init.TEST.par>`_, partly reproduced below (a line starting with `#` is a comment):

.. literalinclude:: ../inputs/init.TEST.par
   :language: none
   :lines: 24-42