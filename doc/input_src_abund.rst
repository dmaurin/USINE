.. _rst_input_src_abund:

Source abundances
--------------------------------------------------------------------------

The *ASCII* file `$USINE/inputs/solarsystem_abundances2003.dat <../../../inputs/solarsystem_abundances2003.dat>`_ gathers Solar system (SS) abundances for long-lived/stable isotopes and elements up to Uranium. Isotopic abundances of GCR sources can be initialised with this SS abundances.

:When to use SS abundances:
   Ideally, SS abundances should be used for comparison purpose after *retro-propagating* CR measurements back to the sources. As CR isotopic data are scarce (they usually only exist at low energy), in many runs, it makes sense to fix the isotopic composition of elements, in order to focus on elemental fluxes (if one is interested in isotopic abundances, then rescaling or fitting them is necessary). This is a choice made by the user in the initialisation file (see :ref:`rst_tutorial_update_runpars` and :ref:`rst_tutorial_update_fitpars`). These values are read and stored by the class ```TUCREntry``` and ```TUCRList```.

Shown below is an excerpt of the file `solarsystem_abundances2003.dat <../../../inputs/solarsystem_abundances2003.dat>`_:

.. literalinclude:: ../inputs/solarsystem_abundances2003.dat
   :language: c
   :lines: 1-30