.. _rst_syntax_usinefit_toadata:

UsineFit\@TOAData
~~~~~~~~~~~~~~~~~

Selection of CR data on which to fit, and whether to use covariance or not (if exists).

.. note:: CR data often have asymmetric error bars. A standard procedure to account for that is
      to compare the model to the upper error if the model is above the data, or to the lower error if the model is below the data.

.. warning:: if you want to use a covariance matrix of errors or nuisance parameters for these errors, you need to define a covariance matrix par quantity measured (see :ref:`rst_input_cr_data_cov`)

================================================= ============================
Group\@subgroup\@parameter                        Description
================================================= ============================
*UsineFit\@TOAData\@QtiesExpsEType* (M=0)         **Qty1,Qty2:Exp1,Exp2:Etype1;Qty3:Exp1,Exp3:Etype2**: enables maximum flexibility to fit quantities from various E-type dependences (e.g., Rkn, R...). For instance ``H,He:AMS,BESS:kEKN;B/C:AMS:KR`` means fitting H and He data of AMS and BESS data (or more precisely look for all experiments having AMS and BESS in their name) in kEKN, and simultaneously fit B/C data from AMS in kR.
*UsineFit\@TOAData\@EminData* (M=0)               Data below EminData are discarded. Default is ``1.e-5`` [GeV, GeV/n, or GV].
*UsineFit\@TOAData\@EmaxData* (M=0)               Data above EmaxData are discarded. Default is ``1.e10`` [GeV, GeV/n, or GV].
*UsineFit\@TOAData\@TStartData* (M=0)             **YYYY-MM-DD_HH:MN:SS** → data before this time are discarded. Default is ``1950-01-01_00:00:00``.
*UsineFit\@TOAData\@TStopData* (M=0)              **YYYY-MM-DD_HH:MN:SS** → data after this time are discarded. Default is ``2100-01-01_00:00:00``.
*UsineFit\@TOAData\@ErrType* (M=0)                | - ``kERRSTAT`` → statistical errors in fit
                                                  | - ``kERRSYST`` → systematics errors in fit
                                                  | - ``kERRTOT``  → total error (quadratic) in fit
                                                  | - ``kERRCOV:DIR`` → covariance matrix of errors in fit, where DIR is the directory in which to search for covariance files, e.g. **DIR** =$USINE/inputs/CRDATA_COVARIANCE/). In practice, with this option, the code proceeds as follows:
                                                  |    - for each EXP_QTY_ETYPE found (*QtiesExpsEType*), search in directory **DIR** for covariance file: a covariance file has a unique USINE built-in name **DIR/cov_EXP_QTY_ETYPE.dat** (see :ref:`rst_input_cr_data_cov`).
                                                  |      → run *./bin/usine* with option ``-m1``, and the code will provide you the names it expects for all the fit data you selected.
                                                  |    - if file not found, enforce ``kERRTOT`` instead of ``kERRCOV`` (for this EXP_QTY_ETYPE).

*UsineFit\@TOAData\@IsModelOrDataForRelCov* (M=0) **true** or **false** → Whether to calculate covariance matrix or errors (from :math:`{\cal C}^{\rm rel}`, the covariance matrix of relative errors) using model values or data values, see `Derome et al. (2009) <http://adsabs.harvard.edu/abs/2019arXiv190408210D>`__: :math:`({\cal C}^{\rm model})_{ij} = ({\cal C}^{\rm rel})_{ij} \times y^{\rm model}_i \times y^{\rm model}_j` or :math:`({\cal C}^{\rm data})_{ij} = ({\cal C}^{\rm rel})_{ij} \times y^{\rm data}_i \times y^{\rm data}_j`.
================================================= ============================