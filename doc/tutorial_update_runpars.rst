.. _rst_tutorial_update_runpars:

Run parameters
---------------

- **Change model selection**: this is where you can use a different propagation model (:numref:`rst_keywords_group_models`) and solar modulation model (:numref:`rst_keywords_group_solmod`) from enabled ones::

   UsineRun  @ Models      @ Propagation        @ M=1 @ Model1DKisoVc
   UsineRun  @ Models      @ SolarModulation    @ M=1 @ SolMod0DFF


- **Change precision for calculation**: just be careful when you change the precision, in particular for minimisation runs for which ``EPS_NORMDATA`` must be kept to high precision, unless the source abundances are not normalized to existing data (set ``IsUseNormList`` to false to bypass any selected list). See :numref:`rst_syntax_usinerun_calculation` for more details::

   UsineRun  @ Calculation @ EPS_ITERCONV       @ M=0 @ 1.e-6
   UsineRun  @ Calculation @ EPS_INTEGR         @ M=0 @ 1.e-4
   UsineRun  @ Calculation @ EPS_NORMDATA       @ M=0 @ 1.e-10
   UsineRun  @ Calculation @ IsUseNormList      @ M=0 @ true


- **Change propagation switches**: you can switch-on/off physics parameters for the calculation. However, we recommend to set all these parameters to *true* (see :numref:`rst_syntax_usinerun_onoff`)::

   UsineRun  @ OnOff       @ IsDestruction      @ M=0 @ true
   UsineRun  @ OnOff       @ IsELossAdiabatic   @ M=0 @ true
   UsineRun  @ OnOff       @ IsEReacc           @ M=0 @ true
   ...


- **Change display options**: the three configurable parameters for the display are the list of quantities to show (use ``ALL`` to display all loaded data), the type of errors to show, and a possible multiplication of fluxes by :math:`{\rm Eaxis}^{\rm FluxPowIndex}`, see :numref:`rst_syntax_usinerun_display`)::

   UsineRun  @ Display     @ QtiesExpsEType     @ M=0 @ He:AMS,BESS:kEKN;B/C:AMS:kR
   UsineRun  @ Display     @ ErrType            @ M=0 @ kERRSTAT
   UsineRun  @ Display     @ FluxPowIndex       @ M=0 @ 2.8