.. _rst_input_xs_data:

Cross section files
--------------------------------------------------------------------------

Cross section files are *ASCII* files on an energy grid, read and log-log interpolated on the CR grid axes (see :ref:`rst_syntax_base_energygrid`) with *TUXSections*. The format and files available are described in `Nuclei (XS_NUCLEI/)`_ and `Anti-nuclei (XS_ANTINUC/)`_ below. You can generate your own XS files, for any kind of XS, any list of CRs, and any list of targets, provided it follows the USINE format (and then you can run them without any change in the code).

   .. note:: Extrapolation: a log-log extrapolation is used at low energy for all XS, whereas constant XS is used for extrapolations at high energies.


Terminology
~~~~~~~~~~~

Before describing the cross section (XS) files, let us start with a bit of  terminology, as different communities may have slightly different definitions and/or usages.

:Elastic vs inelastic: Elastic scattering corresponds to processes in which A+B :math:`\rightarrow` A+B, i.e. same particles in and out (energy is conserved in the centre-of-mass frame). Inelastic scattering are all the other reactions in which A is not conserved: however, depending on their usage, these XS may or may not include quasi-elastic scattering (denoted inelastic non-annihilating for anti-nuclei), i.e. A+B :math:`\rightarrow` A+B+X, in which nuclear excitations and/or resonances occur without particle production, part of the energy being dissipated. Inclusive production XS (A+B :math:`\rightarrow` C+X) are a specific case of inelastic XS: note that the production of fragments may originate from distinct nuclear processes (break-up, spallation, fragmentation), but CR physicists rarely make the distinction - most probably do not know a difference exists! - and use indifferently the word *fragmentation* or *spallation*. The inelastic XS is also sometimes denoted reaction XS.

:Total vs differential XS: A total XS means, for a given process, a measurement over :math:`4\pi` steradians accounting for all possible outgoing energies (for a given incoming energy). Differential XS can be simply or doubly differential (solid angle and outgoing energy). With this definition, one can talk about total elastic (resp. total inelastic) obtained from the integration of differential elastic (resp. inelastic) XS. A possible confusion arises because the sum of the total elastic (:math:`\sigma_{\rm el}`) and total inelastic (:math:`\sigma_{\rm inel}`) XS is denoted total XS (:math:`\sigma_{\rm tot}`); the correct terminology should probably be *total* total XS obtained from the integration of the *differential* total XS!


.. warning:: Elastic scattering is usually disregarded: given the CR kinematics (CR on ISM target at rest), the XS peaks in the forward direction and the CR energy loss is negligible. Actually, for nuclei, the :math:`E^{-2.8}` spectrum also implies a minimal leakage to lower energies.


Nuclei (`XS_NUCLEI/ <../../../inputs/XS_NUCLEI>`_)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**Inelastic XS files**

   The files all listed in :numref:`tab_xs_inel` and the file format is illustrated below. The energies on which the XS are provided are based on a logarithmic grid, with range and number of points as indicated in the file, with

   :math:`E_{\rm k/n}^i = E_{\rm k/n}^{\rm min} \times \Delta^i` with :math:`\Delta = \left(E_{\rm k/n}^{\rm max}/E_{\rm k/n}^{\rm min}\right)^{1/(nE_{\rm k/n} - 1)}`.


   .. tabularcolumns:: |p{1.3cm}|p{1.3cm}|p{1.3cm}|p{9.5cm}|

   .. _tab_xs_inel:

   .. csv-table:: Total inelastic XS files (for nuclei) available in USINE.
      :header: "Ref.", "CRs", "Targets", "File name → comment"
      :widths: 10, 10, 10, 70
      :align: center

      "[Let83]_", "Z=[1-30]", "H, He", "`sigInelLetaw83.dat <../../../inputs/XS_NUCLEI/sigInelLetaw83.dat>`_ → empirical fit on data"
      "[Bar94]_", "Z=[1-30]", "H, He", "`sigInelBarashenkov94.dat <../../../inputs/XS_NUCLEI/sigInelBarashenkov94.dat>`_ → CRN6 code [1]_"
      "[Wel97]_", "Z=[1-30]", "H, He", "`sigInelWellish97.dat <../../../inputs/XS_NUCLEI/sigInelWellish97.dat>`_  → improved [Let83]_ + fit more data [1]_"
      "[Tri99]_", "Z=[1-30]", "H, He", "`sigInelTripathi99.dat <../../../inputs/XS_NUCLEI/sigInelTripathi99.dat>`_ → universal parametrisation (NASA) [2]_"
      "[Web03]_", "Z=[4-30]", "H, He", "`sigInelWebber03.dat <../../../inputs/XS_NUCLEI/sigInelWebber03.dat>`_ → modified [Let83]_, no :math:`^{14}{\rm C}` [3]_"
      "[Cos12]_", "Z=[1-30]", "H, He", "`sigInelTripathi99+Coste12.dat <../../../inputs/XS_NUCLEI/sigInelTripathi99+Coste12.dat>`_ → [Tri99]_ + fit on Z=1-2 data"

   In these files are listed any number of CRs followed by their cross section values on any number of targets (one column per target). Below, in `sigInelTripathi99+Coste12.dat <../../../inputs/XS_NUCLEI/sigInelTripathi99+Coste12.dat>`_, we have two targets (two columns) and the CR shown is :math:`^{70}{\rm Zn}`.

   .. literalinclude:: ../inputs/XS_NUCLEI/sigInelTripathi99+Coste12.dat
      :language: none
      :lines: 1-23


**Production XS files**

   The files are listed in :numref:`tab_xs_prod` and the file format is illustrated below. In this version, only XS in the straight-ahead approximation are provided, i.e. with the approximation that the kinetic energy per nucleon is conserved for all fragments of the reaction,

   :math:`d\sigma^{kj}(T,T')= \delta (T-T')`.

   This simplifies the format as a single energy grid is required for both the projectile and fragments, and the logarithmic scale is as described for the inelastic XS above.

   .. tabularcolumns:: |p{1.3cm}|p{1.3cm}|p{1.3cm}|p{9.5cm}|

   .. _tab_xs_prod:

   .. csv-table:: Production XS files (for nuclei) available in USINE: cumulated XS (ghost contribution included, see :numref:`rst_input_cr_charts`) or XS with ghosts calculated separately in directory `GHOST_SEPARATELY <../../../inputs/XS_NUCLEI/GHOST_SEPARATELY/>`_. For more details about these XS, see [Gén19]_.
      :header: "Ref.", "CRs", "Targets", "File name → comment"
      :widths: 10, 10, 10, 70
      :align: center

      "[Mau01]_", "Z=[1-30]", "H, He", "`sigProdWKS90.dat <../../../inputs/XS_NUCLEI/sigProdWKS90.dat>`_ → [Web90]_, ghosts from [Mau01]_"
      "[Mau01]_", "Z=[1-30]", "H, He", "`sigProdWKS90charts84.dat <../../../inputs/XS_NUCLEI/sigProdWKS90charts84.dat>`_ → [Web90]_, ghosts from [Let84]_"
      "[Mau01]_", "Z=[1-30]", "H, He", "`sigProdWKS98.dat <../../../inputs/XS_NUCLEI/sigProdWKS90.dat>`_ → [Web98]_, ghosts from [Mau01]_"
      "[Mau10]_", "Z=[4-30]", "H, He", "`sigProdSoutoul01.dat <../../../inputs/XS_NUCLEI/sigProdSoutoul01.dat>`_ → priv.com. Soutoul (no :math:`^{14}{\rm C}`)"
      "[Mau10]_", "Z=[4-30]", "H, He", "`sigProdWebber03.dat <../../../inputs/XS_NUCLEI/sigProdWebber03.dat>`_  → priv.com. from [Web03]_ (no :math:`^{14}{\rm C}`)"
      "[Cos12]_", "Z=[1-30]", "H, He", "`sigProdWebber03+Coste12.dat <../../../inputs/XS_NUCLEI/sigProdWebber03+Coste12.dat>`_ → [Web03]_ + fit quartet"
      "[Gén19]_", "Z=[1-30]", "H, He", "`sigSpalGALPROP17_OPT12.dat <../../../inputs/XS_NUCLEI/sigSpalGALPROP17_OPT12.dat>`_ → see [Gén19]_ [1]_"
      "[Gén19]_", "Z=[1-30]", "H, He", "`sigSpalGALPROP17_OPT22.dat <../../../inputs/XS_NUCLEI/sigSpalGALPROP17_OPT22.dat>`_ → see [Gén19]_ [1]_"

   In the production XS files are listed any number of reactions followed by their cross section values on any number of targets (one column per target). Below, in `sigProdWebber03+Coste12.dat <../../../inputs/XS_NUCLEI/sigProdWebber03+Coste12.dat>`_, we have two targets (two columns) and the reaction shown is :math:`^{70}{\rm Zn}\rightarrow ^{68}{\rm Zn}`.

   .. literalinclude:: ../inputs/XS_NUCLEI/sigProdWebber03+Coste12.dat
      :language: none
      :lines: 1-30


Anti-nuclei (`XS_ANTINUC/ <../../../inputs/XS_ANTINUC>`_)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Contrarily to the case of nuclei, we have only one anti-nucleus per XS file. The file format is illustrated below for the various XS.

**Inelastic (ANN and NONANN) XS files**

   Annihilating (ANN) XS correspond to the situation in which the anti-nucleus is destructed, whereas in non-annihilating (NONANN) XS it survives but looses energy (see also below). The corresponding files for these total cross-sections are listed in :numref:`tab_xs_antinuc_inel`.

   .. tabularcolumns:: |p{1.3cm}|p{12.5cm}|

   .. _tab_xs_antinuc_inel:

   .. csv-table:: Inelastic annihilating (ANN) and non-annihilating (NONANN) XS files for anti-nuclei.
      :header: "Ref.", "File name (`xs_projectiles+targets_info.dat`) → comment"
      :widths: 10, 90
      :align: center

      "[Don01]_", "`sigInelANN_pbar+HHe_Donato01.dat <../../../inputs/XS_ANTINUC/sigInelANN_pbar+HHe_Donato01.dat>`_ → based on [Tan83]_"
      "[Don01]_", "`sigInelNONANN_pbar+HHe_Donato01.dat <../../../inputs/XS_ANTINUC/sigInelNONANN_pbar+HHe_Donato01.dat>`_ → based on [Tan83]_"
      "[Dup05]_", "`sigInelANN_dbar+HHe_Duperray05.dat <../../../inputs/XS_ANTINUC/sigInelANN_dbar+HHe_Duperray05.dat>`_ → based on Glauber"
      "[Dup05]_", "`sigInelNONANN_dbar+HHe_Duperray05.dat <../../../inputs/XS_ANTINUC/sigInelNONANN_dbar+HHe_Duperray05.dat>`_ → based on Glauber"
      "[Don08]_", "`sigInelANN_dbar+HHe_Donato08_Composite <../../../inputs/XS_ANTINUC/sigInelANN_dbar+HHe_Donato08_Composite.dat>`_ → See Appendix in [Don08]_"


   Their format is the same as for nuclei above (XS on log-scale Ekn axis), as illustrated with `sigInelANN_pbar+HHe_Donato01.dat <../../../inputs/XS_ANTINUC/sigInelANN_pbar+HHe_Donato01.dat>`_:

   .. literalinclude:: ../inputs/XS_ANTINUC/sigInelANN_pbar+HHe_Donato01.dat
      :language: none
      :lines: 1-24

**Tertiary (redistribution) XS files**

   In tertiary reactions, the anti-nucleus energy is redistributed (tertiary source term), denoted NAR for non-annihilating rescattering) XS, and the corresponding files are listed in :numref:`tab_xs_antinuc_NAR`. They are used to calculate the equilibrium spectrum according to:

   :math:`\frac{dQ_{\rm ter}^P}{dE_{k/n}^{\rm out}}(E_{k/n}^{\rm out}) = \int_{E_{k/n}^{\rm out}}^{\infty} \frac{dN^P}{dE_{k/n}^{\rm in}}(E_{k/n}^{\rm in}) A_P \frac{d\sigma^{P+{\rm ISM} \rightarrow P}}{dE_k^{\rm out}} dE_{k/n}^{\rm in} - \frac{dN^P}{dE_{k/n}^{\rm out}}(E_{k/n}^{\rm out}) n_{\rm ISM} v^{\rm out} \sigma^{\rm NONANN} (E_{k/n}^{\rm out}),`

   where the factor :math:`A_P` originates from the conversion from total to kinetic energy per nucleon in the differential cross section. The two XS in the above equation are associated to:

      - NONANN files (inelastic XS, as listed in listed in :numref:`tab_xs_antinuc_inel`);

      - NAR files (differential XS), where the projectile is also the fragment requires two energy grids, but for for the calculation the same grid is used.

   .. tabularcolumns:: |p{1.3cm}|p{12.5cm}|

   .. _tab_xs_antinuc_NAR:

   .. csv-table::  Non-annihilating rescattering (NAR) differential XS files.
      :header: "Ref.", "File name (`xs_projectiles+targets_info.dat`) → comment"
      :widths: 10, 90
      :align: center

      "[Dup05]_", "`dSdENAR_pbar+HHe_Duperray05_Anderson.dat <../../../inputs/XS_ANTINUC/dSdENAR_pbar+HHe_Duperray05_Anderson.dat>`_ → based on [And67]_ (recommended)"
      "[Dup05]_", "`dSdENAR_pbar+HHe_Duperray05_TanNg.dat <../../../inputs/XS_ANTINUC/dSdENAR_pbar+HHe_Duperray05_TanNg.dat>`_ → based on [Tan83]_"
      "[Dup05]_", "`dSdENAR_dbar+HHe_Duperray05_Anderson.dat <../../../inputs/XS_ANTINUC/dSdENAR_dbar+HHe_Duperray05_Anderson.dat>`_ → based on [And67]_ (recommended)"
      "[Dup05]_", "`dSdENAR_dbar+HHe_Duperray05_TanNg.dat <../../../inputs/XS_ANTINUC/dSdENAR_dbar+HHe_Duperray05_TanNg.dat>`_ → based on [Tan83]_"


**Differential production XS files**

   Secondary production is calculated from

   :math:`\frac{dQ_{\rm sec}^{P\rightarrow F}}{dE_{k/n}^{\rm out}}(E_{k/n}^{\rm out})=\int_{0}^{\infty} dE_{k/n}^{\rm in} \frac{dN^P}{dE_{k/n}^{\rm in}}(E_{k/n}^{\rm in}) \times n_{\rm ISM} \times v^{\rm out} \times A_F \times \frac{d\sigma^{P+{\rm ISM} \rightarrow F}}{dE_{\rm tot}^{\rm out}},`

   where the extra factor :math:`A_F` originates from the conversion from differential kinetic energy per nucleon to differential total energy, which is the format of the XS files. The list of production files for anti-nuclei is listed in :numref:`tab_xs_antinuc_prod`.

   .. tabularcolumns:: |p{1.3cm}|p{12.5cm}|

   .. _tab_xs_antinuc_prod:

   .. csv-table::  Differential production of anti-nuclei XS files.
      :header: "Ref.", "File name (`xs_frag_projectiles+targets_info.dat`) → comment"
      :widths: 10, 90
      :align: center

      "[Don01]_", "`dSdEProd_pbar_1H4He+HHe_Donato01.dat <../../../inputs/XS_ANTINUC/dSdEProd_pbar_1H4He+HHe_Donato01.dat>`_ → [Tan82]_ + DTUNUC [4]_"
      "[Dup05]_", "`dSdEProd_pbar_1H4He+HHe_Duperray05.dat <../../../inputs/XS_ANTINUC/dSdEProd_pbar_1H4He+HHe_Duperray05.dat>`_ → Improved parametrization on data"
      "[Bri07]_", "`dSdEProd_pbar_1H4He+HHe_BringmannSalati07.dat <../../../inputs/XS_ANTINUC/dSdEProd_pbar_1H4He+HHe_BringmannSalati07.dat>`_ → [Tan82]_ at HE (recommended)"
      "[Bou19]_", "`dSdEProd_pbar_1Hto30Si+HHe_Boudaud09.dat <../../../inputs/XS_ANTINUC/dSdEProd_pbar_1Hto30Si+HHe_Boudaud09.dat>`_ [Win17]_ from [Kor18]_"
      "[Dup05]_", "`dSdEProd_dbar_1H4He+HHe_Duperray05_Coal58MeV.dat <../../../inputs/XS_ANTINUC/dSdEProd_dbar_1H4He+HHe_Duperray05_Coal58MeV.dat>`_ → Coalescence model"
      "[Dup05]_", "`dSdEProd_dbar_1H4He+HHe_Duperray05_Coal79MeV.dat <../../../inputs/XS_ANTINUC/dSdEProd_dbar_1H4He+HHe_Duperray05_Coal79MeV.dat>`_ → Coalescence model"
      "[Dup05]_", "`dSdEProd_dbar_pbar+HHe_Duperray05_Coal79MeV.dat <../../../inputs/XS_ANTINUC/dSdEProd_dbar_pbar+HHe_Duperray05_Coal79MeV.dat>`_ → idem + contribs from :math:`\bar{p}`"
      "[Dup05]_", "`dSdEProd_dbar_1H4He+HHe_Duperray05_Micro.dat <../../../inputs/XS_ANTINUC/dSdEProd_dbar_1H4He+HHe_Duperray05_Micro.dat>`_ → Microscopic model"
      "[Dup05]_", "`dSdEProd_dbar_pbar+HHe_Duperray05_Micro.dat <../../../inputs/XS_ANTINUC/dSdEProd_dbar_pbar+HHe_Duperray05_Micro.dat>`_ → idem + contribs from :math:`\bar{p}`"

   Two energy grids are required, one for the projectile, and one for the produced CRs. Both are based on a logarithmic grid, with range and number of points as indicated in the file (for projectile and fragment), with

   :math:`E_{\rm k/n}^i = E_{\rm k/n}^{\rm min} \times \Delta^i` with :math:`\Delta = \left(E_{\rm k/n}^{\rm max}/E_{\rm k/n}^{\rm min}\right)^{1/(nE_{\rm k/n} - 1)}`.

   In this file are listed any number of projectiles followed by their cross section values on any number of targets (one column per target). Below, we have two targets (two columns) and the reaction shown is :math:`^{1}{\rm H} \rightarrow ^{1}{\rm \bar{H}}`.

   .. literalinclude:: ../inputs/XS_ANTINUC/dSdEProd_pbar_1H4He+HHe_BringmannSalati07.dat
      :language: none
      :lines: 1-30


**Footnote**

.. [1] From GALPROP code, generated by Y. Génolini.
.. [2] Implemented by D. Maurin
.. [3] Bill Webber, private communication
.. [4] Uses Tan & Ng (1982) for p+H (PRD 26, 1179, parametrisation fitted on data) and DTUNUC code otherwise (http://sroesler.home.cern.ch/sroesler)

**References**

.. [And67] Anderson et al., PRL 19, 198 (1967)
.. [Tan82] Tan and Ng, PRD 26, 1179 (1982)
.. [Tan83] Tan and Ng, J.Phys.G 9, 227 (1983)
.. [Let83] Letaw, Silberberg, and Tsao, ApJS 51, 271 (1983)
.. [Let84] Letaw, Silberberg, and Tsao, ApJS 56, 369 (1984)
.. [Web90] Webber, Kish, and Schrier, PRC 41, 566 (1990)
.. [Bar94] Barashenkov and Polanski, Tech. Rep. E2-94-417 (Comm. JINR, Dubna, 1994)
.. [Wel97] Wellisch and Axen, PRC 54, 1329 (1996)
.. [Web98] Webber et al., PRC 58, 3539 (1998)
.. [Tri99] Tripathi, Cucinotta, and Wilson, Nucl. Instr. Meth. Phys. Res. B 155, 349 (1999)
.. [Mau01] Maurin, PhD thesis (2001)
.. [Don01] Donato et al., ApJ 563, 172 (2001)
.. [Dup05] Duperray et al., PRD 71, 083013 (2005)
.. [Bri07] Bringmann and Salati, PRD 75, 083006 (2007)
.. [Don08] Donato, Fornengo, and Maurin, PRD 78, 3506 (2008)
.. [Mau10] Maurin, Putze, and Derome, A&A 516, A67 (2010)
.. [Web03] Webber, Soutoul, Kish, and Rockstroh, ApJS 144, 153 (2003)
.. [Cos12] Coste, Derome, Maurin, and Putze, A&A 539, A88 (2012)
.. [Win17] Winkler, JCAP 02, 048 (2017)
.. [Kor18] Korsmeier et al., PRD 97, 103019 (2018)
.. [Gén19] Génolini, Maurin, Moskalenko, and Unger, PRC 98, 4611 (2019)
.. [Bou19] Boudaud, Génolini, Derime, et al. arXiv:1906.07119 (2019)

