.. _rst_models:

Models and equations
======================

We list below the models present in the USINE package, providing their main parameters, equations, and references. Any propagation model is defined by:

   - the model properties in the propagation volume (geometry, ISM...);
   - the transport coefficients (diffusion :math:`K`, convection :math:`V_c`...);
   - the solutions of the transport equation.


Diffusion equation
------------------

Cosmic-ray propagation in the Galaxy can be described by a diffusion equation (see, e.g., `Berezinskii et al. 1990 <http://adsabs.harvard.edu/abs/1990acr..book.....B>`_; `Schlickeiser 2002 <http://adsabs.harvard.edu/abs/2002cra..book.....S>`_; `Strong et al. 2002 <http://adsabs.harvard.edu/abs/2007ARNPS..57..285S>`_):

   .. math::
     \frac{\partial n(\vec r, p, t)}{\partial t}
     &+ \vec\nabla \cdot ( -K\vec\nabla n + \vec{V}_c n ) \\
     &+ \frac{\partial}{\partial p} \left[\dot{p} n + \frac{p}{3} \, (\vec\nabla \cdot \vec{V}_c )n\right] - \frac{\partial}{\partial p}\, p^2 K_{pp} \frac{\partial}{\partial p}\, \frac{1}{p^2}\, n\\
     &= {\rm Source}(\vec r, p, t) - {\rm Sink}(\vec r, p, t),

with :math:`n (\vec r,p,t)` the CR density per particle momentum :math:`p`, :math:`K` the spatial diffusion coefficient, :math:`\vec{V}_c` the convection velocity, :math:`\dot{p}\equiv dp/dt` the momentum losses, and :math:`K_{pp}` the diffusion coefficient in momentum space for reacceleration. This equation is formally the continuity equation (first line) with the energy current (second line), and source and sink terms (last line).


Source terms
~~~~~~~~~~~~


  :Primary origin: Diffusive shock acceleration (e.g., in supernova remnants) is the favoured mechanism to accelerate all species, with an injection spectrum :math:`\propto R^{-\alpha}` with :math:`\alpha\approx 2` (:math:`R=pc/Ze` is the rigidity). The CRs accelerated in sources are denoted *primaries* for short (e.g., :math:`^1{\rm H}`, :math:`^{16}{\rm O}`, :math:`^{30}{\rm Si}`...).

  :Secondary origin: Nuclear interactions of primary CRs on the ISM give rise to secondary particles (:math:`e^+`, :math:`e^-`, :math:`\gamma`, :math:`\nu`), anti-nuclei (:math:`\bar{p}`, :math:`\bar{d}`...), and secondary nuclei as fragments of heavier CR nuclei. These CRs are denoted *secondaries* for short (e.g., Li to B isotopes).

  :Tertiary origin: Non-annihilating inelastic nuclear interactions are reactions in which the particles survive the interactions, but loose some of their energy (e.g., in resonances). This reaction actually both provides a net loss of particles at the energy of interaction, and a gain at lower energies coming from the redistribution of the particles in energy. These redistributed CRs are often denoted *tertiaries* for short, as they involve re-interactions of secondary CRs.

  :Radioactive origin: Unstable CRs are net sources for their progeny. The CR fraction decaying is set by the competition between decay times (e.g., 1.387 Myr for :math:`^{10}{\rm Be}`) and escape time from the Galaxy (tens of Myrs at a few GeV/n): for instance, 15\% of :math:`^{10}{\rm B}` at 10 GeV/n comes the :math:`\beta`-decay of secondary :math:`^{10}{\rm Be}`. Another decay type is electronic capture (EC) decay, in which CR ions must attach first an electron, the effective half-life being a competition between electron attachment, stripping, and EC-decay.

Sink terms
~~~~~~~~~~

  :Destruction: Inelastic interactions on the ISM destroy CRs. The interaction time is constant above GeV/n energies, but gradually becomes a sub-dominant process as the escape time steadily decreases with energy. Also, heavy nuclei are more prone to destruction than lighter ones, as cross sections typically scale as :math:`A^{2/3}`.

  :Redistribution: A fraction of anti-nuclei which suffer non-annihilating interactions disappears. This is this very fraction that feeds the tertiary source term described above.

  :Decay: When unstable species decay, they change their nature. The various decay channels are the ones mirroring those described above in the radioactive source term.



Generic source terms
--------------------


Primary source :math:`q_{\rm prim}^{i}`
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The primary source term, :math:`q_{\rm prim}^{i}`, is expressed in unit of :math:`[{\rm \#part}~{\rm (GeV/n)}^{-1}~{\rm m}^{-3}~{\rm Myr}^{-1}]`.

   .. math::
      q_{\rm prim}^i\equiv \frac{dQ_{\rm prim}^i}{dE_{k/n}} = \frac{A}{Z\beta}\frac{dQ_{\rm prim}^i}{dR}

   .. note:: The term :math:`q_{\rm prim}^{p}` is generically calculated in ``TUValsTXYZEVirtual::GetSrcSpectrum()``.



Secondary source :math:`q_{\rm sec}^{ij}`
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The secondary source term, :math:`q_{\rm sec}^{ij}`, is expressed in unit of :math:`[\frac{dn}{dE_{k/n}}~{\rm Myr}^{-1}]=[{\rm \#part}~{\rm (GeV/n)}^{-1}~{\rm m}^{-3}~{\rm Myr}^{-1}]`.

   - *Differential production* :math:`\frac{d\sigma^{PF}}{dE_{\rm tot}}~[{\rm mb~GeV}^{-1}]`

      Integrates on all incoming projectile energies :math:`E_{k/n}^{\rm in}`, the differential production cross sections of fragment at :math:`E_{k/n}^{\rm out}`:

      .. math::
         \frac{dQ_{\rm sec}^{i\rightarrow j}}{dE_{k/n}^{\rm out}}(E_{k/n}^{\rm out}) \! = \! \int_{0}^{\infty} \!\!\! dE_{k/n}^{\rm in} \;\frac{dn^i}{dE_{k/n}^{\rm in}}(E_{k/n}^{\rm in}) \times \sum_{\rm ISM} \left( n_{\rm ISM} \!\times\! v^{\rm in} \!\times\! A_j \!\times\! \frac{d\sigma^{i+{\rm ISM} \rightarrow j}}{dE_{\rm tot}^{\rm out}}\right).

      The extra factor :math:`A_j` originates from the conversion from differential kinetic energy per nucleon to differential total energy, which is the format of production cross sections files (see :numref:`rst_input_xs_data`).

   - *Straight-ahead approximation for production* :math:`\sigma^{ij}~[{\rm mb}]`

     For nuclei, it is usually assumed that :math:`\frac{d\sigma^{i+{\rm ISM} \rightarrow j}}{dE_{k/n}} = \delta(E_{k/n}^{\rm out}-E_{k/n}^{\rm in})\times\sigma^{ij}`, so that

      .. math::
         \frac{dQ_{\rm sec}^{i\rightarrow j}}{dE_{k/n}}(E_{k/n}) =\frac{dn^i}{dE_{k/n}}(E_{k/n}) \times \sum_{\rm ISM} \left( n_{\rm ISM} \times v \times \sigma^{i+{\rm ISM} \rightarrow j}\right).

   .. note:: The term :math:`q_{\rm sec}^{ij}` is generically calculated in ``TUXSections::SecondaryProduction()``.


Tertiary source :math:`q_{\rm ter}^{i}`
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The tertiary source term (relevant for anti-nuclei only), :math:`q_{\rm ter}^{i}`, is expressed in unit of :math:`[\frac{dn}{dE_{k/n}}~{\rm Myr}^{-1}]=[{\rm \#part}~{\rm (GeV/n)}^{-1}~{\rm m}^{-3}~{\rm Myr}^{-1}]`:


.. math::
   \frac{dQ_{\rm ter}^i}{dE_{k/n}^{\rm out}}(E_{k/n}^{\rm out}) &= \int_{E_{k/n}^{\rm out}}^{\infty} \frac{dn^i}{dE_{k/n}^{\rm in}}(E_{k/n}^{\rm in}) \!\times\! \sum_{\rm ISM} \left( n_{\rm ISM} \!\times\!v^{\rm in} \!\times\! A_i \!\times\! \frac{d\sigma^{i+{\rm ISM} \rightarrow i}}{dE_k^{\rm out}}\right) dE_{k/n}^{\rm in}\\
   &-\frac{dn^i}{dE_{k/n}^{\rm out}}(E_{k/n}^{\rm out}) \!\times\! \sum_{\rm ISM} \left( n_{\rm ISM} \!\times\! v^{\rm out} \!\times\! \sigma^{\rm ina} (E_{k/n}^{\rm out})\right),

with :math:`\sigma^{\rm ina}` the inelastic non-annihilating cross section. The extra factor :math:`A_i` originates from the conversion from total to kinetic energy per nucleon in the differential cross section (see :numref:`rst_input_xs_data`). This equation is solved iteratively (e.g., `Donato et al. 2001 <http://adsabs.harvard.edu/abs/2001ApJ...563..172D>`_).

   .. note:: the term :math:`q_{\rm ter}^{i}` is generically calculated in ``TUXSections::TertiaryProduction()``.


Radioactive source :math:`q_{\rm rad}^{ij}`
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The radioactive progeny from an unstable nucleus gives rise to a radioactive source term :math:`q_{\rm rad}^{ij}`, expressed in unit of :math:`[\frac{dn^i}{dE_{k/n}}~{\rm Myr}^{-1}]=[{\rm \#part}~{\rm (GeV/n)}^{-1}~{\rm m}^{-3}~{\rm Myr}^{-1}]`.

   - :math:`\beta`-decay

      .. math:: \frac{dQ_{\rm rad}^{i\rightarrow j}}{dE_{k/n}}(E_{k/n}) = \frac{dn^i}{dE_{k/n}}(E_{k/n}) \times \Gamma_{\rm rad}^{ij}

    with the decay rate :math:`\Gamma_{\rm rad}^{ij}` in :math:`[{\rm Myr}^{-1}]` related to the unstable nucleus half-life:

      .. math:: \Gamma_{\rm rad}^{ij} = \frac{\ln(2)}{\gamma \times t_{1/2}}.


      .. note:: The term :math:`\Gamma_{\rm rad}^{ij}` is generically calculated in ``TUAxesCrE::GammaRadBETA_perMyr()``.

   - EC-decay [TODO]


.. _rst_semianasol:

Solving for E-derivatives
-------------------------

In semi-analytical models considered below, spatial derivatives of the diffusion equation are solved analytically, so that only energy derivatives remain. The corresponding equation on the differential density :math:`n^{j} \equiv \frac{dn^j}{dE_{k/n}}`, in unit of :math:`[{\rm \#part}~{\rm (GeV/n)}^{-1}~{\rm m}^{-3}~]`, for species :math:`j`, takes the form, for all models considered in the USINE package,

   .. math:: A^{j} n^{j}(E_{k/n}) + \frac{d}{dE} \left( B^{j} n^{j} - C^{j} \frac{dn^{j}}{dE} \right) = Q^{j}(E_{k/n}).


Fluxes are mostly power laws and kinetic energy per nucleon is approximately conserved in nuclear fragmentation reactions (straight-ahead approximation). This is the motivation to solve the above second order differential equation on a logarithmic scale in kinetic energy per nucleon. Using :math:`dE = dEk = \frac{d\ln Ek}{Ek}` and :math:`E_{k/n}=E_k/A`, we get :math:`\frac{d}{dE_{\rm tot}} = \frac{1}{E_k}\frac{d}{d\ln E_{k/n}}`, so that the above equation is rewritten (omitting the indices for the sake of readability)

   .. math:: n + \tilde{\cal A} \frac{d}{d\ln E_{k/n}} \left( \tilde{\cal B} n - \tilde{\cal C} \frac{dn}{d\ln E_{k/n}} \right) = \tilde{\cal S}

with

   .. math::
      \tilde{\cal A} = \frac{1}{A \times E_k},\quad
      \tilde{\cal B} = B,\quad
      \tilde{\cal C} = \frac{C}{E_k},\quad {\rm and~}
      \tilde{\cal S} = \frac{1}{A}.

This equation is solved using a finite difference scheme with boundary conditions. This amounts to a tridiagonal inversion. A detailed description of the chosen boundary conditions, their associated coefficients in the matrix, and the impact on the solution, as well as the stability of the numerical scheme, is provided in Sect. 3.1, App. C, and App D of `Derome et al. (2009) <http://adsabs.harvard.edu/abs/2019arXiv190408210D>`__ respectively.

   .. note:: Below, we provide for each model :math:`A^{j}`, :math:`B^{j}`, :math:`C^{j}`, and :math:`Q^{j}`. In the code, these coefficients are used to calculate :math:`\tilde{\cal A}`, :math:`\tilde{\cal B}`, :math:`\tilde{\cal C}`, :math:`\tilde{\cal S}` on the energy grid, and the tridiagonal inversion is performed with the function ``TUNumMethods::SolveEq_1D2ndOrder_Explicit()``.




Leaky-box model
----------------

Description
~~~~~~~~~~~

The corresponding class is ``TUModel0DLeakyBox``.

   **Assumptions**
      - steady-state
      - no spatial dependence,
      - homogeneous distribution of gas,
      - homogeneous distribution of sources.

   **References for model and solution**
      - Nuclei: `Putze et al., A&A 497, 991 (2009) <http://adsabs.harvard.edu/abs/2009A&A...497..991P>`_,
      - Antinuclei: `Duperray et al., PRD 71, 083013 (2005) <http://adsabs.harvard.edu/abs/2005PhRvD..71h3013D>`_,
      - Leptons (fails): `Jones, PRD 2, 2787 (1970) <http://adsabs.harvard.edu/abs/1970PhRvD...2.2787J>`_
      - Radioactive nuclei (fails): `Prishchep & Ptuskin, ApSS 32, 265 (1975) <http://adsabs.harvard.edu/abs/1975ApSS..32..265P>`_


Free parameters
~~~~~~~~~~~~~~~

   - :math:`n_i~[{\rm cm}^{-3}]`: density of the various elements to calculate the mean mass

      :math:`\displaystyle <n_{\rm ISM}m>~[{\rm g~cm}^{-3}] = \frac{\sum_{i \in {\rm ISM}} (n_i m_i)}{{\cal N}_{\rm Avogadro}}`,

   - :math:`\lambda_{\rm esc}(E)~[{\rm g~cm}^{-2}]`: escape length, related to the escape rate by

      :math:`\displaystyle \tau_{\rm esc}~[{\rm Myr}^{-1}] = \frac{\lambda_{\rm esc}}{v <n_{\rm ISM}m>}`

   - :math:`{\cal V}_A~[{\rm km~s}^{-1}~{\rm kpc}^{-1}]`: pseudo-Alfvénic speed of scatterers for reacceleration, related to a true speed  :math:`V_a` in diffusion models, see `Ptuskin et al. (1994) <http://adsabs.harvard.edu/abs/1994ApJ...431..705S>`_, by (:math:`h` and :math:`L` are the thin disc and diffusion halo size)

      :math:`V_a~[{\rm km~s}^{-1}] ={\cal V}_a\times (hL)^{1/2}`.


Equations and solution
~~~~~~~~~~~~~~~~~~~~~~

The Leaky-box equation can be rewritten as

   .. math:: A^{j} n^{j}(E_{k/n}) + \frac{d}{dE} \left( B^{j} n^{j} - C^{j} \frac{dn^{j}}{dE} \right) = Q^{j}(E_{k/n}),

with (note that quantities are slightly differently defined in `Putze et al. 2009 <http://adsabs.harvard.edu/abs/2009A&A...497..991P>`_)

   .. math::
      n^{j}&~[{\rm \#part}~{\rm (GeV/n)}^{-1}~{\rm m}^{-3}~] \equiv \frac{dn^j}{dE_{k/n}}\\
      A^{j}&~[{\rm Myr}^{-1}] = \frac{1}{\tau_{esc}} + \sum_{\rm ISM} n_{ISM} v^{j} \sigma_{inel}^{j+\rm ISM} + \frac{1}{\tau_{\beta-{\rm decay}}^{j}},\\
      B^{j}&~[{\rm GeV~Myr}^{-1}] = < \frac{dE}{dt}>_{ion,coul.} + (1+\beta^{2}) \frac{{\cal K}_{pp}}{E},\\
      C^{j}&~[{\rm GeV}^2~{\rm Myr}^{-1}] = \beta^{2} \times {\cal K}_{pp},\\
      Q^{j}&~[{\rm \#part}~{\rm (GeV/n)}^{-1}~{\rm m}^{-3}~{\rm Myr}^{-1}] = q_{\rm prim}^{j} + \sum_{m_i>m_j} q_{\rm sec}^{ij} + q_{\rm tert}^{j} + \sum_{i \in \beta}q_{\rm rad}^{ij}.


**Implementation in USINE (specific to LB)**

The default dependence for the escape length and pseudo-diffusion coefficients is:

   .. math::
      \lambda_{\rm esc}~&[{\rm g~cm}^{-2}] \propto \lambda_0 R^{-\delta},\\
      {\cal K}_{pp}~&[{\rm GeV}^2~{\rm Myr}^{-2}]=\frac{4}{3} {\cal V}_a^2\beta^2 E^2 \frac{\tau_{\rm esc}}{\delta(4-\delta^2)(4-\delta)}.

However, because of implementation constraint in USINE, one must define a diffusion coefficient in space :math:`K_{00}` and in momentum :math:`K_{pp}` (in USINE parameter file), and we chose to define:

   .. math::
      K_{00}^{\rm USINE} &\equiv \lambda_{\rm esc}~[{\rm g~cm}^{-2}],\\
      K_{pp}^{\rm USINE} &\equiv {\cal K}_{pp} / \tau_{\rm esc},

**Solution**

The Leaky-box equation is solved numerically, as explained in :ref:`rst_semianasol`.



Standard 1D diffusion model
---------------------------

Description
~~~~~~~~~~~

The corresponding class is ``TUModel1DKisoVc``.

   **Assumptions**
      - steady-state
      - 1D geometry: thin plan (half-height :math:`h`) + halo (half-height :math:`L\gg h`)
      - homogeneous distribution of gas and sources in thin plan,
      - isotropic and spatial-independent diffusion in plan and halo,
      - reacceleration in thin plan only,
      - constant wind in halo (implies discontinuity in plan).

   **References for model and solution**
      - Nuclei: `Putze et al., A&A 516, 66 (2010) <http://adsabs.harvard.edu/abs/2010A&A...516A..66P>`_
      - Impact of local underdensity: `Donato et al., A&A 381, 539 (2002) <http://adsabs.harvard.edu/abs/2002A&A...381..539D>`_ and `Putze et al., A&A 516, 66 (2010) <http://adsabs.harvard.edu/abs/2010A&A...516A..66P>`_


Free parameters
~~~~~~~~~~~~~~~

   - :math:`n_i~[{\rm cm}^{-3}]`: density of the various elements in thin plan
   - :math:`K(E)~[{\rm kpc}^2~{\rm Myr}^{-1}]`: spatial diffusion coefficient (isotropic and homogeneous)
   - :math:`K_{pp}(E)~[{\rm GeV}^2~{\rm Myr}^{-1}]`: momentum diffusion coefficient in thin plan
   - :math:`V_A~[{\rm km~s}^{-1}]`: Alfvénic speed in thin plan for reacceleration
   - :math:`V_c~[{\rm km~s}^{-1}]`: constant velocity in the halo (perpendicular to plan)
   - :math:`L~[{\rm kpc}]`: diffusive halo half-height
   - :math:`r_h~[{\rm kpc}]`: size of local underdense region (flux of radioactive nuclei suppressed)


Equations and solution
~~~~~~~~~~~~~~~~~~~~~~

The model solution for a species :math:`j` is analytical in the halo, and numerical in the thin disc 
(see its resolution in :ref:`rst_semianasol`):

   .. math::
      n^j(z) &=  n^j(z=0) \times \exp^{(V_{c}z/2K)}  \frac{\sinh(S^j(L-z)/2)}{\sinh(S^jL/2)},\\
      A^j n^j(z=0) &+ 2h \times \frac{d}{dE} \left( B^j n^j(z=0) - C^j \frac{dn^j}{dE}(z=0) \right) = 2h \times Q^{j}(z=0),

with (:math:`z` is the vertical coordinate so that :math:`z=0` in in the thin disc)

   .. math::
      n^j(z)&~[{\rm \#part}~{\rm (GeV/n)}^{-1}~{\rm m}^{-3}~] \equiv \frac{dn^j}{dE_{k/n}}(z)\\
      A^j&~[{\rm kpc~Myr}^{-1}] = V_{c} + 2h\Gamma^{j}_{\rm inel} + K S^{j}\coth\left(\frac{S^{j}L}{2}\right),\\
      S^j&~[{\rm Myr}^{-1}] = \sqrt{\frac{V_{c}^{2}}{K^{2}} + 4 \frac{\Gamma_{\rm rad}^{j}}{K}},\\
      B^{j}&~[{\rm GeV~Myr}^{-1}] = < \frac{dE}{dt}>_{\rm ion,coul.,adiab.} + (1+\beta^{2}) \frac{K_{pp}}{E},\\
      C^j&~[{\rm GeV}^2~{\rm Myr}^{-1}] = \beta^{2} \times {\cal K}_{pp},\\
      Q^j(z=0)&~[{\rm \#part}~{\rm (GeV/n)}^{-1}~{\rm m}^{-3}~{\rm Myr}^{-1}] = q_{\rm prim}^{j} + \sum_{m_i>m_j} q_{\rm sec}^{ij} + q_{\rm tert}^{j} + \sum_{i \in \beta}q_{\rm rad}^{ij}.

   .. note:: Radioactive nuclei also decay in the halo and contribute to a complicated term not described above (see `Putze et al. 2010 <http://adsabs.harvard.edu/absPutze2010A&A...516A..66P>`_).

**Default parameters**

The default dependence for the spatial and momentum diffusion coefficients are:

   .. math::
      K(R)~&[{\rm kpc}^2~{\rm Myr}^{-1}] = \beta K_0 R^\delta\\
      K_{pp}~&[{\rm GeV}^2~{\rm Myr}^{-1}]=\frac{4}{3} V_A^2\beta^2 E^2 \frac{1}{\delta(4-\delta^2)(4-\delta)K(R)}.


Standard 2D diffusion model
---------------------------

Description
~~~~~~~~~~~

The model is very similar to its 1D counterpart. The corresponding class is ``TUModel2DKisoVc``.

   **Assumptions**
      - steady-state
      - 2D geometry: thin disc (half-height :math:`h`) and cylindrical halo (half-height :math:`L\gg h`) of radius :math:`R`,
      - homogeneous distribution of gas in thin disc,
      - radially-dependent distribution of sources in thin disc,
      - isotropic and spatial-independent diffusion in disc and halo,
      - reacceleration in thin disc only,
      - constant wind in halo (implies discontinuity in disc).

   **References for model and solution**
      - Standard sources in disc (nuclei and anti-nuclei): `Maurin et al., ApJ 555, 585 (2001) <http://adsabs.harvard.edu/abs/2001ApJ...555..585M>`_, `Donato et al., ApJ 563, 172 (2001) <http://adsabs.harvard.edu/abs/2001ApJ...563..172D>`_, and `Putze et al., A&A 516, 66 (2010) <http://adsabs.harvard.edu/abs/2010A&A...516A..66P>`_; beware of typos in that last paper,
      - Exotic sources in halo: `Barrau et al., A&A 388, 676 (2002) <http://adsabs.harvard.edu/abs/Barrau2002A&A...388..676B>`_,
      - Impact of local underdensity: `Donato et al., A&A 381, 539 (2002) <http://adsabs.harvard.edu/abs/2002A&A...381..539Df>`_ and `Putze et al., A&A 516, 66 (2010) <http://adsabs.harvard.edu/abs/2010A&A...516A..66P>`_.

Free parameters
~~~~~~~~~~~~~~~

   - :math:`n_i~[{\rm cm}^{-3}]`: density of the various elements in thin disc
   - :math:`K(E)~[{\rm kpc}^2~{\rm Myr}^{-1}]`: spatial diffusion coefficient (isotropic and homogeneous)
   - :math:`K_{pp}(E)~[{\rm GeV}^2~{\rm Myr}^{-1}]`: momentum diffusion coefficient in thin disc
   - :math:`V_A~[{\rm km~s}^{-1}]`: Alfvénic speed in thin disc for reacceleration
   - :math:`V_c~[{\rm km~s}^{-1}]`: constant velocity in the halo (perpendicular to disc)
   - :math:`L~[{\rm kpc}]`: diffusive halo half-height
   - :math:`r_h~[{\rm kpc}]`: size of local underdense region (flux of radioactive nuclei suppressed)
   - :math:`R~[{\rm kpc}]`: radius of the cylindrical box


Equations and solution
~~~~~~~~~~~~~~~~~~~~~~

An expansion along the first order Bessel function is performed

   .. math:: n(r,z) = \sum_{i=1}^{\infty} n_i(z) J_0\left(\zeta_i \frac{r}{R}\right),

with :math:`\zeta_i` the :math:`i`-th zero of the zero-th order Bessel function :math:`J_0`. This automatically ensures the boundary condition :math:`n(r=R,z)=0`. Each Bessel coefficient :math:`n_i(z)` follows an equation very similar to that of the 1D version of the model.

The model solution for Bessel order :math:`i` of a species :math:`j` is analytical in the halo, and numerical in the thin disc (see its resolution in :ref:`rst_semianasol`)

   .. math::
      n_i^j(z) &=  n_i^j(z=0) \times \exp^{(V_{c}z/2K)}  \frac{\sinh(S_i^j(L-z)/2)}{\sinh(S_i^jL/2)},\\
      A_i^j n_i^j(z=0) &+ 2h \times \frac{d}{dE} \left( B^j n_i^j(z=0) - C^j \frac{dn_i^j}{dE}(z=0) \right) = 2h \times Q_i^j(z=0),

with (:math:`z` is the vertical coordinate so that :math:`z=0` in in the thin disc)

   .. math::
      n_i^j(z)&~[{\rm \#part}~{\rm (GeV/n)}^{-1}~{\rm m}^{-3}~] \equiv \frac{dn_i^j}{dE_{k/n}}(z)\\
      A_i^j&~[{\rm kpc~Myr}^{-1}] = V_{c} + 2h\Gamma^{j}_{\rm inel} + K S_i^j\coth\left(\frac{S^{j}L}{2}\right),\\
      S^j&~[{\rm Myr}^{-1}] = \sqrt{\frac{V_{c}^{2}}{K^{2}} + (2\zeta_i/R)^2 + 4 \frac{\Gamma_{\rm rad}^{j}}{K}},\\
      B^j&~[{\rm GeV~Myr}^{-1}] = < \frac{dE}{dt}>_{\rm ion,coul.,adiab.} + (1+\beta^{2}) \frac{K_{pp}}{E},\\
      C^j&~[{\rm GeV}^2~{\rm Myr}^{-1}] = \beta^{2} \times {\cal K}_{pp},\\
      Q_i^j(z=0)&~[{\rm \#part}~{\rm (GeV/n)}^{-1}~{\rm m}^{-3}~{\rm Myr}^{-1}] = q_{i,\,\rm prim}^j + \sum_{m_k>m_j} q_{i,\,\rm sec}^{kj} + q_{i,\,\rm tert}^{j} + \sum_{k \in \beta}q_{i,\,\rm rad}^{kj},

where :math:`q_i` terms are Bessel-Fourier counterparts (or expansions) of the source term.

   .. note:: Radioactive nuclei also decay in the halo and contribute to a complicated term not described above (see `Putze et al. 2010 <http://adsabs.harvard.edu/absPutze2010A&A...516A..66P>`_).

**Default parameters**

The default dependence for the spatial and momentum diffusion coefficients are:

   .. math::
      K(R)~&[{\rm kpc}^2~{\rm Myr}^{-1}] = \beta K_0 R^\delta\\
      K_{pp}~&[{\rm GeV}^2~{\rm Myr}^{-1}]=\frac{4}{3} V_A^2\beta^2 E^2 \frac{1}{\delta(4-\delta^2)(4-\delta)K(R)}.
