.. _rst_syntax_base_listofcrs:

Base\@ListOfCRs
~~~~~~~~~~~~~~~~

Selection of atomic and nuclear charts to pick CRs from, as well as the specification of propagated CRs and their parents.

.. tabularcolumns:: |p{6.5cm}|p{9.cm}|

.. csv-table::
   :header: "Group\@subgroup\@parameter", "Description"
   :widths: 30, 70
   :align: center

   "*Base\@ListOfCRs\@fAtomicProperties* (M=0)", "Path [1]_ to file of atomic properties for elements, e.g. `$USINE/inputs/atomic_properties.dat <../../../inputs/atomic_properties.dat>`_. More on the format in :ref:`rst_input_atomprop`."
   "*Base\@ListOfCRs\@fChartsForCRs* (M=0)", "Path [1]_ to file of CR charts (mass, A, Z, half-life, ghosts...), e.g. `$USINE/inputs/crcharts_Zmax30_ghost97.dat <../../../inputs/crcharts_Zmax30_ghost97.dat>`_. More on the format in :ref:`rst_input_cr_charts`."
   "*Base\@ListOfCRs\@IsLoadGhosts* (M=0)", "Whether (true) or not (false) to load ghosts nuclei (used only for XS production calculations)."
   "*Base\@ListOfCRs\@SSRelativeAbund* (M=0)", "Path [1]_ to file of Solar system relative abundances, e.g. `$USINE/inputs/solarsystem_abundances2003.dat <../../../inputs/solarsystem_abundances2003.dat>`_. More on the format in :ref:`rst_input_src_abund`."
   "*Base\@ListOfCRs\@ListOfCRs* (M=0)", "Selection among CRs listed in *fChartsForCRs*:

     | - **[CR1,CR2]** → all CRs in range, e.g. ``[2H-bar,30Si]``
     | - **CR1,CR2,CR2** → list of selected CRs
     | - **ALL** → all CRs in *fChartsForCRs*"
   "*Base\@ListOfCRs\@ListOfParents* (M=0)", "CR1,CR2:CR3,CR4;CR5:PRIMARY** selects as parents for CRs in **ListOfCRs** CR3 and CR4 as the only parents for CR1 and CR2 + no parent for CR5 + all heavier nuclei for unlisted CRs (keyword **DEFAULT** sets all parents to be heavier nuclei, whereas **PRIMARIES** set all CRs as pure primaries (no parents). For instance, set ``2H-bar:1H-bar,1H,4He;1H-bar:1H,4He`` to only have 1H-bar, 1H, and 4He (resp. 1H, 4He) as parents for 2H-bar (resp. 1H-bar)."
   "*Base\@ListOfCRs\@PureSecondaries* (M=0)", "Comma-separated list of isotopes and/or elements forced to be pure secondaries, e.g. ``Li,Be,B``. The associated source terms (for each isotope) will be set and fixed to 0 for the calculation whatever the run configuration."

**Footnote**

.. [1] Environment variables in path (e.g. ``$USINE/``) are enabled and correctly interpreted