.. _rst_keywords_group_base:

Group = ``Base``
^^^^^^^^^^^^^^^^

The syntax of the associated parameters is detailed in :numref:`rst_syntax_base`.

.. _tab_base:

.. table:: Base ingredients (group=Base) for propagation models.

   ========================  =====================================================
   Group\@subgroup           Description
   ========================  =====================================================
   *Base\@CRData*            CR data and normalisation
   *Base\@EnergyGrid*        To define energy grids for (anti-)nuclei
   *Base\@ListOfCRs*         To define propagated CRs (charts, ghosts, parents)
   *Base\@MediumCompo*       List and abundances of species in medium (e.g., H,He)
   *Base\@XSections*         List of XS files
   ========================  =====================================================