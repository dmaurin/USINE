.. _rst_tutorial_runexamples_m:

Fits: ``./bin/usine -m``
~~~~~~~~~~~~~~~~~~~~~~~~

Minimisation run with fit parameters, nuisance parameters, and covariance matrix. The command

   ``./bin/usine -m2  inputs/init.TEST.par  $USINE/output_fit   1    1   1``

produces many ASCII files:

   - output log file (details of run steps): *$USINE/output/usine.last_run.log*,
   - initialisation file for last run: *$USINE/output/usine.last_run.init.par*,
   - result of the fit (also reported in *$USINE/output/usine.last_run.log*)::

             /---------------- ANALYSIS RESULT ----------------\
             | chi2_min=5.890260e+01 for 55 data and 25 pars (20 nuisance)
             | => chi2/dof(=50)=1.178052e+00
             \-------------------------------------------------/


      Minimization status: 1  ->  Covariance was made pos defined

      Covariance matrix status: 2  ->  full matrix but forced pos def

                 - Best-fit parameters after minimisation
                                alpha_NUISANCE = +2.296585e+00(+/-5.78e-02) [-] from alpha_NUISANCE_init=[+1.7e+00,+2.5e+00],+2.3e+00,+1.0e-01
                                         eta_t = +4.785572e-01(+/-7.18e-02) [-] from eta_t_init=[-3.0e+00,+3.0e+00],+0.0e+00,+1.0e-01
                                         delta = +6.000460e-01(+/-2.43e-03) [-] from delta_init=[+2.0e-01,+9.0e-01],+6.0e-01,+2.0e-02
                                            Va = +8.221438e+01(+/-9.53e-01) [km/s] from Va_init=[+1.0e+00,+1.2e+02],+7.0e+01,+1.0e-01
                                            Vc = +1.796892e+01(+/-2.07e-01) [km/s] from Vc_init=[+0.0e+00,+7.0e+01],+2.0e+01,+1.0e-01
                                      log10_K0 = -1.407546e+00(+/-3.65e-03) [kpc^2/Myr] from log10_K0_init=[-2.5e+00,-5.0e-01],-1.7e+00,+1.0e-01
                     phi_AMS02_201105201605__NUISANCE = +9.955319e-01(+/-8.12e-02) [GV] from phi_AMS02_201105201605__NUISANCE_init=[+3.0e-01,+1.1e+00],+7.3e-01,+2.0e-01
                     SCALE_AMS02_201105201605__BC_NUISANCE = +7.213464e-02(+/-9.70e-01) [-] from SCALE_AMS02_201105201605__BC_NUISANCE_init=[-1.0e+01,+1.0e+01],+0.0e+00,+1.0e+00
                     UNF_AMS02_201105201605__BC_NUISANCE = +1.204838e-02(+/-7.70e-01) [-] from UNF_AMS02_201105201605__BC_NUISANCE_init=[-1.0e+01,+1.0e+01],+0.0e+00,+1.0e+00
                     BACK_AMS02_201105201605__BC_NUISANCE = -1.652323e-03(+/-8.03e-01) [-] from BACK_AMS02_201105201605__BC_NUISANCE_init=[-1.0e+01,+1.0e+01],+0.0e+00,+1.0e+00
                     ACC_AMS02_201105201605__BC_NUISANCE = -1.480676e+00(+/-2.14e-01) [-] from ACC_AMS02_201105201605__BC_NUISANCE_init=[-1.0e+01,+1.0e+01],+0.0e+00,+1.0e+00
                     EnhancePowHE_ALL_NUISANCE = +9.527955e-01(+/-8.36e-01) [-] from EnhancePowHE_ALL_NUISANCE_init=[+0.0e+00,+2.0e+00],+1.0e+00,+1.0e+00
                           Norm_16O+H_NUISANCE = +9.996114e-01(+/-4.77e-02) [-] from Norm_16O+H_NUISANCE_init=[+7.0e-01,+1.3e+00],+1.0e+00,+5.0e-02
                           Norm_12C+H_NUISANCE = +1.001394e+00(+/-4.49e-02) [-] from Norm_12C+H_NUISANCE_init=[+7.0e-01,+1.3e+00],+1.0e+00,+5.0e-02
                           Norm_11B+H_NUISANCE = +9.943976e-01(+/-1.98e-02) [-] from Norm_11B+H_NUISANCE_init=[+7.0e-01,+1.3e+00],+1.0e+00,+5.0e-02
                           Norm_10B+H_NUISANCE = +9.978475e-01(+/-3.22e-02) [-] from Norm_10B+H_NUISANCE_init=[+7.0e-01,+1.3e+00],+1.0e+00,+5.0e-02
                      Norm_16O+H->11B_NUISANCE = +1.034031e+00(+/-3.45e-02) [-] from Norm_16O+H->11B_NUISANCE_init=[+5.0e-01,+2.0e+00],+1.0e+00,+2.0e-01
                     EAxisScale_16O+H->11B_NUISANCE = +1.001521e+00(+/-7.12e-01) [-] from EAxisScale_16O+H->11B_NUISANCE_init=[+5.0e-01,+1.5e+00],+1.0e+00,+5.0e-01
                     EThresh_16O+H->11B_NUISANCE = +1.033162e+00(+/-4.36e-01) [-] from EThresh_16O+H->11B_NUISANCE_init=[+5.0e-01,+2.0e+00],+1.0e+00,+5.0e-01
                     SlopeLE_16O+H->11B_NUISANCE = +1.154792e-01(+/-2.69e-01) [-] from SlopeLE_16O+H->11B_NUISANCE_init=[-1.0e+00,+1.0e+00],+0.0e+00,+5.0e-01
                      Norm_12C+H->11B_NUISANCE = +1.072120e+00(+/-1.74e-02) [-] from Norm_12C+H->11B_NUISANCE_init=[+5.0e-01,+2.0e+00],+1.0e+00,+2.0e-01
                     EAxisScale_12C+H->11B_NUISANCE = +9.761634e-01(+/-4.07e-01) [-] from EAxisScale_12C+H->11B_NUISANCE_init=[+5.0e-01,+1.5e+00],+1.0e+00,+5.0e-01
                     EThresh_12C+H->11B_NUISANCE = +1.158264e+00(+/-2.31e-01) [-] from EThresh_12C+H->11B_NUISANCE_init=[+5.0e-01,+2.0e+00],+1.0e+00,+5.0e-01
                     SlopeLE_12C+H->11B_NUISANCE = +2.986691e-01(+/-1.27e-01) [-] from SlopeLE_12C+H->11B_NUISANCE_init=[-1.0e+00,+1.0e+00],+0.0e+00,+5.0e-01
                     HalfLifeBETA_10Be_NUISANCE = +1.387318e+00(+/-1.17e-02) [Myr] from HalfLifeBETA_10Be_NUISANCE_init=[+1.3e+00,+1.5e+00],+1.4e+00,+1.2e-02

   - Plots  and ROOT macros (.C), pdf and png for these plots.


.. figure:: DocImages/option_m/local_fluxes_BC_R.png
   :width: 65%
   :align: center

   Best-fit obtained along with the data selected for display. If nuisance parameters are used for systematics errors of the fit data, shown are the model (thick line) and the biased model (thin line) to be compared to the data, with :math:`{\rm bias}(R_k) = \Pi_{l=0\dots n} (1+\nu_l \times \sigma_l(R_k))`, see :numref:`tab_fitparam`.

.. figure:: DocImages/option_m/local_fluxes_BC_R_residual.png
   :width: 65%
   :align: center

   Residual between (biased)-model and data selected for the fit. The error bars correspond to the relative data errors.

.. figure:: DocImages/option_m/xsec_sig_ref_vs_nuisance.png
   :width: 65%
   :align: center

   If specific cross sections are enabled as nuisance parameters (see parameter *UsineRun\@FitFreePars\@XSection* in :numref:`tab_fitparam`), show the pre-fit (thin lines) and post-fit XS (thick lines) for comparison.