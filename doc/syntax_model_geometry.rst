.. _rst_syntax_model_geometry:

*Model...\@Geometry*
~~~~~~~~~~~~~~~~~~~~

   Geometry, coordinate system, and Sun's position for the model.

   .. warning:: ``Geometry`` and ``ISM`` parameter names and units are uniquely associated to the implemented semi-analytical models and cannot be modified.

   .. note:: All *Model...\@Geometry\@ParNames* values are enabled as free parameters (see :ref:`rst_tutorial_update_fitpars`)

.. tabularcolumns:: |p{5.7cm}|p{9.9cm}|

.. csv-table::
   :header: "Group\@subgroup\@parameter", "Description"
   :widths: 30, 70
   :align: center

   "*Model...\@Geometry\@ParNames* (M=0)", "See :numref:`tab_parnames` (e.g., ``L,h,R,rhole`` for ``Model2DKisoVc``)"
   "*Model...\@Geometry\@ParUnits* (M=0)", "See :numref:`tab_parnames` (e.g., ``kpc,kpc,kpc,kpc`` for ``Model2DKisoVc``)"
   "*Model...\@Geometry\@ParVals* (M=0)", "See :numref:`tab_parnames`"
   "*Model...\@Geometry\@TAxis* (M=0)", "**name_axis:[min,max],nbins,X** with X=LOG or LIN (set to ``-`` if no time coordinate) for time axis [Myr]: **min** and **max** can be *ParNames* params (i.e., enabled for minimisation"
   "*Model...\@Geometry\@XAxis* (M=0)", "Idem *TAxis* for 1st spatial axis, e.g. ``r:[0,R],10,LIN`` in [kpc]"
   "*Model...\@Geometry\@YAxis* (M=0)", "Idem *TAxis* for 2nd spatial axis, e.g. ``z:[0,L],10,LIN`` in [kpc]"
   "*Model...\@Geometry\@ZAxis* (M=0)", "Idem *TAxis* for 3rd spatial axis, e.g. ``-`` (if model is only 2D)"
   "*Model...\@Geometry\@XSun* (M=0)", "*XAxis* Solar system position, e.g. ``8.5`` [kpc]"
   "*Model...\@Geometry\@YSun* (M=0)", "*YAxis* Solar system position, e.g. ``0.`` [kpc]"
   "*Model...\@Geometry\@ZSun* (M=0)", "*ZAxis* Solar system position (``-`` if no coordinate)"