.. _rst_syntax:

Parameter value syntax (init. file)
===================================

We list below group and subgroup keywords, as well as parameter values required in the initialisation file (see :ref:`rst_initfile`).

.. _rst_groups:

Group/subgroup keywords
--------------------------------------------------------------------------

We list below all the subgroups and the associated groups used in USINE.

   .. note:: ``TemplSpectrum`` and ``TemplSpatialDist`` are the only groups for which the user can modify the subgroup names: the latter define template names that can be referred to for instance for source spectra. See :ref:`rst_tutorial_update_models`.

.. toctree::
   :maxdepth: 1

   keywords_group_base
   keywords_group_extra
   keywords_group_models
   keywords_group_solmod
   keywords_group_templates
   keywords_group_usinerun
   keywords_group_usinefit

.. _rst_params:

Parameters
--------------------------------------------------------------------------

We recall that the syntax of a parameter is ``group @ subgroup @ parameter @ M=? @ value``, and we specify here in particular,

   - whether a parameter is multi-valued (M=1) or not (M=0)
   - the parameter's usage with its **syntax** and/or [unit] and/or ``example``


.. toctree::
   :maxdepth: 1

   syntax_base
   syntax_extra
   syntax_model
   syntax_solmod
   syntax_templ
   syntax_usinerun
   syntax_usinefit