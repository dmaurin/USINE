.. _rst_general_release4.0:

Notes for V4.0 (xx/20xx)
~~~~~~~~~~~~~~~~~~~~~~~~

.. note:: V4.0 is being developed with Mathieu Boudaud (to include the propagation of electrons and positrons) and Nathanael Weinrich (for python interface and MCMC). We are doing our best for a release in 2019, so stay tuned!


**Wish list**

   - *Physics*

      - :math:`e^\pm` based on Mathieu's `pinching approach <http://adsabs.harvard.edu/abs/2017A%26A...605A..17B>`_
      - Dark matter contributions for anti-nuclei and positrons (in the 2D model)
      - Enable splines as alternative to 'formulae'
      - Extension of CR list and XS files for Z>30 CRs
      - Proper inclusion of EC-decay CRs
      - 1D spherically symmetric Solar modulation model

   - *Technical*

      - Implement plot for rotated residuals and scores if covariance matrix of errors (as used in `Boudaud et al. 2019 <https://arxiv.org/abs/1906.07119>`__)
      - Implement :math:`V_a` stability condition from `Derome et al. (2019) <https://ui.adsabs.harvard.edu/abs/2019arXiv190408210D/abstract>`__
      - Python wrappers for USINE \& .yaml files for initialisation
      - Interface with MCMC and other tools
      - Use profiler to inspect code
      - Parallelize loop
      - Add spatial dependent plots for quantities


