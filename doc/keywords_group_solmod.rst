.. _rst_keywords_group_solmod:

Group = ``SolMod...``
^^^^^^^^^^^^^^^^^^^^^

It includes the groups ``SolMod0DFF`` and ``SolMod1DSpher``. The syntax of the associated parameters is detailed in :numref:`rst_syntax_solmod`.

.. _tab_solmod:

.. table:: Mandatory subgroups for Solar modulation models.

   ========================  ======================================
   Group\@subgroup           Description
   ========================  ======================================
   *SolMod...\@Base*         Base parameters specific to propagation model (if needed)
   *SolMod...\@Geometry*     Geometry and Solar cavity coordinates (if needed)
   *SolMod...\@Transport*    Transport parameters in Solar cavity (if needed)
   ========================  ======================================