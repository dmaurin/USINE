.. _rst_keywords_group_usinerun:

Group = ``UsineRun``
^^^^^^^^^^^^^^^^^^^^

The syntax of the associated parameters is detailed in :numref:`rst_syntax_usinerun`.

.. _tab_usinerun:

.. table:: Specific for propagation runs (subset of data, displays...).

   =========================  ======================================
   Group\@subgroup            Description
   =========================  ======================================
   *UsineRun\@Calculation*    Precision and boundary conditions for run
   *UsineRun\@Display*        User-selected subset of CR data for display
   *UsineRun\@Models*         Propagation and modulation models for the run
   *UsineRun\@OnOff*          Switches for propagation effects (E losses, wind, etc.)
   =========================  ======================================