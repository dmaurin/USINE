.. _rst_syntax_base_xsections:

Base\@XSections
~~~~~~~~~~~~~~~

Choice of the XS files used for propagation. See :ref:`rst_input_xs_data` for more details.

.. warning:: XS are associated to CR list and parents (*ListOfCRs* and *ListOfParents* in :ref:`rst_syntax_base_listofcrs`), but also ISM targets (:ref:`rst_syntax_base_mediumcompo`) selected for propagation. Inelastic XS files must encompass all propagated CRs on all targets, whereas production XS files must encompass all parents/CRs/targets selected combinations (see :ref:`rst_input_xs_data` for the file formats). USINE internally checks that all required XS are filled and tells you which ones are missing!

.. tabularcolumns:: |p{6.cm}|p{9.6cm}|

.. csv-table::
   :header: "Group\@subgroup\@parameter", "Description"
   :widths: 30, 70
   :align: center

   "*Base\@XSections\@fProd* (M=1)", "Path [1]_ to file for production XS, either straight-ahead (e.g., `$USINE/inputs/XS_NUCLEI/sigProdGALPROP17_OPT12.dat <../../../inputs/XS_NUCLEI/sigProdGALPROP17_OPT12.dat>`_) or differential (e.g., `$USINE/inputs/XS_ANTINUC/dSdEProd_pbar_1H4He+HHe_Donato01.dat <../../../inputs/XS_ANTINUC/dSdEProd_pbar_1H4He+HHe_Donato01.dat>`_)."
   "*Base\@XSections\@fTotInelAnn* (M=1)", "Path [1]_ to file for total inelastic (annihilating for anti-nuclei) XS, e.g. `$USINE/inputs/XS_NUCLEI/sigInelTripathi99.dat <../../../inputs/XS_NUCLEI/sigInelTripathi99.dat>`_ or `$USINE/inputs/XS_ANTINUC/sigInelANN_pbar+HHe_Donato01.dat <../../../inputs/XS_ANTINUC/sigInelANN_pbar+HHe_Donato01.dat>`_."
   "*Base\@XSections\@Tertiaries* (M=0)", "Comma-separated list of CRs for which to account for inelastic non-annihilating (*fInelNonAnn*) rescattering, e.g. ``1H-bar,2H-bar`` (use **NONE** for empty selection)."
   "*Base\@XSections\@fdSigdENAR* (M=1)", "Path [1]_ to file for differential inelastic non-annihilating XS (one file per *Tertiaries*), e.g. `$USINE/inputs/XS_ANTINUC/dSdENAR_pbar+HHe_Duperray05_Anderson.dat <../../../inputs/XS_ANTINUC/dSdENAR_pbar+HHe_Duperray05_Anderson.dat>`_."
   "*Base\@XSections\@fTotInelNonAnn* (M=1)", "Path [1]_ to file for total inelastic non-annihilating XS (one file per *Tertiaries*), e.g. `$USINE/inputs/XS_ANTINUC/sigInelNONANN_pbar+HHe_Donato01.dat <../../../inputs/XS_ANTINUC/sigInelNONANN_pbar+HHe_Donato01.dat>`_."

**Footnote**

.. [1] Environment variables in path (e.g. ``$USINE/``) are enabled and correctly interpreted