.. _rst_syntax_base:

*Base* params
--------------------------------------------------------------------------

The subgroups of the group *Base* are shortly described in :numref:`tab_base`. We list below the parameters for each subgroup.

.. toctree::
   :maxdepth: 1

   syntax_base_crdata
   syntax_base_energygrid
   syntax_base_listofcrs
   syntax_base_mediumcompo
   syntax_base_xsections