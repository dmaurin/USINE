.. _rst_syntax_base_energygrid:

Base\@EnergyGrid
~~~~~~~~~~~~~~~~

Different families of species (nuclei and anti-nuclei) are calculated on different kinetic energy per nucleon ranges, :math:`[E_{\rm k/n}^{\rm min}-E_{\rm k/n}^{\rm max}]`, all on :math:`N_{\rm bins}` logarithmically-spaced bins:

:math:`E_{\rm k/n}^i = E_{\rm k/n}^{\rm min} \times \Delta^i, \quad{\rm with}\;\; \Delta = \left(\frac{E_{\rm k/n}^{\rm max}}{E_{\rm k/n}^{\rm min}}\right)^{1/(N_{\rm bins} - 1)}.`

For anti-nuclei, integrations are involved (primary and tertiary source terms), and a doubling step integration is implemented to check the convergence on the available grid: the optimal number of bins to take is then
:math:`N_{\rm bins} = 2^j + 1.`

==========================================  ============================
Group\@subgroup\@parameter                  Description
==========================================  ============================
*Base\@EnergyGrid\@NBins* (M=0)             # of E bins for all CRs (:math:`2^j+1` optimal for integrations)
*Base\@EnergyGrid\@NUC_EknRange* (M=0)      [Ekn_min,Ekn_max] for nuclei [GeV/n]
*Base\@EnergyGrid\@ANTINUC_EknRange* (M=0)  [Ekn_min,Ekn_max] for anti-nuclei [GeV/n]
==========================================  ============================

   | → Always check that your results is stable when varying Nbins!
   | → Ensure that your results do not depend on E range selected!

.. note:: The energy range for nuclei should preferably be larger than the energy range for anti-nuclei. Indeed, a 100 GeV/n secondary anti-proton may be created by a 10 TeV/n proton, so that using the same energy range for both species would miss most of the production. Ekn_max (*ANTINUC_EknRange*) should be at least 10-100 times Ekn_max (*NUC_EknRange*), otherwise, the high-energy antinucleus flux will be underestimated.