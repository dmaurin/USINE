.. _rst_tutorial_runexamples_e:

Extra plots: ``./bin/usine -e``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

With the command

   ``./bin/usine -e   inputs/init.TEST.par  $USINE/output   1   1   1``,

you enter an interactive session, in which you can loop on many options (after display pop-up, to go back to the interactive session, you have to quit ROOT properly [1]_, as illustrated in :numref:`fig_quitroot`.

   - *Option A* → local fluxes and spatial distributions

      - **A1**: similar to ``./bin/usine -l`` but interactively;
      - **A2**: CR source vs Solar system abundances and FIP vs volatility (:numref:`fig_A2`)
      - **A3**: spatial distribution if model allows (TODO!)

   - *Option B* → edit parameters interactively and rerun (any option) with new parameters.

   - *Option C* → print on screen info on status of parameters (transport, source, CR lists, etc.).

   - *Option D* → extra useful plots and calculations!

      - **D0**: relative prim/sec/rad contributions in isotopes and elements (:numref:`fig_D0`);
      - **D1** and **D2**: tables for the ranking of XS, as published in `Génolini et al. (2018) <http://adsabs.harvard.edu/abs/2018arXiv180304686G>`_;
      - **D3** and **D4**: impact of decay on combos progenitor/daughter (:numref:`fig_D3`);
      - **D5**: source terms for differential production (:numref:`fig_D5`)
      - **D6**: separated contributions for anti-nucleus flux (:numref:`fig_D6`)
      - **D7**: contributions per energy range for anti-nucleus flux (:numref:`fig_D7`)
      - **D8**: tertiary contributions for anti-nucleus flux (:numref:`fig_D8`)

   - *Option E* → comparison plots varying some input ingredients

      - **E1**: varying boundary conditions
      - **E2**: switch-on/off propagation effects, e.g. losses, decay, etc. (:numref:`fig_E2`)
      - **E3**: impact of inelastic XS from `XS_NUCLEI/ <../../../inputs/XS_NUCLEI/>`_\sigInel\* files (:numref:`fig_E3`)
      - **E4**: impact of production XS from `XS_NUCLEI/ <../../../inputs/XS_NUCLEI/>`_\sigSpal\* files (:numref:`fig_E4`)
      - **E5**: impact of inelastic XS from `XS_ANTINUC/ <../../../inputs/XS_ANTINUC/>`_\sigInel\* files)
      - **E6**: impact of production XS from `XS_ANTINUC/ <../../../inputs/XS_ANTINUC/>`_\dSdE\* files)
      - **E7**: impact of inelastic non-annihil. XS from `XS_ANTINUC/ <../../../inputs/XS_ANTINUC/>`_\sigInelNONANN\* files)
      - **E8**: impact of tertiary redistribution XS from `XS_ANTINUC/ <../../../inputs/XS_ANTINUC/>`_\*tertiary\* files (:numref:`fig_E8`)


**Plots from option** -e

.. _fig_quitroot:

.. figure:: DocImages/option_e/quitROOT.png
   :width: 40%
   :align: center

   How to properly quit ROOT in order to go back to interactive session in xterm.


.. _fig_A2:

.. figure:: DocImages/option_e/A2.png
   :width: 80%
   :align: center


   \- **Option A2**: CR source compared to solar system abundances. Top panel shows ratio segregated by first ionization potential (vs volatility not shown). Bottom panel shows directly the comparison, in which contributions of secondary origin are shown separately on the plot. Similar plots for isotopes exist but are not shown).

.. _fig_D0:

.. figure:: DocImages/option_e/D0.png
   :width: 85%
   :align: center

   \- [**Option D0**]: relative contributions per production process for isotopic fluxes (top) and elemental (bottom) fluxes at 1, 10, and 100 GeV/n: primary (black), secondary (1, 2, and >2 steps in red, blue, and green), radioactive (orange).

.. _fig_D3:

.. figure:: DocImages/option_e/D3.png
   :width: 50%
   :align: center

   \- [**Option D3**]: relative fraction of IS flux in excess or disappeared for :math:`\beta`-radioactive isotope :math:`^{10}{\rm Be}` and its daughter :math:`^{10}{\rm B}`. The relative fraction for the element Be and B are also shown: the impact of decay is diluted by the other stable isotopes of the element.

.. _fig_D5:

.. figure:: DocImages/option_e/D5.png
   :width: 60%
   :align: center

   \- [**Option D5**]: separated source terms (before propagation) for CR+ISM reactions leading to :math:`\bar{d}`, i.e. :math:`\frac{dQ_{sec}^{\bar{d}}}{dE_{kn}^{\rm out}}({\rm CR},{\rm ISM},E_{kn}^{\rm out})= \int_{E_{\rm thresh}^{\rm in}}^{\infty} \frac{dN^{\rm CR}}{dE_{kn}^{\rm in}}(E_{kn}^{\rm in}) \times A_{\bar{d}} \times n_{\rm ISM} \times v^{\rm in} \times \frac{d\sigma^{{\rm CR}+{\rm ISM}\rightarrow \bar{d}} (E_{kn}^{\rm in},E_{kn}^{\rm out})}{dE_{kn}^{\rm in}} dE_{kn}^{\rm in}`.

.. _fig_D6:

.. figure:: DocImages/option_e/D6.png
   :width: 60%
   :align: center

   \- [**Option D6**]: separated :math:`\bar{d}` contributions (after propagation) for CR+ISM reactions.

.. _fig_D7:

.. figure:: DocImages/option_e/D7.png
   :width: 60%
   :align: center

   \- [**Option D7**]: secondary :math:`\bar{p}` contributions from different primary CR energy ranges.

.. _fig_D8:

.. figure:: DocImages/option_e/D8.png
   :width: 60%
   :align: center

   \- [**Option D8**]: tertiary :math:`\bar{p}` contribution. The black lines show the calculated flux without (dotted) and with (solid) the tertiary contribution: non-annihilating rescattering of :math:`\bar{p}` on the ISM deplete :math:`\bar{p}` at high energies and redistribute them at lower energies. The various colours show contributions of various energy ranges.

.. _fig_E2:

.. figure:: DocImages/option_e/E2.png
   :width: 60%
   :align: center

   \- [**Option E2**]: comparison plot switching on/off physics effects during propagation (all other parameters being equal). In this example for B/C(R), the inelastic XS, reacceleration, anf galactic wind have been switched-off in turns or together, compared to the reference (grey line).

.. _fig_E3:

.. figure:: DocImages/option_e/E3.png
   :width: 60%
   :align: center

   \- [**Option E3**]: comparison plot using different inelastic XS parametrisations for nuclei.

.. _fig_E4:

.. figure:: DocImages/option_e/E4.png
   :width: 60%
   :align: center

   \- [**Option E4**]: comparison plot using different production XS parametrisations for nuclei.

.. _fig_E8:

.. figure:: DocImages/option_e/E8.png
   :width: 60%
   :align: center

   \- [**Option E8**]: comparison plot using different tertiary differential (redistribution) XS for :math:`\bar{p}`.


**Footnote**

.. [1] Does not always work on MacOS systems!