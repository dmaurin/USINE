.. _rst_keywords_group_models:

Group = ``Model...``
^^^^^^^^^^^^^^^^^^^^

It includes the groups ``Model0DLeakyBox``, ``Model1DKisoVc``, ``Model2DKisoVc``, and ``ModelTEST`` (only used for tests). The syntax of the associated parameters is detailed in :numref:`rst_syntax_model`.

.. _tab_model:

.. table:: Mandatory subgroups for galactic propagation models.

   ===========================  ======================================
   Group\@subgroup              Description
   ===========================  ======================================
   *Model...\@Base*             Base parameters specific to propagation model (if needed)
   *Model...\@Geometry*         Geometry parameters, coordinates name/range, Sun coordinate
   *Model...\@ISM*              ISM densities (HI, HII, H2, elements) and properties (e.g., T°)
   *Model...\@SrcPointLike*     Point-like source (none in implemented models)
   *Model...\@SrcSteadyState*   Radial and spectral dependence for each species
   *Model...\@Transport*        Transport parameters (diffusion, convection, Alfvén speed)
   ===========================  ======================================