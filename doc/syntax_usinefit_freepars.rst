.. _rst_syntax_usinefit_freepars:

UsineFit\@FreePars
~~~~~~~~~~~~~~~~~~

Enable fit/nuisance params for minimization runs, whose syntax is ``name:X,Y,[min,max],init,sigma``

   - ``name`` is the name of the parameter (see table below)
   - ``X`` = FIT, FIXED, or NUISANCE:

      - FIT: use the parameter *name* as a fit parameter. If the minimizer allows it, for instance Minuit, sets initial value to ``init``, initial error to ``sigma``, and lower and upper values of the parameter limited to ``[min,max]`` (use ``[-,-]``, or ``[-,max]``, or ``[min,-]`` for no lower/upper limits).

      - FIXED: use the parameter *name* as a fixed parameter (if the minimizer allows it), set to ``init``. The parameters ``sigma`` and ``[min,max]`` are unused.

      - NUISANCE: assumes a Gaussian distributed parameter :math:`{\cal N}(\mu =` ``init``, :math:`\sigma =` ``sigma``), which contributes to the global :math:`\chi^2` as

      .. math:: \chi^2_{\rm nuisance} = (\frac{{\rm val}-{\rm init}}{\sigma})^2

      where val is the value tested in the fit. Not that we strongly penalize values outside ``[min,max]`` (they are excluded in practice).

   - ``Y`` = LIN or LOG: enables to sample the parameter as *name* or :math:`\log_{10}` (*name*).


.. warning:: All *\@ParNames* parameters can be in principle declared as fit parameters. However, a subtlety arises because of the possibility to have *universal* vs *per CR* source parameters: for universal parameters, USINE directly uses the parameter name, otherwise it builds NCR parameters **X_Y** with X=parameter name and Y=CR name (e.g., for ``q``, generates ``q_1H``, ``q_2H``, ``q_3HE``... which can all be left as free parameters). In addition, modulation parameters can be declared as free/nuisance parameters for all data periods used in *FitTOAData* (see below), and data error uncertainties can also be added as nuisance parameters: in both cases, the name of the parameter is built internally from the experiment name (removing special characters ``([*%@)]...`` by ``_``). Run ``./bin/usine_run -m1  inputs/your.init.file`` (with your.init.file you initialisation file) to list what parameters can be left free for your initialisation file.

.. _tab_fitparam:

.. table:: List/examples of enabled parameters (fit or nuisance parameters).

   ======================================== =========================================================
   Group\@subgroup\@parameter               Description
   ======================================== =========================================================
   *UsineFit\@FreePars\@CRs* (M=1)          Half-life of all unstable CRs are enabled as nuisance parameters, e.g. ``HalfLifeBETA_10Be:NUISANCE,LIN,[1.3,1.5],1.387,0.012`` (use EC instead of BETA if EC-unstable nucleus).
   *UsineFit\@FreePars\@DataErr* (M=1)      If \@FitTOAData\@ErrType parameter set to kERRCOV for CR quantity (see next subgroup), its systematics are enabled as centred nuisance parameters, e.g., ``SCALE_AMS02_201105201605__BC:NUISANCE,LIN,[-10,10],0.,1.``.

                                            |  - If a systematics not set as nuisance parameter, it is accounted for in covariance matrix (see :numref:`rst_input_cr_data_cov`);
                                            |  - if systematics set as nuisance parameter, amounts to a model bias: for :math:`N_s` types of systematics set as nuisance for a quantity Q , the model calculation read
                                            |    :math:`{\rm model}^Q(R_k) = {\rm model}^Q_{\rm true}(R_k) \times {\rm bias}^Q(R_k) =`
                                            |    :math:`{\rm model}^Q_{\rm true}(R_k) \times \Pi_{l=0\dots n} (1+\nu^Q_l \times \sigma^Q_l(R_k))`,
                                            |  with :math:`\nu^Q_l` the nuisance parameter (centred on 0, variance 1) and :math:`\sigma^Q_l(R_k)` the relative error for the calculated point :math:`R_k` (read from the diagonal of the covariance matrix for simplicity). In that case, the systematics is not in :math:`\chi^2_{\rm cov}`, but as a standard :math:`\chi^2_{\rm nuis}` (see above).
   *UsineFit\@FreePars\@Geometry* (M=1)     Any parameter in *Model...\@Geometry\@ParNames*.
   *UsineFit\@FreePars\@ISM* (M=1)          Any parameter in *Model...\@ISM\@ParNames*.
   *UsineFit\@FreePars\@Modulation* (M=1)   Run *./bin/usine* with option ``-m1`` to get enabled names for data and modulation levels, e.g. ``phi_AMS02_201105201605_:NUISANCE,LIN,[0.3,,1.1],0.73,0.2`` ( Force-Field approx.).
   *Run\@FitFreePars\@SrcSteadyState* (M=1) Any source parameter, e.g. ``q_4HE:FIT,LOG,[-5.,-3],-4.,0.1`` or ``alpha:NUISANCE,LIN,[1.7.,2.5],2.3,0.1``
   *Run\@FitFreePars\@Transport* (M=1)      Any parameter in *Model...\@Transport\@ParNames*, e.g. ``delta:FIT,LIN,[0.2,0.9],0.6,0.02``, or ``Va:FIT,LIN,[1.,120],70.,0.1``, etc.
   *UsineFit\@FreePars\@XSection* (M=1)     XS parameters format are **PREFIX_REACTION** (see examples in ``inputs/init.TEST.par``):

                                            | - 'PREFIX' enabled:
                                            |   - for *standard bias* of XS:
                                            |     - **Norm**        → normalisation parameter (all E)
                                            |     - **EAxisScale**  → :math:`\sigma(E_{k/n}^{\rm new})=\sigma(E_{k/n} \times {\rm EAxisScale})`
                                            |     - **EnhancePowHE** → (``TUXSections::XS_EnhancementHE``)^(**EnhancePowHE**)
                                            |     - **EThresh** and **SlopeLE** → below threshold :math:`\sigma(E_{k/n}) = (E_{k/n}/{\rm EThresh})^{\rm SlopeLE} \times \sigma(E_{k/n})`, with :math:`{\rm EThresh}=5~{\rm GeV/n}` if not set as nuisance.
                                            |   - for *linear combination* of XS (:math:`\sigma_{\rm LC} = \sum_i C_i \times \sigma_i`):
                                            |      - **LCInelBar94**, **LCInelTri99**, **LCInelWeb03**, **LCInelWel97** for inelastic XS,
                                            |      - **LCProdGal17**, **LCProdSou01**, **LCProdWeb03**, **LCProdWKS98** for production XS.
                                            | - enabled 'REACTION' keywords
                                            |    - Total inelastic XS:
                                            |       - ``12C+H``   → specific reaction
                                            |       - ``ANY+H``   → any CR on given target
                                            |       - ``12C+ANY`` → 12C on all ISM targets
                                            |    - Production XS:
                                            |       - ``12C+H->10Be``   → specific reaction
                                            |       - ``ANY+H->10Be``   → any CRs
                                            |       - ``12C+ANY->10Be`` → any target
                                            |       - ``12C+H->ANY``    → any fragment
                                            |    - Tertiary XS (tot. and diff. similarly biased):
                                            |       - ``1H-BAR+H->1H-BAR``   → specific reaction
                                            |       - ``1H-BAR+ANY->1H-BAR`` → any target
                                            |    - ``ALL`` → if not linear combination of XS, applies to all XS (inel, prod, and tertiary); otherwise, can be applied for all linear combination of inelastic and/or production XS applies to all XS (inel, prod, and tertiary)!
                                            | Note that it is not allowed to have the same prefix used for overlapping reactions (e.g., 12C+ANY has overlapping reactions with 12C+H).
   ======================================== =========================================================



