.. _rst_syntax_usinefit:

*UsineFit* params
--------------------------------------------------------------------------

The group *UsineFit* and its subgroups are described in :numref:`tab_usinefit`.

.. toctree::
   :maxdepth: 1

   syntax_usinefit_config
   syntax_usinefit_freepars
   syntax_usinefit_toadata
   syntax_usinefit_outputs