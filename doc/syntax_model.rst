.. _rst_syntax_model:

*Model...* params
--------------------------------------------------------------------------

The group *Model...* and its subgroups are described in :numref:`tab_model`. The current models implemented are ``Model0DLeakyBox``, ``Model1DKisoVc``, ``Model2DKisoVc``, and ``ModelTEST`` (for tests only).


.. toctree::
   :maxdepth: 1

   syntax_model_geometry
   syntax_model_ism
   syntax_model_srcpointlike
   syntax_model_srcsteadystate
   syntax_model_transport

.. warning:: For any ``Model...``, all subgroups and parameters listed below are mandatory! Moreover, not all values of the parameters may be changed (see below)!

.. _tab_parnames:

.. table:: Mandatory parameters used for subgroups ``Geometry``, ``ISM``, and ``Transport``

      ==================  ================================================
      \@parameter         Description
      ==================  ================================================
      *\@ParNames* (M=0)  Comma-separated user-defined names (``-`` if no free parameters)
      *\@ParUnits* (M=0)  Comma-separated associated units (``-`` for params without unit)
      *\@ParVals* (M=0)   Comma-separated associated values
      ==================  ================================================