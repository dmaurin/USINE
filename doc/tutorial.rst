.. _rst_tutorial:

Tutorial: ``./bin/usine``
=========================

USINE is a command-line executable with various options and arguments to calculate/fit local spectra, do comparison plots, show graphical views of inputs (e.g., CR data, XS), etc. Calculation for semi-analytical models in USINE are fast, so you get the results almost immediately (except for minimization if you have many free parameters). For these reason, the running steps of USINE are:

   1. ``./bin/usine -option arg1 arg2...``, with ``arg1`` the initialisation file (see :numref:`rst_initfile`)
   2. Model calculation of all isotopes and/or text-user interface for refined selection
   3. Displays and outputs results (and back to step 2 for another selection if the option allows)


.. toctree::
   :maxdepth: 1

   tutorial_options
   tutorial_runexamples
   tutorial_update