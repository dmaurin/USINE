.. _rst_syntax_base_crdata:

Base\@CRData
~~~~~~~~~~~~

Selection of generic data sets to load and specific list of experiment and quantities (in the data sets) to renormalise the model to.

.. tabularcolumns:: |p{5cm}|p{10.5cm}|

.. csv-table::
   :header: "Group\@subgroup\@parameter", "Description"
   :widths: 30, 70
   :align: center

   "*Base\@CRData\@fCRData* (M=0)", "Path [1]_ to file containing USINE-formatted CR data, e.g. `$USINE/inputs/crdata_crdb20170523.dat <../../../inputs/crdata_crdb20170523.dat>`_. More on the format in :ref:`rst_input_cr_data`."
   "*Base\@CRData\@NormList* (M=0)", "**Qties1:Exp1|Eval1|Etype1;Qties2:Exp2|Eval2|Etype2;...** with xtype = kEKN, kR or kETOT (e.g., ``H,He:PAMELA|20.|kEKN;C,N,O,Si,Fe:HEAO|10.6|kEKN``). During propagation, source abundances are rescaled to *NormList* data point selection (sought in fCRData), unless the normalisation is a fit parameter (in *UsineRun\@FitFreePars*) or data name appears in *Base\@ListOfCRs\@PureSecondaries*."

.. note:: Renormalising source abundances to elemental fluxes in the model (keeping the same isotopic relative abundances) is the standard/fastest way to run USINE. To bypass this renormalisation, one has to fix individual source abundances or fit these abundances on the data.


**Footnote**

.. [1] Environment variables in path (e.g. ``$USINE/``) are enabled and correctly interpreted