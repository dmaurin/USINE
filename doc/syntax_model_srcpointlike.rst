.. _rst_syntax_model_srcpointlike:

Model...\@SrcPointLike
~~~~~~~~~~~~~~~~~~~~~~

Point-like source description (location, duration, CR content and spectra) based on templates for the source spectra (see :ref:`rst_keywords_group_templates`), and uniquely identified by an (user-defined) ID.

.. note:: All parameters of the template spectrum are enabled as free parameters (see :ref:`rst_tutorial_update_fitpars`), with one free parameter per CR or a single universal free parameter for all CRs (depending on *SpectValsPerCR* values).

.. tabularcolumns:: |p{7.cm}|p{8.5cm}|

.. csv-table::
   :header: "Group\@subgroup\@parameter", "Description"
   :widths: 30, 70
   :align: center

   "*Model...\@SrcPointLike\@Species*  (M=1)", "**SrcID|List** provides list of CR species present in point-like source **SrcID** (any user-chosen name, provided that it reads SrcID=ASTRO_xxx or DM_xxx, with xxx any string). Allowed options for **List** are ``ALL`` (use all CRs from parameter *ListOfCRs*, but discard pure secondaries) or comma-separated list (e.g., ``1H,4He,12C,14N,16O``)"
   "*Model...\@SrcPointLike\@SpectAbundInit* (M=1)", "**SrcID|X** with X a comma-separated list of gENUM_SRCABUND_INIT enumerators (e.g., X = ``kSSISOTFRAC``, ``kSSISOTABUND``, or ``kFIPBIAS``): the latter keywords indicate how to initialise the normalisation of source spectra for **SrcID**, and it supersedes any default values set for parameter *SpectraPerCR*."
   "*Model...\@SrcPointLike\@SpectTempl* (M=1)", "**SrcID|X|Y** with X chosen among available spectrum templates (see :ref:`rst_keywords_group_templates`), and Y the parameter name (in the template) corresponding to the normalisation variable in the template source spectrum **X**"
   "*Model...\@SrcPointLike\@SpectValsPerCR*  (M=1)", "**SrcID|X** with X = ``-`` (if template is a file) or  **par1[Y1];par2[Y2];par3[Y2]** (if template is a formula [1]_) with as many semi-column separated entries as the number of parameters in the template. For the latter, the following keywords and formats can be used:

     | **Y=SHARED:val** → universal parameter for all CRs
     | **Y=PERCR:DEFAULT=val0,CR1=val1,CR2=val2...** → one parameter per CR
     | **Y=PERZ:DEFAULT=val0,Z1=val1,Z2=val2...** → one parameter per element
     | **Y=LIST:DEFAULT=val0,1H_H_HE=val1,C_N_O=val2...** → one parameter per list (*DEFAULT* is taken to be the list of all remaining CRs not in user-defined lists)"
   "*Model...\@SrcPointLike\@SrcXPosition* (M=1)", "**SrcID|X** with X=f(t,x,y,z) for the time-dependent coordinate X of the source (in the model geometry)"
   "*Model...\@SrcPointLike\@SrcYPosition* (M=1)", "**SrcID|X** with Y=f(t,x,y,z) for the time-dependent coordinate Y of the source (in the model geometry)"
   "*Model...\@SrcPointLike\@SrcZPosition* (M=1)", "**SrcID|X** with Y=f(t,x,y,z) for the time-dependent coordinate Z of the source (in the model geometry)"
   "*Model...\@SrcPointLike\@TStart* (M=1)", "**SrcID|X** with X=t_start"
   "*Model...\@SrcPointLike\@TStop* (M=1)", "**SrcID|X** with X=t_stop"

**Footnote**

.. [1] The syntaxt for formula is based on `fparser library <http://warp.povusers.org/FunctionParser>`_, see the documentation for the `formulae syntax <http://warp.povusers.org/FunctionParser/fparser.html>`_