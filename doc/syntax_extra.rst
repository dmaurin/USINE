.. _rst_syntax_extra:

*Extra* params
--------------------------------------------------------------------------

The group *Extra* and its subgroups are described in :numref:`tab_extra`.

   .. note:: These parameters are unused for propagation runs!

Extra\@ISFlux
~~~~~~~~~~~~~

.. tabularcolumns:: |p{5cm}|p{10.5cm}|

.. csv-table::
   :header: "Group\@subgroup\@parameter", "Description"
   :widths: 30, 70
   :align: center

   "*Extra\@ISFlux\@FluxName* (M=0)", "Name for flux parametrisation"
   "*Extra\@ISFlux\@ParNames* (M=0)", "Comma-separated list of parameters used in formulae, that can later be used as free parameter (see *UsineRun\@FitFreePars*). If no free parameters, set value to ``-``."
   "*Extra\@ISFlux\@ParUnits* (M=0)", "Comma-separated list (mandatory) for units associated parameters (for print purposes). Use ``-`` for quantities without unit."
   "*Extra\@ISFlux\@Fluxes* (M=1)", "**CR:FNCTYPE|X**: flux of a CR species that must be present in parameter *ListOfCRs*, with X=f(beta,Ekn,gamma,Etot,p,Rig; *ParNames*) if FNCTYPE=FORMULA [1]_ or X=file_name if FNCTYPE=GRID. Fluxes are in in :math:`[{\rm (GeV/n)}^{-1} {\rm m}^{-2} {\rm s}^{-1} {\rm sr}^{-1}]`, e.g. ``1H:FORMULA|23350*pow(beta,1.1)*pow(Rig,-2.839)``."

**Footnote**

.. [1] The syntaxt for formula is based on `fparser library <http://warp.povusers.org/FunctionParser>`_, see the documentation for the `formulae syntax <http://warp.povusers.org/FunctionParser/fparser.html>`_