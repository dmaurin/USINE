.. _rst_syntax_usinerun_onoff:

UsineRun\@OnOff
~~~~~~~~~~~~~~~

All physics effects can be enabled or disabled in USINE!

===========================================  ============================
Group\@subgroup\@parameter                   Description
===========================================  ============================
*UsineRun\@OnOff\@IsDecayBETA* (M=0)         If *false*, :math:`\beta`-unstable nuclei do not decay
*UsineRun\@OnOff\@IsDecayFedBETA* (M=0)      If *false*, contribution of :math:`\beta`-decay parent to daughter is disabled
*UsineRun\@OnOff\@IsDecayEC* (M=0)           If *false*, EC-unstable nuclei do not decay
*UsineRun\@OnOff\@IsDecayFedEC* (M=0)        If *false*, contribution of EC-decay parent to daughter is disabled
*UsineRun\@OnOff\@IsDestruction* (M=0)       If *false*, no CR destruction (*fTotInelAnn*)
*UsineRun\@OnOff\@IsELossAdiabatic* (M=0)    If *false*, no adiabatic losses from Galactic wind
*UsineRun\@OnOff\@IsELossBremss* (M=0)       If *false*, no bremsstrahlung losses [1]_
*UsineRun\@OnOff\@IsELossCoulomb* (M=0)      If *false*, no Coulomb losses
*UsineRun\@OnOff\@IsELossIC* (M=0)           If *false*, no inverse Compton losses [1]_
*UsineRun\@OnOff\@IsELossIon* (M=0)          If *false*, no ionization losses
*UsineRun\@OnOff\@IsELossSynchrotron* (M=0)  If *false*, no synchrotron losses [1]_
*UsineRun\@OnOff\@IsEReacc* (M=0)            If *false*, no reacceleration
*UsineRun\@OnOff\@IsPrimExotic* (M=0)        If *false*, discard exotic sources in the Galactic halo
*UsineRun\@OnOff\@IsPrimStandard* (M=0)      If *false*, discard primary sources in the Galactic disc
*UsineRun\@OnOff\@IsSecondaries* (M=0)       If *false*, discard secondary CR production (*fProd*)
*UsineRun\@OnOff\@IsTertiaries* (M=0)        If *false*, no discard tertiaries (*Tertiaries*)
*UsineRun\@OnOff\@IsWind* (M=0)              If *false*, no galactic wind (hence no adiabatic losses)
===========================================  ============================

.. [1] These energy losses are relevant and apply for leptons only