.. _rst_code:

Inside USINE (c++)
==================

This section provide an overview of the code structure and organisation. A detailed description of each class content (members and role in the USINE ecology) is given in the header files (``include/TU*.h``).

Classes
-------

:numref:`fig_codestructure` is a sketch of all USINE classes, grouped by items to underline the most relevant features of the code:

   - **CR physics** (*blue box, top*): classes for CR base ingredients, with CR charts, data, and cross-sections set from input files --- see :ref:`rst_inputfiles`.

   - **USINE utilities** (*grey box, bottom*): the most important class is ``TUValsTXYZCrE.h``, a handler for formula (or values on a grid) dependent on generic CR, energy, and space-time coordinates --- see :ref:`rst_tutorial`. The dashed arrows indicate the classes involved and where it is used (for transport, ISM description, and source parameters).

   - **Models** (*orange box, right-hand side*): dedicated classes for Galactic propagation and Solar modulation models.

   - **Initialisation parameters** (*red box, left-hand side*): class reading the parameter file (see :ref:`rst_initfile`) to set and initialise all classes for a run.

   - **Free parameters** (*red box, centre*): class handling all fit-able parameters. Red arrows connecting red boxes highlight classes with free parameters (spatial coordinates, XS, ISM, transport, sources, Solar modulation) --- see :ref:`rst_tutorial_update_fitpars` for the syntax.

.. _fig_codestructure:

.. figure:: DocImages/usine_classes.png
   :align: center
   :figclass: align-center
   :scale: 70%

   Organisation chart of USINE classes.


Inheritance diagrams
--------------------

Two important classes in USINE largely rely on inheritance: :numref:`fig_model_inheritance` and :numref:`fig_initparams_inheritance` below were obtained with `Doxygen <http://www.stack.nl/~dimitri/doxygen/>`_ (enabling `graphviz <http://graphviz.org>`_).

.. _fig_model_inheritance:

.. figure:: DocImages/usine_model_inheritance.png
   :align: center
   :figclass: align-center
   :scale: 70%

   Inheritance diagram for propagation models.

:Structure of propagation model classes (class TUModelBase.h):
   → inherit from
      - CR list (charts/properties) and energy 'axis'
      - Propagation switches (effects switched on or off)
      - CR data
      - List of data on which to normalise primary fluxes
      - CR source templates (to be used as CR sources)
      - Cross-sections (inelastic, production, etc.)
      - List of free parameters for the model
   → have members for
      - Transport parameters
      - ISM description
      - Source description
      - All other free parameters (model and ingredients)


:Structure of free parameters (class TUInitPars.h): if existing, the free parameters of a class are stored in a TUFreeParList object. They are themselves collected in a global list of free parameters. This allows a very simple declaration of the parameters the user wishes to let free in a minimisation (see :ref:`rst_tutorial_update_fitpars`)

.. _fig_initparams_inheritance:

.. figure:: DocImages/usine_initparams_inheritance.png
   :align: center
   :figclass: align-center
   :scale: 100%

   Inheritance diagram for free parameters.



Enumerators
-----------

USINE relies on many enumerators. Most of these enumerators are for developers only, but some of them are relevant for users as well, as given in :numref:`tab_enums`. The complete list of enumerators is defined in `$USINE/include/TUEnum.h <../../../include/TUEnum.h>`_.

.. tabularcolumns:: |p{3.7cm}|p{2.5cm}|p{9.cm}|

.. _tab_enums:

.. csv-table:: Enumerators to use in the arguments (of ./bin/usine) and/or initialisation file.
   :header: "Enumerator", "Description", "Enabled"
   :widths: 22, 23, 55
   :align: center

   "``gENUM_ETYPE``", "Energy unit/type for data and plots", "``kEKN``, ``kR``, ``kETOT``, ``kEK`` for kinetic energy per nucleon [GeV/n], rigidity [GV], total energy [GeV], and kinetic energy [GeV]"
   "``gENUM_ERRTYPE``", "Handling of data errors", "``kERRSTAT``, ``kERRSYST``, ``kERRTOT``, ``kERRCOV`` for statistical, systematics, total (stat+syst in quadrature), or covariance"
   "``gENUM_FCNTYPE``", "Parametrisation", "``kFORMULA, kGRID`` for a formula or a grid of values"
   "``gENUM_PROPAG_MODEL``", "Propagation models", "``kMODEL0DLEAKYBOX``, ``kMODEL1DKISOVC``, ``kMODEL2DKISOVC`` for Leaky-Box, 1D, or 2D semi-analytical diffusion model"
   "``gENUM_SOLMOD_MODEL``", "Solar modulation models", "``kSOLMOD0DFF`` for Force-Field approximation"


Coding conventions
------------------

If ever you want to have a look at the code, or develop some new modules, it may be useful to know that USINE relies loosely on the `coding conventions <https://root.cern.ch/coding-conventions>`__ used in ROOT. In addition, we use the following syntax:

   - `lower_case_underscore_separated` :math:`\rightarrow` local variables
   - `UpperCaseSeparated` :math:`\rightarrow` method names
   - `gUPPERCASE` :math:`\rightarrow` global variables
   - `kUPPERCASE` :math:`\rightarrow` enumerators (see list of enumerators above)


To ensure homogeneity in the indentation throughout `USINE` sources (`*.cc` files), we use `astyle <http://astyle.sourceforge.net/>`_ and the ROOT/CERN style obtained by copying the lines below in ``~/.astylerc``:

|  ``# ROOT code formatting style``
|  ``--style=stroustrup``
|  ``--mode=c``
|  ``--align-pointer=name``
|  ``--indent=spaces=3``
|  ``--indent-switches``
|  ``--indent-cases``
|  ``--indent-namespaces``
|  ``--max-instatement-indent=40``
|  ``--indent-preprocessor``
|  ``--convert-tabs``
|  ``--pad-header``
|  ``--pad-oper``
|  ``--unpad-paren``