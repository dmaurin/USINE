.. _rst_input_cr_data:

Cosmic ray data
--------------------------------------------------------------------------

CR data files are *ASCII* files in `$USINE/inputs/ <../../../inputs/>`_ whose prefix is ``crdata_*``.

:Data files from CRDB or create our own file:
   A quite complete body of CR data can be retrieved from `CRDB <https://lpsc.in2p3.fr/crdb/>`_: from the ``Welcome`` tab, click on ``Export database content in USINE`` and save the file as ``crdata_crdbYYYYMMDD.dat``. This is how were retrieved *USINE* CR data files---update these files whenever new CR data are available! To learn more from CRDB and its usage, see `Maurin et al. (A&A 569, A32, 2014) <http://adsabs.harvard.edu/abs/2014A%26A...569A..32M>`_. Alternatively, one can create his own data file and add it to the pile of files to load in *USINE*, see :ref:`rst_tutorial_update_config` and the format below.

:Which data and what are they good for:
   Files of CR data contain lists of data points (energy, flux or ratio measurement, uncertainties) and informations related to the experiments they originate from (name, data taking period, position in Solar system, modulation level…). In *USINE*, CR data to display or to fit are based on a subset (user selection) from the CR data files (see :ref:`rst_tutorial_update_runpars` and :ref:`rst_tutorial_update_fitpars`), as handled by the classes ``TUDataEntry`` and ``TUDataSet``.

   .. note::
      Use the command line ```>./bin/usine -id``` to display data files.

Shown below is an excerpt of the file `crdata_crdb20170523.dat <../../../inputs/crdata_crdb20170523.dat>`_:

.. literalinclude:: ../inputs/crdata_crdb20170523.dat
   :language: c
   :lines: 1-24