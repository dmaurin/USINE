.. _rst_tutorial_update_config:

Edit E-grid, CR lists...
------------------------

- **Change energy range and # of bins**: this is a straightforward operation, but ensure that you have a large enough *NBins* for accuracy - very few bins are used in *init.TEST.par* for speed, it is for test only (see the other caveats in :numref:`rst_syntax_base_energygrid`)::

   Base  @ EnergyGrid  @ NBins             @ M=0 @ 257
   Base  @ EnergyGrid  @ NUC_EknRange      @ M=0 @ [5.e-2,5.e3]
   Base  @ EnergyGrid  @ ANTINUC_EknRange  @ M=0 @ [5e-2,1.e2]


- **Change list of CRs/parents and pure secondaries**: use the shortest CR list adapted to your problem, as fewer CRs to propagate means faster runs. The most-likely parameters the user may edit are the ones listed below (see full syntax in :numref:`rst_syntax_base_listofcrs`)::

   Base  @ ListOfCRs   @ ListOfCRs         @ M=0 @ [2H-BAR,30Si]
   Base  @ ListOfCRs   @ ListOfParents     @ M=0 @ 2H-bar:1H-bar,1H,4He;1H-bar:1H,4He
   Base  @ ListOfCRs   @ PureSecondaries   @ M=0 @ Li,Be,B

- **Change CR data files and data used**: you can load any number of CR data files you like (*M=1* for parameter *fCRData*, see syntax in :numref:`rst_syntax_base_crdata`). Other parameters set the subset of data used for display (see parameters :numref:`rst_syntax_usinerun_display`) or for fits (see parameters :numref:`rst_syntax_usinefit_toadata`). In the example below, two CR data files are loaded, all the available data being used for display (using only statistical errors). If a fit is performed, it will be on B/C(R) AMS data found in the files, using a covariance matrix::

   Base  @ CRData      @ fCRData           @ M=1 @ $USINE/inputs/crdata_crdb20170523.dat
   Base  @ CRData      @ fCRData           @ M=1 @ $USINE/inputs/crdata_dummy.dat
   Base  @ CRData      @ NormList          @ M=0 @ H,He:PAMELA|20.|kEkn;C,N,O,F,Ne,Na,Mg,Al,Si,P,S,Cl,Ar,K,Ca,Sc,Ti,V,Cr,Mn,Fe,Ni:HEAO|10.6|kEkn

   UsineRun @ Display    @ QtiesExpsEType  @ M=0 @ ALL
   UsineRun @ Display    @ ErrType         @ M=0 @ kERRSTAT

   UsineRun @ FitTOAData @ QtiesExpsEType  @ M=0 @ B/C:AMS:KR
   UsineRun @ FitTOAData @ ErrType         @ M=0 @ kERRCOV:$USINE/inputs/CRDATA_COVARIANCE/


- **Change XS files**: any list of target elements can be used for the ISM (usually H,He), and any list of isotopes can be used for tertiaries (no need to change the code). The only requirement is that the list of files for XS provided encompass these cases (see format in :numref:`rst_input_xs_data`): note that *M=1* for *fTotInelAnn*, *fProd*... parameters, so that you can ad as many line entries as files you want USINE to look at to find the required XS. See the full syntax and description in :numref:`rst_syntax_base_xsections`::

   Base  @ MediumCompo @ Targets           @ M=0 @ H,He
   Base  @ XSections   @ Tertiaries        @ M=0 @ 1H-bar,2H-bar
   Base  @ XSections   @ fTotInelAnn       @ M=1 @ $USINE/inputs/XS_NUCLEI/sigInel*
   Base  @ XSections   @ fProd             @ M=1 @ $USINE/inputs/XS_NUCLEI/dSdEProd*
   Base  @ XSections   @ fTotInelNonAnn    @ M=1 @ $USINE/inputs/XS_ANTINUC/sigInelNONANN*
   Base  @ XSections   @ fdSigdENAR        @ M=1 @ $USINE/inputs/XS_ANTINUC/dSdENAR*