.. _rst_tutorial_update_models:

Edit source, transport...
-------------------------

As discussed in :numref:`rst_syntax_model`, all models have mandatory parameters (even if many of the are unused) related to (i) `Geometry`_ , (ii) `ISM`_, (iii) `Sources`_, (iv) `Transport`_ parameters. Let us detail below how it works.

.. _Geometry:

Geometry
~~~~~~~~

   It is illustrated below for *Model2DKisoVc* (`Maurin et al., 2001 <http://adsabs.harvard.edu/abs/2001ApJ...555..585M>`_).

   - *Model parameters*: radial extension ``R``, halo half-height ``L``, thin disc half-height (``h``), and a possible local-underdensity ``rhole`` (`Putze et al., 2010 <http://adsabs.harvard.edu/abs/2010A%26A...516A..66P>`_). All these parameters can later be used as fit parameters (e.g., ``L``), see also :numref:`tab_parnames`::

      Model2DKisoVc   @ Geometry       @ ParNames         @ M=0 @ L,h,R,rhole
      Model2DKisoVc   @ Geometry       @ ParUnits         @ M=0 @ kpc,kpc,kpc,kpc
      Model2DKisoVc   @ Geometry       @ ParVals          @ M=0 @ 4.,0.1,20.,0.12

   - *2D cylindrical model*: 1st axis is radial coordinate (``r``, 10 linear bins ``[0,R]``), 2nd axis is vertical coordinate (``z``, 10 linear bins ``[0,L]``). In these coordinates the sun position is set to ``r=8.5`` (same unit as *XAxis*) and ``z=0`` (same unit as *Yaxis*)::

      Model2DKisoVc   @ Geometry       @ XAxis            @ M=0 @ r:[0,R],10,LIN
      Model2DKisoVc   @ Geometry       @ YAxis            @ M=0 @ z:[0,L],10,LIN
      Model2DKisoVc   @ Geometry       @ XSun             @ M=0 @ 8.5
      Model2DKisoVc   @ Geometry       @ YSun             @ M=0 @ 0.

   .. warning:: The only values the user may change are *Geometry\@ParVals* values and the position of the sun *Geometry\@XSun*. See syntax in  :numref:`rst_syntax_model_geometry`.

.. _ISM:

ISM
~~~

   All the model currently implemented have a constant distribution. The syntax below means that the ISM is described by a formula which is just a constant value (that the user can modify) for H (atomic, ionised, and molecular) and He densities (in :math:`{\rm cm}^{-3}`), and plasma temperature (in K)::

      Model2DKisoVc   @ ISM            @ ParNames         @ M=0 @ -
      Model2DKisoVc   @ ISM            @ ParUnits         @ M=0 @ -
      Model2DKisoVc   @ ISM            @ ParVals          @ M=0 @ -
      Model1DKisoVc   @ ISM            @ Density          @ M=1 @ HI:FORMULA|0.867
      Model1DKisoVc   @ ISM            @ Density          @ M=1 @ HII:FORMULA|0.033
      Model1DKisoVc   @ ISM            @ Density          @ M=1 @ H2:FORMULA|0.
      Model1DKisoVc   @ ISM            @ Density          @ M=1 @ He:FORMULA|0.1
      Model1DKisoVc   @ ISM            @ Te               @ M=0 @ FORMULA|1.e4

   .. warning:: The only values the user may change are those in  *FORMULA|*. See syntax in  :numref:`rst_syntax_model_ism`.


.. _Sources:

Sources
~~~~~~~

   Implemented models do not handle point-like sources and only correspond to steady-state sources. The parameters below correspond to:

      - a generic type of sources ``ASTRO_STD`` (any other name would have been fine too), which accelerates all CRs selected for propagation (``ASTRO_STD|ALL``), except those declared as pure secondaries in the list of CRs to propagate - see :numref:`rst_tutorial_update_config`.
      - this source is based on the spectrum template ``POWERLAW`` :math:`= q\times \beta^{\rm eta\_s}\times {\rm Rig}^{\rm -alpha}`, which has three parameters (q, alpha, and eta_s), and where the normalisation parameter is q (``ASTRO_STD|POWERLAW|q``). This template is one of the available templates defined in the same parameter file (see :numref:`rst_syntax_templ`):

         - the source abundance initialisation ``kSSISOTFRAC,kSSISOTABUND,kFIPBIAS`` assumes Solar system isotopic abundances biased by the first ionization potential (we recommend at least to have ``kSSISOTFRAC`` to fix isotopic fractions). If data to normalise is not an empty list, q values are rescaled (unless q_X set as a fitted parameters)::

            Base      @ CRData      @ NormList           @ M=0 @ H,He:PAMELA|20.|kEkn;C,N,O,F,Ne,Na,Mg,Al,Si,P,S,Cl,Ar,K,Ca,Sc,Ti,V,Cr,Mn,Fe,Ni:HEAO|10.6|kEkn
            UsineRun  @ Calculation @ IsUseNormList      @ M=0 @ true

         - in the example below, eta_s is taken to be a universal parameter (same for all CR species), whereas q and alpha are taken per CR (internally, usine creates q_X and alpha_X parameters, with X any CR in source).
         - any of the source parameters could be left as a free parameter!

      - the spatial distribution of the source is taken to be ``SNRMODIFIED``, with universal values for all CRs (``SHARED``).

   The fact that all parameters are *M=1* means than in principle we could declare several types of sources (different name possibly form different templates and/or species accelerated)::

      Model2DKisoVc  @ SrcSteadyState @ Species          @ M=1 @ ASTRO_STD|ALL
      Model2DKisoVc  @ SrcSteadyState @ SpectAbundInit   @ M=1 @ ASTRO_STD|kSSISOTFRAC,kSSISOTABUND,kFIPBIAS
      Model2DKisoVc  @ SrcSteadyState @ SpectTempl       @ M=1 @ ASTRO_STD|POWERLAW|q
      Model2DKisoVc  @ SrcSteadyState @ SpectValsPerCR   @ M=1 @ ASTRO_STD|q[PERCR:DEFAULT=1.e-5];alpha[PERZ:DEFAULT=2.1,H=2.1,He=2.2];eta_s[LIST:DEFAULT=-1.,1H_HE=-1.5,C_N_O=-2]
      Model2DKisoVc  @ SrcSteadyState @ SpatialTempl     @ M=1 @ ASTRO_STD|SNRMODIFIED
      Model2DKisoVc  @ SrcSteadyState @ SpatialValsPerCR @ M=1 @ ASTRO_STD||rsol[SHARED:8.5];a[SHARED:1.];b[SHARED:1.];dex[SHARED:1.]

   .. warning:: The values the user may change are (i) the name of the source ``ASTRO_STD`` (can be anything), (ii) the template spectrum (``POWERLAW``) and/or spatial distribution (``SNRMODIFIED``) used for the source, (iii) the types of parameters in the source (universal ``SHARED``, by CR ``PERCR``, by element ``PERZ``, or a list ``LIST``) and their default values. See syntax in :numref:`rst_syntax_model_srcsteadystate` for more details.

.. _Transport:

Transport
~~~~~~~~~

   It can be as generic as it needs to be (see :numref:`rst_syntax_model_transport`)! However, semi-analytical models implemented and solved in USINE cannot be as flexible as the user would wish them to be. First, only isotropic diffusion models are implemented (so we only need ``K00`` coefficient below). Second the 1D and 2D models can only have a galactic wind perpendicular to the disc (wind component ``W0`` only). For illustration purpose, we provide two examples below, one with a standard diffusion coefficient, and a second one with a break (as used in `Génolini et al., 2018 <http://adsabs.harvard.edu/abs/2017PhRvL.119x1101G>`_). The diffusion coefficient can be any function of the USINE-interpreted energy variables beta (lorentz factor), gamma, p (momentum in GV), Rig (rigidity in GV), Ekn (kinetic energy per nucleon in GeV/n), Etot (total energy in GeV), and Ek (kinetic energy in GeV)

   → No break::

      Model1DKisoVc  @ Transport  @ ParNames  @ M=0 @ Va,Vc,K0,delta,eta_t
      Model1DKisoVc  @ Transport  @ ParUnits  @ M=0 @ km/s,km/s,kpc^2/Myr,-,-
      Model1DKisoVc  @ Transport  @ ParVals   @ M=0 @ 55.,18.7,0.008,0.86,0.
      Model1DKisoVc  @ Transport  @ K         @ M=1 @ K00:FORMULA|beta^eta_t*K0*Rig^delta

   → With break::

      Model1DKisoVc  @ Transport  @ ParNames  @ M=0 @ Va,Vc,K0,delta,eta_t,Rbreak,Deltabreak,sbreak
      Model1DKisoVc  @ Transport  @ ParUnits  @ M=0 @ km/s,km/s,kpc^2/Myr,-,-,GV,-,-
      Model1DKisoVc  @ Transport  @ ParVals   @ M=0 @ 85.4,19.5,0.0358,0.61,-0.059,125.,0.023,0.01
      Model1DKisoVc  @ Transport  @ K         @ M=1 @ K00:FORMULA|beta^eta_t*K0*Rig^delta*(1+(Rig/Rbreak)^(Deltabreak/sbreak))^(-sbreak)

   With or without a break, the remaining parameters are::

      Model1DKisoVc  @ Transport  @ Wind      @ M=1 @ W0:FORMULA|Vc
      Model1DKisoVc  @ Transport  @ VA        @ M=0 @ FORMULA|Va
      Model1DKisoVc  @ Transport  @ Kpp       @ M=0 @ FORMULA|(4./3.)*(Va*1.022712e-3*beta*Etot)^2/(delta*(4-delta^2)*(4-delta)*K00)

   .. warning:: The values the user may change are the names/number of parameters and associated formulae for ``K00`` and ``Kpp``. The other parameters are fixed by the model (``Va`` and ``Vc``) and only their default value can be changed.