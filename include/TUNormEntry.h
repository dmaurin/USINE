// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

#ifndef USINE_TUNormEntry
#define USINE_TUNormEntry

// C/C++ include
// ROOT include
// USINE include
#include "TUEnum.h"

class TUNormEntry {

private:
   Double_t            fE;          // Energy at which calculation is normalised to measurement
   gENUM_ETYPE         fEType;      // E-axis type (kEKN, kR, ...) for fE
   string              fExpList;    // Exps (comma-sep list) whose data are used for normalisation
   Bool_t              fIsElement;  // Whether name is or isn't an element
   string              fName;       // Name whose calculated flux is to match exp. data (case insensitive)
   Int_t               fZ;          // Charge of species or element

public:
   TUNormEntry();
   TUNormEntry(TUNormEntry const &entry);
   virtual ~TUNormEntry();

   inline TUNormEntry *Clone()                       {return new TUNormEntry(*this);}
   void                Copy(TUNormEntry const &entry);
   inline TUNormEntry *Create()                      {return new TUNormEntry;}
   inline Double_t     GetE() const                  {return fE;}
   inline gENUM_ETYPE  GetEType() const              {return fEType;}
   inline string       GetExpList() const            {return fExpList;}
   inline string       GetName() const               {return fName;}
   inline Int_t        GetZ() const                  {return fZ;}
   inline Bool_t       IsElement() const             {return fIsElement;}
   void                Print(FILE *f=stdout) const;
   inline void         SetExpList(string const &exp) {fExpList = exp;}
   inline void         SetE(Double_t const &x)       {fE = x;}
   inline void         SetEType(gENUM_ETYPE xtype)   {fEType = xtype;}
   void                SetQty(string const &name);
   void                TEST(FILE *f=stdout);

   ClassDef(TUNormEntry, 1)  // Normalisation entry (element from given experiment and Etype)
};

#endif
