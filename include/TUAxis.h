// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

#ifndef USINE_TUAxis
#define USINE_TUAxis

// C/C++ include
// ROOT include
// USINE include
#include "TUEnum.h"
#include "TUInitParList.h"
#include "TUMessages.h"

class TUAxis {

private:
   gENUM_AXISTYPE   fAxisType;    // kLIN or kLOG (see TUEnum.h) step
   vector<Double_t> fGrid;        //[N] Grid values
   Bool_t           fIsUpdated;   // Set to true whenever UpdateMin() or UpdateMax() is called
   string           fName;        // Case-sensitive axis name (for printing and display proposes)
   Double_t         fStep;        // Step is x_{i+1}-x_{i} for kLIN, or x_{i+1}/x_{i} for kLOG
   string           fUnit;        // Unit, e.g. [kpc] (mostly for printing and display proposes)

   Double_t         GetValHalfBin(Int_t i, Bool_t is_upper) const;

public:
   TUAxis();
   TUAxis(TUAxis const &axis);
   inline TUAxis(Double_t val_min, Double_t val_max, Int_t n, string const &name, string const &unit, gENUM_AXISTYPE axis_type) {SetClass(val_min, val_max, n, name, unit, axis_type);}
   virtual ~TUAxis();

   inline TUAxis        *Clone()                                       {return new TUAxis(*this);}
   void                  Copy(TUAxis const &axis);
   inline TUAxis        *Create()                                      {return new TUAxis;}
   Double_t              EvalOutOfRange(Int_t i) const;
   inline gENUM_AXISTYPE GetAxisType() const                           {return fAxisType;}
   inline Double_t       GetMin() const                                {return fGrid[0];}
   inline Double_t       GetMax() const                                {return fGrid[fGrid.size()-1];}
   inline Int_t          GetN() const                                  {return (Int_t)fGrid.size();}
   inline string         GetName() const                               {return fName;}
   inline string         GetNameAndUnit() const                        {return (fName + " " + GetUnit(true));}
   inline Double_t       GetStep() const                               {return fStep;}
   inline string         GetUnit(Bool_t is_bracket) const              {return ((is_bracket) ? string("["+fUnit+"]"): fUnit);}
   inline Double_t       GetVal(Int_t i, Int_t halfbin_status=0) const {return ((halfbin_status==0) ? fGrid[i] : GetValHalfBin(i, Bool_t((halfbin_status+1)/2)));}
   inline Double_t      *GetVals(Int_t i = 0)                          {return &(fGrid[i]);}
   Int_t                 IndexClosest(Double_t const &val) const;
   void                  Initialise();
   inline Bool_t         IsUpdated() const                             {return fIsUpdated;}
   Bool_t                IsSameGridValues(TUAxis *axis) const;
   void                  Print(FILE *f=stdout) const;
   void                  PrintSummary(FILE *f=stdout) const;
   inline void           ResetIsUpdated()                              {fIsUpdated = false;}
   inline void           Scale(Double_t const &scale)                  {for (Int_t k=0; k<GetN(); ++k) fGrid[k] *= scale;}
   void                  SetClass(Double_t val_min, Double_t val_max, Int_t n, string const &name, string const &unit, gENUM_AXISTYPE axis_type);
   void                  SetEknGrids(TUInitParList *init_pars, TUAxis *fEkn[gN_CRFAMILY], Bool_t is_verbose, FILE *f_log) const;
   inline void           SetVal(Int_t i, Double_t const &val)          {fGrid[i] = val;}
   inline void           Shift(Double_t const &shift)                  {for (Int_t k=0; k<GetN(); ++k) fGrid[k] += shift;}
   void                  TEST(FILE *f = stdout);
   void                  UpdateMin(Double_t const &val_min);
   void                  UpdateMax(Double_t const &val_max);

   ClassDef(TUAxis, 1)   // Axis 1D: grid of linearly- or logarithmically-spaced values
};

#endif
