// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

#ifndef USINE_TUInitParList
#define USINE_TUInitParList

// C/C++ include
#include <cstdlib>
// ROOT include
// USINE include
#include "TUEnum.h"
#include "TUInitParEntry.h"
#include "TUMisc.h"

class TUInitParList {

private:
   vector<string>          fFileNames;     // Names of initialisation file used
   vector<string>          fInfoGroups;    //![NGroups] Info/help on group (from init. file)
   vector<vector<string> > fInfoSubGroups; //![NGroups][NSubs] Info/help on subgroup (per group)
   vector<string>          fNameGroups;    //![NGroups] List of group names (found in init. file)
   vector<vector<string> > fNameSubGroups; //![NGroups][NSubs] List of subgroup names (per group)
   vector<TUInitParEntry>  fPars;          //![NParams] List of params (from all groups/subgroups)

   void                    AddGroup(string const &group, string const &info_group);
   void                    AddSubGroup(string const&group, string const &subgroup, string const &info_subgroup);
   void                    SetGroup(string &line_group, string &current_group);
   void                    SetSubGroup(string &line_subgroup, string &current_group, string &current_subgroup);

public:
   TUInitParList();
   TUInitParList(TUInitParList const &par_list);
   TUInitParList(string const &f_parfile, Bool_t is_verbose, FILE *f_log);
   virtual ~TUInitParList();

   inline void             ClearVals(Int_t i_par)                                                              {fPars[i_par].ClearVals();}
   inline TUInitParList   *Clone()                                                                             {return new TUInitParList(*this);}
   void                    Copy(TUInitParList const &par_list);
   inline TUInitParList   *Create()                                                                            {return new TUInitParList;}
   inline void             Delete(Int_t i_par)                                                                 {fPars.erase(fPars.begin()+i_par);}
   gENUM_FCNTYPE           ExtractFormat(Int_t i_par, Int_t j_multi, string &extracted, Bool_t is_verbose, FILE *f_log) const;
   inline string           ExtractNameGroups() const                                                           {return TUMisc::List2String(fNameGroups, ',');}
   inline string           GetFileNames() const                                                                {return TUMisc::List2String(fFileNames, ',');}
   inline string           GetInfoGroup(Int_t i_group) const                                                   {return fInfoGroups[i_group];}
   inline string           GetInfoSubGroup(Int_t i_group, Int_t i_subgroup) const                              {return fInfoSubGroups[i_group][i_subgroup];}
   inline string           GetNameGroup(Int_t i_group) const                                                   {return fNameGroups[i_group];}
   inline string           GetNameSubGroup(Int_t i_group, Int_t i_subgroup) const                              {return fNameSubGroups[i_group][i_subgroup];}
   inline Int_t            GetNGroups() const                                                                  {return (Int_t)fNameGroups.size();}
   inline Int_t            GetNPars() const                                                                    {return (Int_t)fPars.size();}
   Int_t                   GetNPars(string const &group, string const &subgroup = "ALL") const;
   inline Int_t            GetNSubGroups(Int_t i_group) const                                                  {return (Int_t)fNameSubGroups[i_group].size();}
   inline TUInitParEntry   GetParEntry(Int_t i_par) const                                                      {return fPars[i_par];}
   inline Int_t            IndexPar(TUInitParEntry *par, Bool_t is_verbose=true) const                         {return IndexPar(par->GetGroup(), par->GetSubGroup(), par->GetName(), is_verbose);}
   Int_t                   IndexPar(string const &group, string const &subgroup, string const &param, Bool_t is_verbose=true) const;
   inline Int_t            IndexPar(string gr_sub_name[3], Bool_t is_verbose=true) const                       {return IndexPar(gr_sub_name[0], gr_sub_name[1], gr_sub_name[2], is_verbose);}
   Int_t                   IndexGroup(string const &group) const;
   Int_t                   IndexSubGroup(string const &group, string const &subgroup) const;
   vector<Int_t>           IndicesPars(string const &group = "ALL", string const &subgroup = "ALL") const;
   inline string           InfoGroup(Int_t i_group) const                                                      {return fInfoGroups[i_group];}
   inline string           InfoSubGroup(Int_t i_group, Int_t i_subgroup) const                                 {return fInfoSubGroups[i_group][i_subgroup];}
   void                    Initialise();
   void                    Print(FILE *f=stdout, string const &group = "ALL", string const &subgroup="ALL") const;
   void                    PrintListGroups(FILE *f=stdout) const;
   void                    PrintListSubGroups(FILE *f=stdout, string const &group = "ALL") const;
   void                    SetClass(string const &f_parfile, Bool_t is_verbose, FILE *f_log);
   inline void             SetVal(Int_t i_par, string const &val, Int_t j_multival=0)                          {fPars[i_par].SetVal(val,j_multival);}
   void                    TEST(string const &f_parfile, FILE *f=stdout);

   ClassDef(TUInitParList, 1) // Init. parameters passed to USINE SetClass() methods [loaded from init file]
};

#endif
