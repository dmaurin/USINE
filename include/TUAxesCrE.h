// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

#ifndef USINE_TUAxesCrE
#define USINE_TUAxesCrE

// C/C++ include
// ROOT include
// USINE include
#include "TUAxis.h"
#include "TUCoordE.h"
#include "TUCRList.h"

class TUAxesCrE : public TUCRList {

private:
   TUAxis           *fE[gN_CRFAMILY];       //![gN_CRFAMILY] E axes for each CR type (see TUEnum.h)
   vector<string>    fEVarsNames;           //![NEVars] Case sensitive E-dependent variables (beta, Rig, gamma...)
   vector<string>    fEVarsUnits;           // Units of fEVarsNames
   Double_t         *fEVars;                //![NCRs*NE*NEVars] Values of fEVarsNames at E bins
   Double_t         *fEVarsHalfBin;         //![NCRs*(NE+1)*NEVars] Values at half E bins (-1/2,1/2,...N+1/2)
   Int_t             fIndexBeta;            // Index of beta variable [-]
   Int_t             fIndexGamma;           // Index of gamma variable [-]
   Int_t             fIndexp;               // Index of momentum variable [GeV]
   Int_t             fIndexRig;             // Index of rigidity variable [GV]
   Int_t             fIndexEk;              // Index of Ek variable [GeV]
   Int_t             fIndexEkn;             // Index of Ekn variable [GeV/n]
   Int_t             fIndexEtot;            // Index of Etot variable [GeV]

   Double_t          CalculateEVarsVal_Rules(Int_t bincr, Double_t const &e_native, Int_t i_evar) const;
   inline string     GetEVarName(Int_t i_evar, Bool_t is_unit =false) const                            {return (is_unit) ? fEVarsNames[i_evar]+" ["+fEVarsUnits[i_evar]+"]" : fEVarsNames[i_evar];}
   inline Double_t   GetEVarVal(Int_t bincr, Int_t bine, Int_t i_evar, Int_t halfbin_status=0) const   {return (halfbin_status==0) ? fEVars[(bincr*GetNE()+bine)*GetNEVars()+i_evar] : fEVarsHalfBin[(bincr*(GetNE()+1)+bine+(halfbin_status+1)/2)*GetNEVars()+i_evar];}
   void              InitialiseE(Bool_t is_delete = false);


public:
   TUAxesCrE();
   TUAxesCrE(TUAxesCrE const &axes_cre);
   virtual ~TUAxesCrE();

   void              AllocateAndFillENamesAndEVars();
   void              CheckEGridInERange(Int_t bincr, TUAxis *e_grid,  gENUM_ETYPE e_type, Bool_t is_abort) const;
   inline TUAxesCrE *Clone()                                                                           {return new TUAxesCrE(*this);}
   void              ConvertBinE2ValsE(Int_t bincr, TUCoordE *coord_e, Bool_t is_use_or_copy=true) const;
   void              Copy(TUAxesCrE const &axes_cre);
   inline TUAxesCrE *Create()                                                                          {return new TUAxesCrE;}
   void              EvalEVarsVals(Int_t bincr, Double_t ekn_gevn, vector<Double_t> &evars_vals) const;
   string            FormEVarsValsE(Int_t bincr, Int_t bine, Int_t halfbin_status=0) const;
   Double_t          GammaRadBETA_perMyr(Int_t j_cr, Int_t k_ekn) const;
   Double_t          GammaRadEC_perMyr(Int_t j_cr, Int_t k_ekn) const;
   inline Double_t   GetBeta(Int_t bincr, Int_t bine, Int_t halfbin_status=0) const                    {return GetEVarVal(bincr, bine, fIndexBeta, halfbin_status);}
   inline TUAxis    *GetE(Int_t bincr) const                                                           {return fE[TUCRList::GetCREntry(bincr).GetFamily()];}
   Double_t          GetE(Int_t bincr, Int_t bine, gENUM_ETYPE e_type, Int_t halfbin_status=0) const;
   gENUM_ETYPE       GetEType(Int_t bincr) const;
   inline TUAxis    *GetEFamily(gENUM_CRFAMILY cr_family) const                                        {return fE[cr_family];}
   inline Double_t   GetEk(Int_t bincr, Int_t bine, Int_t halfbin_status=0) const                      {return GetEVarVal(bincr, bine, fIndexEk, halfbin_status);}
   inline Double_t   GetEkn(Int_t bincr, Int_t bine, Int_t halfbin_status=0) const                     {return GetEVarVal(bincr, bine, fIndexEkn, halfbin_status);}
   inline Double_t   GetEtot(Int_t bincr, Int_t bine, Int_t halfbin_status=0) const                    {return GetEVarVal(bincr, bine, fIndexEtot, halfbin_status);}
   inline string     GetEVarNames() const                                                              {return TUMisc::List2String(fEVarsNames, ',');}
   inline Double_t  *GetEVarsVals(Int_t bincr, Int_t bine, Int_t halfbin_status=0) const               {return (halfbin_status==0) ? &fEVars[(bincr*GetNE()+bine)*GetNEVars()+0] : &fEVarsHalfBin[(bincr*(GetNE()+1)+bine+(halfbin_status+1)/2)*GetNEVars()+0];}
   inline Double_t   GetGamma(Int_t bincr, Int_t bine, Int_t halfbin_status=0) const                   {return GetEVarVal(bincr, bine, fIndexGamma, halfbin_status);}
   inline Int_t      GetNE() const                                                                     {return (fE[0]) ? fE[0]->GetN() : 0;}
   inline Int_t      GetNEVars() const                                                                 {return (Int_t)fEVarsNames.size();}
   inline Double_t   Getp(Int_t bincr, Int_t bine, Int_t halfbin_status=0) const                       {return GetEVarVal(bincr, bine, fIndexp, halfbin_status);}
   inline Double_t   GetRig(Int_t bincr, Int_t bine, Int_t halfbin_status=0) const                     {return GetEVarVal(bincr, bine, fIndexRig, halfbin_status);}
   inline Double_t   GetVelocity_cmperMyr(Int_t bincr, Int_t bine, Int_t halfbin_status=0) const       {return GetBeta(bincr, bine, halfbin_status)*TUPhysics::C_cmperMyr();}
   inline Double_t   GetVelocity_cmpers(Int_t bincr, Int_t bine, Int_t halfbin_status=0) const         {return GetBeta(bincr, bine, halfbin_status)*TUPhysics::C_cmpers();}
   inline Int_t      IndexClosest(Int_t bincr, Double_t const &val) const                              {return fE[TUCRList::GetCREntry(bincr).GetFamily()]->IndexClosest(val);}
   void              Initialise(Bool_t is_delete = false);
   Bool_t            IsComboPossibleForEGrids(string const &combo, gENUM_CRFAMILY &common_family, FILE *f=stdout) const;
   void              LoadEVarsNamesAndUnits(Bool_t is_verbose, FILE *f_log);
   inline TUAxis    *OrphanGetE(Int_t bincr, gENUM_ETYPE e_type, gENUM_AXISTYPE axis_type) const       {TUAxis *axis = new TUAxis(GetE(bincr,0,e_type), GetE(bincr,GetNE()-1,e_type), GetNE(), TUEnum::Enum2Name(e_type), TUEnum::Enum2Name(e_type), axis_type); return axis;}
   TUAxis           *OrphanGetECombo(string const &combo, gENUM_ETYPE e_type, FILE *f=stdout) const;
   TUAxis           *OrphanGetECRs(vector<Int_t> const &cr_indices, gENUM_ETYPE e_type) const;
   inline void       PrintCRSummary(FILE *f=stdout) const                                              {PrintParents(f);}
   void              PrintESummary(FILE *f=stdout) const;
   void              PrintEVarsValues(FILE *f, Int_t bincr, Bool_t is_print_halfbin) const;
   void              PrintEVarsValues(FILE *f, Int_t bincr_first, Int_t bincr_last, Bool_t is_print_halfbin) const;
   void              SetClass(TUInitParList *init_pars, Bool_t is_verbose, FILE *f_log);
   inline void       SetEVar(Int_t bincr, Int_t bine, Int_t i_evar, Double_t const &val)               {fEVars[bincr*GetNE()*GetNEVars() + bine*GetNEVars() + i_evar] = val;}
   inline void       SetEVarHalfBin(Int_t bincr, Int_t bine, Int_t i_evar, Double_t const &val)        {fEVarsHalfBin[bincr*(GetNE()+1)*GetNEVars() + bine*GetNEVars() + i_evar] = val;}


   void              TEST(TUInitParList *init_pars, FILE *f=stdout);

  ClassDef(TUAxesCrE, 1)  // Energy grid (one per CR family) and CR 'grid' (TUCRList)
};

#endif
