// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

#ifndef USINE_TUValsTXYZEVirtual
#define USINE_TUValsTXYZEVirtual

// C/C++ include
// ROOT include
// USINE include
#include "TUCoordE.h"
#include "TUCoordTXYZ.h"
#include "TUFreeParList.h"

class TUValsTXYZEVirtual {
private:

public:
   TUValsTXYZEVirtual();
   virtual ~TUValsTXYZEVirtual() = 0;

   virtual TUValsTXYZEVirtual   *Clone() = 0;
   virtual TUValsTXYZEVirtual   *Create() = 0;
   inline virtual string         GetFormulaString() const                          {return "";}
   inline virtual string         GetFormulaVars() const                            {return "";}
   inline virtual TUFreeParList *GetFreePars() const                               {return NULL;}
   virtual string                GetName() const = 0;
   virtual void                  Initialise(Bool_t is_delete=false) = 0;
   virtual void                  PrintSummary(FILE *f=stdout, string const &description = "") const = 0;
   virtual void                  SetClass(string const &formula_or_file, Bool_t is_verbose, FILE *f_log, string const &commasep_vars="", TUFreeParList *free_pars = NULL, Bool_t is_use_or_copy_freepars=true) = 0;
   virtual void                  SetName(string const &name) = 0;
   inline virtual string         UID() const                                       {return "";}
   inline virtual void           UpdateFromFreeParsAndResetStatus(Bool_t is_force_update) {return;}
   inline virtual Double_t       Value()                                           {return 0.;}
   virtual Double_t              ValueE(TUCoordE *coord_e) = 0;
   virtual Double_t              ValueETXYZ(TUCoordE *coord_e, TUCoordTXYZ *coord_txyz) = 0;
   virtual Double_t              ValueTXYZ(TUCoordTXYZ *coord_txyz=NULL) = 0;
   virtual Int_t                 ValsTXYZEFormat() const = 0;

   ClassDef(TUValsTXYZEVirtual, 1)  // Abstract class for T,X,Y,Z,E values container
};

#endif
