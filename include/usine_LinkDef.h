// See http://root.cern.ch/drupal/content/selecting-dictionary-entries-linkdef+

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link off all namespaces;
//#pragma link C++ nestedclasses;

//#pragma link C++ enum gENUM_AXISTYPE;
//#pragma link C++ enum gENUM_BCTYPE;
//#pragma link C++ enum gENUM_CRFAMILY;
//#pragma link C++ enum gENUM_ETYPE;
//#pragma link C++ enum gENUM_ERRTYPE;
//#pragma link C++ enum gENUM_FCNTYPE;
//#pragma link C++ enum gENUM_INTERTYPE;
//#pragma link C++ enum gENUM_ISFLUX;
//#pragma link C++ enum gENUM_MATRIXINVERT;
//#pragma link C++ enum gENUM_PROPAG_MODEL;
//#pragma link C++ enum gENUM_SOLMOD_MODEL;
//#pragma link C++ enum gENUM_SOLMOD_PARAM;
//#pragma link C++ enum gENUM_SRCABUND_INIT;

#pragma link C++ namespace TUEnum;             //Dictionary
#pragma link C++ namespace TUInteractions;     //Dictionary
#pragma link C++ namespace TUMath;             //Dictionary
#pragma link C++ namespace TUMessages;         //Dictionary
#pragma link C++ namespace TUMisc;             //Dictionary
#pragma link C++ namespace TUNumMethods;       //Dictionary
#pragma link C++ namespace TUPhysics;          //Dictionary

#pragma link C++ class TUAtomElements+;        //Dictionary
#pragma link C++ class TUAtomProperties+;      //Dictionary
#pragma link C++ class TUAxesCrE+;             //Dictionary
#pragma link C++ class TUAxesTXYZ+;            //Dictionary
#pragma link C++ class TUAxis+;                //Dictionary
#pragma link C++ class TUBesselJ0+;            //Dictionary
#pragma link C++ class TUBesselQiSrc+;         //Dictionary
#pragma link C++ class TUCoordE+;              //Dictionary
#pragma link C++ class TUCoordTXYZ+;           //Dictionary
#pragma link C++ class TUCREntry+;             //Dictionary
//#pragma link C++ class TUCRFluxes+;            //Dictionary
#pragma link C++ class TUCRList+;              //Dictionary
#pragma link C++ class TUDatime+;              //Dictionary
#pragma link C++ class TUDataEntry+;           //Dictionary
#pragma link C++ class TUDataSet+;             //Dictionary
#pragma link C++ class TUFreeParEntry+;        //Dictionary
#pragma link C++ class TUFreeParList+;         //Dictionary
#pragma link C++ class TUInitParEntry+;        //Dictionary
#pragma link C++ class TUInitParList+;         //Dictionary
#pragma link C++ class TUMediumEntry+;         //Dictionary
#pragma link C++ class TUMediumTXYZ+;          //Dictionary
#pragma link C++ class TUModelBase+;           //Dictionary
#pragma link C++ class TUModel0DLeakyBox+;     //Dictionary
#pragma link C++ class TUModel1DKisoVc+;       //Dictionary
#pragma link C++ class TUModel2DKisoVc+;       //Dictionary
#pragma link C++ class TUNormEntry+;           //Dictionary
#pragma link C++ class TUNormList+;            //Dictionary
#pragma link C++ class TUPropagSwitches+;      //Dictionary
#pragma link C++ class TURunPropagation+;      //Dictionary
#pragma link C++ class TURunOutputs+;          //Dictionary
#pragma link C++ class TUSolMod0DFF+;          //Dictionary
//#pragma link C++ class TUSolMod0DFF_AMS+;      //Dictionary
//#pragma link C++ class TUSolMod1DSpher+;       //Dictionary
#pragma link C++ class TUSolModVirtual+;       //Dictionary
#pragma link C++ class TUSrcMultiCRs;          //Dictionary
#pragma link C++ class TUSrcPointLike+;        //Dictionary
#pragma link C++ class TUSrcSteadyState+;      //Dictionary
#pragma link C++ class TUSrcTemplates+;        //Dictionary
#pragma link C++ class TUSrcVirtual+;          //Dictionary
//#pragma link C++ class TUSpline+;              //Dictionary
//#pragma link C++ class TUSplineFitFcn+;        //Dictionary
#pragma link C++ class TUTransport+;           //Dictionary
#pragma link C++ class TUValsTXYZECr+;         //Dictionary
#pragma link C++ class TUValsTXYZEFormula+;    //Dictionary
#pragma link C++ class TUValsTXYZEGrid+;       //Dictionary
#pragma link C++ class TUValsTXYZEVirtual+;    //Dictionary
//#pragma link C++ class TUValsTXYZESpline+;     //Dictionary
#pragma link C++ class TUXSections+;           //Dictionary

#endif
