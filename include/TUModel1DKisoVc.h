// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

#ifndef USINE_TUModel1DKisoVc
#define USINE_TUModel1DKisoVc

// C/C++ include
// ROOT include
// USINE include
#include "TUModelBase.h"

class TUModel1DKisoVc : public TUModelBase {

private:

   Double_t               *fA1DTerm;             //![NCRs*NE]
   Double_t               *fCoefPreFactor;       //![NCRs*NE] 2h/(A*Ek) in Eq.(1)  [Myr/GeV]
   Int_t                   fIndexPar_h;          // Index h (disc half-thickness) in TUFreeParList [kpc]
   Int_t                   fIndexPars_L;         // Index L (halo half-thickness) in TUFreeParList [kpc]
   Int_t                   fIndexPars_rh;        // Index rh (local bubble radius) in TUFreeParList [kpc]
   gENUM_MATRIXINVERT      fInvertScheme;        // Scheme matrix inversion (kTRID or kGAUSSJORDAN)
   TUMediumEntry          *fISMEntry;            // 1D model constant interstellar medium
   Double_t               *fN0Prim;              //![NCRs*NE] N(z=0) (primary only) [#part/(m^3.GeV/n)]
   Double_t               *fN0Tot;               //![NCRs*NE] N(z=0) (all contribs) [#part/(m^3.GeV/n)]

   Double_t                A1D(Int_t j_cr, Int_t k_ekn, Double_t const &kdiff);
   Double_t                A1D(Int_t j_cr, Int_t k_ekn);
   Double_t                A1D(Double_t const &s1d, Int_t j_cr, Int_t k_ekn, Double_t const &kdiff);
   virtual Bool_t          CalculateChargedCRs(Int_t jcr_start, Int_t jcr_stop, Bool_t is_verbose, FILE *f_log);
   void                    FillCoefPreFactorAndA1DTerm();
   inline Double_t        *GetA1DTerm(Int_t j_cr) const                                         {return &fA1DTerm[j_cr * GetNE() + 0];}
   inline Double_t         GetA1DTerm(Int_t j_cr, Int_t k_ekn) const                            {return fA1DTerm[j_cr * GetNE() + k_ekn];}
   inline Double_t        *GetCoefPreFactor(Int_t j_cr)                                         {return &(fCoefPreFactor[j_cr*GetNE() + 0]);}
   inline Double_t         GetCoefPreFactor(Int_t j_cr, Int_t k_ekn) const                      {return fCoefPreFactor[j_cr*GetNE() + k_ekn];}
   inline Double_t        *GetN0Prim(Int_t j_cr) const                                          {return &(fN0Prim[j_cr * GetNE() + 0]);}
   inline Double_t         GetN0Prim(Int_t j_cr, Int_t k_ekn) const                             {return fN0Prim[j_cr * GetNE() + k_ekn];}
   inline Double_t        *GetN0Tot(Int_t j_cr) const                                           {return &(fN0Tot[j_cr * GetNE() + 0]);}
   inline Double_t         GetN0Tot(Int_t j_cr, Int_t k_ekn) const                              {return fN0Tot[j_cr * GetNE() + k_ekn];}
   void                    Initialise(Bool_t is_delete);
   void                    IterateTertiariesN0(Int_t j_cr, Double_t* sterm_tot,  Double_t* sterm_prim, Double_t const &eps, Bool_t is_verbose, FILE *f_log);
   Double_t                Nz_WithoutParentBETA(Double_t const &n0, Int_t j_cr, Int_t k_ekn, Double_t z);
   Double_t                Nz_WithParentBETA(Int_t j_parent, Int_t j_cr, Int_t k_ekn, Double_t z);
   Double_t                S1D(Int_t j_cr, Int_t k_ekn, Double_t const &kdiff);
   Double_t                S1D(Int_t j_cr, Int_t k_ekn);
   inline void             SetA1DTerm(Int_t j_cr, Int_t k_ekn, Double_t const &val)            {fA1DTerm[j_cr * GetNE() + k_ekn] = val;}
   virtual Bool_t          SetClass_ModelSpecific(TUInitParList *init_pars, Bool_t is_verbose, FILE *f_log);
   inline void             SetCoefPreFactor(Int_t j_cr, Int_t k_ekn, Double_t const &val)      {fCoefPreFactor[j_cr*GetNE()+k_ekn] = val;}
   inline void             SetN0Prim(Int_t j_cr, Int_t k_ekn, Double_t const &val)             {fN0Prim[j_cr * GetNE() + k_ekn] = val;}
   inline void             SetN0Tot(Int_t j_cr, Int_t k_ekn, Double_t const &val)              {fN0Tot[j_cr * GetNE() + k_ekn] = val;}

public:

   TUModel1DKisoVc();
   virtual ~TUModel1DKisoVc();

   inline virtual Double_t AdiabaticLosses(Int_t j_cr, Double_t const &ek, TUCoordTXYZ *coord_txyz=NULL) {return (-ek*(2.*GetCREntry(j_cr).GetmGeV()+ek)/(GetCREntry(j_cr).GetmGeV()+ek)*ValueVc_kpcperMyr()/(3.*GetParVal_h()));}
   virtual Double_t        CoeffReac1stOrder(Int_t j_cr, Int_t k_ekn);
   virtual Double_t        CoeffReac2ndOrderLowerHalfBin(Int_t j_cr, Int_t k_ekn);
   Double_t                EvalFlux(Int_t j_cr, Int_t k_ekn, Double_t z, Int_t tot0_prim1_sec2=0);
   inline virtual Double_t EvalFluxPrim(Int_t j_cr, Int_t k_ekn, TUCoordTXYZ *coord_txyz=NULL) {return EvalFlux(j_cr, k_ekn, coord_txyz->GetValX(), 1);}
   inline virtual Double_t EvalFluxTot(Int_t j_cr, Int_t k_ekn, TUCoordTXYZ *coord_txyz=NULL)  {return EvalFlux(j_cr, k_ekn, coord_txyz->GetValX(), 0);}
   inline TUFreeParEntry  *GetPar_h()                                                          {return TUModelBase::TUFreeParList::GetParEntry(fIndexPar_h);}
   inline TUFreeParEntry  *GetPar_L()                                                          {return TUModelBase::TUFreeParList::GetParEntry(fIndexPars_L);}
   inline TUFreeParEntry  *GetPar_rh()                                                         {return TUModelBase::TUFreeParList::GetParEntry(fIndexPars_rh);}
   inline Double_t         GetParVal_h() const                                                 {return TUModelBase::TUFreeParList::GetParEntry(fIndexPar_h)->GetVal(false);}
   inline Double_t         GetParVal_L() const                                                 {return TUModelBase::TUFreeParList::GetParEntry(fIndexPars_L)->GetVal(false);}
   inline Double_t         GetParVal_rh() const                                                {return TUModelBase::TUFreeParList::GetParEntry(fIndexPars_rh)->GetVal(false);}
   virtual void            InitialiseForCalculation(Bool_t is_init_or_update, Bool_t is_force_update, Bool_t is_verbose, FILE *f_log);
   void                    TEST(TUInitParList *init_pars, FILE *f=stdout);
   Double_t                ValueK(Int_t j_cr, Int_t k_ekn, Int_t halfbin_status=0);
   Double_t                ValueKpp(Int_t j_cr, Int_t k_ekn, Int_t halfbin_status=0);
   inline Double_t         ValueVa_kmpers()                                                    {return TUModelBase::ValueVA();}
   inline Double_t         ValueVa_kpcperMyr()                                                 {return (ValueVa_kmpers()*TUPhysics::Convert_kmpers_to_kpcperMyr());}
   inline Double_t         ValueVc_kmpers()                                                    {return TUModelBase::ValueWind();}
   inline Double_t         ValueVc_kpcperMyr()                                                 {return (ValueVc_kmpers()*TUPhysics::Convert_kmpers_to_kpcperMyr());}

   ClassDef(TUModel1DKisoVc, 1)  // Propagation model 1D (thin-disc, thick-halo): K_iso(E)+Vc
};

#endif
