// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

#ifndef USINE_TUTransport
#define USINE_TUTransport

// C/C++ include
// ROOT include
// USINE include
#include "TUValsTXYZEVirtual.h"

class TUTransport {

private:
   TUFreeParList             *fFreePars; // Free parameters for the transport parameters below
   Int_t                      fNK;       // Number of components used for the diffusion tensor (<=9)
   Int_t                      fNWind;    // Number of components used for the wind component (<=3)
   TUValsTXYZEVirtual        *fKpp;      // Scalar momentum diffusion coefficient [GeV^2/Myr]
   TUValsTXYZEVirtual       **fK;        //![fNK] Spatial diffusion tensor [kpc^2/Myr]
   TUValsTXYZEVirtual        *fVA;       // Scalar Alfvén velocity [km/s]
   TUValsTXYZEVirtual       **fWind;     //![fNWind] Wind velocity components (up to 3) [km/s]

   void                       Initialise(Bool_t is_delete = true);

public:
   TUTransport();
   TUTransport(TUTransport const &transport);
   virtual ~TUTransport();


   inline TUTransport        *Clone()                                                         {return new TUTransport(*this);}
   void                       Copy(TUTransport const &transport);
   inline TUTransport        *Create()                                                        {return new TUTransport;}
   inline TUFreeParList      *GetFreePars() const                                             {return fFreePars;}
   inline TUValsTXYZEVirtual *GetK(Int_t row=0, Int_t col=0) const                            {return fK[col*3+row];}
   inline TUValsTXYZEVirtual *GetKpp() const                                                  {return fKpp;}
   inline Int_t               GetNK() const                                                   {return fNK;}
   inline Int_t               GetNWind() const                                                {return fNWind;}
   inline Double_t            GetParVal(Int_t i_par) const                                    {return fFreePars->GetParEntry(i_par)->GetVal(false);}
   inline TUValsTXYZEVirtual *GetVA() const                                                   {return fVA;}
   inline TUValsTXYZEVirtual *GetWind(Int_t i=0) const                                        {return fWind[i];}
   inline Int_t               IndexPar(string const &par_name) const                          {return fFreePars->IndexPar(par_name);}
   inline void                PrintKpp(FILE *f=stdout) const                                  {if (fKpp) fKpp->PrintSummary(f,"Kpp");}
   inline void                PrintK(FILE *f=stdout, Int_t row=0, Int_t col=0) const          {if (fK && fK[col*3+row]) fK[col*3+row]->PrintSummary(f, Form("K%d%d", row,col));}
   void                       PrintSummary(FILE *f=stdout) const;
   inline void                PrintVA(FILE *f=stdout) const                                   {if (fVA) fVA->PrintSummary(f,"VA");}
   inline void                PrintWind(FILE *f=stdout, Int_t coord=0) const                  {if (fWind[coord]) fWind[coord]->PrintSummary(f, Form("Wind%d", coord));}
   void                       SetClass(TUInitParList *init_pars, string const &group, string const &subgroup, Bool_t is_verbose, FILE *f_log);
   void                       TEST(TUInitParList *init_pars, FILE *f=stdout);
   inline Bool_t              TUI_ModifyFreeParameters()                                      {return fFreePars->TUI_Modify();}
   void                       UpdateFromFreeParsAndResetStatus();
   inline Double_t            ValueK(TUCoordE *coord_e, TUCoordTXYZ *coord_txyz, Int_t row, Int_t col) {UpdateFromFreeParsAndResetStatus(); return fK[col*3+row]->ValueETXYZ(coord_e, coord_txyz);}
   inline Double_t            ValueK(TUCoordE *coord_e, Int_t row=0, Int_t col=0)             {TUCoordTXYZ *coord_txyz=NULL; UpdateFromFreeParsAndResetStatus(); return fK[col*3+row]->ValueETXYZ(coord_e, coord_txyz);}
   inline Double_t            ValueKpp(TUCoordE *coord_e, TUCoordTXYZ *coord_txyz=NULL)       {UpdateFromFreeParsAndResetStatus(); return fKpp->ValueETXYZ(coord_e, coord_txyz);}
   inline Double_t            ValueVA(TUCoordTXYZ *coord_txyz=NULL)                           {UpdateFromFreeParsAndResetStatus(); return fVA->ValueTXYZ(coord_txyz);}
   inline Double_t            ValueWind(TUCoordTXYZ *coord_txyz=NULL, Int_t coord=0)          {UpdateFromFreeParsAndResetStatus(); return fWind[coord]->ValueTXYZ(coord_txyz);}

   ClassDef(TUTransport, 1)  // Transport coefficients: diffusion tensor, convection,... [grid or formula]
};

#endif
