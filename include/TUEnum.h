// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

#ifndef USINE_TUEnum
#define USINE_TUEnum

// C/C++ include
#include <string>
using namespace std;
// ROOT include
#include <Rtypes.h>
// USINE include
#include "TUMessages.h"
#include "TUMisc.h"

const string gUSINE_VERSION  = "USINE V3.5";

// Generic macro to automatically declare Name2Enum...
#define DEFINE_NAME2ENUM(typeName)                                        \
   inline gENUM_##typeName Name2Enum_##typeName(string str)               \
   {                                                                      \
      TUMisc::UpperCase(str);                                             \
      if (str[0] == 'K') str.erase(0, 1); /* if start with "k" */         \
      string valid_names = "";                                            \
      for (Int_t i = 0; i < gN_##typeName; ++i) {                         \
         string test = gENUM_##typeName##_NAME[i];                        \
         TUMisc::UpperCase(test);                                         \
         if (str == test)                                                 \
            return gENUM_##typeName##_LIST[i];                            \
         else valid_names += "k" + test + " ";                            \
      }                                                                   \
      string message = str + " is not valid, choose enum in "             \
                    + valid_names;                                        \
      TUMessages::Error(stdout, "TUEnum", "Name2Enum", message);          \
      return gENUM_##typeName##_LIST[0];                                  \
   }

enum         gENUM_AXISTYPE      {kLIN=0, kLOG};
enum         gENUM_BCTYPE        {kUNSET=0, kNOCHANGE, kD2NDLNEKN2_ZERO, kNOCURRENT, kDFDP_ZERO};
enum         gENUM_CRFAMILY      {kNUC=0, kANTINUC, kLEPTON};
enum         gENUM_ETYPE         {kEKN=0, kR, kETOT, kEK};
enum         gENUM_ERRTYPE       {kERRSTAT=0, kERRSYST, kERRTOT, kERRCOV};
enum         gENUM_FCNTYPE       {kFORMULA=0, kSPLINE, kGRID};
enum         gENUM_FREEPARTYPE   {kANY=-1, kFIT=0, kFIXED, kNUISANCE};
enum         gENUM_INTERPTYPE    {kLINLIN=0, kLINLOG, kLOGLIN, kLOGLIN_INFNULL_SUPCST, kLOGLOG};
enum         gENUM_ISFLUX        {kGARCIAMUNOZ75=0, kBURGER00, kLANGNER03, kWEBBERHIGBIE03, kSHIKAZE07,kWEBBERHIGBIE09,kBADHWARONEILL11,kCHEMINET14,kBURGER00G14,kBADHWARONEILL11G14, kWEBBERHIGBIE03G14, kLOGPOLYNOMIALG14,kFLUXEKN14,kZHAOR14,kZHAOEKN14,kZHAOETOT14,kMAURIN14};
enum         gENUM_MATRIXINVERT  {kTRID=0, kGAUSSJORDAN};
enum         gENUM_PROPAG_MODEL  {kBASE=-1, kMODEL0DLEAKYBOX, kMODEL1DKISOVC, kMODEL2DKISOVC};
enum         gENUM_SOLMOD_MODEL  {kIS=0, kSOLMOD0DFF, kSOLMOD0DFF_AMS, kSOLMOD1DSPHER};
enum         gENUM_SOLMOD_PARAM  {kPHI=0};
enum         gENUM_SRCABUND_INIT {kSSISOTFRAC=0, kSSISOTABUND, kFIPBIAS};

// Number of available options for each enum
const Int_t  gN_AXISTYPE = 2;
const Int_t  gN_BCTYPE = 5;
const Int_t  gN_CRFAMILY = 3;
const Int_t  gN_ETYPE = 4;
const Int_t  gN_ERRTYPE = 4;
const Int_t  gN_FCNTYPE = 3;
const Int_t  gN_FREEPARTYPE = 3;
const Int_t  gN_INTERPTYPE = 5;
const Int_t  gN_ISFLUX = 17;
const Int_t  gN_MATRIXINVERT = 2;
const Int_t  gN_PROPAG_MODEL = 3;
const Int_t  gN_SOLMOD_MODEL = 4;
const Int_t  gN_SOLMOD_PARAM = 1;
const Int_t  gN_SRCABUND_INIT = 3;

namespace TUEnum {
   // Axis lin or log
   const gENUM_AXISTYPE      gENUM_AXISTYPE_LIST[gN_AXISTYPE] = {kLIN, kLOG};
   const string              gENUM_AXISTYPE_NAME[gN_AXISTYPE] = {"Lin", "Log"};
   // Boundary conditions
   const gENUM_BCTYPE        gENUM_BCTYPE_LIST[gN_BCTYPE] = {kUNSET, kNOCHANGE, kD2NDLNEKN2_ZERO, kNOCURRENT, kDFDP_ZERO};
   const string              gENUM_BCTYPE_NAME[gN_BCTYPE] = {"Unset", "NoChange", "D2NDLNEKN2_ZERO", "NOCURRENT", "DFDP_ZERO"};
   // Family for CR data
   const gENUM_CRFAMILY      gENUM_CRFAMILY_LIST[gN_CRFAMILY] = {kNUC, kANTINUC, kLEPTON};
   const string              gENUM_CRFAMILY_NAME[gN_CRFAMILY] = {"Nuclei", "Anti-nuclei", "Leptons"};
   const string              gENUM_CRFAMILY_UNIT[gN_CRFAMILY] = {"GeV/n", "GeV/n", "GeV"};
   // X-axis types for Energy
   const gENUM_ETYPE         gENUM_ETYPE_LIST[gN_ETYPE] = {kEKN, kR, kETOT, kEK};
   const string              gENUM_ETYPE_NAME[gN_ETYPE] = {"Ekn", "R", "Etot", "Ek"};
   const string              gENUM_ETYPE_UNIT[gN_ETYPE] = {"GeV/n", "GV", "GeV", "GeV"};
   // Error types
   const gENUM_ERRTYPE       gENUM_ERRTYPE_LIST[gN_ERRTYPE] = {kERRSTAT, kERRSYST, kERRTOT, kERRCOV};
   const string              gENUM_ERRTYPE_NAME[gN_ERRTYPE] = {"ErrStat", "ErrSyst", "ErrTot", "ErrCov"};
   // Family of FCN types
   const gENUM_FCNTYPE       gENUM_FCNTYPE_LIST[gN_FCNTYPE] = {kFORMULA, kSPLINE, kGRID};
   const string              gENUM_FCNTYPE_NAME[gN_FCNTYPE] = {"FORMULA", "SPLINE", "GRID"};
   // Family of FREEPAR types
   const gENUM_FREEPARTYPE   gENUM_FREEPARTYPE_LIST[gN_FREEPARTYPE] = {kFIT, kFIXED, kNUISANCE};
   const string              gENUM_FREEPARTYPE_NAME[gN_FREEPARTYPE] = {"FIT", "FIXED", "NUISANCE"};
   // Interpolation (and extrapolation) types
   const gENUM_INTERPTYPE    gENUM_INTERPTYPE_LIST[gN_INTERPTYPE] = {kLINLIN, kLINLOG, kLOGLIN, kLOGLIN_INFNULL_SUPCST, kLOGLOG};
   const string              gENUM_INTERPTYPE_NAME[gN_INTERPTYPE] = {"LinLin", "LinLog", "LogLin", "LogLin_InfNull_SupCst", "LogLog"};
   // Family of IS fluxes
   const gENUM_ISFLUX        gENUM_ISFLUX_LIST[gN_ISFLUX] = {kGARCIAMUNOZ75, kBURGER00, kLANGNER03, kWEBBERHIGBIE03, kSHIKAZE07,kWEBBERHIGBIE09, kBADHWARONEILL11, kCHEMINET14,kBURGER00G14,kBADHWARONEILL11G14, kWEBBERHIGBIE03G14, kLOGPOLYNOMIALG14, kFLUXEKN14, kZHAOR14,kZHAOEKN14,kZHAOETOT14,kMAURIN14};
   const string              gENUM_ISFLUX_NAME[gN_ISFLUX] = {"GARCIAMUNOZ75", "BURGER00_USOSKIN05", "LANGNER03", "WEBBERHIGBIE03", "SHIKAZE07", "WEBBERHIGBIE09", "BADHWARONEILL11", "CHEMINET14", "BURGER00_USOSKIN05_G14", "BADHWARONEILL11_G14", "WEBBERHIGBIE03_G14", "LOGPOLYNOMIAL_G14", "FLUXEKN14", "ZHAOR14", "ZHAOEKN14", "ZHAOETOT14","MAURIN14"};
  // Matrix inversion method selected
   const gENUM_MATRIXINVERT  gENUM_MATRIXINVERT_LIST[gN_MATRIXINVERT] = {kTRID, kGAUSSJORDAN};
   const string              gENUM_MATRIXINVERT_NAME[gN_MATRIXINVERT] = {"Tridiagonal", "GaussJordan"};
   // Propagation Models
   const gENUM_PROPAG_MODEL  gENUM_PROPAG_MODEL_LIST[gN_PROPAG_MODEL] = {kMODEL0DLEAKYBOX, kMODEL1DKISOVC, kMODEL2DKISOVC};
   const string              gENUM_PROPAG_MODEL_NAME[gN_PROPAG_MODEL] = {"Model0DLeakyBox", "Model1DKisoVc", "Model2DKisoVc"};
   // Solar Modulation Models (option kIS is used to force 'Interstellar', i.e. no modulation)
   const gENUM_SOLMOD_MODEL  gENUM_SOLMOD_MODEL_LIST[gN_SOLMOD_MODEL] = {kIS, kSOLMOD0DFF,  kSOLMOD0DFF_AMS, kSOLMOD1DSPHER};
   const string              gENUM_SOLMOD_MODEL_NAME[gN_SOLMOD_MODEL] = {"IS", "SolMod0DFF", "SolMod0DFF_AMS", "SolMod1DSpher"};
   // Solar Modulation Parameters (if you add a new sol_mod 'parameter' type, don't forget to update SetSolModPar() in TUDataEntry)
   const gENUM_SOLMOD_PARAM  gENUM_SOLMOD_PARAM_LIST[gN_SOLMOD_PARAM] = {kPHI};
   const string              gENUM_SOLMOD_PARAM_NAME[gN_SOLMOD_PARAM] = {"#phi"};
   const string              gENUM_SOLMOD_PARAM_UNIT[gN_SOLMOD_PARAM] = {"GV"};
   // Source abundance initialisation
   const gENUM_SRCABUND_INIT gENUM_SRCABUND_INIT_LIST[gN_SRCABUND_INIT] = {kSSISOTFRAC, kSSISOTABUND, kFIPBIAS};
   const string              gENUM_SRCABUND_INIT_NAME[gN_SRCABUND_INIT] = {"SSIsotFrac", "SSIsotAbund", "FIPBias"};

   // For all enum typeName (e.g. RCUTOFF) define from MACRO 'DEFINE_NAME2ENUM(typeName)'
   // (see in TUEnum.h) the function, e.g., gENUM_RCUTOFF String2RCUTOFF(string)'
   DEFINE_NAME2ENUM(AXISTYPE)
   DEFINE_NAME2ENUM(BCTYPE)
   DEFINE_NAME2ENUM(CRFAMILY)
   DEFINE_NAME2ENUM(ETYPE)
   DEFINE_NAME2ENUM(ERRTYPE)
   DEFINE_NAME2ENUM(FCNTYPE)
   DEFINE_NAME2ENUM(FREEPARTYPE)
   DEFINE_NAME2ENUM(INTERPTYPE)
   DEFINE_NAME2ENUM(ISFLUX)
   DEFINE_NAME2ENUM(MATRIXINVERT)
   DEFINE_NAME2ENUM(PROPAG_MODEL)
   DEFINE_NAME2ENUM(SOLMOD_MODEL)
   DEFINE_NAME2ENUM(SOLMOD_PARAM)
   DEFINE_NAME2ENUM(SRCABUND_INIT)

   string                    CRQUnit(gENUM_ETYPE const etype, Bool_t is_bracket);
   inline string             Enum2Name(gENUM_AXISTYPE e)                        {return gENUM_AXISTYPE_NAME[e];}
   inline string             Enum2Name(gENUM_BCTYPE e)                          {return gENUM_BCTYPE_NAME[e];}
   inline string             Enum2Name(gENUM_CRFAMILY e)                        {return gENUM_CRFAMILY_NAME[e];}
   inline string             Enum2Name(gENUM_ETYPE e)                           {return gENUM_ETYPE_NAME[e];}
   inline string             Enum2Name(gENUM_ERRTYPE e)                         {return gENUM_ERRTYPE_NAME[e];}
   inline string             Enum2Name(gENUM_FCNTYPE e)                         {return gENUM_FCNTYPE_NAME[e];}
   inline string             Enum2Name(gENUM_FREEPARTYPE e)                     {return gENUM_FREEPARTYPE_NAME[e];}
   inline string             Enum2Name(gENUM_INTERPTYPE e)                      {return gENUM_INTERPTYPE_NAME[e];}
   inline string             Enum2Name(gENUM_ISFLUX e)                          {return gENUM_ISFLUX_NAME[e];}
   inline string             Enum2Name(gENUM_MATRIXINVERT e)                    {return gENUM_MATRIXINVERT_NAME[e];}
   inline string             Enum2Name(gENUM_PROPAG_MODEL e)                    {return gENUM_PROPAG_MODEL_NAME[e];}
   inline string             Enum2Name(gENUM_SOLMOD_MODEL e)                    {return gENUM_SOLMOD_MODEL_NAME[e];}
   inline string             Enum2Name(gENUM_SOLMOD_PARAM e)                    {return gENUM_SOLMOD_PARAM_NAME[e];}
   inline string             Enum2Name(gENUM_SRCABUND_INIT e)                   {return gENUM_SRCABUND_INIT_NAME[e];}
   inline string             Enum2NameUnit(gENUM_CRFAMILY e)                    {return gENUM_CRFAMILY_NAME[e] + " [" + gENUM_CRFAMILY_UNIT[e] + "]";}
   inline string             Enum2NameUnit(gENUM_ETYPE e)                       {return gENUM_ETYPE_NAME[e] + " [" + gENUM_ETYPE_UNIT[e] + "]";}
   inline string             Enum2NameUnit(gENUM_SOLMOD_PARAM e)                {return gENUM_SOLMOD_PARAM_NAME[e] + " [" + gENUM_SOLMOD_PARAM_UNIT[e] + "]";}
   inline string             Enum2Unit(gENUM_CRFAMILY e, Bool_t is_bracket)     {return ((is_bracket) ? ("["+gENUM_CRFAMILY_UNIT[e]+"]") : gENUM_CRFAMILY_UNIT[e]);}
   inline string             Enum2Unit(gENUM_ETYPE e, Bool_t is_bracket)        {return ((is_bracket) ? ("["+gENUM_ETYPE_UNIT[e]+"]") : gENUM_ETYPE_UNIT[e]);}
   inline string             Enum2Unit(gENUM_SOLMOD_PARAM e, Bool_t is_bracket) {return ((is_bracket) ? ("["+gENUM_SOLMOD_PARAM_UNIT[e]+"]") : gENUM_SOLMOD_PARAM_UNIT[e]);}
   gENUM_CRFAMILY            ExtractCRFamily(string qty);
   inline void               Name2Enum(string &name, gENUM_AXISTYPE &e)         {e = Name2Enum_AXISTYPE(name);}
   inline void               Name2Enum(string &name, gENUM_BCTYPE &e)           {e = Name2Enum_BCTYPE(name);}
   inline void               Name2Enum(string &name, gENUM_CRFAMILY &e)         {e = Name2Enum_CRFAMILY(name);}
   inline void               Name2Enum(string &name, gENUM_ETYPE &e)            {e = Name2Enum_ETYPE(name);}
   inline void               Name2Enum(string &name, gENUM_ERRTYPE &e)          {e = Name2Enum_ERRTYPE(name);}
   inline void               Name2Enum(string &name, gENUM_FCNTYPE &e)          {e = Name2Enum_FCNTYPE(name);}
   inline void               Name2Enum(string &name, gENUM_FREEPARTYPE &e)      {e = Name2Enum_FREEPARTYPE(name);}
   inline void               Name2Enum(string &name, gENUM_INTERPTYPE &e)       {e = Name2Enum_INTERPTYPE(name);}
   inline void               Name2Enum(string &name, gENUM_ISFLUX &e)           {e = Name2Enum_ISFLUX(name);}
   inline void               Name2Enum(string &name, gENUM_MATRIXINVERT &e)     {e = Name2Enum_MATRIXINVERT(name);}
   inline void               Name2Enum(string &name, gENUM_PROPAG_MODEL &e)     {e = Name2Enum_PROPAG_MODEL(name);}
   inline void               Name2Enum(string &name, gENUM_SOLMOD_MODEL &e)     {e = Name2Enum_SOLMOD_MODEL(name);}
   inline void               Name2Enum(string &name, gENUM_SOLMOD_PARAM &e)     {e = Name2Enum_SOLMOD_PARAM(name);}
   inline void               Name2Enum(string &name, gENUM_SRCABUND_INIT &e)    {e = Name2Enum_SRCABUND_INIT(name);}
   void                      XYTitles(string &x_title, string &y_title, string const &qty, gENUM_ETYPE e_type, Double_t const &e_power = 0, Bool_t is_qty_cr_combo=true);
}

#endif
