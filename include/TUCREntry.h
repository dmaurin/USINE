// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

#ifndef USINE_TUCREntry
#define USINE_TUCREntry

// C/C++ include
// ROOT include
// USINE include
#include "TUAtomElements.h"
#include "TUPhysics.h"

class TUCREntry {

private:
   Int_t                     fA;                   // Atomic weight A
   Double_t                  fBETAHalfLife;        // Half-life (-1 if stable) [Myr]
   Double_t                  fBETAHalfLifeErr;     // Error on half-life [Myr]
   string                    fBETACRFriend;        // Daughter/progenitor name (case insensitive)
   Double_t                  fECHalfLife;          // Half-life (-1 if stable) [Myr]
   Double_t                  fECHalfLifeErr;       // Error on half-life [Myr]
   string                    fECCRFriend ;         // Daughter/progenitor name (case insensitive)
   gENUM_CRFAMILY            fFamily;              // kNUC, kANTINUC, or kLEPTON
   Double_t                  fGhostBr;             // Branching ratio (if entry=ghost) to calculate Xsec
   vector<TUCREntry>         fGhosts;              //[NGhosts] List of entry 'ghosts' to calculate XSec
   Bool_t                    fIsEmpty;             // Whether the class is filled or not with a CR
   Bool_t                    fIsUnstable;          // CR is unstable if  type = BETA, EC, or MIX
   Double_t                  fm;                   // Mass in a.m.u. (also defined for non-nuclei)
   string                    fName;                // CR name (case insensitive)
   Double_t                  fRrms;                // Nucleus mean radius (unused in propagation)
   Double_t                  fSSIsotopeAbundance;  // Solar system isotope (relative) abundance
   Double_t                  fSSIsotopicFraction;  // Solar system isotopic fraction
   string                    fType;                // STABLE, BETA(-FED), EC(-FED), MIX(-FED)
   Int_t                     fZ;                   // Atomic number Z

   void                      Initialise();
   inline Double_t           GetGhostBr() const                                    {return fGhostBr;}
   void                      SetFamily();

public:
   TUCREntry();
   TUCREntry(TUCREntry const &cr);
   virtual ~TUCREntry();

   inline void               AddGhost(TUCREntry const &ghost)                      {fGhosts.push_back(ghost);}
   inline TUCREntry         *Clone()                                               {return new TUCREntry(*this);}
   void                      Copy(TUCREntry const &cr);
   inline TUCREntry         *Create()                                              {return new TUCREntry;}
   inline Int_t              GetA() const                                          {return fA;}
   inline Double_t           GetBETAHalfLife() const                               {return fBETAHalfLife;}
   inline Double_t           GetBETAHalfLifeErr() const                            {return fBETAHalfLifeErr;}
   inline string             GetBETACRFriend() const                               {return fBETACRFriend;}
   inline Double_t           GetECHalfLife() const                                 {return fECHalfLife;}
   inline Double_t           GetECHalfLifeErr() const                              {return fECHalfLifeErr;}
   inline string             GetECCRFriend() const                                 {return fECCRFriend;}
   inline gENUM_CRFAMILY     GetFamily() const                                     {return fFamily;}
   inline TUCREntry          GetGhost(Int_t j_ghost) const                         {return fGhosts[j_ghost];};
   inline Double_t           GetGhostBr(Int_t j_ghost) const                       {return fGhosts[j_ghost].GetGhostBr();};
   inline Double_t           Getmamu() const                                       {return fm;}
   inline Double_t           GetmGeV() const                                       {return (fm * TUPhysics::Convert_amu_to_GeV());}
   inline string             GetName(Int_t switch_format=0) const                  {return ((switch_format==0) ? fName : TUMisc::FormatCRQty(fName,switch_format));}
   inline Int_t              GetNGhosts() const                                    {return (Int_t)fGhosts.size();};
   inline Double_t           GetRrms() const                                       {return fRrms;}
   inline Double_t           GetSSIsotopeAbundance() const                         {return fSSIsotopeAbundance;}
   inline Double_t           GetSSIsotopicFraction() const                         {return fSSIsotopicFraction;}
   inline string             GetType() const                                       {return fType;}
   inline Int_t              GetZ() const                                          {return fZ;}
   Bool_t                    IsChargedCR() const;
   inline Bool_t             IsEmpty() const                                       {return fIsEmpty;}
   Bool_t                    IsSame(TUCREntry const &cr) const;
   inline Bool_t             IsUnstable() const                                    {return fIsUnstable;}
   Bool_t                    IsUnstable(Bool_t is_beta_or_ec) const;
   void                      Print(FILE *f=stdout, Bool_t is_ghost=false) const;
   void                      PrintGhosts(FILE *f=stdout, Bool_t is_only_ghosts=false) const;
   inline void               SetCRFriend(string const &name, Bool_t is_beta_or_ec) {(is_beta_or_ec) ? fBETACRFriend=name : fECCRFriend=name;}
   inline void               SetGhost(Int_t a, Int_t z, Double_t const &m_amu, Double_t const &br, string const &name) {fZ = z; fA = a; fm = m_amu; fGhostBr = br; fName = name;}
   inline void               SetHalfLife(Double_t const &hl, Bool_t is_beta_or_ec) {(is_beta_or_ec) ? fBETAHalfLife = hl : fECHalfLife = hl;}
   void                      SetEntry(Int_t a, Int_t z, Double_t const &m_gev, string const &name = "");
   void                      SetEntry(string const &cr, string const &f_charts, Bool_t is_set_ghost=false);
   void                      SetEntry(string const &crentry_line, TUAtomElements const &atom, Bool_t is_set_ghost=false);
   inline void               SetSSIsotopeAbundance(Double_t const &abund)          {fSSIsotopeAbundance=abund;}
   inline void               SetSSIsotopicFraction(Double_t const &abund)          {fSSIsotopicFraction=abund;}
   void                      SetType(string const &type);
   void                      TEST(FILE *f=stdout);

   ClassDef(TUCREntry, 1)  // CR entry: A, Z, m, name (case insensitive), ...
};

#endif
