// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

#ifndef USINE_TUInitParEntry
#define USINE_TUInitParEntry

// C/C++ include
#include <string>
using namespace std;
// ROOT include
#include <Rtypes.h>
#include <TRint.h>
// USINE include

class TUInitParEntry {

private:
   string                 fGroup;       // Group to which the entry belongs
   Bool_t                 fIsMultiVal;  // Multiple values (fVal.size()>1) for this entry allowed [true]
   string                 fName;        // Parameter name (case sensitive)
   string                 fSubGroup;    // Subgroup to which the entry belongs
   vector<string>         fVal;         //![Nval] Possible values for this entry (most of the time, only one val)

public:
   TUInitParEntry();
   TUInitParEntry(TUInitParEntry const &par);
   virtual ~TUInitParEntry();

   inline void            ClearVals()                                         {fVal.clear();}
   inline TUInitParEntry *Clone()                                             {return new TUInitParEntry(*this);}
   void                   Copy(TUInitParEntry const &par);
   inline TUInitParEntry *Create()                                            {return new TUInitParEntry;}
   inline void            AddVal(string const &val)                           {fVal.push_back(val);}
   inline string          GetGroup() const                                    {return fGroup;}
   inline string          GetName() const                                     {return fName;}
   inline Int_t           GetNVals() const                                    {return (Int_t)fVal.size();}
   inline string          GetSubGroup() const                                 {return fSubGroup;}
   inline string          GetVal(Int_t j_multi=0) const                       {return fVal[j_multi];}
   inline vector<string>  GetVals() const                                     {return fVal;}
   inline Bool_t          IsMultiVal() const                                  {return fIsMultiVal;}
   Bool_t                 IsSame(TUInitParEntry *par) const;
   Bool_t                 IsSame(string const &group, string const &subgroup, string const &name) const;
   void                   Print(FILE *f=stdout) const;
   void                   SetEntry(string const &line_entry, string const &info_id = "");
   inline void            SetVal(string const &val, Int_t j_multi=0)          {if (j_multi<(Int_t)fVal.size()) fVal[j_multi] = val; else fVal.push_back(val);}
   void                   TEST(FILE *f=stdout);

   ClassDef(TUInitParEntry, 1)  //Initialisation parameter: group, subgroup, name, value...
};
#endif
