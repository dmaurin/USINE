// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

#ifndef USINE_TUSrcTemplates
#define USINE_TUSrcTemplates

// C/C++ include
// ROOT include
// USINE include
#include "TUValsTXYZECr.h"

class TUSrcTemplates {

private:
   vector<Bool_t>             fIsSpectrumOrSpatDist; //![NTemplates] Is it a spectra or a spatial distrib?
   Int_t                      fNTemplates;           // Number of template functions
   TUValsTXYZEVirtual       **fTemplates;            //![NTemplates] Description (formula/values)

   void                       Initialise(Bool_t is_delete = false);

public:
   TUSrcTemplates();
   TUSrcTemplates(TUSrcTemplates const &src_templ);
   virtual ~TUSrcTemplates();

   inline TUSrcTemplates     *Clone()                                          {return new TUSrcTemplates(*this);}
   void                       Copy(TUSrcTemplates const &src_templ);
   inline TUSrcTemplates     *Create()                                         {return new TUSrcTemplates;}
   inline TUFreeParList      *GetFreePars(Int_t i_templ)                       {return fTemplates[i_templ]->GetFreePars();}
   inline Int_t               GetNTemplates() const                            {return fNTemplates;}
   inline TUValsTXYZEVirtual *GetSrc(Int_t i_templ) const                      {return fTemplates[i_templ];}
   inline TUValsTXYZEVirtual *GetSrc(string const &src_templ, Bool_t is_spectrum_or_spatdist) {return fTemplates[IndexSrcTemplate(src_templ, is_spectrum_or_spatdist)];}
   inline string              GetTemplateName(Int_t i_templ) const             {return fTemplates[i_templ]->GetName();}
   Int_t                      IndexSrcTemplate(string const &src_templ, Bool_t is_spectrum_or_spatdist) const;
   inline Int_t               IsSpectrumOrSpatDist(Int_t i_templ) const        {return fIsSpectrumOrSpatDist[i_templ];}
   void                       PrintSrcTemplate(FILE* f, Int_t i_templ) const;
   void                       PrintSrcTemplates(FILE* f=stdout) const;
   void                       SetClass(TUInitParList *init_pars, Bool_t is_verbose, FILE *f_log);
   void                       TEST(TUInitParList *init_pars, FILE *f=stdout);
   inline Int_t               ValsTXYZEFormat(Int_t i_templ) const             {return fTemplates[i_templ]->ValsTXYZEFormat();}

   ClassDef(TUSrcTemplates, 1)  // Templates for spatial distributions and spectra [grid or formula]
};

#endif
