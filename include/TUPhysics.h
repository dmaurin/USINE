// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

#ifndef USINE_TUPhysics
#define USINE_TUPhysics

#include "TUAxis.h"
#include "TUMath.h"

namespace TUPhysics {

   /* ------------------ */
   /* Conversion Factors */
   /* ------------------ */


   // mb to cm2 (i.e. 1mb = mb_to_cm2 * cm2)
   inline Double_t Convert_mb_to_cm2()                {return 1.e-27;}

   // Mass/Energy                                     (PDG 2016)
   inline Double_t Convert_amu_to_GeV()               {return 0.9314940954;}
   inline Double_t Convert_GeV_to_kg()                {return 1.782661907e-27;}
   inline Double_t Convert_erg_to_GeV()               {return 624.15091258832;}

   // Length                                          (PDG 2016)
   inline Double_t Convert_pc_to_m()                  {return 3.08567758149e16;} /*Exact*/
   inline Double_t Convert_AU_to_m()                  {return 1.49597870700e11;} /*Exact*/

   // Time                                            (PDG 2016)
   Double_t        Convert_to_Myr(string const &unit);
   inline Double_t Convert_yr_to_s()                  {return 3.15581498e7;} /*Sideral year*/

   // Speed
   inline Double_t Convert_kmpers_to_kpcperMyr()      {return (1.e6*Convert_yr_to_s()/Convert_pc_to_m());}
   inline Double_t Convert_cmpers_to_kpcperMyr()      {return (10.*Convert_yr_to_s()/Convert_pc_to_m());}
   inline Double_t Convert_AUperyr_to_mpers()         {return (Convert_AU_to_m()/Convert_yr_to_s());}

   // Pressure
   inline Double_t Convert_mbar_to_gpercm2()          {return 1.01971621298;}
   inline Double_t Convert_Pa_to_bar()                {return 1.e-5;}

   /* --------- */
   /* Constants */
   /* --------- */

   // Number of stable elements used
   inline Int_t    Zmax()                             {return 109;}
   // QED coupling [-]                                (PDG 2016)
   inline Double_t AlphaQED()                         {return 7.2973525664e-3;}
   // Avogadro number [/mol] (1 g = NAvogadro amu)    (PDG 2016)
   inline Double_t NAvogadro()                        {return 6.022140857e23;}
   // BohrRadius [cm] is r_e/alpha^2                  (PDG 2016)
   inline Double_t BohrRadius_cm()                    {return 0.52917721067e-8;}
   // e- radius [cm] is e^2/(4*pi*epsilon_o*m_e*c^2)  (PDG 2016)
   inline Double_t ElectronRadius_cm()                {return 2.8179403227e-13;}
   // Planck constant [GeV.s]                         (PDG 2016)
   inline Double_t hbar_GeVs()                        {return 6.582119514e-25;}
   // Boltzmann constant [GeV/K]                      (PDG 2016)
   inline Double_t kB_GeVperK()                       {return 8.6173303e-14;}
   // Rydberg constant [eV] is h*c*Rin= m_e*c^2*alpha_struct_fine^2/2 (PDG 2016)
   inline Double_t Rydberg_eV()                       {return 13.605693009;}
   // sig_Thomson [barn] is 8*Pi* r_e^2/3             (PDG 2016)
   inline Double_t SigmaThomson_barn()                {return 0.66524587158;}

   // Speed of Light                                  (PDG 2016)
   inline Double_t C_cmpers()                         {return 2.99792458e10;} /*Exact*/
   inline Double_t C_mpers()                          {return 2.99792458e8;}  /*Exact*/
   inline Double_t C_cmperMyr()                       {return (C_cmpers()*Convert_yr_to_s()*1.e6);}

   // Energy density of black-body
   inline Double_t URadBlackBody_GeVcm3(Double_t const &T0_K) {return (8.*TUMath::Pi()*pow(TUMath::Pi()*kB_GeVperK()*T0_K,4.) / (15.*pow(hbar_GeVs()*2.*TUMath::Pi()*C_cmpers(),3.)));}

   // Electron mass [GeV]                             (PDG 2016)
   inline Double_t me_GeV()                           {return 0.5109989461e-3;}
   // Mass of pi0 [GeV]                               (PDG 2016)
   inline Double_t mPi0_GeV()                         {return 0.1349766;}
   // Mass of \pi^+ and \pi^- [GeV]                   (PDG 2016)
   inline Double_t mPiCharged_GeV()                   {return 0.13957018;}
   // Proton mass [GeV]                               (PDG 2016)
   inline Double_t mp_GeV()                           {return 0.9382720813;}
   // Deuteron mass [GeV]                             (PDG 2016)
   inline Double_t md_GeV()                           {return 1.875612928;}
   // 3He mass (GeV)
   inline Double_t m3He_GeV()                         {return 2.8094132;}
   // 4He mass (GeV)     [PDG 2016]
   inline Double_t m4He_GeV()                         {return 3.728401;}
   // mass of Hydrogen in amu [PDG 2016]
   inline Double_t mH_amu()                           {return 1.007276466879;}
   // mass of Helium in amu
   inline Double_t mHe_amu()                          {return 4.00260325;}

   Double_t        AltitudeToPressure_mbar(Double_t const &h_m);
   inline Double_t AltitudeToGrammage_gpercm2(Double_t const &h_m) {return (AltitudeToPressure_mbar(h_m) * Convert_mbar_to_gpercm2());}



   /* ------------------------ */
   /* Trivia for E, p, beta... */
   /* ------------------------ */
   //
   //  - m is the mass (in amu or in GeV)
   //  - E (total energy), Ek (kinetic energy) and p (momentum) are in GeV
   //  - Ekn (kinetic energy per nucleon) is in GeV/n
   //  - R (rigidity) is in GV

   // beta and gamma Lorentz
   Double_t Beta_pE(Double_t const &p_gev, Double_t const &e_gev);
   Double_t Beta_mEk(Double_t const &m_gev, Double_t const &ek_gev);
   Double_t Beta_mAEkn(Double_t const &m_gev, Int_t a_cr, Double_t const &ekn_gevn);

   Double_t Gamma_Em(Double_t const &e_gev, Double_t const &m_gev);
   Double_t Gamma_mAZR(Double_t const &m_gev, Int_t a_cr, Int_t z_cr, Double_t const &r_gv);
   Double_t Gamma_mAEkn(Double_t const &m_gev, Int_t a_cr, Double_t const &ekn_gevn);

   Double_t Vcms_mAEkn(Double_t const &m_gev, Int_t a_cr, Double_t const &ekn_gevn);
   Double_t VkpcMyr_mAEkn(Double_t const &m_gev, Int_t a_cr, Double_t const &ekn_gevn);
   Double_t Vms_mAEkn(Double_t const &m_gev, Int_t a_cr, Double_t const &ekn_gevn);

   // Formulae to go from (and to) E to Ek, Ekn, p, R, etc.
   Double_t Ekn_to_E(Double_t const &ekn_gevn, Int_t a_cr, Double_t const &m_gev);
   Double_t Ekn_to_Ek(Double_t const &ekn_gevn, Int_t a_cr);
   Double_t Ekn_to_p(Double_t const &ekn_gevn, Int_t a_cr, Double_t const &m_gev);
   Double_t Ekn_to_R(Double_t const &ekn_gevn, Int_t a_cr, Double_t const &m_gev, Int_t z_cr);

   Double_t Ek_to_E(Double_t const &ek_gev, Double_t const &m_gev);
   Double_t Ek_to_Ekn(Double_t const &ek_gev, Int_t a_cr);
   Double_t Ek_to_p(Double_t const &ek_gev, Double_t const &m_gev);
   Double_t Ek_to_R(Double_t const &ek_gev, Double_t const &m_gev, Int_t z_cr);

   Double_t E_to_Ek(Double_t const &e_gev, Double_t const &m_gev);
   Double_t E_to_Ekn(Double_t const &e_gev, Int_t a_cr,Double_t const &m_gev);
   Double_t E_to_p(Double_t const &e_gev, Double_t const &m_gev);
   Double_t E_to_R(Double_t const &e_gev, Double_t const &m_gev, Int_t z_cr);

   Double_t p_to_Ek(Double_t const &p_gev, Double_t const &m_gev);
   Double_t p_to_Ekn(Double_t const &p_gev, Int_t a_cr, Double_t const &m_gev);
   Double_t p_to_E(Double_t const &p_gev, Double_t const &m_gev);
   Double_t p_to_R(Double_t const &p_gev, Int_t z_cr);

   Double_t R_to_Ek(Double_t const &r_gv, Double_t const &m_gev, Int_t z_cr);
   Double_t R_to_Ekn(Double_t const &r_gv, Int_t a_cr, Double_t const &m_gev, Int_t z_cr);
   Double_t R_to_E(Double_t const &r_gv, Double_t const &m_gev, Int_t z_cr);
   Double_t R_to_p(Double_t const &r_gv, Int_t z_cr);

   Double_t ConvertE(Double_t const &e_in, gENUM_ETYPE etype_in, gENUM_ETYPE etype_out, Int_t a_cr, Int_t z_cr, Double_t const &m_gev);
   Double_t dNdEin_to_dNdEout(Double_t const &e_in, gENUM_ETYPE etype_in, gENUM_ETYPE etype_out, Int_t a_cr, Int_t z_cr, Double_t const &m_gev);
   Double_t dEkndR_Ekn(Double_t const &ekn_gevn, Int_t a_cr, Int_t z_cr, Double_t const &m_gev);
   void     dNdEkn_to_dNdR(TUAxis *ekn_axis, Double_t *dndekn, TUAxis *r_axis, vector<Double_t> &dndr, Int_t a_cr, Int_t z_cr, Double_t const &m_gev);
   void     dNdR_to_dNdEkn(TUAxis *r_axis, Double_t *dndr, TUAxis *ekn_axis, vector<Double_t> &dndekn, Int_t a_cr, Int_t z_cr, Double_t const &m_gev);
   Double_t dRdEkn_R(Double_t const &r_gv, Int_t a_cr, Int_t z_cr, Double_t const &m_gev);

   void     TEST(FILE *f=stdout);
}
#endif
