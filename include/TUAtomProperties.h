// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

#ifndef USINE_TUAtomProperties
#define USINE_TUAtomProperties

// C/C++ include
// ROOT include
// USINE include
#include "TUAtomElements.h"
#include "TUInitParList.h"

class TUAtomProperties: public TUAtomElements {

private:
   vector<Double_t>         fAMeanEarthWeighted; //![NElements] <A> [amu] from Earth's isotopic abundances
   vector<Double_t>         fEionKshell;         //![NElements] K-shell ionisation energy
   string                   fFileName;           // File name (on disk) of loaded atomic data
   vector<Double_t>         fFIP;                //![NElements] First Ionisation Potential (atomic elements)
   vector<Double_t>         fIMean;              //![NElements] Mean excitation energy <I>
   vector<Double_t>         fTcVolatility;       //![NElements] Tc [K] for which 50% of element is in gas form

public:
   TUAtomProperties();
   TUAtomProperties(TUAtomProperties const &atom_prop);
   TUAtomProperties(TUInitParList *init_pars, Bool_t is_verbose, FILE *f_log);
   virtual ~TUAtomProperties();

   inline TUAtomProperties *Clone()                                      {return new TUAtomProperties(*this);}
   void                     Copy(TUAtomProperties const &atom_prop);
   inline TUAtomProperties *Create()                                     {return new TUAtomProperties;}
   static Double_t          FIPBias(Double_t const &fip);
   Double_t                 FIPBias(Int_t z) const;
   inline Double_t          GetAMeanEarthWeighted(Int_t z) const         {TUAtomElements::CheckZ(z); return fAMeanEarthWeighted[z - 1];}
   inline Double_t          GetEionKshell(Int_t z) const                 {TUAtomElements::CheckZ(z); return fEionKshell[z - 1];}
   inline string            GetFileName() const                          {return fFileName;}
   inline Double_t          GetFIP(Int_t z) const                        {TUAtomElements::CheckZ(z); return fFIP[z - 1];}
   inline Double_t          GetIMean(Int_t z) const                      {TUAtomElements::CheckZ(z); return fIMean[z - 1];}
   inline Double_t          GetTcVolatility(Int_t z) const               {TUAtomElements::CheckZ(z); return fTcVolatility[z - 1];}
   void                     Initialise(void);
   void                     Print(FILE *f=stdout, Int_t z_min=1, Int_t z_max=1000) const;
   void                     SetClass(string const &f_atomprop, FILE *f_log);
   void                     SetClass(TUInitParList *init_pars, Bool_t is_verbose, FILE *f_log);
   inline void              SetFileName(string &file)                    {fFileName = file;}
   void                     TEST(TUInitParList *init_pars, FILE *f=stdout);

   ClassDef(TUAtomProperties, 1)  // Atomic properties for TUAtomElements [loaded from file]
};

#endif
