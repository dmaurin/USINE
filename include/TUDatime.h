// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

#ifndef USINE_TUDatime
#define USINE_TUDatime

// C/C++ include
#include <algorithm>
#include <cstdlib>
#include <ctime>
// ROOT include
#include <TRint.h>
#include <TTimeStamp.h>
// USINE include
#include "TUMessages.h"

class TUDatime {
private:
   string fStart;          // Start: format YYYY-MM-DD HH:MM:SS (e.g., 2009-10-25 23:05:02)
   string fStop;           // Stop: format YYYY-MM-DD HH:MM:SS (e.g., 2009-10-25 23:05:02)

   void                    Set(string const &datime, Bool_t is_start=true);
   void                    Set(string const &date, string const &time, Bool_t is_start=true);
   void                    Set(struct tm const &raw, Bool_t is_start=true);
   void                    Set(time_t const &raw, Bool_t is_start, Bool_t raw_utc_or_local = true);

public:
   TUDatime();
   TUDatime(TUDatime const &datime);
   virtual ~TUDatime() { }

   inline Bool_t operator() (TUDatime const &a, TUDatime const &b) const {
      //--- Operator (to use, e.g., in sort()) to time-order two datimes,
      //   according to their start dates (or stop dates is same start dates).
      if (a.GetStart() == b.GetStart()) {
         if (a.GetStop() != "" && b.GetStop() != "")
            return IsStartBeforeStop(a.GetStop(), b.GetStop());
         else
            return false;
      } else {
         return IsStartBeforeStop(a.GetStart(), b.GetStart());
      }
   }

   void                    AddMonth(Bool_t is_start=true);
   void                    AddSec(ULong_t const &dt, Bool_t is_start=true);
   string inline           AsString(void) const                      {return fStart;}
   static Bool_t           CheckDateFormat(string const &date, Bool_t std_or_usine, Bool_t is_abort);
   static Bool_t           CheckDatimeFormat(string const &datime, Bool_t std_or_usine, Bool_t is_abort);
   inline TUDatime        *Clone()                                   {return new TUDatime(*this);}
   static vector<TUDatime> CommonDatimes(vector<vector<TUDatime> > const &datimes);
   static vector<TUDatime> CommonDatimes(vector<TUDatime> const &datimes1, vector<TUDatime> const &datimes2);
   static void             ConvertUSINE2STD(string &datime);
   static void             ConvertSTD2USINE(string &datime);
   void                    Copy(TUDatime const &datime);
   inline TUDatime        *Create()                                  {return new TUDatime;}
   static Int_t            DaysInMonth(Int_t month, Int_t year);
   static Int_t            DaysInYear(Int_t year);
   inline string           GetDate(Bool_t is_start=true) const       {return ((is_start) ? fStart.substr(0,10) : fStop.substr(0,10));}
   time_t                  GetDatime_t(Bool_t is_start=true) const;
   struct tm               GetDatime_tm(Bool_t is_start=true) const;
   inline Int_t            GetDay(Bool_t is_start=true) const        {return ((is_start) ? atoi((fStart.substr(8,2)).c_str()) : atoi((fStop.substr(8,2)).c_str()));}
   inline Int_t            GetHour(Bool_t is_start=true) const       {return ((is_start) ? atoi((fStart.substr(11,2)).c_str()) : atoi((fStop.substr(11,2)).c_str()));}
   inline Int_t            GetMinute(Bool_t is_start=true) const     {return ((is_start) ? atoi((fStart.substr(14,2)).c_str()) : atoi((fStop.substr(14,2)).c_str()));}
   inline Int_t            GetMonth(Bool_t is_start=true) const      {return ((is_start) ? atoi((fStart.substr(5,2)).c_str()) : atoi((fStop.substr(5,2)).c_str()));}
   inline Int_t            GetSecond(Bool_t is_start=true) const     {return ((is_start) ? atoi((fStart.substr(17,2)).c_str()) : atoi((fStop.substr(17,2)).c_str()));}
   inline string           GetStart() const                          {return fStart;}
   inline string           GetStop() const                           {return fStop;}
   string                  GetStartStop(string const &separator, Int_t datime0_date1_time2) const;
   inline string           GetTime(Bool_t is_start=true) const       {return ((is_start) ? fStart.substr(11,8) : fStop.substr(11,8));}
   inline Int_t            GetYear(Bool_t is_start=true) const       {return ((is_start) ? atoi((fStart.substr(0,4)).c_str()) : atoi((fStop.substr(0,4)).c_str()));}
   Int_t                   IndexClosest(vector<TUDatime> &datimes, TUDatime &datime) const;
   inline void             Initialise()                              {fStart=""; fStop="";}
   inline ULong_t          IntervalSec() const                       {TUDatime tmp1, tmp2; tmp1.Set(GetStart()); tmp2.Set(GetStop()); return IntervalSec(tmp1, tmp2);}
   ULong_t                 IntervalSec(TUDatime const &datime1, TUDatime const &datime2) const;
   static Bool_t           IntervalsCheckTimeOrderedNonOverlapping(vector<TUDatime> const &intervals, Bool_t is_abort, FILE *f=stdout);
   static void             IntervalsExtract(TUDatime const &user_datime, vector<TUDatime> &intervals, vector<Int_t> &indices_new, FILE *f=stdout);
   void                    IntervalsMerge(vector<TUDatime> const &intervals, vector<TUDatime> &merged) const;
   inline void             IntervalsSort(vector<TUDatime> &intervals) const {sort(intervals.begin(), intervals.end(), *this);}
   static Bool_t           IsLeapYear(Int_t year);
   inline Bool_t           IsStartBeforeStop() const                 {return TUDatime::IsStartBeforeStop(GetStart(), GetStop());}
   static Bool_t           IsStartBeforeStop(string datime_start, string datime_stop);
   inline Bool_t           IsStartEqualStop() const                  {return ((fStart==fStop) ? true : false);}
   inline void             PrintDatime(FILE *f=stdout, Bool_t is_start=true) const {(is_start) ? fprintf(f, "         - Tevent = %s\n", fStart.c_str()) : fprintf(f, "         - Tevent = %s\n", fStop.c_str());}
   inline void             PrintInterval(FILE *f=stdout) const       {fprintf(f, "         - [Tstart | TStop] = [%s | %s]\n", fStart.c_str(), fStop.c_str());}
   ULong_t                 SecondsFromStartOfYear( Bool_t is_start=true) const;
   static inline ULong_t   SecondsInYear(Int_t year)                 {return DaysInYear(year)*86400;}
   inline ULong_t          SecondsTillEndOfYear(Bool_t is_start=true) const {return SecondsInYear(GetYear()) - SecondsFromStartOfYear(is_start);}
   inline void             SetStart(string const &datime_start)      {Set(datime_start, true);}
   inline void             SetStartStop(string const &datime_start, string const &datime_stop) {Set(datime_start, true); Set(datime_stop, false);}
   inline void             SetStop(string const &datime_stop)        {Set(datime_stop, false);}
   void                    SubtractMonth(Bool_t is_start=true);
   void                    TEST(FILE *f=stdout);

   ClassDef(TUDatime,1)  // Entry date (YYYY-MM-DD) and time (HH:MM:SS)
};

#endif
