// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

#ifndef USINE_TUMisc
#define USINE_TUMisc

// C/C++ include
#include <string>
#include <vector>
using namespace std;
// ROOT include
#include <TCanvas.h>
#include <TGraph.h>
#include <TH1D.h>
#include <TLegend.h>
#include <TMultiGraph.h>
#include <TText.h>
// USINE include

namespace TUMisc {


   vector<Double_t>       ApplyBias(vector<Double_t> &x_orig, vector<Double_t> &y_orig, vector<Double_t> &x_bias, vector<Double_t> &y_bias);
   vector<Double_t>       Array2Vector(Double_t const *ptr, Int_t n);
   string                 CreateDir(string const &dir, string const &class_name, string const &method);
   vector<string>         ExtractFromDir(string const &pattern, Bool_t is_verbose, FILE *f, Bool_t is_selection, string const &exclude_pattern = "", Bool_t is_test=false);
   Int_t                  IndexInList(vector<Int_t> const &list, Int_t val_to_find);
   Int_t                  IndexInList(vector<string> const &list, string const &nametofind, Bool_t is_comp_uppercase);
   Bool_t                 IsFileExist(ifstream &f, string const &f_name, Bool_t is_abort=true);
   inline Bool_t          IsInList(vector<Int_t> const &list, Int_t i_tofind)                 {return (IndexInList(list, i_tofind) < 0 ? false : true);}
   inline Bool_t          IsInList(vector<string> &list, string &tofind, Bool_t is_uppercase) {return (IndexInList(list, tofind, is_uppercase) < 0 ? false : true);}
   inline Bool_t          IsRatio(string const &qty)                                          {return (qty.find("/") == string::npos) ? false : true;}
   Bool_t                 IsYaxisLog(string const &combo);
   string                 FormatCRQty(string const &qty, Int_t switch_format/*0=usine-name,1=text,2=root-plot*/);
   void                   FormatCRQtyName(string &qty);
   string                 FormatEPower(Double_t const &e_power);
   string                 GetDatime();
   string                 GetPath(string const &str);
   string                 List2String(vector<Double_t> const &list, char delimiter, Int_t n_digits);
   string                 List2String(vector<Int_t> const &list, char delimiter);
   string                 List2String(vector<string> const &list, char delimiter);
   inline void            LowerCase(char& c)                                                  {if ('A' <= c && c <= 'Z') c = c + 'a' - 'A';}
   inline void            LowerCase(string& s)                                                {for (Int_t i = s.length() - 1; i >= 0; --i) LowerCase(s[i]);}
   inline Int_t           Number2Digits(Int_t n)                                              {Int_t n_digits = 0; while (n != 0) {n /= 10; ++n_digits;} return n_digits;}
   void                   OptimiseRange(Double_t *array, Double_t const &threshold, Int_t n, Int_t &k_min, Int_t &k_max, Bool_t &is_force_adapt);
   void                   OptimiseRange(Double_t *array, Double_t *arraybis, Double_t const &threshold, Int_t n, Int_t &k_min, Int_t &k_max, Bool_t &is_force_adapt);
   TText                 *OrphanUSINEAdOnPlots();
   void                   Print(FILE *f, TGraph *gr, Int_t opt_0xy_1err_2asymerr);
   void                   Print(FILE *f, TMultiGraph *mg, Int_t opt_0xy_1err_2asymerr);
   inline void            PrintHisto(FILE *f, TH1D *h, Int_t opt_0xy_1err_2asymerr)           {TGraph gr_tmp(h); Print(f, &gr_tmp, opt_0xy_1err_2asymerr);}
   void                   PrintUSINEHeader(FILE *f, Bool_t is_print_datime);
   string                 RemoveBlancksFromStartStop(string const&str);
   string                 RemoveChars(string const &str,string const &delimiters);
   void                   RemoveDuplicates(vector<Int_t> &list);
   void                   RemoveDuplicates(vector<string> &list);
   void                   RemoveDuplicates(vector<Double_t> &list, Double_t const &thresh);
   void                   RemoveSpecialChars(string& s);
   Int_t                  RootColor(Int_t i);
   Int_t                  RootFill(Int_t i);
   Int_t                  RootMarker(Int_t i);
   void                   RootSave(TCanvas* c, string const &dir, string const &file_name, string const &opt);
   inline void            RootSave(TCanvas* c, string const&file_name, string const &opt)     {RootSave(c, ".", file_name, opt);}
   void                   RootSetStylePlots();
   Bool_t                 String2Bool(string const &str);
   void                   String2List(string const &str, string const &delimiters, vector<Double_t>& list);
   void                   String2List(string const &str, string const &delimiters, vector<Int_t>& list);
   void                   String2List(string const &str, string const &delimiters, vector<string>& list, Int_t parse_n_first=-1);
   void                   SubstituteEnvInPath(string &path, string const &env="$USINE");
   string                 SystemExec(const char* cmd);
   void                   TEST(FILE *f=stdout);
   inline void            UpperCase(char& c)                                                  {if ('a' <= c && c <= 'z') c = c - 'a' + 'A';}
   inline void            UpperCase(string& s)                                                {for (Int_t i = s.length() - 1; i >= 0; --i) UpperCase(s[i]);}
}

#endif
