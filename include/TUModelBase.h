// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

#ifndef USINE_TUModelBase
#define USINE_TUModelBase

// C/C++ include
// ROOT include
// USINE include
#include "TUDataSet.h"
#include "TUMediumEntry.h"
#include "TUNormList.h"
#include "TUPropagSwitches.h"
#include "TURunOutputs.h"
#include "TUSolModVirtual.h"
#include "TUSrcMultiCRs.h"
#include "TUTransport.h"
#include "TUXSections.h"

class TUModelBase : public TUAxesCrE, public TUPropagSwitches, public TUDataSet, public TUNormList,
                    public TUSrcTemplates, public TUXSections, public TUFreeParList {

private:
   TUAxesTXYZ                *fAxesTXYZ;              // Model geometry
   gENUM_BCTYPE               fBCTypeAntinucLE;       // Boundary condition type for ANTINUC at low E
   gENUM_BCTYPE               fBCTypeAntinucHE;       // Boundary condition type for ANTINUC at high E
   gENUM_BCTYPE               fBCTypeLeptonLE;        // Boundary condition type for LEPTON at low E
   gENUM_BCTYPE               fBCTypeLeptonHE;        // Boundary condition type for LEPTON at high E
   gENUM_BCTYPE               fBCTypeNucLE;           // Boundary condition type for NUC at low E
   gENUM_BCTYPE               fBCTypeNucHE;           // Boundary condition type for NUC at high E
   TUCoordE                  *fCoordE;                // Generic E coordinate (bin and/or E values)
   TUCoordTXYZ               *fCoordsTXYZ;            // Generic TXYZ coordinate (bins and/or values)
   Double_t                   fEpsIntegr;             // Relative precision sought for integrations (e.g., XS)
   Double_t                   fEpsIterConv;           // Relative precision sought for iterative procedure (e.g., tertiaries)
   Double_t                   fEpsNormData;           // Relative precision sought for normalisation to data
   TUInitParList             *fInitPars;              // Initialisation parameters
   Bool_t                     fIsDeleteSolMod;        // Whether or not fSolMod is deleted in destructor
   TUMediumTXYZ              *fISM;                   // Interstellar medium
   Bool_t                     fIsPropagated;          // Whether or not propagation is already done
   gENUM_PROPAG_MODEL         fModel;                 // Model loaded (see TUEnum.h)
   Int_t                      fNSrcAstro;             // Number of astrophysical sources
   Int_t                      fNSrcDM;                // Number of DM sources
   TUCoordTXYZ               *fSolarSystemCoords;     // Solar system coordinates for the model
   TUValsTXYZECr             *fSolarSystemFluxesPrim; // Local primary IS fluxes (all CRs & energies)
   TUValsTXYZECr             *fSolarSystemFluxesSec;  // Local secondary IS fluxes (all CRs & energies)
   TUValsTXYZECr             *fSolarSystemFluxesTot;  // Local total IS Fluxes (all CRs & energies)
   TUSolModVirtual           *fSolMod;                // Solar modulation models to use
   TUSrcMultiCRs            **fSrcAstro;              //![NSrcAstro] List of astrophysical sources
   TUSrcMultiCRs            **fSrcDM;                 //![NSrcDM] List of DM sources
   TUTransport               *fTransport;             // Transport parameters (diffusion, convection...)

   virtual Bool_t             CalculateChargedCRs(Int_t jcr_start, Int_t jcr_stop, Bool_t is_verbose, FILE *f_log);
   void                       CalculateChargedCRs_NormalisedToData(Int_t jcr_start, Int_t jcr_stop, Bool_t is_verbose, FILE *f_log);
   Double_t                   CalculateScaleFactor(Int_t i_norm, Bool_t is_first_attempt, Bool_t is_verbose, FILE *f_log) const;
   Double_t                   EvalFlux(Int_t j_cr, Int_t k_ekn, TUCoordTXYZ *coord_txyz, Int_t tot0_prim1_sec2);
   void                       FillEFirstSecondOrderTerms(Int_t j_cr, TUMediumEntry *ism_entry, vector<Double_t> &b_startm1, vector<Double_t> &c_startmhalf, TUCoordTXYZ *coord_txyz=NULL);
   void                       FillModelFreePars(Bool_t is_verbose, FILE *f_log);
   gENUM_BCTYPE               GetBCType(Int_t j_cr, Bool_t is_le_or_he) const;
   Double_t                   NormaliseSrcAbundToCRDataUsingNormList(Int_t i_attempt, Bool_t is_verbose, FILE *f_log);
   void                       SetClass_ModelBaseDepMembers(TUInitParList* init_pars, string const &propag_model, Bool_t is_verbose, FILE *f_log);
   void                       SetClass_ModelBaseIndepMembers(TUInitParList* init_pars, Bool_t is_verbose, FILE *f_log);
   virtual Bool_t             SetClass_ModelSpecific(TUInitParList *init_pars, Bool_t is_verbose, FILE *f_log);


protected:
   inline virtual Double_t    AdiabaticLosses(Int_t j_cr, Double_t const &ek, TUCoordTXYZ *coord_txyz=NULL)      {return 0.;}
   inline virtual Double_t    CoeffReac1stOrder(Int_t j_cr, Int_t k_ekn)                                         {return 0.;}
   inline virtual Double_t    CoeffReac2ndOrderLowerHalfBin(Int_t j_cr, Int_t k_ekn)                             {return 0.;}
   virtual Double_t           EvalFluxPrim(Int_t j_cr, Int_t k_ekn, TUCoordTXYZ *coord_txyz);
   inline Double_t            EvalFluxSec(Int_t j_cr, Int_t k_ekn, TUCoordTXYZ *coord_txyz=NULL)                 {return EvalFlux(j_cr, k_ekn, coord_txyz, 2);}
   virtual Double_t           EvalFluxTot(Int_t j_cr, Int_t k_ekn, TUCoordTXYZ *coord_txyz);
   Double_t                   EvalFluxCombo(string const &combo, Int_t k_ekn, TUCoordTXYZ *coord_txyz, Int_t tot0_prim1_sec2, Bool_t is_verbose);
   Double_t                   EvalFraction(Int_t j_cr, Int_t k_ekn, TUCoordTXYZ *coord_txyz, Int_t isotopic0_prim1_sec2);
   Double_t                   EvalFractionElement(Int_t z_element, Int_t k_ekn, TUCoordTXYZ *coord_txyz, Int_t prim1_sec2);
   Bool_t                     GetFreeParsStatusHalfLives() const;
   Bool_t                     GetFreeParsStatusISM() const;
   Bool_t                     GetFreeParsStatusSolMod() const;
   inline Double_t            ValueSrcAstroPosition(Int_t s_src, Int_t j_cr, Int_t i_coord/*0,1,2*/, Double_t const &t_myr) {return ((IsExistSrc(s_src, j_cr, true)) ? fSrcAstro[s_src]->ValueSrcPosition(fSrcAstro[s_src]->IndexCRInSrcSpecies(j_cr), i_coord, t_myr) : 0.);}
   inline Double_t            ValueSrcAstroSpatialDistrib(Int_t s_src, Int_t j_cr, TUCoordTXYZ *coord_txyz = NULL){return ((IsExistSrc(s_src, j_cr, true)) ? fSrcAstro[s_src]->ValueSrcSpatialDistrib(fSrcAstro[s_src]->IndexCRInSrcSpecies(j_cr), coord_txyz) : 0.);}
   inline Double_t            ValueSrcAstroSpectra(Int_t j_cr, TUCoordE *coord_e, TUCoordTXYZ *coord_txyz = NULL) {Double_t res = 0.; for (Int_t s=0; s<GetNSrcAstro(); ++s) res+= ValueSrcAstroSpectrum(s, j_cr, coord_e, coord_txyz); return res;}
   inline Double_t            ValueSrcAstroSpectrum(Int_t s_src, Int_t j_cr, TUCoordE *coord_e, TUCoordTXYZ *coord_txyz = NULL) {return ((IsExistSrc(s_src, j_cr, true)) ? fSrcAstro[s_src]->ValueSrcSpectrum(fSrcAstro[s_src]->IndexCRInSrcSpecies(j_cr), coord_e, coord_txyz) : 0.);}
   inline Double_t            ValueSrcDMPosition(Int_t s_src, Int_t j_cr, Int_t i_coord/*0,1,2*/, Double_t const &t_myr) {return ((IsExistSrc(s_src, j_cr, false)) ? fSrcDM[s_src]->ValueSrcPosition(fSrcAstro[s_src]->IndexCRInSrcSpecies(j_cr), i_coord, t_myr) : 0.);}
   inline Double_t            ValueSrcDMSpatialDistrib(Int_t s_src, Int_t j_cr, TUCoordTXYZ *coord_txyz = NULL)   {return ((IsExistSrc(s_src, j_cr, false)) ? fSrcDM[s_src]->ValueSrcSpatialDistrib(fSrcAstro[s_src]->IndexCRInSrcSpecies(j_cr), coord_txyz) : 0.);}
   inline Double_t            ValueSrcDMSpectra(Int_t j_cr, TUCoordE *coord_e, TUCoordTXYZ *coord_txyz = NULL)    {Double_t res = 0.; for (Int_t s=0; s<GetNSrcDM(); ++s) res+= ValueSrcDMSpectrum(s, j_cr, coord_e, coord_txyz); return res;}
   inline Double_t            ValueSrcDMSpectrum(Int_t s_src, Int_t j_cr, TUCoordE *coord_e, TUCoordTXYZ *coord_txyz = NULL) {return ((IsExistSrc(s_src, j_cr, false)) ? fSrcDM[s_src]->ValueSrcSpectrum(fSrcAstro[s_src]->IndexCRInSrcSpecies(j_cr), coord_e, coord_txyz) : 0.);}
   inline Double_t            ValueK(TUCoordE *coord_e, TUCoordTXYZ *coord_txyz=NULL, Int_t row=0, Int_t col=0)   {return fTransport->ValueK(coord_e, coord_txyz, row, col);}
   inline Double_t            ValueKpp(TUCoordE *coord_e, TUCoordTXYZ *coord_txyz=NULL)                           {return fTransport->ValueKpp(coord_e, coord_txyz);}
   inline Double_t            ValueVA(TUCoordTXYZ *coord_txyz=NULL)                                               {return fTransport->ValueVA(coord_txyz);}
   inline Double_t            ValueWind(TUCoordTXYZ *coord_txyz=NULL, Int_t coord=0)                              {return (TUPropagSwitches::IsWind() ? fTransport->ValueWind(coord_txyz, coord) : 0.);}
   void                       UpdateCoordE(Int_t j_cr, Int_t k_ekn, Int_t halfbin_status=0);
   inline void                UpdateCoordE(Int_t j_cr, Double_t ekn_gevn)                                         {vector<Double_t> evars_vals; TUAxesCrE::EvalEVarsVals(j_cr, ekn_gevn, evars_vals); fCoordE->SetValsE(&evars_vals[0], TUAxesCrE::GetNEVars(), 0, false);}


public:
   TUModelBase();
   virtual ~TUModelBase();

   void                       FillFlux(vector<Double_t> &flux, Int_t j_cr, TUCoordTXYZ *coord_txyz, Int_t tot0_prim1_sec2);
   void                       FillFluxCombo(vector<Double_t> &flux, string const &combo, TUCoordTXYZ *coord_txyz, Int_t tot0_prim1_sec2, Bool_t is_verbose);
   void                       FillFraction(vector<Double_t> &fraction, Int_t j_cr, TUCoordTXYZ *coord_txyz, Int_t isotopic0_prim1_sec2);
   void                       FillFractionElement(vector<Double_t> &fraction, Int_t z_element, TUCoordTXYZ *coord_txyz, Int_t prim1_sec2);
   void                       FillModelParameters(TURunOutputs &outputs) const;
   void                       FillSolarSystemFluxes();
   inline TUAxesTXYZ         *GetAxesTXYZ() const                                                                {return fAxesTXYZ;}
   inline gENUM_BCTYPE        GetBCTypeAntinucLE() const                                                         {return fBCTypeAntinucLE;}
   inline gENUM_BCTYPE        GetBCTypeAntinucHE() const                                                         {return fBCTypeAntinucHE;}
   inline gENUM_BCTYPE        GetBCTypeLeptonLE() const                                                          {return fBCTypeLeptonLE;}
   inline gENUM_BCTYPE        GetBCTypeLeptonHE() const                                                          {return fBCTypeLeptonHE;}
   inline gENUM_BCTYPE        GetBCTypeNucLE() const                                                             {return fBCTypeNucLE;}
   inline gENUM_BCTYPE        GetBCTypeNucHE() const                                                             {return fBCTypeNucHE;}
   inline gENUM_BCTYPE        GetBCTypeHE(Int_t j_cr) const                                                      {return GetBCType(j_cr, false);}
   inline gENUM_BCTYPE        GetBCTypeLE(Int_t j_cr) const                                                      {return GetBCType(j_cr, true);}
   inline TUCoordE           *GetCoordE()                                                                        {return fCoordE;}
   inline TUCoordTXYZ        *GetCoordsTXYZ() const                                                              {return fCoordsTXYZ;}
   inline Double_t            GetEpsIntegr() const                                                               {return fEpsIntegr;}
   inline Double_t            GetEpsIterConv() const                                                             {return fEpsIterConv;}
   inline Double_t            GetEpsNormData() const                                                             {return fEpsNormData;}
   Bool_t                     GetFreeParsStatusSrcAstro(Int_t s_src) const;
   Bool_t                     GetFreeParsStatusSrcDM(Int_t s_src) const;
   Bool_t                     GetFreeParsStatusTransport() const;
   Bool_t                     GetFreeParsStatusXSections() const;
   inline TUMediumTXYZ       *GetISM()                                                                           {return fISM;}
   inline gENUM_PROPAG_MODEL  GetModel() const                                                                   {return fModel;}
   inline string              GetModelName() const                                                               {return ((fModel!=kBASE) ? TUEnum::Enum2Name(GetModel()) : "Base");}
   inline Int_t               GetNCRs() const                                                                    {return TUAxesCrE::GetNCRs();}
   inline Int_t               GetNE() const                                                                      {return TUAxesCrE::GetNE();}
   inline Int_t               GetNSrcAstro() const                                                               {return fNSrcAstro;}
   inline Int_t               GetNSrcDM() const                                                                  {return fNSrcDM;}
   inline TUCoordTXYZ        *GetSolarSystemCoords() const                                                       {return fSolarSystemCoords;}
   inline TUValsTXYZECr      *GetSolarSystemFluxesPrim()                                                         {return fSolarSystemFluxesPrim;}
   inline TUValsTXYZECr      *GetSolarSystemFluxesSec()                                                          {return fSolarSystemFluxesSec;}
   inline TUValsTXYZECr      *GetSolarSystemFluxesTot()                                                          {return fSolarSystemFluxesTot;}
   inline TUSolModVirtual    *GetSolMod()                                                                        {return fSolMod;}
   inline TUSrcMultiCRs      *GetSrcAstro(Int_t s_src)                                                           {return fSrcAstro[s_src];}
   inline TUSrcMultiCRs      *GetSrcDM(Int_t s_src)                                                              {return fSrcDM[s_src];}
   inline TUTransport        *GetTransport()                                                                     {return fTransport;}
   void                       Initialise(Bool_t is_delete);
   virtual void               InitialiseForCalculation(Bool_t is_init_or_update, Bool_t is_force_update, Bool_t is_verbose, FILE *f_log);
   void                       InvertForELossGain(Int_t j_cr, TUMediumEntry *ism_entry, Double_t *alpha, Double_t *sterm_tot, Double_t *sterm_prim, gENUM_MATRIXINVERT invert_scheme, Double_t *n_tot, Double_t *n_prim);
   inline Bool_t              IsDeleteSolMod() const                                                             {return fIsDeleteSolMod;}
   Bool_t                     IsExistSrc(Int_t s_src, Int_t j_cr, Bool_t is_astro_or_dm) const;
   inline Bool_t              IsPropagated() const                                                               {return fIsPropagated;}
   TUValsTXYZECr             *OrphanFluxes(TUCoordTXYZ *coord_txyz, Int_t tot0_prim1_sec2);
   TCanvas                   *OrphanModelParameters(TURunOutputs const &outputs, Bool_t is_print, FILE *f_print=stdout, Bool_t is_date=true) const;
   TH1D                      *OrphanPrimaryContent(Double_t &ekn_gevn, TUCoordTXYZ *coord_txyz, string const &start, string const &stop, Bool_t is_element_or_isotope);
   TGraph                    *OrphanPrimaryContent(Int_t z_or_index_cr, TUCoordTXYZ *coord_txyz, Bool_t is_element_or_isotope);
   TH1D                      *OrphanGCRLocalAbundances(Bool_t is_cr_or_element, Double_t &ekn, Int_t tot0_prim1_sec2, string element_onwhich2rescale="Si", Double_t rescaled_value=100.);
   void                       PrintBoundaryConditions(FILE * f=stdout) const;
   void                       PrintIsotopicFraction(FILE * f=stdout, TUCoordTXYZ *coord_txyz=NULL);
   void                       PrintSolarSystemISFluxes(FILE *f=stdout, Int_t tot0_prim1_sec2=0);
   void                       PrintSources(FILE *f, Bool_t is_summary) const;
   void                       PrintSummary(FILE *f=stdout) const;
   void                       PrintSummaryBase(FILE *f=stdout) const;
   void                       PrintSummaryModel(FILE *f=stdout) const;
   inline void                PrintSummaryTransport(FILE *f=stdout) const                                         {if (fTransport) fTransport->PrintSummary(f);}
   virtual void               Propagate(Int_t jcr_start, Int_t jcr_stop, Bool_t is_norm_to_data, Bool_t is_verbose, FILE *f_log);
   void                       SetBCType(TUInitParList* init_pars, Bool_t is_verbose, FILE *f_log);
   inline void                SetBCTypeAntinucLE(gENUM_BCTYPE bctype4e)                                           {fBCTypeAntinucLE = bctype4e;}
   inline void                SetBCTypeAntinucHE(gENUM_BCTYPE bctype4e)                                           {fBCTypeAntinucHE = bctype4e;}
   inline void                SetBCTypeLeptonLE(gENUM_BCTYPE bctype4e)                                            {fBCTypeLeptonLE = bctype4e;}
   inline void                SetBCTypeLeptonHE(gENUM_BCTYPE bctype4e)                                            {fBCTypeLeptonHE = bctype4e;}
   inline void                SetBCTypeNucLE(gENUM_BCTYPE bctype4e)                                               {fBCTypeNucLE = bctype4e;}
   inline void                SetBCTypeNucHE(gENUM_BCTYPE bctype4e)                                               {fBCTypeNucHE = bctype4e;}
   void                       SetClass(string const &usine_initfile, string const &propag_model, Bool_t is_verbose, FILE *f_log);
   void                       SetClass(TUInitParList* init_pars, string const &propag_model, Bool_t is_verbose, FILE *f_log);
   inline void                SetEpsIntegr(Double_t const &eps_integr)                                            {fEpsIntegr = eps_integr;}
   inline void                SetEpsIterConv(Double_t const &eps_iter)                                            {fEpsIterConv = eps_iter;}
   inline void                SetEpsNormData(Double_t const &eps_normdata)                                        {fEpsNormData = eps_normdata;}
   inline void                SetPropagated(Bool_t is_propag)                                                     {fIsPropagated = is_propag;}
   void                       TEST(TUInitParList *init_pars, FILE *f=stdout);
   inline Bool_t              TUI_ModifyTransportParameters()                                                     {return fTransport->TUI_ModifyFreeParameters();}
   Bool_t                     TUI_ModifySrcParameters();
   void                       TUI_SetTXYZ(TUCoordTXYZ &coords);
   void                       UpdateAllParListStatusFromIndividualParStatus();
   void                       UseSolMod(TUSolModVirtual *sol_mod);

   ClassDef(TUModelBase, 1)  // Base ingredients (CR data, X-sections, transport, etc.) for propagation
};

#endif
