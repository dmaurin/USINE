// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

#ifndef USINE_TUCoordE
#define USINE_TUCoordE

// C/C++ include
#include <string>
using namespace std;
// ROOT include
#include <TRint.h>
// USINE include

class TUCoordE {

private:
   Int_t              fBinE;               // Index of bin for energy coordinate
   Int_t              fHalfBinStatus;      // Bin is fBinE (0), fBinE-1/2 (-1), or fBinE+1/2 (+1)
   Bool_t             fIsDeleteValsE;      // Whether to delete fValsE or not
   Int_t              fNValsE;             // Number of E-dependent variable values
   Double_t          *fValsE;              //![NValsE] E-dependent variable values (at bin fBinE)

public:
   TUCoordE();
   TUCoordE(TUCoordE const &coords);
   virtual ~TUCoordE();

   inline TUCoordE   *Clone()                                                  {return new TUCoordE(*this);}
   void               Copy(TUCoordE const &coords);
   inline TUCoordE   *Create()                                                 {return new TUCoordE;}
   string             FormValsE() const;
   inline Int_t       GetBinE() const                                          {return fBinE;}
   inline Int_t       GetHalfBinStatus() const                                 {return fHalfBinStatus;}
   inline Int_t       GetNValsE() const                                        {return fNValsE;}
   inline Double_t    GetValE(Int_t i_edepval) const                           {return fValsE[i_edepval];}
   inline Double_t   *GetValsE() const                                         {return &fValsE[0];}
   void               Initialise(Bool_t is_delete);
   inline Bool_t      IsDeleteValsE() const                                    {return fIsDeleteValsE;}
   void               PrintVals(FILE *f=stdout) const;
   inline void        SetBinE(Int_t bine, Int_t halfbin_status=0)              {fBinE=bine; fHalfBinStatus=halfbin_status;}
   inline void        SetCoordE(Double_t *valse, Int_t n_valse, Int_t bine, Int_t halfbin_status=0) {SetBinE(bine, halfbin_status); SetValsE(valse, n_valse, halfbin_status, false);}
   void               SetValsE(Double_t *valse, Int_t n_valse, Int_t halfbin_status, Bool_t is_use_or_copy);
   void               TEST(FILE *f=stdout);

   ClassDef(TUCoordE, 1)  // E coordinate: bin index and/or associated E-dependent variable values
};

#endif
