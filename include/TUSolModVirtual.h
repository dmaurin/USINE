// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

#ifndef USINE_TUSolModVirtual
#define USINE_TUSolModVirtual

// C/C++ include
#include <vector>
using namespace std;
// ROOT include
// USINE include
#include "TUCREntry.h"
#include "TUDataSet.h"
#include "TUFreeParList.h"

class TUSolModVirtual : public TUFreeParList {

private:
   vector<TUFreeParList*>     fParsExps;  //[NExp+1] Store free pars associated to exps (SolMod par. names copied in [NExp])

public:
   TUSolModVirtual();
   virtual ~TUSolModVirtual() = 0;

   inline TUFreeParList      *GetFreePars()                                {return this;}
   virtual gENUM_SOLMOD_MODEL GetModel() const = 0;
   inline string              GetModelName() const                         {return TUEnum::Enum2Name(GetModel());}
   virtual void               IStoTOA(TUCREntry const &cr, TUAxis *axis_ekn_is, Double_t *vflux_is, Double_t *vflux_toa, Double_t *r=NULL) = 0;
   void                       ParsModExps_Create(TUDataSet *data);
   void                       ParsModExps_Delete();
   inline TUFreeParList      *ParsModExps_Get(Int_t i_exp)                 {return fParsExps[i_exp];}
   inline TUFreeParList      *ParsModExps_GetBase()                        {return fParsExps[ParsModExps_GetN()];}
   inline Int_t               ParsModExps_GetN() const                     {return (Int_t)fParsExps.size()-1; /*'-1' because last entry in vector is not an experiment parameter*/}
   Int_t                      ParsModExps_Index(string exp_name) const;
   void                       ParsModExps_Indices(string const &par_name, Int_t &i_exp, Int_t &i_par) const;
   inline void                ParsModExps_Print(FILE *f) const             {for (Int_t i=0; i<(Int_t)fParsExps.size(); ++i) fParsExps[i]->PrintPars(f);}
   inline void                ParsModExps_UpdateSolModPars(Int_t i_exp)    {TUFreeParList::Copy(*fParsExps[i_exp]);}
   virtual void               Print(FILE *f, Bool_t is_summary) const = 0;
   virtual void               SetClass(TUInitParList *init_pars, Bool_t is_verbose, FILE *f_log) = 0;
   virtual void               TEST(TUInitParList *init_pars, FILE *f=stdout) = 0;
   inline string              UID() const                                  {return string(GetModelName() + "_" + TUFreeParList::UID());}

   ClassDef(TUSolModVirtual, 1)  // Abstract class for Solar modulation models
};

#endif
