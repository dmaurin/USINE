// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

#ifndef USINE_TUAxesTXYZ
#define USINE_TUAxesTXYZ

// C/C++ include
// ROOT include
// USINE include
#include "TUAxis.h"
#include "TUFreeParList.h"

class TUAxesTXYZ {

private:
   TUAxis              **fAxes;                //![fNAxes] Axis for T, X, Y, and Z (boundary and grid values)
   TUFreeParList        *fFreePars;            // Free parameters for TXYZ axes (only boundaries allowed)
   string                fGeomName;            // String storing the geometry name (0D...3D w/wo Time)
   Int_t                 fNAxes;               // Number of axes (set to 4 in constructor)
   Int_t                 fNDim;                // Number of spatial dims allocated (0; 1->X; 2->X,Y; or 3->X,Y,Z)

   void                  Initialise(Bool_t is_delete);
   void                  SetAxis(Int_t i, Double_t val_min, Double_t val_max, Int_t n, string const &name, string const &unit, gENUM_AXISTYPE axis_type);
   Bool_t                GetFreeParsStatusAxesTXYZ(Int_t i_axis) const;
   void                  UpdateNDimAndGeomName();
   void                  UpdateFromFreePars(Int_t i_axis, Bool_t is_min_or_max, Double_t const &val);

public:
   TUAxesTXYZ();
   TUAxesTXYZ(TUAxesTXYZ const &axes_txyz);
   virtual ~TUAxesTXYZ();

   inline TUAxesTXYZ    *Clone()                                                  {return new TUAxesTXYZ(*this);}
   void                  Copy(TUAxesTXYZ const &axes_txyz);
   inline TUAxesTXYZ    *Create()                                                 {return new TUAxesTXYZ;}
   inline TUAxis        *GetAxis(Int_t i_axis) const                              {return fAxes[i_axis];}
   inline Double_t       GetVal(Int_t i_axis, Int_t bin) const                    {return fAxes[i_axis]->GetVal(bin);}
   inline TUFreeParList *GetFreePars() const                                      {return fFreePars;}
   inline Bool_t         GetFreeParsStatusTAxis() const                           {return GetFreeParsStatusAxesTXYZ(0);}
   inline Bool_t         GetFreeParsStatusXAxis() const                           {return GetFreeParsStatusAxesTXYZ(1);}
   inline Bool_t         GetFreeParsStatusYAxis() const                           {return GetFreeParsStatusAxesTXYZ(2);}
   inline Bool_t         GetFreeParsStatusZAxis() const                           {return GetFreeParsStatusAxesTXYZ(3);}
   inline string         GetGeomName() const                                      {return fGeomName;}
   inline Double_t       GetMin(Int_t i_axis) const                               {return ((IsTXYZ(i_axis)) ? fAxes[i_axis]->GetVal(0) : 0.);}
   inline Double_t       GetMax(Int_t i_axis) const                               {return ((IsTXYZ(i_axis)) ? fAxes[i_axis]->GetVal(GetN(i_axis)-1) : 0.);}
   inline Int_t          GetN(Int_t i_axis) const                                 {return ((fAxes[i_axis]) ? fAxes[i_axis]->GetN() : 0);}
   inline Int_t          GetNAxes() const                                         {return fNAxes;}
   inline Int_t          GetNDim() const                                          {return fNDim;}
   inline Int_t          GetNT() const                                            {return GetN(0);}
   inline Int_t          GetNX() const                                            {return GetN(1);}
   inline Int_t          GetNY() const                                            {return GetN(2);}
   inline Int_t          GetNZ() const                                            {return GetN(3);}
   inline Double_t       GetT(Int_t bint) const                                   {return fAxes[0]->GetVal(bint);}
   inline Double_t       GetTMin() const                                          {return GetMin(0);}
   inline Double_t       GetTMax() const                                          {return GetMax(0);}
   inline TUAxis        *GetT() const                                             {return fAxes[0];}
   inline Double_t       GetX(Int_t binx) const                                   {return fAxes[1]->GetVal(binx);}
   inline Double_t       GetXMin() const                                          {return GetMin(1);}
   inline Double_t       GetXMax() const                                          {return GetMax(1);}
   inline TUAxis        *GetX() const                                             {return fAxes[1];}
   inline Double_t       GetY(Int_t biny) const                                   {return fAxes[2]->GetVal(biny);}
   inline Double_t       GetYMin() const                                          {return GetMin(2);}
   inline Double_t       GetYMax() const                                          {return GetMax(2);}
   inline TUAxis        *GetY() const                                             {return fAxes[2];}
   inline Double_t       GetZ(Int_t binz) const                                   {return fAxes[3]->GetVal(binz);}
   inline Double_t       GetZMin() const                                          {return GetMin(3);}
   inline Double_t       GetZMax() const                                          {return GetMax(3);}
   inline TUAxis        *GetZ() const                                             {return fAxes[3];}
   inline Int_t          IndexClosest(Int_t i_axis, Double_t const &val) const    {return fAxes[i_axis]->IndexClosest(val);}
   inline Bool_t         IsFreePars() const                                       {return ((fFreePars) ? true : false);}
   inline Bool_t         IsTXYZ(Int_t i_axis) const                               {return ((fAxes[i_axis]) ? true : false);}
   inline Bool_t         IsT() const                                              {return IsTXYZ(0);}
   inline Bool_t         IsX() const                                              {return IsTXYZ(1);}
   inline Bool_t         IsY() const                                              {return IsTXYZ(2);}
   inline Bool_t         IsZ() const                                              {return IsTXYZ(3);}
   void                  PrintSummary(FILE *f=stdout) const;
   void                  SetClass(TUInitParList *init_pars, string const &group, string const &subgroup, Bool_t is_verbose, FILE *f_log);
   inline void           SetTAxis(Double_t val_min, Double_t val_max, Int_t n, string const &name, string const &unit, gENUM_AXISTYPE axis_type) {SetAxis(0,val_min, val_max, n, name, unit, axis_type);}
   inline void           SetXAxis(Double_t val_min, Double_t val_max, Int_t n, string const &name, string const &unit, gENUM_AXISTYPE axis_type) {SetAxis(1,val_min, val_max, n, name, unit, axis_type);}
   inline void           SetYAxis(Double_t val_min, Double_t val_max, Int_t n, string const &name, string const &unit, gENUM_AXISTYPE axis_type) {SetAxis(2,val_min, val_max, n, name, unit, axis_type);}
   inline void           SetZAxis(Double_t val_min, Double_t val_max, Int_t n, string const &name, string const &unit, gENUM_AXISTYPE axis_type) {SetAxis(3,val_min, val_max, n, name, unit, axis_type);}
   void                  UpdateFromFreePars();
   void                  TEST(TUInitParList *init_pars, FILE *f=stdout);

   ClassDef(TUAxesTXYZ, 1)  // Generic spacetime (up to 3D+1) coordinate grids: TUAxis[1->4]
};

#endif
