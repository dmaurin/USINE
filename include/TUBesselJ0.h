// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

#ifndef USINE_TUBesselJ0
#define USINE_TUBesselJ0

// C/C++ include
#include <cmath>
#include <vector>
using namespace std;
// ROOT include
#include <Rtypes.h>
// USINE include
#include "TUMath.h"

class TUBesselJ0 {

private:
   vector<Double_t>   fBessRSol; //[fNBessel] Values for the often-used J0(fZero[i]*Rsol/R)
   vector<Double_t>   fJ1Zero;   //[fNBessel] Bessel J1 evaluated @ zeros of J0 (i.e. J1(fZero[i])
   Int_t              fNBessel;  // Order up to which expansion/summation is calculated
   Double_t           fRSol;     // Solar position [kpc] for which fBessRSol is filled
   Double_t           fRmax;     // radial boundary [kpc] for which fBessRSol is filled
   vector<Double_t>   fZero;     //[fNBessel] Zeros of the Bessel function J0 (J0(fZero[i])=0)

public:
   TUBesselJ0();
   virtual ~TUBesselJ0();

   static Double_t    BringmannCoeff(Int_t i_bess, Int_t n_bess);
   Double_t           Coeffs2Func(Double_t const &rho, Double_t *ni, Int_t n, Bool_t is_bringmann_weight);
   Double_t           Coeffs2Func(Double_t const *coef_j0i_rho, Double_t *ni, Int_t n, Bool_t is_bringmann_weight);
   inline Double_t    GetBessRSol(Int_t i_bess) const             {return fBessRSol[i_bess];}
   inline Double_t    GetJ1Zero(Int_t i_bess) const               {return fJ1Zero[i_bess];}
   inline Double_t   *GetJ1Zeros()                                {return &(fJ1Zero[0]);}
   inline Int_t       GetNBessel() const                          {return fNBessel;}
   inline Double_t    GetZero(Int_t i_bess) const                 {return fZero[i_bess];}
   inline Double_t   *GetZeros()                                  {return &(fZero[0]);}
   inline Bool_t      IsJ1ZeroFilled() const                      {return ((fJ1Zero.size()!=0) ? true : false);}
   inline Bool_t      IsZeroFilled() const                        {return ((fZero.size()!=0) ? true : false);}
   inline Double_t    J0Zetai_r2R(Int_t i_bess, Double_t r) const {return (fRSol>0. && (fabs(r - fRSol) / fRSol < 1.e-3) ? fBessRSol[i_bess] : TUMath::J0(fZero[i_bess] * r/fRmax));}
   void               Print(FILE *f, Int_t n_terms) const;
   void               SetBessRSol(Double_t const &rsol, Double_t const &rmax);
   inline void        SetNBessel(Int_t n)                         {fNBessel = n;}
   void               SetZerosAndJ1Zeros(Int_t nmax);
   void               TEST(FILE *f=stdout);

   ClassDef(TUBesselJ0, 1)  // Expansion/summation on J0 Bessel functions
};
#endif
