// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

#ifndef USINE_TUDataSet
#define USINE_TUDataSet

// C/C++ include
#include <iostream>
// ROOT include
#include <TGraphAsymmErrors.h>
#include <TMultiGraph.h>
// USINE include
#include "TUAxis.h"
#include "TUCREntry.h"
#include "TUDataEntry.h"
#include "TUInitParList.h"
#include "TUNormEntry.h"
#include "TUFreeParList.h"

class TUDataSet {
private:
   vector<TUDataEntry>             fData;                    //![NData] List of data entries
   vector<vector<Int_t> >          fDataPerExp[gN_ETYPE];    //![gN_ETYPE][NExps][NDataPerExp] Data indices per exp
   vector<vector<vector<Int_t> > > fDataPerQtyExp[gN_ETYPE]; //![gN_ETYPE][NExps][NQties][NDataPerQtyExp] Data indices per qty/exp
   vector<string>                  fFileNames;               //![NFiles] File names used to fill the class
   vector<string>                  fExps;                    //![NExps] Exp. names in fData
   vector<Bool_t>                  fIsNuisParsInFit;         // [NExpQty] Whether (data) nuisance parameters are used or not in fit
   vector<TUFreeParList*>          fNuisPars;                // [NExpQty] Free pars associated to exp/qty
   vector<Int_t>                   fMapDataIndex2NuisPars;   // [NData] Associate data to free parameter
   vector<gENUM_ETYPE>             fMapNuisPars2EType;       // [NExpQty] Associate free parameter to data
   vector<pair<Int_t,Int_t> >      fMapNuisPars2ExpQty;      // [NExpQty] Associate free parameter to data
   vector<string>                  fQty;                     //![NQties] Quantities in fData
   string                          fQtiesEtypesphiFF;        // If fData from OrphanFormSubSet(), stores Qties/Etypes/phiFF

   inline void                     AddFileEntry(string const &filename)                             {fFileNames.push_back(filename);}
   void                            FillQtiesAndDataPerQtyExp();
   void                            FillExpsAndDataPerExp();

public:
   TUDataSet();
   TUDataSet(TUDataSet const &dataset);
   TUDataSet(TUInitParList *init_pars, Bool_t is_verbose, FILE *f_log);
   virtual ~TUDataSet();

   inline void                     AddDataEntry(TUDataEntry const&entry)                            {fData.push_back(entry);}
   inline TUDataSet               *Clone()                                                          {return new TUDataSet(*this);}
   void                            Copy(TUDataSet const &dataset);
   inline TUDataSet               *Create()                                                         {return new TUDataSet;}
   inline void                     EmptyDataFiles()                                                 {fData.clear(); fFileNames.clear();}
   inline string                   ExtractExps() const                                              {return TUMisc::List2String(fExps,',');}
   inline string                   ExtractQties() const                                             {return TUMisc::List2String(fQty,',');}
   vector<Double_t>                GetERange(gENUM_CRFAMILY) const;
   inline TUDataEntry             *GetEntry(Int_t i_data) const                                     {return (TUDataEntry*)(&(fData[i_data]));}
   inline string                   GetExp(Int_t i_exp) const                                        {return fExps[i_exp];}
   inline string                   GetFileName(Int_t i_file) const                                  {return fFileNames[i_file];}
   inline Int_t                    GetNData() const                                                 {return (Int_t)fData.size();}
   inline Int_t                    GetNData(Int_t i_exp, gENUM_ETYPE etype) const                   {return (Int_t)fDataPerExp[(Int_t)etype][i_exp].size();}
   inline Int_t                    GetNData(Int_t i_exp, Int_t i_qty, gENUM_ETYPE etype) const      {return (Int_t)fDataPerQtyExp[(Int_t)etype][i_exp][i_qty].size();}
   inline Int_t                    GetNFiles() const                                                {return (Int_t)fFileNames.size();}
   inline Int_t                    GetNQties() const                                                {return (Int_t)fQty.size();}
   inline Int_t                    GetNExps() const                                                 {return (Int_t)fExps.size();}
   inline string                   GetQty(Int_t i_qty) const                                        {return fQty[i_qty];}
   inline string                   GetQtiesEtypesphiFF() const                                      {return fQtiesEtypesphiFF;}
   Int_t                           IndexData(TUDataEntry const &entry) const;
   inline Int_t                    IndexData(Int_t i_exp, gENUM_ETYPE etype) const                  {return ((GetNData(i_exp, etype) > 0) ? fDataPerExp[(Int_t)etype][i_exp][0] : -1);}
   inline Int_t                    IndexData(Int_t i_exp, Int_t i_qty, gENUM_ETYPE etype) const     {return ((GetNData(i_exp, i_qty, etype) > 0) ? fDataPerQtyExp[(Int_t)etype][i_exp][i_qty][0] : -1);}
   inline Int_t                    IndexDataClosestMatch(TUNormEntry const &ne) const               {return IndexDataClosestMatch(ne.GetName(), ne.GetExpList(), ne.GetE(), ne.GetEType());}
   Int_t                           IndexDataClosestMatch(string const &qty, string const &l_exp, Double_t e_val, gENUM_ETYPE e_axis, FILE *f=stdout) const;
   Int_t                           IndexInExps(string exp_name) const;
   Int_t                           IndexInQties(string qty) const;
   vector<Int_t>                   IndicesData(string const &qty) const;
   void                            IndicesData4AllExpQty(gENUM_ETYPE etype, vector<pair<Int_t,Int_t> > &idata_start_stop) const;
   void                            IndicesQtiesInExp(Int_t i_exp, vector<Int_t> &indices_qties) const;
   void                            Initialise();
   void                            LoadData(string const &filename, Bool_t is_replace_or_add, Bool_t is_verbose, FILE *f_log);
   Bool_t                          LoadDataRelCov(Int_t i_data_start, Int_t i_data_stop, string const &file_relcov, FILE *f_log) const;
   void                            LoadDataRelCov(Int_t i_data, vector<string> const &err_types, Int_t ne_cov, vector<vector<Double_t> > const &cov, FILE *f_log) const;
   string                          ListOfExpphi() const;
   string                          ListOfExpphi(string const & qties_exps_etype, FILE *f=stdout, Double_t e_min=1.e-40, Double_t e_max=1.e40, string const &t_start = "1940-01-01 00:00:00", string const &t_stop = "2100-01-01 00:00:00") const;
   string                          ListOfQties(Int_t i_exp, gENUM_ETYPE etype) const;
   TUDataSet                      *OrphanFormSubSet(string qties_expnames_etypes, FILE *f=stdout, Double_t e_min=1.e-40, Double_t e_max=1.e40, string const &t_start = "1940-01-01 00:00:00", string const &t_stop = "2100-01-01 00:00:00", Bool_t is_elem_into_isotopes=false) const;
   TGraphAsymmErrors              *OrphanGetGraph(Int_t i_data, gENUM_ERRTYPE err_type, gENUM_ETYPE e_type = kEKN, Double_t const& e_power = 0.);
   TMultiGraph                    *OrphanGetMultiGraph(string const &qty, string const &list_exp, gENUM_ERRTYPE err_type, gENUM_ETYPE e_type = kEKN, Double_t e_power = 0, Bool_t is_demodulate=false, TLegend* leg=NULL, FILE *f=stdout, Int_t z_demodul=-1, Int_t a_demodul=-1);
   void                            ParsData_Create();
   void                            ParsData_Delete();
   inline gENUM_ETYPE              ParsData_EType(Int_t i_nuispar) const                            {return fMapNuisPars2EType[i_nuispar];}
   inline TUFreeParList           *ParsData_Get(Int_t i_nuispar) const                              {return fNuisPars[i_nuispar];}
   inline Int_t                    ParsData_GetN() const                                            {return (Int_t)fNuisPars.size();}
   inline Int_t                    ParsData_IndexExp(Int_t i_nuispar) const                         {return fMapNuisPars2ExpQty[i_nuispar].first;}
   inline Int_t                    ParsData_IndexQty(Int_t i_nuispar) const                         {return fMapNuisPars2ExpQty[i_nuispar].second;}
   inline Bool_t                   ParsData_IsNuisParsInFit(Int_t i_nuispar) const                  {return fIsNuisParsInFit[i_nuispar];}
   void                            ParsData_Indices(string const &par_name, Int_t &i_expqty /*=i_nuispar*/, Int_t &t_type) const;
   inline void                     ParsData_Print(FILE *f) const                                    {for (Int_t i=0; i<(Int_t)fNuisPars.size(); ++i) fNuisPars[i]->PrintPars(f);}
   Double_t                        ParsData_ModelBias(Int_t i_data) const;
   inline void                     ParsData_SetIsNuisParsInFit(Int_t i_nuispar, Bool_t is_fit)      {fIsNuisParsInFit[i_nuispar] = is_fit;}
   void                            Print(FILE *f=stdout, Int_t switch_print=0) const;
   void                            PrintDataRelCov(FILE *f, Int_t k_type, Int_t i_data_start, Int_t i_data_stop) const;
   inline void                     PrintEntry(FILE *f, Int_t i_data) const                          {fData[i_data].Print(f,1);}
   inline void                     PrintExp(FILE *f, Int_t i_data, Bool_t is_header=true) const     {fData[i_data].PrintExp(f, is_header);}
   void                            PrintExps(FILE *f=stdout) const;
   void                            PrintExps(FILE *f, string const &qty) const;
   void                            PrintExps(FILE *f, string const &commasep_qties, Double_t ekn_min, Double_t ekn_max, gENUM_ETYPE e_type=kEKN) const;
   void                            PrintExpsQties(FILE *f, Bool_t is_expqty_or_qtyexp) const;
   void                            PrintFileNames(FILE *f=stdout) const;
   void                            PrintFromExpList(FILE *f, string const &commasep_exps) const;
   void                            SetClass(TUInitParList *init_pars, Bool_t is_verbose, FILE *f_log);
   inline void                     SetQtiesEtypesphiFF(string const &qties_etypes_phiff)            {fQtiesEtypesphiFF = qties_etypes_phiff;}
   void                            TEST(TUInitParList *init_pars, FILE *f=stdout);

   ClassDef(TUDataSet, 1)  // Sort/Queries/Form subsets from list of TUDataEntry [loaded from file]
};
#endif
