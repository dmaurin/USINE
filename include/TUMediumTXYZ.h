// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

#ifndef USINE_TUMediumTXYZ
#define USINE_TUMediumTXYZ

#ifdef __MAKECINT__
class FunctionParser{};
#else
#include "../FunctionParser/fparser.hh"
#endif
// C/C++ include
// ROOT include
// USINE include
#include "TUMediumEntry.h"
#include "TUValsTXYZEVirtual.h"

class TUMediumTXYZ {

private:
   TUValsTXYZEVirtual       **fDensities;   //![NElements] Number density of elements [cm^{-3}]
   TUValsTXYZEVirtual        *fDensityH2;   // H2 [cm^{-3}] (enabled only if H in medium)
   TUValsTXYZEVirtual        *fDensityHI;   // HI [cm^{-3}] (enabled only if H in medium)
   TUValsTXYZEVirtual        *fDensityHII;  // HII [cm^{-3}] (enabled only if H in medium)
   TUFreeParList             *fFreePars;    // Free parameters for the medium parameters
   TUMediumEntry             *fMediumEntry; // Medium description for a given space-time location
   TUValsTXYZEVirtual        *fPlasmaT;     // Plasma (electrons=HII) temperature [K]

   inline Int_t               IndexElementInMedium(Int_t z_elem) const                        {return fMediumEntry->IndexElementInMedium(z_elem);}
   void                       CheckIsBelongToListOfElement(string name) const;
   inline void                ResetElements()                                                 {fMediumEntry->ResetElements();}
   inline void                SetElements(string const &commasep_elements)                    {fMediumEntry->SetElements(commasep_elements);}
   inline Double_t            ValueDensityH(TUCoordTXYZ *coord_txyz)                          {UpdateFromFreeParsAndResetStatus(); return (ValueDensityHI(coord_txyz) + ValueDensityHII(coord_txyz) + 2.*ValueDensityH2(coord_txyz));}

public:
   TUMediumTXYZ();
   TUMediumTXYZ(TUMediumTXYZ const &medium);
   virtual ~TUMediumTXYZ();

   inline TUMediumTXYZ       *Clone()                                                         {return new TUMediumTXYZ(*this);}
   void                       Copy(TUMediumTXYZ const &medium);
   inline TUMediumTXYZ       *Create()                                                        {return new TUMediumTXYZ;}
   void                       FillMediumEntry(TUCoordTXYZ *coord_txyz);
   inline TUValsTXYZEVirtual *GetDensity(Int_t i_elem) const                                  {return fDensities[i_elem];}
   inline TUValsTXYZEVirtual *GetDensityH2() const                                            {return fDensityH2;}
   inline TUValsTXYZEVirtual *GetDensityHI() const                                            {return fDensityHI;}
   inline TUValsTXYZEVirtual *GetDensityHII() const                                           {return fDensityHII;}
   inline TUValsTXYZEVirtual *GetPlasmaT() const                                              {return fPlasmaT;}
   inline TUFreeParList      *GetFreePars() const                                             {return fFreePars;}
   inline TUMediumEntry      *GetMediumEntry() const                                          {return fMediumEntry;}
   inline TUMediumEntry      *GetMediumEntry(TUCoordTXYZ *coord_txyz)                         {FillMediumEntry(coord_txyz); return fMediumEntry;}
   inline string              GetNameMediumElement(Int_t i_element) const                     {return fMediumEntry->GetNameMediumElement(i_element);}
   inline Int_t               GetNMediumElements() const                                      {return fMediumEntry->GetNMediumElements();}
   inline Int_t               GetZElement(Int_t i_element) const                              {return fMediumEntry->GetZElement(i_element);}
   void                       Initialise(Bool_t is_delete = false);
   inline void                PrintEntry(FILE *f=stdout) const                                {fMediumEntry->Print(f);}
   void                       PrintSummary(FILE *f=stdout) const;
   void                       SetClass(TUInitParList *init_pars, string const &group, string const &subgroup, Bool_t is_verbose, FILE *f_log);
   void                       TEST(TUInitParList *init_pars, FILE *f=stdout);
   void                       UpdateFromFreeParsAndResetStatus();
   inline Double_t            ValueDensity(Int_t i_element, TUCoordTXYZ *coord_txyz)          {return ((GetZElement(i_element)==1) ?  ValueDensityH(coord_txyz) : fDensities[i_element]->ValueTXYZ(coord_txyz));}
   inline Double_t            ValueDensityHI(TUCoordTXYZ *coord_txyz)                         {UpdateFromFreeParsAndResetStatus(); return fDensityHI->ValueTXYZ(coord_txyz);}
   inline Double_t            ValueDensityHII(TUCoordTXYZ *coord_txyz)                        {UpdateFromFreeParsAndResetStatus(); return fDensityHII->ValueTXYZ(coord_txyz);}
   inline Double_t            ValueDensityH2(TUCoordTXYZ *coord_txyz)                         {UpdateFromFreeParsAndResetStatus(); return fDensityH2->ValueTXYZ(coord_txyz);}
   inline Double_t            ValueDensityNeutral(Int_t i_element, TUCoordTXYZ *coord_txyz)   {UpdateFromFreeParsAndResetStatus(); return ((GetZElement(i_element)==1) ?  ValueDensityHI(coord_txyz) : ValueDensity(i_element, coord_txyz));}
   inline Double_t            ValuePlasmaDensity(TUCoordTXYZ *coord_txyz)                     {UpdateFromFreeParsAndResetStatus(); return fDensityHII->ValueTXYZ(coord_txyz);}
   inline Double_t            ValuePlasmaT(TUCoordTXYZ *coord_txyz)                           {UpdateFromFreeParsAndResetStatus(); return fPlasmaT->ValueTXYZ(coord_txyz);}

   ClassDef(TUMediumTXYZ, 1)  // Medium generic 3D+1 (TXYZ) description [values or formula]
};

#endif
