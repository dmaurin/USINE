// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

#ifndef USINE_TUDataEntry
#define USINE_TUDataEntry

// C/C++ include
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <string>
#include <vector>
using namespace std;
// ROOT include
#include <Rtypes.h>
// USINE include
#include "TUCREntry.h"
#include "TUDatime.h"
#include "TUEnum.h"
#include "TUMisc.h"


class TUDataEntry {
private:
   string                    fBibURL;           // URL for the ADS bib. ref. (cdsads.u-strasbg.fr/abs/URL)
   Double_t                  fEBinL;            // Lower energy bin: unit depends on fEType [GeV/n, GeV, GV]
   Double_t                  fEBinU;            // Upper energy bin: unit depends on fEType [GeV/n, GeV, GV]
   Double_t                  fEMean;            // Mean energy: unit depends on fEType [GeV/n, GeV, GV]
   gENUM_ETYPE               fEType;            // kEKN, kR... see gENUM_ETYPE_UNITS in TUEnum.h
   vector<TUDatime>          fExpDatimes;       //![NDatimes] Data taking periods [YYYY-MM-DD HH:MM:SS]
   Double_t                  fExpDistance;      // Distance of instrument in Solar cavity [AU]
   string                    fExpName;          // Experiment name (case insensitive, no space)
   Double_t                  fExpphi;           // Solar Modulation parameter phi [GV]  (N.B.: phi = A/|Z| PHI)
   Bool_t                    fIsUpperLimit;     // Whether this is an upper limit (true) or a regular point
   Bool_t                    fIsAtLeast1NuisPar;// Is at least on CovType used as nuisance parameter?
   vector<Bool_t>            fIsCovTypeNuisPar; // [NTypes] Whether to use or not cov.err. for types (init=false)
   string                    fQty;              // Quantity measured (case insensitive)
   Double_t                  fY;                // Y value [gENUM_CRQTYPE_UNIT in TUEnum.h]
   vector<vector<Double_t> > fYErrRelCov;       // [NTypes][NErr] Array of rho_ij*sig_i*sig_j (i=fEMean)
   vector<string>            fYErrRelCovTypes;  // [NTypes] Error types for which cov. provided (case insensitive)
   Double_t                  fYErrStatL;        // Statistical error- (same unit as Y)
   Double_t                  fYErrStatU;        // Statistical error+ (same unit as Y)
   Double_t                  fYErrSystL;        // Systematic error- (same unit as Y)
   Double_t                  fYErrSystU;        // Systematic error+ (same unit as Y)

   inline void               AllocateYErrRelCovTypes(Int_t n_types)                 {fYErrRelCovTypes.clear(); fYErrRelCovTypes.assign(n_types, "");}
   Double_t                  GetYErr(gENUM_ERRTYPE err_type, Bool_t is_lower_or_upper) const;

public:
   TUDataEntry();
   TUDataEntry(TUDataEntry const &entry);
   virtual ~TUDataEntry();

   void                      AllocateYErrRelCov(Int_t n_types, Int_t ne_cov);
   inline TUDataEntry       *Clone()                                             {return new TUDataEntry(*this);}
   void                      Convert(gENUM_ETYPE e_type, TUCREntry const &cr);
   void                      Copy(TUDataEntry const &entry);
   inline TUDataEntry       *Create()                                            {return new TUDataEntry;}
   inline void               EraseYErrRelCov(Int_t first, Int_t last)            {for (Int_t i=0; i<GetNCovTypes(); ++i) fYErrRelCov[i].erase(fYErrRelCov[i].begin()+first, fYErrRelCov[i].begin()+last);}
   inline string             FormExpQtyEType() const                             {return GetExpName() + "_" + GetQty() + "_" + TUEnum::Enum2Name(GetEType());}
   inline string             GetBibFullURL() const                               {return "cdsads.u-strasbg.fr/abs/"+fBibURL;}
   inline string             GetBibURL() const                                   {return fBibURL;}
   inline Double_t           GetEBinL() const                                    {return fEBinL;}
   inline Double_t           GetEBinU() const                                    {return fEBinU;}
   inline Double_t           GetEMean() const                                    {return fEMean;}
   inline gENUM_ETYPE        GetEType() const                                    {return fEType;}
   inline string             GetEUnit(Bool_t is_bracket) const                   {return TUEnum::Enum2Unit(fEType, is_bracket);}
   inline TUDatime           GetExpDatime(Int_t j_flight) const                  {return fExpDatimes[j_flight];}
   string                    GetExpDatimes() const;
   inline Double_t           GetExpDistance() const                              {return fExpDistance;}
   inline string             GetExpName() const                                  {return fExpName;}
   inline Double_t           GetExpphi() const                                   {return fExpphi;}
   inline string             GetExpStart() const                                 {return fExpDatimes[0].GetStart();}
   inline TUDatime           GetExpStartStop() const                             {TUDatime tmp; tmp.SetStartStop(fExpDatimes[0].GetStart(), fExpDatimes[GetNDatimes()-1].GetStop()); return tmp;}
   inline string             GetExpStop() const                                  {return fExpDatimes[GetNDatimes()-1].GetStop();}
   inline string             GetQty(Int_t switch_format=0) const                 {return ((switch_format==0) ? fQty : TUMisc::FormatCRQty(fQty,switch_format));}
   inline Int_t              GetNCovE() const                                    {return GetNCovTypes()>0 ? (Int_t)fYErrRelCov[0].size() : 0;}
   inline Int_t              GetNCovTypes() const                                {return (Int_t)fYErrRelCovTypes.size();}
   inline Int_t              GetNDatimes() const                                 {return (Int_t)fExpDatimes.size();}
   inline Double_t           GetY() const                                        {return fY;}
   inline Double_t           GetYErrRelCov(Int_t t_type, Int_t k_e) const        {return fYErrRelCov[t_type][k_e];}
   inline string             GetYErrRelCovType(Int_t t_type) const               {return fYErrRelCovTypes[t_type];}
   inline string             GetYErrRelCovTypes() const                          {return TUMisc::List2String(fYErrRelCovTypes, ',');}
   inline Double_t           GetYErrL(gENUM_ERRTYPE err_type) const              {return GetYErr(err_type, true);}
   inline Double_t           GetYErrU(gENUM_ERRTYPE err_type) const              {return GetYErr(err_type, false);}
   inline Double_t           GetYErrStatL() const                                {return fabs(fYErrStatL);}
   inline Double_t           GetYErrStatU() const                                {return fabs(fYErrStatU);}
   inline Double_t           GetYErrSystL() const                                {return fabs(fYErrSystL);}
   inline Double_t           GetYErrSystU() const                                {return fabs(fYErrSystU);}
   inline Double_t           GetYErrTotL() const                                 {return sqrt(fYErrSystL*fYErrSystL + fYErrStatL*fYErrStatL);}
   inline Double_t           GetYErrTotU() const                                 {return sqrt(fYErrSystU*fYErrSystU + fYErrStatU*fYErrStatU);}
   inline string             GetYUnit(Bool_t is_bracket) const                   {if (TUMisc::IsRatio(fQty)) return (is_bracket) ? "[-]" : "-"; else return TUEnum::CRQUnit(fEType, is_bracket);}
   inline Int_t              IndexYErrCovType(string &type_name)                 {TUMisc::UpperCase(type_name); return TUMisc::IndexInList(fYErrRelCovTypes, type_name, false);}
   void                      Initialise();
   inline Bool_t             IsAtLeast1NuisPar() const                           {return fIsAtLeast1NuisPar;}
   inline Bool_t             IsCovTypeNuisPar(Int_t t_type) const                {return fIsCovTypeNuisPar[t_type];}
   Bool_t                    IsSameEntry(TUDataEntry const &entry) const;
   Bool_t                    IsSameQtyExpEtype(TUDataEntry const &entry) const;
   inline Bool_t             IsUpperLimit() const                                {return fIsUpperLimit;}
   void                      Print(FILE *f=stdout, Int_t switch_print=0, Bool_t is_header=false) const;
   void                      PrintExp(FILE *f=stdout, Bool_t is_header=false) const;
   void                      PrintDataRelCov(FILE *f=stdout, Int_t t_type = -1, Int_t ke_start=-1, Int_t ke_stop=-1, Bool_t is_header=false) const;
   inline void               SetE(Double_t const &e, Double_t const &e_l, Double_t const &e_u) {fEMean = e; fEBinL = e_l; fEBinU = e_u;}
   void                      SetEntry(string const &entry_line, FILE *f_log = stdout);
   void                      SetExpDatimes(string const &datimes);
   inline void               SetExpphi(Double_t const & phi)                     {fExpphi = phi;}
   inline void               SetIsCovTypeNuisPar(Int_t t_type)                   {fIsCovTypeNuisPar[t_type] = true; fIsAtLeast1NuisPar=true;}
   inline void               SetQty(string const &entry)                         {fQty = entry; TUMisc::UpperCase(fQty);}
   inline void               SetY(Double_t const &y)                             {fY = y;}
   inline void               SetYErrCov(Int_t t_type, Int_t k_e, Double_t cov)   {if (k_e<0 || k_e>=GetNCovE()) TUMessages::Error(stdout, "TUDataEntry","SetYErrCov","Call AllocateYErrRelCov() first!"); else fYErrRelCov[t_type][k_e] = cov;}
   inline void               SetYErrCovType(Int_t t_type, string const &type)    {if (t_type<0 || t_type>=GetNCovTypes()) TUMessages::Error(stdout, "TUDataEntry","SetYErrCovType","Call AllocateYErrRelCovType() first!"); else fYErrRelCovTypes[t_type] = type;}
   inline void               SetYErrStat(Double_t const &yl, Double_t const &yu) {fYErrStatL = yl; fYErrStatU = yu;}
   inline void               SetYErrSyst(Double_t const &yl, Double_t const &yu) {fYErrSystL = yl; fYErrSystU = yu;}
   void                      TEST(FILE *f=stdout);
   void                      YTimesPowerOfE(Double_t const& e_power);

   ClassDef(TUDataEntry, 1)  // Experimental data point (+ syst/stat errors) vs energy
};

#endif
