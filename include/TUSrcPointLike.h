// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

#ifndef USINE_TUSrcPointLike
#define USINE_TUSrcPointLike

// C/C++ include
// ROOT include
// USINE include
#include "TUSrcVirtual.h"

class TUSrcPointLike : public TUSrcVirtual {

private:
   Bool_t                             fIsDeleteSrcSpectrum;    // Whether to delete or not fSrcSpectrum in destructor
   Bool_t                            *fIsDeleteTDepPosition;   //![3] Whether to delete or not fTDepPosition in destructor
   TUValsTXYZEVirtual                *fSrcSpectrum;            // Source spectrum dN/dEkn[#part/(m^3.s.GeV/n)]
   string                             fSrcName;                // Source name (for a single CR species)
   TUValsTXYZEFormula               **fTDepPosition;           //![3] Time-dependent position X(t), Y(t), Z(t)
   Double_t                           fTStart;                 // Start time of the source
   Double_t                           fTStop;                  // Stop time of the source

   virtual void                       UpdateFromFreeParsAndResetStatus();

public:
   TUSrcPointLike();
   TUSrcPointLike(TUSrcPointLike const &src_pointlike);
   virtual ~TUSrcPointLike();

   inline virtual TUSrcPointLike     *Clone()                                                               {return new TUSrcPointLike(*this);}
   virtual void                       CopySpectrumOrSpatDist(TUValsTXYZEVirtual *spect_or_spatdist, Bool_t is_spect_or_spatdist);
   void                               Copy(TUSrcPointLike const &src_pointlike);
   inline virtual TUSrcPointLike     *Create()                                                              {return new TUSrcPointLike;}
   void                               FillSpectrumFromTemplate(TUSrcTemplates *templ, string const &templ_name, FILE *f_log);
   virtual void                       FillSpectrumOrSpatialDist(string const &formula, Bool_t is_verbose, FILE *f_log, string const &commasep_vars, TUFreeParList* free_pars, Bool_t is_use_or_copy_freepars, Bool_t is_spect_or_spatdist);
   virtual void                       FillSpectrumOrSpatialDistFromTemplate(TUSrcTemplates *templ, string const &templ_name, Bool_t is_spect_or_spatdist, FILE *f_log);
   inline virtual TUFreeParList      *GetSrcFreePars(Bool_t is_spect_or_spatdist)                           {TUFreeParList *dummy = NULL; return (is_spect_or_spatdist) ? fSrcSpectrum->GetFreePars() : dummy;}
   inline virtual string              GetSrcName() const                                                    {return fSrcName;}
   inline virtual TUValsTXYZEVirtual *GetSrcSpectrum() const                                                {return fSrcSpectrum;}
   inline virtual TUValsTXYZEFormula *GetTDepPosition(Int_t i_coord) const                                  {return fTDepPosition[i_coord];}
   inline Double_t                    GetTStart() const                                                     {return fTStart;}
   inline Double_t                    GetTStop() const                                                      {return fTStop;}
   virtual void                       Initialise(Bool_t is_delete);
   inline Bool_t                      IsDeleteSrcSpectrum() const                                           {return fIsDeleteSrcSpectrum;}
   inline Bool_t                      IsDeleteTDepPosition(Int_t i_coord) const                             {return fIsDeleteTDepPosition[i_coord];}
   virtual void                       PrintSrc(FILE* f=stdout, Bool_t is_header=true) const;
   inline virtual void                SetSrcName(string const &name)                                        {fSrcName = name;}
   void                               SetTDepPosition(Int_t i_coord, string const &fparser_formula);
   inline void                        SetTStart(Double_t tstart)                                            {fTStart = tstart;}
   inline void                        SetTStop(Double_t tstop)                                              {fTStop = tstop;}
   virtual void                       TEST(TUInitParList *init_pars, FILE *f=stdout);
   virtual Bool_t                     TUI_ModifyFreeParameters();
   inline virtual string              UIDSpectrum() const                                                   {return string(fSrcName + "_" + fSrcSpectrum->UID());}
   void                               UseTDepPosition(Int_t i_coord/*0,1,2*/, TUValsTXYZEFormula *pos);
   virtual void                       UseSpectrumOrSpatDist(TUValsTXYZEVirtual *spect_or_spatdist, Bool_t is_spect_or_spatdist);
   inline virtual Double_t            ValueSrcPosition(Int_t i_coord/*0,1,2*/, Double_t const &t_myr)       {TUCoordTXYZ t; t.SetValT(t_myr); return fTDepPosition[i_coord]->ValueTXYZ(&t);}
   inline virtual Double_t            ValueSrcSpectrum(TUCoordE *coord_e, TUCoordTXYZ *coord_txyz = NULL)   {return fSrcSpectrum->ValueETXYZ(coord_e, coord_txyz);}

   ClassDef(TUSrcPointLike,1)  // Point-like source: name, spectrum, t-dep. position [inherits from TUSrcVirtual]
};

#endif
