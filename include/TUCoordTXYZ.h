// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

#ifndef USINE_TUCoordTXYZ
#define USINE_TUCoordTXYZ

// C/C++ include
#include <string>
using namespace std;
// ROOT include
#include <TRint.h>
// USINE include
#include "TUAxesTXYZ.h"

class TUCoordTXYZ {

private:
   static const Int_t  fN = 4;     // Number of space-time dimensions
   Int_t               fBins[fN];  // T,X,Y,Z index (time/1st/2nd/3rd spatial coord.)
   Double_t            fVals[fN];  // T,X,Y,Z value (time/1st/2nd/3rd spatial coord.)

public:
   TUCoordTXYZ();
   TUCoordTXYZ(TUCoordTXYZ const &coords);
   virtual ~TUCoordTXYZ();

   inline TUCoordTXYZ  *Clone()                                                   {return new TUCoordTXYZ(*this);}
   void                 ConvertBins2Vals(TUAxesTXYZ *axes_txyz);
   void                 ConvertVals2Bins(TUAxesTXYZ *axes_txyz);
   void                 Copy(TUCoordTXYZ const &coords);
   inline TUCoordTXYZ  *Create()                                                  {return new TUCoordTXYZ;}
   string               FormBins(Bool_t is_extra=true) const;
   string               FormVals(Bool_t is_extra=true, Bool_t is_t=true, Int_t ndim=3) const;
   inline Int_t         GetBin(Int_t i_axis) const                                {return fBins[i_axis];}
   inline Int_t        *GetBins()                                                 {return fBins;}
   inline Int_t         GetBinT() const                                           {return fBins[0];}
   inline Int_t         GetBinX() const                                           {return fBins[1];}
   inline Int_t         GetBinY() const                                           {return fBins[2];}
   inline Int_t         GetBinZ() const                                           {return fBins[3];}
   inline Double_t      GetVal(Int_t i_axis) const                                {return fVals[i_axis];}
   inline Double_t     *GetVals()                                                 {return fVals;}
   inline Double_t      GetValT() const                                           {return fVals[0];}
   inline Double_t      GetValX() const                                           {return fVals[1];}
   inline Double_t      GetValY() const                                           {return fVals[2];}
   inline Double_t      GetValZ() const                                           {return fVals[3];}
   void                 Initialise();
   void                 PrintBins(FILE *f=stdout) const;
   void                 PrintVals(FILE *f=stdout) const;
   inline void          SetBin(Int_t i_axis, Int_t bin)                           {fBins[i_axis] = bin;}
   inline void          SetBins(Int_t *bin_txyz)                                  {SetBins(bin_txyz[0], bin_txyz[1], bin_txyz[2], bin_txyz[3]);}
   void                 SetBins(Int_t bint, Int_t binx, Int_t biny, Int_t binz);
   inline void          SetBinT(Int_t bint)                                       {SetBin(0, bint);}
   inline void          SetBinTX(Int_t bint, Int_t binx)                          {SetBinT(bint), SetBinX(binx);}
   inline void          SetBinTXY(Int_t bint, Int_t binx, Int_t biny)             {SetBinTX(bint,binx), SetBinY(biny);}
   inline void          SetBinTXYZ(Int_t bint, Int_t binx, Int_t biny, Int_t binz){SetBinTXY(bint,binx,biny), SetBinZ(binz);}
   inline void          SetBinX(Int_t binx)                                       {SetBin(1, binx);}
   inline void          SetBinXY(Int_t binx, Int_t biny)                          {SetBinX(binx), SetBinY(biny);}
   inline void          SetBinXYZ(Int_t binx, Int_t biny, Int_t binz)             {SetBinXY(binx,biny), SetBinZ(binz);}
   inline void          SetBinY(Int_t biny)                                       {SetBin(2, biny);}
   inline void          SetBinZ(Int_t binz)                                       {SetBin(3, binz);}
   inline void          SetVal(Int_t i_axis, Double_t val)                        {fVals[i_axis] = val;}
   inline void          SetVals(Double_t const *vals_txyz)                        {SetVals(vals_txyz[0], vals_txyz[1], vals_txyz[2], vals_txyz[3]);}
   void                 SetVals(Double_t valt, Double_t valx, Double_t valy, Double_t valz) {fVals[0] = valt; fVals[1] = valx; fVals[2] = valy; fVals[3] = valz;}
   inline void          SetValT(Double_t valt)                                    {SetVal(0, valt);}
   inline void          SetValTX(Double_t valt, Double_t valx)                    {SetValT(valt), SetValX(valx);}
   inline void          SetValTXY(Double_t valt, Double_t valx, Double_t valy)    {SetValTX(valt,valx), SetValY(valy);}
   inline void          SetValTXYZ(Double_t valt, Double_t valx, Double_t valy, Double_t valz){SetValTXY(valt,valx,valy), SetValZ(valz);}
   inline void          SetValX(Double_t valx)                                    {SetVal(1, valx);}
   inline void          SetValXY(Double_t valx, Double_t valy)                    {SetValX(valx), SetValY(valy);}
   inline void          SetValXYZ(Double_t valx, Double_t valy, Double_t valz)    {SetValXY(valx,valy), SetValZ(valz);}
   inline void          SetValY(Double_t valy)                                    {SetVal(2, valy);}
   inline void          SetValZ(Double_t valz)                                    {SetVal(3, valz);}
   void                 TEST(FILE *f=stdout);

   ClassDef(TUCoordTXYZ, 1)  // TXYZ space-time coordinates: bin indices and/or TXYZ values
};

#endif
