// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

#ifndef USINE_TUCRList
#define USINE_TUCRList

// C/C++ include
// ROOT include
#include <TH1D.h>
// USINE include
#include "TUAtomProperties.h"
#include "TUCREntry.h"
#include "TUFreeParList.h"
#include "TUInitParList.h"

class TUCRList : public TUAtomProperties {

private:

   vector<TUCREntry>          fCRs;                     //![NCRs] List of CR entries/charts (TUCREntry)
   string                     fFileName;                // File name (on disk) from which list of CRs is filled.
   TUFreeParList             *fFreePars;                // Free parameters for CR list
   string                     fIDList;                  // List identification (useful if several CR lists)
   vector<Int_t>              fIndexParentBETA;         //![NCRs] Index of BETA-decay parent (-1 if none)
   vector<Int_t>              fIndexParentEC;           //![NCRs] Index of EC-decay parent (-1 if none)
   vector<Int_t>              fIndicesAntinuclei;       //![NAntinuclei] Indices (in fCRs) of antinuclei
   vector<Int_t>             *fIndicesFragments;        //![NCRs][NFragments(cr)] Indices (in fCRs) of CR spallative fragments
   vector<Int_t>              fIndicesLeptons;          //![NLeptons] Indices (in fCRs) of leptons
   vector<Int_t>              fIndicesNuclei;           //![NNuclei] Indices (in fCRs) of nuclei
   vector<Int_t>             *fIndicesParents;          //![NCRs][NParents(cr)] Indices (in fCRs) of CR spallation parents
   vector<Int_t>              fIndicesPureSecondaries;  //![NPureSec] Indices (in fCRs) of pure secondaries
   Bool_t                     fIsDeleteFreePars;        // Delete fFreePars or not (in destructor)
   Bool_t                     fIsGhosts;                // Whether ghost nuclei are filled or not in list
   vector<Bool_t>             fIsPureSecondary;         //![NCRs] Whether a CR is a secondary or not (not in source)
   vector<pair<Int_t,Int_t> > fMapBETAIndex2CRIndex;    //![NDecayBETA] Indices BETA unstable/daughter in CR list
   vector<pair<Int_t,Int_t> > fMapECIndex2CRIndex;      //![NDecayEC] Indices EC unstable/daughter in CR list
   string                     fNamePureSecondaries;     // Comma-sep. list of CRs (or elements) forced to be pure sec.
   vector<Int_t>              fParsHalfLife_CRIndices;  //![N_HL] Indices of CRs whose half-life are free parameters
   vector<Bool_t>             fParsHalfLife_IsBETAOrEC; //![N_HL] Whether Half-live is for BETA or EC
   vector<Double_t>           fParsHalfLife_RefVals;    //![N_HL] Reference values for half-lives

   void                       AllocateParentsAndFragments();
   void                       CheckParents(Int_t j_cr);
   inline Bool_t              IsDeleteFreePars() const                                     {return fIsDeleteFreePars;}
   void                       ParsHalfLife_AddPar(TUFreeParList *pars, Int_t i_par, Bool_t is_use_or_copy, FILE *f_log);
   inline Int_t               ParsHalfLife_GetN() const                                    {return (Int_t)fParsHalfLife_CRIndices.size();}
   void                       ParsHalfLife_Initialise(Bool_t is_delete=false);
   void                       PrintCharts(FILE *f=stdout, string crname_or_type="ALL") const;
   void                       PrintElements(FILE *f, Bool_t is_separator = true, Bool_t is_summary=true) const;
   void                       PrintFragments(FILE *f=stdout) const;
   void                       PrintFragments(FILE *f, Int_t j_cr) const;
   void                       PrintGhosts(FILE *f, Int_t j_cr) const;
   void                       PrintGhosts(FILE *f, Int_t z_first, Int_t z_last, string type = "ALL", Double_t br_min = 0., Double_t br_max = 100.) const;
   void                       PrintListForGhost(FILE *f, string ghost_name) const;
   void                       PrintPureSecondaries(FILE *f=stdout) const;
   void                       PrintReactions(FILE *f, string const &cr_heavier, string const &cr_lighter, Bool_t is_print_az, Bool_t is_use_parentlist) const;
   void                       PrintSSIsotopeAbundances(FILE *f=stdout) const;
   void                       PrintSSIsotopicFractions(FILE *f=stdout) const;
   void                       PrintSSIsotopicFractions(FILE *f, Int_t z) const;
   void                       UpdateCRs(TUInitParList *init_pars, Bool_t is_verbose, FILE *f_log);
   void                       UpdateFragments();
   void                       UpdateParents();
   void                       UpdateParents(TUInitParList *init_pars, Bool_t is_verbose, FILE *f_log);
   void                       UpdatePureSecondaries(TUInitParList *init_pars, Bool_t is_verbose, FILE *f_log);

public:
   TUCRList();
   TUCRList(TUCRList const &crs);
   TUCRList(TUInitParList *init_pars, Bool_t is_verbose, FILE *f_log);
   virtual ~TUCRList();

   void                       CheckAndUpdateNormElement(string &element) const;
   inline TUCRList           *Clone()                                                      {return new TUCRList(*this);}
   Bool_t                     ComboToCRIndices(string const &combo, vector<Int_t> &cr_indices, Int_t &n_denom, Bool_t is_verbose = true, FILE *f=stdout) const;
   Bool_t                     ComboToCRNames(string const &combo, vector<string> &cr_names, Int_t &n_denom, Bool_t is_verbose = true, FILE *f=stdout) const;
   void                       ComboToElementNames(string const &combo, vector<string> &element_names) const;
   void                       CombosToCRNames(vector<string> &list_combos, vector<string> &cr_names) const;
   void                       CombosToElementAndCRNames(vector<string> &list_combos, vector<string> &all_names) const;
   void                       CombosToElementNames(vector<string> &list_combos, vector<string> &element_names) const;
   void                       Copy(TUCRList const &crs);
   inline TUCRList           *Create()                                                     {return new TUCRList;}
   inline string              CRToElementName(Int_t j_cr) const                            {return ZToElementName(fCRs[j_cr].GetZ());}
   Bool_t                     ElementNameToCRNames(string const &element, vector<string> & cr_names, Bool_t is_verbose = true, FILE *f=stdout) const;
   Bool_t                     ElementNameToCRIndices(string const &element,vector<Int_t> & cr_indices, Bool_t is_verbose = true, FILE *f=stdout) const;
   vector<Int_t>              ExtractAllCRs(Bool_t is_only_nuc, Int_t jcr_min=-100, Int_t jcr_max=10000) const;
   vector<string>             ExtractAllElements() const;
   vector<Int_t>              ExtractAllZ(Bool_t is_only_nuc, Int_t z_min=-100, Int_t z_max=200) const;
   vector<string>             ExtractCombo(string const &commasep_list, Bool_t is_verbose, FILE *f=stdout) const;
   void                       ExtractComboEtypephiff(string const &combos_etypes_phiff, vector<string> &list_combo, vector<gENUM_ETYPE> &list_etype, vector<vector<Double_t> > &list_phiff, Bool_t is_verbose, FILE *f=stdout) const;
   string                     ExtractCRNames(vector<Int_t> &indices, char sep=',') const;
   Int_t                      ExtractZMin() const;
   Int_t                      ExtractZMax() const;
   inline TUCREntry           GetCREntry(Int_t j_cr) const                                 {return fCRs[j_cr];}
   inline string              GetFileName() const                                          {return fFileName;}
   inline TUFreeParList      *GetFreePars() const                                          {return fFreePars;}
   inline string              GetIDList() const                                            {return fIDList;}
   inline string              GetNameROOT(Int_t j_cr) const                                {return fCRs[j_cr].GetName(2);}
   string                     GetNamesPureSecondaries(Bool_t is_thoseinlist=true) const;
   inline Int_t               GetNAntiNuc() const                                          {return fIndicesAntinuclei.size();}
   inline Int_t               GetNCRs() const                                              {return fCRs.size();}
   inline Int_t               GetNFragments(Int_t j_cr) const                              {return (!fIndicesFragments) ? 0 : (Int_t)(fIndicesFragments[j_cr].size());}
   inline Int_t               GetNLeptons() const                                          {return fIndicesLeptons.size();}
   inline Int_t               GetNNuclei() const                                           {return fIndicesNuclei.size();}
   inline Int_t               GetNParents(Int_t j_cr) const                                {return (!fIndicesParents) ? 0 : (Int_t)(fIndicesParents[j_cr].size());}
   inline Int_t               GetNPureSecondaries() const                                  {return (Int_t)(fIndicesPureSecondaries.size());}
   inline Int_t               GetNUnstables(Bool_t is_beta_or_ec) const                    {return ((is_beta_or_ec) ? fMapBETAIndex2CRIndex.size() : fMapECIndex2CRIndex.size());}
   inline Int_t               GetNZ() const                                                {return ExtractZMax()-ExtractZMin()+1;}
   Int_t                      Index(string const &cr, Bool_t is_verbose=true, FILE*f_log=stdout, Bool_t is_abort = false) const;
   inline Int_t               IndexAntinucleus(Int_t i_antinuc=0) const                    {return fIndicesAntinuclei[i_antinuc];}
   inline Int_t               IndexDecayDaughter(Int_t j_unstable, Bool_t is_beta_or_ec) const {return ((is_beta_or_ec) ? fMapBETAIndex2CRIndex[j_unstable].second : fMapECIndex2CRIndex[j_unstable].second);}
   inline Int_t               IndexDecayParent(Int_t j_unstable, Bool_t is_beta_or_ec) const   {return ((is_beta_or_ec) ? fMapBETAIndex2CRIndex[j_unstable].first : fMapECIndex2CRIndex[j_unstable].first);}
   inline Int_t               IndexInCRList_Fragment(Int_t j_cr, Int_t j_frag) const       {return fIndicesFragments[j_cr][j_frag];}
   inline Int_t               IndexInCRList_Parent(Int_t j_cr, Int_t j_proj) const         {return fIndicesParents[j_cr][j_proj];}
   Int_t                      IndexInParentList(Int_t j_cr, string const &parent) const;
   inline Int_t               IndexPureSecondary(Int_t j_puresec) const                    {return fIndicesPureSecondaries[j_puresec];}
   inline Int_t               IndexLepton(Int_t i_lepton=0) const                          {return fIndicesLeptons[i_lepton];}
   inline Int_t               IndexNucleus(Int_t i_nuc=0) const                            {return fIndicesNuclei[i_nuc];}
   inline Int_t               IndexParentBETA(Int_t j_cr) const                            {return fIndexParentBETA[j_cr];}
   inline Int_t               IndexParentEC(Int_t j_cr) const                              {return fIndexParentEC[j_cr];}
   void                       Indices(string commasep_list_crs, vector<Int_t> &cr_indices, Bool_t is_inlist = true) const;
   void                       IndicesForGhost(string ghost_name, vector<Int_t> &cr_indices, vector<Int_t> &ghost_indices) const;
   void                       Initialise(Bool_t is_delete = false);
   inline Bool_t              IsAtLeastOneAntinuc() const                                  {return (GetNAntiNuc()>0 ? true : false);}
   inline Bool_t              IsAtLeastOneLepton() const                                   {return (GetNLeptons()>0 ? true : false);}
   inline Bool_t              IsAtLeastOneNucleus() const                                  {return (GetNNuclei()>0 ? true : false);}
   inline Bool_t              IsDecayFed(Int_t j_cr, Bool_t is_beta_or_ec) const           {return ((is_beta_or_ec) ? (((fIndexParentBETA[j_cr]<0) ? false : true)) : (((fIndexParentEC[j_cr]<0) ? false : true)));}
   inline Bool_t              IsDecayFed(Int_t j_cr) const                                 {if (IsDecayFed(j_cr, true) || IsDecayFed(j_cr, false)) return true; else return false;}
   Bool_t                     IsIndex(Int_t j_cr, FILE *f=stdout) const;
   Bool_t                     IsIndex(Int_t j_cr, Int_t j_proj, FILE *f=stdout) const;
   Bool_t                     IsElementAndInList(string const &qty) const;
   inline Bool_t              IsGhostsLoaded() const                                       {return fIsGhosts;}
   inline Bool_t              IsOnlyNucAndAntinucInList() const                            {return ((IsAtLeastOneNucleus() || IsAtLeastOneAntinuc()) && !IsAtLeastOneLepton());}
   inline Bool_t              IsPureSecondary(Int_t j_cr) const                            {return fIsPureSecondary[j_cr];}
   void                       LoadSSAbundances(string const &file_ssabund);
   Bool_t                     MapIndices(TUCRList *newlist, vector<Int_t> &indices_newlist_inclasslist, Bool_t is_verbose = false, FILE *f=stdout) const;
   TH1D                      *OrphanEmptyHistoOfCRs(string const& h_name, string const &title, vector<Int_t> const &list_jcr) const;
   TH1D                      *OrphanEmptyHistoOfCRs(string const& h_name, string const &title, string cr_first="DEFAULT", string cr_last="DEFAULT") const;
   TH1D                      *OrphanEmptyHistoOfElements(string const& h_name, string const &title, vector<Int_t> const &list_z) const;
   TH1D                      *OrphanEmptyHistoOfElements(string const& h_name, string const &title, string element_first="DEFAULT", string element_last="DEFAULT") const;
   void                       ParsHalfLife_AddPars(TUFreeParList *pars, Int_t p_first, Int_t p_last, Bool_t is_use_or_copy, Bool_t is_verbose, FILE *f_log);
   void                       ParsHalfLife_Print(FILE *f, Int_t p_halflife) const;
   void                       ParsHalfLife_UpdateFromFreeParsAndResetStatus();
   void                       PrintList(FILE *f=stdout, Bool_t is_separator = true, Bool_t is_summary=true) const;
   void                       PrintParents(FILE *f=stdout) const;
   void                       PrintParents(FILE *f, Int_t j_cr) const;
   void                       PrintUnstables(FILE *f, Bool_t is_beta_or_ec) const;
   void                       SetClass(TUInitParList *init_pars, Bool_t is_verbose, FILE *f_log, Bool_t is_reload_atom);
   void                       SetCRList(string const &f_charts, string const &commasep_list, Bool_t is_fill_ghosts, string const &list_name);
   void                       SetCRList(string const &f_charts, Int_t cr_switch, vector<string> &crs, Bool_t is_fill_ghosts, string const &list_name="");
   vector <Int_t>             SortByGrowingZ(vector<string> &crs_and_elements) const;
   vector<Int_t>              SortByGrowingZ(vector<Int_t> &cr_indices) const;
   Double_t                   SSElementAbundance(Int_t z) const;
   void                       TEST(TUInitParList *init_pars, FILE *f=stdout);
   Double_t                   TUI_SelectCombosEAxisphiFF(vector<string> &list_combos, vector<gENUM_ETYPE> &list_etypes, vector<vector<Double_t> > &list_phiff, Bool_t is_epower = false)const;
   Double_t                   TUI_SelectCombosEAxisphiFF(string &combos_etypes_phiff, Bool_t is_epower = false) const;
   inline void                UpdateCREntry(Int_t j_cr, TUCREntry const &cr)                       {fCRs[j_cr].Copy(cr);}
   inline void                UpdateCRFriend(Int_t j_cr, string const &name, Bool_t is_beta_or_ec) {fCRs[j_cr].SetCRFriend(name, is_beta_or_ec);}
   inline void                UpdateHalfLife(Int_t j_cr, Double_t const &hl, Bool_t is_beta_or_ec) {fCRs[j_cr].SetHalfLife(hl, is_beta_or_ec);}
   void                       UpdateIndicesOfUnstables();
   void                       UpdateParents(Int_t j_cr, Int_t cr_switch, vector<string> &parents);
   void                       UpdateType(Int_t j_cr, string const &type)                           {fCRs[j_cr].SetType(type);}

   ClassDef(TUCRList, 1)  // CR charts and their fragmentation/decay parents [loaded from file]
};

#endif
