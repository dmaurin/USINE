// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

#ifndef USINE_TUFreeParEntry
#define USINE_TUFreeParEntry

// C/C++ include
#include <cmath>
#include <string>
using namespace std;
// ROOT include
#include <TRint.h>
// USINE include
#include "TUEnum.h"

class TUFreeParEntry {

private:
   Double_t                 fFitInit;           // Starting point for fit (minimisation routine only)
   Double_t                 fFitInitMin;        // Min value for fVal if fIsLowerLimitedPar=true (minimisation only)
   Double_t                 fFitInitMax;        // Max value for fVal if fIsUpperLimitedPar=true (minimisation only)
   Double_t                 fFitInitSigma;      // Width for fVal (minimisation routine only)
   gENUM_AXISTYPE           fFitSampling;       // Sampling is kLIN or kLOG. If kLOG, fFitVal/fFitInit values are 'log'
   gENUM_FREEPARTYPE        fFitType;           // Parameter type: FIT, FIXED, or NUISANCE (see TUEnum.h)
   Double_t                 fFitValBest;        // Value for best fit (from minimisation routine)
   Double_t                 fFitValErr;         // Symmetric error on best fit (from minimisation routine)
   Double_t                 fFitValErrAsymmLo;  // Asymmetric error (low) on best fit (from minimisation routine)
   Double_t                 fFitValErrAsymmUp;  // Asymmetric error (high) on best fit (from minimisation routine)
   Int_t                    fIndexInExtClass;   // If free par of another class, store matching index in that class
   Bool_t                   fIsLowerLimitedPar; // If true, use fFitInitMin, otherwise unlimited
   Bool_t                   fIsUpperLimitedPar; // If true, use fFitInitMax, otherwise unlimited
   string                   fName;              // Name (case sensitive)
   Bool_t                   fStatus;            // Whenever fVal is modified, fStatus set to 1 (0 otherwise)
   string                   fUnit;              // Unit (default is no unit set to [-])
   Double_t                 fVal;               // Entry value ('lin', even if fFit... values in 'log', i.e. kLOG)

   string                   FormInitPar(Int_t n_digits) const;
   void                     PrintFitRelated(FILE *f, Int_t all0_fitonly1_initonly2) const;

public:
   TUFreeParEntry();
   TUFreeParEntry(TUFreeParEntry const &par);
   virtual ~TUFreeParEntry();

   inline TUFreeParEntry   *Clone()                                        {return new TUFreeParEntry(*this);}
   void                     Copy(TUFreeParEntry const &par);
   inline void              CopyValInFitInit()                             {if (fFitSampling == kLOG) fFitInit = log10(fVal); else fFitInit = fVal;}
   inline void              CopyFitValBestInVal()                          {if (fFitSampling == kLOG) fVal = pow(10.,fFitValBest); else fVal = fFitValBest;}
   inline TUFreeParEntry   *Create()                                       {return new TUFreeParEntry;}
   void                     ExtractFromFitName(string &fit_name, gENUM_AXISTYPE &sampling, gENUM_FREEPARTYPE &type) const;
   inline Double_t          GetFitInit() const                             {return fFitInit;}
   inline Double_t          GetFitInitMin() const                          {return fFitInitMin;}
   inline Double_t          GetFitInitMax() const                          {return fFitInitMax;}
   inline Double_t          GetFitInitSigma() const                        {return fFitInitSigma;}
   inline string            GetFitName(Bool_t is_add_type=true) const      {string name = fName; if (fFitSampling==kLOG) name = "log10_"+name; if (is_add_type) name = name + "_" + TUEnum::Enum2Name(fFitType); return name;}
   inline gENUM_AXISTYPE    GetFitSampling() const                         {return fFitSampling;}
   inline gENUM_FREEPARTYPE GetFitType() const                             {return fFitType;}
   inline Double_t          GetFitValBest() const                          {return fFitValBest;}
   inline Double_t          GetFitValErr() const                           {return fFitValErr;}
   inline Double_t          GetFitValErrAsymmLo() const                    {return fFitValErrAsymmLo;}
   inline Double_t          GetFitValErrAsymmUp() const                    {return fFitValErrAsymmUp;}
   inline string            GetName() const                                {return fName;}
   inline Bool_t            GetStatus() const                              {return fStatus;}
   inline string            GetUnit(Bool_t is_bracket) const               {return ((is_bracket) ? string("["+fUnit+"]"): fUnit);}
   inline Double_t          GetVal(Bool_t is_return_logval) const          {return ((is_return_logval)? log10(fVal) : fVal);}
   inline Int_t             IndexInExtClass() const                        {return fIndexInExtClass;}
   inline Bool_t            IsLowerLimitedPar() const                      {return fIsLowerLimitedPar;}
   inline Bool_t            IsUpperLimitedPar() const                      {return fIsUpperLimitedPar;}
   Bool_t                   IsSamePar(TUFreeParEntry const &par) const;
   void                     Initialise();
   string                   ParNameValUnit(Int_t uid0_base1_fit2, string const &use_name="") const;
   void                     Print(FILE *f=stdout) const;
   inline void              PrintFitInit(FILE *f) const                    {PrintFitRelated(f, 2);}
   inline void              PrintFitVals(FILE *f) const                    {PrintFitRelated(f, 1);}
   inline void              PrintFitValsAndInit(FILE *f) const             {PrintFitRelated(f, 0);}
   void                     SetFitInit(string const &fit_param_format, FILE *f_log);
   inline void              SetFitSampling(gENUM_AXISTYPE sampling)        {fFitSampling = sampling;}
   inline void              SetFitType(gENUM_FREEPARTYPE type)             {fFitType = type;}
   inline void              SetFitValBest(Double_t const &best)            {fFitValBest = best;}
   inline void              SetFitValErr(Double_t const &err)              {fFitValErr = err;}
   inline void              SetFitValErrAsymm(Double_t const &lo, Double_t const &up) {fFitValErrAsymmLo = lo; fFitValErrAsymmUp = up;}
   inline void              SetIndexInExtClass(Int_t i)                    {fIndexInExtClass = i;}
   inline void              SetPar(string const &name, string const &unit, Double_t val) {fName=name; fUnit=unit; fVal=val; fStatus=true;}
   inline void              SetStatus(Bool_t status)                       {fStatus = status;}
   inline void              SetVal(Double_t const &val, Bool_t is_set_pow10val) {if (is_set_pow10val) fVal = pow(10., val); else fVal = val; fStatus=true;}
   void                     TEST(FILE *f=stdout);
   ClassDef(TUFreeParEntry, 1)  // Free parameter entry: name, unit, value... [change at runtime]
};
#endif
