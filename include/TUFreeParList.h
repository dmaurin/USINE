// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

#ifndef USINE_TUFreeParList
#define USINE_TUFreeParList

// C/C++ include
// ROOT include
#include <TMatrixDSym.h>
#include <TVectorD.h>
// USINE include
#include "TUEnum.h"
#include "TUFreeParEntry.h"
#include "TUInitParList.h"

class TUFreeParList {
private:

   TMatrixDSym             fFitCovMatrix;   //![NCovPars][NCovPars] Covariance matrix of fit parameters
   vector<string>          fFitCovParNames; //![NCovPars] Name of params in fit covariance matrix
   vector<Bool_t>          fIsDeletePar;    //![NPars] Delete fPars[...] or not (in destructor)
   vector<TUFreeParEntry*> fPars;           //![NPars] Free pars (point to existing ones, or created)
   Bool_t                  fParListStatus;  // True whenever (at least) one fPars entry is updated

   void                    PrintFitRelated(FILE *f, Int_t all0_fitonly1_initonly2, Int_t i_start = 0, Int_t i_stop = 100000) const;

public:
   TUFreeParList();
   TUFreeParList(TUFreeParList const &free_pars);
   virtual ~TUFreeParList();

   void                    AddPar(TUFreeParEntry *par, Bool_t is_use_or_copy);
   void                    AddPar(string name, string unit, Double_t val);
   void                    AddPars(TUFreeParList *pars, Bool_t is_use_or_copy);
   Bool_t                  AddPars(TUInitParList *init_pars, string group, string subgroup, Bool_t is_verbose, FILE *f_log);
   void                    CheckMatchingParNamesAndCovParNames(FILE *f_log);
   inline TUFreeParList   *Clone()                                                            {return new TUFreeParList(*this);}
   void                    Copy(TUFreeParList const &free_pars);
   inline TUFreeParList   *Create()                                                           {return new TUFreeParList;}
   inline TVectorD         ExtractBestFitVals() const                                         {TVectorD vals(GetNPars()); for (Int_t i=0; i<GetNPars(); ++i) vals(i) = fPars[i]->GetFitValBest(); return vals;}
   inline vector<Double_t> ExtractParsValsForChi2() const                                     {vector<Double_t> vals(GetNPars()); for (Int_t i=0; i<GetNPars(); ++i) vals[i] = fPars[i]->GetVal(fPars[i]->GetFitSampling()); return vals;}
   inline TMatrixDSym      GetFitCovMatrix() const                                            {return fFitCovMatrix;}
   inline Double_t         GetFitCovMatrix(Int_t i_row, Int_t i_col) const                    {return ( (i_col>=i_row) ? fFitCovMatrix(i_row, i_col) : fFitCovMatrix(i_col, i_row));}
   inline string           GetFitCovParName(Int_t i_covpar) const                             {return fFitCovParNames[i_covpar];}
   inline vector<string>   GetFitCovParNames() const                                          {return fFitCovParNames;}
   inline Int_t            GetNCovPars() const                                                {return (Int_t)fFitCovParNames.size();}
   inline Int_t            GetNPars(gENUM_FREEPARTYPE par_type = kANY) const                  {if (par_type==kANY) return fPars.size(); else {Int_t n=0; for (Int_t i=0; i<GetNPars(); ++i) if (fPars[i]->GetFitType()==par_type) ++n; return n;}}
   inline TUFreeParEntry  *GetParEntry(Int_t i_par) const                                     {return fPars[i_par];}
   inline Bool_t           GetParListStatus() const                                           {return fParListStatus;}
   inline string           GetParNames() const                                                {return GetParNames(0,GetNPars()-1);}
   string                  GetParNames(Int_t i_start, Int_t i_stop) const;
   Int_t                   IndexPar(string const &par_name) const;
   Int_t                   IndexInExtClassToIndexPar(Int_t i_ext) const;
   vector<Int_t>           IndicesPars(gENUM_FREEPARTYPE par_type) const;
   vector<Int_t>           IndicesValUpdated() const;
   void                    Initialise(Bool_t is_delete);
   inline Bool_t           IsDeletePar(Int_t i_par) const                                     {return fIsDeletePar[i_par];}
   void                    LoadFitCovMatrix(string const &file_fitcov, vector<Int_t> const &indices, Bool_t is_verbose = true, FILE *f_log=stdout);
   void                    LoadFitValues(string const &file_fitvals, vector<Int_t> const &indices, Bool_t is_skipped_fixed, Bool_t is_verbose = true, FILE *f_log=stdout);
   string                  ParsNameValUnit(Int_t uid0_base1_fit2) const;
   void                    PrintFitCovMatrix(FILE *f) const;
   inline void             PrintFitInit(FILE *f, Int_t i_start = 0, Int_t i_stop = 100000) const        {PrintFitRelated(f, 2, i_start, i_stop);}
   inline void             PrintFitVals(FILE *f, Int_t i_start = 0, Int_t i_stop = 100000) const        {PrintFitRelated(f, 1, i_start, i_stop);}
   inline void             PrintFitValsAndInit(FILE *f, Int_t i_start = 0, Int_t i_stop = 100000) const {PrintFitRelated(f, 0, i_start, i_stop);}
   void                    PrintPars(FILE *f, gENUM_FREEPARTYPE par_type) const;
   void                    PrintPars(FILE *f = stdout, Int_t i_start = 0, Int_t i_stop = 100000) const;
   inline void             SetParListStatus(Bool_t status)                                    {fParListStatus=status;}
   inline void             SetParVal(Int_t i_par, Double_t const &val, Bool_t is_set_pow10val=false) {fPars[i_par]->SetVal(val, is_set_pow10val); fParListStatus=true;}
   inline void             ResetStatusParsAndParList()                                        {SetParListStatus(false); for (Int_t i=0;i<GetNPars();++i) fPars[i]->SetStatus(false);}
   void                    ResetToReference(TUFreeParList *pars_ref, FILE *f_print);
   void                    TEST(TUInitParList *init_pars, FILE *f=stdout);
   Bool_t                  TUI_Modify();
   inline string           UID() const                                                        {return ParsNameValUnit(0);}
   void                    UpdateParListStatusFromIndividualStatus();
   Bool_t                  UpdateParVals(TUFreeParList &pars, gENUM_FREEPARTYPE k_fittype, Bool_t is_enforce_allfound, Bool_t is_check_range, FILE *f_log);
   void                    UseParEntries(TUFreeParList *par_list);
   inline void             UseParEntry(TUFreeParEntry *par)                                   {if (!par) return; AddPar(par, true);}

   ClassDef(TUFreeParList, 1)  // Free parameter list of TUFreeParEntry [loaded from init file]
};

#endif
