// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

#ifndef USINE_TUMediumEntry
#define USINE_TUMediumEntry

// C/C++ include
#include <cstdlib>
using namespace std;
// ROOT include
#include <Rtypes.h>
// USINE include

class TUMediumEntry {
private:
   vector<Double_t>      fDensities;           //![NElements] Number density for each element in class [cm^{-3}]
   Double_t              fDensityH2;           // Number density of H2 [cm^{-3}] (enabled only if H in fDensities)
   Double_t              fDensityHI;           // Number density of HI [cm^{-3}] (enabled only if H in fDensities)
   Double_t              fDensityHII;          // Number density of HII [cm^{-3}] (enabled only if H in fDensities)
   vector<string>        fElementNames;        //![NElements] Case-insensitive element names (e.g., H, He)
   vector<Int_t>         fElementZ;            //![NElements] Z of fElementNames (elements in medium)
   Double_t              fPlasmaT;             // Plasma (electrons=HII) temperature [K]

   inline void           UpdateDensityH()                                    {Int_t i_h = IndexElementInMedium(1); if (i_h>=0)  fDensities[i_h]=fDensityHI+fDensityHII+2.*fDensityH2;}

public:
   TUMediumEntry();
   TUMediumEntry(TUMediumEntry const &medium);
   explicit TUMediumEntry(string const &commasep_elements);
   virtual ~TUMediumEntry();

   inline TUMediumEntry *Clone()                                             {return new TUMediumEntry(*this);}
   void                  Copy(TUMediumEntry const &medium);
   inline TUMediumEntry *Create()                                            {return new TUMediumEntry;}
   inline Double_t       GetDensity(Int_t i_element) const                   {return fDensities[i_element];}
   inline Double_t       GetDensityHI() const                                {return fDensityHI;}
   inline Double_t       GetDensityHII() const                               {return fDensityHII;}
   inline Double_t       GetDensityH2() const                                {return fDensityH2;}
   inline Double_t       GetDensityNeutral(Int_t i_element) const            {return ((GetZElement(i_element)==1) ? GetDensityHI() : GetDensity(i_element));};
   inline string         GetNameMediumElement(Int_t i_element) const         {return fElementNames[i_element];}
   inline Int_t          GetNMediumElements() const                          {return fElementNames.size();}
   inline Double_t       GetPlasmaDensity() const                            {return fDensityHII;}
   inline Double_t       GetPlasmaT() const                                  {return fPlasmaT;}
   inline Int_t          GetZElement(Int_t i_element) const                  {return fElementZ[i_element];}
   inline Int_t          IndexElementInMedium(Int_t z_elem) const            {for (Int_t i=0; i<GetNMediumElements();++i) {if (z_elem==GetZElement(i)) return i;} return -1;}
   void                  Initialise();
   Double_t              MeanMassDensity() const;
   void                  Print(FILE *f=stdout) const;
   inline void           ResetElements()                                     {fElementNames.clear(); fElementZ.clear();}
   inline void           SetDensity(Int_t i_element, Double_t const &dens)   {if (fElementNames[i_element]=="H") SetDensityH(dens,0.,0.); else (fDensities[i_element] = dens);}
   inline void           SetDensityH(Double_t hi, Double_t hii, Double_t h2) {fDensityHI = hi; fDensityHII = hii; fDensityH2 = h2; UpdateDensityH();}
   void                  SetElements(string const &commasep_elements);
   inline void           SetPlasmaT(Double_t const &temp)                    {fPlasmaT = temp;}
   void                  TEST(FILE *f=stdout);

   ClassDef(TUMediumEntry, 1)  // Medium entry: densities (HI, HII, H2, any element list), T...
};

#endif
