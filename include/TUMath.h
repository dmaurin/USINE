// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

#ifndef USINE_TUMath
#define USINE_TUMath

// C/C++ include
#include <cmath>
#include <numeric>
// ROOT include
#include <Rtypes.h>
#include <TMatrixD.h>
#include <TMatrixDSym.h>
#include <TMatrixDSymEigen.h>
#include <TVectorD.h>
// USINE include
#include "TUEnum.h"

namespace TUMath {

   void            Bias(vector<Double_t> const &x, vector<Double_t> &y, vector<Double_t> const &x_bias, vector<Double_t> const&y_bias, gENUM_INTERPTYPE interp_type);
   void            DrawFromMultiGauss(const TVectorD& pars_mean, const TMatrixDSym& cov_matrix, TVectorD& pars_gen);
   void            DrawFromMultiGaussAndPlot(TApplication *my_app, vector<string> par_names_units, const TVectorD &pars_mean, const TMatrixDSym &cov_matrix, Int_t n_samples, Double_t const *bound_min=NULL, Double_t const *bound_max=NULL);
   inline void     PowerRescale(Double_t const *x, Double_t *y, Int_t n, Double_t const& e_power) {if (fabs(e_power) > 1.e-3) {for (Int_t k = 0; k < n; ++k) y[k] *= pow(x[k], e_power);}}


   Int_t           BinarySearch(Int_t n, Double_t const*array, Double_t const &value);
   Int_t           BinarySearch(Int_t n, Int_t const*array, Int_t const &value);
   void            Quantiles(vector<Double_t> const &values, vector<Double_t> const &quantiles, vector<Double_t> &v_q);
   inline Double_t Cub(Double_t const &x)                              {return (x*x*x);}
   inline Int_t    Cub(Int_t const &x)                                 {return (x*x*x);}
   inline Double_t Ln2()                                               {return 0.69314718055994530941;}
   inline Double_t Pi()                                                {return 3.14159265358979323846;}
   inline Double_t Sqr(Double_t const &x)                              {return (x*x);}
   inline Int_t    Sqr(Int_t const &x)                                 {return (x*x);}

   // Interpolation (return y_interp)
   inline Double_t Derivative(Double_t const &x1,  Double_t const &x2, Double_t const &y1, Double_t const &y2)                      {return ((y2-y1)/(x2-x1));}
   Double_t        Interpolate(Double_t const &x, Double_t const &x1,  Double_t const &x2, Double_t const &y1, Double_t const &y2, gENUM_INTERPTYPE interp_type);
   Bool_t          Interpolate(Double_t const *x, Int_t n, Double_t const *y, Double_t const *x_new, Int_t n_new, Double_t *y_interp, gENUM_INTERPTYPE interp_type, Bool_t is_verbose=false, FILE *f_log=stdout);

   // Expansion or non-diverging formulations
   inline Double_t CoshToSinh(Double_t const &a, Double_t const &b)    {return ((a > 1.) ? (exp(-b)+ exp(b-2.*a))/(-expm1(-2.*a)) : (exp(2.*a-b)+exp(b))/(expm1(2.*a)));} // cosh(a-b)/sinh(a) valid for a>>1 or a<<1 cases (no approx.).
   inline Double_t Coth(Double_t const &x)                             {return ((x > 20.) ? (1.+2.*exp(-2.*x)) : (1./tanh(x)));}            // coth for large arguments
   inline Double_t InvSinh(Double_t const &x)                          {return ((x > 20.) ? (2.* exp(-x)*(1.+exp(-2 *x))) : (1./sinh(x)));} // 1/sinh(x) for large arguments.
   inline Double_t SinhC(Double_t const &x)                            {return ((x < 1.e-6) ? 1. : (sinh(x)/x));}                           // sinh(x)/x for small argument.
   inline Double_t SinhRatio(Double_t const &a, Double_t const &b)     {return ((a > 1.) ? (exp(-b)-exp(b-2.*a))/(-expm1(-2.*a)) : (exp(2.*a-b)-exp(b))/(expm1(2.*a)));}  // sinh(a-b)/sinh(a) valid fpr a>>1 or a<<1 cases (no approx.).

   // Bessel and Gamma function
   Double_t        GammaFunction_PosHalfInteger(Int_t i);
   Double_t        GammaFunction_NegHalfInteger(Int_t i);
   Double_t        J0(Double_t const &x);
   Double_t        J1(Double_t const &x);
   Double_t        J1Series(Double_t const &x);
   Double_t        J1_ZeroJ0(Int_t i, Double_t const &zeroJ0_i);
   Double_t        JpGamma(Int_t p, Double_t const &x);
   Double_t        JpIntegral(Int_t p, Double_t const &x);
   Double_t        JpOptimized(Int_t p, Double_t const &x);
   Double_t        RatioGammaFunction_HalfInteger(Int_t p, Int_t i);
   Double_t        ZeroJ0(Int_t i);
   void            TEST(FILE *f=stdout);
}
#endif
