// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

#ifndef USINE_TURunOutputs
#define USINE_TURunOutputs

// C/C++ include
#include <vector>
#include <string>
#include <utility>
using namespace std;
// ROOT include
// USINE include
#include "TUAxesCrE.h"
#include "TUDataSet.h"
#include "TUEnum.h"

class TURunOutputs {

private:
   TUAxesCrE                 *fAxesCrE;               // CR list and Energy ranges
   vector<string>             fCombos;                // [NCombos] Queried combos for which the calculation was performed
   vector<vector<string>>     fCombosRatios;          // [NCombos][NRatios] Names of ratios if combo is a ratio (e.g. B/C => 10B/C,11B/C)
   vector<string>             fEaxisNamesUnits;       // [NCombos] Name of E-type and unit for each combo
   vector<vector<Double_t>>   fEaxisVals;             // [NCombos][NE] For each combination, E values
   Double_t                   fEPower;                // TOA fluxes (except if ratio) are multiplied by E^epower
   vector<gENUM_ETYPE>        fETypes;                // [NCombos] Queried E-types (one per combo)
   TUDataSet                 *fFitData;               // Data for fit (if used)
   vector<vector<Int_t>>      fIndexExp4FitData;      // [NCombos][NExps] Indices in fFit data of experiments matching combo/etype
   vector<vector<Bool_t>>     fIsExpSolModNuis;       // [NCombos][NExps] Track whether Exp has an associated nuisance parameter
   vector<string>             fIsot;                  // [NIsot] All CR names appearing in the queried combos
   vector<vector<vector<Double_t>>> fIsotFrac;        // [NIsot][NModels][NE] Isotopic fraction (w.r.t. Z) for all CRs in combos (names_crs)
   vector<string>             fIsotAndZ;              // [NIsotAndZ] All element and CR names appearing in the queried combos
   vector<vector<vector<Double_t>>> fIsotAndZPrimFrac;// [NIsotAndZ][NModels][NE] IS fluxes primary/secondary fraction for Z and CRs n combos (names_zandcrs)
   vector<string>             fModels;                // [NModels] Names of propagation configurations
   vector<vector<Double_t>>   fphiFF;                 // [NCombos][NphiFF] Queried modulation level (several possible per combo)
   vector<Int_t>              fPropModIndexStart;     // [NPropModTitles] Storage for model param group indices
   vector<string>             fPropModTitles;         // [NPropModTitles] Storage for model param group titles
   vector<string>             fPropModParNames;       // [NPropModPars] Storage for model param names
   vector<string>             fPropModParUnits;       // [NPropModPars] Storage for model param units
   vector<Double_t>           fPropModParValues;      // [NPropModPars] Storage for model param values
   vector<vector<vector<vector<Double_t>>>> fTOACombos[3]; // [3][NCombos][NModels][NphiFF][NE] Prim/Sec/Tot TOA flux for combo
   vector<vector<vector<vector<vector<Double_t>>>>> fTOACombosRatios; // [NCombos][NRatios][NModels][NphiFF][NE] Total for broken-down quantities




   void                       CopyExpFitData(TURunOutputs const &outputs);
   void                       CopyISAndTOA(TURunOutputs const &outputs);
   void                       ResizeFluxes(FILE *f_print);
   void                       SetTOACombo(Int_t i_pst, Int_t i_combo, Int_t i_model, Int_t i_phi, Int_t ke, Double_t const& val) {fTOACombos[i_pst][i_combo][i_model][i_phi][ke] = val;}

public:
   TURunOutputs();
   TURunOutputs(TURunOutputs const &outputs);
   virtual ~TURunOutputs();

   void                       Copy(TURunOutputs const &outputs);
   void                       FillAsCLs(vector<TURunOutputs> const& sample_results, Bool_t is_median = true, Bool_t is_1sigma=true, Bool_t is_2sigma=false, Bool_t is_3sigma=false);
   string                     FormComboEtypephiff() const;
   inline TUAxesCrE          *GetAxesCrE() const                                             {return fAxesCrE;}
   inline string              GetCombo(Int_t i_combo) const                                  {return fCombos[i_combo];}
   inline string              GetComboRatio(Int_t i_combo, Int_t i_ratio) const              {return fCombosRatios[i_combo][i_ratio];}
   inline vector<string>      GetCombos() const                                              {return fCombos;}
   inline string              GetEAxisNameUnit(Int_t i_combo) const                          {return fEaxisNamesUnits[i_combo];}
   inline Double_t            GetEAxisVal(Int_t i_combo, Int_t ke) const                     {return fEaxisVals[i_combo][ke];}
   inline Double_t            GetEPower() const                                              {return fEPower;}
   inline gENUM_ETYPE         GetEType(Int_t i_combo) const                                  {return fETypes[i_combo];}
   inline TUDataSet          *GetFitData() const                                             {return fFitData;}
   inline Int_t               GetIndexExp4FitData(Int_t i_combo, Int_t i_exp) const          {return fIndexExp4FitData[i_combo][i_exp];}
   inline Bool_t              GetIsExpSolModNuis(Int_t i_combo, Int_t i_exp) const           {return fIsExpSolModNuis[i_combo][i_exp];}
   inline string              GetIsot(Int_t i_isot) const                                    {return fIsot[i_isot];}
   inline string              GetIsotAndZ(Int_t i_isotandz) const                            {return fIsotAndZ[i_isotandz];}
   inline Double_t            GetIsotAndZPrimFrac(Int_t i_isotandz, Int_t i_model, Int_t ke) const {return fIsotAndZPrimFrac[i_isotandz][i_model][ke];}
   inline Double_t            GetIsotFrac(Int_t i_isot, Int_t i_model, Int_t ke) const       {return fIsotFrac[i_isot][i_model][ke];}
   inline Double_t            GetphiFF(Int_t i_combo, Int_t i_phi) const                     {return fphiFF[i_combo][i_phi];}
   inline string              GetModel(Int_t i_model) const                                  {return fModels[i_model];}
   inline vector<string>      GetModels() const                                              {return fModels;}
   inline Int_t               GetNIndicesExp4FitData(Int_t i_combo) const                    {return (Int_t)fIndexExp4FitData[i_combo].size();}
   inline Int_t               GetNCombos() const                                             {return (Int_t)fCombos.size();}
   inline Int_t               GetNCombosRatios(Int_t i_combo) const                          {return (Int_t)fCombosRatios[i_combo].size();}
   inline Int_t               GetNE() const                                                  {return fAxesCrE->GetNE();}
   inline Int_t               GetNExp4FitData(Int_t i_combo) const                           {return (Int_t)fIndexExp4FitData[i_combo].size();}
   inline Int_t               GetNIsot() const                                               {return (Int_t)fIsot.size();}
   inline Int_t               GetNIsotAndZ() const                                           {return (Int_t)fIsotAndZ.size();}
   inline Int_t               GetNModels() const                                             {return (Int_t)fModels.size();}
   inline Int_t               GetNphiFF(Int_t i_combo) const                                 {return (Int_t)fphiFF[i_combo].size();}
   inline Int_t               GetNPropModPars() const                                        {return fPropModParNames.size();}
   inline Int_t               GetNPropModTitles() const                                      {return fPropModTitles.size();}
   inline Int_t               GetPropModIndexStart(Int_t i_propmodtitle) const               {return fPropModIndexStart[i_propmodtitle];}
   inline string              GetPropModTitles(Int_t i_propmodtitle) const                   {return fPropModTitles[i_propmodtitle];}
   inline string              GetPropModParNames(Int_t i_propmodpar) const                   {return fPropModParNames[i_propmodpar];}
   inline Double_t            GetPropModParValues(Int_t i_propmodpar) const                  {return fPropModParValues[i_propmodpar];}
   inline string              GetPropModParUnits(Int_t i_propmodpar) const                   {return fPropModParUnits[i_propmodpar];}
   inline Double_t            GetTOACombo(Int_t i_pst, Int_t i_combo, Int_t i_model, Int_t i_phi, Int_t ke) const {return fTOACombos[i_pst][i_combo][i_model][i_phi][ke];}
   inline Double_t            GetTOAComboPrim(Int_t i_combo, Int_t i_model, Int_t i_phi, Int_t ke) const {return GetTOACombo(0, i_combo, i_model, i_phi, ke);}
   inline Double_t            GetTOAComboSec(Int_t i_combo, Int_t i_model, Int_t i_phi, Int_t ke) const  {return GetTOACombo(1, i_combo, i_model, i_phi, ke);}
   inline Double_t            GetTOAComboTot(Int_t i_combo, Int_t i_model, Int_t i_phi, Int_t ke) const  {return GetTOACombo(2, i_combo, i_model, i_phi, ke);}
   inline Double_t            GetTOAComboRatio(Int_t i_combo, Int_t i_ratio, Int_t i_model, Int_t i_phi, Int_t ke) const {return fTOACombosRatios[i_combo][i_ratio][i_model][i_phi][ke];}
   void                       Initialise();
   void                       SetCombosAndResize(vector<string> const& models, TUAxesCrE *axes_cre, string const &combos_etypes_phiff, Double_t const &e_power, Bool_t is_print, FILE *f_print);
   void                       SetFitDataRelated(TUDataSet const *fit_data, vector<vector<Double_t> > const &phiff, vector<vector<Int_t> > const &list_iexp_fitdata, vector<vector<Bool_t> > const &list_isphinuis);
   void                       SetIsotAndZPrimFrac(Int_t i_isotandz, Int_t i_model, Int_t ke, Double_t const& val)     {fIsotAndZPrimFrac[i_isotandz][i_model][ke] = val;}
   void                       SetIsotFrac(Int_t i_isot, Int_t i_model, Int_t ke, Double_t const& val)                 {fIsotFrac[i_isot][i_model][ke] = val;}
   inline void                SetPropModPars(vector<string> const &titles, vector<Int_t> const &i_start, vector<string> const &names, vector<Double_t> const &values, vector<string> const &units) {fPropModIndexStart = i_start; fPropModTitles = titles; fPropModParNames = names; fPropModParValues = values; fPropModParUnits = units;}
   void                       SetTOAComboPrim(Int_t i_combo, Int_t i_model, Int_t i_phi, Int_t ke, Double_t const& val) {SetTOACombo(0, i_combo, i_model, i_phi, ke, val);}
   void                       SetTOAComboSec(Int_t i_combo, Int_t i_model, Int_t i_phi, Int_t ke, Double_t const& val) {SetTOACombo(1, i_combo, i_model, i_phi, ke, val);}
   void                       SetTOAComboTot(Int_t i_combo, Int_t i_model, Int_t i_phi, Int_t ke, Double_t const& val) {SetTOACombo(2, i_combo, i_model, i_phi, ke, val);}
   void                       SetTOAComboRatio(Int_t i_combo, Int_t i_ratio, Int_t i_model, Int_t i_phi, Int_t ke, Double_t const& val) {fTOACombosRatios[i_combo][i_ratio][i_model][i_phi][ke] = val;}

   ClassDef(TURunOutputs, 1)   // Storage container for USINE calculation outputs
};

#endif
