// @(#)USINE/include:$Id:
// Author(s): D. Maurin (1999-2019)

#ifndef USINE_TUModel0DLeakyBox
#define USINE_TUModel0DLeakyBox

// C/C++ include
// ROOT include
// USINE include
#include "TUModelBase.h"

class TUModel0DLeakyBox : public TUModelBase {

private:
   Double_t               *fATerm;               //![NCRs*NE]
   Double_t               *fCoefPreFactor;       //![NCRs*NE] Term 1/(A*Ek) in Eq.(1)  [Myr/GeV]
   gENUM_MATRIXINVERT      fInvertScheme;        // Scheme for matrix inversion (kTRID or kGAUSSJORDAN)
   Double_t               *fdNdEknPrim;          //![NCRs*NE] dN/dEkn (primary only) [#part/(m^3.GeV/n)]
   Double_t               *fdNdEknTot;           //![NCRs*NE] dN/dEkn (all contribs) [#part/(m^3.GeV/n)]
   TUMediumEntry          *fISMEntry;            // Interstellar medium for LBM (simple entry)

   Double_t                Aterm(Int_t j_cr, Int_t k_ekn);
   virtual Bool_t          CalculateChargedCRs(Int_t jcr_start, Int_t jcr_stop, Bool_t is_verbose, FILE *f_log);
   void                    FillCoefPreFactorAndATerm();
   inline Double_t        *GetATerm(Int_t j_cr) const                                           {return &fATerm[j_cr * GetNE() + 0];}
   inline Double_t         GetATerm(Int_t j_cr, Int_t k_ekn) const                              {return fATerm[j_cr * GetNE() + k_ekn];}
   inline Double_t        *GetCoefPreFactor(Int_t j_cr)                                         {return &(fCoefPreFactor[j_cr * GetNE() + 0]);}
   inline Double_t         GetCoefPreFactor(Int_t j_cr, Int_t k_ekn) const                      {return fCoefPreFactor[j_cr * GetNE() + k_ekn];}
   inline Double_t        *GetdNdEknPrim(Int_t j_cr)                                            {return &(fdNdEknPrim[j_cr * GetNE() + 0]);}
   inline Double_t         GetdNdEknPrim(Int_t j_cr, Int_t k_ekn) const                         {return fdNdEknPrim[j_cr * GetNE() + k_ekn];}
   inline Double_t        *GetdNdEknTot(Int_t j_cr)                                             {return &(fdNdEknTot[j_cr * GetNE() + 0]);}
   inline Double_t         GetdNdEknTot(Int_t j_cr, Int_t k_ekn) const                          {return fdNdEknTot[j_cr * GetNE() + k_ekn];}
   void                    Initialise(Bool_t is_delete);
   void                    IterateTertiariesdNdEkn(Int_t j_cr, Double_t* sterm_tot,  Double_t* sterm_prim, Double_t const &eps, Bool_t is_verbose, FILE *f_log);
   inline void             SetATerm(Int_t j_cr, Int_t k_ekn, Double_t const &val)               {fATerm[j_cr * GetNE() + k_ekn] = val;}
   virtual Bool_t          SetClass_ModelSpecific(TUInitParList *init_pars, Bool_t is_verbose, FILE *f_log);
   inline void             SetCoefPreFactor(Int_t j_cr, Int_t k_ekn, Double_t const &val)       {fCoefPreFactor[j_cr * GetNE() + k_ekn] = val;}
   inline void             SetdNdEknPrim(Int_t j_cr, Int_t k_ekn, Double_t const &val)          {fdNdEknPrim[j_cr * GetNE() + k_ekn] = val;}
   inline void             SetdNdEknTot(Int_t j_cr, Int_t k_ekn, Double_t const &val)           {fdNdEknTot[j_cr * GetNE() + k_ekn] = val;}

public:
   TUModel0DLeakyBox();
   virtual ~TUModel0DLeakyBox();

   virtual Double_t        CoeffReac1stOrder(Int_t j_cr, Int_t k_ekn);
   virtual Double_t        CoeffReac2ndOrderLowerHalfBin(Int_t j_cr, Int_t k_ekn);
   virtual Double_t        EvalFluxPrim(Int_t j_cr, Int_t k_ekn, TUCoordTXYZ *coord_txyz=NULL);
   virtual Double_t        EvalFluxTot(Int_t j_cr, Int_t k_ekn, TUCoordTXYZ *coord_txyz=NULL);
   virtual void            InitialiseForCalculation(Bool_t is_init_or_update, Bool_t is_force_update, Bool_t is_verbose, FILE *f_log);
   void                    TEST(TUInitParList *init_pars, FILE *f=stdout);
   Double_t                ValuePseudoKpp(Int_t j_cr, Int_t k_ekn, Int_t halfbin_status=0);
   Double_t                ValueTauEsc_Myr(Int_t j_cr, Int_t k_ekn, Int_t halfbin_status=0);

   ClassDef(TUModel0DLeakyBox, 1)  // Propagation model 0D: Leaky Box
};

#endif
