
            -------------------------
              TUValsTXYZEGrid::TEST
            -------------------------

- Class content: Container for space-time+E dependent quantities on a grid

   > AllocateVals(ne=3, nt=0, nx=4, ny=2, nz=0);
   > GetNE();                => 3
   > GetNT();                => 0
   > GetNX();                => 4
   > GetNY();                => 2
   > GetNZ();                => 0
   > GetNDimXYZ();           => 2
   > GetNCellsTXYZ();        => 8
   > GetNCells();            => 24
   > IsE();                  => 1
   > ValsTXYZEFormat();=> 1
   > IsT();                  => 0
   > IsX();                  => 1
   > IsY();                  => 1
   > IsZ();                  => 0

 * Test private methods GetBinETXYZ() and GetBinGlobal() is involutive:
     glob_bin=  0: GetBinETXYZ()=> bin_e=0  (t,x,y,z)_bin=(0,0,0,0)   GetBinGlobal()=> bin_glob=  0
     glob_bin=  1: GetBinETXYZ()=> bin_e=1  (t,x,y,z)_bin=(0,0,0,0)   GetBinGlobal()=> bin_glob=  1
     glob_bin=  2: GetBinETXYZ()=> bin_e=2  (t,x,y,z)_bin=(0,0,0,0)   GetBinGlobal()=> bin_glob=  2
     glob_bin=  3: GetBinETXYZ()=> bin_e=0  (t,x,y,z)_bin=(0,1,0,0)   GetBinGlobal()=> bin_glob=  3
     glob_bin=  4: GetBinETXYZ()=> bin_e=1  (t,x,y,z)_bin=(0,1,0,0)   GetBinGlobal()=> bin_glob=  4
     glob_bin=  5: GetBinETXYZ()=> bin_e=2  (t,x,y,z)_bin=(0,1,0,0)   GetBinGlobal()=> bin_glob=  5
     glob_bin=  6: GetBinETXYZ()=> bin_e=0  (t,x,y,z)_bin=(0,2,0,0)   GetBinGlobal()=> bin_glob=  6
     glob_bin=  7: GetBinETXYZ()=> bin_e=1  (t,x,y,z)_bin=(0,2,0,0)   GetBinGlobal()=> bin_glob=  7
     glob_bin=  8: GetBinETXYZ()=> bin_e=2  (t,x,y,z)_bin=(0,2,0,0)   GetBinGlobal()=> bin_glob=  8
     glob_bin=  9: GetBinETXYZ()=> bin_e=0  (t,x,y,z)_bin=(0,3,0,0)   GetBinGlobal()=> bin_glob=  9
     glob_bin= 10: GetBinETXYZ()=> bin_e=1  (t,x,y,z)_bin=(0,3,0,0)   GetBinGlobal()=> bin_glob= 10
     glob_bin= 11: GetBinETXYZ()=> bin_e=2  (t,x,y,z)_bin=(0,3,0,0)   GetBinGlobal()=> bin_glob= 11
     glob_bin= 12: GetBinETXYZ()=> bin_e=0  (t,x,y,z)_bin=(0,0,1,0)   GetBinGlobal()=> bin_glob= 12
     glob_bin= 13: GetBinETXYZ()=> bin_e=1  (t,x,y,z)_bin=(0,0,1,0)   GetBinGlobal()=> bin_glob= 13
     glob_bin= 14: GetBinETXYZ()=> bin_e=2  (t,x,y,z)_bin=(0,0,1,0)   GetBinGlobal()=> bin_glob= 14
     glob_bin= 15: GetBinETXYZ()=> bin_e=0  (t,x,y,z)_bin=(0,1,1,0)   GetBinGlobal()=> bin_glob= 15
     glob_bin= 16: GetBinETXYZ()=> bin_e=1  (t,x,y,z)_bin=(0,1,1,0)   GetBinGlobal()=> bin_glob= 16
     glob_bin= 17: GetBinETXYZ()=> bin_e=2  (t,x,y,z)_bin=(0,1,1,0)   GetBinGlobal()=> bin_glob= 17
     glob_bin= 18: GetBinETXYZ()=> bin_e=0  (t,x,y,z)_bin=(0,2,1,0)   GetBinGlobal()=> bin_glob= 18
     glob_bin= 19: GetBinETXYZ()=> bin_e=1  (t,x,y,z)_bin=(0,2,1,0)   GetBinGlobal()=> bin_glob= 19
     glob_bin= 20: GetBinETXYZ()=> bin_e=2  (t,x,y,z)_bin=(0,2,1,0)   GetBinGlobal()=> bin_glob= 20
     glob_bin= 21: GetBinETXYZ()=> bin_e=0  (t,x,y,z)_bin=(0,3,1,0)   GetBinGlobal()=> bin_glob= 21
     glob_bin= 22: GetBinETXYZ()=> bin_e=1  (t,x,y,z)_bin=(0,3,1,0)   GetBinGlobal()=> bin_glob= 22
     glob_bin= 23: GetBinETXYZ()=> bin_e=2  (t,x,y,z)_bin=(0,3,1,0)   GetBinGlobal()=> bin_glob= 23

 * Set and print 'array' methods
   > TUCoordTXYZ coords_xy;      [create generic bin coord for txyz]
   > GetBinGlobal(bine=0, bins_xy=(2,0));   => 6
   > PrintVals(f, coords_xy.GetBins());
     bin_e=0   2,0   val=0.000000e+00
     bin_e=1   2,0   val=0.000000e+00
     bin_e=2   2,0   val=0.000000e+00

   > SetVal(val=10,bine=1, bins_xy);
   > PrintVals(f, coords_xy.GetBins());
     bin_e=0   2,0   val=0.000000e+00
     bin_e=1   2,0   val=1.000000e+01
     bin_e=2   2,0   val=0.000000e+00

   > SetVals(val=2);
   > PrintVals(f, coords_xy.GetBins());
     bin_e=0   2,0   val=2.000000e+00
     bin_e=1   2,0   val=2.000000e+00
     bin_e=2   2,0   val=2.000000e+00


 * Copy, add, and multiply 'array' methods
   > TUValsTXYZ *array = new TUValsTXYZ();
   > array->Copy(*this);
   > array->PrintVals(f, coords_xy.GetBins());
     bin_e=0   2,0   val=2.000000e+00
     bin_e=1   2,0   val=2.000000e+00
     bin_e=2   2,0   val=2.000000e+00
   > IsSameFormat(array);
   > array->SetVals(0.5);
   > array->PrintVals(f, coords_xy.GetBins());
     bin_e=0   2,0   val=5.000000e-01
     bin_e=1   2,0   val=5.000000e-01
     bin_e=2   2,0   val=5.000000e-01

   > PrintVals(f, coords_xy.GetBins());
     bin_e=0   2,0   val=2.000000e+00
     bin_e=1   2,0   val=2.000000e+00
     bin_e=2   2,0   val=2.000000e+00
   > AddVals(array);
   > PrintVals(f, coords_xy.GetBins());
     bin_e=0   2,0   val=2.500000e+00
     bin_e=1   2,0   val=2.500000e+00
     bin_e=2   2,0   val=2.500000e+00
   > Subtract(array);
   > PrintVals(f, coords_xy.GetBins());
     bin_e=0   2,0   val=2.000000e+00
     bin_e=1   2,0   val=2.000000e+00
     bin_e=2   2,0   val=2.000000e+00

   > AddVals(array);
   > PrintVals(f);
     bin_e=0   0,0,0,0   val=2.500000e+00
     bin_e=1   0,0,0,0   val=2.500000e+00
     bin_e=2   0,0,0,0   val=2.500000e+00
     bin_e=0   0,1,0,0   val=2.500000e+00
     bin_e=1   0,1,0,0   val=2.500000e+00
     bin_e=2   0,1,0,0   val=2.500000e+00
     bin_e=0   0,2,0,0   val=2.500000e+00
     bin_e=1   0,2,0,0   val=2.500000e+00
     bin_e=2   0,2,0,0   val=2.500000e+00
     bin_e=0   0,3,0,0   val=2.500000e+00
     bin_e=1   0,3,0,0   val=2.500000e+00
     bin_e=2   0,3,0,0   val=2.500000e+00
     bin_e=0   0,0,1,0   val=2.500000e+00
     bin_e=1   0,0,1,0   val=2.500000e+00
     bin_e=2   0,0,1,0   val=2.500000e+00
     bin_e=0   0,1,1,0   val=2.500000e+00
     bin_e=1   0,1,1,0   val=2.500000e+00
     bin_e=2   0,1,1,0   val=2.500000e+00
     bin_e=0   0,2,1,0   val=2.500000e+00
     bin_e=1   0,2,1,0   val=2.500000e+00
     bin_e=2   0,2,1,0   val=2.500000e+00
     bin_e=0   0,3,1,0   val=2.500000e+00
     bin_e=1   0,3,1,0   val=2.500000e+00
     bin_e=2   0,3,1,0   val=2.500000e+00
   > SubtractVals(array);
   > PrintVals(f);
     bin_e=0   0,0,0,0   val=2.000000e+00
     bin_e=1   0,0,0,0   val=2.000000e+00
     bin_e=2   0,0,0,0   val=2.000000e+00
     bin_e=0   0,1,0,0   val=2.000000e+00
     bin_e=1   0,1,0,0   val=2.000000e+00
     bin_e=2   0,1,0,0   val=2.000000e+00
     bin_e=0   0,2,0,0   val=2.000000e+00
     bin_e=1   0,2,0,0   val=2.000000e+00
     bin_e=2   0,2,0,0   val=2.000000e+00
     bin_e=0   0,3,0,0   val=2.000000e+00
     bin_e=1   0,3,0,0   val=2.000000e+00
     bin_e=2   0,3,0,0   val=2.000000e+00
     bin_e=0   0,0,1,0   val=2.000000e+00
     bin_e=1   0,0,1,0   val=2.000000e+00
     bin_e=2   0,0,1,0   val=2.000000e+00
     bin_e=0   0,1,1,0   val=2.000000e+00
     bin_e=1   0,1,1,0   val=2.000000e+00
     bin_e=2   0,1,1,0   val=2.000000e+00
     bin_e=0   0,2,1,0   val=2.000000e+00
     bin_e=1   0,2,1,0   val=2.000000e+00
     bin_e=2   0,2,1,0   val=2.000000e+00
     bin_e=0   0,3,1,0   val=2.000000e+00
     bin_e=1   0,3,1,0   val=2.000000e+00
     bin_e=2   0,3,1,0   val=2.000000e+00

   > MultiplyVals(factor=10);
   > PrintVals(f);
     bin_e=0   0,0,0,0   val=2.000000e+01
     bin_e=1   0,0,0,0   val=2.000000e+01
     bin_e=2   0,0,0,0   val=2.000000e+01
     bin_e=0   0,1,0,0   val=2.000000e+01
     bin_e=1   0,1,0,0   val=2.000000e+01
     bin_e=2   0,1,0,0   val=2.000000e+01
     bin_e=0   0,2,0,0   val=2.000000e+01
     bin_e=1   0,2,0,0   val=2.000000e+01
     bin_e=2   0,2,0,0   val=2.000000e+01
     bin_e=0   0,3,0,0   val=2.000000e+01
     bin_e=1   0,3,0,0   val=2.000000e+01
     bin_e=2   0,3,0,0   val=2.000000e+01
     bin_e=0   0,0,1,0   val=2.000000e+01
     bin_e=1   0,0,1,0   val=2.000000e+01
     bin_e=2   0,0,1,0   val=2.000000e+01
     bin_e=0   0,1,1,0   val=2.000000e+01
     bin_e=1   0,1,1,0   val=2.000000e+01
     bin_e=2   0,1,1,0   val=2.000000e+01
     bin_e=0   0,2,1,0   val=2.000000e+01
     bin_e=1   0,2,1,0   val=2.000000e+01
     bin_e=2   0,2,1,0   val=2.000000e+01
     bin_e=0   0,3,1,0   val=2.000000e+01
     bin_e=1   0,3,1,0   val=2.000000e+01
     bin_e=2   0,3,1,0   val=2.000000e+01

   > MultiplyVals(factor=3.);
   > PrintVals(f);
     bin_e=0   0,0,0,0   val=6.000000e+01
     bin_e=1   0,0,0,0   val=6.000000e+01
     bin_e=2   0,0,0,0   val=6.000000e+01
     bin_e=0   0,1,0,0   val=6.000000e+01
     bin_e=1   0,1,0,0   val=6.000000e+01
     bin_e=2   0,1,0,0   val=6.000000e+01
     bin_e=0   0,2,0,0   val=6.000000e+01
     bin_e=1   0,2,0,0   val=6.000000e+01
     bin_e=2   0,2,0,0   val=6.000000e+01
     bin_e=0   0,3,0,0   val=6.000000e+01
     bin_e=1   0,3,0,0   val=6.000000e+01
     bin_e=2   0,3,0,0   val=6.000000e+01
     bin_e=0   0,0,1,0   val=6.000000e+01
     bin_e=1   0,0,1,0   val=6.000000e+01
     bin_e=2   0,0,1,0   val=6.000000e+01
     bin_e=0   0,1,1,0   val=6.000000e+01
     bin_e=1   0,1,1,0   val=6.000000e+01
     bin_e=2   0,1,1,0   val=6.000000e+01
     bin_e=0   0,2,1,0   val=6.000000e+01
     bin_e=1   0,2,1,0   val=6.000000e+01
     bin_e=2   0,2,1,0   val=6.000000e+01
     bin_e=0   0,3,1,0   val=6.000000e+01
     bin_e=1   0,3,1,0   val=6.000000e+01
     bin_e=2   0,3,1,0   val=6.000000e+01


 * Set values E (no spacetime) from vector (reinitialise first):
   > Initialise(is_delete=true);
   > TUAxesCrE *axes_cre = new TUAxesCrE();
   > axes_cre->SetClass(init_pars, f_log=f);
   > AllocateVals(axes_cre->GetNE());
   > SetValsE(axes_cre, bincr=10, vals from pow(Ekn,-2);
   > axes_cre->GetE(bincr)->PrintSummary(f);
      Ekn   [GeV/n]   : 33 bins in [5.000e-02,5.000e+03] (log-step=1.4330e+00)
   > PrintVals(f, bins_txyz=NULL);
     bin_e=0   [no-spacetime]   val=4.000000e+02
     bin_e=1   [no-spacetime]   val=1.947870e+02
     bin_e=2   [no-spacetime]   val=9.485495e+01
     bin_e=3   [no-spacetime]   val=4.619128e+01
     bin_e=4   [no-spacetime]   val=2.249365e+01
     bin_e=5   [no-spacetime]   val=1.095368e+01
     bin_e=6   [no-spacetime]   val=5.334086e+00
     bin_e=7   [no-spacetime]   val=2.597527e+00
     bin_e=8   [no-spacetime]   val=1.264911e+00
     bin_e=9   [no-spacetime]   val=6.159706e-01
     bin_e=10   [no-spacetime]   val=2.999577e-01
     bin_e=11   [no-spacetime]   val=1.460697e-01
     bin_e=12   [no-spacetime]   val=7.113118e-02
     bin_e=13   [no-spacetime]   val=3.463857e-02
     bin_e=14   [no-spacetime]   val=1.686786e-02
     bin_e=15   [no-spacetime]   val=8.214100e-03
     bin_e=16   [no-spacetime]   val=4.000000e-03
     bin_e=17   [no-spacetime]   val=1.947870e-03
     bin_e=18   [no-spacetime]   val=9.485495e-04
     bin_e=19   [no-spacetime]   val=4.619128e-04
     bin_e=20   [no-spacetime]   val=2.249365e-04
     bin_e=21   [no-spacetime]   val=1.095368e-04
     bin_e=22   [no-spacetime]   val=5.334086e-05
     bin_e=23   [no-spacetime]   val=2.597527e-05
     bin_e=24   [no-spacetime]   val=1.264911e-05
     bin_e=25   [no-spacetime]   val=6.159706e-06
     bin_e=26   [no-spacetime]   val=2.999577e-06
     bin_e=27   [no-spacetime]   val=1.460697e-06
     bin_e=28   [no-spacetime]   val=7.113118e-07
     bin_e=29   [no-spacetime]   val=3.463857e-07
     bin_e=30   [no-spacetime]   val=1.686786e-07
     bin_e=31   [no-spacetime]   val=8.214100e-08
     bin_e=32   [no-spacetime]   val=4.000000e-08
 * Clone method
   > TUValsTXYZEGrid *vals2 = Clone();
   > vals2->PrintValsTXYZ(f);
 => Values
     (t,x,y,z)_bin=(0,0,0,0)   val=4.000000e+02
     (t,x,y,z)_bin=(0,0,0,0)   val=4.000000e+02
     (t,x,y,z)_bin=(0,0,0,0)   val=4.000000e+02
     (t,x,y,z)_bin=(0,0,0,0)   val=4.000000e+02
     (t,x,y,z)_bin=(0,0,0,0)   val=4.000000e+02
     (t,x,y,z)_bin=(0,0,0,0)   val=4.000000e+02
     (t,x,y,z)_bin=(0,0,0,0)   val=4.000000e+02
     (t,x,y,z)_bin=(0,0,0,0)   val=4.000000e+02
     (t,x,y,z)_bin=(0,0,0,0)   val=4.000000e+02
     (t,x,y,z)_bin=(0,0,0,0)   val=4.000000e+02
     (t,x,y,z)_bin=(0,0,0,0)   val=4.000000e+02
     (t,x,y,z)_bin=(0,0,0,0)   val=4.000000e+02
     (t,x,y,z)_bin=(0,0,0,0)   val=4.000000e+02
     (t,x,y,z)_bin=(0,0,0,0)   val=4.000000e+02
     (t,x,y,z)_bin=(0,0,0,0)   val=4.000000e+02
     (t,x,y,z)_bin=(0,0,0,0)   val=4.000000e+02
     (t,x,y,z)_bin=(0,0,0,0)   val=4.000000e+02
     (t,x,y,z)_bin=(0,0,0,0)   val=4.000000e+02
     (t,x,y,z)_bin=(0,0,0,0)   val=4.000000e+02
     (t,x,y,z)_bin=(0,0,0,0)   val=4.000000e+02
     (t,x,y,z)_bin=(0,0,0,0)   val=4.000000e+02
     (t,x,y,z)_bin=(0,0,0,0)   val=4.000000e+02
     (t,x,y,z)_bin=(0,0,0,0)   val=4.000000e+02
     (t,x,y,z)_bin=(0,0,0,0)   val=4.000000e+02
     (t,x,y,z)_bin=(0,0,0,0)   val=4.000000e+02
     (t,x,y,z)_bin=(0,0,0,0)   val=4.000000e+02
     (t,x,y,z)_bin=(0,0,0,0)   val=4.000000e+02
     (t,x,y,z)_bin=(0,0,0,0)   val=4.000000e+02
     (t,x,y,z)_bin=(0,0,0,0)   val=4.000000e+02
     (t,x,y,z)_bin=(0,0,0,0)   val=4.000000e+02
     (t,x,y,z)_bin=(0,0,0,0)   val=4.000000e+02
     (t,x,y,z)_bin=(0,0,0,0)   val=4.000000e+02
     (t,x,y,z)_bin=(0,0,0,0)   val=4.000000e+02

