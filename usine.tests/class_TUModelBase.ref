
              ---------------------
                TUModelBase::TEST
              ---------------------

- Class content: Load model base ingredients and test.

 * Load base and check prints
   > SetClass(init_pars="inputs/init.TEST.par", BASE, false, f_log=f);
 * Check Print() methods
   > PrintTargetList(f)
     -- Targets loaded: H,HE
   > PrintTertiaryList(f)
     -- Tertiaries loaded: 1H-BAR,2H-BAR
   > PrintXSecFiles(f)
     -- Cross section files used:
        - $USINE/inputs/XS_NUCLEI/sigInelTripathi99+Coste12.dat
        - $USINE/inputs/XS_ANTINUC/sigInelANN_pbar+HHe_Donato01.dat
        - $USINE/inputs/XS_ANTINUC/sigInelANN_dbar+HHe_Duperray05.dat
        - $USINE/inputs/XS_NUCLEI/sigProdGALPROP17_OPT12.dat
        - $USINE/inputs/XS_ANTINUC/dSdEProd_pbar_1H4He+HHe_Donato01.dat
        - $USINE/inputs/XS_ANTINUC/dSdEProd_dbar_1H4He+HHe_Duperray05_Coal79MeV.dat
        - $USINE/inputs/XS_ANTINUC/dSdEProd_dbar_pbar+HHe_Duperray05_Coal79MeV.dat
        - $USINE/inputs/XS_ANTINUC/sigInelNONANN_pbar+HHe_Donato01.dat
        - $USINE/inputs/XS_ANTINUC/sigInelNONANN_dbar+HHe_Duperray05.dat
        - $USINE/inputs/XS_ANTINUC/dSdENAR_pbar+HHe_Duperray05_Anderson.dat
        - $USINE/inputs/XS_ANTINUC/dSdENAR_dbar+HHe_Duperray05_Anderson.dat


   > PrintSummaryBase(f)

             ----------------------
               Nuclei and parents
             ----------------------

     --   0:    2H-BAR (parents:   1H-BAR,1H,4HE)
     --   1:    1H-BAR (parents:       1H,4HE)
     --   2:        1H (parents:      2H -> 30SI)
     --   3:        2H (parents:     3HE -> 30SI)
     --   4:       3HE (parents:     4HE -> 30SI)
     --   5:       4HE (parents:     6LI -> 30SI)
     --   6:       6LI (parents:     7LI -> 30SI)
     --   7:       7LI (parents:     7BE -> 30SI)
     --   8:       7BE (parents:     9BE -> 30SI)
     --   9:       9BE (parents:     10B -> 30SI)
     --  10:       10B (parents:    10BE -> 30SI)
     --  11:      10BE (parents:     11B -> 30SI)
     --  12:       11B (parents:     12C -> 30SI)
     --  13:       12C (parents:     13C -> 30SI)
     --  14:       13C (parents:     14N -> 30SI)
     --  15:       14N (parents:     14C -> 30SI)
     --  16:       14C (parents:     15N -> 30SI)
     --  17:       15N (parents:     16O -> 30SI)
     --  18:       16O (parents:     17O -> 30SI)
     --  19:       17O (parents:     18O -> 30SI)
     --  20:       18O (parents:     19F -> 30SI)
     --  21:       19F (parents:    20NE -> 30SI)
     --  22:      20NE (parents:    21NE -> 30SI)
     --  23:      21NE (parents:    22NE -> 30SI)
     --  24:      22NE (parents:    23NA -> 30SI)
     --  25:      23NA (parents:    24MG -> 30SI)
     --  26:      24MG (parents:    25MG -> 30SI)
     --  27:      25MG (parents:    26MG -> 30SI)
     --  28:      26MG (parents:    26AL -> 30SI)
     --  29:      26AL (parents:    27AL -> 30SI)
     --  30:      27AL (parents:    28SI -> 30SI)
     --  31:      28SI (parents:    29SI -> 30SI)
     --  32:      29SI (only one parent:    30SI)
     --  33:      30SI (primary - no parents)

             -----------------------
               Cosmic Ray energies
             -----------------------

      => Nuclei                  Ekn   [GeV/n]   : 33 bins in [5.000e-02,5.000e+03] (log-step=1.4330e+00)
      => Anti-nuclei             Ekn   [GeV/n]   : 33 bins in [5.000e-02,1.000e+02] (log-step=1.2681e+00)
      => E-dependent variables: beta,gamma,p,Rig,Ek,Ekn,Etot  (7 E-variables)

                   -----------
                     CR Data
                   -----------

   Data read from:
    - $USINE/inputs/crdata_crdb20170523.dat
    - $USINE/inputs/crdata_dummy.dat

   -------------------------------------------
     Data/energy used to normalise CR fluxes
   -------------------------------------------

       Qty       Sub-exps      <Ekn>
         H        PAMELA          2.000000e+01 [GeV/n]
         HE       PAMELA          2.000000e+01 [GeV/n]
         C        HEAO            1.060000e+01 [GeV/n]
         N        HEAO            1.060000e+01 [GeV/n]
         O        HEAO            1.060000e+01 [GeV/n]
         F        HEAO            1.060000e+01 [GeV/n]
         NE       HEAO            1.060000e+01 [GeV/n]
         NA       HEAO            1.060000e+01 [GeV/n]
         MG       HEAO            1.060000e+01 [GeV/n]
         AL       HEAO            1.060000e+01 [GeV/n]
         SI       HEAO            1.060000e+01 [GeV/n]
         P        HEAO            1.060000e+01 [GeV/n]
         S        HEAO            1.060000e+01 [GeV/n]
         CL       HEAO            1.060000e+01 [GeV/n]
         AR       HEAO            1.060000e+01 [GeV/n]
         K        HEAO            1.060000e+01 [GeV/n]
         CA       HEAO            1.060000e+01 [GeV/n]
         SC       HEAO            1.060000e+01 [GeV/n]
         TI       HEAO            1.060000e+01 [GeV/n]
         V        HEAO            1.060000e+01 [GeV/n]
         CR       HEAO            1.060000e+01 [GeV/n]
         MN       HEAO            1.060000e+01 [GeV/n]
         FE       HEAO            1.060000e+01 [GeV/n]
         NI       HEAO            1.060000e+01 [GeV/n]

              --------------------
                Source templates
              --------------------

       - Energy spectrum template: POWERLAW
          [formula] = q*beta^(eta_s)*Rig^(-alpha)
          - base variables: t,x,y,z,beta,gamma,p,Rig,Ek,Ekn,Etot
          - user-defined variables: q,alpha,eta_s
       - Energy spectrum template: TEST
          [formula] = q*beta^(eta_s)*Rig^(-alpha)
          - base variables: t,x,y,z,beta,gamma,p,Rig,Ek,Ekn,Etot
          - user-defined variables: q,alpha,eta_s
       - Spatial distribution template: CST
          [formula] = 1
          - base variables: t,x,y,z
          - user-defined variables: NONE
       - Spatial distribution template: DOOR
          [formula] = if((x>rup),0,if((x<rlo),0,1))
          - base variables: t,x,y,z
          - user-defined variables: rlo,rup
       - Spatial distribution template: STEPUP
          [formula] = if((x>rup),1,0)
          - base variables: t,x,y,z
          - user-defined variables: rup
       - Spatial distribution template: STEPDOWN
          [formula] = if((x<rlo),1,0)
          - base variables: t,x,y,z
          - user-defined variables: rlo
       - Spatial distribution template: CASEBHATTA96
          [formula] = pow(x/8.5,2.)*exp(-3.53*(x-8.5)/8.5)
          - base variables: t,x,y,z
          - user-defined variables: NONE
       - Spatial distribution template: SNRMODIFIED
          [formula] = pow(10.,dex*(x-rsol))*pow((x/rsol),a)*exp(b*(x-rsol)/rsol)
          - base variables: t,x,y,z
          - user-defined variables: rsol,a,b,dex
       - Spatial distribution template: STECKERJONES77
          [formula] = pow(x/10.,0.6)*exp(-3.*(x-10.)/10.)
          - base variables: t,x,y,z
          - user-defined variables: NONE
       - Spatial distribution template: STRONGMOSKA99
          [formula] = pow(x/8.5,0.5)*exp(-1.*(x-8.5)/8.5)
          - base variables: t,x,y,z
          - user-defined variables: NONE
       - Spatial distribution template: EINASTO_CYL
          [formula] = r_sp:=x*x+y*y;rhos*exp((-2/alpha)*(pow((r_sp/rs),alpha)-1))
          - base variables: t,x,y,z
          - user-defined variables: rhos,rs,alpha
       - Spatial distribution template: DUMMY
          [formula] = exp(-(x*x+y*y+z*z/8.))
          - base variables: t,x,y,z
          - user-defined variables: NONE

               ------------------
                 Cross sections
               ------------------

     -- Cross section files used:
        - $USINE/inputs/XS_NUCLEI/sigInelTripathi99+Coste12.dat
        - $USINE/inputs/XS_ANTINUC/sigInelANN_pbar+HHe_Donato01.dat
        - $USINE/inputs/XS_ANTINUC/sigInelANN_dbar+HHe_Duperray05.dat
        - $USINE/inputs/XS_NUCLEI/sigProdGALPROP17_OPT12.dat
        - $USINE/inputs/XS_ANTINUC/dSdEProd_pbar_1H4He+HHe_Donato01.dat
        - $USINE/inputs/XS_ANTINUC/dSdEProd_dbar_1H4He+HHe_Duperray05_Coal79MeV.dat
        - $USINE/inputs/XS_ANTINUC/dSdEProd_dbar_pbar+HHe_Duperray05_Coal79MeV.dat
        - $USINE/inputs/XS_ANTINUC/sigInelNONANN_pbar+HHe_Donato01.dat
        - $USINE/inputs/XS_ANTINUC/sigInelNONANN_dbar+HHe_Duperray05.dat
        - $USINE/inputs/XS_ANTINUC/dSdENAR_pbar+HHe_Duperray05_Anderson.dat
        - $USINE/inputs/XS_ANTINUC/dSdENAR_dbar+HHe_Duperray05_Anderson.dat
     -- Targets loaded: H,HE
     -- Tertiaries loaded: 1H-BAR,2H-BAR
   > PrintPars(f)
      => No free parameters
   > PrintSummary(f)

             ----------------------
               Nuclei and parents
             ----------------------

     --   0:    2H-BAR (parents:   1H-BAR,1H,4HE)
     --   1:    1H-BAR (parents:       1H,4HE)
     --   2:        1H (parents:      2H -> 30SI)
     --   3:        2H (parents:     3HE -> 30SI)
     --   4:       3HE (parents:     4HE -> 30SI)
     --   5:       4HE (parents:     6LI -> 30SI)
     --   6:       6LI (parents:     7LI -> 30SI)
     --   7:       7LI (parents:     7BE -> 30SI)
     --   8:       7BE (parents:     9BE -> 30SI)
     --   9:       9BE (parents:     10B -> 30SI)
     --  10:       10B (parents:    10BE -> 30SI)
     --  11:      10BE (parents:     11B -> 30SI)
     --  12:       11B (parents:     12C -> 30SI)
     --  13:       12C (parents:     13C -> 30SI)
     --  14:       13C (parents:     14N -> 30SI)
     --  15:       14N (parents:     14C -> 30SI)
     --  16:       14C (parents:     15N -> 30SI)
     --  17:       15N (parents:     16O -> 30SI)
     --  18:       16O (parents:     17O -> 30SI)
     --  19:       17O (parents:     18O -> 30SI)
     --  20:       18O (parents:     19F -> 30SI)
     --  21:       19F (parents:    20NE -> 30SI)
     --  22:      20NE (parents:    21NE -> 30SI)
     --  23:      21NE (parents:    22NE -> 30SI)
     --  24:      22NE (parents:    23NA -> 30SI)
     --  25:      23NA (parents:    24MG -> 30SI)
     --  26:      24MG (parents:    25MG -> 30SI)
     --  27:      25MG (parents:    26MG -> 30SI)
     --  28:      26MG (parents:    26AL -> 30SI)
     --  29:      26AL (parents:    27AL -> 30SI)
     --  30:      27AL (parents:    28SI -> 30SI)
     --  31:      28SI (parents:    29SI -> 30SI)
     --  32:      29SI (only one parent:    30SI)
     --  33:      30SI (primary - no parents)

             -----------------------
               Cosmic Ray energies
             -----------------------

      => Nuclei                  Ekn   [GeV/n]   : 33 bins in [5.000e-02,5.000e+03] (log-step=1.4330e+00)
      => Anti-nuclei             Ekn   [GeV/n]   : 33 bins in [5.000e-02,1.000e+02] (log-step=1.2681e+00)
      => E-dependent variables: beta,gamma,p,Rig,Ek,Ekn,Etot  (7 E-variables)

                   -----------
                     CR Data
                   -----------

   Data read from:
    - $USINE/inputs/crdata_crdb20170523.dat
    - $USINE/inputs/crdata_dummy.dat

   -------------------------------------------
     Data/energy used to normalise CR fluxes
   -------------------------------------------

       Qty       Sub-exps      <Ekn>
         H        PAMELA          2.000000e+01 [GeV/n]
         HE       PAMELA          2.000000e+01 [GeV/n]
         C        HEAO            1.060000e+01 [GeV/n]
         N        HEAO            1.060000e+01 [GeV/n]
         O        HEAO            1.060000e+01 [GeV/n]
         F        HEAO            1.060000e+01 [GeV/n]
         NE       HEAO            1.060000e+01 [GeV/n]
         NA       HEAO            1.060000e+01 [GeV/n]
         MG       HEAO            1.060000e+01 [GeV/n]
         AL       HEAO            1.060000e+01 [GeV/n]
         SI       HEAO            1.060000e+01 [GeV/n]
         P        HEAO            1.060000e+01 [GeV/n]
         S        HEAO            1.060000e+01 [GeV/n]
         CL       HEAO            1.060000e+01 [GeV/n]
         AR       HEAO            1.060000e+01 [GeV/n]
         K        HEAO            1.060000e+01 [GeV/n]
         CA       HEAO            1.060000e+01 [GeV/n]
         SC       HEAO            1.060000e+01 [GeV/n]
         TI       HEAO            1.060000e+01 [GeV/n]
         V        HEAO            1.060000e+01 [GeV/n]
         CR       HEAO            1.060000e+01 [GeV/n]
         MN       HEAO            1.060000e+01 [GeV/n]
         FE       HEAO            1.060000e+01 [GeV/n]
         NI       HEAO            1.060000e+01 [GeV/n]

              --------------------
                Source templates
              --------------------

       - Energy spectrum template: POWERLAW
          [formula] = q*beta^(eta_s)*Rig^(-alpha)
          - base variables: t,x,y,z,beta,gamma,p,Rig,Ek,Ekn,Etot
          - user-defined variables: q,alpha,eta_s
       - Energy spectrum template: TEST
          [formula] = q*beta^(eta_s)*Rig^(-alpha)
          - base variables: t,x,y,z,beta,gamma,p,Rig,Ek,Ekn,Etot
          - user-defined variables: q,alpha,eta_s
       - Spatial distribution template: CST
          [formula] = 1
          - base variables: t,x,y,z
          - user-defined variables: NONE
       - Spatial distribution template: DOOR
          [formula] = if((x>rup),0,if((x<rlo),0,1))
          - base variables: t,x,y,z
          - user-defined variables: rlo,rup
       - Spatial distribution template: STEPUP
          [formula] = if((x>rup),1,0)
          - base variables: t,x,y,z
          - user-defined variables: rup
       - Spatial distribution template: STEPDOWN
          [formula] = if((x<rlo),1,0)
          - base variables: t,x,y,z
          - user-defined variables: rlo
       - Spatial distribution template: CASEBHATTA96
          [formula] = pow(x/8.5,2.)*exp(-3.53*(x-8.5)/8.5)
          - base variables: t,x,y,z
          - user-defined variables: NONE
       - Spatial distribution template: SNRMODIFIED
          [formula] = pow(10.,dex*(x-rsol))*pow((x/rsol),a)*exp(b*(x-rsol)/rsol)
          - base variables: t,x,y,z
          - user-defined variables: rsol,a,b,dex
       - Spatial distribution template: STECKERJONES77
          [formula] = pow(x/10.,0.6)*exp(-3.*(x-10.)/10.)
          - base variables: t,x,y,z
          - user-defined variables: NONE
       - Spatial distribution template: STRONGMOSKA99
          [formula] = pow(x/8.5,0.5)*exp(-1.*(x-8.5)/8.5)
          - base variables: t,x,y,z
          - user-defined variables: NONE
       - Spatial distribution template: EINASTO_CYL
          [formula] = r_sp:=x*x+y*y;rhos*exp((-2/alpha)*(pow((r_sp/rs),alpha)-1))
          - base variables: t,x,y,z
          - user-defined variables: rhos,rs,alpha
       - Spatial distribution template: DUMMY
          [formula] = exp(-(x*x+y*y+z*z/8.))
          - base variables: t,x,y,z
          - user-defined variables: NONE

               ------------------
                 Cross sections
               ------------------

     -- Cross section files used:
        - $USINE/inputs/XS_NUCLEI/sigInelTripathi99+Coste12.dat
        - $USINE/inputs/XS_ANTINUC/sigInelANN_pbar+HHe_Donato01.dat
        - $USINE/inputs/XS_ANTINUC/sigInelANN_dbar+HHe_Duperray05.dat
        - $USINE/inputs/XS_NUCLEI/sigProdGALPROP17_OPT12.dat
        - $USINE/inputs/XS_ANTINUC/dSdEProd_pbar_1H4He+HHe_Donato01.dat
        - $USINE/inputs/XS_ANTINUC/dSdEProd_dbar_1H4He+HHe_Duperray05_Coal79MeV.dat
        - $USINE/inputs/XS_ANTINUC/dSdEProd_dbar_pbar+HHe_Duperray05_Coal79MeV.dat
        - $USINE/inputs/XS_ANTINUC/sigInelNONANN_pbar+HHe_Donato01.dat
        - $USINE/inputs/XS_ANTINUC/sigInelNONANN_dbar+HHe_Duperray05.dat
        - $USINE/inputs/XS_ANTINUC/dSdENAR_pbar+HHe_Duperray05_Anderson.dat
        - $USINE/inputs/XS_ANTINUC/dSdENAR_dbar+HHe_Duperray05_Anderson.dat
     -- Targets loaded: H,HE
     -- Tertiaries loaded: 1H-BAR,2H-BAR

                -----------------
                  Model summary
                -----------------

      --------------------------------
       1. Primary (standard) contrib.  ON
       2. Secondary contribution       ON
       3. Inelastic inter. (destruct.) ON
       4. Bremsstrahlung losses        OFF
       5. Coulomb losses               ON
       6. Inverse Compton losses       OFF
       7. Ionisation losses            ON
       8. Synchrotron losses           OFF
       9. Adiabatic losses (if Vgal)   ON
      10. Reacceleration               ON
      11. BETA-decay                   ON
      12. EC-decay                     ON
      13. BETA-fed                     ON
      14. EC-fed                       ON
      15. Tertiary contribution        ON
      16. Exotic contribution          OFF
      17. Wind                         ON
      --------------------------------
      => No free parameters
   > PrintSummaryTransport(f)
   > PrintSummaryModel(f)

                -----------------
                  Model summary
                -----------------

      --------------------------------
       1. Primary (standard) contrib.  ON
       2. Secondary contribution       ON
       3. Inelastic inter. (destruct.) ON
       4. Bremsstrahlung losses        OFF
       5. Coulomb losses               ON
       6. Inverse Compton losses       OFF
       7. Ionisation losses            ON
       8. Synchrotron losses           OFF
       9. Adiabatic losses (if Vgal)   ON
      10. Reacceleration               ON
      11. BETA-decay                   ON
      12. EC-decay                     ON
      13. BETA-fed                     ON
      14. EC-fed                       ON
      15. Tertiary contribution        ON
      16. Exotic contribution          OFF
      17. Wind                         ON
      --------------------------------
      => No free parameters
