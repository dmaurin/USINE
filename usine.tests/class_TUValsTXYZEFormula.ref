
          ----------------------------
            TUValsTXYZEFormula::TEST
          ----------------------------

- Class content: Container for space-time+E dependent formula

 * Set formula based on a 'Function Parser'-formatted string. In this example,
     - we use a pure space-time function (variables are t, x, y, and z);
     - free parameters of the function are 'par1' and 'par2'.
  This is done in 3 steps: i) create free parameters; ii) fill formula; iii) use formula.

   > TUFreeParList *par_list = new TUFreeParList();
   > par_list->AddPar(par1,"-",-1);
   > par_list->AddPar(par2,"-",-2);
   > par_list->PrintPars(f);
      => 2 free parameters:
                                                  par1 = -1.0000e+00 [-]              {updated value}
                                                  par2 = -1.0000e+00 [-]              {updated value}

 * Check TXYZ formula
   > SetFormula(formula="x1:=par1*cos(x);x2:=par2*sin(y);x1*x1+y1*y1", var="t,x,y,z", free_par=par_list, is_use_or_copy_freepars=true);
   > ValsTXYZEFormat();  =>  0
   > PrintFormula(f);
       [formula] = x1:=par1*cos(x);y1:=par2*sin(y);x1*x1+y1*y1
       - base variables: t,x,y,z
       - user-defined variables: par1,par2
   > GetFreePars()->PrintPars(f);
      => 2 free parameters:
                                                  par1 = -1.0000e+00 [-]              {updated value}
                                                  par2 = -1.0000e+00 [-]              {updated value}
   > GetFreePars()->GetParListStatus() = 1;
   > par_list->GetParListStatus() = 1;
   > EvalFormulaTXYZ(vals_txyz={0,1,1,0});    with (par1=-1,par2=-2) =>  1.000000e+00
   > GetFreePars()->GetParListStatus() = 0;
   > par_list->GetParListStatus() = 0;
   > EvalFormulaTXYZ(vals_txyz={0,2,1,0});    with (par1=-1,par2=-2) =>  8.812516e-01
   > GetFreePars()->GetParListStatus() = 0;
   > par_list->GetParListStatus() = 0;
 N.B.: par_list and GetFreePars() point to the same address: comparison of addresses = 1
   ---
   > par_list->SetParVal(par2, 2);
   > par_list->PrintPars(f);
      => 2 free parameters:
                                                  par1 = -1.0000e+00 [-]             
                                                  par2 = +2.0000e+00 [-]              {updated value}
   > par_list->GetParListStatus() = 1;
   > GetFreePars()->PrintPars(f);
      => 2 free parameters:
                                                  par1 = -1.0000e+00 [-]             
                                                  par2 = +2.0000e+00 [-]              {updated value}
   > GetFreePars()->GetParListStatus() = 1;
   > UpdateFromFreeParsAndResetStatus(is_force_update=false)
   > GetFreePars()->GetParListStatus() = 0;
   > EvalFormulaTXYZ(vals_txyz={0,1,1,0});    with (par1=-1,par2=2) =>  3.005472e+00
   > EvalFormulaTXYZ(vals_txyz={0,2,1,0});    with (par1=-1,par2=2) =>  3.005472e+00
   > TUCoordTXYZ *coord_txyz = new TUCoordTXYZ(); coord_txyz->SetVals(vals_txyz);
   > ValueTXYZ(TUCoordTXYZ *coord_txyz));                         =>  3.005472e+00


 * Check E-dep formula
   > SetFormula(formula="par1*beta*Etot^2", var="beta,gamma,Etot", free_par=par_list);
   > ValsTXYZEFormat();  =>  0
   > PrintFormula(f);
       [formula] = par1*beta*Etot^2
       - base variables: beta,gamma,Etot
       - user-defined variables: par1,par2
   > EvalFormulaE(edepvals={0.99, 2., 3.}); with (par1=-1) =>  -8.910000e+00
   > TUCoordE *coord_e = new TUCoordE(); coord_txyz->SetCoordE(edepvals, 3, 0);
   > ValueE(coord_e);                                      =>  -4.500000e+00
   > ValueETXYZ(coord_e, coord_txyz);                      =>  -0.000000e+00
         N.B.: FAILS AS "t,x,y,z" VARIABLES NOT DECLARED IN SetFormula()
   > SetFormula(formula="par1*beta*Etot^2", var="t,x,y,z,beta,gamma,Etot", free_par=par_list);
   > ValueETXYZ(coord_e, coord_txyz);                      =>  -4.500000e+00
         N.B.: NOW CORRECT, EVEN THOUGH coord_txyz UNUSED!
   > SetFormula(formula="par1*x*beta*Etot^2", var="t,x,y,z,beta,gamma,Etot", free_par=par_list);
   > ValueETXYZ(coord_e, coord_txyz);                      =>  -9.000000e+00


 * Copy formula
   > TUValsTXYZEFormula *formula = new TUValsTXYZEFormula();
   > formula->Copy(*this);
   > formula->PrintFormula(f);
       [formula] = par1*x*beta*Etot^2
       - base variables: t,x,y,z,beta,gamma,Etot
       - user-defined variables: par1,par2

 * Clone method
   > TUValsTXYZEFormula *formula2 = Clone();
   > formula2->PrintFormula(f);
       [formula] = par1*x*beta*Etot^2
       - base variables: t,x,y,z,beta,gamma,Etot
       - user-defined variables: par1,par2

