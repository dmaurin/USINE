
                ----------------
                  TUMisc::TEST
                ----------------

- Class content: The namespace TUMisc contains functions
to sort/cast/extract lists from strings and vice-versa.

 * String extractions (from list of integers, doubles, and strings...
   N.B.: blank spaces are systematically discarded (see in last example).

   > String2List(string=??2?5??2?4?56?2, separator="?", output=vector<string>);
       2        5        2        4        56        2 
   > RemoveDuplicates(vector<int>);
       2        5        4        56 
   > String2List(string=fff2.5f3.1ffff2.51ff2.5099999fff, separator="f", output=vector<string>);
       2.500000e+00        3.100000e+00        2.510000e+00        2.510000e+00 
   > RemoveDuplicates(vector<string>, thresh=1.000000e-04);
       2.500000e+00        3.100000e+00        2.510000e+00 
   > RemoveDuplicates(vector<string>, thresh=1.000000e-01);
       2.500000e+00        3.100000e+00 
   > String2List(string=ar#&AA&~ghh,AA;kk::bb,AA<sdd!ss,ggg,AA,, separators=",:<;", output=vector<string>);
       ar#&AA&~ghh        AA        kk        bb        AA        sdd!ss        ggg        AA 
   > RemoveDuplicates(vector<string>);
       ar#&AA&~ghh        AA        kk        bb        sdd!ss        ggg 
   > String2List(string=Double_t  @DensityH  @hidden=0, separator="@", output=vector<string>);
       Double_t        DensityH        hidden=0 
   > List2String(input=vector<string> above, ',');  =>  Double_t,DensityH,hidden=0

   > RemoveBlancksFromStartStop(blanks=" 	 	 bob 	 	");  =>  "bob"

   > RemoveChars("B/C-(68)","/-()");  =>  BC68

   > vector<string> = ExtractFromDir($USINE/inputs/XS*, is_verbose=true, f_out=f, is_selection=false, exclude_pattern="", is_test=true);
      2 files found for $USINE/inputs/XS*:
          1 $USINE/inputs/XS_ANTINUC
          2 $USINE/inputs/XS_NUCLEI
   > vector<string> = ExtractFromDir($USINE/inputs/XS_ANTINUC/dSdEProd_pbar*, is_verbose=true, f_out=f, is_selection=false, exlude_pattern="", is_test=true));
      4 files found for $USINE/inputs/XS_ANTINUC/dSdEProd_pbar*:
          1 $USINE/inputs/XS_ANTINUC/dSdEProd_pbar_1H4He+HHe_BringmannSalati07.dat
          2 $USINE/inputs/XS_ANTINUC/dSdEProd_pbar_1H4He+HHe_Donato01.dat
          3 $USINE/inputs/XS_ANTINUC/dSdEProd_pbar_1H4He+HHe_Duperray05.dat
          4 $USINE/inputs/XS_ANTINUC/dSdEProd_pbar_1Hto30Si+HHe_Boudaud19.dat
   > vector<string> = ExtractFromDir($USINE/inputs/XS_ANTINUC/dSdEProd_pbar*, is_verbose=true, f_out=f, is_selection=false, exlude_pattern="tertiary", is_test=true));
      4 files found for $USINE/inputs/XS_ANTINUC/dSdEProd_pbar*:
          1 $USINE/inputs/XS_ANTINUC/dSdEProd_pbar_1H4He+HHe_BringmannSalati07.dat
          2 $USINE/inputs/XS_ANTINUC/dSdEProd_pbar_1H4He+HHe_Donato01.dat
          3 $USINE/inputs/XS_ANTINUC/dSdEProd_pbar_1H4He+HHe_Duperray05.dat
          4 $USINE/inputs/XS_ANTINUC/dSdEProd_pbar_1Hto30Si+HHe_Boudaud19.dat

 * Upper- and lower-case-isation
   > UpperCase(c);  =>  C
   > UpperCase(test)  =>  TEST

 * Format for print or root-displays
   > FormatCRQty(qty=POSITRON/ELECTRON+POSITRON, switch_format=1);  => e+/(e-+e+)
   > FormatCRQty(qty=POSITRON/ELECTRON+POSITRON, switch_format=2);  => e^{+}/(e^{-}+e^{+})
   > FormatCRQty(qty=10Be/9Be, switch_format=1);  => 10Be/9Be
   > FormatCRQty(qty=10Be/9Be, switch_format=2);  => ^{10}Be/^{9}Be
   > FormatCRQtyName(qty=E+/(e-+e+));  => qty=POSITRON/ELECTRON+POSITRON


   > RemoveSpecialChars("AMS-02(2011/06-2013/07)") = AMS02_201106201307_

